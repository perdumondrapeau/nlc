/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/span.hpp>

namespace nlc {
class allocator;
}  // namespace nlc

// This file provides an 8-radix sort implementation.
// This algorithm is capable of sorting arithmetic types (integers & floating point decimals) in
// linear time. However, it requires some memory so it's not faster than a comparison sort for few
// elements. It's better if you have approximately some hundreds / thousands of entries,
// depending on the type of entry.
// TODO: Make some benchmarks to document more precisely the use cases.

// References:
// [1] http://www.codercorner.com/RadixSortRevisited.htm
// [2] http://stereopsis.com/radix.html
// [3] http://www.heterogeneouscompute.org/wordpress/wp-content/uploads/2011/06/RadixSort.pdf
// [4]
// https://www.intel.com/content/dam/www/public/us/en/documents/technology-briefs/intel-labs-radix-sort-mic-report.pdf
// [5] http://www.vldb.org/pvldb/vol8/p1274-inoue.pdf

// Possible improvements:
// - Find the best radix size ([2])
// - Use prefetch instruction ([2])
// - Use SIMD to map values to unsigned integers
// - Other improvements found in [3], [4] & [5]

namespace nlc {

// Very fast and straightforward if your data happen to be 1-byte sized.
// These functions require no dynamic allocation, and only 256 * sizeof(usize) static memory.
auto radix_sort(span<byte> data) -> void;
auto radix_sort(span<i8> data) -> void;

// Standard radix sort, with internal dynamic allocation of size data.size() * sizeof(T) bytes.
// See the following overload if you want to use some external memory.
template<typename T> auto radix_sort(allocator & allocator, span<T> data) -> void;

}  // namespace nlc

#include "detail/radix_sort.inl"
