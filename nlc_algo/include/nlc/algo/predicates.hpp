/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

namespace nlc {

template<typename T> constexpr auto eq(T const & lhs, T const & rhs) -> bool { return lhs == rhs; }

template<typename T> constexpr auto less(T const & lhs, T const & rhs) -> bool { return lhs < rhs; }

template<typename T> constexpr auto less_eq(T const & lhs, T const & rhs) -> bool {
    return eq<T>(lhs, rhs) || less<T>(lhs, rhs);
}

template<typename T> constexpr auto greater(T const & lhs, T const & rhs) -> bool {
    return lhs > rhs;
}

template<typename T> constexpr auto greater_eq(T const & lhs, T const & rhs) -> bool {
    return eq<T>(lhs, rhs) || greater<T>(lhs, rhs);
}

}  // namespace nlc
