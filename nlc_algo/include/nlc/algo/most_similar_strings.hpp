/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/scoped.hpp>
#include <nlc/meta/numeric_limits.hpp>

namespace nlc {
class allocator;

struct MssOptionalArgs final {
    usize max_result_count { meta::limits<usize>::max };
    usize max_tolerated_distance { meta::limits<usize>::max };
};

auto find_most_similar_strings(allocator & allocator,
                               string_view const reference_string,
                               span<string_view const> const strings,
                               MssOptionalArgs const optional_args = MssOptionalArgs {})
    -> scoped<span<string_view>>;
}  // namespace nlc
