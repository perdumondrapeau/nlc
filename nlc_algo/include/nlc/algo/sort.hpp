/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/scoped.hpp>

namespace nlc {

class allocator;

// If your predicate is branchless, specify it to enjoy a potential speedup
template<typename T> inline auto sort(span<T> const values) -> void;
template<bool is_predicate_branchless, typename Pred, typename T>
inline auto sort(span<T> const values, Pred && predicate) -> void;

// Values are sorted using the value of their projection.
// The `projection` will be called once per value and must return a value on which the < operator
// can be used.
template<typename Proj, typename T, typename IndexType = u16>
[[nodiscard]] auto projective_sort(allocator & allocator,
                                   span<T const> const values,
                                   Proj && projection) -> scoped<span<IndexType>>;

// Quick sort of three values, always performs 3 predicate calls
template<typename T> inline auto sort_three(T & v1, T & v2, T & v3) -> void;
template<typename T, typename Pred>
inline auto sort_three(T & v1, T & v2, T & v3, Pred && predicate) -> void;

}  // namespace nlc

#include "detail/sort.inl"
