/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "meta.hpp"

namespace nlc {

template<typename T> inline auto count(span<T> const values, T const & value) -> usize {
    return count_if(values.begin(), values.end(), [&value](auto v) { return v == value; });
}

template<typename It>
inline auto count(It const begin, It const end, impl::it_value<It> const & value) -> usize {
    return count_if(begin, end, [&value](auto v) { return v == value; });
}

template<typename T, typename Pred>
inline auto count_if(span<T> const values, Pred predicate) -> usize {
    return count_if(values.begin(), values.end(), nlc_fwd(predicate));
}

template<typename It, typename Pred>
inline auto count_if(It const begin, It const end, Pred predicate) -> usize {
    usize count = 0u;

    for (It it = begin; it != end; ++it) {
        if (predicate(*it)) {
            ++count;
        }
    }

    return count;
}

}  // namespace nlc
