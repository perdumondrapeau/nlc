/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/fundamentals/function.hpp>
#include <nlc/meta/basics.hpp>

namespace nlc {
template<typename T, typename Pred, typename IndexType>
inline auto filter(allocator & allocator,
                   span<T const> const values,
                   Pred predicate) -> scoped<span<IndexType>> {
    auto const filtered_indices_buffer { allocator.allocate<IndexType>(values.size()) };
    auto * next_entry_ptr { filtered_indices_buffer.begin() };

    for (IndexType i = 0; auto & v : values) {
        defer { ++i; };

        if (predicate(v))
            *(next_entry_ptr++) = i;
    }

    return { span<IndexType> { filtered_indices_buffer.begin(), next_entry_ptr },
             function<void(span<IndexType> &)> { allocator,
                                                 [&allocator, filtered_indices_buffer](auto &) {
                                                     allocator.free(filtered_indices_buffer);
                                                 } } };
}

}  // namespace nlc
