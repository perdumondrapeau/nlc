/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/sort.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/allocator.hpp>

namespace nlc {

template<typename T, typename Func>
inline auto for_each_sorted(allocator & allocator, span<T> const values, Func && function) -> void {
    for_each_sorted<true>(allocator, values, nlc_fwd(function), less<T>);
}

template<bool is_predicate_branchless, typename Pred, typename T, typename Func>
inline auto for_each_sorted(allocator & allocator,
                            span<T> const values,
                            Func && function,
                            Pred && predicate) -> void {
    for_each_sorted<is_predicate_branchless>(allocator,
                                             values.begin(),
                                             values.size(),
                                             nlc_fwd(function),
                                             nlc_fwd(predicate));
}

template<bool is_predicate_branchless, typename Pred, typename Iter, typename Func>
inline auto for_each_sorted(allocator & allocator,
                            Iter const begin,
                            Iter const end,
                            Func && function,
                            Pred && predicate) -> void {
    usize size = 0u;
    for (auto it = begin; it != end; ++it)
        ++size;

    for_each_sorted<is_predicate_branchless>(allocator, begin, size, nlc_fwd(function), nlc_fwd(predicate));
}

template<bool is_predicate_branchless, typename Pred, typename Iter, typename Func>
inline auto for_each_sorted(allocator & allocator,
                            Iter const begin,
                            usize const size,
                            Func && function,
                            Pred && predicate) -> void {
    if (size == 0u)
        return;

    auto const value_ptrs = allocator.allocate<impl::value_ptr<Iter>>(size);
    defer { allocator.free(value_ptrs); };

    for_each_sorted<is_predicate_branchless>(value_ptrs, begin, nlc_fwd(function), nlc_fwd(predicate));
}

template<bool is_predicate_branchless, typename Pred, typename Iter, typename Func>
inline auto for_each_sorted(span<impl::value_ptr<Iter>> const buffer,
                            Iter const begin,
                            Func && function,
                            Pred && predicate) -> void {
    if (buffer.is_empty())
        return;

    auto const size = buffer.size();

    // Fill buffer with value pointers
    auto it = begin;
    for (auto & ptr : buffer) {
        ptr = &*it++;
    }

    // Apply sort
    auto internal_predicate = [pred = nlc_fwd(predicate)](auto const rhs, auto const lhs) {
        return pred(*rhs, *lhs);
    };
    sort<is_predicate_branchless>(buffer, nlc_fwd(internal_predicate));

    // Call the function
    for (auto i = 0u; i < size; ++i) {
        function(*buffer[i]);
    }
}

}  // namespace nlc
