/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/buffer.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/function_view.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/numeric_limits.hpp>
#include <nlc/meta/traits.hpp>

namespace nlc::radix_impl {

inline constexpr auto radix = 1 << 8;
template<typename T> using sort_func = function_view<void(span<meta::make_unsigned<T>>)>;
using bucket_elem_type = usize;
using bucket_type = buffer<bucket_elem_type, radix>;
inline constexpr auto bucket_byte_size = radix * sizeof(bucket_elem_type);

template<typename T> struct signed_unsigned_integer_delta {
    static_assert(meta::is_integer<T>);
    using signed_type = meta::make_signed<T>;
    using unsigned_type = meta::make_unsigned<T>;

    static inline constexpr T value = static_cast<T>(
        meta::limits<unsigned_type>::max - static_cast<unsigned_type>(meta::limits<signed_type>::max));
};

template<typename T, typename uint_type> struct unsigned_integer_mapping final {
    function_view<void(T &)> map;
    function_view<void(uint_type &)> unmap;
};

auto compute_prefix_sums(bucket_type & counters) -> void;

template<typename T> inline auto get_byte(T value, u8 byte_offset) -> byte {
    nlc_assert_msg(sizeof(T) > byte_offset, nlc_dump_var(sizeof(T)), nlc_dump_var(byte_offset));
    return reinterpret_cast<byte const *>(&value)[byte_offset];
}

template<typename T> inline constexpr auto shift_to_unsigned(T value) -> meta::make_unsigned<T> {
    static_assert(meta::is_signed_integer<T>);
    static_assert(meta::is_same<T, meta::make_signed<T>>);
    using signed_type = meta::make_signed<T>;
    using unsigned_type = meta::make_unsigned<T>;

    constexpr auto delta = signed_unsigned_integer_delta<signed_type>::value;

    return static_cast<unsigned_type>(value) + static_cast<unsigned_type>(delta);
}

template<typename T> inline constexpr auto shift_to_signed(T value) -> meta::make_signed<T> {
    static_assert(meta::is_unsigned_integer<T>);
    static_assert(meta::is_same<T, meta::make_unsigned<T>>);
    using signed_type = meta::make_signed<T>;
    using unsigned_type = meta::make_unsigned<T>;

    constexpr auto delta = signed_unsigned_integer_delta<unsigned_type>::value;

    return static_cast<signed_type>(value - delta);
}

template<typename T> auto count_occurences(span<T> data, span<bucket_type> buckets) -> void {
    constexpr auto byte_count = sizeof(T);
    nlc_assert(buckets.size() == byte_count);

    for (u8 byte_offset = 0u; byte_offset < byte_count; ++byte_offset) {
        auto & bucket = buckets[byte_offset];
        memset(reinterpret_cast<void *>(bucket.ptr()), 0, bucket_byte_size);

        for (auto const value : data) {
            auto const & byte_value = get_byte(value, byte_offset);
            ++bucket[byte_value];
        }
    }
}

template<typename T>
auto partial_sort(span<T const> input, span<T> output, bucket_type & prefix_sums, u8 byte_offset) -> void {
    auto const count = input.size();
    nlc_assert_msg(output.size() == count, nlc_dump_var(count), nlc_dump_var(output.size()));

    for (usize i = 0u; i < count; ++i) {
        auto const & full_value = input[count - i - 1u];
        auto const byte_value = get_byte(full_value, byte_offset);
        nlc_assert(prefix_sums[byte_value] > 0u);
        auto const index = --prefix_sums[byte_value];

        output[index] = full_value;
    }
}

template<typename T> auto swap_buffers(span<T> ** input, span<T> ** output) -> void {
    nlc_assert(input != nullptr);
    nlc_assert(output != nullptr);

    auto * const tmp = *input;
    *input = *output;
    *output = tmp;
}

template<typename T> auto sort_unsigned_int(span<T> data, span<T> buffer) -> void {
    static_assert(meta::is_unsigned_integer<T>);
    static_assert(sizeof(T) > 1u);

    auto * input = &data;
    auto * output = &buffer;

    // Memory for counting
    bucket_type counters[sizeof(T)];

    count_occurences(*input, counters);
    for (auto i = 0u; i < sizeof(T); ++i) {
        auto const byte_offset = static_cast<u8>(i);
        compute_prefix_sums(counters[byte_offset]);
        partial_sort(span<T const> { *input }, *output, counters[byte_offset], byte_offset);
        swap_buffers(&input, &output);
    }

    nlc_assert_msg(input == &data, "An odd number of swaps happened");
}

template<typename T, typename uint_type>
auto sort_mapped(span<T> data,
                 unsigned_integer_mapping<T, uint_type> const & mapping,
                 sort_func<uint_type> const & sort_unsigned_func) -> void {
    for (auto & value : data)
        mapping.map(value);

    auto const unsigned_int_data = span_cast<uint_type>(data);
    sort_unsigned_func(unsigned_int_data);

    for (auto & value : unsigned_int_data)
        mapping.unmap(value);
}

template<typename T>
auto sort_signed_int(span<T> data, sort_func<T> const & sort_unsigned_func) -> void {
    static_assert(meta::is_signed_integer<T>);
    static_assert(meta::is_same<T, meta::make_signed<T>>);
    using unsigned_type = meta::make_unsigned<T>;

    unsigned_integer_mapping<T, unsigned_type> const mapping = {
        [](T & signed_value) {
            auto const unsigned_value = shift_to_unsigned(signed_value);
            signed_value = reinterpret_cast<T const &>(unsigned_value);
        },
        [](unsigned_type & unsigned_value) {
            auto const signed_value = shift_to_signed(unsigned_value);
            unsigned_value = reinterpret_cast<unsigned_type const &>(signed_value);
        }
    };

    sort_mapped(data, mapping, sort_unsigned_func);
}

template<usize size> struct float_mapping_infos {};

template<> struct float_mapping_infos<4u> {
    using float_type = f32;
    using int_type = u32;
    static inline constexpr int_type sign_shift = sizeof(float_type) * 8 - 1;
    static inline constexpr int_type positive_flip_mask = 0x80'00'00'00;  // Only flip the bit sign
    static inline constexpr int_type negative_flip_mask = 0xFF'FF'FF'FF;  // Flip every bit
};

template<> struct float_mapping_infos<8u> {
    using float_type = f64;
    using int_type = u64;
    static inline constexpr int_type sign_shift = sizeof(float_type) * 8 - 1;
    static inline constexpr int_type positive_flip_mask = 0x80'00'00'00'00'00'00'00;
    static inline constexpr int_type negative_flip_mask = 0xFF'FF'FF'FF'FF'FF'FF'FF;
};

template<typename T> auto sort_floating(span<T> data, span<T> buffer) -> void {
    static_assert(meta::is_floating<T>);

    if constexpr (meta::is_same<T, f32> || meta::is_same<T, f64>) {
        using mapping_infos = float_mapping_infos<sizeof(T)>;
        using int_type = typename mapping_infos::int_type;
        static_assert(meta::is_same<T, typename mapping_infos::float_type>);
        static_assert(meta::is_unsigned_integer<int_type>);

#if defined(__clang__)
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wundefined-reinterpret-cast"  // reinterpret_cast from
                                                                   // 'float_type' to 'int_type &'
                                                                   // has undefined behavior
#endif
        unsigned_integer_mapping<T, int_type> const mapping {
            [](T & value) {
                auto & bits = reinterpret_cast<int_type &>(value);
                auto const is_negative = bits >> mapping_infos::sign_shift;
                bits ^= is_negative ? mapping_infos::negative_flip_mask
                                    : mapping_infos::positive_flip_mask;
            },
            [](int_type & value) {
                auto const was_negative = 1 - (value >> mapping_infos::sign_shift);
                value ^= was_negative ? mapping_infos::negative_flip_mask
                                      : mapping_infos::positive_flip_mask;
            }
        };
#if defined(__clang__)
#    pragma GCC diagnostic pop
#endif

        sort_mapped(data, mapping, [buffer](span<int_type> const unsigned_data) {
            sort_unsigned_int(unsigned_data, span_cast<int_type>(buffer));
        });
    } else
        nlc_assert_msg(false, "not radix_implemented");
}
}  // namespace nlc::radix_impl

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace nlc {

template<typename T> auto radix_sort(allocator & allocator, span<T> data) -> void {
    auto const buffer { allocator.allocate<T>(data.size()) };
    defer { allocator.free(buffer); };

    if constexpr (meta::is_integer<T> && sizeof(T) == 1)
        radix_sort(data);
    else if constexpr (meta::is_unsigned_integer<T>)
        radix_impl::sort_unsigned_int(data, buffer);
    else if constexpr (meta::is_signed_integer<T>) {
        radix_impl::sort_signed_int(data, [buffer](auto unsigned_data) {
            using unsigned_type = meta::make_unsigned<T>;
            auto const unsigned_buffer = span_cast<unsigned_type>(buffer);
            radix_impl::sort_unsigned_int(unsigned_data, unsigned_buffer);
        });
    } else if constexpr (meta::is_floating<T>)
        radix_impl::sort_floating(data, buffer);
    else
        nlc_assert_msg(false, "not implemented");
}

}  // namespace nlc
