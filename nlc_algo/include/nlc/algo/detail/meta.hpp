
#pragma once

#include <nlc/meta/basics.hpp>
#include <nlc/meta/traits.hpp>

namespace nlc::impl {
template<typename It> using it_value = meta::rm_ref<decltype(*declval<It>)>;
}  // namespace nlc::impl
