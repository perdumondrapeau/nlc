/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/swap.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/meta/basics.hpp>

namespace nlc::detail {

namespace impl::heap_sort {
    inline auto get_left_child_index(usize i) -> usize { return 2u * i + 1u; }
    inline auto get_right_child_index(usize i) -> usize { return 2u * i + 2u; }
    inline auto get_parent_index(usize i) -> usize {
        nlc_assert(i > 0u);
        return (i - 1u) / 2u;
    }

    template<typename T, typename Pred>
    auto move_up_heap(span<T> heap, usize i, Pred predicate) -> void {
        while (i > 0) {
            // If parent's value is less than the node value, we reached the right position
            auto const parent_index = get_parent_index(i);
            if (predicate(heap[parent_index], heap[i])) {
                break;
            }

            // Else, we swap with the parent and continue
            swap(heap[parent_index], heap[i]);
            i = parent_index;
        }
    }

    template<typename T, typename Pred>
    auto move_down_heap(span<T> heap, usize i, Pred predicate) -> void {
        auto const count = heap.size();

        while (i < count) {
            auto const left_child_index = get_left_child_index(i);
            auto const right_child_index = get_right_child_index(i);
            if (left_child_index >= count) {
                break;
            }

            // Computing the child which values is the lowest
            auto const min_child_index =
                [left_child_index, right_child_index, count, &heap, &predicate]() {
                    if (right_child_index >= count) {
                        return left_child_index;
                    }

                    return predicate(heap[left_child_index], heap[right_child_index])
                               ? left_child_index
                               : right_child_index;
                }();

            // If the two children have higher values, we reached the good position
            if (predicate(heap[i], heap[min_child_index])) {
                break;
            }

            // Else, we swap with the min child and continue
            swap(heap[i], heap[min_child_index]);
            i = min_child_index;
        }
    }

    template<typename T, typename Pred> auto make_heap(span<T> values, Pred predicate) -> void {
        for (auto i = 0u; i < values.size(); ++i) {
            auto heap = nlc::span(values.begin(), i + 1u);
            move_up_heap(heap, i, predicate);
        }
    }

    template<typename T, typename Pred> auto sort(span<T> values, Pred predicate) -> void {
        auto const count = values.size();
        for (auto i = 0u; i < count; ++i) {
            auto const target_index = count - i - 1u;
            swap(values[0], values[target_index]);
            auto heap = nlc::span(values.begin(), target_index);
            move_down_heap(heap, 0, predicate);
        }
    }
}  // namespace impl::heap_sort

template<typename T, typename Pred> auto heap_sort(span<T> values, Pred predicate) -> void {
    // We invert the predicate for the heap: its top element will be put at the end of the output
    auto const heap_predicate = [&predicate](auto lhs, auto rhs) { return !predicate(lhs, rhs); };

    impl::heap_sort::make_heap(values, heap_predicate);
    impl::heap_sort::sort(values, heap_predicate);
}

}  // namespace nlc::detail
