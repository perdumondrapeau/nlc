/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/predicates.hpp>
#include <nlc/algo/swap.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/function.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/numeric_limits.hpp>

#include <pdqsort.hpp>

namespace nlc {

template<typename T> inline auto sort(span<T> const values) -> void { sort<true>(values, less<T>); }

template<bool is_predicate_branchless, typename Pred, typename T>
inline auto sort(span<T> const values, Pred && predicate) -> void {
    pdqsort<is_predicate_branchless>(values.begin(), values.end(), nlc_fwd(predicate));
}

template<typename Proj, typename T, typename IndexType>
auto projective_sort(allocator & allocator,
                     span<T const> const values,
                     Proj && projection) -> scoped<span<IndexType>> {
    struct ProjectionType final {
        decltype(declval<Proj>(declval<T>)) value;
        IndexType index;
    };

    if (values.is_empty())
        return { span<IndexType> { nullptr, nullptr }, [](auto const &) {} };

    auto const value_count { values.size() };
    nlc_assert_msg(value_count <= meta::limits<IndexType>::max,
                   "projective_sort: IndexType is not big enough to index the ",
                   value_count,
                   " values to sort");

    auto const projected_values { allocator.allocate<ProjectionType>(value_count) };
    defer { allocator.free(projected_values); };

    // Compute projected values
    for (IndexType i = 0u; auto const & v : values) {
        defer { ++i; };
        projected_values[i].value = projection(v);
        projected_values[i].index = i;
    }

    sort<true>(projected_values, [](ProjectionType const & a, ProjectionType const & b) {
        return less(a.value, b.value);
    });

    auto const indices { allocator.allocate<IndexType>(value_count) };
    for (IndexType i = 0u; i < value_count; ++i)
        indices[i] = projected_values[i].index;

    return { indices, function<void(span<IndexType> &)> { allocator, [&allocator](auto & v) -> void {
                                                             allocator.free(v);
                                                         } } };
}

template<typename T> inline auto sort_three(T & v1, T & v2, T & v3) -> void {
    sort_three(v1, v2, v3, less<T>);
}

template<typename T, typename Pred>
inline auto sort_three(T & v1, T & v2, T & v3, Pred && predicate) -> void {
    if (predicate(v1, v2) == false)
        swap(v1, v2);
    if (predicate(v1, v3) == false)
        swap(v1, v3);
    if (predicate(v2, v3) == false)
        swap(v2, v3);

    nlc_assert(predicate(v1, v2));
    nlc_assert(predicate(v2, v3));
}

}  // namespace nlc
