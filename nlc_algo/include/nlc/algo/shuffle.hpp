/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/range.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/random.hpp>

#include "swap.hpp"

namespace nlc {

template<typename T> inline auto shuffle(xoroshiro128plus & rng, span<T> const values) -> void {
    auto const size = values.size();

    if (size < 2)
        return;

    for (auto i : range(0u, size - 1)) {
        auto const j = rng.in_range(i, size);
        swap(values[i], values[j]);
    }
}

}  // namespace nlc
