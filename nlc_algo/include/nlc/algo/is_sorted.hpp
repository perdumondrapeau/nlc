/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/algo/predicates.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/meta/basics.hpp>

#include "detail/meta.hpp"

namespace nlc {

template<typename It, typename Pred = decltype(less_eq<impl::it_value<It>>)>
auto is_sorted(It const begin, It const end, Pred predicate = less_eq<impl::it_value<It>>) -> bool {
    for (auto it = begin; it != end;) {
        auto current = it;
        auto next = ++it;
        if (next != end && !predicate(*current, *next))
            return false;
    }

    return true;
}

template<typename T, typename Pred = decltype(less_eq<T>)>
inline auto is_sorted(span<T> data, Pred predicate = less_eq<T>) -> bool {
    return is_sorted(data.begin(), data.end(), nlc_fwd(predicate));
}

}  // namespace nlc
