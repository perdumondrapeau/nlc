/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/algo/predicates.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/meta/basics.hpp>

namespace nlc {
class allocator;
}  // namespace nlc

namespace nlc {

namespace impl {
    template<typename Iter> using value_ptr = decltype(&*declval<Iter>);
}  // namespace impl

// Contiguous values
template<typename T, typename Func>
inline auto for_each_sorted(allocator & allocator, span<T> const values, Func && function) -> void;

template<bool is_predicate_branchless, typename Pred, typename T, typename Func>
inline auto for_each_sorted(allocator & allocator,
                            span<T> const values,
                            Func && function,
                            Pred && predicate) -> void;

// Arbitrarily-organized values
template<bool is_predicate_branchless, typename Pred, typename Iter, typename Func>
inline auto for_each_sorted(allocator & allocator,
                            Iter const begin,
                            Iter const end,
                            Func && function,
                            Pred && predicate) -> void;

template<bool is_predicate_branchless, typename Pred, typename Iter, typename Func>
inline auto for_each_sorted(allocator & allocator,
                            Iter const begin,
                            usize const size,
                            Func && function,
                            Pred && predicate) -> void;

template<bool is_predicate_branchless, typename Pred, typename Iter, typename Func>
inline auto for_each_sorted(span<impl::value_ptr<Iter>> const buffer,
                            Iter const begin,
                            Func && function,
                            Pred && predicate) -> void;

}  // namespace nlc

#include "detail/for_each_sorted.inl"
