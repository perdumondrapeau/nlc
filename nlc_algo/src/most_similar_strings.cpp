/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "most_similar_strings.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/function.hpp>
#include <nlc/fundamentals/string_manip.hpp>
#include "nlc/dialect/arithmetic_types.hpp"

#include "sort.hpp"

namespace nlc {
struct DistanceAndIndex final {
    usize distance;
    usize index;
};

auto find_most_similar_strings(allocator & allocator,
                               string_view const reference_string,
                               span<string_view const> const strings,
                               MssOptionalArgs const optional_args) -> scoped<span<string_view>> {
    nlc_assert_msg(optional_args.max_result_count == meta::limits<usize>::max ||
                       optional_args.max_result_count <= strings.size(),
                   "find_most_similar_strings: required max string count must not be greater than "
                   "string count",
                   nlc_dump_var(optional_args.max_result_count),
                   nlc_dump_var(strings.size()));

    // Shortcut
    if (strings.is_empty())
        return { span<string_view> { nullptr, nullptr }, [](auto &) {} };

    auto const max_output_size { strings.size() < optional_args.max_result_count
                                     ? strings.size()
                                     : optional_args.max_result_count };
    auto output_buffer { allocator.allocate<string_view>(max_output_size) };

    // Compute Levenshtein distance for all input strings
    auto const distance_buffer { allocator.allocate<DistanceAndIndex>(strings.size()) };
    defer { allocator.free(distance_buffer); };

    for (auto i = 0u; auto const string : strings) {
        defer { ++i; };
        distance_buffer[i].distance = levenshtein_distance(allocator, reference_string, string);
        distance_buffer[i].index = i;
    }

    // Sort the result
    sort<false>(distance_buffer, [](auto const & a, auto const & b) {
        return a.distance == b.distance ? a.index < b.index : a.distance < b.distance;
    });

    // Fill the output buffer
    usize output_size { 0u };
    for (auto i = 0u; i < max_output_size; ++i) {
        auto const & [distance, index] = distance_buffer[i];
        if (distance >= optional_args.max_tolerated_distance)
            break;

        output_buffer[i] = strings[index];
        ++output_size;
    }

    return { span<string_view> { output_buffer.begin(), output_size },
             function<void(span<string_view> &)> { allocator, [&allocator, output_buffer](auto &) {
                                                      allocator.free(output_buffer);
                                                  } } };
}
}  // namespace nlc
