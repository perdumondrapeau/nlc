/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/types.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/meta/units.hpp>
#include <nlc/test.hpp>

class non_allocator final : public nlc::allocator {
  protected:
    [[nodiscard]] auto _allocate(nlc::bytes<usize> size,
                                 nlc::alignment align) -> nlc::span<byte> override {
        nlc_check_msg(false,
                      "Tried to allocate ",
                      nlc::rm_unit(size),
                      " bytes with alignment ",
                      nlc::rm_unit(align));
        return { nullptr, nullptr };
    }

    [[nodiscard]] auto _reallocate(nlc::span<byte> old,
                                   nlc::bytes<usize> size,
                                   nlc::alignment align) -> nlc::span<byte> override {
        nlc_check_msg(false,
                      "Tried to allocate ",
                      old.size(),
                      " bytes to ",
                      nlc::rm_unit(size),
                      " bytes with alignment ",
                      nlc::rm_unit(align));
        return { nullptr, nullptr };
    }

    auto _free(nlc::span<byte> memory) -> void override {
        nlc_check_msg(false, "Tried to free ", memory.size(), " bytes");
    }
};
