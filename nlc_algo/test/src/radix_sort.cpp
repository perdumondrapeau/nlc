/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/is_sorted.hpp>
#include <nlc/algo/predicates.hpp>
#include <nlc/algo/radix_sort.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/memory.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/test.hpp>

#include "sort_utils.hpp"

using namespace nlc;

template<typename T> void test_signed_unsigned_mapping() {
    using namespace radix_impl;
    using signed_type = meta::make_signed<T>;
    using unsigned_type = meta::make_unsigned<T>;
    using signed_limits = meta::limits<signed_type>;
    using unsigned_limits = meta::limits<unsigned_type>;

    constexpr unsigned_type unsigned_limits_values[] = { unsigned_limits::min, unsigned_limits::max };
    for (auto const u : unsigned_limits_values) {
        [[maybe_unused]] auto const s = shift_to_signed(u);
        nlc_check_msg(shift_to_unsigned(s) == u, u, s);
    }

    constexpr signed_type signed_limits_values[] = { signed_limits::min, signed_limits::max };
    for (auto const s : signed_limits_values) {
        [[maybe_unused]] auto const u = shift_to_unsigned(s);
        nlc_check_msg(shift_to_signed(u) == s, s, u);
    }
}

template<typename T, typename sort_func>
auto sort_and_check(allocator & allocator, span<T> const data, sort_func sort) -> void {
    auto const count = data.size();
    auto const values = allocator.allocate<T>(count);
    defer {
        for (auto & v : values)
            v.~T();
        allocator.free(values);
    };

    memcpy(reinterpret_cast<void *>(values.ptr()),
           reinterpret_cast<void *>(data.begin()),
           count * sizeof(T));

    sort(values);
    nlc_check(nlc::is_sorted(values));
    nlc_check(is_permutation(allocator, make_span(data), values));
}

template<typename T> auto sort_and_check(allocator & allocator, span<T> const data) -> void {
    sort_and_check<T>(allocator, data, [&allocator](auto const values_span) {
        radix_sort(allocator, values_span);
    });
}

nlc_test("basic_usage") {
    standard_allocator allocator;

    test_signed_unsigned_mapping<u8>();
    test_signed_unsigned_mapping<u16>();
    test_signed_unsigned_mapping<u32>();
    test_signed_unsigned_mapping<u64>();

    // u8
    {
        byte data[] = { 5, 125, 7, 212, 0 };
        sort_and_check(allocator, make_span(data));
    }

    // u16
    {
        u16 data[] = { 5 << 8 | 125, 7 << 8 | 212, 37 << 8 | 250, 42 << 8 | 0,  5 << 8 | 13,
                       41 << 8 | 0,  7 << 8 | 212, 7 << 8 | 212,  7 << 8 | 212, 7 << 8 | 212 };
        sort_and_check(allocator, make_span(data));
    }

    // i16
    {
        i16 data[] = { -5, 4, 12, 120, -57, 4, 4, -5, 4 };
        sort_and_check(allocator, make_span(data));
    }

    // f32
    {
        f32 data[] = { 5.23664f, -86E10f, -87E10f, -86E11f, 0.f, -0.f, 0.00000001f, -0.00001f };
        sort_and_check(allocator, make_span(data));
    }

    // f64
    {
        f64 data[] = { 5.23664, -86E10, -87E10, -86E11, 0., -0., 0.00000001, -0.00001 };
        sort_and_check(allocator, make_span(data));
    }
}
