/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/shuffle.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/memory.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/random.hpp>
#include <nlc/meta/traits.hpp>
#include <nlc/test.hpp>
#include <string.h>

#include "sort_utils.hpp"

nlc_test("shuffle - empty input") {
    constexpr nlc::span<int> null_span;
    constexpr u64 seed { 0xBACABACA };
    nlc::xoroshiro128plus rng { seed };

    nlc::shuffle(rng, null_span);
}

nlc_test("shuffle - basic") {
    constexpr u64 seed { 0xBACABACA };
    constexpr int input_values[] = { 5, -6, 41, 19, -875 };
    constexpr auto value_count { nlc::meta::array_len<decltype(input_values)> };

    nlc::standard_allocator allocator;
    nlc::xoroshiro128plus rng { seed };
    int shuffled_values[value_count];
    nlc::memcpy(reinterpret_cast<void *>(shuffled_values),
                reinterpret_cast<void const *>(input_values),
                value_count * sizeof(int));

    nlc::shuffle(rng, nlc::make_span(shuffled_values));

    // The result still contains all the input values
    nlc_check(is_permutation(allocator,
                             nlc::make_span(input_values),
                             nlc::span_cast<int const>(nlc::make_span(shuffled_values))));
    // The result is not the same as the input
    nlc_check(memcmp(reinterpret_cast<void const *>(input_values),
                     reinterpret_cast<void const *>(shuffled_values),
                     value_count * sizeof(int)));
}
