/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/count.hpp>
#include <nlc/algo/filter.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/test.hpp>

#include "dummy_container.hpp"

using namespace nlc;

static auto null_pred(int const) -> bool { return false; }
static auto is_positive(int const value) -> bool { return value >= 0; }
template<typename IndexType>
static auto contains(span<IndexType const> const values, IndexType const i) -> bool {
    for (auto const value : values)
        if (i == value)
            return true;
    return false;
}

nlc_test("empty input") {
    nlc::standard_allocator allocator;
    span<int const> empty_input;
    nlc_check(empty_input.is_empty());

    auto const res { filter(allocator, empty_input, null_pred) };
    nlc_check(res->is_empty());
}

template<typename IndexType> static auto check_index_type() {
    constexpr int input[] = { 1, 5, 6, -6 };
    nlc::standard_allocator allocator;

    auto const res {
        filter<int, decltype(is_positive), IndexType>(allocator, make_span(input), is_positive)
    };
    nlc_check_msg(res->size() == 3u, "got ", res->size(), " filtered elements, expected ", 3u);
    nlc_check(contains<IndexType>(*res, 0u));
    nlc_check(contains<IndexType>(*res, 1u));
    nlc_check(contains<IndexType>(*res, 2u));

    for (auto const idx : *res) {
        auto const & value { input[idx] };
        nlc_check_msg(is_positive(value),
                      "filtered value ",
                      value,
                      " at index ",
                      idx,
                      " does not verify predicate");
    }
}

nlc_test("custom index type") {
    check_index_type<u8>();
    check_index_type<i8>();
    check_index_type<u16>();
    check_index_type<i16>();
    check_index_type<u32>();
    check_index_type<i32>();
    check_index_type<u64>();
    check_index_type<i64>();
    check_index_type<usize>();
    check_index_type<isize>();
}
