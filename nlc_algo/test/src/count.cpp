/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/count.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/test.hpp>

#include "dummy_container.hpp"

static constexpr int int_values_array[] = { -5, 4578, -236, 0, 0, 7 };
static auto const int_values = nlc::make_span(int_values_array);

static constexpr float float_values_array[] = {
    0.0001f, 0.001f, -0.f, 0.f, 4E6f, -5.123E-12f, 0.0001f,
};
static auto const float_values = nlc::make_span(float_values_array);

template<typename T> [[maybe_unused]] static constexpr auto true_pred(T const &) -> bool {
    return true;
}
template<typename T> [[maybe_unused]] static constexpr auto false_pred(T const &) -> bool {
    return false;
}
template<typename T> [[maybe_unused]] static constexpr auto is_negative(T const & v) -> bool {
    return v <= static_cast<T>(0);
}
[[maybe_unused]] static constexpr auto is_big(float v) -> bool { return v > 100.f; }

nlc_test("count") {
    nlc_check(nlc::count<int const>(int_values, -236) == 1u);
    nlc_check(nlc::count<int const>(int_values, 0) == 2u);
    nlc_check(nlc::count<int const>(int_values, 42) == 0u);
    nlc_check(nlc::count<float const>(float_values, 4E6f) == 1u);
    nlc_check(nlc::count<float const>(float_values, -13.37f) == 0u);
    nlc_check(nlc::count<float const>(float_values, 0.0001f) == 2u);

    auto container = make_container();
    nlc_check(nlc::count(container.begin(), container.end(), DummyPair<int, float> { -3, 0.3f }) == 1u);
}

nlc_test("count_if") {
    [[maybe_unused]] auto const is_even = [](int v) { return v % 2 == 0; };

    nlc_check(nlc::count_if<int const>(int_values, is_even) == 4u);
    nlc_check(nlc::count_if<int const>(int_values, true_pred<int>) == int_values.size());
    nlc_check(nlc::count_if<int const>(int_values, false_pred<int>) == 0u);
    nlc_check(nlc::count_if<int const>(int_values, is_negative<int>) == 4u);
    nlc_check(nlc::count_if<float const>(float_values, true_pred<float>) == float_values.size());
    nlc_check(nlc::count_if<float const>(float_values, false_pred<float>) == 0u);
    nlc_check(nlc::count_if<float const>(float_values, is_big) == 1u);

    auto container = make_container();
    nlc_check(nlc::count_if(container.begin(), container.end(), [](auto const & pair) {
                  return pair.first > -5;
              }) == 5u);
}
