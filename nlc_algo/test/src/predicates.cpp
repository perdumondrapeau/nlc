/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/predicates.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/test.hpp>

template<typename T> void test_less_greater(nlc::span<T const> values) {
    using namespace nlc;

    for (auto i = 1u; i < values.size(); ++i) {
        auto const a = values[i - 1u];
        auto const b = values[i];

        nlc_check_msg((less<T>(a, b) == !greater<T>(a, b)) || (a == b), a, b);
        nlc_check_msg(less<T>(a, b) == (a < b), a, b);
        nlc_check_msg(greater<T>(a, b) == (a > b), a, b);
    }
}

nlc_test("int") {
    constexpr int values[] = { -5, 4578, -236, 0, 0, 7 };
    test_less_greater(nlc::make_span(values));
}

nlc_test("float") {
    constexpr float values[] = { 0.0001f, 0.001f, -0.f, 0.f, 4E6f, -5.123E-12f };
    test_less_greater(nlc::make_span(values));
}
