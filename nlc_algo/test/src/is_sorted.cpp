/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/is_sorted.hpp>
#include <nlc/algo/predicates.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/test.hpp>

nlc_test("is_sorted - empty input") {
    auto const false_pred = [](int const, int const) { return false; };
    constexpr nlc::span<int> null_span;

    nlc_check(nlc::is_sorted(null_span));
    nlc_check(nlc::is_sorted(null_span, nlc::greater));
    nlc_check(nlc::is_sorted(null_span, false_pred));

    nlc_check(nlc::is_sorted(null_span.begin(), null_span.end()));
    nlc_check(nlc::is_sorted(null_span.begin(), null_span.end(), nlc::greater));
    nlc_check(nlc::is_sorted(null_span.begin(), null_span.end(), false_pred));
}

nlc_test("is_sorted - span") {
    constexpr int values_with_doublons[] = { -7, -7, -5, -2, 0, 0, 5, 8, 78 };
    nlc_check(nlc::is_sorted(nlc::make_span(values_with_doublons)));
    nlc_check(nlc::is_sorted(nlc::make_span(values_with_doublons), nlc::less) == false);
    nlc_check(nlc::is_sorted(nlc::make_span(values_with_doublons), nlc::greater_eq) == false);

    constexpr int values_without_doublons[] = { -7, -5, -2, 0, 5, 8, 78 };
    nlc_check(nlc::is_sorted(nlc::make_span(values_without_doublons)));
    nlc_check(nlc::is_sorted(nlc::make_span(values_without_doublons), nlc::less));
    nlc_check(nlc::is_sorted(nlc::make_span(values_without_doublons), nlc::greater_eq) == false);
}

nlc_test("is sorted - iterator") {
    constexpr int values_with_doublons[] = { -7, -7, -5, -2, 0, 0, 5, 8, 78 };
    auto const span_with_doublons = nlc::make_span(values_with_doublons);
    nlc_check(nlc::is_sorted(span_with_doublons.begin(), span_with_doublons.end()));
    nlc_check(nlc::is_sorted(span_with_doublons.begin(), span_with_doublons.end(), nlc::less) == false);
    nlc_check(nlc::is_sorted(span_with_doublons.begin() + 1, span_with_doublons.begin() + 4, nlc::less));
    nlc_check(nlc::is_sorted(span_with_doublons.begin(), span_with_doublons.end(), nlc::greater_eq) ==
              false);

    constexpr int values_without_doublons[] = { -7, -5, -2, 0, 5, 8, 78 };
    auto const span_without_doublons = nlc::make_span(values_without_doublons);
    nlc_check(nlc::is_sorted(span_without_doublons.begin(), span_without_doublons.end()));
    nlc_check(nlc::is_sorted(span_without_doublons.begin(), span_without_doublons.end(), nlc::less));
    nlc_check(nlc::is_sorted(span_without_doublons.begin(),
                             span_without_doublons.end(),
                             nlc::greater_eq) == false);
}
