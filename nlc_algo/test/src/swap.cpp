/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/swap.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/test.hpp>

namespace {
struct Copyable final {
    static usize copy_construction_count;
    static usize copy_assignment_count;
    static usize self_copy_assignment_count;

    int value;

    static auto reset() -> void {
        copy_construction_count = 0u;
        copy_assignment_count = 0u;
        self_copy_assignment_count = 0u;
    }

    Copyable(int v)
        : value { v } {}

    Copyable(Copyable const & other)
        : value { other.value } {
        ++copy_construction_count;
    }

    auto operator=(Copyable const & other) -> Copyable & {
        ++copy_assignment_count;
        if (this != &other)
            value = other.value;
        else
            ++self_copy_assignment_count;
        return *this;
    }
};

struct Movable final {
    static usize copy_construction_count;
    static usize copy_assignment_count;
    static usize self_copy_assignment_count;
    static usize move_construction_count;
    static usize move_assignment_count;
    static usize self_move_assignment_count;

    int value;

    static auto reset() -> void {
        copy_construction_count = 0u;
        copy_assignment_count = 0u;
        self_copy_assignment_count = 0u;
        move_construction_count = 0u;
        move_assignment_count = 0u;
        self_move_assignment_count = 0u;
    }

    Movable(int v)
        : value { v } {}

    Movable(Movable const & other)
        : value { other.value } {
        ++copy_construction_count;
    }

    Movable(Movable && other)
        : value { other.value } {
        ++move_construction_count;
    }

    auto operator=(Movable const & other) -> Movable & {
        ++copy_assignment_count;
        if (this != &other)
            value = other.value;
        else
            ++self_copy_assignment_count;
        return *this;
    }

    auto operator=(Movable && other) -> Movable & {
        ++move_assignment_count;
        if (this != &other)
            value = other.value;
        else
            ++self_move_assignment_count;
        return *this;
    }
};

usize Copyable::copy_construction_count { 0u };
usize Copyable::copy_assignment_count { 0u };
usize Copyable::self_copy_assignment_count { 0u };
usize Movable::copy_construction_count { 0u };
usize Movable::copy_assignment_count { 0u };
usize Movable::self_copy_assignment_count { 0u };
usize Movable::move_construction_count { 0u };
usize Movable::move_assignment_count { 0u };
usize Movable::self_move_assignment_count { 0u };
}  // namespace

nlc_test("swap - basic") {
    int a = 5;
    int b = 7;
    nlc::swap(a, b);
    nlc_check_msg(a == 7, "expected 7, got ", a);
    nlc_check_msg(b == 5, "expected 7, got ", b);

    int c = 42;
    nlc::swap(c, c);
    nlc_check_msg(a == 7, "swap with self failed: expected 42, got ", c);
}

nlc_test("swap - copyable") {
    Copyable a { -3 };
    Copyable b { 5 };
    nlc::swap(a, b);
    nlc_check_msg(a.value == 5, "expected 5, got ", a.value);
    nlc_check_msg(b.value == -3, "expected -3, got ", b.value);
    nlc_check_msg(Copyable::copy_construction_count == 1u,
                  "expected 1 copy constructions, got ",
                  Copyable::copy_construction_count);
    nlc_check_msg(Copyable::copy_assignment_count == 2u,
                  "expected 2 copy assignments, got ",
                  Copyable::copy_assignment_count);
    nlc_check_msg(Copyable::self_copy_assignment_count == 0u,
                  "expected 0 self copy assignments, got ",
                  Copyable::self_copy_assignment_count);

    Copyable::reset();

    Copyable c { 6 };
    nlc::swap(c, c);
    nlc_check_msg(c.value == 6, "expected 6, got ", c.value);
    nlc_check_msg(Copyable::copy_construction_count == 1u,
                  "expected 1 copy constructions, got ",
                  Copyable::copy_construction_count);
    nlc_check_msg(Copyable::copy_assignment_count == 2u,
                  "expected 2 copy assignments, got ",
                  Copyable::copy_assignment_count);
    nlc_check_msg(Copyable::self_copy_assignment_count == 1u,
                  "expected 1 self copy assignments, got ",
                  Copyable::self_copy_assignment_count);

    Copyable::reset();
}

nlc_test("swap - movable") {
    Movable a { -3 };
    Movable b { 5 };
    nlc::swap(a, b);
    nlc_check_msg(a.value == 5, "expected 5, got ", a.value);
    nlc_check_msg(b.value == -3, "expected -3, got ", b.value);
    nlc_check_msg(Movable::copy_construction_count == 0u,
                  "expected 0 copy constructions, got ",
                  Movable::copy_construction_count);
    nlc_check_msg(Movable::copy_assignment_count == 0u,
                  "expected 0 copy assignments, got ",
                  Movable::copy_assignment_count);
    nlc_check_msg(Movable::self_copy_assignment_count == 0u,
                  "expected 0 self copy assignments, got ",
                  Movable::self_copy_assignment_count);
    nlc_check_msg(Movable::move_construction_count == 1u,
                  "expected 1 move constructions, got ",
                  Movable::move_construction_count);
    nlc_check_msg(Movable::move_assignment_count == 2u,
                  "expected 2 move assignments, got ",
                  Movable::move_assignment_count);
    nlc_check_msg(Movable::self_move_assignment_count == 0u,
                  "expected 0 self move assignments, got ",
                  Movable::self_move_assignment_count);

    Movable::reset();

    Movable c { 6 };
    nlc::swap(c, c);
    nlc_check_msg(c.value == 6, "expected 6, got ", c.value);
    nlc_check_msg(Movable::copy_construction_count == 0u,
                  "expected 0 copy constructions, got ",
                  Movable::copy_construction_count);
    nlc_check_msg(Movable::copy_assignment_count == 0u,
                  "expected 0 copy assignments, got ",
                  Movable::copy_assignment_count);
    nlc_check_msg(Movable::self_copy_assignment_count == 0u,
                  "expected 0 self copy assignments, got ",
                  Movable::self_copy_assignment_count);
    nlc_check_msg(Movable::move_construction_count == 1u,
                  "expected 1 move constructions, got ",
                  Movable::move_construction_count);
    nlc_check_msg(Movable::move_assignment_count == 2u,
                  "expected 2 move assignments, got ",
                  Movable::move_assignment_count);
    nlc_check_msg(Movable::self_move_assignment_count == 1u,
                  "expected 1 self move assignments, got ",
                  Movable::self_move_assignment_count);

    Movable::reset();
}

nlc_test("swap - iterators") {
    int some_values[] = { -8, 0, 2, -4 };
    auto const span { nlc::make_span(some_values) };

    nlc::iter_swap(span.begin(), span.begin() + 2);

    nlc_check_msg(some_values[0] == 2, "expected 2, got ", some_values[0]);
    nlc_check_msg(some_values[2] == -8, "expected -8, got ", some_values[2]);
}
