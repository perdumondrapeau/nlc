/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/is_sorted.hpp>
#include <nlc/algo/predicates.hpp>
#include <nlc/algo/sort.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/test.hpp>

#include "non_allocator.hpp"
#include "sort_utils.hpp"

nlc_test("sort - standard") {
    int values[] = { 12, 45, -2, 8, -56, 1753 };
    auto span = nlc::make_span(values);
    nlc::sort(span);
    nlc_check(nlc::is_sorted(span));
}

nlc_test("sort - reverse") {
    int values[] = { 12, 45, -2, 8, -56, 1753 };
    auto span = nlc::make_span(values);
    auto const reverse_sort_pred = [](auto lhs, auto rhs) { return lhs > rhs; };
    nlc::sort<true>(span, reverse_sort_pred);
    nlc_check(nlc::is_sorted(span, reverse_sort_pred));
}

nlc_test("sort - empty_input") { nlc::sort(nlc::span<int>(nullptr, nullptr)); }

nlc_test("projective sort - standard") {
    constexpr auto value_count { 6u };

    nlc::standard_allocator allocator;
    constexpr int values[value_count] = { 12, 45, -2, 8, -56, 1753 };
    int sorted_values[value_count];
    auto const sorted_indices {
        nlc::projective_sort(allocator, nlc::make_span(values), [](int const v) { return -v; })
    };

    for (auto i = 0u; i < value_count; ++i)
        sorted_values[i] = values[(*sorted_indices)[i]];

    nlc_check(nlc::is_sorted(nlc::make_span(sorted_values), nlc::greater<int>));
}

nlc_test("projective sort - structured element") {
    constexpr auto value_count { 6u };

    struct Element final {
        float weight;
        byte payload[64u] = {};
    };

    nlc::standard_allocator allocator;
    constexpr Element values[value_count] = { { 1.2f }, { 45.f },  { -2.f },
                                              { 0.8f }, { -56.f }, { 1.753f } };
    Element sorted_values[value_count];

    auto const sorted_indices { nlc::projective_sort(allocator,
                                                     nlc::make_span(values),
                                                     [](Element const & e) { return e.weight; }) };

    for (auto i = 0u; i < value_count; ++i)
        sorted_values[i] = values[(*sorted_indices)[i]];

    nlc_check(nlc::is_sorted(nlc::make_span(sorted_values), [](Element const & e, Element const & f) {
        return e.weight < f.weight;
    }));
}

nlc_test("projective sort - empty input") {
    non_allocator allocator;
    auto const sorted_indices { nlc::projective_sort(allocator,
                                                     nlc::span<int const>(nullptr, nullptr),
                                                     [](int const v) { return -v; }) };
    nlc_check(sorted_indices->is_empty());
}

nlc_test("sort - sort_three") {
    int values[] = { 12, 45, -2, 8, -56, 1753 };
    auto span = nlc::make_span(values);

    for (auto i = 0u; i < span.size() - 2u; ++i) {
        auto & v1 = values[i];
        auto & v2 = values[i + 1u];
        auto & v3 = values[i + 2u];
        nlc::sort_three(v1, v2, v3);
    }
}
