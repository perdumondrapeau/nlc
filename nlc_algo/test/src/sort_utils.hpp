/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/algo/predicates.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/memory.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/allocator.hpp>

template<typename T, typename Pred>
auto is_permutation(nlc::allocator & allocator,
                    nlc::span<T> const rhs,
                    nlc::span<T> const lhs,
                    Pred const predicate) -> bool;

template<typename T>
inline auto is_permutation(nlc::allocator & allocator,
                           nlc::span<T> const rhs,
                           nlc::span<T> const lhs) -> bool {
    return is_permutation(allocator, rhs, lhs, nlc::eq<T>);
}

template<typename T, typename Pred>
auto is_permutation(nlc::allocator & allocator,
                    nlc::span<T> const rhs,
                    nlc::span<T> const lhs,
                    Pred const predicate) -> bool {
    nlc_assert(rhs.size() == lhs.size());
    auto const count = rhs.size();

    auto const was_visited = allocator.allocate<bool>(count);
    defer { allocator.free(was_visited); };
    nlc_assert(was_visited.ptr() != nullptr);

    memset(was_visited, 0);

    for (auto const & value : rhs) {
        bool found = false;
        for (auto i = 0u; i < count; ++i) {
            if (predicate(lhs[i], value) && !was_visited[i]) {
                was_visited[i] = true;
                found = true;
                break;
            }
        }

        if (!found) {
            return false;
        }
    }

    return true;
}
