#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

template<typename T, typename U> struct DummyPair final {
    T first;
    U second;

    constexpr DummyPair(T const & _first, U const & _second)
        : first { _first }
        , second { _second } {}

    auto operator==(DummyPair const & rhs) const -> bool {
        return first == rhs.first && second == rhs.second;
    }
};

struct DummyContainer final {
    using ValueType = DummyPair<int, float>;
    static constexpr usize size = 8u;

    ValueType values[size] = { ValueType { 0, 0.f }, ValueType { 0, 0.f }, ValueType { 0, 0.f },
                               ValueType { 0, 0.f }, ValueType { 0, 0.f }, ValueType { 0, 0.f },
                               ValueType { 0, 0.f }, ValueType { 0, 0.f } };

    class Iterator final {
      public:
        Iterator(DummyContainer & _container, usize _index)
            : container { &_container }
            , index { _index } {}

        bool operator!=(Iterator const & rhs) const {
            return container != rhs.container || index != rhs.index;
        }

        auto operator++() {
            ++index;
            return *this;
        }

        auto operator++(int) {
            auto const it = *this;
            operator++();
            return it;
        }

        ValueType & operator*() const { return container->values[index]; }

        auto operator-(Iterator const & rhs) const { return index - rhs.index; }

      private:
        DummyContainer * container;
        usize index;
    };

    ValueType const & operator[](usize idx) { return values[idx]; }
    Iterator begin() { return { *this, 0u }; }
    Iterator end() { return { *this, size }; }
};

inline auto make_container() -> DummyContainer {
    DummyContainer container;

    for (auto i = 0u; i < container.size; ++i)
        container.values[i] = { -static_cast<int>(i), static_cast<float>(i) * 0.1f };

    return container;
}
