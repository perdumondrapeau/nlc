/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/find.hpp>
#include <nlc/algo/is_sorted.hpp>
#include <nlc/algo/most_similar_strings.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/string_manip.hpp>
#include <nlc/test.hpp>

#include "non_allocator.hpp"

static auto check_no_duplicate(nlc::span<nlc::string_view const> const values) -> void {
    for (auto i = 0u; i < values.size(); ++i) {
        for (auto j = i + 1u; j < values.size(); ++j) {
            nlc_check_msg(values[i] != values[j], "values at ", i, " and ", j, " are the same");
        }
    }
}

static auto check_result(nlc::allocator & allocator,
                         nlc::string_view const ref,
                         nlc::span<nlc::string_view const> const entries,
                         nlc::span<nlc::string_view const> const results) -> void {
    nlc_check_msg(results.size() <= entries.size(),
                  entries.size(),
                  " entries and ",
                  results.size(),
                  " results");
    check_no_duplicate(entries);
    check_no_duplicate(results);

    for (auto i = 0u; i < results.size(); ++i) {
        auto const * const entry = find<nlc::string_view const>(entries, results[i]);
        nlc_check_msg(entry != nullptr, "result contains '", results[i], "' at ", i, " but it's not an entry");
    }

    nlc_check(nlc::is_sorted(results, [&allocator, ref](auto const lhs, auto const rhs) {
        return nlc::levenshtein_distance(allocator, ref, lhs) <=
               nlc::levenshtein_distance(allocator, ref, rhs);
    }));
}

nlc_test("most_similar_strings - invalid max string count") {
    [[maybe_unused]] constexpr nlc::string_view entries[] = { "foo", "bar", "baz" };
    nlc::standard_allocator allocator;

    nlc_expect_assert(nlc::find_most_similar_strings(allocator,
                                                     "",
                                                     nlc::make_span(entries),
                                                     { .max_result_count = 4u }));
}

nlc_test("most_similar_strings") {
    constexpr nlc::string_view entries[] = { "foo", "bar", "baz" };
    constexpr nlc::string_view ref { "bat" };
    nlc::standard_allocator allocator;

    auto const res { nlc::find_most_similar_strings(allocator, ref, nlc::make_span(entries)) };
    check_result(allocator, ref, nlc::make_span(entries), *res);
    nlc_check_msg((*res)[0] == "bar", "expected 'bar', got, '", (*res)[0], "'");  // Same distance
                                                                                  // as 'baz' but
                                                                                  // closer in the
                                                                                  // entry list
    nlc_check_msg((*res)[1] == "baz", "expected 'baz', got, '", (*res)[1], "'");
    nlc_check_msg((*res)[2] == "foo", "expected 'foo', got, '", (*res)[2], "'");
}

nlc_test("most_similar_strings - empty_input") {
    constexpr nlc::string_view ref { "bat" };
    non_allocator allocator;

    auto const res { nlc::find_most_similar_strings(allocator, ref, { nullptr, nullptr }) };
    nlc_check(res->is_empty());
}

nlc_test("most_similar_strings - result size") {
    constexpr nlc::string_view entries[] = { "foo", "bar", "baz" };
    constexpr nlc::string_view ref { "boo" };
    constexpr auto result_count { 1u };
    nlc::standard_allocator allocator;

    auto const res { nlc::find_most_similar_strings(allocator,
                                                    ref,
                                                    nlc::make_span(entries),
                                                    { .max_result_count = result_count }) };
    check_result(allocator, ref, nlc::make_span(entries), *res);
    nlc_check_msg(res->size() == result_count, "expected ", result_count, ", got ", res->size());
    nlc_check_msg((*res)[0] == "foo", "expected 'foo', got, '", (*res)[0], "'");
}

nlc_test("most_similar_strings - max distance") {
    constexpr nlc::string_view entries[] = { "foo", "bar", "baz" };
    constexpr auto max_distance { 2u };
    nlc::standard_allocator allocator;

    // Some results found
    {
        constexpr nlc::string_view ref { "bat" };
        auto const res { nlc::find_most_similar_strings(allocator,
                                                        ref,
                                                        nlc::make_span(entries),
                                                        { .max_tolerated_distance = max_distance }) };
        check_result(allocator, ref, nlc::make_span(entries), *res);
        nlc_check_msg(res->size() == 2u, "expected 2, got ", res->size());

        for (auto const str : *res) {
            nlc_check_msg(nlc::levenshtein_distance(allocator, ref, str) <= max_distance,
                          "expected less or equal to ",
                          max_distance,
                          ", got ",
                          nlc::levenshtein_distance(allocator, ref, str));
        }
    }

    // No results
    {
        constexpr nlc::string_view ref { "foobar" };
        auto const res { nlc::find_most_similar_strings(allocator,
                                                        ref,
                                                        nlc::make_span(entries),
                                                        { .max_tolerated_distance = max_distance }) };
        check_result(allocator, ref, nlc::make_span(entries), *res);
        nlc_check_msg(res->is_empty(), "expected empty result, but it has ", res->size(), " element(s)");
    }
}
