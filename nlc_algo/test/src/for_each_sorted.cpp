/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/for_each_sorted.hpp>
#include <nlc/algo/is_sorted.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/pair.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/test.hpp>

#include "sort_utils.hpp"

using namespace nlc;

constexpr pair<int, int> pairs[] = { { 0, 1 }, { 0, -2 }, { -2, 4 }, { 5, 6 }, { 5, 12 }, { 5, 0 } };
constexpr pair<int, int> sorted_pairs[] = {
    { -2, 4 }, { 0, -2 }, { 0, 1 }, { 5, 0 }, { 5, 6 }, { 5, 12 },
};

template<typename T1, typename T2>
inline auto are_pairs_equal(pair<T1, T2> const & rhs, pair<T1, T2> const & lhs) -> bool {
    return rhs.first == lhs.first && rhs.second == lhs.second;
}

template<typename T1, typename T2>
inline auto is_lexically_ordered(pair<T1, T2> const & rhs, pair<T1, T2> const & lhs) -> bool {
    return rhs.first != lhs.first ? less(rhs.first, lhs.first) : less(rhs.second, lhs.second);
}

template<typename Key, typename Value, usize Size> class DummyMap final {
  public:
    constexpr DummyMap(pair<Key, Value> const (&_values)[Size])
        : values { _values } {}

    using Iterator = pair<Key, Value> const *;

    auto ptr() const -> Iterator { return values; }
    auto begin() const -> Iterator { return ptr(); }
    auto end() const -> Iterator { return ptr() + Size; }
    static constexpr auto size() -> usize { return Size; }

  private:
    using Array = pair<Key, Value> const (&)[Size];
    Array & values;
};

template<typename Key, typename Value, usize Size>
DummyMap(pair<Key, Value> const (&)[Size]) -> DummyMap<Key, Value, Size>;

constexpr auto map = DummyMap { pairs };
using Iter = decltype(map)::Iterator;
using ValuePtr = pair<int, int> const *;

nlc_test("constants validation") {
    standard_allocator allocator;
    nlc_check(
        is_permutation(allocator, make_span(pairs), make_span(sorted_pairs), are_pairs_equal<int, int>));
    nlc_check(is_sorted(make_span(sorted_pairs), is_lexically_ordered<int, int>));
}

nlc_test("empty array") {
    standard_allocator allocator;
    span<float> const empty_span {};
    for_each_sorted(allocator, empty_span, [](auto const) { nlc_check(false); });
}

nlc_test("array with pre-allocated memory") {
    standard_allocator allocator;
    auto const buffer = allocator.allocate<ValuePtr>(map.size());
    defer { allocator.free(buffer); };

    usize i = 0u;
    for_each_sorted<false>(
        buffer,
        map.begin(),
        [&](auto const & p) { nlc_check(are_pairs_equal(p, sorted_pairs[i++])); },
        is_lexically_ordered<int, int>);
}

nlc_test("array without pre-allocated memory") {
    standard_allocator allocator;
    usize i { 0u };

    for_each_sorted<false>(
        allocator,
        map.begin(),
        map.end(),
        [&](auto const & p) { nlc_check(are_pairs_equal(p, sorted_pairs[i++])); },
        is_lexically_ordered<int, int>);
}

nlc_test("empty map") {
    standard_allocator allocator;
    using LocIter = DummyMap<int, int, 5u>::Iterator;

    for_each_sorted<false, decltype(is_lexically_ordered<int, int>), LocIter>(
        allocator,
        nullptr,
        nullptr,
        [&](auto const &) { nlc_assert(false); },
        is_lexically_ordered<int, int>);
}

nlc_test("map with pre-allocated memory") {
    standard_allocator allocator;
    auto const buffer = allocator.allocate<ValuePtr>(map.size());
    defer { allocator.free(buffer); };
    usize i = 0u;

    for_each_sorted<false>(
        buffer,
        map.begin(),
        [&](auto const & p) { nlc_check(are_pairs_equal(p, sorted_pairs[i++])); },
        is_lexically_ordered<int, int>);
}

nlc_test("map without pre-allocated memory") {
    standard_allocator allocator;
    usize i { 0u };

    for_each_sorted<false>(
        allocator,
        map.begin(),
        map.end(),
        [&](auto const & p) { nlc_check(are_pairs_equal(p, sorted_pairs[i++])); },
        is_lexically_ordered<int, int>);
}
