/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/find.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/traits.hpp>
#include <nlc/test.hpp>

#include "dummy_container.hpp"

static constexpr int int_values_array[] = { -5, 4578, -236, 0, 0, 7 };
static const auto int_values = nlc::make_span(int_values_array);

static constexpr float float_values_array[] = {
    0.0001f, 0.001f, -0.f, 0.f, 4E6f, -5.123E-12f, 0.0001f,
};
static const auto float_values = nlc::make_span(float_values_array);

template<typename T> static auto constexpr is_strictly_positive(T v) -> bool {
    return v > static_cast<T>(0);
}
template<typename T> static auto constexpr is_negative(T v) -> bool {
    return v <= static_cast<T>(0);
}

template<typename It> using value_type = nlc::meta::rm_ref<decltype(*nlc::declval<It>)>;

template<typename T>
void test_find(nlc::span<T> const values, T const not_contained_value, T const multiple_occurences_value) {
    auto const first_value = *values.begin();
    auto const simple_res = nlc::find<T>(values, first_value);
    nlc_check_msg(simple_res != nullptr && *simple_res == first_value,
                  "value was ",
                  simple_res != nullptr ? "" : "not ",
                  "found");

    nlc_check(nlc::find<T>(values, not_contained_value) == nullptr);

    auto const multiple_occurences_first_res = nlc::find<T>(values, multiple_occurences_value);
    nlc_check_msg(multiple_occurences_first_res != nullptr &&
                      *multiple_occurences_first_res == multiple_occurences_value,
                  "value was ",
                  multiple_occurences_first_res != nullptr ? "" : "not ",
                  "found");

    auto const multiple_occurences_second_res =
        nlc::find<T>(nlc::span(multiple_occurences_first_res + 1, values.end()),
                     multiple_occurences_value);
    nlc_check(multiple_occurences_second_res != nullptr);
    nlc_check_msg(multiple_occurences_second_res != nullptr &&
                      *multiple_occurences_second_res == multiple_occurences_value,
                  "value was ",
                  multiple_occurences_second_res != nullptr ? "" : "not ",
                  "found");
}

template<typename It>
void test_find(It const begin,
               It const end,
               value_type<It> const not_contained_value,
               value_type<It> const multiple_occurences_value) {
    using T = value_type<It>;

    auto const first_value = *begin;
    auto const simple_res = nlc::find(begin, end, first_value);
    nlc_check_msg(simple_res != nullptr && *simple_res == first_value,
                  "value was ",
                  simple_res != nullptr ? "" : "not ",
                  "found");

    nlc_check(nlc::find(begin, end, not_contained_value) == nullptr);

    auto const multiple_occurences_first_res = nlc::find(begin, end, multiple_occurences_value);
    nlc_check_msg(multiple_occurences_first_res != nullptr &&
                      *multiple_occurences_first_res == multiple_occurences_value,
                  "value was ",
                  multiple_occurences_first_res != nullptr ? "" : "not ",
                  "found");

    auto const multiple_occurences_second_res =
        nlc::find<T>(nlc::span(multiple_occurences_first_res + 1, &(*end)), multiple_occurences_value);
    nlc_check_msg(multiple_occurences_second_res != nullptr &&
                      *multiple_occurences_second_res == multiple_occurences_value,
                  "value was ",
                  multiple_occurences_second_res != nullptr ? "" : "not ",
                  "found");
}

template<typename T, typename Pred>
void test_find_if(nlc::span<T> values, Pred predicate, usize expected_matching_count) {
    usize matching_count = 0u;

    do {
        auto const r = nlc::find_if(values, predicate);
        if (r == nullptr)
            break;

        nlc_check(r < values.end());

        ++matching_count;
        values = nlc::span(r + 1, values.end());
    } while (true);

    nlc_check_msg(matching_count == expected_matching_count, matching_count, expected_matching_count);
}

template<typename It, typename Pred>
void test_find_if(It const begin, It const end, Pred predicate, usize expected_matching_count) {
    usize matching_count = 0u;

    auto values = nlc::span(&(*begin), end - begin);
    do {
        auto const r = nlc::find_if(values.begin(), values.end(), predicate);
        if (r == nullptr)
            break;

        nlc_check(r < values.end());

        ++matching_count;
        values = nlc::span(r + 1, values.end());
    } while (true);

    nlc_check_msg(matching_count == expected_matching_count, matching_count, expected_matching_count);
}

nlc_test("find") {
    test_find<int const>(int_values, -2, 0);
    test_find<float const>(float_values, 4.2f, 0.0001f);

    auto container = make_container();
    container.values[0] = container.values[1];
    test_find(container.begin(),
              container.end(),
              DummyPair<int, float> { 5, 0.5f },
              DummyPair<int, float> { -1, 0.1f });
}

nlc_test("find_if") {
    test_find_if<int const>(int_values, is_strictly_positive<int const>, 2u);
    test_find_if<int const>(int_values, is_negative<int const>, int_values.size() - 2u);
    test_find_if<float const>(float_values, is_strictly_positive<float const>, 4u);
    test_find_if<float const>(float_values, is_negative<float const>, float_values.size() - 4u);

    auto container = make_container();
    container.values[0] = container.values[1];
    test_find_if(
        container.begin(),
        container.end(),
        [](auto const & pair) { return pair.second < 0.35f; },
        4u);
}
