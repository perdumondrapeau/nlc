/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#ifdef NLC_PROFILING_ENABLED
#    ifndef TRACY_ENABLE
#        error "TRACY_ENABLE should be defined when profiling is enabled"
#    endif
#    include <TracyC.h>

#    ifndef TRACY_HAS_CALLSTACK
#        error "we want to have callstacks"
#    endif

namespace nlc::impl {

constexpr auto random_color(uint64_t line) -> uint32_t {
    line ^= line >> 30;
    line *= 0xbf58476d1ce4e5b9;
    line ^= line >> 27;
    line *= 0x94d049bb133111eb;
    line ^= line >> 31;
    return static_cast<uint32_t>(line);
}

class profiling_scope final {
  public:
    profiling_scope(TracyCZoneCtx const & ctx_)
        : ctx { ctx_ } {}

    ~profiling_scope() { ___tracy_emit_zone_end(ctx); }

  private:
    TracyCZoneCtx const & ctx;
};

inline int callstack_depth = 0;

// Control the depth of callstacks to collect at each event
// 0 disables callstack collection
#    define nlc_pf_set_callstack_depth(val)   \
        do {                                  \
            nlc::impl::callstack_depth = val; \
        } while (false)

// Automatic zone for the duration of the scope
#    define nlc_pf_scope(ctx, name)                                                                   \
        static constexpr ___tracy_source_location_data const TracyConcat(                             \
            location_,                                                                                \
            __LINE__) { name, __func__, __FILE__, __LINE__, nlc::impl::random_color(__LINE__) };      \
        auto const ctx =                                                                              \
            nlc::impl::callstack_depth == 0                                                           \
                ? ___tracy_emit_zone_begin(&TracyConcat(location_, __LINE__), static_cast<int>(true)) \
                : ___tracy_emit_zone_begin_callstack(&TracyConcat(location_, __LINE__),               \
                                                     nlc::impl::callstack_depth,                      \
                                                     static_cast<int>(true));                         \
        nlc::impl::profiling_scope const TracyConcat(scope_, __LINE__) { ctx }

#    define nlc_pf_scope_auto(name) nlc_pf_scope(TracyConcat(ctx_, __LINE__), name)

// Manual start and stop of zone
#    define nlc_pf_start_zone(ctx, name)                                                              \
        static constexpr ___tracy_source_location_data const TracyConcat(                             \
            location_,                                                                                \
            __LINE__) { name, __func__, __FILE__, __LINE__, nlc::impl::random_color(__LINE__) };      \
        auto const ctx =                                                                              \
            nlc::impl::callstack_depth == 0                                                           \
                ? ___tracy_emit_zone_begin(&TracyConcat(location_, __LINE__), static_cast<int>(true)) \
                : ___tracy_emit_zone_begin_callstack(&TracyConcat(location_, __LINE__),               \
                                                     nlc::impl::callstack_depth,                      \
                                                     static_cast<int>(true))

#    define nlc_pf_end_zone(ctx) ___tracy_emit_zone_end(ctx)

// Dynamic naming of zone
#    define nlc_pf_name_zone(ctx, stringview) \
        ___tracy_emit_zone_name(ctx, stringview.ptr(), stringview.size().v)

// User data
#    define nlc_pf_add_text(ctx, stringview) \
        ___tracy_emit_zone_text(ctx, stringview.ptr(), stringview.size().v)

#    define nlc_pf_add_value(ctx, val) ___tracy_emit_zone_value(ctx, static_cast<uint64_t>(val))

#    define nlc_pf_plot(name, val) ___tracy_emit_plot(name, static_cast<double>(val))

// Frame instrumentation
#    define nlc_pf_frame_mark() ___tracy_emit_frame_mark(nullptr)

}  // namespace nlc::impl

#else
#    define nlc_pf_set_callstack_depth(val)
#    define nlc_pf_scope(ctx, name)
#    define nlc_pf_scope_auto(name)
#    define nlc_pf_start_zone(ctx, name)
#    define nlc_pf_end_zone(ctx)
#    define nlc_pf_name_zone(ctx, stringview)
#    define nlc_pf_add_text(ctx, stringview)
#    define nlc_pf_add_value(ctx, val)
#    define nlc_pf_plot(name, val)
#    define nlc_pf_frame_mark()
#endif
