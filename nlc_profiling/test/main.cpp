/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "profiling.hpp"

int main() {
    nlc_pf_frame_mark();
    nlc_pf_scope(ctx, "coucou ici");
    nlc_pf_add_value(ctx, 123);
    nlc_pf_scope_auto("coucou ici auto");
    nlc_pf_plot("variable", 0.5);
    nlc_pf_start_zone(ctx2, "test");
    nlc_pf_end_zone(ctx2);
    return 0;
}
