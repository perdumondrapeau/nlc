# nlc profiling library

This library offers an API, mostly consisting of preprocessor macros, to instrument client code with calls to a profiling library.

For now it is quite directly wrapping [tracy](https://github.com/wolfpld/tracy).
We use the C bindings instead of the C++ API to avoid including C++'s standard headers that are use by tracy.
Another option to consider would be to hide the usage of tracy inside the implementation only and offer a pure nlc interface. This could even allow using other profilers.

# Enabling profiling

By default, the library builds almost nothing and all macros are without effect.
Superprojects can override the meson option `profiling` to enable it (see `meson_options.txt`).

```console
# configure with profiling
`meson build -Dprofiling=enabled`

# enable profiling afterwards
`meson build --reconfigure -Dprofiling=enabled`
```
