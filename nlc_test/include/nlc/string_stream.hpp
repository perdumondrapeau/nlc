/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <stdint.h>

namespace nlc::test {

class string_stream final {
  public:
    string_stream();
    ~string_stream();

  public:
    auto reserve(unsigned int const) -> void;
    auto append(char const) -> void;
    auto append(bool const) -> void;
    auto append(int8_t const) -> void;
    auto append(int16_t const) -> void;
    auto append(int32_t const) -> void;
    auto append(int64_t const) -> void;
    auto append(uint8_t const) -> void;
    auto append(uint16_t const) -> void;
    auto append(uint32_t const) -> void;
    auto append(uint64_t const) -> void;
    auto append(float const) -> void;
    auto append(double const) -> void;
    auto append(char * const) -> void;
    auto append(char const * const) -> void;
    auto append(char * const str, unsigned long long const len) -> void;
    auto append(char const * const str, unsigned long long const len) -> void;
    auto append(void const *) -> void;
    template<typename T> auto append(T const & other) -> void { format(*this, other); }
    template<typename T> auto append(T * ptr) -> void { append(static_cast<void const *>(ptr)); }

  public:
    auto c_str() const { return _data; }
    auto steal_c_str() {
        auto const str = _data;
        _data = nullptr;
        return str;
    }

  private:
    static inline constexpr unsigned int min_not_null_capacity { 8u };
    static inline constexpr unsigned int number_space_allocation { 32u };

    char * _data { nullptr };
    unsigned int _size { 0u };
    unsigned int _capacity { min_not_null_capacity };
};

template<typename... T> auto append(string_stream & ss, T &&... args) -> void {
    (ss.append(static_cast<T &&>(args)), ...);
}

template<unsigned int size>
auto append(nlc::test::string_stream & ss, char const (&str)[size]) -> void {
    ss.append(str, size - 1);
}

}  // namespace nlc::test
