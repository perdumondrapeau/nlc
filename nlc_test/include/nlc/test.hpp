/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#ifdef NLC_TEST

#    define NLC_TEST_CONCATENATE_IMPL(s1, s2) s1##s2
#    define NLC_TEST_CONCATENATE(s1, s2)      NLC_TEST_CONCATENATE_IMPL(s1, s2)
#    define NLC_TEST_STREAM_NAME()            internal_stream_##__FILE__##__LINE__

#    include "function_view.hpp"
#    include "string_stream.hpp"

// -------------------------------------------------------------------
// Testing API
// -------------------------------------------------------------------

// -------------------------------------------------------------------
#    define nlc_test(name)                                                           \
        static void NLC_TEST_CONCATENATE(NLC_TEST_FUNC_, __LINE__)();                \
        [[maybe_unused]] static auto NLC_TEST_CONCATENATE(NLC_TEST_, __LINE__) =     \
            ::nlc::test::registrator(name,                                           \
                                     __FILE__,                                       \
                                     __LINE__,                                       \
                                     NLC_TEST_CONCATENATE(NLC_TEST_FUNC_, __LINE__), \
                                     ::nlc::test::result_t::success);                \
        void NLC_TEST_CONCATENATE(NLC_TEST_FUNC_, __LINE__)()

// -------------------------------------------------------------------
#    define nlc_test_todo(name)                                                  \
        [[maybe_unused]] static auto NLC_TEST_CONCATENATE(NLC_TEST_, __LINE__) = \
            ::nlc::test::registrator(name, __FILE__, __LINE__, nullptr, ::nlc::test::result_t::todo)

// -------------------------------------------------------------------
#    define nlc_test_case(name) ::nlc::test::test_case { name, __FILE__, __LINE__ } + [&]()

// -------------------------------------------------------------------
#    define nlc_check(...)                                                   \
        do {                                                                 \
            if ((__VA_ARGS__) == false) {                                    \
                ::nlc::test::report_error(__FILE__, __LINE__, #__VA_ARGS__); \
                ::nlc::test::impl::trap_debugger();                          \
            }                                                                \
        } while (false)

// -------------------------------------------------------------------
#    define nlc_check_msg(cond, ...)                                             \
        do {                                                                     \
            if ((cond) == false) {                                               \
                ::nlc::test::string_stream NLC_TEST_STREAM_NAME();               \
                append(NLC_TEST_STREAM_NAME(), __VA_ARGS__);                     \
                ::nlc::test::report_error(__FILE__,                              \
                                          __LINE__,                              \
                                          #cond,                                 \
                                          NLC_TEST_STREAM_NAME().steal_c_str()); \
                ::nlc::test::impl::trap_debugger();                              \
            }                                                                    \
        } while (false)

// -------------------------------------------------------------------
#    ifdef NDEBUG
#        define nlc_expect_assert(...)
#        define nlc_expect_assert_msg(...)
#    else
#        define nlc_expect_assert(...)                                                           \
            do {                                                                                 \
                if (!::nlc::test::expect_assert_impl([&]() { static_cast<void>(__VA_ARGS__); })) \
                    ::nlc::test::report_missing_assert(__FILE__, __LINE__, #__VA_ARGS__);        \
            } while (false)

// -------------------------------------------------------------------

#        define nlc_expect_assert_msg(cond, ...)                                                   \
            do {                                                                                   \
                if (!::nlc::test::expect_assert_impl([&]() { static_cast<void>(__VA_ARGS__); })) { \
                    ::nlc::test::string_stream NLC_TEST_STREAM_NAME();                             \
                    append(NLC_TEST_STREAM_NAME(), __VA_ARGS__);                                   \
                    ::nlc::test::report_missing_assert(__FILE__,                                   \
                                                       __LINE__,                                   \
                                                       #cond,                                      \
                                                       NLC_TEST_STREAM_NAME().steal_c_str());      \
                }                                                                                  \
            } while (false)
#    endif

// -------------------------------------------------------------------
// Internal functions
// -------------------------------------------------------------------

namespace nlc::test {

namespace impl {
    auto trap_debugger() -> void;
}  // namespace impl

enum class result_t {
    success,
    failure,
    assert,
    crash,
    todo,
};

struct test_case final {
    char const * name;
    char const * file;
    int line;
};

auto operator+(test_case, function_view<void()>) -> void;

using test_func = void (*)();

auto expect_assert_impl(function_view<void()>) -> bool;

auto report_error(char const * file, int line, char const * test, char const * msg = nullptr) -> void;
auto report_missing_assert(char const * file, int line, char const * test, char const * msg = nullptr)
    -> void;

struct registrator final {
    registrator(char const * name, char const * file, int line, test_func, result_t expected_result);
};

auto has_argument(char const * arg) -> bool;

[[nodiscard]] auto timed_out() -> bool;

}  // namespace nlc::test

// -------------------------------------------------------------------
#endif
