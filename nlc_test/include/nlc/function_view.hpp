/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

namespace nlc::test {

template<class T> class function_view;

template<class Ret, class... Args> class function_view<Ret(Args...)> final {
    template<typename T> friend class function;

  public:
    using signature_type = Ret(Args...);

    function_view(function_view const &) = default;
    function_view & operator=(function_view const &) = default;

    template<class F>
    function_view(F const & f)
        : _ptr(reinterpret_cast<void const *>(&f))
        , _pf([](void const * const * ptr, Args... args) -> Ret {
            return (*reinterpret_cast<F const *>(const_cast<void *>(*ptr)))(nlc_fwd(args)...);
        }) {}

    auto operator()(Args... args) const -> Ret { return _pf(&_ptr, nlc_fwd(args)...); }

  private:
    function_view(void const * ptr, Ret (*pf)(void const * const *, Args...))
        : _ptr(ptr)
        , _pf(pf) {}

  private:
    void const * const _ptr;
    Ret (* const _pf)(void const * const *, Args...);
};

}  // namespace nlc::test
