/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "debugger.hpp"

#include <nlc/sanity/platforms.hpp>
#include <ctype.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

#if NLC_OS_WINDOWS
#    define WIN32_LEAN_AND_MEAN
#    define NOMINMAX
#    include <Windows.h>
#    include <crtdbg.h>

#    include <io.h>
#    define dup  _dup
#    define dup2 _dup2
#else
#    include <unistd.h>
#endif

namespace nlc::test {

auto is_debugger_present() -> bool {
#if NLC_OS_WINDOWS
    return IsDebuggerPresent();
#else
    static bool const debugger_present = [] {
        char buf[4096];

        auto const status_fd = ::open("/proc/self/status", O_RDONLY);
        if (status_fd == -1)
            return false;

        auto const num_read = ::read(status_fd, buf, sizeof(buf) - 1);
        ::close(status_fd);

        if (num_read <= 0)
            return false;

        buf[num_read] = '\0';
        constexpr char const tracer_pid_str[] = "TracerPid:";
        auto const tracer_pid_ptr = ::strstr(buf, tracer_pid_str);
        if (!tracer_pid_ptr)
            return false;

        for (auto const * c = tracer_pid_ptr + sizeof(tracer_pid_str) - 1; c <= buf + num_read; ++c) {
            if (isspace(*c))
                continue;

            return ::isdigit(*c) != 0 && *c != '\0';
        }

        return false;
    }();

    return debugger_present;
#endif
}

}  // namespace nlc::test
