/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "span.hpp"
#include "test.hpp"

namespace nlc::test {

struct test_case_data final {
    char const * name;
    int line;
    result_t result;
};

struct test_data final {
    char const * name;
    char const * file;
    int line;
    test_func func;
    result_t expected_result;
    span<test_case_data> test_cases;
    unsigned int registered_test_case_count;
};

auto register_test(char const * name, char const * file, int line, test_func f, result_t expected_result)
    -> void;
[[nodiscard]] auto register_test_case(char const * name, int line) -> bool;

[[nodiscard]] auto get_current_test() -> test_data &;
[[nodiscard]] auto get_current_test_case() -> test_case_data &;

[[nodiscard]] auto start_new_test() -> bool;
[[nodiscard]] auto start_new_test_case() -> bool;

[[nodiscard]] auto get_tests() -> span<test_data>;

auto free_test_memory() -> void;

}  // namespace nlc::test
