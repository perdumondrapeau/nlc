/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/debug/stacktrace.hpp>

namespace nlc::test {

namespace style {
    static auto constexpr reset = "\x1b[0m";
    static auto constexpr dim = "\x1b[0m\x1b[90m";  // reset then grey

    static auto constexpr file = "\x1b[0m\x1b[1m";  // reset then bold
    static auto constexpr location = reset;
    static auto constexpr failure = "\x1b[0m\x1b[31m";  // reset then red
    static auto constexpr success = "\x1b[0m\x1b[32m";  // reset then green
    static auto constexpr assert = failure;             // reset then red
    static auto constexpr crash = "\x1b[0m\x1b[41m";    // reset then red background
    static auto constexpr todo = "\x1b[0m\x1b[33m";     // reset then yellow
    static auto constexpr warning = "\x1b[0m\x1b[33m";  // reset then yellow
    static auto constexpr function = "\x1b[0m\x1b[34m";
    static auto constexpr should_have_asserted = "\x1b[0m\x1b[35m";
    static auto constexpr code = "\x1b[0m\x1b[33m";
    static auto constexpr dots = dim;
}  // namespace style

auto write_dots(unsigned int n) -> void;
auto print() -> void;
auto report_assert(debug::frame_ptr last_frame,
                   char const * file,
                   int line,
                   char const * test,
                   char const * msg) -> void;
auto report_crash(debug::frame_ptr last_frame) -> void;

}  // namespace nlc::test
