/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "output.hpp"

#include <nlc/sanity/platforms.hpp>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if NLC_OS_WINDOWS
#    define WIN32_LEAN_AND_MEAN
#    define NOMINMAX
#    include <Windows.h>
#    include <crtdbg.h>

#    include <io.h>
#    define dup  _dup
#    define dup2 _dup2
#else
#    include <unistd.h>
#endif

#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wdisabled-macro-expansion"
#endif

namespace nlc::test {

// -------------------------------------------------------------------------------

static int stdcout_saved = -1, stdcerr_saved = -1, dev_null = -1;

static bool is_verbose_flag_present = false;

auto init_outputs(int argc, char ** argv) -> void {
    for (auto i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "-v") == 0 || strcmp(argv[i], "--verbose") == 0) {
            is_verbose_flag_present = true;
            break;
        }
    }

#if NLC_OS_WINDOWS
    dev_null = open("nul", O_WRONLY);
#else
    dev_null = open("/dev/null", O_WRONLY);
#endif

    if (dev_null == -1) {
        fprintf(stderr, "could not open /dev/null\n");
        abort();
    }
}

auto deinit_outputs() -> void { close(dev_null); }

auto redirect_output_if_needed() -> void {
    auto impl = [&](auto const & output, auto & saved, auto const & id) {
        fflush(output);
        if (is_verbose_flag_present == false) {
            saved = dup(id);
            dup2(dev_null, id);
        }
    };

    impl(stdout, stdcout_saved, 1);
    impl(stderr, stdcerr_saved, 2);
}

auto restore_output_if_needed() -> void {
    auto impl = [&](auto const & output, auto & saved, auto const & id) {
        fflush(output);
        if (is_verbose_flag_present == false)
            dup2(saved, id);
    };

    impl(stdout, stdcout_saved, 1);
    impl(stderr, stdcerr_saved, 2);
}

auto is_verbose() -> bool { return is_verbose_flag_present; }

}  // namespace nlc::test

#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic pop
#endif
