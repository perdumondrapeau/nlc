/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "string_stream.hpp"

#include <assert.h>
#include <inttypes.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

namespace nlc::test {

string_stream::string_stream() {
    _data = reinterpret_cast<char *>(malloc(_capacity));
    _data[_size] = '\0';
    assert(strlen(_data) == _size);
}

string_stream::~string_stream() { free(_data); }

auto string_stream::reserve(unsigned int const size) -> void {
    auto const allocSize = size + 1u;
    if (_capacity < allocSize) {
        while (_capacity < allocSize) {
            _capacity *= 2u;
        }

        _data = reinterpret_cast<char *>(realloc(_data, _capacity));
    }
}

auto string_stream::append(char c) -> void {
    assert(strlen(_data) == _size);
    reserve(_size + static_cast<unsigned int>(sizeof(char)));
    _data[_size] = c;
    _size += static_cast<unsigned int>(sizeof(char));
    _data[_size] = '\0';
    assert(strlen(_data) == _size);
}

auto string_stream::append(bool const b) -> void { append(b ? "true" : "false"); }

#define IMPLEMENT_APPEND(TYPE, FMT)                                                             \
    auto string_stream::append(TYPE v) -> void {                                                \
        assert(strlen(_data) == _size);                                                         \
        reserve(_size + number_space_allocation);                                               \
        auto const ret = snprintf(_data + _size, number_space_allocation, FMT, v);              \
        assert(ret >= 0); /* Error occured ? */                                                 \
        auto const written_size = static_cast<unsigned int>(ret);                               \
        assert(written_size < number_space_allocation); /* Sufficient memory was allocated ? */ \
        _size += written_size;                                                                  \
        _data[_size] = '\0';                                                                    \
        assert(strlen(_data) == _size);                                                         \
    }

IMPLEMENT_APPEND(int8_t, "%" PRIi8)
IMPLEMENT_APPEND(int16_t, "%" PRIi16)
IMPLEMENT_APPEND(int32_t, "%" PRIi32)
IMPLEMENT_APPEND(int64_t, "%" PRIi64)
IMPLEMENT_APPEND(uint8_t, "%" PRIu8)
IMPLEMENT_APPEND(uint16_t, "%" PRIu16)
IMPLEMENT_APPEND(uint32_t, "%" PRIu32)
IMPLEMENT_APPEND(uint64_t, "%" PRIu64)
IMPLEMENT_APPEND(double, "%.9g")
IMPLEMENT_APPEND(void const *, "%p")

#undef IMPLEMENT_APPEND

auto string_stream::append(float v) -> void { append(static_cast<double>(v)); }

auto string_stream::append(char * const str) -> void { append(const_cast<char const *>(str)); }

auto string_stream::append(char const * const str) -> void {
    append(str, static_cast<unsigned int>(strlen(str)));
}

auto string_stream::append(char * const str, unsigned long long const len) -> void {
    append(const_cast<char const *>(str), len);
}

auto string_stream::append(char const * const str, unsigned long long const len) -> void {
    assert(strlen(_data) == _size);
    reserve(_size + static_cast<unsigned int>(len));
    memcpy(_data + _size, str, len);
    _size += static_cast<unsigned int>(len);
    _data[_size] = '\0';
    assert(strlen(_data) == _size);
}

}  // namespace nlc::test
