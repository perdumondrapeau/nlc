/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "time.hpp"

#include <nlc/sanity/platforms.hpp>

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if NLC_OS_WINDOWS
#    define WIN32_LEAN_AND_MEAN
#    define NOMINMAX
#    include <Windows.h>
#    include <crtdbg.h>
#else
#    include <time.h>
#    include <unistd.h>
#endif

#include "printing.hpp"
#include "test.hpp"  // for timed_out

#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wdisabled-macro-expansion"
#endif

namespace nlc::test {

static constexpr u64 ns_per_s = 1'000'000'000;
static constexpr u64 default_timeout_ms = 10'000;

#if NLC_OS_WINDOWS
struct state_t final {
    LARGE_INTEGER freq;
    LARGE_INTEGER start;
};
#endif

static auto init() {
#if NLC_OS_WINDOWS
    state_t s;
    QueryPerformanceFrequency(&s.freq);
    QueryPerformanceCounter(&s.start);
    return s;
#else
    timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return static_cast<u64>(ts.tv_sec) * ns_per_s + static_cast<u64>(ts.tv_nsec);
#endif
}

// Automatic initialization on static variable is useful.
// No need for the client to call an init function.
static auto state = init();

auto now() -> u64 {
#if NLC_OS_WINDOWS
    // https://docs.microsoft.com/en-us/windows/win32/sysinfo/acquiring-high-resolution-time-stamps#using-qpc-in-native-code
    LARGE_INTEGER counter;
    QueryPerformanceCounter(&counter);

    LARGE_INTEGER elapsed;
    elapsed.QuadPart = counter.QuadPart - state.start.QuadPart;
    elapsed.QuadPart *= ns_per_s;
    elapsed.QuadPart /= state.freq.QuadPart;

    return static_cast<u64>(elapsed.QuadPart);
#else
    timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    auto const t = static_cast<u64>(ts.tv_sec) * ns_per_s + static_cast<u64>(ts.tv_nsec);

    return t - state;
#endif
}

static auto timeout_ms = default_timeout_ms;
static auto start = now();

auto reset_timeout() -> void { start = now(); }

auto timed_out() -> bool { return now() - start >= timeout_ms * 1'000'000; }

auto set_timeout_duration(int argc, char ** argv) -> void {
    // Look for specified timeout duration via command line.
    for (auto i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "--no-timeout") == 0) {
            timeout_ms = 0xFFFFFFFF;
            return;
        }

        if (strcmp(argv[i], "--timeout-ms") != 0)
            continue;

        auto const s = argv[i + 1];
        char * endptr = nullptr;
        auto const ms = static_cast<unsigned int>(strtoul(s, &endptr, 10));

        if (endptr != s + strlen(s) || errno == ERANGE) {
            if (errno == ERANGE)
                errno = 0;
            fprintf(stderr,
                    "%swarning: invalid --timeout-ms parameter: `%s`, expected a positive number, "
                    "defaulting to %ums%s\n",
                    style::warning,
                    s,
                    static_cast<unsigned int>(timeout_ms),
                    style::reset);
        } else
            timeout_ms = ms;
    }
}

}  // namespace nlc::test

#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic pop
#endif
