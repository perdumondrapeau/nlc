/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "test.hpp"

#include <nlc/sanity/platforms.hpp>

#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include "nlc/debug/stacktrace.hpp"

#include "common.hpp"
#include "debugger.hpp"
#include "hooks.hpp"
#include "output.hpp"
#include "printing.hpp"
#include "storage.hpp"
#include "string_stream.hpp"
#include "time.hpp"

#if NLC_OS_WINDOWS
#    define WIN32_LEAN_AND_MEAN
#    define NOMINMAX
#    include <Windows.h>
#    include <crtdbg.h>
#endif

#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wdisabled-macro-expansion"
#endif

namespace nlc::test {

// -------------------------------------------------------------------------------
// Tests storage

registrator::registrator(char const * name, char const * file, int line, test_func f, result_t expected_result) {
    register_test(name, file, line, f, expected_result);
}

// Tests configuration and status

static bool an_expected_assert_was_triggered = false;
static bool wait_for_assert = false;
#if !NLC_OS_WINDOWS
static sigjmp_buf env_buffer;
static sigjmp_buf assert_env_buffer;
#endif

namespace impl {

    auto trap_debugger() -> void {
        if (is_debugger_present()) {
#if NLC_OS_WINDOWS
            __debugbreak();
#else
            // gdb will catch the exception but pass it on to the program afterwards (unlike windows
            // debugger), so we need to ignore the signal
            wait_for_assert = true;
            if (sigsetjmp(assert_env_buffer, 1) == 0)
                __builtin_trap();
            wait_for_assert = false;
#endif
        }
    }
}  // namespace impl

#if NLC_OS_WINDOWS
static auto nlc_sig_handler(int) -> void { RaiseException(0, 0, 0, nullptr); }
#else
[[noreturn]] static auto nlc_sig_handler(int) -> void {
    if (wait_for_assert) {
        siglongjmp(assert_env_buffer, 1);
    } else {
        report_crash(returnaddress());
        siglongjmp(env_buffer, 1);
    }
}
#endif

#if NLC_OS_WINDOWS
int const signals[] = { SIGABRT };
#else
int const signals[] = { SIGABRT, SIGFPE, SIGILL, SIGSEGV, SIGTRAP };
#endif

static auto set_signal_handlers() -> void {
    for (auto sig : signals) {
        if (signal(sig, &nlc_sig_handler) == SIG_ERR) {
            fprintf(stderr, "Failed to setup signal handler\n");
            exit(-1);
        }
    }
}

static auto reset_signal_handlers() -> void {
    for (auto sig : signals) {
        signal(sig, SIG_DFL);
    }
}

#ifndef NDEBUG
auto expect_assert_impl(function_view<void()> func) -> bool {
    wait_for_assert = true;
    an_expected_assert_was_triggered = false;
#    if NLC_OS_WINDOWS
#        if NLC_COMPILER_CLANG_CL
#            pragma GCC diagnostic push
#            pragma GCC diagnostic ignored "-Wlanguage-extension-token"
#        endif
    __try {
#        if NLC_COMPILER_CLANG_CL
#            pragma GCC diagnostic pop
#        endif
        func();
    } __except (EXCEPTION_EXECUTE_HANDLER) {}
#    else
    if (sigsetjmp(assert_env_buffer, 1) == 0) {
        func();
    }
#    endif

    wait_for_assert = false;

    return an_expected_assert_was_triggered;
}
#endif

// -------------------------------------------------------------------------------
// Test execution

static auto test_case_guard = 0;

auto operator+(test_case tag, function_view<void()> f) -> void {
    nlc_assert(test_case_guard == 0, tag.file, tag.line, "Nested test cases is forbidden");
    ++test_case_guard;
    if (register_test_case(tag.name, tag.line))
        f();
    --test_case_guard;
}

static auto run_test_case(const test_data & test) -> result_t {
    redirect_output_if_needed();

    assert_hook = [](debug::frame_ptr last_frame,
                     char const * condition,
                     char const * file,
                     char const *,
                     int line,
                     char const * data) {
        if (wait_for_assert) {
            an_expected_assert_was_triggered = true;
#if NLC_OS_WINDOWS
            RaiseException(0, 0, 0, nullptr);
#else
            siglongjmp(assert_env_buffer, 1);
#endif
        } else if (is_debugger_present())
            impl::trap_debugger();

        if (data == nullptr)
            report_assert(last_frame, file, line, condition, nullptr);
        else {
            string_stream ss;
            ss.append(data);
            report_assert(last_frame, file, line, condition, ss.steal_c_str());
        }

        get_current_test_case().result = result_t::assert;
#if NLC_OS_WINDOWS
        RaiseException(0, 0, 0, nullptr);
#else
        siglongjmp(env_buffer, 1);
#endif
    };

    get_current_test_case().result = result_t::success;

#if NLC_OS_WINDOWS
#    if NLC_COMPILER_CLANG_CL
#        pragma GCC diagnostic push
#        pragma GCC diagnostic ignored "-Wlanguage-extension-token"
#    endif
    __try {
#    if NLC_COMPILER_CLANG_CL
#        pragma GCC diagnostic pop
#    endif
#else
    if (sigsetjmp(env_buffer, 1) == 0) {
#endif
        test_case_guard = 0;
        test.func();
#if NLC_OS_WINDOWS
    } __except (EXCEPTION_EXECUTE_HANDLER) {
#else
    } else {
#endif
        if (get_current_test_case().result != result_t::assert)
            get_current_test_case().result = result_t::crash;
    }

    assert_hook = nullptr;
    restore_output_if_needed();
    return get_current_test_case().result;
}

}  // namespace nlc::test

using namespace nlc::test;

auto main(int argc, char ** argv) -> int {
#if NLC_OS_WINDOWS
    _CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_FILE | _CRTDBG_MODE_DEBUG);
    _CrtSetReportFile(_CRT_WARN, _CRTDBG_FILE_STDERR);
    _CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_FILE | _CRTDBG_MODE_DEBUG);
    _CrtSetReportFile(_CRT_ASSERT, _CRTDBG_FILE_STDERR);
    _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_FILE | _CRTDBG_MODE_DEBUG);
    _CrtSetReportFile(_CRT_ERROR, _CRTDBG_FILE_STDERR);
#endif

    set_timeout_duration(argc, argv);
    init_outputs(argc, argv);

    set_signal_handlers();

    auto ret = true;

    while (start_new_test()) {
        auto const & test = get_current_test();

        reset_timeout();

        while (start_new_test_case()) {
            if (test.expected_result == result_t::todo) {
                get_current_test_case().result = result_t::todo;
            } else {
                ret &= run_test_case(test) == test.expected_result;
            }
        }
    }

    reset_signal_handlers();

    print();
    free_test_memory();

    deinit_outputs();

    return ret ? 0 : 1;
}

#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic pop
#endif
