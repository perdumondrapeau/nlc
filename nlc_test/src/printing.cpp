/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "printing.hpp"

#include <nlc/debug/stacktrace.hpp>
#include <nlc/sanity/platforms.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "output.hpp"
#include "storage.hpp"

#if NLC_OS_WINDOWS
#    define WIN32_LEAN_AND_MEAN
#    define NOMINMAX
#    include <io.h>
#else
#    include <unistd.h>
#endif

#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wdisabled-macro-expansion"
#endif

namespace nlc::test {

// -------------------------------------------------------------------------------

static constexpr auto max_error_count = 1024u;
static constexpr auto max_stacktrace_depth = 256u;

enum class error_t {
    check,
    assert,
    crash,
    missing_assert,
};

struct error_data final {
    test_data const * test;
    test_case_data const * test_case;
    char const * file;
    int line;
    char const * condition;
    char const * message;
    error_t type;
    debug::frame_ptr stacktrace[max_stacktrace_depth] = { 0 };
    debug::usize stacktrace_depth = 0;
};

static error_data errors[max_error_count];
static auto error_count = 0u;

static auto add_error(error_t error_type,
                      nlc::debug::frame_ptr last_frame,
                      char const * file,
                      int line,
                      char const * test_text,
                      char const * msg) -> void {
    if (get_current_test_case().result == result_t::success) {
        get_current_test_case().result = [&] {
            switch (error_type) {
                case error_t::assert: return result_t::assert;
                case error_t::crash: return result_t::crash;
                default: return result_t::failure;
            }
        }();
    }

    if (error_count == max_error_count) {
        return;  // TODO do something better please
    }
    auto & error = errors[error_count++];

    error = {
        &get_current_test(), &get_current_test_case(), file, line, test_text, msg, error_type,
    };

    error.stacktrace_depth =
        debug::capture_stacktrace(last_frame, error.stacktrace, max_stacktrace_depth);
}

auto report_error(char const * file, int line, char const * test_text, char const * msg) -> void {
    add_error(error_t::check, returnaddress(), file, line, test_text, msg);
}

auto report_assert(debug::frame_ptr last_frame,
                   char const * file,
                   int line,
                   char const * test_text,
                   char const * msg) -> void {
    add_error(error_t::assert, last_frame, file, line, test_text, msg);
}

auto report_missing_assert(char const * file, int line, char const * test_text, char const * msg)
    -> void {
    add_error(error_t::missing_assert, returnaddress(), file, line, test_text, msg);
}

auto report_crash(debug::frame_ptr last_frame) -> void {
    add_error(error_t::crash, last_frame, "", 0, "crash", nullptr);
}

static auto print_error_batch(span<error_data> error_batch) -> void {
    auto const & first = error_batch[0];
    switch (first.type) {
        case error_t::assert:
        case error_t::missing_assert:
            fprintf(stderr, "\n%sASSERT%s: in test ", style::failure, style::dim);
            break;
        case error_t::check:
            fprintf(stderr, "\n%sERROR%s: in test ", style::failure, style::dim);
            break;
        case error_t::crash:
            fprintf(stderr, "\n%sCRASH%s: in test ", style::crash, style::dim);
            break;
    }

    if (first.test->registered_test_case_count == 0) {
        fprintf(stderr, "%s%s%s", style::warning, first.test->name, style::dim);
    } else {
        fprintf(stderr,
                "case %s%s%s / %s%s",
                style::warning,
                first.test->name,
                style::dim,
                style::warning,
                first.test_case->name);
    }

    fprintf(stderr,
            "%s\n       at %s%s%s:%s%d",
            style::dim,
            style::reset,
            first.test->file,
            style::dim,
            style::reset,
            first.test->line);

    for (auto const & error : error_batch) {
        switch (error.type) {
            case error_t::assert: [[fallthrough]];
            case error_t::check:
                fprintf(stderr,
                        "\n\n     %s✖ %s%s(%s%s%s)%s triggered",
                        style::failure,
                        style::reset,
                        error.type == error_t::assert
                            ? (error.message == nullptr ? "nlc_assert" : "nlc_assert_msg")
                            : "nlc_check",
                        style::failure,
                        error.condition,
                        style::reset,
                        style::dim);

                if (error.message != nullptr)
                    fprintf(stderr, " with message:\n       %s%s", style::warning, error.message);

                fprintf(stderr,
                        "%s\n       at %s%s%s:%s%d",
                        style::dim,
                        style::location,
                        error.file,
                        style::dim,
                        style::reset,
                        error.line);
                free(const_cast<char *>(error.message));
                break;
            case error_t::crash: break;
            case error_t::missing_assert:
                fprintf(stderr,
                        "%s\n\n     ✖ %s%s%s%s should have triggered an assert",
                        style::failure,
                        style::should_have_asserted,
                        error.condition,
                        style::reset,
                        style::dim);

                if (error.message != nullptr)
                    fprintf(stderr, " with message:\n       %s%s", style::warning, error.message);

                fprintf(stderr,
                        "%s\n       at %s%s%s:%s%d",
                        style::dim,
                        style::location,
                        error.file,
                        style::dim,
                        style::reset,
                        error.line);
                free(const_cast<char *>(error.message));
        }

        fprintf(stderr, "\n\n       %sstacktrace:", style::dim);
        for (auto i = 0u; i < error.stacktrace_depth; ++i) {
            auto const frame = debug::decode_frame(error.stacktrace[i]);

            if (strlen(frame.symbol) == 0)
                continue;
            if (strstr(frame.filename, "/nlc_test/include/") != nullptr ||
                strstr(frame.filename, "/nlc_test/src/") != nullptr)
                continue;

            auto line = static_cast<unsigned int>(frame.line);
            auto column = static_cast<unsigned int>(frame.column);

            auto constexpr buffer_len = 1024;
            char buffer[buffer_len];

            // print the line
            if (debug::retrieve_line_of_code(frame, buffer, buffer_len))
                fprintf(stderr, "\n         %s%s", style::code, buffer);

            fprintf(stderr,
                    "\n           %sat %s%s%s:%s%u%s:%s%u%s in %s%s%s",
                    style::dim,
                    style::location,
                    frame.filename,
                    style::dim,
                    style::location,
                    line,
                    style::dim,
                    style::file,
                    column,
                    style::dim,
                    style::function,
                    frame.symbol,
                    style::reset);

            auto constexpr prefix = "NLC_TEST_FUNC_";
            if (strncmp(prefix, frame.symbol, strlen(prefix)) == 0)
                break;
        }
    }

    fprintf(stderr, "\n");

    error_count = 0u;
}

static auto are_equivalent(error_t a, error_t b) -> bool {
    if ((a == error_t::assert || a == error_t::missing_assert) &&
        (b == error_t::assert || b == error_t::missing_assert))
        return true;

    return a == b;
}

static auto print_errors() -> void {
    if (error_count == 0)
        return;
    auto it = &errors[0];
    auto end = it + error_count;
    auto first_in_test_case = it;

    fprintf(stderr, "\n\nError summary:");
    while (it != end) {
        if (it->test_case != first_in_test_case->test_case ||
            are_equivalent(it->type, first_in_test_case->type) == false) {
            print_error_batch(span<error_data> { first_in_test_case,
                                                 static_cast<unsigned int>(it - first_in_test_case) });
            first_in_test_case = it;
        }
        ++it;
    }

    print_error_batch(span<error_data> { first_in_test_case,
                                         static_cast<unsigned int>(it - first_in_test_case) });
}

// -------------------------------------------------------------------------------

auto write_dots(unsigned int n) -> void {
    static char constexpr dots[] = " ........................................................."
                                   ".........................................................."
                                   ".........................................................."
                                   "..........................................................";

    auto const bytes_to_write = sizeof(dots) < n ? sizeof(dots) : n;
    fprintf(stderr, style::dots);

#if NLC_OS_WINDOWS
    _write(2, dots, static_cast<unsigned int>(bytes_to_write));
#else
    [[maybe_unused]] auto const written = write(2, dots, bytes_to_write);
#endif
}

// -------------------------------------------------------------------------------

static auto compute_max_test_name_size() -> size_t {
    auto size = 0ull;
    for (auto const & test : get_tests()) {
        auto const s = strlen(test.name);
        if (size < s)
            size = s;
        auto const f = strlen(test.file);
        if (size < f)
            size = f;
    }
    return size;
}

static auto get_color(result_t result) -> char const * {
    switch (result) {
        case result_t::success: return style::success;
        case result_t::failure: return style::failure;
        case result_t::assert: return style::assert;
        case result_t::crash: return style::crash;
        case result_t::todo: return style::todo;
    }
    nlc_assert(false, __FILE__, __LINE__, "Internal error");
}

static auto get_text(result_t result) -> char const * {
    switch (result) {
        case result_t::success: return "OK";
        case result_t::failure: return "FAILURE";
        case result_t::assert: return "ASSERT";
        case result_t::crash: return "CRASHED";
        case result_t::todo: return "TODO";
    }
    nlc_assert(false, __FILE__, __LINE__, "Internal error");
}

// -------------------------------------------------------------------------------

/*static char const * last_printed_file = nullptr;*/
/*static test_data const * last_printed_test = nullptr;*/
/*static test_case_data const * last_printed_test_case = nullptr;*/

// -------------------------------------------------------------------------------

static auto print_test_case(test_case_data const & test_case, size_t size) -> void {
    fprintf(stderr, "\n     - %s", test_case.name);

    auto const name_len = strlen(test_case.name);
    write_dots(name_len + 8 >= size ? 4 : static_cast<unsigned int>(size - name_len - 4));

    fprintf(stderr, " %s%s%s", get_color(test_case.result), get_text(test_case.result), style::reset);
}

static auto print_test(test_data const & test, size_t size) -> void {
    fprintf(stderr, "\n - %s", test.name);

    if (test.registered_test_case_count == 0) {
        write_dots(static_cast<unsigned int>(size - strlen(test.name)));
        auto const result = test.test_cases[0].result;
        fprintf(stderr, " %s%s%s", get_color(result), get_text(result), style::reset);
    } else {
        auto success_count = 0u;
        auto failure_count = 0u;
        auto todo_count = 0u;

        for (auto const & test_case : test.test_cases) {
            switch (test_case.result) {
                case result_t::success: ++success_count; break;
                case result_t::todo: ++todo_count; break;
                default: ++failure_count; break;
            }
        }

        if (failure_count == 0 && is_verbose() == false) {
            write_dots(static_cast<unsigned int>(size - strlen(test.name)));
            fprintf(stderr,
                    " %s%s%s (%u)",
                    get_color(result_t::success),
                    get_text(result_t::success),
                    style::reset,
                    success_count);
            if (todo_count > 0u) {
                fprintf(stderr,
                        " %s%s%s (%u)",
                        get_color(result_t::todo),
                        get_text(result_t::todo),
                        style::reset,
                        todo_count);
            }
        } else {
            for (auto const & test_case : test.test_cases) {
                print_test_case(test_case, size);
            }
        }
    }
}

static auto print_test_file(span<test_data> tests, size_t size) -> void {
    if (tests.size() == 0)
        return;

    fprintf(stderr, "\n%s%s:%s", style::file, tests[0].file, style::reset);

    auto success_count = 0u;
    auto failure_count = 0u;
    auto todo_count = 0u;

    for (auto const & test : tests) {
        for (auto const & test_case : test.test_cases) {
            switch (test_case.result) {
                case result_t::success: ++success_count; break;
                case result_t::todo: ++todo_count; break;
                default: ++failure_count; break;
            }
        }
    }

    if (failure_count == 0 && is_verbose() == false) {
        write_dots(static_cast<unsigned int>(size - strlen(tests[0].file) + 2));
        fprintf(stderr,
                " %s%s%s (%u)",
                get_color(result_t::success),
                get_text(result_t::success),
                style::reset,
                success_count);
        if (todo_count > 0u) {
            fprintf(stderr,
                    " %s%s%s (%u)",
                    get_color(result_t::todo),
                    get_text(result_t::todo),
                    style::reset,
                    todo_count);
        }
    } else {
        for (auto const & test : tests)
            print_test(test, size);
    }
    // TODO
}

// -------------------------------------------------------------------------------

auto print() -> void {
    restore_output_if_needed();

    auto tests = get_tests();

    if (tests.size() == 0) {
        fprintf(stderr, "No test was run.\n");
        return;
    }

    static auto const size = []() {
        // Choosen to align with meson test printing
        auto constexpr min_test_line_for_short_named_tests = 49;

        auto constexpr min_dot_lenght_for_long_named_test = 4;

        auto const min_line_length = min_dot_lenght_for_long_named_test + compute_max_test_name_size();
        return min_line_length < min_test_line_for_short_named_tests
                   ? min_test_line_for_short_named_tests
                   : min_line_length;
    }();

    auto it = tests.begin();
    auto first_in_file = it;
    auto current_file = it->file;

    fprintf(stderr, "\nTest summary:");
    while (it != tests.end()) {
        if (strcmp(it->file, current_file) != 0) {
            print_test_file(span<test_data> { first_in_file,
                                              static_cast<unsigned int>(it - first_in_file) },
                            size);
            current_file = it->file;
            first_in_file = it;
        }
        ++it;
    }
    print_test_file(span<test_data> { first_in_file, static_cast<unsigned int>(it - first_in_file) },
                    size);

    print_errors();
    fprintf(stderr, "%s\n", style::reset);

    redirect_output_if_needed();
}

// -------------------------------------------------------------------------------

}  // namespace nlc::test

#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic pop
#endif
