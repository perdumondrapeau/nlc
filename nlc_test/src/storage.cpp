/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "storage.hpp"

#include "common.hpp"
#include "span.hpp"

namespace nlc::test {

// -------------------------------------------------------------------------------

static constexpr auto test_max_count = 1024u;
static constexpr auto test_case_max_count = 128u;
static test_data tests[test_max_count] = {};
static auto test_count = 0u;
static auto current_test_index = -1;  // negative value means their is no active test
static auto current_test_case_index = -1;

// -------------------------------------------------------------------------------

auto get_current_test() -> test_data & {
    nlc_assert(current_test_index >= 0, __FILE__, __LINE__, "Internal error: no test is active.");
    nlc_assert(current_test_index < static_cast<int>(test_count),
               __FILE__,
               __LINE__,
               "Internal error: out of bound test access.");
    return tests[current_test_index];
}

auto start_new_test() -> bool {
    current_test_index += 1;
    current_test_case_index = -1;
    return current_test_index < static_cast<int>(test_count);
}

auto register_test_case(char const * name, int line) -> bool {
    auto & test = get_current_test();

    // If this is the first nlc_test_case macro reached in this test, fill default test case
    // informations and run it
    if (test.registered_test_case_count == 0) {
        test.test_cases[0].name = name;
        test.test_cases[0].line = line;
        test.registered_test_case_count = 1;
        return true;
    }

    // Otherwise, check if the test_case is already known and run it if its index is current one
    for (auto index = 0u; index < test.test_cases.size(); ++index) {
        // This could leads to some disapointment in case of two test cases with the same name in
        // the same test on the same line
        if (test.test_cases[index].name == name && test.test_cases[index].line == line)
            return static_cast<int>(index) == current_test_case_index;
    }

    // If the test case is not already known, register it:
    test.registered_test_case_count += 1;
    nlc_assert(test.registered_test_case_count < test_case_max_count - 1,
               get_current_test().file,
               line,
               "Max test case count reached");

    auto const ptr = test.test_cases.begin();
    auto const index = test.test_cases.size();
    test.test_cases = span<test_case_data> { ptr, index + 1 };

    auto & test_case = test.test_cases[index];
    test_case.name = name;
    test_case.line = line;
    test_case.result = result_t::failure;

    return false;
}

auto start_new_test_case() -> bool {
    current_test_case_index += 1;
    return current_test_case_index < static_cast<int>(get_current_test().test_cases.size());
}

auto get_current_test_case() -> test_case_data & {
    nlc_assert(current_test_case_index >= 0, __FILE__, __LINE__, "Internal error");
    return get_current_test().test_cases[static_cast<unsigned int>(current_test_case_index)];
}

auto register_test(char const * name, char const * file, int line, test_func f, result_t expected_result)
    -> void {
    nlc_assert(test_count < test_max_count - 1, file, line, "Max test count reached");
    auto & new_test = tests[test_count];
    new_test = test_data { name,
                           file,
                           line,
                           f,
                           expected_result,
                           span<test_case_data> { new test_case_data[test_case_max_count], 1 },
                           0 };
    new_test.test_cases[0].name = nullptr;
    new_test.test_cases[0].line = line;
    new_test.test_cases[0].result = result_t::failure;

    test_count += 1;
}

auto free_test_memory() -> void {
    for (auto const & test : tests)
        if (test.test_cases.size() > 0)
            delete[] test.test_cases.begin();
}

auto get_tests() -> span<test_data> { return span<test_data> { tests, test_count }; }

// -------------------------------------------------------------------------------

}  // namespace nlc::test
