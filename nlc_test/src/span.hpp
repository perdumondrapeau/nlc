/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "common.hpp"

namespace nlc::test {

// This struct has to be a POD in order to be used in static memory initializers
template<typename T> struct span final {
    [[nodiscard]] auto operator[](unsigned int index) const -> T & {
        nlc_assert(index < _size, __FILE__, __LINE__, "Internal error: out of bound.");
        return _ptr[index];
    }

    [[nodiscard]] auto begin() const -> T * { return _ptr; }
    [[nodiscard]] auto end() const -> T * { return _ptr + _size; }
    [[nodiscard]] auto size() const -> unsigned int { return _size; }

    T * _ptr;
    unsigned int _size;
};

}  // namespace nlc::test
