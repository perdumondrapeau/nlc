#include <nlc/test.hpp>

#include <assert.h>

[[maybe_unused]] static auto should_assert() { assert(true); }

nlc_test("expected assert failed") { nlc_expect_assert(should_assert()); }
