#include <nlc/test.hpp>

static auto crash() {
    volatile auto v = 0;
    return *reinterpret_cast<int const *>(v);
}

nlc_test("expected success crash") { nlc_check(crash() == 0); }
