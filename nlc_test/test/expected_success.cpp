#include <nlc/test.hpp>

nlc_test("expected success") { nlc_check(true); }

nlc_test("one case") {
    int i = 0;
    nlc_test_case("first case") { i += 1; };
    nlc_check(i == 1);
}

nlc_test("two cases") {
    int i = 1;
    static int j = 0;

    nlc_test_case("first case") {
        i += 2;
        j += 1;
    };

    nlc_test_case("second case") {
        i *= 7;
        j += 1;
    };

    switch (j) {
        case 1: nlc_check_msg(i == 3, i); break;
        case 2: nlc_check_msg(i == 7, i); break;
        default: nlc_check(false); break;
    }
}

static int j = 0;

nlc_test("three cases") {
    int i = 1;

    nlc_test_case("first case") {
        i += 2;
        j += 1;
    };

    nlc_test_case("second case") {
        i *= 7;
        j += 1;
    };

    nlc_test_case("third case") {
        i *= 2;
        j += 1;
    };

    switch (j) {
        case 1: nlc_check_msg(i == 3, i); break;
        case 2: nlc_check_msg(i == 7, i); break;
        case 3: nlc_check_msg(i == 2, i); break;
        default: nlc_check(false); break;
    }
}

nlc_test("check three cases") { nlc_check(j == 3); }
