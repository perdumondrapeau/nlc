#include <nlc/sanity/platforms.hpp>
#include <nlc/test.hpp>

static_assert(NLC_ASAN, "compiling without ASAN");

nlc_test("trigger asan") {
    int * array = new int[100];
    delete[] array;
    nlc_check(array[13] != 0);
}
