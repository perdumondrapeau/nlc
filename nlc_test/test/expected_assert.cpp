#include <nlc/sanity/platforms.hpp>
#include <nlc/test.hpp>

#include <nlc/dialect/assert.hpp>

[[maybe_unused]] static void do_assert() { nlc_assert(false); }

nlc_test("expected assert") { nlc_expect_assert(do_assert()); }
