/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/test.hpp>

struct InstanceCounter final {
    static inline int instances = 0;

    InstanceCounter()
        : value(0) {
        ++instances;
    }
    InstanceCounter(InstanceCounter const & other)
        : value { other.value } {
        ++instances;
    }
    InstanceCounter(InstanceCounter && other)
        : value { other.value } {
        ++instances;
    }
    InstanceCounter(int const v)
        : value { v } {
        ++instances;
    }

    ~InstanceCounter() { --instances; }

    int value;
};

nlc_test("one instance counting") {
    nlc_check(InstanceCounter::instances == 0);
    {
        InstanceCounter a;
        nlc_check(InstanceCounter::instances == 1);
    }
    nlc_check(InstanceCounter::instances == 0);
}

nlc_test("two instance counting") {
    nlc_check(InstanceCounter::instances == 0);
    {
        InstanceCounter a;
        InstanceCounter b;
        nlc_check(InstanceCounter::instances == 2);
    }
    nlc_check(InstanceCounter::instances == 0);
}

nlc_test("copy instance counting") {
    nlc_check(InstanceCounter::instances == 0);
    {
        InstanceCounter a;
        InstanceCounter b = a;
        nlc_check(InstanceCounter::instances == 2);
    }
    nlc_check(InstanceCounter::instances == 0);
}
