#include <nlc/test.hpp>

nlc_test("trigger ubsan") {
    [[maybe_unused]] int a = 0x7fffffff;
    a += 1;
    nlc_check(a == 0);  // avoid optimizing a out
}
