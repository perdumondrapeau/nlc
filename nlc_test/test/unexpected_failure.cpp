#include <nlc/test.hpp>

[[maybe_unused]] static auto check([[maybe_unused]] bool const value) { nlc_check(value); }

nlc_test("unexpected failure") { check(false); }
