/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

namespace {
struct vec2 final {
    int x, y;
};

}  // namespace

namespace nlc {
namespace test {
    class string_stream;
}  // namespace test
auto format(test::string_stream & ss, vec2 const vec) -> void;
}  // namespace nlc

#include <nlc/string_stream.hpp>
#include <nlc/test.hpp>

namespace nlc {
auto format(test::string_stream & ss, vec2 const vec) -> void {
    append(ss, "(x:", vec.x, ",y:", vec.y, ")");
}
}  // namespace nlc

namespace {
template<typename T, uint32_t size>
auto check_stream_content(char const (&expected)[size], T const value) -> void {
    nlc::test::string_stream sstr;
    sstr.append(value);

    auto const * const res = sstr.c_str();
    auto const res_len = strlen(res);
    nlc_check_msg(res_len == size - 1 && memcmp(sstr.c_str(), expected, res_len) == 0,
                  "appending ",
                  value,
                  " failed: got '",
                  res,
                  "' of size ",
                  res_len,
                  ", expected '",
                  expected,
                  "' of size ",
                  size - 1);
}

template<typename T, uint32_t size>
auto check_stream_content(char const (&expected)[size], T const value, unsigned int const len) -> void {
    nlc::test::string_stream sstr;
    sstr.append(value, len);

    auto const * const res = sstr.c_str();
    auto const res_len = strlen(res);
    nlc_check_msg(res_len == size - 1 && memcmp(sstr.c_str(), expected, res_len) == 0,
                  "appending ",
                  value,
                  " failed: got '",
                  res,
                  "' of size ",
                  res_len,
                  ", expected '",
                  expected,
                  "' of size ",
                  size - 1);
}

template<unsigned int size> auto check_stream_content(char const (&value)[size]) -> void {
    nlc::test::string_stream sstr;
    append(sstr, value);

    auto const * const res = sstr.c_str();
    auto const res_len = strlen(res);
    nlc_check_msg(memcmp(sstr.c_str(), value, res_len) == 0,
                  "appending ",
                  value,
                  " failed: got '",
                  res,
                  "' of size ",
                  res_len,
                  ", expected '",
                  value,
                  "' of size ",
                  size - 1);
}
}  // namespace

nlc_test("string_stream - basic types") {
    check_stream_content<int8_t>("42", 42);
    check_stream_content<int16_t>("42", 42);
    check_stream_content<int32_t>("42", 42);
    check_stream_content<int64_t>("42", 42);
    check_stream_content<uint8_t>("42", 42);
    check_stream_content<uint16_t>("42", 42);
    check_stream_content<uint32_t>("42", 42);
    check_stream_content<uint64_t>("42", 42);
    check_stream_content("42.1337013", 42.1337f);
    check_stream_content("42.1337", 42.1337);
    check_stream_content("c", 'c');

    char * const dynamic_buffer = reinterpret_cast<char *>(malloc(15));
    memcpy(dynamic_buffer, "dynamic_string", 15);
    check_stream_content<char *>("dynamic_string", dynamic_buffer);
    check_stream_content<char *>("dynamic_string",
                                 dynamic_buffer,
                                 static_cast<unsigned int>(strlen(dynamic_buffer)));
    free(dynamic_buffer);

    check_stream_content<bool>("true", true);
    check_stream_content<bool>("false", false);

    char const * const static_buffer = "static_string";
    check_stream_content<char const *>("static_string", static_buffer);
    check_stream_content<char const *>("static_string",
                                       static_buffer,
                                       static_cast<unsigned int>(strlen(static_buffer)));

    check_stream_content("string");
}

nlc_test("string_stream - custom types") {
    vec2 v { 1, -2 };
    check_stream_content("(x:1,y:-2)", v);

    auto const & u { v };
    check_stream_content("(x:1,y:-2)", u);
}
