cpp_args = ['-DNLC_TEST']

if meson.get_compiler('cpp').get_argument_syntax() == 'msvc'
      # This is just easier to work with ...
      cpp_args += ['-D_CRT_SECURE_NO_WARNINGS', '-D_CRT_NONSTDC_NO_DEPRECATE']
endif

sources = [
    'src/debugger.cpp',
    'src/printing.cpp',
    'src/output.cpp',
    'src/storage.cpp',
    'src/string_stream.cpp',
    'src/test.cpp',
    'src/time.cpp',
]

private_deps = [
    nlc_sanity_dep,
    nlc_debug_dep,
]

nlc_test_lib = static_library(
    'nlc_test',
    sources,
    cpp_args: cpp_args,
    include_directories: 'include/nlc',
    dependencies: private_deps,
)

nlc_test_hooks_lib = static_library(
    'nlc_test_hooks',
    'src/hooks.cpp',
    cpp_args: cpp_args,
    include_directories: 'include/nlc',
    dependencies: private_deps,
)


nlc_test_enabled_dep = declare_dependency(
    compile_args: '-DNLC_TEST',
    link_with: [nlc_test_lib, nlc_test_hooks_lib],
    include_directories: 'include',
)

nlc_test_dep = declare_dependency(
    include_directories: 'include',
    link_with: nlc_test_hooks_lib,
)

# In order to test interactions between tests and assert (defined in nlc_dialect)
# we have to postpone test directory inclusion
# subdir('test')
