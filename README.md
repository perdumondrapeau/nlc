# nlc

[[_TOC_]]

`nlc` is a C++ library aiming to replace the standard library.
Mind you, it doesn't aim to replace it _for everyone_ but at least for us, and did well in that regard. So maybe it can be of use to other folks in the vast world.

As such, it's a highly opinionated library. Some design principles and implementation choices may not fit everyone's needs, and that's OK. While it is generic enough, it is written with high performance games in mind.

## Why ditch the C++ standard library ?

The standard library has a number of flaws that we do not want to bother with.

##### Compile times

The standard library is awfully slow to compile and gets worse by the day.
Modules are still not usable in 2022 and no convincing evidence of them improving build times was shown.

`nlc` is carefully written to be as fast as possible to compile (for C++ code).
Compile times are monitored on every commit and most headers incur less than 50ms when included on a typical development machine, where a standard C++ header can easily add 2 _seconds_ of compile time.

##### API design

Design of the standard's library API is constrained by stuff we don't care about, like exceptions or exotic platforms for example.
By not supporting these use cases, we can simplify the API a lot and make it easier to use and remember.
It's also possible to make it less error-prone.

##### Performance

Performance of the standard library is also constrained by stuff we don't care about.
A common example is the implementation of standard containers that need enormous complexity to be exception-safe.

Some parts of it are known to have much worse performance than old pre-existing libraries, for example `std::regex`.

##### Why not ?

Finally, also just because we felt like it. It's a fun experiment to write this kind of code, and to design your most basic C++ tools for your tastes exactly.

## Caveats, or rather, design choices

- Always on latest published C++ standard (currently C++20).
- No exceptions
- No RTTI
- 64bits only
- No ABI stability
- No (absolute) API stability. Breaking changes happen
- Code duplication is considered acceptable to cut some dependencies
- Always explicit allocations, no `new` or `delete` allowed
- Macros are usually bad, unless the pure C++ solution is worse (because language-level solution would be best)
- Microsoft's compiler, `msvc`, is not supported. `clang-cl` is better in every aspect
- Composition over inheritance
- Data Oriented Design

We took some inspiration from the [zen of zig](https://ziglang.org/documentation/0.9.0/#Zen).

## Documentation summary

`nlc` is divided into several small libraries that depend on each other to form the global library.

These smaller libraries are organized in layers. Each lib is allowed to depend only on ones from higher layers.

- [Library overview](doc/overview.md)
- [From the standard library to `nlc`](doc/from_stdlib_to_nlc.md)

| library                               | description                                                                                                                                                                          |
| :------------------------------------ | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [nlc_sanity](nlc_sanity/)             | A collection of small "infrastructure" pieces. Contains CI helpers, meson boilerplate, etc.                                                                                          |
| [nlc_test](nlc_test/)                 | A minimal testing framework.                                                                                                                                                         |
| [nlc_profiling](nlc_profiling/)       | A minimal profiling framework. Its goal is to abstract code instrumentation without leaking lots of code and huge headers in the client code. It's currently a wrapper around Tracy. |
| [nlc_meta](nlc_meta/)                 | A basic metaprogramming library.                                                                                                                                                     |
| [nlc_dialect](nlc_dialect/)           | `dialect` is thought of as a kind of extension of the language. It mostly contains vocabulary types and a few basic helpers.                                                         |
| [nlc_fundamentals](nlc_fundamentals/) | `fundamentals` introduces the concept of memory allocation, and is a collection of a bit higher level constructs over `dialect`.                                                     |
| [nlc_maths](nlc_maths/)               | A games-oriented math library.                                                                                                                                                       |
| [nlc_algo](nlc_algo/)                 | A collection of more or less generic algorithms.                                                                                                                                     |
| [nlc_containers](nlc_containers/)     | The usual containers.                                                                                                                                                                |
| [nlc_os](nlc_os/)                     | A collection a helpers for OS-dependant features (file system, processes, ...)                                                                                                       |

### Layers visualized
```mermaid
flowchart BT;
subgraph layer1 [layer 1]
    nlc_sanity
end
subgraph layer2 [layer 2]
    nlc_test
    nlc_profiling
end
subgraph layer3 [layer 3]
    nlc_meta
end
subgraph layer4 [layer 4]
    nlc_dialect
end
subgraph layer5 [layer 5]
    nlc_fundamentals
end
subgraph layer6 [layer 6]
    nlc_maths
    nlc_algo
end
subgraph layer7 [layer 7]
    nlc_containers
end
subgraph layer8 [layer 8]
    nlc_os
end

layer2 --> layer1
layer3 --> layer2
layer4 --> layer3
layer5 --> layer4
layer6 --> layer5
layer7 --> layer6
layer8 --> layer7
nlc_dialect --> nlc_meta
nlc_fundamentals --> nlc_dialect
nlc_maths --> nlc_meta
nlc_maths --> nlc_dialect
nlc_maths --> nlc_fundamentals
nlc_algo --> nlc_meta
nlc_algo --> nlc_dialect
nlc_algo --> nlc_fundamentals
nlc_containers --> nlc_meta
nlc_containers --> nlc_dialect
nlc_containers --> nlc_fundamentals
nlc_os --> nlc_meta
nlc_os --> nlc_dialect
nlc_os --> nlc_fundamentals
nlc_os --> nlc_algo
nlc_os --> nlc_containers
```

## Building

We use Meson as our meta build system, and conan as our package manager.
`nlc` has one and a half external dependencies :
- the `rang` library that is used to simplify colored output in `nlc_test` (and would go away someday).
- the `tracy` (clientside) library if you enable the `profiling` option (disabled by default)
Other than that, we use C's standard library in a few places, and C++'s standard library in fewer places (mostly unit tests).

### Getting started

```console
# Configure a build directory
$ meson build
# Build the library and run tests
$ meson test -C build
```
