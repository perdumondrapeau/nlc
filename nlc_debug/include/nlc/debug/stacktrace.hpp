
/* Copyright © 2025-2025
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/sanity/platforms.hpp>

#if NLC_OS_WINDOWS
extern "C" void * _ReturnAddress();
#    pragma intrinsic(_ReturnAddress)
#    define returnaddress() reinterpret_cast<::nlc::debug::frame_ptr>(_ReturnAddress())
#else
#    define returnaddress() reinterpret_cast<::nlc::debug::frame_ptr>(__builtin_return_address(0))
#endif

namespace nlc::debug {

#if NLC_OS_WINDOWS
using usize = unsigned long long int;
#else
using usize = unsigned long int;  // we dont use long long to be compatible with the stl
#endif

using frame_ptr = usize;

[[nodiscard]] auto get_stack_depth() -> usize;
auto capture_stacktrace(frame_ptr first_address, frame_ptr * frames, usize count) -> usize;

struct frame final {
    usize line;
    usize column;
    char filename[1024] = { 0 };
    char symbol[1024] = { 0 };
    char compile_unit[1024] = { 0 };
};

[[nodiscard]] auto decode_frame(frame_ptr) -> frame;
auto decode_frames(frame_ptr * frame_ptrs, frame * frames, usize count) -> void;

auto retrieve_line_of_code(frame const &, char * buffer, usize buffer_len) -> bool;
}  // namespace nlc::debug
