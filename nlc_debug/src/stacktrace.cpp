/* Copyright © 2025-2025
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "stacktrace.hpp"
#include <cstring>

#include <stdio.h>

#ifdef ZIG_STACKTRACE
extern "C" {  // Functions implemented in stacktrace.zig
using namespace nlc::debug;
// return number of captured frames
usize capture_stacktrace(frame_ptr return_address, frame_ptr * frames, usize frames_count);
// return number of bytes written to `out`
usize write_stacktrace(frame_ptr * frames, usize frames_count, char * out, usize out_size);
bool decode_frame(frame_ptr frame,
                  usize * line,
                  usize * colunm,
                  char * filename,
                  char * symbol,
                  char * compile_unit);
}
#endif

namespace nlc::debug {

auto get_stack_depth() -> usize {
    constexpr auto max = 1024;
    frame_ptr dummy[max];
    return capture_stacktrace(returnaddress(), dummy, max);
}

auto capture_stacktrace([[maybe_unused]] frame_ptr first_address,
                        [[maybe_unused]] frame_ptr * frames,
                        [[maybe_unused]] usize count) -> usize {
#ifdef ZIG_STACKTRACE
    return ::capture_stacktrace(first_address, frames, count);
#else
    return 0;
#endif
}

auto decode_frame([[maybe_unused]] frame_ptr ptr) -> frame {
    frame f;
#ifdef ZIG_STACKTRACE
    ::decode_frame(ptr, &f.line, &f.column, f.filename, f.symbol, f.compile_unit);
#endif
    return f;
}

auto retrieve_line_of_code(frame const & f, char * buffer, usize buffer_len) -> bool {
    // Try to print the code line
#if NLC_OS_WINDOWS
    FILE * fp;
    fopen_s(&fp, f.filename, "rb");
#else
    auto const fp = fopen(f.filename, "rb");
#endif
    if (fp == nullptr)
        return false;

    // get the designated line
    for (auto j = 1u; j <= f.line; ++j) {
        auto const _ = fgets(buffer, static_cast<int>(buffer_len), fp);
        static_cast<void>(_);
    }
    fclose(fp);

    // Ensure buffer ends with null character
    buffer[buffer_len - 1] = '\0';

    // Find the effective begining of the line to remove indent
    auto const indent = [&]() {
        auto j = 0u;
        while (buffer[j] != '\0' && (buffer[j] == ' ' || buffer[j] == '\t'))
            ++j;
        return j;
    }();

    if (buffer[indent] == '\0')
        return false;

    // Find end of line
    auto const end = [&]() -> usize {
        for (auto j = indent; j < buffer_len; ++j)
            if (buffer[j] == '\n' || buffer[j] == '\r' || buffer[j] == '\0')
                return j;
        return buffer_len - 1;
    }();

    auto const len = end - indent;

    memmove(buffer, buffer + indent, len);
    buffer[len] = '\0';

    return true;
}
}  // namespace nlc::debug
