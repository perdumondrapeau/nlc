// Copyright © 2019-2025
//          Alexis A. D. COLIN,
//          Antoine VUGLIANO,
//          Gaëtan CHAMPARNAUD,
//          Geoffrey L. TOURON,
//          Grégoire A. P. BADIN
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequentversions of the EUPL (the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.///

const std = @import("std");

export fn capture_stacktrace(ret_addr: usize, frames: [*]usize, count: usize) usize {
    var st: std.builtin.StackTrace = .{
        .index = 0,
        .instruction_addresses = frames[0..count],
    };
    std.debug.captureStackTrace(ret_addr, &st);

    return st.index;
}

fn copy(to: [*c]u8, from: []const u8) void {
    const size = @min(1024, from.len);
    @memcpy(to[0..size], from);
}

export fn decode_frame(next_frame: usize, line: *usize, column: *usize, filename: [*c]u8, symbol: [*c]u8, compile_unit: [*c]u8) bool {
    const debug_info = std.debug.getSelfDebugInfo() catch return false;

    if (next_frame < 1)
        return false;

    const frame = next_frame - 1;

    const module = debug_info.getModuleForAddress(frame) catch return false;

    const symbol_info = module.getSymbolAtAddress(debug_info.allocator, frame) catch return false;
    defer symbol_info.deinit(debug_info.allocator);

    if (symbol_info.line_info) |line_info| {
        line.* = symbol_info.line_info.?.line;
        column.* = symbol_info.line_info.?.column;
        copy(filename, line_info.file_name);
    } else return false;

    copy(symbol, symbol_info.symbol_name);
    copy(compile_unit, symbol_info.compile_unit_name);

    return true;
}
