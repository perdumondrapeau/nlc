/*  Copyright © 2024-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/concurrency/mutex.hpp>
#include <nlc/concurrency/rw_lock.hpp>
#include <nlc/concurrency/thread.hpp>
#include <nlc/test.hpp>

using namespace nlc;

static usize global_counter;
static rw_lock lock;
static mutex mtx;

static void * adder(void * data) {
    usize * value = reinterpret_cast<usize *>(data);
    *value += 7;
    return nullptr;
}

static void * counter_no_sync(void *) {
    for (unsigned i = 0; i < 100'000; ++i)
        ++global_counter;
    return nullptr;
}

static void * counter_sync_mutex(void *) {
    for (unsigned i = 0; i < 100'000; ++i) {
        auto const sl = mtx.lock();
        ++global_counter;
    }
    return nullptr;
}

static void * counter_sync_rw_lock(void *) {
    for (unsigned i = 0; i < 100'000; ++i) {
        auto const sl = lock.lock_exclusive();
        ++global_counter;
    }
    return nullptr;
}

nlc_test("get_core_count") {
    nlc_check(core_count > 0);
    nlc_check(core_count < 4048);
}

nlc_test("thread adder") {
    usize value = 3;
    auto const thr_id = create_thread(adder, &value);
    nlc_check(thr_id.is_valid());
    join_thread(thr_id);
    nlc_check(value == 10);
}

nlc_test("thread counter no sync") {
    global_counter = 0;
    auto const counter_a = create_thread(counter_no_sync, nullptr);
    auto const counter_b = create_thread(counter_no_sync, nullptr);
    nlc_check(counter_a.is_valid());
    nlc_check(counter_b.is_valid());
    join_thread(counter_a);
    join_thread(counter_b);
    nlc_check(global_counter > 0);
    nlc_check(global_counter <= 200'000);
}

nlc_test("thread counter sync mutex") {
    global_counter = 0;
    auto const counter_a = create_thread(counter_sync_mutex, nullptr);
    auto const counter_b = create_thread(counter_sync_mutex, nullptr);
    nlc_check(counter_a.is_valid());
    nlc_check(counter_b.is_valid());
    join_thread(counter_a);
    join_thread(counter_b);
    nlc_check(global_counter == 200'000);
}

nlc_test("thread counter sync rw_lock") {
    global_counter = 0;
    auto const counter_a = create_thread(counter_sync_rw_lock, nullptr);
    auto const counter_b = create_thread(counter_sync_rw_lock, nullptr);
    nlc_check(counter_a.is_valid());
    nlc_check(counter_b.is_valid());
    join_thread(counter_a);
    join_thread(counter_b);
    nlc_check(global_counter == 200'000);
}
