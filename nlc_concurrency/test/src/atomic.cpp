/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/concurrency/atomic.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test("atomic_flag") {
    atomic_flag basic;
    atomic_flag defaulted(meta::default_);

    basic.clear();
    nlc_check(basic.test() == false);
    nlc_check(defaulted.test() == false);
    nlc_check(defaulted.test_and_set() == false);
    nlc_check(defaulted.test_and_set() == true);
    defaulted.clear();
    nlc_check(defaulted.test() == false);
}

nlc_test("atomic ptr") {
    atomic<void *> atom;

    atom.store(nullptr);
    nlc_check(atom.load() == nullptr);
    nlc_check(atom.exchange(&atom) == nullptr);
    nlc_check(atom.load() == &atom);
    nlc_check(atom.compare_exchange_weak(&atom, nullptr));
    nlc_check(atom.load() == nullptr);
    nlc_check(atom.compare_exchange_strong(nullptr, &atom));
    nlc_check(atom.load() == &atom);
}

nlc_test("atomic i8") {
    atomic<i8> atom;

    atom.store(-10);
    nlc_check(atom.load() == -10);
    nlc_check(atom.exchange(36) == -10);

    nlc_check(atom.add_fetch(-3) == 33);
    nlc_check(atom.sub_fetch(2) == 31);
    nlc_check(atom.fetch_add(10) == 31);
    nlc_check(atom.fetch_sub(9) == 41);
    nlc_check(atom.load() == 32);
}

nlc_test("atomic u8") {
    atomic<u8> atom;

    atom.store(10);
    nlc_check(atom.load() == 10);
    nlc_check(atom.exchange(36) == 10);

    nlc_check(atom.add_fetch(3) == 39);
    nlc_check(atom.sub_fetch(2) == 37);
    nlc_check(atom.fetch_add(10) == 37);
    nlc_check(atom.fetch_sub(9) == 47);
    nlc_check(atom.load() == 38);

    atom.store(0xff);
    nlc_check(atom.and_fetch(0xf0) == 0xf0);
    nlc_check(atom.xor_fetch(0xff) == 0x0f);
    nlc_check(atom.or_fetch(0xff) == 0xff);
    nlc_check(atom.nand_fetch(0xff) == 0x00);

    atom.store(0xff);
    nlc_check(atom.fetch_and(0xf0) == 0xff);
    nlc_check(atom.fetch_xor(0xff) == 0xf0);
    nlc_check(atom.fetch_or(0xff) == 0x0f);
    nlc_check(atom.fetch_nand(0xff) == 0xff);
    nlc_check(atom.load() == 0x00);
}

nlc_test("atomic i16") {
    atomic<i16> atom;

    atom.store(-10);
    nlc_check(atom.load() == -10);
    nlc_check(atom.exchange(36) == -10);

    nlc_check(atom.add_fetch(-3) == 33);
    nlc_check(atom.sub_fetch(2) == 31);
    nlc_check(atom.fetch_add(10) == 31);
    nlc_check(atom.fetch_sub(9) == 41);
    nlc_check(atom.load() == 32);
}

nlc_test("atomic u16") {
    atomic<u16> atom;

    atom.store(10);
    nlc_check(atom.load() == 10);
    nlc_check(atom.exchange(36) == 10);

    nlc_check(atom.add_fetch(3) == 39);
    nlc_check(atom.sub_fetch(2) == 37);
    nlc_check(atom.fetch_add(10) == 37);
    nlc_check(atom.fetch_sub(9) == 47);
    nlc_check(atom.load() == 38);

    atom.store(0xffff);
    nlc_check(atom.and_fetch(0xfff0) == 0xfff0);
    nlc_check(atom.xor_fetch(0xffff) == 0x000f);
    nlc_check(atom.or_fetch(0x00ff) == 0x00ff);
    nlc_check(atom.nand_fetch(0xffff) == 0xff00);

    atom.store(0xffff);
    nlc_check(atom.fetch_and(0xfff0) == 0xffff);
    nlc_check(atom.fetch_xor(0xffff) == 0xfff0);
    nlc_check(atom.fetch_or(0x00ff) == 0x000f);
    nlc_check(atom.fetch_nand(0xffff) == 0x00ff);
    nlc_check(atom.load() == 0xff00);
}

nlc_test("atomic i32") {
    atomic<i32> atom;

    atom.store(-10);
    nlc_check(atom.load() == -10);
    nlc_check(atom.exchange(36) == -10);

    nlc_check(atom.add_fetch(-3) == 33);
    nlc_check(atom.sub_fetch(2) == 31);
    nlc_check(atom.fetch_add(10) == 31);
    nlc_check(atom.fetch_sub(9) == 41);
    nlc_check(atom.load() == 32);
}

nlc_test("atomic u32") {
    atomic<u32> atom;

    atom.store(10);
    nlc_check(atom.load() == 10);
    nlc_check(atom.exchange(36) == 10);

    nlc_check(atom.add_fetch(3) == 39);
    nlc_check(atom.sub_fetch(2) == 37);
    nlc_check(atom.fetch_add(10) == 37);
    nlc_check(atom.fetch_sub(9) == 47);
    nlc_check(atom.load() == 38);

    atom.store(0x0000ffff);
    nlc_check(atom.and_fetch(0xfffffff0) == 0x0000fff0);
    nlc_check(atom.xor_fetch(0xffffffff) == 0xffff000f);
    nlc_check(atom.or_fetch(0x000000ff) == 0xffff00ff);
    nlc_check(atom.nand_fetch(0x0000ffff) == 0xffffff00);

    atom.store(0x0000ffff);
    nlc_check(atom.fetch_and(0xfffffff0) == 0x0000ffff);
    nlc_check(atom.fetch_xor(0xffffffff) == 0x0000fff0);
    nlc_check(atom.fetch_or(0x000000ff) == 0xffff000f);
    nlc_check(atom.fetch_nand(0x0000ffff) == 0xffff00ff);
    nlc_check(atom.load() == 0xffffff00);
}

nlc_test("atomic i64") {
    atomic<i64> atom;

    atom.store(-10);
    nlc_check(atom.load() == -10);
    nlc_check(atom.exchange(36) == -10);

    nlc_check(atom.add_fetch(-3) == 33);
    nlc_check(atom.sub_fetch(2) == 31);
    nlc_check(atom.fetch_add(10) == 31);
    nlc_check(atom.fetch_sub(9) == 41);
    nlc_check(atom.load() == 32);
}

nlc_test("atomic u64") {
    atomic<u64> atom;

    atom.store(10);
    nlc_check(atom.load() == 10);
    nlc_check(atom.exchange(36) == 10);

    nlc_check(atom.add_fetch(3) == 39);
    nlc_check(atom.sub_fetch(2) == 37);
    nlc_check(atom.fetch_add(10) == 37);
    nlc_check(atom.fetch_sub(9) == 47);
    nlc_check(atom.load() == 38);

    atom.store(0x0000'ffff'0000'ffff);
    nlc_check(atom.and_fetch(0xffff'fff0'ffff'fff0) == 0x0000'fff0'0000'fff0);
    nlc_check(atom.xor_fetch(0xffff'ffff'ffff'ffff) == 0xffff'000f'ffff'000f);
    nlc_check(atom.or_fetch(0x0000'00ff'0000'00ff) == 0xffff'00ff'ffff'00ff);
    nlc_check(atom.nand_fetch(0x0000'ffff'0000'ffff) == 0xffff'ff00'ffff'ff00);

    atom.store(0x0000'ffff'0000'ffff);
    nlc_check(atom.fetch_and(0xffff'fff0'ffff'fff0) == 0x0000'ffff'0000'ffff);
    nlc_check(atom.fetch_xor(0xffff'ffff'ffff'ffff) == 0x0000'fff0'0000'fff0);
    nlc_check(atom.fetch_or(0x0000'00ff'0000'00ff) == 0xffff'000f'ffff'000f);
    nlc_check(atom.fetch_nand(0x0000'ffff'0000'ffff) == 0xffff'00ff'ffff'00ff);
    nlc_check(atom.load() == 0xffff'ff00'ffff'ff00);
}
