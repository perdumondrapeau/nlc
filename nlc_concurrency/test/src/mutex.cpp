/*  Copyright © 2024-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/concurrency/mutex.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test("mutex init") { mutex lock; }

nlc_test("mutex") {
    mutex lock;
    u32 lock_counter = 0;

    {
        auto sl = lock.lock();
        ++lock_counter;
    }
    nlc_check(lock_counter == 1);

    if (auto stl = lock.try_lock()) {
        ++lock_counter;
    } else
        nlc_check(false);
    nlc_check(lock_counter == 2);
}
