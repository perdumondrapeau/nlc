/* Copyright © 2024-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/concurrency/condition_variable.hpp>
#include <nlc/concurrency/mutex.hpp>
#include <nlc/concurrency/thread.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/test.hpp>

using namespace nlc;

static u32 global_counter;
static condition_variable cv;
static mutex mtx;
static bool ready;
static bool job_done;

static void * worker(void *) {
    {
        auto sl = cv.wait(mtx, []() { return ready; });
        global_counter *= 13;
        job_done = true;
    }
    cv.notify_one();
    return nullptr;
}

nlc_test("condition_variable init") { condition_variable local_cv; }

nlc_test("condition_variable") {
    global_counter = 0;
    ready = false;
    job_done = false;
    auto const worker_thread = create_thread(&worker, nullptr);

    {
        auto sl = mtx.lock();
        global_counter += 1;
        ready = true;
    }
    cv.notify_one();
    {
        auto sl = cv.wait(mtx, [] { return job_done; });
    }
    join_thread(worker_thread);
    nlc_check(global_counter == 13);
}
