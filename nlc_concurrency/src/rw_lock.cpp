/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "rw_lock.hpp"

#include <nlc/sanity/platforms.hpp>

#if NLC_OS_WINDOWS

// Define necessary stuff to avoid including windows.h
#    define WINAPI __stdcall

extern "C" {

typedef void * PVOID;

typedef struct _RTL_SRWLOCK {
    PVOID Ptr;
} RTL_SRWLOCK, *PRTL_SRWLOCK;

typedef RTL_SRWLOCK SRWLOCK, *PSRWLOCK;

void WINAPI InitializeSRWLock(PSRWLOCK);

void WINAPI AcquireSRWLockShared(PSRWLOCK);
bool WINAPI TryAcquireSRWLockShared(PSRWLOCK);
void WINAPI ReleaseSRWLockShared(PSRWLOCK);

void WINAPI AcquireSRWLockExclusive(PSRWLOCK);
void WINAPI ReleaseSRWLockExclusive(PSRWLOCK);
bool WINAPI TryAcquireSRWLockExclusive(PSRWLOCK);
}

#else

#    include <nlc/dialect/assert.hpp>
#    include <nlc/dialect/new.hpp>

#    include <errno.h>
#    include <pthread.h>

#endif

namespace nlc {

#if NLC_OS_WINDOWS

static_assert(sizeof(SRWLOCK) == sizeof(rw_lock));

rw_lock::rw_lock() { InitializeSRWLock(reinterpret_cast<PSRWLOCK>(&_handle)); }

rw_lock::~rw_lock() {}

// read

auto rw_lock::_lock_shared() -> void { AcquireSRWLockShared(reinterpret_cast<PSRWLOCK>(&_handle)); }

auto rw_lock::_try_lock_shared() -> bool {
    return TryAcquireSRWLockShared(reinterpret_cast<PSRWLOCK>(&_handle));
}

auto rw_lock::_unlock_shared() -> void {
    ReleaseSRWLockShared(reinterpret_cast<PSRWLOCK>(&_handle));
}

// write

auto rw_lock::_lock_exclusive() -> void {
    AcquireSRWLockExclusive(reinterpret_cast<PSRWLOCK>(&_handle));
}

auto rw_lock::_try_lock_exclusive() -> bool {
    return TryAcquireSRWLockExclusive(reinterpret_cast<PSRWLOCK>(&_handle));
}

auto rw_lock::_unlock_exclusive() -> void {
    ReleaseSRWLockExclusive(reinterpret_cast<PSRWLOCK>(&_handle));
}

#else  // unix

// Errors triggered by pthread_rwlock_* are mostly due to bad programming.
// In every case we cannot recover from thoose.

[[nodiscard]] static auto get_typed_handle(void * handle) -> pthread_rwlock_t * {
#    if NLC_OS_LINUX
    return reinterpret_cast<pthread_rwlock_t *>(handle);
#    else
    return reinterpret_cast<pthread_rwlock_t *>(&handle);
#    endif
}

rw_lock::rw_lock() {
#    if NLC_OS_LINUX
    static_assert(sizeof(pthread_rwlock_t) <= sizeof(_handle) &&
                  sizeof(pthread_rwlock_t) > sizeof(void *));
#    endif
    [[maybe_unused]] auto const result = pthread_rwlock_init(get_typed_handle(_handle), nullptr);
    nlc_assert_msg(result == 0,
                   "This rw_lock is alreay initialised "
                   "or the address of this rw_lock is invalid "
                   "or the system lacks of ressources to initialise it "
                   "or this process doesn't have the privilege.");
}

rw_lock::~rw_lock() {
    [[maybe_unused]] auto const result = pthread_rwlock_destroy(get_typed_handle(_handle));
    nlc_assert_msg(result == 0, "This rw_lock is invalid or is locked.");
}

// read

auto rw_lock::_lock_shared() -> void {
    [[maybe_unused]] auto const result = pthread_rwlock_rdlock(get_typed_handle(_handle));
    nlc_assert_msg(result == 0,
                   "This rw_lock is invalid or is already locked by this thread in exclusive "
                   "mode.");
}

auto rw_lock::_try_lock_shared() -> bool {
    auto const result = pthread_rwlock_rdlock(get_typed_handle(_handle));
    nlc_assert_msg(result == 0 || result == EBUSY,
                   "This rw_lock is invalid or is already locked by this thread in exclusive "
                   "mode.");
    return result == 0;
}

auto rw_lock::_unlock_shared() -> void {
    [[maybe_unused]] auto const result = pthread_rwlock_unlock(get_typed_handle(_handle));
    nlc_assert_msg(result == 0, "This rw_lock is unitialised or haven't been locked by this thread.");
}

// write

auto rw_lock::_lock_exclusive() -> void {
    [[maybe_unused]] auto const result = pthread_rwlock_wrlock(get_typed_handle(_handle));
    nlc_assert_msg(result == 0,
                   "This rw_lock is invalid or is already locked by this thread in shared mode.");
}

auto rw_lock::_try_lock_exclusive() -> bool {
    auto const result = pthread_rwlock_trywrlock(get_typed_handle(_handle));
    nlc_assert_msg(result == 0 || result == EBUSY,
                   "This rw_lock is invalid or is already locked by this thread in shared mode.");
    return result == 0;
}

auto rw_lock::_unlock_exclusive() -> void {
    [[maybe_unused]] auto const result = pthread_rwlock_unlock(get_typed_handle(_handle));
    nlc_assert_msg(result == 0, "This rw_lock is unitialised or haven't been locked by this thread.");
}

#endif

}  // namespace nlc
