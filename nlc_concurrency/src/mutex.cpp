/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "mutex.hpp"

#include <nlc/sanity/platforms.hpp>

#if NLC_OS_WINDOWS

// Define necessary stuff to avoid including windows.h
#    define WINAPI __stdcall

extern "C" {

typedef void * PVOID;

typedef struct _RTL_SRWLOCK {
    PVOID Ptr;
} RTL_SRWLOCK, *PRTL_SRWLOCK;

typedef RTL_SRWLOCK SRWLOCK, *PSRWLOCK;

void WINAPI InitializeSRWLock(PSRWLOCK);

void WINAPI AcquireSRWLockExclusive(PSRWLOCK);
void WINAPI ReleaseSRWLockExclusive(PSRWLOCK);
bool WINAPI TryAcquireSRWLockExclusive(PSRWLOCK);
}

#else

#    include <nlc/dialect/assert.hpp>
#    include <nlc/dialect/new.hpp>

#    include <errno.h>
#    include <pthread.h>

#endif

namespace nlc {

#if NLC_OS_WINDOWS

static_assert(sizeof(SRWLOCK) == sizeof(mutex));

mutex::mutex() { InitializeSRWLock(reinterpret_cast<PSRWLOCK>(&_handle)); }

mutex::~mutex() {}

auto mutex::_lock() -> void { AcquireSRWLockExclusive(reinterpret_cast<PSRWLOCK>(&_handle)); }

auto mutex::_try_lock() -> bool {
    return TryAcquireSRWLockExclusive(reinterpret_cast<PSRWLOCK>(&_handle));
}

auto mutex::_unlock() -> void { ReleaseSRWLockExclusive(reinterpret_cast<PSRWLOCK>(&_handle)); }

#else  // unix

// Errors triggered by pthread_mutex_* are mostly due to bad programming.
// In every case we cannot recover from thoose.

[[nodiscard]] static auto get_typed_handle(void * handle) -> pthread_mutex_t * {
#    if NLC_OS_LINUX
    return reinterpret_cast<pthread_mutex_t *>(handle);
#    else
    return reinterpret_cast<pthread_mutex_t *>(&handle);
#    endif
}

mutex::mutex() {
#    if NLC_OS_LINUX
    static_assert(sizeof(pthread_mutex_t) <= sizeof(_handle) &&
                  sizeof(pthread_mutex_t) > sizeof(void *));
#    endif
    [[maybe_unused]] auto const result = pthread_mutex_init(get_typed_handle(_handle), nullptr);
    nlc_assert_msg(result == 0,
                   "This mutex is alreay initialised "
                   "or the address of this rw_lock is invalid "
                   "or the system lacks of ressources to initialise it "
                   "or this process doesn't have the privilege.");
}

mutex::~mutex() {
    [[maybe_unused]] auto const result = pthread_mutex_destroy(get_typed_handle(_handle));
    nlc_assert_msg(result == 0, "This mutex is invalid or is locked by another thread.");
}

auto mutex::_lock() -> void {
    [[maybe_unused]] auto const result = pthread_mutex_lock(get_typed_handle(_handle));
    nlc_assert_msg(result == 0, "This mutex is invalid.");
}

auto mutex::_try_lock() -> bool {
    auto const result = pthread_mutex_trylock(get_typed_handle(_handle));
    nlc_assert_msg(result == 0 || result == EBUSY, "This mutex is invalid or already locked.");
    return result == 0;
}

auto mutex::_unlock() -> void {
    [[maybe_unused]] auto const result = pthread_mutex_unlock(get_typed_handle(_handle));
    nlc_assert_msg(result == 0, "This mutex is invalid or already locked.");
}

#endif

}  // namespace nlc
