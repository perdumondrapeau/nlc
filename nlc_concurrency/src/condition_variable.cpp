/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "condition_variable.hpp"

#include <nlc/sanity/platforms.hpp>

#if NLC_OS_WINDOWS

// Define necessary stuff to avoid including windows.h
#    define WINAPI __stdcall

extern "C" {

typedef void * PVOID;

typedef struct _RTL_SRWLOCK {
    PVOID Ptr;
} RTL_SRWLOCK, *PRTL_SRWLOCK;

typedef struct _RTL_CONDITION_VARIABLE {
    PVOID Ptr;
} RTL_CONDITION_VARIABLE, *PRTL_CONDITION_VARIABLE;

typedef RTL_SRWLOCK SRWLOCK, *PSRWLOCK;
typedef RTL_CONDITION_VARIABLE CONDITION_VARIABLE, *PCONDITION_VARIABLE;

void WINAPI InitializeConditionVariable(PCONDITION_VARIABLE);

bool WINAPI SleepConditionVariableSRW(PCONDITION_VARIABLE, PSRWLOCK, unsigned long, unsigned long);
void WINAPI WakeConditionVariable(PCONDITION_VARIABLE);
void WINAPI WakeAllConditionVariable(PCONDITION_VARIABLE);
}

#else

#    include <nlc/dialect/assert.hpp>
#    include <nlc/dialect/new.hpp>

#    include <errno.h>
#    include <pthread.h>

#endif

namespace nlc {

#if NLC_OS_WINDOWS

static_assert(sizeof(PCONDITION_VARIABLE) == sizeof(condition_variable));

condition_variable::condition_variable() {
    InitializeConditionVariable(reinterpret_cast<PCONDITION_VARIABLE>(&_handle));
}

condition_variable::~condition_variable() {}

auto condition_variable::_wait(mutex & mtx) -> void {
    [[maybe_unused]] auto const result =
        SleepConditionVariableSRW(reinterpret_cast<PCONDITION_VARIABLE>(&_handle),
                                  reinterpret_cast<PSRWLOCK>(&mtx._handle),
                                  static_cast<unsigned long>(-1),
                                  0);
    nlc_assert_msg(result, "Something went wrong.");
}

auto condition_variable::notify_one() -> void {
    WakeConditionVariable(reinterpret_cast<PCONDITION_VARIABLE>(&_handle));
}

auto condition_variable::notify_all() -> void {
    WakeAllConditionVariable(reinterpret_cast<PCONDITION_VARIABLE>(&_handle));
}

#else  // unix

// Errors triggered by pthread_mutex_* are mostly due to bad programming.
// In every case we cannot recover from thoose.

[[nodiscard]] static auto get_typed_condition_variable_handle(void * handle) -> pthread_cond_t * {
#    if NLC_OS_LINUX
    return reinterpret_cast<pthread_cond_t *>(handle);
#    else
    return reinterpret_cast<pthread_cond_t *>(&handle);
#    endif
}

[[nodiscard]] static auto get_typed_mutex_handle(void * handle) -> pthread_mutex_t * {
#    if NLC_OS_LINUX
    return reinterpret_cast<pthread_mutex_t *>(handle);
#    else
    return reinterpret_cast<pthread_mutex_t *>(&handle);
#    endif
}

condition_variable::condition_variable() {
#    if NLC_OS_LINUX
    static_assert(sizeof(pthread_cond_t) <= sizeof(_handle) && sizeof(pthread_cond_t) > sizeof(void *));
#    endif
    [[maybe_unused]] auto const result =
        pthread_cond_init(get_typed_condition_variable_handle(_handle), nullptr);
    nlc_assert_msg(result == 0,
                   "This condition_variable is alreay initialised "
                   "or the system lacks of ressources to initialise it.");
}

condition_variable::~condition_variable() {
    [[maybe_unused]] auto const result =
        pthread_cond_destroy(get_typed_condition_variable_handle(_handle));
    nlc_assert_msg(result == 0, "This condition_variable is invalid or is locked by another thread.");
}

auto condition_variable::_wait(mutex & mtx) -> void {
    [[maybe_unused]] auto const result =
        pthread_cond_wait(get_typed_condition_variable_handle(_handle),
                          get_typed_mutex_handle(mtx._handle));
    nlc_assert_msg(result == 0,
                   "The condition_varaible is invalid or the mutex is invalid "
                   "or the mutex is locked by another thread.");
}

auto condition_variable::notify_one() -> void {
    [[maybe_unused]] auto const result =
        pthread_cond_signal(get_typed_condition_variable_handle(_handle));
    nlc_assert_msg(result == 0, "This condition_varaible is invalid.");
}

auto condition_variable::notify_all() -> void {
    [[maybe_unused]] auto const result =
        pthread_cond_broadcast(get_typed_condition_variable_handle(_handle));
    nlc_assert_msg(result == 0, "This condition_varaible is invalid.");
}

#endif

}  // namespace nlc
