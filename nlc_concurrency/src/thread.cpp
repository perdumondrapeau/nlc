/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "thread.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/sanity/platforms.hpp>

#if NLC_OS_WINDOWS
#    define NOMINMAX
#    define WIN32_LEAN_AND_MEAN
#    define VC_EXTRALEAN
#    include <Windows.h>
#    include <processthreadsapi.h>
#else
#    include <pthread.h>
#    include <sched.h>
#    ifndef _SYS_PARAM_H
#        include <sys/param.h>
#    endif
#    if NLC_OS_LINUX
#        include <unistd.h>
#    else  // bsd
#        include <sys/sysctl.h>
#    endif
#endif

namespace nlc {

#if NLC_OS_WINDOWS

static auto _get_core_count() -> u32 {
    SYSTEM_INFO sysinfo;

    GetSystemInfo(&sysinfo);
    return sysinfo.dwNumberOfProcessors;
}

auto create_thread(void * (*function)(void *), void * data) -> thread_id {
#    if NLC_COMPILER_CLANG
#        pragma clang diagnostic push
#        pragma clang diagnostic ignored "-Wcast-function-type"
#        pragma clang diagnostic ignored "-Wcast-function-type-strict"
#    endif
    auto const handle =
        CreateThread(nullptr, 0, reinterpret_cast<LPTHREAD_START_ROUTINE>(function), data, 0, nullptr);
#    if NLC_COMPILER_CLANG
#        pragma clang diagnostic pop
#    endif
    nlc_assert(handle != nullptr);
    thread_id const id { reinterpret_cast<usize>(handle) };
    return id;
}

auto this_thread() -> thread_id {
    return thread_id { reinterpret_cast<usize>(GetCurrentThread()) };
}

auto join_thread(thread_id const id) -> void {
    [[maybe_unused]] auto const result =
        WaitForSingleObject(reinterpret_cast<void *>(id.value()), INFINITE);
    nlc_assert(result != WAIT_FAILED);
}

auto set_core_affinity(thread_id const id, u32 const core_index) -> void {
    SetThreadIdealProcessor(reinterpret_cast<void *>(id.value()), core_index);
}

#else  // unix

#    if NLC_OS_LINUX

static auto _get_core_count() -> u32 {
    return static_cast<unsigned int>(sysconf(_SC_NPROCESSORS_ONLN));
}

#    else

static auto _get_core_count() -> u32 {
    u32 cn;
    u32 mib[2];
    usize len = sizeof(cn);

    mib[0] = CTL_HW;
    mib[1] = HW_NCPU;
    sysctl(mib, 2, &cn, &len, 0, 0);
    return cn;
}

#    endif

auto create_thread(void * (*function)(void *), void * data) -> thread_id {
    pthread_t thread;
    [[maybe_unused]] auto const result = pthread_create(&thread, nullptr, function, data);
    nlc_assert_msg(result == 0,
                   "The system lacks of ressources to initialise it "
                   "or this process doesn't have the privilege.");
    thread_id const id { thread };
    return id;
}

auto this_thread() -> thread_id { return thread_id { pthread_self() }; }

auto join_thread(thread_id const id) -> void {
    [[maybe_unused]] auto const result = pthread_join(id.value(), nullptr);
    nlc_assert_msg(result == 0, "Thread_id is invalid or a deadlock has been detected.");
}

auto set_core_affinity(thread_id const id, u32 const core_index) -> void {
    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(core_index, &cpuset);

    [[maybe_unused]] auto const reseult =
        pthread_setaffinity_np(id.value(), sizeof(cpu_set_t), &cpuset);
    nlc_assert(reseult == 0);
}

#endif

u32 const core_count = _get_core_count();

}  // namespace nlc
