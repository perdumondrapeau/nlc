/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/meta/id.hpp>
#include <nlc/meta/numeric_limits.hpp>
#include <nlc/sanity/platforms.hpp>

namespace nlc {

namespace impl {
    struct thread_id {
        using underlying_type = usize;
#if NLC_OS_WINDOWS
        static constexpr auto invalid_value = 0;
#else
        static constexpr auto invalid_value = nlc::meta::limits<underlying_type>::max;
#endif
        static constexpr auto default_value = invalid_value;
    };
}  // namespace impl
using thread_id = nlc::meta::id<impl::thread_id>;

[[nodiscard]] auto create_thread(void * (*function)(void *), void * data) -> thread_id;
[[nodiscard]] auto this_thread() -> thread_id;
auto join_thread(thread_id) -> void;
auto set_core_affinity(thread_id, u32 core_index) -> void;  // core_index must be >= 0
                                                            // and < core_count

extern u32 const core_count;

}  // namespace nlc
