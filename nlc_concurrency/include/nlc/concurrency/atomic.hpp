/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/assert.hpp>
#include <nlc/meta/tags.hpp>

namespace nlc {

enum class memory_order : int {
    relaxed = __ATOMIC_RELAXED,
    consume = __ATOMIC_CONSUME,
    acquire = __ATOMIC_ACQUIRE,
    release = __ATOMIC_RELEASE,
    acquire_release = __ATOMIC_ACQ_REL,
    sequentially_consistent = __ATOMIC_SEQ_CST
};

enum class read_memory_order : int {
    relaxed = __ATOMIC_RELAXED,
    consume = __ATOMIC_CONSUME,
    acquire = __ATOMIC_ACQUIRE,
    sequentially_consistent = __ATOMIC_SEQ_CST
};

enum class write_memory_order : int {
    relaxed = __ATOMIC_RELAXED,
    release = __ATOMIC_RELEASE,
    sequentially_consistent = __ATOMIC_SEQ_CST
};

class [[nodiscard]] atomic_flag final {
    bool _value;

    static_assert(__atomic_always_lock_free(sizeof(_value), nullptr),
                  "sizeof(_value) must be <= 8 and even");

  public:
    atomic_flag() {}
    atomic_flag(meta::default_type) { clear(); }

    // set the flag to false
    auto clear(write_memory_order const order = write_memory_order::sequentially_consistent) -> void {
        __atomic_clear(&_value, static_cast<int>(order));
    }

    // return the value of the flag
    [[nodiscard]] auto
    test(read_memory_order const order = read_memory_order::sequentially_consistent) -> bool {
        return __atomic_load_n(&_value, static_cast<int>(order));
    }

    // set the flag to true and return the previous value
    [[nodiscard]] auto test_and_set(memory_order const order = memory_order::sequentially_consistent) -> bool {
        return __atomic_test_and_set(&_value, static_cast<int>(order));
    }
};

template<typename T> class [[nodiscard]] atomic final {
    T _value;

    static_assert(__atomic_always_lock_free(sizeof(_value), nullptr),
                  "sizeof(_value) must be <= 8 and even");

  public:
    atomic() {}
    atomic(T const value) { store(value); }

    [[nodiscard]] auto
    load(read_memory_order const order = read_memory_order::sequentially_consistent) const -> T {
        return __atomic_load_n(&_value, static_cast<int>(order));
    }

    auto store(T const value,
               write_memory_order const order = write_memory_order::sequentially_consistent) -> void {
        __atomic_store_n(&_value, value, static_cast<int>(order));
    }

    [[nodiscard]] auto exchange(T const value,
                                memory_order const order = memory_order::sequentially_consistent) -> T {
        return __atomic_exchange_n(&_value, value, static_cast<int>(order));
    }

    // On some architecture compare_exchange_weak CANs fail spuriously.
    // On some architecture compare_exchange_strong CAN be slower to guaranty that the fail to the
    // operation is legit.
    [[nodiscard]] auto compare_exchange_weak(T const expected,
                                             T const desired,
                                             memory_order const success,
                                             memory_order const failure) -> bool {
        nlc_assert(failure != memory_order::release && failure != memory_order::acquire_release);
        nlc_assert(failure >= success);
        return __atomic_compare_exchange_n(&_value,
                                           const_cast<T *>(&expected),
                                           desired,
                                           true,
                                           static_cast<int>(success),
                                           static_cast<int>(failure));
    }

    [[nodiscard]] auto compare_exchange_strong(T const expected,
                                               T const desired,
                                               memory_order const success,
                                               memory_order const failure) -> bool {
        nlc_assert(failure != memory_order::release && failure != memory_order::acquire_release);
        nlc_assert(failure >= success);
        return __atomic_compare_exchange_n(&_value,
                                           const_cast<T *>(&expected),
                                           desired,
                                           false,
                                           static_cast<int>(success),
                                           static_cast<int>(failure));
    }

    [[nodiscard]] auto
    compare_exchange_weak(T const expected,
                          T const desired,
                          memory_order const order = memory_order::sequentially_consistent) -> bool {
        auto const failure_order = [&]() {
            if (order == memory_order::acquire_release)
                return memory_order::acquire;
            else if (order == memory_order::release)
                return memory_order::relaxed;
            else
                return order;
        }();
        return compare_exchange_weak(expected, desired, order, failure_order);
    }

    [[nodiscard]] auto
    compare_exchange_strong(T const expected,
                            T const desired,
                            memory_order const order = memory_order::sequentially_consistent) -> bool {
        auto const failure_order = [&]() {
            if (order == memory_order::acquire_release)
                return memory_order::acquire;
            else if (order == memory_order::release)
                return memory_order::relaxed;
            else
                return order;
        }();
        return compare_exchange_strong(expected, desired, order, failure_order);
    }

    // operation then fetch //
    [[nodiscard]] auto add_fetch(T const value,
                                 memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_add_fetch(&_value, value, static_cast<int>(mem_order));
    }

    [[nodiscard]] auto sub_fetch(T const value,
                                 memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_sub_fetch(&_value, value, static_cast<int>(mem_order));
    }

    [[nodiscard]] auto and_fetch(T const value,
                                 memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_and_fetch(&_value, value, static_cast<int>(mem_order));
    }

    [[nodiscard]] auto xor_fetch(T const value,
                                 memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_xor_fetch(&_value, value, static_cast<int>(mem_order));
    }

    [[nodiscard]] auto or_fetch(T const value,
                                memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_or_fetch(&_value, value, static_cast<int>(mem_order));
    }

    [[nodiscard]] auto
    nand_fetch(T const value,
               memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_nand_fetch(&_value, value, static_cast<int>(mem_order));
    }

    // fetch then operation //
    [[nodiscard]] auto fetch_add(T const value,
                                 memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_fetch_add(&_value, value, static_cast<int>(mem_order));
    }

    [[nodiscard]] auto fetch_sub(T const value,
                                 memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_fetch_sub(&_value, value, static_cast<int>(mem_order));
    }

    [[nodiscard]] auto fetch_and(T const value,
                                 memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_fetch_and(&_value, value, static_cast<int>(mem_order));
    }

    [[nodiscard]] auto fetch_xor(T const value,
                                 memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_fetch_xor(&_value, value, static_cast<int>(mem_order));
    }

    [[nodiscard]] auto fetch_or(T const value,
                                memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_fetch_or(&_value, value, static_cast<int>(mem_order));
    }

    [[nodiscard]] auto
    fetch_nand(T const value,
               memory_order const mem_order = memory_order::sequentially_consistent) -> T {
        return __atomic_fetch_nand(&_value, value, static_cast<int>(mem_order));
    }
};

}  // namespace nlc
