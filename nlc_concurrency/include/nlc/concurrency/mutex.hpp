/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/assert.hpp>
#include <nlc/sanity/platforms.hpp>

namespace nlc {

class [[nodiscard]] alignas(alignof(void *)) mutex final {
    friend class condition_variable;
#if NLC_OS_LINUX
    char _handle[48];
#else
    void * _handle = nullptr;
#endif

    class [[nodiscard]] scoped_lock final {
        mutex & _lock;

      public:
        explicit scoped_lock(mutex & lock)
            : _lock(lock) {
            lock._lock();
        }
        ~scoped_lock() { _lock._unlock(); }
        scoped_lock(scoped_lock &&) = delete;
    };

    class [[nodiscard]] scoped_try_lock final {
        mutex & _lock;
        bool const _is_acquired;
#ifndef NDEBUG
        mutable bool _has_been_used = false;
#endif
      public:
        explicit scoped_try_lock(mutex & lock)
            : _lock(lock)
            , _is_acquired(lock._try_lock()) {}
        ~scoped_try_lock() {
#ifndef NDEBUG
            nlc_assert(_has_been_used);
#endif
            if (_is_acquired)
                _lock._unlock();
        }

        [[nodiscard]] operator bool() const & {
#ifndef NDEBUG
            _has_been_used = true;
#endif
            return _is_acquired;
        }
        scoped_try_lock(scoped_try_lock &&) = delete;
        operator bool() const && = delete;
    };

  public:
    mutex();
    ~mutex();

    [[nodiscard]] auto lock() { return scoped_lock(*this); }
    [[nodiscard]] auto try_lock() { return scoped_try_lock(*this); }

  private:
    auto _lock() -> void;
    [[nodiscard]] auto _try_lock() -> bool;
    auto _unlock() -> void;
};

}  // namespace nlc
