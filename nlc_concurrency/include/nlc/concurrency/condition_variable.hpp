/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/sanity/platforms.hpp>

#include "mutex.hpp"

namespace nlc {

class [[nodiscard]] alignas(alignof(void *)) condition_variable final {
#if NLC_OS_LINUX
    char _handle[48];
#else
    void * _handle = nullptr;
#endif

    class scoped_lock final {
        mutex & _lock;

      public:
        explicit scoped_lock(mutex & lock)
            : _lock(lock) {}
        ~scoped_lock() { _lock._unlock(); }
        scoped_lock(scoped_lock &&) = delete;
    };

  public:
    condition_variable();
    ~condition_variable();

    template<typename F> auto wait(mutex & mtx, F && lambda) {
        mtx._lock();
        while (lambda() == false)
            _wait(mtx);
        return scoped_lock(mtx);
    }
    auto notify_one() -> void;
    auto notify_all() -> void;

  private:
    auto _wait(mutex &) -> void;
};

}  // namespace nlc
