/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/assert.hpp>
#include <nlc/sanity/platforms.hpp>

namespace nlc {

class [[nodiscard]] alignas(alignof(void *)) rw_lock final {
#if NLC_OS_LINUX
    char _handle[64];
#else
    void * _handle = nullptr;
#endif

    class [[nodiscard]] shared_scoped_lock final {
        rw_lock & _lock;

      public:
        explicit shared_scoped_lock(rw_lock & lock)
            : _lock(lock) {
            lock._lock_shared();
        }
        ~shared_scoped_lock() { _lock._unlock_shared(); }
        shared_scoped_lock(shared_scoped_lock &&) = delete;
    };

    class [[nodiscard]] shared_scoped_try_lock final {
        rw_lock & _lock;

      public:
        bool const _is_acquired;
#ifndef NDEBUG
        mutable bool _has_been_used = false;
#endif

        explicit shared_scoped_try_lock(rw_lock & lock)
            : _lock(lock)
            , _is_acquired(lock._try_lock_shared()) {}
        ~shared_scoped_try_lock() {
#ifndef NDEBUG
            nlc_assert(_has_been_used);
#endif
            if (_is_acquired)
                _lock._unlock_shared();
        }

        [[nodiscard]] operator bool() const & {
#ifndef NDEBUG
            _has_been_used = true;
#endif
            return _is_acquired;
        }
        shared_scoped_try_lock(shared_scoped_try_lock &&) = delete;
        operator bool() const && = delete;
    };

    class [[nodiscard]] exclusive_scoped_lock final {
        rw_lock & _lock;

      public:
        explicit exclusive_scoped_lock(rw_lock & lock)
            : _lock(lock) {
            lock._lock_exclusive();
        }
        ~exclusive_scoped_lock() { _lock._unlock_exclusive(); }
        exclusive_scoped_lock(exclusive_scoped_lock &&) = delete;
    };

    class [[nodiscard]] exclusive_scoped_try_lock final {
        rw_lock & _lock;
        bool const _is_acquired;
#ifndef NDEBUG
        mutable bool _has_been_used = false;
#endif
      public:
        explicit exclusive_scoped_try_lock(rw_lock & lock)
            : _lock(lock)
            , _is_acquired(lock._try_lock_exclusive()) {}
        ~exclusive_scoped_try_lock() {
#ifndef NDEBUG
            nlc_assert(_has_been_used);
#endif
            if (_is_acquired)
                _lock._unlock_exclusive();
        }

        [[nodiscard]] operator bool() const & {
#ifndef NDEBUG
            _has_been_used = true;
#endif
            return _is_acquired;
        }
        exclusive_scoped_try_lock(exclusive_scoped_try_lock &&) = delete;
        operator bool() const && = delete;
    };

  public:
    rw_lock();
    ~rw_lock();

    [[nodiscard]] auto lock_shared() { return shared_scoped_lock(*this); }
    [[nodiscard]] auto try_lock_shared() { return shared_scoped_try_lock(*this); }
    [[nodiscard]] auto lock_exclusive() { return exclusive_scoped_lock(*this); }
    [[nodiscard]] auto try_lock_exclusive() { return exclusive_scoped_try_lock(*this); }

  private:
    auto _lock_shared() -> void;
    [[nodiscard]] auto _try_lock_shared() -> bool;
    auto _unlock_shared() -> void;

    auto _lock_exclusive() -> void;
    [[nodiscard]] auto _try_lock_exclusive() -> bool;
    auto _unlock_exclusive() -> void;
};

}  // namespace nlc
