# nlc overview

This page will quickly present some concepts introduced by nlc. Its goal is to provide an overview of the library and make it easy to understand how the subprojects are articulated.

[[_TOC_]]

# Asserts

Asserts are made via the `nlc_assert(COND)` and `nlc_asert_msg(COND, ...)` macros, defined in [`<nlc/dialect/assert.hpp>`](nlc_dialect/include/nlc/dialect/assert.hpp). `nlc_assert_msg` allows to append some text to the assert output, using the same format as `nlc::print` and `nlc::warn` (see [the IO section](#io)). It is also possible to add a variable's name and value using `nlc_dump_var(X)`.

# IO

[`<nlc/dialect/basic_output.hpp>`](nlc_dialect/include/nlc/dialect/basic_output.hpp) provides 2 functions to append stuff to the standard outputs: `nlc::print` for `stdout` and `nlc::warn` for `stderr`. They can take as many arguments as you want of any type that is compatible and will concatenate them.

To read from / write to files, you may use `nlc::read_file` and `nlc::write_file`, defined in [`<nlc/fundamentals/read_write_file.hpp>`](nlc_fundamentals/include/nlc/fundamentals/read_write_file.hpp).

# Memory handling

Using `new` and `delete` is prohibited. You should use a `nlc::allocator`, an abstract class defined in [`<nlc/fundamentals/allocator.hpp>`](nlc_fundamentals/include/nlc/fundamentals/allocator.hpp). A basic implementation can be found in [`<nlc/fundamentals/standard_allocator.hpp>`](nlc_fundamentals/include/nlc/fundamentals/mallocator.hpp): `nlc::standard_allocator`. This allocator uses standard allocation (`malloc` / `realloc` / `free`) and keeps count of allocatad memory blocks in debug.

[`<nlc/dialect/memory.hpp>`](nlc_dialect/include/nlc/dialect/memory.hpp) defines `nlc::memmove`, `nlc::memcpy` and `nlc::memset`, as well as `nlc::set_memory`, which allows to repeatedly write small blocks (the size of any integer type) of a given value in a memory segment.

nlc also provides the `nlc::unique_ptr` class through [`<nlc/fundalements/unique_ptr.hpp>`](nlc_fundamentals/include/nlc/fundamentals/unique_ptr.hpp). Its behaviour is equivalent to `std::unique_ptr`'s.

# Fixed-size artihmetic types

These types are defined in [`<nlc/dialect/arithmetic_types.hpp>`](nlc_dialect/include/nlc/dialect/arithmetic_types.hpp).

| size (bytes) | signed integer | unsigned integer | floating point |
| :----------- | :------------- | :--------------- | :------------- |
| 1            | `i8`           | `u8`             | N/A            |
| 2            | `i16`          | `u16`            | N/A            |
| 4            | `i32`          | `u32`            | `f32`          |
| 8            | `i64`          | `u64`            | `f64`          |

The header also defines `isize` and `usize` whose size is 8 bytes, and `byte` which is an unsigned integer on 8 bits.

# Strings

String are always considered UTF8-encoded in nlc. [`<nlc/dialect/utf8.hpp>`](nlc_dialect/include/nlc/dialect/utf8.hpp) defines some basic functions to manage UTF8 string, such as `nlc::str_len` to get the number of codepoints in the string or `nlc::str_size` to get its size in bytes.

`nlc::string` is a **constant** storage for a null-terminated string. It owns the underlying memory. `nlc::string_view` is a full or partial view of a string and thus doesn't own the underlying memory. It doesn't necessarily end with `\0`. Use `nlc::string_iterator` to iterate on strings or string views by codepoints.

To dynamically build a string, use a `nlc::string_stream`. This object owns the string memory and is null-terminated. You can convert a string from / to an arithmetic types using functions defined in [`<nlc/fundamentals/from_string.hpp>`](nlc_fundamentals/fundamentals/include/nlc/fundamentals/from_string.hpp) and [`<nlc/fundamentals/to_string.hpp>`](nlc_fundamentals/fundamentals/include/nlc/fundamentals/to_string.hpp). [`<nlc/fundamentals/string_manip.hpp>`](nlc_fundamentals/fundamentals/include/nlc/fundamentals/string_manip.hpp) defines some algorithms on strings such as `nlc::begins_with`, `nlc::find`, `nlc::contains`, ...

| class                  | header                                                                                                    |
| :--------------------- | :-------------------------------------------------------------------------------------------------------- |
| `nlc::string`          | [`<nlc/fundamentals/string.hpp>`](nlc_fundamentals/fundamentals/include/nlc/fundamentals/from_string.hpp) |
| `nlc::string_view`     | [`<nlc/dialect/string_view.hpp>`](nlc_dialect/include/nlc/dialect/string_view.hpp)                        |
| `nlc::string_iterator` | [`<nlc/dialect/string_iterator.hpp>`](nlc_dialect/include/nlc/dialect/string_iterator.hpp)                |
| `nlc::string_stream`   | [`<nlc/dialect/string_stream.hpp>`](nlc_dialect/include/nlc/dialect/string_stream.hpp)                    |

# Containers

## Arrays

nlc provides a variety of array implementations to fullfil different needs.

| class                    | header                                                                                                  | is contiguous      | embeds allocator*  | is resizable       | handles objects lifespan* | is stable in memory* | allows doublons    | comment                                                                                                  |
| :----------------------- | :------------------------------------------------------------------------------------------------------ | :----------------- | :----------------- | :----------------- | :------------------------ | :------------------- | :----------------- | :------------------------------------------------------------------------------------------------------- |
| `nlc::buffer`            | [`<nlc/dialect/buffer.hpp>`](nlc_dialect/include/nlc/dialect/buffer.hpp)                                | :heavy_check_mark: | :x:                | :x:                | :x:                       | :heavy_check_mark:   | :heavy_check_mark: | Similar to `std::array`                                                                                  |
| `nlc::vector`            | [`<nlc/containers/vector.hpp>`](nlc_containers/include/nlc/containers/vector.hpp)                       | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark:        | :x:                  | :heavy_check_mark: | Most commonly used container, similar to `std::vector`                                                   |
| `nlc::light_vector`      | [`<nlc/containers/light_vector.hpp>`](nlc_containers/include/nlc/containers/light_vector.hpp)           | :heavy_check_mark: | :x:                | :heavy_check_mark: | :heavy_check_mark:        | :x:                  | :heavy_check_mark: | Useful when working with a lot of small vectors                                                          |
| `nlc::monolithic_vector` | [`<nlc/containers/monolithic_vector.hpp>`](nlc_containers/include/nlc/containers/monolithic_vector.hpp) | :heavy_check_mark: | :x:                | :heavy_check_mark: | :heavy_check_mark:        | :heavy_check_mark:   | :heavy_check_mark: | Useful when you want a really big array or when you want the objects to be stable in memory              |
| `nlc::ring_buffer`       | [`<nlc/containers/ring_buffer.hpp>`](nlc_containers/include/nlc/containers/ring_buffer.hpp)             | ~                  | :x:                | :x:                | :heavy_check_mark:        | :heavy_check_mark:   | :heavy_check_mark: | Standard ring buffer implementation. The internal memory is contiguous but the iteration may "loop" back |
| `nlc::hashset`           | [`<nlc/containers/hashset.hpp>`](nlc_containers/include/nlc/containers/hashset.hpp)                     | :x:                | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark:        | :x:                  | :x:                | Stored objects must be hashable (see [here](#conteneurs-associatifs))                                    |

**\*embeds allocator:** this means that the container stores a pointer to a memory allocator. It eases operations on dynamic allocation but increases the container's size.

**\*handles objects lifespan:** this means that the container will call the contained object's constructor and destructor for you.

**\*stable memory:** this means that contained object will not be moved byt the container, e.g. a pointer / iterator is never invalidated.


The `nlc::span` class, defined in [`<nlc/dialect/span.hpp>`](nlc_dialect/include/nlc/dialect/span.hpp) is a view on contiguous objects in memory. It allows container type abstraction.

## Associative containers

The only association container of nlc is `nlc::hashmap`, defined in [`<nlc/containers/hashmap.hpp>`](nlc_containers/include/nlc/containers/hashmap.hpp). Key types must be hashable, meaning that they must define a `nlc::hash_result T::hash()` method or there must be a `nlc::hash_result hash(T)` free function declared in the namespace of `T`. Basic types are natively supported. You can use already existing hash methods by including [`<nlc/fundamentals/hash.hpp>`](nlc_fundamentals/fundamentals/include/nlc/fundamentals/hash.hpp).

# Metaprogramming

All metaprogramming concepts are encapsulated in the `nlc_meta` library. A more detailed documentation can be found [here](nlc_meta/README.md).

This library provides amongst other features:
* **type lists** through header [`<nlc/meta/list.hpp>`](nlc_meta/include/nlc/meta/list.hpp) and files in [`nlc/meta/list/`](nlc_meta/include/nlc/meta/list)
* **type values** through header [`<nlc/meta/type_value.hpp>`](nlc_meta/include/nlc/meta/type_value.hpp)
* **strongly typed identifiers** that can have an invalid value and/or a default value through header [`<nlc/meta/id.hpp>`](nlc_meta/include/nlc/meta/id.hpp)
* **dimension-typed units**, very useful to avoid errors in physics computation, space change, etc through header [`<nlc/meta/units.hpp>`](nlc_meta/include/nlc/meta/units.hpp)
* support for a lot of **STL features**

# Callables

There are two classes allowing callable encapsulation: `nlc::function` et `nlc::function_view`. `nlc::function` owns the memory that may be needed for a lambda capture, whereas `nlc::function_view` is just a view on the callable and therefore does not own this kind of memory.

| class                | header                                                                                                   |
| :------------------- | :------------------------------------------------------------------------------------------------------- |
| `nlc::function`      | [`<nlc/fundamentals/function.hpp>`](nlc_fundamentals/fundamentals/include/nlc/fundamentals/function.hpp) |
| `nlc::function_view` | [`<nlc/dialect/function_view.hpp>`](nlc_dialect/include/nlc/dialect/function_view.hpp)                   |

# Utilities

## Scope handling

The `defer` macro, that behaves as a keyword, may be used to execute a code block at the end of the current scope. It is defined in [`<nlc/dialect/defer.hpp>`](nlc_dialect/include/nlc/dialect/defer.hpp).

Template class `nlc::scoped`, defined in [`<nlc/dialect/scoped.hpp>`](nlc_dialect/include/nlc/dialect/scoped.hpp), can be used to define a custom destructor on an object. It also allows to expand an object's scope by giving the control of the object's lifespan to the user.

## Error handling

:warning: :hammer: WIP
* [`<nlc/dialect/expected.hpp>`](nlc_dialect/include/nlc/dialect/expected.hpp)

## Random
:warning: :hammer: WIP
 * [`<nlc/fundamentals/random.hpp>`](nlc_fundamentals/fundamentals/include/nlc/fundamentals/random.hpp)

## Time
:warning: :hammer: WIP
* [`<nlc/fundamentals/time.hpp>`](nlc_fundamentals/fundamentals/include/nlc/fundamentals/time.hpp)

## Sorts
:warning: :hammer: WIP
* [`<nlc/algo/sort.hpp>`](nlc_fundamentals/algo/include/nlc/algo/sort.hpp)
* [`<nlc/algo/radix_sort.hpp>`](nlc_fundamentals/algo/include/nlc/algo/radix_sort.hpp)
* [`<nlc/algo/predicates.hpp>`](nlc_fundamentals/algo/include/nlc/algo/predicates.hpp)
* [`<nlc/algo/is_sorted.hpp>`](nlc_fundamentals/algo/include/nlc/algo/is_sorted.hpp)
* [`<nlc/algo/for_each_sorted.hpp>`](nlc_fundamentals/algo/include/nlc/algo/for_each_sorted.hpp)

## Range
:warning: :hammer: WIP
* [`<nlc/dialect/range.hpp>`](nlc_dialect/include/nlc/dialect/range.hpp)

## Non-null pointer
:warning: :hammer: WIP
* [`<nlc/dialect/pointer.hpp>`](nlc_dialect/include/nlc/dialect/pointer.hpp)
