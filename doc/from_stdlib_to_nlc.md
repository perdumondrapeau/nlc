# From the standard library to nlc

For an explanation of why we started `nlc` instead of using the standard library, [see the readme](README.md).

In this section you will find an as exhaustive as possible list of nlc equivalents to standard symbols. Be cautious, even though they behave mainly the same, we sometimes introduced slightly different behaviours. Please refer to the corresponding library documentation for more details.

[[_TOC_]]

## Strings

| standard symbol(s)       | nlc symbol(s)        | nlc include(s)                                                                                            |
| :------------------ | :------------------- | :-------------------------------------------------------------------------------------------------------- |
| `std::string`       | `nlc::string`        | [`<nlc/fundamentals/string.hpp>`](nlc_fundamentals/fundamentals/include/nlc/fundamentals/from_string.hpp) |
| `std::string_view`  | `nlc::string_view`   | [`<nlc/dialect/string_view.hpp>`](nlc_dialect/include/nlc/dialect/string_view.hpp)                        |
| `std::stringstream` | `nlc::string_stream` | [`<nlc/dialect/string_stream.hpp>`](nlc_dialect/include/nlc/dialect/string_stream.hpp)                    |

## Containers

| standard symbol(s)                            | nlc symbol(s)  | nlc include(s)                                                                      |
| :--------------------------------------- | :------------- | :---------------------------------------------------------------------------------- |
| `std::array`                             | `nlc::buffer`  | [`<nlc/dialect/buffer.hpp>`](nlc_dialect/include/nlc/dialect/buffer.hpp)            |
| `std::vector`                            | `nlc::vector`  | [`<nlc/containers/vector.hpp>`](nlc_containers/include/nlc/containers/vector.hpp)   |
| `std::set`, `std::unordered_set`         | `nlc::hashset` | [`<nlc/containers/hashset.hpp>`](nlc_containers/include/nlc/containers/hashset.hpp) |
| `std::hashmap`, `std::unordered_hashmap` | `nlc::hashmap` | [`<nlc/containers/hashmap.hpp>`](nlc_containers/include/nlc/containers/hashmap.hpp) |

## Callables

| standard symbol(s)   | nlc symbol(s)                         | nlc include(s)                                                                                                                                                                                   |
| :-------------- | :------------------------------------ | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `std::function` | `nlc::function`, `nlc::function_view` | [`<nlc/fundamentals/function.hpp>`](nlc_fundamentals/fundamentals/include/nlc/fundamentals/function.hpp), [`<nlc/dialect/function_view.hpp>`](nlc_dialect/include/nlc/dialect/function_view.hpp) |

## Wrappers

| standard symbol(s)     | nlc symbol(s)     | nlc include(s)                                                                                               |
| :---------------- | :---------------- | :----------------------------------------------------------------------------------------------------------- |
| `std::optional`   | `nlc::optional`   | [`<nlc/dialect/optional.hpp>`](nlc_dialect/include/nlc/dialect/optional.hpp)                                 |
| `std::pair`       | `nlc::pair`       | [`<nlc/dialect/pair.hpp>`](nlc_dialect/include/nlc/dialect/pair.hpp)                                         |
| `std::tuple`      | `nlc::tuple`      | [`<nlc/dialect/tuple.hpp>`](nlc_dialect/include/nlc/dialect/tuple.hpp)                                       |
| `std::variant`    | `nlc::variant`    | [`<nlc/dialect/variant.hpp>`](nlc_dialect/include/nlc/dialect/variant.hpp)                                   |
| `std::unique_ptr` | `nlc::unique_ptr` | [`<nlc/fundalements/unique_ptr.hpp>`](nlc_fundamentals/fundamentals/include/nlc/fundamentals/unique_ptr.hpp) |

## Algorithms

| standard symbol(s)                 | nlc symbol(s)                  | nlc include(s)                                                             |
| :---------------------------- | :----------------------------- | :------------------------------------------------------------------------- |
| `std::count`, `std::count_if` | `nlc::count`,  `nlc::count_if` | [`<nlc/algo/count.hpp>`](nlc_fundamentals/algo/include/nlc/algo/count.hpp) |
| `std::find`, `std::find_if`   | `nlc::find`, `nlc::find_if`    | [`<nlc/algo/find.hpp>`](nlc_fundamentals/algo/include/nlc/algo/find.hpp)   |
| `std::swap`                   | `nlc::swap`                    | [`<nlc/algo/swap.hpp>`](nlc_fundamentals/algo/include/nlc/algo/swap.hpp)   |
| `std::sort`                   | `nlc::sort`                    | [`<nlc/algo/sort.hpp>`](nlc_fundamentals/algo/include/nlc/algo/sort.hpp)   |

## Metaprogramming

| standard symbol(s)                              | nlc symbol(s)                                  | nlc include(s)                                                                  |
| :----------------------------------------- | :--------------------------------------------- | :------------------------------------------------------------------------------ |
| `std::move`                                | `nlc::move`                                    | [`<nlc/meta/basics.hpp>`](nlc_meta/include/nlc/meta/basics.hpp)                 |
| `std::forward`                             | `nlc::forward`, `nlc_fwd`                      | [`<nlc/meta/basics.hpp>`](nlc_meta/include/nlc/meta/basics.hpp)                 |
| `std::declval`                             | `nlc::declval`                                 | [`<nlc/meta/basics.hpp>`](nlc_meta/include/nlc/meta/basics.hpp)                 |
| `std::enable_if_t`                         | `nlc::meta::enable_if`                         | [`<nlc/meta/enable_if.hpp>`](nlc_meta/include/nlc/meta/enable_if.hpp)           |
| `std::is_same_v`                           | `nlc::meta::is_same`                           | [`<nlc/meta/is_same.hpp>`](nlc_meta/include/nlc/meta/is_same.hpp)               |
| `std::numeric_limits`                      | `nlc::meta::limits`                            | [`<nlc/meta/numeric_limits.hpp>`](nlc_meta/include/nlc/meta/numeric_limits.hpp) |
| `std::is_lvalue_reference_v`               | `nlc::meta::is_lref`                           | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_rvalue_reference_v`               | `nlc::meta::is_rref`                           | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_reference_v`                      | `nlc::meta::is_ref`                            | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::remove_reference_t`                  | `nlc::meta::rm_ref`                            | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::add_lvalue_reference_t`              | `nlc::meta::add_lref`                          | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::add_rvalue_reference_t`              | `nlc::meta::add_rref`                          | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_pointer_v`                        | `nlc::meta::is_ptr`                            | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::add_pointer_t`                       | `nlc::meta::add_ptr`                           | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_volatile_v`                       | `nlc::meta::is_volatile`                       | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::remove_volatile_t`                   | `nlc::meta::rm_volatile`                       | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::add_volatile_t`                      | `nlc::meta::add_volatile`                      | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_const_v`                          | `nlc::meta::is_const`                          | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::remove_const_t`                      | `nlc::meta::rm_const`                          | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::add_const_t`                         | `nlc::meta::add_const`                         | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::remove_cv_t`                         | `nlc::meta::rm_const_volatile`                 | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::add_cv_t`                            | `nlc::meta::add_const_volatile`                | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::has_virtual_destructor_v`            | `nlc::meta::has_virtual_destructor`            | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_array_v`                          | `nlc::meta::is_array`                          | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_empty_v`                          | `nlc::meta::is_empty_type`                     | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_trivially_copyable_v`             | `nlc::meta::is_trivially_copyable`             | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_trivially_destructible_v`         | `nlc::meta::is_trivially_destructible`         | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_enum_v`                           | `nlc::meta::is_enum`                           | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_union_v`                          | `nlc::meta::is_union`                          | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_class_v`                          | `nlc::meta::is_class`                          | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::underlying_type_t`                   | `nlc::meta::underlying_type`                   | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_integral_v`                       | `nlc::meta::is_integral`                       | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_unsigned_v`                       | `nlc::meta::is_unsigned_integer`               | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_signed_v`                         | `nlc::meta::is_signed_integer`                 | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::numeric_limits<T>::is_integer`       | `nlc::meta::is_integer`                        | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::make_signed_t`                       | `nlc::meta::make_signed`                       | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::make_unsigned_t`                     | `nlc::meta::make_unsigned`                     | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_floating_point_v`                 | `nlc::meta::is_floating`                       | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_arithmetic_v`                     | `nlc::meta::is_arithmetic`                     | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_polymorphic_v`                    | `nlc::meta::is_polymorphic`                    | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::has_unique_object_representations_v` | `nlc::meta::has_unique_object_representations` | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
| `std::is_constructible_v`                  | `nlc::meta::is_constructible`                  | [`<nlc/meta/traits.hpp>`](nlc_meta/include/nlc/meta/traits.hpp)                 |
