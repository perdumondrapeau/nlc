/* Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/expected.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/fundamentals/time.hpp>

#include "errors.hpp"

namespace nlc {
class allocator;
class string_view;
}  // namespace nlc

namespace nlc {
using set_current_dir_result =
    expected<success, permission_denied, loop_or_too_many_symlinks, name_too_long, path_does_not_exist, invalid_parameter, unknown_error>;
auto set_current_directory(string_view const path) -> set_current_dir_result;

using get_current_dir_result =
    expected<string, permission_denied, name_too_long, not_enough_memory, internal_error, unknown_error>;
auto get_current_directory(allocator & allocator) -> get_current_dir_result;

using does_file_exist_result =
    expected<bool, permission_denied, loop_or_too_many_symlinks, name_too_long, not_enough_memory, internal_error, unknown_error>;
auto does_file_exist(string_view const path) -> does_file_exist_result;

using get_files_in_directory_result = expected<vector<string>,
                                               permission_denied,
                                               too_many_filedescriptors_open,
                                               loop_or_too_many_symlinks,
                                               name_too_long,
                                               path_does_not_exist,
                                               not_enough_memory,
                                               internal_error,
                                               unknown_error>;
auto get_files_in_directory(allocator & allocator,
                            string_view const path,
                            usize depth = 0) -> get_files_in_directory_result;

using get_file_modification_time_result = expected<nlc::time::duration,
                                                   permission_denied,
                                                   loop_or_too_many_symlinks,
                                                   name_too_long,
                                                   path_does_not_exist,
                                                   not_enough_memory,
                                                   internal_error,
                                                   unknown_error>;
auto get_file_modification_time(string_view const path) -> get_file_modification_time_result;
}  // namespace nlc
