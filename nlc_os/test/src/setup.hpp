/* Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/expected.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/os/file_system.hpp>
#include <nlc/test.hpp>

inline auto set_working_dir(nlc::string_view const dir) noexcept -> void {
    auto const success = nlc::set_current_directory(dir);
#ifdef NDEBUG
    nlc_check_msg(success.succeeded(), "couldn't set current directory to '", dir.ptr(), "'");
#else
    nlc_check_msg(success.succeeded(),
                  "couldn't set current directory to '",
                  dir.ptr(),
                  "': ",
                  success.get_error_name().ptr());
#endif
}
