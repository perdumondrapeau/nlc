/* Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/utf8.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/string_manip.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/os/file_system.hpp>
#include <nlc/test.hpp>

nlc_test("get current directory") {
    nlc::standard_allocator allocator;
    auto const cwd = nlc::get_current_directory(allocator);
    nlc_check(cwd.succeeded());
    nlc_check(cwd.get().is_empty() == false);
}

nlc_test("set current directory") {
    nlc::standard_allocator allocator;
    auto const previous_cwd = nlc::get_current_directory(allocator);
    nlc_check(previous_cwd.succeeded());
    nlc::set_current_directory("../");
    auto const new_cwd = nlc::get_current_directory(allocator);
    nlc_check(new_cwd.succeeded());

    nlc_check(nlc::str_cmp(previous_cwd.get().c_str(), new_cwd.get().c_str()) ==
              nlc::comparison_result::more);
    nlc_check(nlc::starts_with(previous_cwd.get(), new_cwd.get()));
}
