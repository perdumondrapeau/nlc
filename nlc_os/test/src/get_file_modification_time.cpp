/* Copyright © 2020-2023
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/read_write_file.hpp>
#include <nlc/fundamentals/time.hpp>
#include <nlc/os/file_system.hpp>
#include <nlc/test.hpp>

#include <stdio.h>

#include "setup.hpp"

constexpr nlc::string_view filename = "data/modification_time.txt";
constexpr nlc::string_view content = "test content";

nlc_test("setup") { set_working_dir(WORK_DIR); }

nlc_test("path not found") {
    auto t = nlc::get_file_modification_time("non-existant");
    nlc_check(t.failed_with<nlc::path_does_not_exist>());
}

nlc_test("get_file_modification_time") {
    nlc::write_file(filename, { reinterpret_cast<byte const *>(content.ptr()), content.size().v });
    auto t1 = nlc::get_file_modification_time(filename);
    nlc_check(t1.succeeded());
    nlc_check(t1.get() > 1690574424_s);  // time of writing

    auto t2 = nlc::get_file_modification_time(filename);
    nlc_check(t2.succeeded());
    nlc_check(t1.get() == t2.get());

    // This is unreliable because of the filesystem
    // usleep(10);
    // nlc::write_file(filename, { reinterpret_cast<byte const *>(content.ptr()), content.size().v
    // });

    // auto t3 = nlc::get_file_modification_time(filename);
    // nlc_check(t3.succeeded());
    // nlc_check_msg(t3.get() > t2.get(), "t1 ", t1.get().v, " t2 ", t2.get().v, " t3 ",
    // t3.get().v);

    remove(filename.ptr());
}
