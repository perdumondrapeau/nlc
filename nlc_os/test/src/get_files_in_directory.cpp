/* Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/find.hpp>
#include <nlc/dialect/expected.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/os/errors.hpp>
#include <nlc/os/file_system.hpp>
#include <nlc/test.hpp>

#include "setup.hpp"

using namespace nlc;

static auto check_directory_content(allocator & allocator,
                                    string_view const path,
                                    span<string_view const> const expected_files,
                                    usize depth = 0) -> void {
    auto const res = get_files_in_directory(allocator, string { allocator, path }, depth);
#ifdef NDEBUG
    nlc_check_msg(res.succeeded(), "Couldn't get files in '", path.ptr(), "'");
#else
    nlc_check_msg(res.succeeded(),
                  "Couldn't get files in '",
                  path.ptr(),
                  "': ",
                  res.get_error_name().ptr());
#endif

    if (res.get().size() != expected_files.size()) {
#define view(s) string_view::from_c_str(s)
        string_stream stream { allocator };
        stream << view("got ") << res.get().size() << view(" files, expected ")
               << expected_files.size() << view("\n");
        stream << view("found files:\n");
        for (auto const & file : res.get())
            stream << view("- ") << file << view("\n");
        stream << view("expected files:\n");
        for (auto const & file : expected_files)
            stream << view("- ") << file << view("\n");
#undef conv

        nlc_check_msg(res.get().size() == expected_files.size(), stream.c_str());
    }

    for (auto const & found_file : res.get()) {
        auto const found = find<string_view const>(expected_files, found_file);
        nlc_check_msg(found != nullptr, "file '", found_file.c_str(), "' was found but not expected");
    }
}

nlc_test("setup") { set_working_dir(WORK_DIR); }

nlc_test("empty string is not a folder") {
    standard_allocator allocator;

    auto const res = get_files_in_directory(allocator, string::from_c_str(allocator, ""));
    nlc_check(res.failed_with<path_does_not_exist>());
}

nlc_test("simple paths") {
    standard_allocator allocator;

    static string_view const data_files[] = { "file1.txt" };
    check_directory_content(allocator, "data", data_files);

    static string_view const folder1_files[] = { ".hidden", "file2" };
    check_directory_content(allocator, "data/folder1", folder1_files);
}

nlc_test("complex paths") {
    standard_allocator allocator;

    static string_view const files1[] = { "file1.txt" };
    check_directory_content(allocator, "./data/", files1);

    static string_view const files2[] = { "file1.txt" };
    check_directory_content(allocator, "./data/../data/", files2);

    static string_view const files3[] = { "file1.txt" };
    check_directory_content(allocator, "data/../data/folder1/../../data/", files3);

    static string_view const files4[] = { "file3.exe", "file4.json" };
    check_directory_content(allocator, "data/folder3/folder4/folder5/folder6/../../../", files4);
}

nlc_test("depth variations") {
    standard_allocator allocator;

    static string_view const files_before_depth_3[] = { "file3.exe", "file4.json" };
    static string_view const files_from_depth_3[] = { "file3.exe", "file4.json", "deep_file.net" };

    for (auto depth = 0u; depth < 5u; ++depth) {
        auto const files =
            depth < 3u ? make_span(files_before_depth_3) : make_span(files_from_depth_3);
        check_directory_content(allocator, "data/folder3", files, depth);
    }
}

nlc_test("get everything") {
    standard_allocator allocator;

    static string_view const all_the_files[] = { ".hidden",    "file2",     "file3.exe",
                                                 "file4.json", "file1.txt", "deep_file.net" };
    check_directory_content(allocator, "data", all_the_files, 1024u);
}
