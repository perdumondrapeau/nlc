/* Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/expected.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/os/file_system.hpp>
#include <nlc/test.hpp>

#include "setup.hpp"

using namespace nlc;

static auto check_does_file_exist_impl(allocator & allocator,
                                       char const * const file,
                                       bool const should_exist,
                                       char const * const msg) {
    auto const res = does_file_exist(string::from_c_str(allocator, file));
#ifdef NDEBUG
    nlc_check(res.succeeded());
#else
    nlc_check_msg(res.succeeded(), res.get_error_name().ptr());
#endif
    nlc_check_msg(res.get() == should_exist, "file '", file, "' ", msg);
}

static auto check_does_file_exist(allocator & allocator, char const * const file) {
    check_does_file_exist_impl(allocator, file, true, "doesn't exist but it should");
}

static auto check_file_does_not_exist(allocator & allocator, char const * const file) {
    check_does_file_exist_impl(allocator, file, false, "exists but it shouldn't");
}

nlc_test("setup") { set_working_dir(WORK_DIR); }

nlc_test("the empty string is not a file") {
    standard_allocator allocator;
    check_file_does_not_exist(allocator, "");
}

nlc_test("a folder is not a file") {
    standard_allocator allocator;
    check_file_does_not_exist(allocator, "data");
}

nlc_test("hidden file exists") {
    standard_allocator allocator;
    check_does_file_exist(allocator, "data/folder1/.hidden");
}

nlc_test("files in subfolders") {
    standard_allocator allocator;

    check_does_file_exist(allocator, "data/file1.txt");
    check_does_file_exist(allocator, "data/folder1/file2");
    check_does_file_exist(allocator, "data/folder3/file3.exe");
    check_does_file_exist(allocator, "data/folder3/folder4/folder5/folder6/deep_file.net");
}

nlc_test("complex paths") {
    standard_allocator allocator;

    check_does_file_exist(allocator, "./data/file1.txt");
    check_does_file_exist(allocator, "./data/../data/file1.txt");
    check_does_file_exist(allocator, "data/../data/folder1/../../data/file1.txt");
    check_does_file_exist(allocator, "data/folder3/folder4/folder5/folder6/../../../file3.exe");
}

nlc_test("almost valid paths") {
    standard_allocator allocator;

    check_file_does_not_exist(allocator, "data/file.txt");
    check_file_does_not_exist(allocator, "data/folder3/file3");
    check_file_does_not_exist(allocator, "folder3/file4.json");
    check_file_does_not_exist(allocator, "/data/folder3/file4.json");
}
