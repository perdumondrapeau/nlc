/* Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/scoped.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/units.hpp>

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <Windows.h>
#include <direct.h>
#include <errno.h>
#include <limits.h>

#include "file_system.hpp"
#include "file_system_common.hpp"

#define static_str_from_view(view, string)

static auto get_error_str(u64 const error_code) -> nlc::scoped<char *> {
    LPVOID error_msg_buffer { nullptr };

    [[maybe_unused]] auto const size =
        FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
                          FORMAT_MESSAGE_IGNORE_INSERTS,
                      nullptr,
                      static_cast<DWORD>(error_code),
                      MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                      reinterpret_cast<LPTSTR>(&error_msg_buffer),
                      0,
                      nullptr);
    nlc_assert_msg(size > 0u, "get_last_error_str: FormatMessageFailed for error code ", error_code);

    return { reinterpret_cast<char *>(error_msg_buffer), [](char * ptr) {
                [[maybe_unused]] auto const res = LocalFree(ptr);
                nlc_assert(res == nullptr);
            } };
}

static auto is_folder(WIN32_FIND_DATAA const & find_data) -> bool {
    return (find_data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
}

static auto get_files_in_directory(nlc::allocator & allocator,
                                   nlc::string_view const path,
                                   usize depth,
                                   nlc::vector<nlc::string> & file_names)
    -> nlc::expected<void, nlc::path_does_not_exist> {
    nlc::vector<nlc::string> folders_to_explore { allocator };
    folders_to_explore.append(allocator, path);

    while (folders_to_explore.is_empty() == false) {
        nlc::vector<nlc::string> next_folders_to_explore { allocator };

        for (auto const & folder : folders_to_explore) {
            nlc::string_stream search_path { allocator };
            concat_strings(search_path, folder, "/*");  // Yes, this is mandatory ...

            WIN32_FIND_DATAA find_data;
            HANDLE const handle = FindFirstFileA(search_path.c_str(), &find_data);
            if (handle == INVALID_HANDLE_VALUE)
                return nlc::path_does_not_exist {};

            while (FindNextFileA(handle, &find_data) != 0) {
                auto found_path = nlc::string::from_c_str(allocator, find_data.cFileName);

                if (is_folder(find_data) == false)
                    file_names.append(nlc_move(found_path));
                else if (depth > 0u && is_special_folder(found_path) == false) {
                    auto full_path = concat_strings(allocator, folder, "/", found_path);
                    next_folders_to_explore.append(nlc_move(full_path));
                }
            }
        }

        if (depth == 0u)
            break;
        --depth;
        folders_to_explore = nlc_move(next_folders_to_explore);
    }

    return {};
}

namespace nlc {
auto set_current_directory(string_view const path) -> set_current_dir_result {
    if (_chdir(path.ptr()) == 0)
        return success {};

    switch (errno) {
        case ENOENT: return path_does_not_exist {};
        case EINVAL: return invalid_parameter {};
        default: return unknown_error {};
    }
}

auto get_current_directory(allocator & allocator) -> get_current_dir_result {
    char buff[MAX_PATH + 1];
    char const * const result = _getcwd(buff, MAX_PATH + 1);

    if (result == nullptr) {
        switch (errno) {
            case ENOMEM: return not_enough_memory {};
            case ERANGE: return internal_error {};
            default: return unknown_error {};
        }
    }

    return string::from_c_str(allocator, result);
}

auto does_file_exist(nlc::string_view const path) -> does_file_exist_result {
    CHAR buffer[MAX_PATH];
    make_null_terminated(path, buffer);

    WIN32_FIND_DATA find_data;
    HANDLE const handle = FindFirstFile(buffer, &find_data);
    bool const found = handle != INVALID_HANDLE_VALUE && is_folder(find_data) == false;
    if (found) {
        auto const close_res = FindClose(handle);
        if (close_res == false) {
            auto const error_code = static_cast<u64>(GetLastError());
            nlc::warn("does_file_exist: FindClose() failed with error code ",
                      error_code,
                      ": ",
                      *get_error_str(error_code));
            return internal_error {};
        }
    }

    return found;
}

auto get_files_in_directory(nlc::allocator & allocator,
                            nlc::string_view const path,
                            usize depth) -> get_files_in_directory_result {
    nlc::vector<nlc::string> file_names { allocator };

    // As we must add '/*' to the path later, an empty path would correspond to the disk
    // root. We don't want that so we abort here.
    if (path.is_empty())
        return path_does_not_exist {};

    auto const res = ::get_files_in_directory(allocator, path, depth, file_names);
    if (res.failed())
        return res.forward_error();

    return file_names;
}

auto get_file_modification_time(string_view const path) -> get_file_modification_time_result {
    CHAR buffer[MAX_PATH];
    make_null_terminated(path, buffer);

    HANDLE file =
        CreateFile(buffer, GENERIC_READ, FILE_SHARE_READ, nullptr, OPEN_EXISTING, 0, nullptr);
    if (file == INVALID_HANDLE_VALUE)
        return path_does_not_exist {};
    defer { CloseHandle(file); };

    FILETIME time;

    if (!GetFileTime(file, nullptr, nullptr, &time)) {
        auto const error_code = static_cast<u64>(GetLastError());
        nlc::warn("get_file_modification_time: GetFileTime() failed with error code ",
                  error_code,
                  ": ",
                  *get_error_str(error_code));
        return internal_error {};
    }

    constexpr auto windows_to_unix_epoch = 11644473600_s;

    auto const ns = (u64(time.dwHighDateTime) << 32 | time.dwLowDateTime) * 100;
    return nlc::time::from_nanoseconds(ns) - windows_to_unix_epoch;
}
}  // namespace nlc
