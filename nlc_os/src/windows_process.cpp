/* Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>

#include "process.hpp"

#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <Windows.h>
#include <shellapi.h>

namespace nlc {
auto is_process_elevated() -> bool {
    bool res = false;
    HANDLE token = nullptr;

    if (OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &token)) {
        TOKEN_ELEVATION Elevation;
        DWORD size = sizeof(TOKEN_ELEVATION);
        if (GetTokenInformation(token, TokenElevation, &Elevation, sizeof(Elevation), &size))
            res = Elevation.TokenIsElevated;
    }

    if (token)
        CloseHandle(token);
    return res;
}

auto restart_process_elevated() -> void {
    char filePath[2048];
    GetModuleFileNameA(nullptr, filePath, 2048);
    auto const commandLine = GetCommandLineA();

    HANDLE const child =
        ShellExecuteA(nullptr, "runas", filePath, commandLine, nullptr, SW_SHOWNORMAL);
    if (reinterpret_cast<isize>(child) > 32)  // MSDN says so
    {
        CloseHandle(child);
        ExitProcess(0);
    }
}
}  // namespace nlc
