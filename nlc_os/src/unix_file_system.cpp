/* Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/containers/vector.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/string_view.hpp>

#include <dirent.h>
#include <errno.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#if NLC_OS_LINUX
#    include <linux/limits.h>
#endif

#include "file_system.hpp"
#include "file_system_common.hpp"

#define static_str_from_view(view, string) \
    char string[PATH_MAX];                 \
    nlc::make_null_terminated(view, string)

namespace {
template<typename T>
using stat_result = nlc::expected<T,
                                  nlc::permission_denied,
                                  nlc::loop_or_too_many_symlinks,
                                  nlc::name_too_long,
                                  nlc::path_does_not_exist,
                                  nlc::not_enough_memory,
                                  nlc::internal_error,
                                  nlc::unknown_error>;
}  // namespace

static auto get_path_stats(char const * const path) -> stat_result<struct stat> {
    struct stat stats;
    if (stat(path, &stats) == 0)
        return stats;

    switch (errno) {
        case EACCES: return nlc::permission_denied {};
        case ELOOP: return nlc::loop_or_too_many_symlinks {};
        case ENAMETOOLONG: return nlc::name_too_long {};
        case ENOENT: [[fallthrough]];
        case ENOTDIR: return nlc::path_does_not_exist {};
        case ENOMEM: return nlc::not_enough_memory {};

        case EBADF:
            nlc::warn("is_folder: stat() was called with a bad file descriptor");
            return nlc::internal_error {};
        case EFAULT:
            nlc::warn("is_folder: stat() was called with a bad address");
            return nlc::internal_error {};
        case EOVERFLOW:
            nlc::warn("is_folder: the file descriptor has a size, inode number or number of "
                      "blocks that overflows");
            return nlc::internal_error {};

        default: return nlc::unknown_error {};
    }
}

// DO NOT implement `is_path` with is_file || is_folder.
// Just call `access` which is less costly than `stat`.
static auto is_file(char const * const path) -> stat_result<bool> {
    auto const stats = get_path_stats(path);
    if (stats.failed())
        return stats.forward_error();

    return S_ISREG(stats.get().st_mode);
}

[[maybe_unused]] static auto is_folder(char const * const path) -> stat_result<bool> {
    auto const stats = get_path_stats(path);
    if (stats.failed())
        return stats.forward_error();

    return S_ISDIR(stats.get().st_mode);
}

static auto get_files_in_directory(nlc::allocator & allocator,
                                   nlc::string_view const path,
                                   usize depth,
                                   nlc::vector<nlc::string> & file_names)
    -> nlc::expected<void,
                     nlc::permission_denied,
                     nlc::too_many_filedescriptors_open,
                     nlc::path_does_not_exist,
                     nlc::not_enough_memory,
                     nlc::internal_error,
                     nlc::unknown_error> {
    nlc::vector<nlc::string> folders_to_explore { allocator };
    folders_to_explore.append(allocator, path);

    while (folders_to_explore.is_empty() == false) {
        nlc::vector<nlc::string> next_folders_to_explore { allocator };

        for (auto const & folder : folders_to_explore) {
            DIR * const dir = opendir(folder.c_str());
            if (dir == nullptr) {
                switch (errno) {
                    case EACCES: return nlc::permission_denied {};
                    case EMFILE: [[fallthrough]];
                    case ENFILE: return nlc::too_many_filedescriptors_open {};
                    case ENOENT: [[fallthrough]];
                    case ENOTDIR: return nlc::path_does_not_exist {};
                    case ENOMEM: return nlc::not_enough_memory {};

                    case EBADF:
                        nlc::warn("get_files_in_directory: opendir() was called with a bad "
                                  "file descriptor");
                        return nlc::internal_error {};

                    default: return nlc::unknown_error {};
                }
            }

            defer {
                [[maybe_unused]] auto close_res = closedir(dir);
                nlc_assert_msg(close_res == 0, "get_files_in_directory: closedir() failed");
            };

            while (dirent * const entry = readdir(dir)) {
                auto entry_name = nlc::string::from_c_str(allocator, entry->d_name);
                if (entry->d_type != DT_DIR)
                    file_names.append(nlc_move(entry_name));
                else if (depth > 0u && is_special_folder(entry_name) == false) {
                    auto concatenated_path = concat_strings(allocator, folder, "/", entry_name);
                    next_folders_to_explore.append(nlc_move(concatenated_path));
                }
            }

            if (errno == EBADF) {
                nlc::warn("get_files_in_directory: readdir() was called with a bad file "
                          "descriptor");
                return nlc::internal_error {};
            }
        }

        if (depth == 0u)
            break;
        --depth;
        folders_to_explore = nlc_move(next_folders_to_explore);
    }

    static_str_from_view(path, buffer);

    return {};
}

namespace nlc {
auto set_current_directory(string_view const path) -> set_current_dir_result {
    if (chdir(path.ptr()) == 0)
        return success {};

    switch (errno) {
        case EACCES: return permission_denied {};
        case ELOOP: return loop_or_too_many_symlinks {};
        case ENAMETOOLONG: return name_too_long {};
        case ENOENT: [[fallthrough]];
        case ENOTDIR: return path_does_not_exist {};
        default: return unknown_error {};
    }
}

auto get_current_directory(allocator & allocator) -> get_current_dir_result {
    char buff[PATH_MAX + 1];
    char const * const result = getcwd(buff, PATH_MAX + 1);

    if (result == nullptr) {
        switch (errno) {
            case EACCES: return permission_denied {};
            case EFAULT: [[fallthrough]];
            case EINVAL: [[fallthrough]];
            case ERANGE: return internal_error {};
            case ENAMETOOLONG: return name_too_long {};
            case ENOMEM: return not_enough_memory {};
            case ENOENT: [[fallthrough]];
            default: return unknown_error {};
        }
    }

    return string::from_c_str(allocator, result);
}

auto does_file_exist(string_view const path) -> does_file_exist_result {
    static_str_from_view(path, buffer);
    auto const res = is_file(buffer);
    if (res.failed_with<path_does_not_exist>())
        return false;
    else if (res.failed())
        return res.forward_error();

    return res.get();
}

auto get_files_in_directory(allocator & allocator,
                            string_view const path,
                            usize depth) -> get_files_in_directory_result {
    vector<string> file_names { allocator };
    auto const res = ::get_files_in_directory(allocator, path, depth, file_names);
    if (res.failed())
        return res.forward_error();

    return file_names;
}

auto get_file_modification_time(string_view const path) -> get_file_modification_time_result {
    static_str_from_view(path, buffer);
    auto const stats = get_path_stats(buffer);
    if (stats.failed())
        return stats.forward_error();

    auto const & mtim = stats.get().st_mtim;
    return nlc::time::from_seconds(mtim.tv_sec) + nlc::time::from_nanoseconds(mtim.tv_nsec);
}
}  // namespace nlc
