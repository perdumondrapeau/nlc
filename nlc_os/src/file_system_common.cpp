/* Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "file_system_common.hpp"

#include <nlc/algo/find.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/dialect/optional.hpp>

static constexpr nlc::string_view special_folders[] = { ".", ".." };

namespace nlc {
auto is_special_folder(string_view const name) -> bool {
    return find<string_view const>(special_folders, name) != nullptr;
}
}  // namespace nlc
