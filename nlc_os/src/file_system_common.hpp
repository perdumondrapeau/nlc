/* Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/meta/basics.hpp>

namespace nlc {
class allocator;
}  // namespace nlc

namespace nlc {
auto is_special_folder(string_view const name) -> bool;

template<typename... Strings>
inline auto concat_strings(string_stream & stream, Strings &&... strings) -> void {
    (stream.append(nlc_fwd(strings)), ...);
}

template<typename... Strings>
inline auto concat_strings(allocator & allocator, Strings &&... strings) -> string {
    string_stream stream { allocator };
    concat_strings(stream, nlc_fwd(strings)...);
    return string::from_c_str(allocator, stream.c_str());
}
}  // namespace nlc
