/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/maths/float_cmp.hpp>

#define nlc_check_float_equality(lhs, rhs)                                                                  \
    do {                                                                                                    \
        auto const check_float_lhs = lhs;                                                                   \
        auto const check_float_rhs = rhs;                                                                   \
        nlc_check_msg(nlc::are_floats_close_absolute_value_wise(check_float_lhs, check_float_rhs, 2E-3f) || \
                          nlc::are_floats_close_ulp_wise(check_float_lhs, check_float_rhs, 4u),             \
                      "lhs = ",                                                                             \
                      check_float_lhs,                                                                      \
                      " rhs = ",                                                                            \
                      check_float_rhs);                                                                     \
    } while (false)

#define nlc_check_double_equality(lhs, rhs)                                                                \
    do {                                                                                                   \
        auto const check_float_lhs = lhs;                                                                  \
        auto const check_float_rhs = rhs;                                                                  \
        nlc_check_msg(nlc::are_floats_close_absolute_value_wise(check_float_lhs, check_float_rhs, 2E-3) || \
                          nlc::are_floats_close_ulp_wise(check_float_lhs, check_float_rhs, 4u),            \
                      "lhs = ",                                                                            \
                      check_float_lhs,                                                                     \
                      " rhs = ",                                                                           \
                      check_float_rhs);                                                                    \
    } while (false)
