/*  Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/maths/units/physics_units.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test("basic physics") {
    constexpr seconds<> _t { 1.5f };
    constexpr acc2<> acc { 10.f, 10.f };
    vel2<> vel { 15.f, 30.f };
    pos2<> pos { 0.f, 0.f };

    // tech-debt:unit2d
    auto const t = rm_unit(_t);
    vel += acc * t;
    pos += vel * t;
}
