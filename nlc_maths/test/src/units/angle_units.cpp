/*  Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/maths/constants.hpp>
#include <nlc/maths/float_cmp.hpp>
#include <nlc/maths/simple_maths.hpp>
#include <nlc/maths/units/angle_units.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/traits.hpp>
#include <nlc/meta/units.hpp>
#include <nlc/test.hpp>

#define static_and_dynamic_check(cond) \
    static_assert(cond, #cond);        \
    nlc_check(cond)

#define COMMA ,

constexpr auto ulps_tolerance = 4u;

using namespace nlc;

nlc_test("angle conversion") {
    constexpr radians half_circle_rad { pi<float> };
    constexpr auto half_circle_deg { rad_to_deg(half_circle_rad) };

    static_and_dynamic_check(
        meta::is_same<meta::rm_const<decltype(half_circle_deg)> COMMA degrees<float>>);
    nlc_check(are_floats_close_ulp_wise(rm_unit(half_circle_deg), 180.f, ulps_tolerance));

    constexpr degrees<> full_circle_deg { 360.f };
    constexpr auto full_circle_rad { deg_to_rad(full_circle_deg) };

    static_and_dynamic_check(meta::is_same<meta::rm_const<decltype(full_circle_rad)> COMMA radians>);
    nlc_check(are_floats_close_ulp_wise(rm_unit(full_circle_rad), 2.f * pi<float>, ulps_tolerance));
}

nlc_test("angle suffixes") {
    constexpr auto rad = -2_rad;
    static_and_dynamic_check(meta::is_same<meta::rm_const<decltype(rad)> COMMA radians>);
    constexpr auto frad = 1.57_rad;
    static_and_dynamic_check(meta::is_same<meta::rm_const<decltype(frad)> COMMA radians>);

    constexpr auto deg = -90_deg;
    static_and_dynamic_check(meta::is_same<meta::rm_const<decltype(deg)> COMMA degrees<float>>);
    constexpr auto fdeg = 52.7_deg;
    static_and_dynamic_check(meta::is_same<meta::rm_const<decltype(fdeg)> COMMA degrees<float>>);
}

#undef COMMA
