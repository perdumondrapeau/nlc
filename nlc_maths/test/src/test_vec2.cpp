/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/maths/float_cmp.hpp>
#include <nlc/maths/vec2.hpp>
#include <nlc/maths/vec2_common.hpp>
#include <nlc/test.hpp>

#include "check_float.hpp"

using namespace nlc;

nlc_test("u8_2 operators") {
    u8_2 a { .x = 6, .y = 3 };
    u8_2 b { .x = 3, .y = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3, "x=", b.x, ", y=", b.y);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2, "x=", b.x, ", y=", b.y);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4, "x=", a.x, ", y=", a.y);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5, "x=", a.x, ", y=", a.y);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);

    a = { 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15, "x=", a.x, ", y=", a.y);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8, "x=", a.x, ", y=", a.y);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0, "x=", a.x, ", y=", a.y);

    a = { 7, 13 };
    a %= u8_2 { 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check((u8_2 { 1, 13 } | u8_2 { 1, 2 }) == u8_2 { 1, 15 });
    nlc_check((u8_2 { 1, 13 } & u8_2 { 1, 3 }) == u8_2 { 1, 1 });
    nlc_check((u8_2 { 24, 13 } >> u8_2 { 2, 1 }) == u8_2 { 6, 6 });
    nlc_check((u8_2 { 1, 13 } << u8_2 { 3, 2 }) == u8_2 { 8, 52 });
    nlc_check((u8_2 { 1, 13 } ^ u8_2 { 3, 2 }) == u8_2 { 2, 15 });
    nlc_check((u8_2 { 7, 13 } % u8_2 { 5, 4 }) == u8_2 { 2, 1 });

    u8 const cst { 3 };
    a = { 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("i8_2 operators") {
    i8_2 a { .x = -6, .y = 3 };
    i8_2 b { .x = 3, .y = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5, "x=", b.x, ", y=", b.y);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6, "x=", b.x, ", y=", b.y);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4, "x=", a.x, ", y=", a.y);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5, "x=", a.x, ", y=", a.y);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6, "x=", a.x, ", y=", a.y);

    a = { 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1, "x=", a.x, ", y=", a.y);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a <<= i8_2 { 2, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48, "x=", a.x, ", y=", a.y);
    a >>= i8_2 { 2, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a ^= i8_2 { 1, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3, "x=", a.x, ", y=", a.y);

    a = { 7, 13 };
    a %= i8_2 { 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check((i8_2 { 1, 13 } | i8_2 { 1, 2 }) == i8_2 { 1, 15 });
    nlc_check((i8_2 { 1, 13 } & i8_2 { 1, 3 }) == i8_2 { 1, 1 });
    nlc_check((i8_2 { 24, 13 } >> i8_2 { 2, 1 }) == i8_2 { 6, 6 });
    nlc_check((i8_2 { 1, 13 } << i8_2 { 3, 2 }) == i8_2 { 8, 52 });
    nlc_check((i8_2 { 1, 13 } ^ i8_2 { 3, 2 }) == i8_2 { 2, 15 });
    nlc_check((i8_2 { 7, 13 } % i8_2 { 5, 4 }) == i8_2 { 2, 1 });

    i8 const cst { 3 };
    a = { 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("u16_2 operators") {
    u16_2 a { .x = 6, .y = 3 };
    u16_2 b { .x = 3, .y = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3, "x=", b.x, ", y=", b.y);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2, "x=", b.x, ", y=", b.y);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4, "x=", a.x, ", y=", a.y);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5, "x=", a.x, ", y=", a.y);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);

    a = { 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15, "x=", a.x, ", y=", a.y);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8, "x=", a.x, ", y=", a.y);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0, "x=", a.x, ", y=", a.y);

    a = { 7, 13 };
    a %= u16_2 { 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check((u16_2 { 1, 13 } | u16_2 { 1, 2 }) == u16_2 { 1, 15 });
    nlc_check((u16_2 { 1, 13 } & u16_2 { 1, 3 }) == u16_2 { 1, 1 });
    nlc_check((u16_2 { 24, 13 } >> u16_2 { 2, 1 }) == u16_2 { 6, 6 });
    nlc_check((u16_2 { 1, 13 } << u16_2 { 3, 2 }) == u16_2 { 8, 52 });
    nlc_check((u16_2 { 1, 13 } ^ u16_2 { 3, 2 }) == u16_2 { 2, 15 });
    nlc_check((u16_2 { 7, 13 } % u16_2 { 5, 4 }) == u16_2 { 2, 1 });

    u16 const cst { 3 };
    a = { 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("i16_2 operators") {
    i16_2 a { .x = -6, .y = 3 };
    i16_2 b { .x = 3, .y = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5, "x=", b.x, ", y=", b.y);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6, "x=", b.x, ", y=", b.y);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4, "x=", a.x, ", y=", a.y);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5, "x=", a.x, ", y=", a.y);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6, "x=", a.x, ", y=", a.y);

    a = { 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1, "x=", a.x, ", y=", a.y);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a <<= i16_2 { 2, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48, "x=", a.x, ", y=", a.y);
    a >>= i16_2 { 2, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a ^= i16_2 { 1, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3, "x=", a.x, ", y=", a.y);

    a = { 7, 13 };
    a %= i16_2 { 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check((i16_2 { 1, 13 } | i16_2 { 1, 2 }) == i16_2 { 1, 15 });
    nlc_check((i16_2 { 1, 13 } & i16_2 { 1, 3 }) == i16_2 { 1, 1 });
    nlc_check((i16_2 { 24, 13 } >> i16_2 { 2, 1 }) == i16_2 { 6, 6 });
    nlc_check((i16_2 { 1, 13 } << i16_2 { 3, 2 }) == i16_2 { 8, 52 });
    nlc_check((i16_2 { 1, 13 } ^ i16_2 { 3, 2 }) == i16_2 { 2, 15 });
    nlc_check((i16_2 { 7, 13 } % i16_2 { 5, 4 }) == i16_2 { 2, 1 });

    i16 const cst { 3 };
    a = { 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("u32_2 operators") {
    u32_2 a { .x = 6, .y = 3 };
    u32_2 b { .x = 3, .y = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3, "x=", b.x, ", y=", b.y);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2, "x=", b.x, ", y=", b.y);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4, "x=", a.x, ", y=", a.y);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5, "x=", a.x, ", y=", a.y);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);

    a = { 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15, "x=", a.x, ", y=", a.y);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8, "x=", a.x, ", y=", a.y);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0, "x=", a.x, ", y=", a.y);

    a = { 7, 13 };
    a %= u32_2 { 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check((u32_2 { 1, 13 } | u32_2 { 1, 2 }) == u32_2 { 1, 15 });
    nlc_check((u32_2 { 1, 13 } & u32_2 { 1, 3 }) == u32_2 { 1, 1 });
    nlc_check((u32_2 { 24, 13 } >> u32_2 { 2, 1 }) == u32_2 { 6, 6 });
    nlc_check((u32_2 { 1, 13 } << u32_2 { 3, 2 }) == u32_2 { 8, 52 });
    nlc_check((u32_2 { 1, 13 } ^ u32_2 { 3, 2 }) == u32_2 { 2, 15 });
    nlc_check((u32_2 { 7, 13 } % u32_2 { 5, 4 }) == u32_2 { 2, 1 });

    u32 const cst { 3 };
    a = { 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("i32_2 operators") {
    i32_2 a { .x = -6, .y = 3 };
    i32_2 b { .x = 3, .y = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5, "x=", b.x, ", y=", b.y);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6, "x=", b.x, ", y=", b.y);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4, "x=", a.x, ", y=", a.y);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5, "x=", a.x, ", y=", a.y);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6, "x=", a.x, ", y=", a.y);

    a = { 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1, "x=", a.x, ", y=", a.y);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a <<= i32_2 { 2, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48, "x=", a.x, ", y=", a.y);
    a >>= i32_2 { 2, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a ^= i32_2 { 1, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3, "x=", a.x, ", y=", a.y);

    a = { 7, 13 };
    a %= i32_2 { 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check((i32_2 { 1, 13 } | i32_2 { 1, 2 }) == i32_2 { 1, 15 });
    nlc_check((i32_2 { 1, 13 } & i32_2 { 1, 3 }) == i32_2 { 1, 1 });
    nlc_check((i32_2 { 24, 13 } >> i32_2 { 2, 1 }) == i32_2 { 6, 6 });
    nlc_check((i32_2 { 1, 13 } << i32_2 { 3, 2 }) == i32_2 { 8, 52 });
    nlc_check((i32_2 { 1, 13 } ^ i32_2 { 3, 2 }) == i32_2 { 2, 15 });
    nlc_check((i32_2 { 7, 13 } % i32_2 { 5, 4 }) == i32_2 { 2, 1 });

    i32 const cst { 3 };
    a = { 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("u64_2 operators") {
    u64_2 a { .x = 6, .y = 3 };
    u64_2 b { .x = 3, .y = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3, "x=", b.x, ", y=", b.y);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2, "x=", b.x, ", y=", b.y);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4, "x=", a.x, ", y=", a.y);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5, "x=", a.x, ", y=", a.y);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);

    a = { 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15, "x=", a.x, ", y=", a.y);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8, "x=", a.x, ", y=", a.y);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0, "x=", a.x, ", y=", a.y);

    a = { 7, 13 };
    a %= u64_2 { 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check((u64_2 { 1, 13 } | u64_2 { 1, 2 }) == u64_2 { 1, 15 });
    nlc_check((u64_2 { 1, 13 } & u64_2 { 1, 3 }) == u64_2 { 1, 1 });
    nlc_check((u64_2 { 24, 13 } >> u64_2 { 2, 1 }) == u64_2 { 6, 6 });
    nlc_check((u64_2 { 1, 13 } << u64_2 { 3, 2 }) == u64_2 { 8, 52 });
    nlc_check((u64_2 { 1, 13 } ^ u64_2 { 3, 2 }) == u64_2 { 2, 15 });
    nlc_check((u64_2 { 7, 13 } % u64_2 { 5, 4 }) == u64_2 { 2, 1 });

    u64 const cst { 3 };
    a = { 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("i64_2 operators") {
    i64_2 a { .x = -6, .y = 3 };
    i64_2 b { .x = 3, .y = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5, "x=", b.x, ", y=", b.y);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6, "x=", b.x, ", y=", b.y);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4, "x=", a.x, ", y=", a.y);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5, "x=", a.x, ", y=", a.y);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6, "x=", a.x, ", y=", a.y);

    a = { 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1, "x=", a.x, ", y=", a.y);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a <<= i64_2 { 2, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48, "x=", a.x, ", y=", a.y);
    a >>= i64_2 { 2, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a ^= i64_2 { 1, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3, "x=", a.x, ", y=", a.y);

    a = { 7, 13 };
    a %= i64_2 { 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check((i64_2 { 1, 13 } | i64_2 { 1, 2 }) == i64_2 { 1, 15 });
    nlc_check((i64_2 { 1, 13 } & i64_2 { 1, 3 }) == i64_2 { 1, 1 });
    nlc_check((i64_2 { 24, 13 } >> i64_2 { 2, 1 }) == i64_2 { 6, 6 });
    nlc_check((i64_2 { 1, 13 } << i64_2 { 3, 2 }) == i64_2 { 8, 52 });
    nlc_check((i64_2 { 1, 13 } ^ i64_2 { 3, 2 }) == i64_2 { 2, 15 });
    nlc_check((i64_2 { 7, 13 } % i64_2 { 5, 4 }) == i64_2 { 2, 1 });

    i64 const cst { 3 };
    a = { 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("usize_2 operators") {
    usize_2 a { .x = 6, .y = 3 };
    usize_2 b { .x = 3, .y = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3, "x=", b.x, ", y=", b.y);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2, "x=", b.x, ", y=", b.y);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4, "x=", a.x, ", y=", a.y);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5, "x=", a.x, ", y=", a.y);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);

    a = { 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15, "x=", a.x, ", y=", a.y);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8, "x=", a.x, ", y=", a.y);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2, "x=", a.x, ", y=", a.y);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0, "x=", a.x, ", y=", a.y);

    a = { 7, 13 };
    a %= usize_2 { 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check((usize_2 { 1, 13 } | usize_2 { 1, 2 }) == usize_2 { 1, 15 });
    nlc_check((usize_2 { 1, 13 } & usize_2 { 1, 3 }) == usize_2 { 1, 1 });
    nlc_check((usize_2 { 24, 13 } >> usize_2 { 2, 1 }) == usize_2 { 6, 6 });
    nlc_check((usize_2 { 1, 13 } << usize_2 { 3, 2 }) == usize_2 { 8, 52 });
    nlc_check((usize_2 { 1, 13 } ^ usize_2 { 3, 2 }) == usize_2 { 2, 15 });
    nlc_check((usize_2 { 7, 13 } % usize_2 { 5, 4 }) == usize_2 { 2, 1 });

    usize const cst { 3 };
    a = { 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("isize_2 operators") {
    isize_2 a { .x = -6, .y = 3 };
    isize_2 b { .x = 3, .y = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5, "x=", b.x, ", y=", b.y);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6, "x=", b.x, ", y=", b.y);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4, "x=", a.x, ", y=", a.y);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5, "x=", a.x, ", y=", a.y);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6, "x=", a.x, ", y=", a.y);

    a = { 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1, "x=", a.x, ", y=", a.y);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a <<= isize_2 { 2, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48, "x=", a.x, ", y=", a.y);
    a >>= isize_2 { 2, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6, "x=", a.x, ", y=", a.y);
    a ^= isize_2 { 1, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3, "x=", a.x, ", y=", a.y);

    a = { 7, 13 };
    a %= isize_2 { 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check((isize_2 { 1, 13 } | isize_2 { 1, 2 }) == isize_2 { 1, 15 });
    nlc_check((isize_2 { 1, 13 } & isize_2 { 1, 3 }) == isize_2 { 1, 1 });
    nlc_check((isize_2 { 24, 13 } >> isize_2 { 2, 1 }) == isize_2 { 6, 6 });
    nlc_check((isize_2 { 1, 13 } << isize_2 { 3, 2 }) == isize_2 { 8, 52 });
    nlc_check((isize_2 { 1, 13 } ^ isize_2 { 3, 2 }) == isize_2 { 2, 15 });
    nlc_check((isize_2 { 7, 13 } % isize_2 { 5, 4 }) == isize_2 { 2, 1 });

    isize const cst { 3 };
    a = { 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("f32_2 operators") {
    f32_2 a { .x = -6, .y = 3 };
    f32_2 b { .x = 3, .y = -4 };

    a = b;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 6 && a.y == -8, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 9 && a.y == 16, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 6 && a.y == -8, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 9 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a = -a;
    nlc_check_msg(a.x == -3 && a.y == 4, "x=", a.x, ", y=", a.y);

    f32 const cst { 3 };
    a = { .x = -6, .y = 3 };

    a += cst;
    nlc_check_msg(a.x == -3 && a.y == 6, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == -6 && a.y == 3, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == -18 && a.y == 9, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == -6 && a.y == 3, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == -3 && a.y == 6, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == -6 && a.y == 3, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == -18 && a.y == 9, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == -6 && a.y == 3, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 3 && a.y == -6, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("f64_2 operators") {
    f64_2 a { .x = -6, .y = 3 };
    f64_2 b { .x = 3, .y = -4 };

    a = b;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a += b;
    nlc_check_msg(a.x == 6 && a.y == -8, "x=", a.x, ", y=", a.y);
    a -= b;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a *= b;
    nlc_check_msg(a.x == 9 && a.y == 16, "x=", a.x, ", y=", a.y);
    a /= b;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a = a + b;
    nlc_check_msg(a.x == 6 && a.y == -8, "x=", a.x, ", y=", a.y);
    a = a - b;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a = a * b;
    nlc_check_msg(a.x == 9 && a.y == 16, "x=", a.x, ", y=", a.y);
    a = a / b;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a = +a;
    nlc_check_msg(a.x == 3 && a.y == -4, "x=", a.x, ", y=", a.y);
    a = -a;
    nlc_check_msg(a.x == -3 && a.y == 4, "x=", a.x, ", y=", a.y);

    f64 const cst { 3 };
    a = { .x = -6, .y = 3 };

    a += cst;
    nlc_check_msg(a.x == -3 && a.y == 6, "x=", a.x, ", y=", a.y);
    a -= cst;
    nlc_check_msg(a.x == -6 && a.y == 3, "x=", a.x, ", y=", a.y);
    a *= cst;
    nlc_check_msg(a.x == -18 && a.y == 9, "x=", a.x, ", y=", a.y);
    a /= cst;
    nlc_check_msg(a.x == -6 && a.y == 3, "x=", a.x, ", y=", a.y);

    a = a + cst;
    nlc_check_msg(a.x == -3 && a.y == 6, "x=", a.x, ", y=", a.y);
    a = a - cst;
    nlc_check_msg(a.x == -6 && a.y == 3, "x=", a.x, ", y=", a.y);
    a = a * cst;
    nlc_check_msg(a.x == -18 && a.y == 9, "x=", a.x, ", y=", a.y);
    a = a / cst;
    nlc_check_msg(a.x == -6 && a.y == 3, "x=", a.x, ", y=", a.y);

    a = nlc_swizzle(a, y, x);
    nlc_check_msg(a.x == 3 && a.y == -6, "x=", a.x, ", y=", a.y);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

nlc_test("u8_2 common functions") {
    nlc_check(min(u8_2 { 3, 5 }, u8_2 { 6, 2 }) == u8_2 { 3, 2 });
    nlc_check(max(u8_2 { 3, 5 }, u8_2 { 6, 2 }) == u8_2 { 6, 5 });
    nlc_check(clamp(u8_2 { 3, 5 }, u8_2 { 6, 2 }, u8_2 { 10, 4 }) == u8_2 { 6, 4 });
    nlc_check(sum(u8_2 { 5, 12 }) == 17);
    nlc_check(dot(u8_2 { 1, 5 }, u8_2 { 4, 2 }) == 14);
    nlc_check(det(u8_2 { 5, 1 }, u8_2 { 2, 4 }) == 18);

    nlc_check(sqrnorm(u8_2 { 3, 4 }) == 25);
    nlc_check(norm(u8_2 { 3, 4 }) == 5);
    nlc_check(normalize(u8_2 { 2, 0 }) == u8_2 { 1, 0 });
    nlc_check(normalize(u8_2 { 0, 5 }) == u8_2 { 0, 1 });
    nlc_check(sqrdistance(u8_2 { 5, 6 }, u8_2 { 8, 10 }) == 25);
    nlc_check(distance(u8_2 { 5, 6 }, u8_2 { 8, 10 }) == 5);

    nlc_check(project(u8_2 { 2, 2 }, u8_2 { 1, 0 }) == i8_2 { 0, 2 });
    nlc_check(normalize_project(u8_2 { 2, 2 }, u8_2 { 8, 0 }) == i8_2 { 0, 2 });
}

nlc_test("i8_2 common functions") {
    nlc_check(abs(i8_2 { -2, -4 }) == i8_2 { 2, 4 });
    nlc_check(abs(i8_2 { 2, 4 }) == i8_2 { 2, 4 });
    nlc_check(sign(i8_2 { -2, -4 }) == i8_2 { -1, -1 });
    nlc_check(sign(i8_2 { 0, 4 }) == i8_2 { 0, 1 });
    nlc_check(min(i8_2 { 3, -5 }, i8_2 { 6, 2 }) == i8_2 { 3, -5 });
    nlc_check(max(i8_2 { 3, 5 }, i8_2 { -6, 2 }) == i8_2 { 3, 5 });
    nlc_check(clamp(i8_2 { 3, 5 }, i8_2 { -6, -4 }, i8_2 { 10, -2 }) == i8_2 { 3, -2 });
    nlc_check(sum(i8_2 { 5, -12 }) == -7);
    nlc_check(dot(i8_2 { 1, -5 }, i8_2 { 4, 2 }) == -6);
    nlc_check(det(i8_2 { 5, 1 }, i8_2 { -2, 4 }) == 22);

    nlc_check(sqrnorm(i8_2 { -3, -4 }) == 25);
    nlc_check(norm(i8_2 { -3, -4 }) == 5);
    nlc_check(normalize(i8_2 { -2, 0 }) == i8_2 { -1, 0 });
    nlc_check(normalize(i8_2 { 0, 5 }) == i8_2 { 0, 1 });
    nlc_check(sqrdistance(i8_2 { -5, -6 }, i8_2 { -8, -10 }) == 25);
    nlc_check(distance(i8_2 { -5, -6 }, i8_2 { -8, -10 }) == 5);

    nlc_check(reflect(i8_2 { 2, -2 }, i8_2 { 0, -1 }) == i8_2 { 2, 2 });
    nlc_check(reflect(i8_2 { 2, 0 }, i8_2 { -1, 0 }) == i8_2 { -2, 0 });
    nlc_check(normalize_reflect(i8_2 { 2, -2 }, i8_2 { 0, -5 }) == i8_2 { 2, 2 });
    nlc_check(normalize_reflect(i8_2 { 2, 0 }, i8_2 { -8, 0 }) == i8_2 { -2, 0 });
    nlc_check(project(i8_2 { 2, 2 }, i8_2 { -1, 0 }) == i8_2 { 0, 2 });
    nlc_check(normalize_project(i8_2 { 2, 2 }, i8_2 { -6, 0 }) == i8_2 { 0, 2 });
}

nlc_test("u16_2 common functions") {
    nlc_check(min(u16_2 { 3, 5 }, u16_2 { 6, 2 }) == u16_2 { 3, 2 });
    nlc_check(max(u16_2 { 3, 5 }, u16_2 { 6, 2 }) == u16_2 { 6, 5 });
    nlc_check(clamp(u16_2 { 3, 5 }, u16_2 { 6, 2 }, u16_2 { 10, 4 }) == u16_2 { 6, 4 });
    nlc_check(sum(u16_2 { 5, 12 }) == 17);
    nlc_check(dot(u16_2 { 1, 5 }, u16_2 { 4, 2 }) == 14);
    nlc_check(det(u16_2 { 5, 1 }, u16_2 { 2, 4 }) == 18);

    nlc_check(sqrnorm(u16_2 { 3, 4 }) == 25);
    nlc_check(norm(u16_2 { 3, 4 }) == 5);
    nlc_check(normalize(u16_2 { 2, 0 }) == u16_2 { 1, 0 });
    nlc_check(normalize(u16_2 { 2, 5 }) == u16_2 { 0, 1 });
    nlc_check(sqrdistance(u16_2 { 5, 6 }, u16_2 { 8, 10 }) == 25);
    nlc_check(distance(u16_2 { 5, 6 }, u16_2 { 8, 10 }) == 5);

    nlc_check(project(u16_2 { 2, 2 }, u16_2 { 1, 0 }) == i16_2 { 0, 2 });
    nlc_check(normalize_project(u16_2 { 2, 2 }, u16_2 { 8, 0 }) == i16_2 { 0, 2 });
}

nlc_test("i16_2 common functions") {
    nlc_check(abs(i16_2 { -2, -4 }) == i16_2 { 2, 4 });
    nlc_check(abs(i16_2 { 2, 4 }) == i16_2 { 2, 4 });
    nlc_check(sign(i16_2 { -2, -4 }) == i16_2 { -1, -1 });
    nlc_check(sign(i16_2 { 0, 4 }) == i16_2 { 0, 1 });
    nlc_check(min(i16_2 { 3, -5 }, i16_2 { 6, 2 }) == i16_2 { 3, -5 });
    nlc_check(max(i16_2 { 3, 5 }, i16_2 { -6, 2 }) == i16_2 { 3, 5 });
    nlc_check(clamp(i16_2 { 3, 5 }, i16_2 { -6, -4 }, i16_2 { 10, -2 }) == i16_2 { 3, -2 });
    nlc_check(sum(i16_2 { 5, -12 }) == -7);
    nlc_check(dot(i16_2 { 1, -5 }, i16_2 { 4, 2 }) == -6);
    nlc_check(det(i16_2 { 5, 1 }, i16_2 { -2, 4 }) == 22);

    nlc_check(sqrnorm(i16_2 { -3, -4 }) == 25);
    nlc_check(norm(i16_2 { -3, -4 }) == 5);
    nlc_check(normalize(i16_2 { -2, 0 }) == i16_2 { -1, 0 });
    nlc_check(normalize(i16_2 { 0, 5 }) == i16_2 { 0, 1 });
    nlc_check(sqrdistance(i16_2 { -5, -6 }, i16_2 { -8, -10 }) == 25);
    nlc_check(distance(i16_2 { -5, -6 }, i16_2 { -8, -10 }) == 5);

    nlc_check(reflect(i16_2 { 2, -2 }, i16_2 { 0, -1 }) == i16_2 { 2, 2 });
    nlc_check(reflect(i16_2 { 2, 0 }, i16_2 { -1, 0 }) == i16_2 { -2, 0 });
    nlc_check(normalize_reflect(i16_2 { 2, -2 }, i16_2 { 0, -5 }) == i16_2 { 2, 2 });
    nlc_check(normalize_reflect(i16_2 { 2, 0 }, i16_2 { -8, 0 }) == i16_2 { -2, 0 });
    nlc_check(project(i16_2 { 2, 2 }, i16_2 { -1, 0 }) == i16_2 { 0, 2 });
    nlc_check(normalize_project(i16_2 { 2, 2 }, i16_2 { -6, 0 }) == i16_2 { 0, 2 });
}

nlc_test("u32_2 common functions") {
    nlc_check(min(u32_2 { 3, 5 }, u32_2 { 6, 2 }) == u32_2 { 3, 2 });
    nlc_check(max(u32_2 { 3, 5 }, u32_2 { 6, 2 }) == u32_2 { 6, 5 });
    nlc_check(clamp(u32_2 { 3, 5 }, u32_2 { 6, 2 }, u32_2 { 10, 4 }) == u32_2 { 6, 4 });
    nlc_check(sum(u32_2 { 5, 12 }) == 17);
    nlc_check(dot(u32_2 { 1, 5 }, u32_2 { 4, 2 }) == 14);
    nlc_check(det(u32_2 { 5, 1 }, u32_2 { 2, 4 }) == 18);

    nlc_check(sqrnorm(u32_2 { 3, 4 }) == 25);
    nlc_check(norm(u32_2 { 3, 4 }) == 5);
    nlc_check(normalize(u32_2 { 2, 0 }) == u32_2 { 1, 0 });
    nlc_check(normalize(u32_2 { 2, 5 }) == u32_2 { 0, 1 });
    nlc_check(sqrdistance(u32_2 { 5, 6 }, u32_2 { 8, 10 }) == 25);
    nlc_check(distance(u32_2 { 5, 6 }, u32_2 { 8, 10 }) == 5);

    nlc_check(project(u32_2 { 2, 2 }, u32_2 { 1, 0 }) == i32_2 { 0, 2 });
    nlc_check(normalize_project(u32_2 { 2, 2 }, u32_2 { 8, 0 }) == i32_2 { 0, 2 });
}

nlc_test("i32_2 common functions") {
    nlc_check(abs(i32_2 { -2, -4 }) == i32_2 { 2, 4 });
    nlc_check(abs(i32_2 { 2, 4 }) == i32_2 { 2, 4 });
    nlc_check(sign(i32_2 { -2, -4 }) == i32_2 { -1, -1 });
    nlc_check(sign(i32_2 { 0, 4 }) == i32_2 { 0, 1 });
    nlc_check(min(i32_2 { 3, -5 }, i32_2 { 6, 2 }) == i32_2 { 3, -5 });
    nlc_check(max(i32_2 { 3, 5 }, i32_2 { -6, 2 }) == i32_2 { 3, 5 });
    nlc_check(clamp(i32_2 { 3, 5 }, i32_2 { -6, -4 }, i32_2 { 10, -2 }) == i32_2 { 3, -2 });
    nlc_check(sum(i32_2 { 5, -12 }) == -7);
    nlc_check(dot(i32_2 { 1, -5 }, i32_2 { 4, 2 }) == -6);
    nlc_check(det(i32_2 { 5, 1 }, i32_2 { -2, 4 }) == 22);

    nlc_check(sqrnorm(i32_2 { -3, -4 }) == 25);
    nlc_check(norm(i32_2 { -3, -4 }) == 5);
    nlc_check(normalize(i32_2 { -2, 0 }) == i32_2 { -1, 0 });
    nlc_check(normalize(i32_2 { 0, 5 }) == i32_2 { 0, 1 });
    nlc_check(sqrdistance(i32_2 { -5, -6 }, i32_2 { -8, -10 }) == 25);
    nlc_check(distance(i32_2 { -5, -6 }, i32_2 { -8, -10 }) == 5);

    nlc_check(reflect(i32_2 { 2, -2 }, i32_2 { 0, -1 }) == i32_2 { 2, 2 });
    nlc_check(reflect(i32_2 { 2, 0 }, i32_2 { -1, 0 }) == i32_2 { -2, 0 });
    nlc_check(normalize_reflect(i32_2 { 2, -2 }, i32_2 { 0, -5 }) == i32_2 { 2, 2 });
    nlc_check(normalize_reflect(i32_2 { 2, 0 }, i32_2 { -8, 0 }) == i32_2 { -2, 0 });
    nlc_check(project(i32_2 { 2, 2 }, i32_2 { -1, 0 }) == i32_2 { 0, 2 });
    nlc_check(normalize_project(i32_2 { 2, 2 }, i32_2 { -6, 0 }) == i32_2 { 0, 2 });
}

nlc_test("u64_2 common functions") {
    nlc_check(min(u64_2 { 3, 5 }, u64_2 { 6, 2 }) == u64_2 { 3, 2 });
    nlc_check(max(u64_2 { 3, 5 }, u64_2 { 6, 2 }) == u64_2 { 6, 5 });
    nlc_check(clamp(u64_2 { 3, 5 }, u64_2 { 6, 2 }, u64_2 { 10, 4 }) == u64_2 { 6, 4 });
    nlc_check(sum(u64_2 { 5, 12 }) == 17);
    nlc_check(dot(u64_2 { 1, 5 }, u64_2 { 4, 2 }) == 14);
    nlc_check(det(u64_2 { 5, 1 }, u64_2 { 2, 4 }) == 18);

    nlc_check(sqrnorm(u64_2 { 3, 4 }) == 25);
    nlc_check(norm(u64_2 { 3, 4 }) == 5);
    nlc_check(normalize(u64_2 { 2, 0 }) == u64_2 { 1, 0 });
    nlc_check(normalize(u64_2 { 2, 5 }) == u64_2 { 0, 1 });
    nlc_check(sqrdistance(u64_2 { 5, 6 }, u64_2 { 8, 10 }) == 25);
    nlc_check(distance(u64_2 { 5, 6 }, u64_2 { 8, 10 }) == 5);

    nlc_check(project(u64_2 { 2, 2 }, u64_2 { 1, 0 }) == i64_2 { 0, 2 });
    nlc_check(normalize_project(u64_2 { 2, 2 }, u64_2 { 8, 0 }) == i64_2 { 0, 2 });
}

nlc_test("i64_2 common functions") {
    nlc_check(abs(i64_2 { -2, -4 }) == i64_2 { 2, 4 });
    nlc_check(abs(i64_2 { 2, 4 }) == i64_2 { 2, 4 });
    nlc_check(sign(i64_2 { -2, -4 }) == i64_2 { -1, -1 });
    nlc_check(sign(i64_2 { 0, 4 }) == i64_2 { 0, 1 });
    nlc_check(min(i64_2 { 3, -5 }, i64_2 { 6, 2 }) == i64_2 { 3, -5 });
    nlc_check(max(i64_2 { 3, 5 }, i64_2 { -6, 2 }) == i64_2 { 3, 5 });
    nlc_check(clamp(i64_2 { 3, 5 }, i64_2 { -6, -4 }, i64_2 { 10, -2 }) == i64_2 { 3, -2 });
    nlc_check(sum(i64_2 { 5, -12 }) == -7);
    nlc_check(dot(i64_2 { 1, -5 }, i64_2 { 4, 2 }) == -6);
    nlc_check(det(i64_2 { 5, 1 }, i64_2 { -2, 4 }) == 22);

    nlc_check(sqrnorm(i64_2 { -3, -4 }) == 25);
    nlc_check(norm(i64_2 { -3, -4 }) == 5);
    nlc_check(normalize(i64_2 { -2, 0 }) == i64_2 { -1, 0 });
    nlc_check(normalize(i64_2 { 0, 5 }) == i64_2 { 0, 1 });
    nlc_check(sqrdistance(i64_2 { -5, -6 }, i64_2 { -8, -10 }) == 25);
    nlc_check(distance(i64_2 { -5, -6 }, i64_2 { -8, -10 }) == 5);

    nlc_check(reflect(i64_2 { 2, -2 }, i64_2 { 0, -1 }) == i64_2 { 2, 2 });
    nlc_check(reflect(i64_2 { 2, 0 }, i64_2 { -1, 0 }) == i64_2 { -2, 0 });
    nlc_check(normalize_reflect(i64_2 { 2, -2 }, i64_2 { 0, -5 }) == i64_2 { 2, 2 });
    nlc_check(normalize_reflect(i64_2 { 2, 0 }, i64_2 { -8, 0 }) == i64_2 { -2, 0 });
    nlc_check(project(i64_2 { 2, 2 }, i64_2 { -1, 0 }) == i64_2 { 0, 2 });
    nlc_check(normalize_project(i64_2 { 2, 2 }, i64_2 { -6, 0 }) == i64_2 { 0, 2 });
}

nlc_test("usize_2 common functions") {
    nlc_check(min(usize_2 { 3, 5 }, usize_2 { 6, 2 }) == usize_2 { 3, 2 });
    nlc_check(max(usize_2 { 3, 5 }, usize_2 { 6, 2 }) == usize_2 { 6, 5 });
    nlc_check(clamp(usize_2 { 3, 5 }, usize_2 { 6, 2 }, usize_2 { 10, 4 }) == usize_2 { 6, 4 });
    nlc_check(sum(usize_2 { 5, 12 }) == 17);
    nlc_check(dot(usize_2 { 1, 5 }, usize_2 { 4, 2 }) == 14);
    nlc_check(det(usize_2 { 5, 1 }, usize_2 { 2, 4 }) == 18);

    nlc_check(sqrnorm(usize_2 { 3, 4 }) == 25);
    nlc_check(norm(usize_2 { 3, 4 }) == 5);
    nlc_check(normalize(usize_2 { 2, 0 }) == usize_2 { 1, 0 });
    nlc_check(normalize(usize_2 { 2, 5 }) == usize_2 { 0, 1 });
    nlc_check(sqrdistance(usize_2 { 5, 6 }, usize_2 { 8, 10 }) == 25);
    nlc_check(distance(usize_2 { 5, 6 }, usize_2 { 8, 10 }) == 5);

    nlc_check(project(usize_2 { 2, 2 }, usize_2 { 1, 0 }) == isize_2 { 0, 2 });
    nlc_check(normalize_project(usize_2 { 2, 2 }, usize_2 { 8, 0 }) == isize_2 { 0, 2 });
}

nlc_test("isize_2 common functions") {
    nlc_check(abs(isize_2 { -2, -4 }) == isize_2 { 2, 4 });
    nlc_check(abs(isize_2 { 2, 4 }) == isize_2 { 2, 4 });
    nlc_check(sign(isize_2 { -2, -4 }) == isize_2 { -1, -1 });
    nlc_check(sign(isize_2 { 0, 4 }) == isize_2 { 0, 1 });
    nlc_check(min(isize_2 { 3, -5 }, isize_2 { 6, 2 }) == isize_2 { 3, -5 });
    nlc_check(max(isize_2 { 3, 5 }, isize_2 { -6, 2 }) == isize_2 { 3, 5 });
    nlc_check(clamp(isize_2 { 3, 5 }, isize_2 { -6, -4 }, isize_2 { 10, -2 }) == isize_2 { 3, -2 });
    nlc_check(sum(isize_2 { 5, -12 }) == -7);
    nlc_check(dot(isize_2 { 1, -5 }, isize_2 { 4, 2 }) == -6);
    nlc_check(det(isize_2 { 5, 1 }, isize_2 { -2, 4 }) == 22);

    nlc_check(sqrnorm(isize_2 { -3, -4 }) == 25);
    nlc_check(norm(isize_2 { -3, -4 }) == 5);
    nlc_check(normalize(isize_2 { -2, 0 }) == isize_2 { -1, 0 });
    nlc_check(normalize(isize_2 { 0, 5 }) == isize_2 { 0, 1 });
    nlc_check(sqrdistance(isize_2 { -5, -6 }, isize_2 { -8, -10 }) == 25);
    nlc_check(distance(isize_2 { -5, -6 }, isize_2 { -8, -10 }) == 5);

    nlc_check(reflect(isize_2 { 2, -2 }, isize_2 { 0, -1 }) == isize_2 { 2, 2 });
    nlc_check(reflect(isize_2 { 2, 0 }, isize_2 { -1, 0 }) == isize_2 { -2, 0 });
    nlc_check(normalize_reflect(isize_2 { 2, -2 }, isize_2 { 0, -5 }) == isize_2 { 2, 2 });
    nlc_check(normalize_reflect(isize_2 { 2, 0 }, isize_2 { -8, 0 }) == isize_2 { -2, 0 });
    nlc_check(project(isize_2 { 2, 2 }, isize_2 { -1, 0 }) == isize_2 { 0, 2 });
    nlc_check(normalize_project(isize_2 { 2, 2 }, isize_2 { -6, 0 }) == isize_2 { 0, 2 });
}

nlc_test("f32_2 common functions") {
    nlc_check(abs(f32_2 { -2, -4 }) == f32_2 { 2, 4 });
    nlc_check(abs(f32_2 { 2, 4 }) == f32_2 { 2, 4 });
    nlc_check(sign(f32_2 { -2, -4 }) == f32_2 { -1, -1 });
    nlc_check(sign(f32_2 { 0, 4 }) == f32_2 { 1, 1 });
    nlc_check(sign(f32_2 { -0.f, 4 }) == f32_2 { -1, 1 });
    nlc_check(min(f32_2 { 3, 5 }, f32_2 { 6, 2 }) == f32_2 { 3, 2 });
    nlc_check(max(f32_2 { 3, 5 }, f32_2 { 6, 2 }) == f32_2 { 6, 5 });
    nlc_check(clamp(f32_2 { 3, 5 }, f32_2 { 6, 2 }, f32_2 { 10, 4 }) == f32_2 { 6, 4 });

    nlc_check_float_equality(dot(f32_2 { 1, 5 }, f32_2 { 4, 2 }), 14);
    nlc_check_float_equality(det(f32_2 { 5, 1 }, f32_2 { 2, 4 }), 18);

    nlc_check_float_equality(sqrnorm(f32_2 { 3, 4 }), 25.0f);
    nlc_check_float_equality(norm(f32_2 { 3, 4 }), 5.0f);
    nlc_check_float_equality(normalize(f32_2 { 2, 0 }), (f32_2 { 1, 0 }));
    nlc_check_float_equality(normalize(f32_2 { 0, 5 }), (f32_2 { 0, 1 }));
    nlc_check_float_equality(sqrdistance(f32_2 { 5, 6 }, f32_2 { 8, 10 }), 25.0f);
    nlc_check_float_equality(distance(f32_2 { 5, 6 }, f32_2 { 8, 10 }), 5.0f);

    nlc_check_float_equality(reflect(f32_2 { 2, -2 }, f32_2 { 0, -1 }), (f32_2 { 2, 2 }));
    nlc_check_float_equality(reflect(f32_2 { 2, 0 }, f32_2 { -1, 0 }), (f32_2 { -2, 0 }));
    nlc_check_float_equality(normalize_reflect(f32_2 { 2, -2 }, f32_2 { 0, -5 }), (f32_2 { 2, 2 }));
    nlc_check_float_equality(normalize_reflect(f32_2 { 2, 0 }, f32_2 { -8, 0 }), (f32_2 { -2, 0 }));
    nlc_check_float_equality(project(f32_2 { 2, 2 }, f32_2 { -1, 0 }), (f32_2 { 0, 2 }));
    nlc_check_float_equality(normalize_project(f32_2 { 2, 2 }, f32_2 { -6, 0 }), (f32_2 { 0, 2 }));
    nlc_check_float_equality(rnorm(f32_2 { 3, 4 }), 0.2f);
    nlc_check_float_equality(normalize(f32_2 { 3, 4 }), (f32_2 { 0.6f, 0.8f }));
}

nlc_test("f64_2 common functions") {
    nlc_check(abs(f64_2 { -2, -4 }) == f64_2 { 2, 4 });
    nlc_check(abs(f64_2 { 2, 4 }) == f64_2 { 2, 4 });
    nlc_check(sign(f64_2 { -2, -4 }) == f64_2 { -1, -1 });
    nlc_check(sign(f64_2 { 0, 4 }) == f64_2 { 1, 1 });
    nlc_check(sign(f64_2 { -0.0, 4 }) == f64_2 { -1, 1 });
    nlc_check(min(f64_2 { 3, 5 }, f64_2 { 6, 2 }) == f64_2 { 3, 2 });
    nlc_check(max(f64_2 { 3, 5 }, f64_2 { 6, 2 }) == f64_2 { 6, 5 });
    nlc_check(clamp(f64_2 { 3, 5 }, f64_2 { 6, 2 }, f64_2 { 10, 4 }) == f64_2 { 6, 4 });

    nlc_check_double_equality(sum(f64_2 { 5, 12 }), 17.0);
    nlc_check_double_equality(det(f64_2 { 5, 1 }, f64_2 { 2, 4 }), 18.0);

    nlc_check_double_equality(sqrnorm(f64_2 { 3, 4 }), 25.0);
    nlc_check_double_equality(norm(f64_2 { 3, 4 }), 5.0);
    nlc_check_double_equality(normalize(f64_2 { 2, 0 }), (f64_2 { 1, 0 }));
    nlc_check_double_equality(normalize(f64_2 { 0, 5 }), (f64_2 { 0, 1 }));
    nlc_check_double_equality(sqrdistance(f64_2 { 5, 6 }, f64_2 { 8, 10 }), 25.0);
    nlc_check_double_equality(distance(f64_2 { 5, 6 }, f64_2 { 8, 10 }), 5.0);

    nlc_check_double_equality(reflect(f64_2 { 2, -2 }, f64_2 { 0, -1 }), (f64_2 { 2, 2 }));
    nlc_check_double_equality(reflect(f64_2 { 2, 0 }, f64_2 { -1, 0 }), (f64_2 { -2, 0 }));
    nlc_check_double_equality(normalize_reflect(f64_2 { 2, -2 }, f64_2 { 0, -5 }), (f64_2 { 2, 2 }));
    nlc_check_double_equality(normalize_reflect(f64_2 { 2, 0 }, f64_2 { -8, 0 }), (f64_2 { -2, 0 }));
    nlc_check_double_equality(project(f64_2 { 2, 2 }, f64_2 { -1, 0 }), (f64_2 { 0, 2 }));
    nlc_check_double_equality(normalize_project(f64_2 { 2, 2 }, f64_2 { -6, 0 }), (f64_2 { 0, 2 }));
    nlc_check_double_equality(rnorm(f64_2 { 3, 4 }), 0.2);
    nlc_check_double_equality(normalize(f64_2 { 3, 4 }), (f64_2 { 0.6, 0.8 }));
}

nlc_test("u8_2 lerp") {
    nlc_check(scalar_lerp(u8_2 { 0, 0 }, u8_2 { 255, 255 }, 0.5f) == u8_2 { 127, 127 });
    nlc_check(scalar_lerp(u8_2 { 0, 0 }, u8_2 { 255, 255 }, 0.5) == u8_2 { 127, 127 });
}
