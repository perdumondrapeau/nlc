/*  Copyright © 2023-2023
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/maths/fixed_point.hpp>
#include <nlc/maths/float_cmp.hpp>
#include <nlc/test.hpp>

using fp32 = nlc::fixed_point<i32, 16>;
using fp64 = nlc::fixed_point<i64, 32>;

constexpr auto f0 = 0.0;
constexpr auto f1 = -0.0;
constexpr auto f2 = 1.0;
constexpr auto f3 = -1.0;
constexpr auto f4 = 13.326;
constexpr auto f5 = -13.326;
constexpr auto f6 = 13000.5326;
constexpr auto f7 = -13000.5326;

nlc_test("from") {
    nlc_check_msg(fp32::from<f64>(f0)._mantissa == 0, "mantissa for f0=", fp32::from<f64>(f0)._mantissa);
    nlc_check_msg(fp32::from<f64>(f1)._mantissa == 0, "mantissa for f1=", fp32::from<f64>(f1)._mantissa);
    nlc_check_msg(fp32::from<f64>(f2)._mantissa == 65536, "mantissa for f2=", fp32::from<f64>(f2)._mantissa);
    nlc_check_msg(fp32::from<f64>(f3)._mantissa == -65536, "mantissa for f3=", fp32::from<f64>(f3)._mantissa);
    nlc_check_msg(fp32::from<f64>(f4)._mantissa == 873332, "mantissa for f4=", fp32::from<f64>(f4)._mantissa);
    nlc_check_msg(fp32::from<f64>(f5)._mantissa == -873332,
                  "mantissa for f5=",
                  fp32::from<f64>(f5)._mantissa);
    nlc_check_msg(fp32::from<f64>(f6)._mantissa == 852002904,
                  "mantissa for f6=",
                  fp32::from<f64>(f6)._mantissa);
    nlc_check_msg(fp32::from<f64>(f7)._mantissa == -852002904,
                  "mantissa for f7=",
                  fp32::from<f64>(f7)._mantissa);
}

nlc_test("to") {
    constexpr auto fp0 = fp32::from<f64>(f0);
    constexpr auto fp1 = fp32::from<f64>(f1);
    constexpr auto fp2 = fp32::from<f64>(f2);
    constexpr auto fp3 = fp32::from<f64>(f3);
    constexpr auto fp4 = fp32::from<f64>(f4);
    constexpr auto fp5 = fp32::from<f64>(f5);
    constexpr auto fp6 = fp32::from<f64>(f6);
    constexpr auto fp7 = fp32::from<f64>(f7);

    constexpr auto r0 = fp0.to<f64>();
    constexpr auto r1 = fp1.to<f64>();
    constexpr auto r2 = fp2.to<f64>();
    constexpr auto r3 = fp3.to<f64>();
    constexpr auto r4 = fp4.to<f64>();
    constexpr auto r5 = fp5.to<f64>();
    constexpr auto r6 = fp6.to<f64>();
    constexpr auto r7 = fp7.to<f64>();

    nlc_check_msg(r0 == 0.0, "r0=", r0);
    nlc_check_msg(r1 == -0.0, "r1=", r1);
    nlc_check_msg(r2 == 1.0, "r2=", r2);
    nlc_check_msg(r3 == -1.0, "r3=", r3);
    nlc_check_msg(r4 == 13.32598876953125, "r4=", r4);
    nlc_check_msg(r5 == -13.32598876953125, "r5=", r5);
    nlc_check_msg(r6 == 13000.532592773438, "r6=", r6);
    nlc_check_msg(r7 == -13000.532592773438, "r7=", r7);
}

nlc_test("mul32") {
    constexpr auto fp0 = fp32::from<f64>(f0);
    constexpr auto fp1 = fp32::from<f64>(f1);
    constexpr auto fp2 = fp32::from<f64>(f2);
    constexpr auto fp3 = fp32::from<f64>(f3);
    constexpr auto fp4 = fp32::from<f64>(f4);
    constexpr auto fp5 = fp32::from<f64>(f5);

    // we do not use f6 and f7 because it overflow the capacity of fp32
    auto r0 = fp0 * fp1;
    auto r1 = fp1 * fp2;
    auto r2 = fp2 * fp3;
    auto r3 = fp3 * fp4;
    auto r4 = fp4 * fp5;

    nlc_check_msg(r0 == fp32::from<f64>(f0 * f1), "r0=", r0, " should be=", f0 * f1);
    nlc_check_msg(r1 == fp32::from<f64>(f1 * f2), "r1=", r1, " should be=", f1 * f2);
    nlc_check_msg(r2 == fp32::from<f64>(f2 * f3), "r2=", r2, " should be=", f2 * f3);
    nlc_check_msg(r3 == fp32::from<f64>(f3 * f4), "r3=", r3, " should be=", f3 * f4);
    nlc_check_msg(nlc::are_floats_close_absolute_value_wise(r4.to<f64>(), f4 * f5, 0.001),
                  "r4=",
                  r4,
                  " should be=",
                  f4 * f5);
}

nlc_test("mul64") {
    constexpr auto fp0 = fp64::from<f64>(f0);
    constexpr auto fp1 = fp64::from<f64>(f1);
    constexpr auto fp2 = fp64::from<f64>(f2);
    constexpr auto fp3 = fp64::from<f64>(f3);
    constexpr auto fp4 = fp64::from<f64>(f4);
    constexpr auto fp5 = fp64::from<f64>(f5);
    constexpr auto fp6 = fp64::from<f64>(f6);
    constexpr auto fp7 = fp64::from<f64>(f7);

    auto r0 = fp0 * fp1;
    auto r1 = fp1 * fp2;
    auto r2 = fp2 * fp3;
    auto r3 = fp3 * fp4;
    auto r4 = fp4 * fp5;
    auto r5 = fp5 * fp6;
    auto r6 = fp6 * fp7;
    auto r7 = fp7 * fp0;

    nlc_check_msg(r0 == fp64::from<f64>(f0 * f1), "r0=", r0, " should be=", f0 * f1);
    nlc_check_msg(r1 == fp64::from<f64>(f1 * f2), "r1=", r1, " should be=", f1 * f2);
    nlc_check_msg(r2 == fp64::from<f64>(f2 * f3), "r2=", r2, " should be=", f2 * f3);
    nlc_check_msg(r3 == fp64::from<f64>(f3 * f4), "r3=", r3, " should be=", f3 * f4);
    nlc_check_msg(nlc::are_floats_close_absolute_value_wise(r4.to<f64>(), f4 * f5, 0.00001),
                  "r4=",
                  r4,
                  " should be=",
                  f4 * f5);
    nlc_check_msg(nlc::are_floats_close_absolute_value_wise(r5.to<f64>(), f5 * f6, 0.00001),
                  "r5=",
                  r5,
                  " should be=",
                  f5 * f6);
    nlc_check_msg(nlc::are_floats_close_absolute_value_wise(r6.to<f64>(), f6 * f7, 0.00001),
                  "r6=",
                  r6,
                  " should be=",
                  f6 * f7);
    nlc_check_msg(r7 == fp64::from<f64>(f7 * f0), "r7=", r7, " should be=", f7 * f0);
}

nlc_test("div32") {
    constexpr auto fp1 = fp32::from<f64>(f1);
    constexpr auto fp2 = fp32::from<f64>(f2);
    constexpr auto fp3 = fp32::from<f64>(f3);
    constexpr auto fp4 = fp32::from<f64>(f4);
    constexpr auto fp5 = fp32::from<f64>(f5);
    constexpr auto fp6 = fp32::from<f64>(f6);
    constexpr auto fp7 = fp32::from<f64>(f7);

    auto r0 = fp1 / fp2;
    auto r1 = fp2 / fp3;
    auto r2 = fp3 / fp4;
    auto r3 = fp4 / fp5;
    auto r4 = fp5 / fp6;
    auto r5 = fp6 / fp7;

    nlc_check_msg(r0 == fp32::from<f64>(f1 / f2), "r0=", r0, " should be=", f1 / f2);
    nlc_check_msg(r1 == fp32::from<f64>(f2 / f3), "r1=", r1, " should be=", f2 / f3);
    nlc_check_msg(r2 == fp32::from<f64>(f3 / f4), "r2=", r2, " should be=", f3 / f4);
    nlc_check_msg(r3 == fp32::from<f64>(f4 / f5), "r3=", r3, " should be=", f4 / f5);
    nlc_check_msg(r4 == fp32::from<f64>(f5 / f6), "r4=", r4, " should be=", f5 / f6);
    nlc_check_msg(r5 == fp32::from<f64>(f6 / f7), "r5=", r5, " should be=", f6 / f7);
}

nlc_test("floor") {
    constexpr auto fp0 = fp32::from<f64>(f0);
    constexpr auto fp1 = fp32::from<f64>(f1);
    constexpr auto fp2 = fp32::from<f64>(f2);
    constexpr auto fp3 = fp32::from<f64>(f3);
    constexpr auto fp4 = fp32::from<f64>(f4);
    constexpr auto fp5 = fp32::from<f64>(f5);
    constexpr auto fp6 = fp32::from<f64>(f6);
    constexpr auto fp7 = fp32::from<f64>(f7);

    constexpr auto r0 = floor(fp0);
    constexpr auto r1 = floor(fp1);
    constexpr auto r2 = floor(fp2);
    constexpr auto r3 = floor(fp3);
    constexpr auto r4 = floor(fp4);
    constexpr auto r5 = floor(fp5);
    constexpr auto r6 = floor(fp6);
    constexpr auto r7 = floor(fp7);

    nlc_check_msg(r0 == fp32::from<f64>(0.0), "r0=", r0);
    nlc_check_msg(r1 == fp32::from<f64>(0.0), "r1=", r1);
    nlc_check_msg(r2 == fp32::from<f64>(1.0), "r2=", r2);
    nlc_check_msg(r3 == fp32::from<f64>(-1.0), "r3=", r3);
    nlc_check_msg(r4 == fp32::from<f64>(13.0), "r4=", r4);
    nlc_check_msg(r5 == fp32::from<f64>(-14.0), "r5=", r5);
    nlc_check_msg(r6 == fp32::from<f64>(13000.0), "r6=", r6);
    nlc_check_msg(r7 == fp32::from<f64>(-13001.0), "r7=", r7);
}

nlc_test("ceil") {
    constexpr auto fp0 = fp32::from<f64>(f0);
    constexpr auto fp1 = fp32::from<f64>(f1);
    constexpr auto fp2 = fp32::from<f64>(f2);
    constexpr auto fp3 = fp32::from<f64>(f3);
    constexpr auto fp4 = fp32::from<f64>(f4);
    constexpr auto fp5 = fp32::from<f64>(f5);
    constexpr auto fp6 = fp32::from<f64>(f6);
    constexpr auto fp7 = fp32::from<f64>(f7);

    constexpr auto r0 = ceil(fp0);
    constexpr auto r1 = ceil(fp1);
    constexpr auto r2 = ceil(fp2);
    constexpr auto r3 = ceil(fp3);
    constexpr auto r4 = ceil(fp4);
    constexpr auto r5 = ceil(fp5);
    constexpr auto r6 = ceil(fp6);
    constexpr auto r7 = ceil(fp7);

    nlc_check_msg(r0 == fp32::from<f64>(0.0), "r0=", r0);
    nlc_check_msg(r1 == fp32::from<f64>(0.0), "r1=", r1);
    nlc_check_msg(r2 == fp32::from<f64>(1.0), "r2=", r2);
    nlc_check_msg(r3 == fp32::from<f64>(-1.0), "r3=", r3);
    nlc_check_msg(r4 == fp32::from<f64>(14.0), "r4=", r4);
    nlc_check_msg(r5 == fp32::from<f64>(-13.0), "r5=", r5);
    nlc_check_msg(r6 == fp32::from<f64>(13001.0), "r6=", r6);
    nlc_check_msg(r7 == fp32::from<f64>(-13000.0), "r7=", r7);
}

nlc_test("trunc") {
    constexpr auto fp0 = fp32::from<f64>(f0);
    constexpr auto fp1 = fp32::from<f64>(f1);
    constexpr auto fp2 = fp32::from<f64>(f2);
    constexpr auto fp3 = fp32::from<f64>(f3);
    constexpr auto fp4 = fp32::from<f64>(f4);
    constexpr auto fp5 = fp32::from<f64>(f5);
    constexpr auto fp6 = fp32::from<f64>(f6);
    constexpr auto fp7 = fp32::from<f64>(f7);

    constexpr auto r0 = trunc(fp0);
    constexpr auto r1 = trunc(fp1);
    constexpr auto r2 = trunc(fp2);
    constexpr auto r3 = trunc(fp3);
    constexpr auto r4 = trunc(fp4);
    constexpr auto r5 = trunc(fp5);
    constexpr auto r6 = trunc(fp6);
    constexpr auto r7 = trunc(fp7);

    nlc_check_msg(r0 == fp32::from<f64>(0.0), "r0=", r0);
    nlc_check_msg(r1 == fp32::from<f64>(0.0), "r1=", r1);
    nlc_check_msg(r2 == fp32::from<f64>(1.0), "r2=", r2);
    nlc_check_msg(r3 == fp32::from<f64>(-1.0), "r3=", r3);
    nlc_check_msg(r4 == fp32::from<f64>(13.0), "r4=", r4);
    nlc_check_msg(r5 == fp32::from<f64>(-13.0), "r5=", r5);
    nlc_check_msg(r6 == fp32::from<f64>(13000.0), "r6=", r6);
    nlc_check_msg(r7 == fp32::from<f64>(-13000.0), "r7=", r7);
}

nlc_test("round") {
    constexpr auto fp0 = fp32::from<f64>(f0);
    constexpr auto fp1 = fp32::from<f64>(f1);
    constexpr auto fp2 = fp32::from<f64>(f2);
    constexpr auto fp3 = fp32::from<f64>(f3);
    constexpr auto fp4 = fp32::from<f64>(f4);
    constexpr auto fp5 = fp32::from<f64>(f5);
    constexpr auto fp6 = fp32::from<f64>(f6);
    constexpr auto fp7 = fp32::from<f64>(f7);

    constexpr auto r0 = round(fp0);
    constexpr auto r1 = round(fp1);
    constexpr auto r2 = round(fp2);
    constexpr auto r3 = round(fp3);
    constexpr auto r4 = round(fp4);
    constexpr auto r5 = round(fp5);
    constexpr auto r6 = round(fp6);
    constexpr auto r7 = round(fp7);

    nlc_check_msg(r0 == fp32::from<f64>(0.0), "r0=", r0);
    nlc_check_msg(r1 == fp32::from<f64>(0.0), "r1=", r1);
    nlc_check_msg(r2 == fp32::from<f64>(1.0), "r2=", r2);
    nlc_check_msg(r3 == fp32::from<f64>(-1.0), "r3=", r3);
    nlc_check_msg(r4 == fp32::from<f64>(13.0), "r4=", r4);
    nlc_check_msg(r5 == fp32::from<f64>(-13.0), "r5=", r5);
    nlc_check_msg(r6 == fp32::from<f64>(13001.0), "r6=", r6);
    nlc_check_msg(r7 == fp32::from<f64>(-13001.0), "r7=", r7);
}
