/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/maths/float_cmp.hpp>
#include <nlc/meta/numeric_limits.hpp>
#include <nlc/test.hpp>

namespace impl {
inline auto are_floats_close_ulp_wise(f32 const a, f32 const b, usize const max_ulps) -> bool {
    auto const res = nlc::are_floats_close_ulp_wise(a, b, max_ulps);
    auto const inverted_res = nlc::are_floats_close_ulp_wise(b, a, max_ulps);
    nlc_check_msg(res == inverted_res, "are_floats_close_ulp_wise: ", a, ", ", b, ", ", max_ulps);
    return res;
}

inline auto are_floats_close_ulp_wise(f64 const a, f64 const b, usize const max_ulps) -> bool {
    auto const res = nlc::are_floats_close_ulp_wise(a, b, max_ulps);
    auto const inverted_res = nlc::are_floats_close_ulp_wise(b, a, max_ulps);
    nlc_check_msg(res == inverted_res, "are_floats_close_ulp_wise: ", a, ", ", b, ", ", max_ulps);
    return res;
}

inline auto are_floats_close_relative_value_wise(
    f32 const a,
    f32 const b,
    f32 const relative_epsilon = nlc::meta::limits<f32>::epsilon) -> bool {
    auto const res = nlc::are_floats_close_relative_value_wise(a, b, relative_epsilon);
    auto const inverted_res = nlc::are_floats_close_relative_value_wise(b, a, relative_epsilon);
    nlc_check_msg(res == inverted_res, "are_floats_close_relative_value_wise: ", a, ", ", b, ", ", relative_epsilon);
    return res;
}

inline auto are_floats_close_relative_value_wise(
    f64 const a,
    f64 const b,
    f64 const relative_epsilon = nlc::meta::limits<f64>::epsilon) -> bool {
    auto const res = nlc::are_floats_close_relative_value_wise(a, b, relative_epsilon);
    auto const inverted_res = nlc::are_floats_close_relative_value_wise(b, a, relative_epsilon);
    nlc_check_msg(res == inverted_res, "are_floats_close_relative_value_wise: ", a, ", ", b, ", ", relative_epsilon);
    return res;
}

inline auto are_floats_close_absolute_value_wise(
    f32 const a,
    f32 const b,
    f32 const absolute_epsilon = nlc::meta::limits<f32>::epsilon) -> bool {
    auto const res = nlc::are_floats_close_absolute_value_wise(a, b, absolute_epsilon);
    auto const inverted_res = nlc::are_floats_close_absolute_value_wise(b, a, absolute_epsilon);
    nlc_check_msg(res == inverted_res, "are_floats_close_absolute_value_wise: ", a, ", ", b, ", ", absolute_epsilon);
    return res;
}

inline auto are_floats_close_absolute_value_wise(
    f64 const a,
    f64 const b,
    f64 const absolute_epsilon = nlc::meta::limits<f64>::epsilon) -> bool {
    auto const res = nlc::are_floats_close_absolute_value_wise(a, b, absolute_epsilon);
    auto const inverted_res = nlc::are_floats_close_absolute_value_wise(b, a, absolute_epsilon);
    nlc_check_msg(res == inverted_res, "are_floats_close_absolute_value_wise: ", a, ", ", b, ", ", absolute_epsilon);
    return res;
}
}  // namespace impl

nlc_test("compare with self") {
    constexpr float f1 = 4.2f;
    constexpr auto f2 = f1;

    nlc_check_msg(impl::are_floats_close_ulp_wise(f1, f2, 0u), "f1=", f1, ", f2=", f2);
    nlc_check_msg(impl::are_floats_close_relative_value_wise(f1, f2), "f1=", f1, ", f2=", f2);
    nlc_check_msg(impl::are_floats_close_relative_value_wise(f1, f2, 0.f), "f1=", f1, ", f2=", f2);
    nlc_check_msg(impl::are_floats_close_absolute_value_wise(f1, f2), "f1=", f1, ", f2=", f2);
    nlc_check_msg(impl::are_floats_close_absolute_value_wise(f1, f2, 0.f), "f1=", f1, ", f2=", f2);

    constexpr double d1 = 4.2;
    constexpr auto d2 = d1;
    nlc_check_msg(impl::are_floats_close_ulp_wise(d1, d2, 0u), "d1=", d1, ", d2=", d2);
    nlc_check_msg(impl::are_floats_close_relative_value_wise(d1, d2), "d1=", d1, ", d2=", d2);
    nlc_check_msg(impl::are_floats_close_relative_value_wise(d1, d2, 0.), "d1=", d1, ", d2=", d2);
    nlc_check_msg(impl::are_floats_close_absolute_value_wise(d1, d2), "d1=", d1, ", d2=", d2);
    nlc_check_msg(impl::are_floats_close_absolute_value_wise(d1, d2, 0.), "d1=", d1, ", d2=", d2);
}

nlc_test("compare with zero") {
    constexpr auto f = 1.337f;

    nlc_check_msg(impl::are_floats_close_ulp_wise(f, 0.f, 4u) == false, "f=", f);
    nlc_check_msg(impl::are_floats_close_relative_value_wise(f, 0.f) == false, "f=", f);
    nlc_check_msg(impl::are_floats_close_relative_value_wise(f, 0.f, 1.f), "f=", f);
    nlc_check_msg(impl::are_floats_close_absolute_value_wise(f, 0.f) == false, "f=", f);
    nlc_check_msg(impl::are_floats_close_absolute_value_wise(f, 0.f, f), "f=", f);

    constexpr auto d = 1.337;

    nlc_check_msg(impl::are_floats_close_ulp_wise(d, 0., 4u) == false, "d=", d);
    nlc_check_msg(impl::are_floats_close_relative_value_wise(d, 0.) == false, "d=", d);
    nlc_check_msg(impl::are_floats_close_relative_value_wise(d, 0., 1.), "d=", d);
    nlc_check_msg(impl::are_floats_close_absolute_value_wise(d, 0.) == false, "d=", d);
    nlc_check_msg(impl::are_floats_close_absolute_value_wise(d, 0., d), "d=", d);
}

nlc_test("compare 2 close floats") {
    constexpr auto f1 = 67329.234f;  // arbitrarily chosen
    constexpr auto f2 = 67329.242f;  // exactly one ULP away
    nlc_check_msg(impl::are_floats_close_ulp_wise(f1, f2, 1u), "f1=", f1, ", f2=", f2);
    nlc_check_msg(impl::are_floats_close_ulp_wise(f1, f2, 0u) == false, "f1=", f1, ", f2=", f2);
    nlc_check_msg(impl::are_floats_close_relative_value_wise(f1, f2), "f1=", f1, ", f2=", f2);
    nlc_check_msg(impl::are_floats_close_absolute_value_wise(f1, f2, 0.01f), "f1=", f1, ", f2=", f2);
}

nlc_test("compare 2 small float diff to zero") {
    constexpr auto f1 = 67329.234f;  // arbitrarily chosen
    constexpr auto f2 = 67329.242f;  // exactly one ULP away
    constexpr auto diff = f2 - f1;   // 0.0078125000
    nlc_check_msg(impl::are_floats_close_ulp_wise(diff, 0.f, 1u) == false, "diff=", diff);
    nlc_check_msg(impl::are_floats_close_relative_value_wise(diff, 0.f) == false, "diff=", diff);
    nlc_check_msg(impl::are_floats_close_relative_value_wise(diff, 0.f, 1.f), "diff=", diff);
    nlc_check_msg(impl::are_floats_close_absolute_value_wise(diff, 0.f) == false, "diff=", diff);
    nlc_check_msg(impl::are_floats_close_absolute_value_wise(diff, 0.f, 0.01f), "diff=", diff);
}
