/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/pair.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/maths/geometry2d/aabb2.hpp>
#include <nlc/test.hpp>

using namespace nlc;

#define verbose_static_assert(X) static_assert(X, "assert '" #X "' failed")

nlc_test("constructors") {
    [[maybe_unused]] constexpr aabb2 r1;

    constexpr aabb2 r2 { { 1.f, 2.5f }, { 50.f, 100.f } };
    verbose_static_assert(r2.min.x == 1.f);
    verbose_static_assert(r2.min.y == 2.5f);
    verbose_static_assert(r2.max.x == 50.f);
    verbose_static_assert(r2.max.y == 100.f);

    constexpr f32_2 min { -5.f, -7.f };
    constexpr f32_2 max { 30.f, -57.f };
    constexpr aabb2 r3 { min, max };
    verbose_static_assert(r3.min == min);
    verbose_static_assert(r3.max == max);

    constexpr f32_4 values { 12.f, -35.f, 0.5f, 0.1f };
    constexpr aabb2 r4 = aabb2::from_f32_4(values);
    verbose_static_assert(r4.min.x == 12.f);
    verbose_static_assert(r4.min.y == -35.f);
    verbose_static_assert(r4.max.x == 0.5f);
    verbose_static_assert(r4.max.y == 0.1f);

    constexpr aabb2 r5 { r4 };
    verbose_static_assert(r5 == r4);
}

nlc_test("copy") {
    constexpr aabb2 r1 { { 1.f, 2.5f }, { 50.f, 100.f } };
    aabb2 r2 { { 12.f, -35.f }, { 0.5f, 0.1f } };
    r2 = r1;

    nlc_check(r2.min == f32_2 { 1.f, 2.5f });
    nlc_check(r2.max == f32_2 { 50.f, 100.f });
}

nlc_test("[in]equality") {
    constexpr aabb2 r1 { { 1.f, 2.5f }, { 50.f, 100.f } };
    constexpr aabb2 r2 { { 1.f, 2.5f }, { 50.f, 100.f } };
    verbose_static_assert(r1 == r2);
    verbose_static_assert((r1 != r2) == false);

    constexpr aabb2 r3 { { -1.f, -2.5f }, { 5.f, 0.6f } };
    verbose_static_assert(r3 != r1);
    verbose_static_assert(r3 != r1);
    verbose_static_assert((r3 == r2) == false);
    verbose_static_assert((r3 == r1) == false);
}

nlc_test("is_empty") {
    // Explicitly empty rects
    {
        // Null width
        constexpr aabb2 r1 { { -5.f, 2.f }, { -5.f, 2.f } };
        verbose_static_assert(r1.is_empty());
    }

    // Non-empty rects
    {
        constexpr aabb2 r1 { { 1.f, 2.5f }, { 50.f, 100.f } };
        verbose_static_assert(r1.is_empty() == false);

        // Negative width
        constexpr aabb2 r2 { { 1.f, 2.5f }, { -50.f, 100.f } };
        verbose_static_assert(r2.is_empty() == false);
    }

    // Implicit construction
    constexpr aabb2 r;
    verbose_static_assert(r.is_empty());
}

nlc_test("center") {
    // Empty AABB
    constexpr aabb2 r1;
    constexpr auto c1 = r1.center();
    verbose_static_assert(c1 == r1.min);

    // Positive width & height
    constexpr auto c2 = aabb2 { { -4.f, -2.f }, { 4.f, 4.f } }.center();
    verbose_static_assert(c2.x == 0.f);
    verbose_static_assert(c2.y == 1.f);
}

nlc_test("from_pos_and_size") {
    constexpr f32_2 origin { 0.f, 0.f };

    auto const b1 = aabb2::from_pos_and_size(origin, { 0.f, 0.f });
    nlc_check(b1.is_empty());

    f32_2 const size2 { 5.f, 9.f };
    auto const b2 = aabb2::from_pos_and_size(origin, size2);
    auto const s2 = b2.size();
    nlc_check(b2.min == origin);
    nlc_check(s2 == size2);

    // Random
    auto const b3 = aabb2::from_pos_and_size({ 1.75f, 0.f }, { 3.5f, 6.f });
    nlc_check(b3.min == f32_2 { 1.75f, 0.f });
    nlc_check(b3.size() == f32_2 { 3.5f, 6.f });
}

nlc_test("from_centered_pos_and_size") {
    constexpr f32_2 origin { 0.f, 0.f };

    // Null-sized
    auto const b1 = aabb2::from_centered_pos_and_size(origin, { 0.f, 0.f });
    nlc_check(b1.is_empty());

    // Origin-centered
    f32_2 const size2 { 5.f, 9.f };
    auto const b2 = aabb2::from_centered_pos_and_size(origin, size2);
    auto const s2 = b2.size();
    nlc_check(b2.center() == origin);
    nlc_check(s2 == size2);

    // Random
    auto const b3 = aabb2::from_centered_pos_and_size({ 1.75f, 0.f }, { 3.5f, 6.f });
    nlc_check(b3.min == f32_2 { 0.f, -3.f });
    nlc_check(b3.size() == f32_2 { 3.5f, 6.f });
}

nlc_test("contains point") {
    // Inside / outside points
    {
        constexpr aabb2 r { { -5.f, -2.f }, { 5.f, 2.f } };
        verbose_static_assert(r.contains(r.center()));

        constexpr f32_2 inside_points[] = { { 0.f, 0.f },
                                            { -2.f, -1.f },
                                            { 1.f, -1.5f },
                                            { 3.f, 0.2f },
                                            { -4.5f, 0.75f } };
        constexpr f32_2 outside_points[] = { { -7.f, 5.f },    // top-left
                                             { 0.f, 5.f },     // top-middle
                                             { 7.f, 5.f },     // top-right
                                             { 7.f, 0.f },     // middle-right
                                             { 7.f, -5.f },    // bottom-right
                                             { 0.f, -5.f },    // bottom-midle
                                             { -7.f, -5.f },   // bottom-left
                                             { -7.f, 0.f } };  // middle-left

        for (auto const & p : inside_points) {
            nlc_check(r.contains(p));
        }

        for (auto const & p : outside_points) {
            nlc_check(r.contains(p) == false);
        }
    }

    // Borders
    {
        constexpr aabb2 r { { -5.f, -2.f }, { 5.f, 2.f } };
        const nlc::pair<f32_2, bool> points[] = {
            { f32_2 { -5.f, 2.f }, true },   // top-left corner
            { f32_2 { 5.f, 2.f }, true },    // top-right corner
            { f32_2 { 5.f, 0.f }, true },    // right border
            { f32_2 { 5.f, -2.f }, true },   // bottom-right corner
            { f32_2 { 0.f, -2.f }, true },   // bottom border
            { f32_2 { -5.f, -2.f }, true },  // bottom-left corner
            { f32_2 { -5.f, 0.f }, true }    // left border
        };

        for (auto const & [p, contained] : points) {
            nlc_check(r.contains(p) == contained);
        }
    }

    // Empty AABB
    constexpr aabb2 r;
    verbose_static_assert(r.contains(r.center()));
}

nlc_test("contains AABB") {
    aabb2 const r1 { { -5.f, -2.f }, { 5.f, 2.f } };
    nlc_check(r1.contains(r1));

    // Half AABB, centered
    aabb2 const r2 = aabb2::from_pos_and_size(r1.center(), r1.size() * 0.5f);
    nlc_check(r1.contains(r2));
    nlc_check(r2.contains(r1) == false);

    // Half AABB touching left border
    aabb2 const r3 =
        aabb2::from_pos_and_size({ r1.min.x, r1.min.y + r1.size().y * 0.25f }, r1.size() * 0.5f);
    nlc_check(r1.contains(r3));
    nlc_check(r3.contains(r1) == false);

    // Half AABB touching top border
    aabb2 const r4 =
        aabb2::from_pos_and_size({ r1.min.x + r1.size().x * 0.25f, r1.min.y + r1.size().y * 0.5f },
                                 r1.size() * 0.5f);
    nlc_check(r1.contains(r4));
    nlc_check(r4.contains(r1) == false);

    // Half AABB touching right border
    aabb2 const r5 =
        aabb2::from_pos_and_size({ r1.min.x + r1.size().x * 0.5f, r1.min.y + r1.size().y * 0.25f },
                                 r1.size() * 0.5f);
    nlc_check(r1.contains(r5));
    nlc_check(r5.contains(r1) == false);

    // Half AABB touching bottom border
    aabb2 const r6 =
        aabb2::from_pos_and_size({ r1.min.x + r1.size().x * 0.25f, r1.min.y }, r1.size() * 0.5f);
    nlc_check(r1.contains(r6));
    nlc_check(r6.contains(r1) == false);

    // Double AABB with no intersection
    aabb2 const r7 = aabb2::from_pos_and_size(r1.min - r1.size() * 0.5f, r1.size() * 2.f);
    nlc_check(r1.contains(r7) == false);
    nlc_check(r7.contains(r1));

    // aabb2 intersecting top & left
    aabb2 const r8 =
        aabb2::from_pos_and_size({ r1.min.x - r1.size().x * 0.5f, r1.min.y + r1.size().y * 0.5f },
                                 r1.size());
    nlc_check(r1.contains(r8) == false);
    nlc_check(r8.contains(r1) == false);

    // aabb2 intersection bottom & right
    aabb2 const r9 =
        aabb2::from_pos_and_size({ r1.min.x + r1.size().x * 0.5f, r1.min.y - r1.size().y * 0.5f },
                                 r1.size());
    nlc_check(r1.contains(r9) == false);
    nlc_check(r9.contains(r1) == false);
}

nlc_test("merge simple") {
    // Same AABB
    constexpr aabb2 r1 { { -6.f, -8.f }, { 5.f, 10.f } };
    auto const b1 = aabb2::merge(r1, r1);
    nlc_check(b1 == r1);

    constexpr aabb2 r2 { { -5.f, -8.f }, { 100.f, 4.f } };
    constexpr aabb2 r3 { { 0.f, 0.f }, { 5.f, 25.f } };
    auto const b2 = aabb2::merge(r2, r3);
    nlc_check(b2.min == f32_2 { -5.f, -8.f });
    nlc_check(b2.size() == f32_2 { 105.f, 33.f });
}

nlc_test("merge span<aabb2>") {
    // Same AABB
    constexpr aabb2 r1 { { -6.f, -8.f }, { -1.f, 2.f } };
    constexpr aabb2 rs1[] = { r1, r1, r1, r1 };
    auto const b1 = aabb2::merge(nlc::make_span(rs1));
    nlc_check(b1 == r1);

    constexpr aabb2 rs2[] = { { { -5.f, -8.f }, { 100.f, 4.f } },
                              { { 0.f, 0.f }, { 5.f, 25.f } },
                              { { -10.f, 2.f }, { -5.f, 56.f } } };
    auto const b2 = aabb2::merge(nlc::make_span(rs2));
    nlc_check(b2.min == f32_2 { -10.f, -8.f });
    nlc_check(b2.size() == f32_2 { 110.f, 64.f });
}
