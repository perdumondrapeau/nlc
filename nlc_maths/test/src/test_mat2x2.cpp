/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/maths/mat2x2.hpp>
#include <nlc/maths/mat2x2_common.hpp>
#include <nlc/maths/vec2.hpp>
#include <nlc/test.hpp>

#include "test_mat_common.hpp"

using namespace nlc;

nlc_test("mat2x2 identity") {
    check_identity<u8, 2>();
    check_identity<i8, 2>();
    check_identity<u16, 2>();
    check_identity<i16, 2>();
    check_identity<u32, 2>();
    check_identity<i32, 2>();
    check_identity<u64, 2>();
    check_identity<i64, 2>();
    check_identity<usize, 2>();
    check_identity<isize, 2>();
    check_identity<f32, 2>();
    check_identity<f64, 2>();
}

nlc_test("mat2x2 accessors") {
    check_accessors<u8, 2>();
    check_accessors<i8, 2>();
    check_accessors<u16, 2>();
    check_accessors<i16, 2>();
    check_accessors<u32, 2>();
    check_accessors<i32, 2>();
    check_accessors<u64, 2>();
    check_accessors<i64, 2>();
    check_accessors<usize, 2>();
    check_accessors<isize, 2>();
    check_accessors<f32, 2>();
    check_accessors<f64, 2>();
}

nlc_test("u8_2x2 operators") {
    constexpr u8_2x2 a { { 2, 4 }, { 6, 3 } };
    constexpr u8_2x2 b { { 5, 1 }, { 0, 4 } };
    constexpr u8_2x2 a_times_b_expected_res { { 16, 23 }, { 24, 12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u8_2x2 { { 2, 4 }, { 6, 3 } } * u8_2 { 6, 2 }, u8_2 { 24, 30 });
    check_vector_value(u8_2 { 6, 2 } * u8_2x2 { { 2, 4 }, { 6, 3 } }, u8_2 { 20, 42 });
}

nlc_test("u8_2x2 common functions") {
    check_matrix_value(nlc::transpose(u8_2x2 { { 2, 4 }, { 6, 3 } }), u8_2x2 { { 2, 6 }, { 4, 3 } });
    nlc_check_msg(nlc::det(u8_2x2 { { 4, 2 }, { 3, 6 } }) == 18,
                  "det=",
                  nlc::det(u8_2x2 { { 4, 2 }, { 3, 6 } }));
}

nlc_test("i8_2x2 operators") {
    constexpr i8_2x2 a { { 2, -4 }, { -6, 3 } };
    constexpr i8_2x2 b { { -5, -1 }, { 0, 4 } };
    constexpr i8_2x2 a_times_b_expected_res { { -4, 17 }, { -24, 12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i8_2x2 { { -2, 4 }, { -6, 3 } } * i8_2 { 6, -2 }, i8_2 { 0, 18 });
    check_vector_value(i8_2 { -6, 2 } * i8_2x2 { { -2, 4 }, { -6, -3 } }, i8_2 { 20, 30 });
}

nlc_test("i8_2x2 common functions") {
    check_matrix_value(nlc::transpose(i8_2x2 { { 2, -4 }, { 6, -3 } }),
                       i8_2x2 { { 2, 6 }, { -4, -3 } });
    nlc_check_msg(nlc::det(i8_2x2 { { -4, -2 }, { -11, -6 } }) == 2,
                  "det=",
                  nlc::det(i8_2x2 { { -4, -2 }, { -3, -6 } }));
    check_matrix_value(nlc::inverse(i8_2x2 { { -4, -2 }, { -11, -6 } }),
                       i8_2x2 { { -3, 1 }, { 5, -2 } });
}

nlc_test("u16_2x2 operators") {
    constexpr u16_2x2 a { { 2, 4 }, { 6, 3 } };
    constexpr u16_2x2 b { { 5, 1 }, { 0, 4 } };
    constexpr u16_2x2 a_times_b_expected_res { { 16, 23 }, { 24, 12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u16_2x2 { { 2, 4 }, { 6, 3 } } * u16_2 { 6, 2 }, u16_2 { 24, 30 });
    check_vector_value(u16_2 { 6, 2 } * u16_2x2 { { 2, 4 }, { 6, 3 } }, u16_2 { 20, 42 });
}

nlc_test("u16_2x2 common functions") {
    check_matrix_value(nlc::transpose(u16_2x2 { { 2, 4 }, { 6, 3 } }), u16_2x2 { { 2, 6 }, { 4, 3 } });
    nlc_check_msg(nlc::det(u16_2x2 { { 4, 2 }, { 3, 6 } }) == 18,
                  "det=",
                  nlc::det(u16_2x2 { { 4, 2 }, { 3, 6 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("i16_2x2 operators") {
    constexpr i16_2x2 a { { 2, -4 }, { -6, 3 } };
    constexpr i16_2x2 b { { -5, -1 }, { 0, 4 } };
    constexpr i16_2x2 a_times_b_expected_res { { -4, 17 }, { -24, 12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i16_2x2 { { -2, 4 }, { -6, 3 } } * i16_2 { 6, -2 }, i16_2 { 0, 18 });
    check_vector_value(i16_2 { -6, 2 } * i16_2x2 { { -2, 4 }, { -6, -3 } }, i16_2 { 20, 30 });
}

nlc_test("i16_2x2 common functions") {
    check_matrix_value(nlc::transpose(i16_2x2 { { 2, -4 }, { 6, -3 } }),
                       i16_2x2 { { 2, 6 }, { -4, -3 } });
    nlc_check_msg(nlc::det(i16_2x2 { { -4, -2 }, { -11, -6 } }) == 2,
                  "det=",
                  nlc::det(i16_2x2 { { -4, -2 }, { -11, -6 } }));
    check_matrix_value(nlc::inverse(i16_2x2 { { -4, -2 }, { -11, -6 } }),
                       i16_2x2 { { -3, 1 }, { 5, -2 } });
}

nlc_test("u32_2x2 operators") {
    constexpr u32_2x2 a { { 2, 4 }, { 6, 3 } };
    constexpr u32_2x2 b { { 5, 1 }, { 0, 4 } };
    constexpr u32_2x2 a_times_b_expected_res { { 16, 23 }, { 24, 12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u32_2x2 { { 2, 4 }, { 6, 3 } } * u32_2 { 6, 2 }, u32_2 { 24, 30 });
    check_vector_value(u32_2 { 6, 2 } * u32_2x2 { { 2, 4 }, { 6, 3 } }, u32_2 { 20, 42 });
}

nlc_test("u32_2x2 common functions") {
    check_matrix_value(nlc::transpose(u32_2x2 { { 2, 4 }, { 6, 3 } }), u32_2x2 { { 2, 6 }, { 4, 3 } });
    nlc_check_msg(nlc::det(u32_2x2 { { 4, 2 }, { 3, 6 } }) == 18,
                  "det=",
                  nlc::det(u32_2x2 { { 4, 2 }, { 3, 6 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("i32_2x2 operators") {
    constexpr i32_2x2 a { { 2, -4 }, { -6, 3 } };
    constexpr i32_2x2 b { { -5, -1 }, { 0, 4 } };
    constexpr i32_2x2 a_times_b_expected_res { { -4, 17 }, { -24, 12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i32_2x2 { { -2, 4 }, { -6, 3 } } * i32_2 { 6, -2 }, i32_2 { 0, 18 });
    check_vector_value(i32_2 { -6, 2 } * i32_2x2 { { -2, 4 }, { -6, -3 } }, i32_2 { 20, 30 });
}

nlc_test("i32_2x2 common functions") {
    check_matrix_value(nlc::transpose(i32_2x2 { { 2, -4 }, { 6, -3 } }),
                       i32_2x2 { { 2, 6 }, { -4, -3 } });
    nlc_check_msg(nlc::det(i32_2x2 { { -4, -2 }, { -11, -6 } }) == 2,
                  "det=",
                  nlc::det(i32_2x2 { { -4, -2 }, { -3, -6 } }));
    check_matrix_value(nlc::inverse(i32_2x2 { { -4, -2 }, { -11, -6 } }),
                       i32_2x2 { { -3, 1 }, { 5, -2 } });
}

nlc_test("u64_2x2 operators") {
    constexpr u64_2x2 a { { 2, 4 }, { 6, 3 } };
    constexpr u64_2x2 b { { 5, 1 }, { 0, 4 } };
    constexpr u64_2x2 a_times_b_expected_res { { 16, 23 }, { 24, 12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u64_2x2 { { 2, 4 }, { 6, 3 } } * u64_2 { 6, 2 }, u64_2 { 24, 30 });
    check_vector_value(u64_2 { 6, 2 } * u64_2x2 { { 2, 4 }, { 6, 3 } }, u64_2 { 20, 42 });
}

nlc_test("u64_2x2 common functions") {
    check_matrix_value(nlc::transpose(u64_2x2 { { 2, 4 }, { 6, 3 } }), u64_2x2 { { 2, 6 }, { 4, 3 } });
    nlc_check_msg(nlc::det(u64_2x2 { { 4, 2 }, { 3, 6 } }) == 18,
                  "det=",
                  nlc::det(u64_2x2 { { 4, 2 }, { 3, 6 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("i64_2x2 operators") {
    constexpr i64_2x2 a { { 2, -4 }, { -6, 3 } };
    constexpr i64_2x2 b { { -5, -1 }, { 0, 4 } };
    constexpr i64_2x2 a_times_b_expected_res { { -4, 17 }, { -24, 12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i64_2x2 { { -2, 4 }, { -6, 3 } } * i64_2 { 6, -2 }, i64_2 { 0, 18 });
    check_vector_value(i64_2 { -6, 2 } * i64_2x2 { { -2, 4 }, { -6, -3 } }, i64_2 { 20, 30 });
}

nlc_test("i64_2x2 common functions") {
    check_matrix_value(nlc::transpose(i64_2x2 { { 2, -4 }, { 6, -3 } }),
                       i64_2x2 { { 2, 6 }, { -4, -3 } });
    nlc_check_msg(nlc::det(i64_2x2 { { -4, -2 }, { -11, -6 } }) == 2,
                  "det=",
                  nlc::det(i64_2x2 { { -4, -2 }, { -3, -6 } }));
    check_matrix_value(nlc::inverse(i64_2x2 { { -4, -2 }, { -11, -6 } }),
                       i64_2x2 { { -3, 1 }, { 5, -2 } });
}

nlc_test("usize_2x2 operators") {
    constexpr usize_2x2 a { { 2, 4 }, { 6, 3 } };
    constexpr usize_2x2 b { { 5, 1 }, { 0, 4 } };
    constexpr usize_2x2 a_times_b_expected_res { { 16, 23 }, { 24, 12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(usize_2x2 { { 2, 4 }, { 6, 3 } } * usize_2 { 6, 2 }, usize_2 { 24, 30 });
    check_vector_value(usize_2 { 6, 2 } * usize_2x2 { { 2, 4 }, { 6, 3 } }, usize_2 { 20, 42 });
}

nlc_test("usize_2x2 common functions") {
    check_matrix_value(nlc::transpose(usize_2x2 { { 2, 4 }, { 6, 3 } }),
                       usize_2x2 { { 2, 6 }, { 4, 3 } });
    nlc_check_msg(nlc::det(usize_2x2 { { 4, 2 }, { 3, 6 } }) == 18,
                  "det=",
                  nlc::det(usize_2x2 { { 4, 2 }, { 3, 6 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("isize_2x2 operators") {
    constexpr isize_2x2 a { { 2, -4 }, { -6, 3 } };
    constexpr isize_2x2 b { { -5, -1 }, { 0, 4 } };
    constexpr isize_2x2 a_times_b_expected_res { { -4, 17 }, { -24, 12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(isize_2x2 { { -2, 4 }, { -6, 3 } } * isize_2 { 6, -2 }, isize_2 { 0, 18 });
    check_vector_value(isize_2 { -6, 2 } * isize_2x2 { { -2, 4 }, { -6, -3 } }, isize_2 { 20, 30 });
}

nlc_test("isize_2x2 common functions") {
    check_matrix_value(nlc::transpose(isize_2x2 { { 2, -4 }, { 6, -3 } }),
                       isize_2x2 { { 2, 6 }, { -4, -3 } });
    nlc_check_msg(nlc::det(isize_2x2 { { -4, -2 }, { -11, -6 } }) == 2,
                  "det=",
                  nlc::det(isize_2x2 { { -4, -2 }, { -3, -6 } }));
    check_matrix_value(nlc::inverse(isize_2x2 { { -4, -2 }, { -11, -6 } }),
                       isize_2x2 { { -3, 1 }, { 5, -2 } });
}

nlc_test("f32_2x2 operators") {
    constexpr f32_2x2 a { { 2.f, -4.f }, { -6.f, 3.f } };
    constexpr f32_2x2 b { { -5.f, -1.f }, { 0.f, 4.f } };
    constexpr f32_2x2 a_times_b_expected_res { { -4.f, 17.f }, { -24.f, 12.f } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(f32_2x2 { { -2.f, 4.f }, { -6.f, 3.f } } * f32_2 { 6.f, -2.f },
                       f32_2 { 0.f, 18.f });
    check_vector_value(f32_2 { -6.f, 2.f } * f32_2x2 { { -2.f, 4.f }, { -6.f, -3.f } },
                       f32_2 { 20.f, 30.f });
}

nlc_test("f32_2x2 common functions") {
    check_matrix_value(nlc::transpose(f32_2x2 { { 2.f, -4.f }, { 6.f, -3.f } }),
                       f32_2x2 { { 2.f, 6.f }, { -4.f, -3.f } });
    nlc_check_msg(nlc::det(f32_2x2 { { -4.f, -2.f }, { -11.f, -6.f } }) == 2.f,
                  "det=",
                  nlc::det(f32_2x2 { { -4.f, -2.f }, { -3.f, -6.f } }));
    check_matrix_value(nlc::inverse(f32_2x2 { { -4.f, -2.f }, { -11.f, -6.f } }),
                       f32_2x2 { { -3.f, 1.f }, { 5.5f, -2.f } });
}

nlc_test("f64_2x2 operators") {
    constexpr f64_2x2 a { { 2., -4. }, { -6., 3. } };
    constexpr f64_2x2 b { { -5., -1. }, { 0., 4. } };
    constexpr f64_2x2 a_times_b_expected_res { { -4., 17. }, { -24., 12. } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(f64_2x2 { { -2., 4. }, { -6., 3. } } * f64_2 { 6., -2. }, f64_2 { 0., 18. });
    check_vector_value(f64_2 { -6., 2. } * f64_2x2 { { -2., 4. }, { -6., -3. } }, f64_2 { 20., 30. });
}

nlc_test("f64_2x2 common functions") {
    check_matrix_value(nlc::transpose(f64_2x2 { { 2., -4. }, { 6., -3. } }),
                       f64_2x2 { { 2., 6. }, { -4., -3. } });
    nlc_check_msg(nlc::det(f64_2x2 { { -4., -2. }, { -11., -6. } }) == 2.,
                  "det=",
                  nlc::det(f64_2x2 { { -4., -2. }, { -3., -6. } }));
    check_matrix_value(nlc::inverse(f64_2x2 { { -4., -2. }, { -11., -6. } }),
                       f64_2x2 { { -3., 1. }, { 5.5, -2. } });
}
