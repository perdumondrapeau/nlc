/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/maths/float_cmp.hpp>
#include <nlc/maths/vec3.hpp>
#include <nlc/maths/vec3_common.hpp>
#include <nlc/test.hpp>

#include "check_float.hpp"

using namespace nlc;

nlc_test("u8_3 operators") {
    u8_3 a { .x = 6, .y = 3, .z = 3 };
    u8_3 b { .x = 3, .y = 4, .z = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3 && b.z == 3, "x=", b.x, ", y=", b.y, ", z=", b.z);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2 && b.z == 2, "x=", b.x, ", y=", b.y, ", z=", b.z);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5 && a.z == 5, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 1, 13, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15 && a.z == 15, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8 && a.z == 8, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0 && a.z == 0, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 7, 13, 13 };
    a %= u8_3 { 5, 4, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check((u8_3 { 1, 13, 13 } | u8_3 { 1, 2, 2 }) == u8_3 { 1, 15, 15 });
    nlc_check((u8_3 { 1, 13, 13 } & u8_3 { 1, 3, 3 }) == u8_3 { 1, 1, 1 });
    nlc_check((u8_3 { 24, 13, 13 } >> u8_3 { 2, 1, 1 }) == u8_3 { 6, 6, 6 });
    nlc_check((u8_3 { 1, 13, 13 } << u8_3 { 3, 2, 2 }) == u8_3 { 8, 52, 52 });
    nlc_check((u8_3 { 1, 13, 13 } ^ u8_3 { 3, 2, 2 }) == u8_3 { 2, 15, 15 });
    nlc_check((u8_3 { 7, 13, 13 } % u8_3 { 5, 4, 4 }) == u8_3 { 2, 1, 1 });

    u8 const cst { 3 };
    a = { 1, 13, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 13 && a.y == 13 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

nlc_test("i8_3 operators") {
    i8_3 a { .x = -6, .y = 3, .z = 3 };
    i8_3 b { .x = 3, .y = -4, .z = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5 && b.z == -5, "x=", b.x, ", y=", b.y, ", z=", b.z);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6 && b.z == -6, "x=", b.x, ", y=", b.y, ", z=", b.z);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5 && a.z == 5, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == -12, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 36, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == -12, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 36, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6 && a.z == 6, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 1, 13, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1 && a.z == -1, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a <<= i8_3 { 2, 3, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48 && a.z == -48, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a >>= i8_3 { 2, 3, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a ^= i8_3 { 1, 7, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3 && a.z == -3, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 7, 13, 13 };
    a %= i8_3 { 5, 4, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check((i8_3 { 1, 13, 13 } | i8_3 { 1, 2, 2 }) == i8_3 { 1, 15, 15 });
    nlc_check((i8_3 { 1, 13, 13 } & i8_3 { 1, 3, 3 }) == i8_3 { 1, 1, 1 });
    nlc_check((i8_3 { 24, 13, 13 } >> i8_3 { 2, 1, 1 }) == i8_3 { 6, 6, 6 });
    nlc_check((i8_3 { 1, 13, 13 } << i8_3 { 3, 2, 2 }) == i8_3 { 8, 52, 52 });
    nlc_check((i8_3 { 1, 13, 13 } ^ i8_3 { 3, 2, 2 }) == i8_3 { 2, 15, 15 });
    nlc_check((i8_3 { 7, 13, 13 } % i8_3 { 5, 4, 4 }) == i8_3 { 2, 1, 1 });

    i8 const cst { 3 };
    a = { 1, 13, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 13 && a.y == 13 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

nlc_test("u16_3 operators") {
    u16_3 a { .x = 6, .y = 3, .z = 3 };
    u16_3 b { .x = 3, .y = 4, .z = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3 && b.z == 3, "x=", b.x, ", y=", b.y, ", z=", b.z);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2 && b.z == 2, "x=", b.x, ", y=", b.y, ", z=", b.z);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5 && a.z == 5, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 1, 13, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15 && a.z == 15, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8 && a.z == 8, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0 && a.z == 0, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 7, 13, 13 };
    a %= u16_3 { 5, 4, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check((u16_3 { 1, 13, 13 } | u16_3 { 1, 2, 2 }) == u16_3 { 1, 15, 15 });
    nlc_check((u16_3 { 1, 13, 13 } & u16_3 { 1, 3, 3 }) == u16_3 { 1, 1, 1 });
    nlc_check((u16_3 { 24, 13, 13 } >> u16_3 { 2, 1, 1 }) == u16_3 { 6, 6, 6 });
    nlc_check((u16_3 { 1, 13, 13 } << u16_3 { 3, 2, 2 }) == u16_3 { 8, 52, 52 });
    nlc_check((u16_3 { 1, 13, 13 } ^ u16_3 { 3, 2, 2 }) == u16_3 { 2, 15, 15 });
    nlc_check((u16_3 { 7, 13, 13 } % u16_3 { 5, 4, 4 }) == u16_3 { 2, 1, 1 });

    u16 const cst { 3 };
    a = { 1, 13, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 13 && a.y == 13 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

nlc_test("i16_3 operators") {
    i16_3 a { .x = -6, .y = 3, .z = 3 };
    i16_3 b { .x = 3, .y = -4, .z = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5 && b.z == -5, "x=", b.x, ", y=", b.y, ", z=", b.z);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6 && b.z == -6, "x=", b.x, ", y=", b.y, ", z=", b.z);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5 && a.z == 5, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == -12, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 36, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == -12, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 36, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6 && a.z == 6, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 1, 13, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1 && a.z == -1, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a <<= i16_3 { 2, 3, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48 && a.z == -48, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a >>= i16_3 { 2, 3, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a ^= i16_3 { 1, 7, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3 && a.z == -3, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 7, 13, 13 };
    a %= i16_3 { 5, 4, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check((i16_3 { 1, 13, 13 } | i16_3 { 1, 2, 2 }) == i16_3 { 1, 15, 15 });
    nlc_check((i16_3 { 1, 13, 13 } & i16_3 { 1, 3, 3 }) == i16_3 { 1, 1, 1 });
    nlc_check((i16_3 { 24, 13, 13 } >> i16_3 { 2, 1, 1 }) == i16_3 { 6, 6, 6 });
    nlc_check((i16_3 { 1, 13, 13 } << i16_3 { 3, 2, 2 }) == i16_3 { 8, 52, 52 });
    nlc_check((i16_3 { 1, 13, 13 } ^ i16_3 { 3, 2, 2 }) == i16_3 { 2, 15, 15 });
    nlc_check((i16_3 { 7, 13, 13 } % i16_3 { 5, 4, 4 }) == i16_3 { 2, 1, 1 });

    i16 const cst { 3 };
    a = { 1, 13, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 13 && a.y == 13 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

nlc_test("u32_3 operators") {
    u32_3 a { .x = 6, .y = 3, .z = 3 };
    u32_3 b { .x = 3, .y = 4, .z = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3 && b.z == 3, "x=", b.x, ", y=", b.y, ", z=", b.z);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2 && b.z == 2, "x=", b.x, ", y=", b.y, ", z=", b.z);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5 && a.z == 5, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 1, 13, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15 && a.z == 15, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8 && a.z == 8, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0 && a.z == 0, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 7, 13, 13 };
    a %= u32_3 { 5, 4, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check((u32_3 { 1, 13, 13 } | u32_3 { 1, 2, 2 }) == u32_3 { 1, 15, 15 });
    nlc_check((u32_3 { 1, 13, 13 } & u32_3 { 1, 3, 3 }) == u32_3 { 1, 1, 1 });
    nlc_check((u32_3 { 24, 13, 13 } >> u32_3 { 2, 1, 1 }) == u32_3 { 6, 6, 6 });
    nlc_check((u32_3 { 1, 13, 13 } << u32_3 { 3, 2, 2 }) == u32_3 { 8, 52, 52 });
    nlc_check((u32_3 { 1, 13, 13 } ^ u32_3 { 3, 2, 2 }) == u32_3 { 2, 15, 15 });
    nlc_check((u32_3 { 7, 13, 13 } % u32_3 { 5, 4, 4 }) == u32_3 { 2, 1, 1 });

    u32 const cst { 3 };
    a = { 1, 13, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 13 && a.y == 13 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

nlc_test("i32_3 operators") {
    i32_3 a { .x = -6, .y = 3, .z = 3 };
    i32_3 b { .x = 3, .y = -4, .z = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5 && b.z == -5, "x=", b.x, ", y=", b.y, ", z=", b.z);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6 && b.z == -6, "x=", b.x, ", y=", b.y, ", z=", b.z);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5 && a.z == 5, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == -12, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 36, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == -12, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 36, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6 && a.z == 6, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 1, 13, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1 && a.z == -1, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a <<= i32_3 { 2, 3, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48 && a.z == -48, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a >>= i32_3 { 2, 3, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a ^= i32_3 { 1, 7, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3 && a.z == -3, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 7, 13, 13 };
    a %= i32_3 { 5, 4, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check((i32_3 { 1, 13, 13 } | i32_3 { 1, 2, 2 }) == i32_3 { 1, 15, 15 });
    nlc_check((i32_3 { 1, 13, 13 } & i32_3 { 1, 3, 3 }) == i32_3 { 1, 1, 1 });
    nlc_check((i32_3 { 24, 13, 13 } >> i32_3 { 2, 1, 1 }) == i32_3 { 6, 6, 6 });
    nlc_check((i32_3 { 1, 13, 13 } << i32_3 { 3, 2, 2 }) == i32_3 { 8, 52, 52 });
    nlc_check((i32_3 { 1, 13, 13 } ^ i32_3 { 3, 2, 2 }) == i32_3 { 2, 15, 15 });
    nlc_check((i32_3 { 7, 13, 13 } % i32_3 { 5, 4, 4 }) == i32_3 { 2, 1, 1 });

    i32 const cst { 3 };
    a = { 1, 13, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 13 && a.y == 13 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

nlc_test("u64_3 operators") {
    u64_3 a { .x = 6, .y = 3, .z = 3 };
    u64_3 b { .x = 3, .y = 4, .z = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3 && b.z == 3, "x=", b.x, ", y=", b.y, ", z=", b.z);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2 && b.z == 2, "x=", b.x, ", y=", b.y, ", z=", b.z);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5 && a.z == 5, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 1, 13, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15 && a.z == 15, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8 && a.z == 8, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0 && a.z == 0, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 7, 13, 13 };
    a %= u64_3 { 5, 4, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check((u64_3 { 1, 13, 13 } | u64_3 { 1, 2, 2 }) == u64_3 { 1, 15, 15 });
    nlc_check((u64_3 { 1, 13, 13 } & u64_3 { 1, 3, 3 }) == u64_3 { 1, 1, 1 });
    nlc_check((u64_3 { 24, 13, 13 } >> u64_3 { 2, 1, 1 }) == u64_3 { 6, 6, 6 });
    nlc_check((u64_3 { 1, 13, 13 } << u64_3 { 3, 2, 2 }) == u64_3 { 8, 52, 52 });
    nlc_check((u64_3 { 1, 13, 13 } ^ u64_3 { 3, 2, 2 }) == u64_3 { 2, 15, 15 });
    nlc_check((u64_3 { 7, 13, 13 } % u64_3 { 5, 4, 4 }) == u64_3 { 2, 1, 1 });

    u64 const cst { 3 };
    a = { 1, 13, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 13 && a.y == 13 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

nlc_test("i64_3 operators") {
    i64_3 a { .x = -6, .y = 3, .z = 3 };
    i64_3 b { .x = 3, .y = -4, .z = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5 && b.z == -5, "x=", b.x, ", y=", b.y, ", z=", b.z);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6 && b.z == -6, "x=", b.x, ", y=", b.y, ", z=", b.z);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5 && a.z == 5, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == -12, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 36, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == -12, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 36, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6 && a.z == 6, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 1, 13, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1 && a.z == -1, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a <<= i64_3 { 2, 3, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48 && a.z == -48, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a >>= i64_3 { 2, 3, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a ^= i64_3 { 1, 7, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3 && a.z == -3, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 7, 13, 13 };
    a %= i64_3 { 5, 4, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check((i64_3 { 1, 13, 13 } | i64_3 { 1, 2, 2 }) == i64_3 { 1, 15, 15 });
    nlc_check((i64_3 { 1, 13, 13 } & i64_3 { 1, 3, 3 }) == i64_3 { 1, 1, 1 });
    nlc_check((i64_3 { 24, 13, 13 } >> i64_3 { 2, 1, 1 }) == i64_3 { 6, 6, 6 });
    nlc_check((i64_3 { 1, 13, 13 } << i64_3 { 3, 2, 2 }) == i64_3 { 8, 52, 52 });
    nlc_check((i64_3 { 1, 13, 13 } ^ i64_3 { 3, 2, 2 }) == i64_3 { 2, 15, 15 });
    nlc_check((i64_3 { 7, 13, 13 } % i64_3 { 5, 4, 4 }) == i64_3 { 2, 1, 1 });

    i64 const cst { 3 };
    a = { 1, 13, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 13 && a.y == 13 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

nlc_test("usize_3 operators") {
    usize_3 a { .x = 6, .y = 3, .z = 3 };
    usize_3 b { .x = 3, .y = 4, .z = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3 && b.z == 3, "x=", b.x, ", y=", b.y, ", z=", b.z);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2 && b.z == 2, "x=", b.x, ", y=", b.y, ", z=", b.z);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5 && a.z == 5, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 1, 13, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15 && a.z == 15, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8 && a.z == 8, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 2, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0 && a.z == 0, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 7, 13, 13 };
    a %= usize_3 { 5, 4, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check((usize_3 { 1, 13, 13 } | usize_3 { 1, 2, 2 }) == usize_3 { 1, 15, 15 });
    nlc_check((usize_3 { 1, 13, 13 } & usize_3 { 1, 3, 3 }) == usize_3 { 1, 1, 1 });
    nlc_check((usize_3 { 24, 13, 13 } >> usize_3 { 2, 1, 1 }) == usize_3 { 6, 6, 6 });
    nlc_check((usize_3 { 1, 13, 13 } << usize_3 { 3, 2, 2 }) == usize_3 { 8, 52, 52 });
    nlc_check((usize_3 { 1, 13, 13 } ^ usize_3 { 3, 2, 2 }) == usize_3 { 2, 15, 15 });
    nlc_check((usize_3 { 7, 13, 13 } % usize_3 { 5, 4, 4 }) == usize_3 { 2, 1, 1 });

    usize const cst { 3 };
    a = { 1, 13, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 13 && a.y == 13 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

nlc_test("isize_3 operators") {
    isize_3 a { .x = -6, .y = 3, .z = 3 };
    isize_3 b { .x = 3, .y = -4, .z = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5 && b.z == -5, "x=", b.x, ", y=", b.y, ", z=", b.z);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6 && b.z == -6, "x=", b.x, ", y=", b.y, ", z=", b.z);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5 && a.z == 5, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == -12, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 36, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == -12, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 36, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6 && a.z == 6, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 1, 13, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1 && a.z == -1, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a <<= isize_3 { 2, 3, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48 && a.z == -48, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a >>= isize_3 { 2, 3, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a ^= isize_3 { 1, 7, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3 && a.z == -3, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = { 7, 13, 13 };
    a %= isize_3 { 5, 4, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check((isize_3 { 1, 13, 13 } | isize_3 { 1, 2, 2 }) == isize_3 { 1, 15, 15 });
    nlc_check((isize_3 { 1, 13, 13 } & isize_3 { 1, 3, 3 }) == isize_3 { 1, 1, 1 });
    nlc_check((isize_3 { 24, 13, 13 } >> isize_3 { 2, 1, 1 }) == isize_3 { 6, 6, 6 });
    nlc_check((isize_3 { 1, 13, 13 } << isize_3 { 3, 2, 2 }) == isize_3 { 8, 52, 52 });
    nlc_check((isize_3 { 1, 13, 13 } ^ isize_3 { 3, 2, 2 }) == isize_3 { 2, 15, 15 });
    nlc_check((isize_3 { 7, 13, 13 } % isize_3 { 5, 4, 4 }) == isize_3 { 2, 1, 1 });

    isize const cst { 3 };
    a = { 1, 13, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 39, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 13, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 13 && a.y == 13 && a.z == 1, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

nlc_test("f32_3 operators") {
    f32_3 a { .x = -6, .y = 3, .z = 3 };
    f32_3 b { .x = 3, .y = -4, .z = -4 };

    a = b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 6 && a.y == -8 && a.z == -8, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 9 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 6 && a.y == -8 && a.z == -8, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 9 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = -a;
    nlc_check_msg(a.x == -3 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);

    f32 const cst { 3 };
    a = { .x = -6, .y = 3, .z = 3 };

    a += cst;
    nlc_check_msg(a.x == -3 && a.y == 6 && a.z == 6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == 3, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == -18 && a.y == 9 && a.z == 9, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == 3, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == -3 && a.y == 6 && a.z == 6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == 3, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == -18 && a.y == 9 && a.z == 9, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == 3, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 3 && a.y == 3 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

nlc_test("f64_3 operators") {
    f64_3 a { .x = -6, .y = 3, .z = 3 };
    f64_3 b { .x = 3, .y = -4, .z = -4 };

    a = b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a += b;
    nlc_check_msg(a.x == 6 && a.y == -8 && a.z == -8, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= b;
    nlc_check_msg(a.x == 9 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a + b;
    nlc_check_msg(a.x == 6 && a.y == -8 && a.z == -8, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * b;
    nlc_check_msg(a.x == 9 && a.y == 16 && a.z == 16, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = +a;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == -4, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = -a;
    nlc_check_msg(a.x == -3 && a.y == 4 && a.z == 4, "x=", a.x, ", y=", a.y, ", z=", a.z);

    f64 const cst { 3 };
    a = { .x = -6, .y = 3, .z = 3 };

    a += cst;
    nlc_check_msg(a.x == -3 && a.y == 6 && a.z == 6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a -= cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == 3, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a *= cst;
    nlc_check_msg(a.x == -18 && a.y == 9 && a.z == 9, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a /= cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == 3, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = a + cst;
    nlc_check_msg(a.x == -3 && a.y == 6 && a.z == 6, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a - cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == 3, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a * cst;
    nlc_check_msg(a.x == -18 && a.y == 9 && a.z == 9, "x=", a.x, ", y=", a.y, ", z=", a.z);
    a = a / cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == 3, "x=", a.x, ", y=", a.y, ", z=", a.z);

    a = nlc_swizzle(a, y, z, x);
    nlc_check_msg(a.x == 3 && a.y == 3 && a.z == -6, "x=", a.x, ", y=", a.y, ", z=", a.z);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

nlc_test("u8_3 common functions") {
    nlc_check(min(u8_3 { 3, 5, 5 }, u8_3 { 6, 2, 3 }) == u8_3 { 3, 2, 3 });
    nlc_check(max(u8_3 { 3, 5, 5 }, u8_3 { 6, 2, 3 }) == u8_3 { 6, 5, 5 });
    nlc_check(clamp(u8_3 { 3, 5, 0 }, u8_3 { 6, 2, 1 }, u8_3 { 10, 4, 3 }) == u8_3 { 6, 4, 1 });
    nlc_check(sum(u8_3 { 5, 12, 3 }) == 20);
    nlc_check(dot(u8_3 { 1, 5, 2 }, u8_3 { 4, 2, 4 }) == 22);
    nlc_check(det(u8_3 { 6, 4, 3 }, u8_3 { 2, 3, 4 }, u8_3 { 1, 0, 8 }) == 87);

    nlc_check(sqrnorm(u8_3 { 3, 4, 4 }) == 41);
    nlc_check(norm(u8_3 { 3, 4, 4 }) == 6);
    nlc_check(normalize(u8_3 { 2, 0, 0 }) == u8_3 { 1, 0, 0 });
    nlc_check(normalize(u8_3 { 0, 0, 5 }) == u8_3 { 0, 0, 1 });
    nlc_check(sqrdistance(u8_3 { 5, 6, 5 }, u8_3 { 8, 10, 1 }) == 41);
    nlc_check(distance(u8_3 { 5, 6, 5 }, u8_3 { 8, 10, 1 }) == 6);

    nlc_check(project(u8_3 { 2, 2, 0 }, u8_3 { 1, 0, 0 }) == i8_3 { 0, 2, 0 });
    nlc_check(normalize_project(u8_3 { 2, 2, 0 }, u8_3 { 8, 0, 0 }) == i8_3 { 0, 2, 0 });
}

nlc_test("i8_3 common functions") {
    nlc_check(abs(i8_3 { -2, -4, 1 }) == i8_3 { 2, 4, 1 });
    nlc_check(abs(i8_3 { 2, 4, 0 }) == i8_3 { 2, 4, 0 });
    nlc_check(sign(i8_3 { -2, -4, 2 }) == i8_3 { -1, -1, 1 });
    nlc_check(sign(i8_3 { 0, 4, -5 }) == i8_3 { 0, 1, -1 });
    nlc_check(min(i8_3 { 3, -5, 5 }, i8_3 { 6, 2, -3 }) == i8_3 { 3, -5, -3 });
    nlc_check(max(i8_3 { 3, 5, -5 }, i8_3 { -6, 2, -5 }) == i8_3 { 3, 5, -5 });
    nlc_check(clamp(i8_3 { 3, 5, 0 }, i8_3 { -6, -4, -2 }, i8_3 { 10, -2, 4 }) == i8_3 { 3, -2, 0 });
    nlc_check(sum(i8_3 { 5, -12, 3 }) == -4);
    nlc_check(dot(i8_3 { 1, -5, 2 }, i8_3 { 4, 2, 7 }) == 8);
    nlc_check(det(i8_3 { 1, -4, -3 }, i8_3 { -2, 3, -4 }, i8_3 { 1, 0, -8 }) == 65);

    nlc_check(sqrnorm(i8_3 { -3, -4, -4 }) == 41);
    nlc_check(norm(i8_3 { -3, -4, -4 }) == 6);
    nlc_check(normalize(i8_3 { -2, 0, 0 }) == i8_3 { -1, 0, 0 });
    nlc_check(normalize(i8_3 { 0, 5, 0 }) == i8_3 { 0, 1, 0 });
    nlc_check(sqrdistance(i8_3 { -5, -6, 5 }, i8_3 { -8, -10, 1 }) == 41);
    nlc_check(distance(i8_3 { -5, -6, 5 }, i8_3 { -8, -10, 1 }) == 6);

    nlc_check(reflect(i8_3 { 2, -2, 0 }, i8_3 { 0, -1, 0 }) == i8_3 { 2, 2, 0 });
    nlc_check(reflect(i8_3 { 2, 0, 0 }, i8_3 { -1, 0, 0 }) == i8_3 { -2, 0, 0 });
    nlc_check(normalize_reflect(i8_3 { 2, -2, 0 }, i8_3 { 0, -5, 0 }) == i8_3 { 2, 2, 0 });
    nlc_check(normalize_reflect(i8_3 { 2, 0, 0 }, i8_3 { -8, 0, 0 }) == i8_3 { -2, 0, 0 });
    nlc_check(project(i8_3 { 2, 2, 0 }, i8_3 { -1, 0, 0 }) == i8_3 { 0, 2, 0 });
    nlc_check(normalize_project(i8_3 { 2, 2, 0 }, i8_3 { -6, 0, 0 }) == i8_3 { 0, 2, 0 });
}

nlc_test("u16_3 common functions") {
    nlc_check(min(u16_3 { 3, 5, 5 }, u16_3 { 6, 2, 3 }) == u16_3 { 3, 2, 3 });
    nlc_check(max(u16_3 { 3, 5, 5 }, u16_3 { 6, 2, 3 }) == u16_3 { 6, 5, 5 });
    nlc_check(clamp(u16_3 { 3, 5, 0 }, u16_3 { 6, 2, 1 }, u16_3 { 10, 4, 3 }) == u16_3 { 6, 4, 1 });
    nlc_check(sum(u16_3 { 5, 12, 3 }) == 20);
    nlc_check(dot(u16_3 { 1, 5, 2 }, u16_3 { 4, 2, 4 }) == 22);
    nlc_check(det(u16_3 { 6, 4, 3 }, u16_3 { 2, 3, 4 }, u16_3 { 1, 0, 8 }) == 87);

    nlc_check(sqrnorm(u16_3 { 3, 4, 4 }) == 41);
    nlc_check(norm(u16_3 { 3, 4, 4 }) == 6);
    nlc_check(normalize(u16_3 { 2, 0, 0 }) == u16_3 { 1, 0, 0 });
    nlc_check(normalize(u16_3 { 0, 0, 5 }) == u16_3 { 0, 0, 1 });
    nlc_check(sqrdistance(u16_3 { 5, 6, 5 }, u16_3 { 8, 10, 1 }) == 41);
    nlc_check(distance(u16_3 { 5, 6, 5 }, u16_3 { 8, 10, 1 }) == 6);

    nlc_check(project(u16_3 { 2, 2, 0 }, u16_3 { 1, 0, 0 }) == i16_3 { 0, 2, 0 });
    nlc_check(normalize_project(u16_3 { 2, 2, 0 }, u16_3 { 8, 0, 0 }) == i16_3 { 0, 2, 0 });
}

nlc_test("i16_3 common functions") {
    nlc_check(abs(i16_3 { -2, -4, 1 }) == i16_3 { 2, 4, 1 });
    nlc_check(abs(i16_3 { 2, 4, 0 }) == i16_3 { 2, 4, 0 });
    nlc_check(sign(i16_3 { -2, -4, 2 }) == i16_3 { -1, -1, 1 });
    nlc_check(sign(i16_3 { 0, 4, -5 }) == i16_3 { 0, 1, -1 });
    nlc_check(min(i16_3 { 3, -5, 5 }, i16_3 { 6, 2, -3 }) == i16_3 { 3, -5, -3 });
    nlc_check(max(i16_3 { 3, 5, -5 }, i16_3 { -6, 2, -5 }) == i16_3 { 3, 5, -5 });
    nlc_check(clamp(i16_3 { 3, 5, 0 }, i16_3 { -6, -4, -2 }, i16_3 { 10, -2, 4 }) ==
              i16_3 { 3, -2, 0 });
    nlc_check(sum(i16_3 { 5, -12, 3 }) == -4);
    nlc_check(dot(i16_3 { 1, -5, 2 }, i16_3 { 4, 2, 7 }) == 8);
    nlc_check(det(i16_3 { 1, -4, -3 }, i16_3 { -2, 3, -4 }, i16_3 { 1, 0, -8 }) == 65);

    nlc_check(sqrnorm(i16_3 { -3, -4, -4 }) == 41);
    nlc_check(norm(i16_3 { -3, -4, -4 }) == 6);
    nlc_check(normalize(i16_3 { -2, 0, 0 }) == i16_3 { -1, 0, 0 });
    nlc_check(normalize(i16_3 { 0, 5, 0 }) == i16_3 { 0, 1, 0 });
    nlc_check(sqrdistance(i16_3 { -5, -6, 5 }, i16_3 { -8, -10, 1 }) == 41);
    nlc_check(distance(i16_3 { -5, -6, 5 }, i16_3 { -8, -10, 1 }) == 6);

    nlc_check(reflect(i16_3 { 2, -2, 0 }, i16_3 { 0, -1, 0 }) == i16_3 { 2, 2, 0 });
    nlc_check(reflect(i16_3 { 2, 0, 0 }, i16_3 { -1, 0, 0 }) == i16_3 { -2, 0, 0 });
    nlc_check(normalize_reflect(i16_3 { 2, -2, 0 }, i16_3 { 0, -5, 0 }) == i16_3 { 2, 2, 0 });
    nlc_check(normalize_reflect(i16_3 { 2, 0, 0 }, i16_3 { -8, 0, 0 }) == i16_3 { -2, 0, 0 });
    nlc_check(project(i16_3 { 2, 2, 0 }, i16_3 { -1, 0, 0 }) == i16_3 { 0, 2, 0 });
    nlc_check(normalize_project(i16_3 { 2, 2, 0 }, i16_3 { -6, 0, 0 }) == i16_3 { 0, 2, 0 });
}

nlc_test("u32_3 common functions") {
    nlc_check(min(u32_3 { 3, 5, 5 }, u32_3 { 6, 2, 3 }) == u32_3 { 3, 2, 3 });
    nlc_check(max(u32_3 { 3, 5, 5 }, u32_3 { 6, 2, 3 }) == u32_3 { 6, 5, 5 });
    nlc_check(clamp(u32_3 { 3, 5, 0 }, u32_3 { 6, 2, 1 }, u32_3 { 10, 4, 3 }) == u32_3 { 6, 4, 1 });
    nlc_check(sum(u32_3 { 5, 12, 3 }) == 20);
    nlc_check(dot(u32_3 { 1, 5, 2 }, u32_3 { 4, 2, 4 }) == 22);
    nlc_check(det(u32_3 { 6, 4, 3 }, u32_3 { 2, 3, 4 }, u32_3 { 1, 0, 8 }) == 87);

    nlc_check(sqrnorm(u32_3 { 3, 4, 4 }) == 41);
    nlc_check(norm(u32_3 { 3, 4, 4 }) == 6);
    nlc_check(normalize(u32_3 { 2, 0, 0 }) == u32_3 { 1, 0, 0 });
    nlc_check(normalize(u32_3 { 0, 0, 5 }) == u32_3 { 0, 0, 1 });
    nlc_check(sqrdistance(u32_3 { 5, 6, 5 }, u32_3 { 8, 10, 1 }) == 41);
    nlc_check(distance(u32_3 { 5, 6, 5 }, u32_3 { 8, 10, 1 }) == 6);

    nlc_check(project(u32_3 { 2, 2, 0 }, u32_3 { 1, 0, 0 }) == i32_3 { 0, 2, 0 });
    nlc_check(normalize_project(u32_3 { 2, 2, 0 }, u32_3 { 8, 0, 0 }) == i32_3 { 0, 2, 0 });
}

nlc_test("i32_3 common functions") {
    nlc_check(abs(i32_3 { -2, -4, 1 }) == i32_3 { 2, 4, 1 });
    nlc_check(abs(i32_3 { 2, 4, 0 }) == i32_3 { 2, 4, 0 });
    nlc_check(sign(i32_3 { -2, -4, 2 }) == i32_3 { -1, -1, 1 });
    nlc_check(sign(i32_3 { 0, 4, -5 }) == i32_3 { 0, 1, -1 });
    nlc_check(min(i32_3 { 3, -5, 5 }, i32_3 { 6, 2, -3 }) == i32_3 { 3, -5, -3 });
    nlc_check(max(i32_3 { 3, 5, -5 }, i32_3 { -6, 2, -5 }) == i32_3 { 3, 5, -5 });
    nlc_check(clamp(i32_3 { 3, 5, 0 }, i32_3 { -6, -4, -2 }, i32_3 { 10, -2, 4 }) ==
              i32_3 { 3, -2, 0 });
    nlc_check(sum(i32_3 { 5, -12, 3 }) == -4);
    nlc_check(dot(i32_3 { 1, -5, 2 }, i32_3 { 4, 2, 7 }) == 8);
    nlc_check(det(i32_3 { 1, -4, -3 }, i32_3 { -2, 3, -4 }, i32_3 { 1, 0, -8 }) == 65);

    nlc_check(sqrnorm(i32_3 { -3, -4, -4 }) == 41);
    nlc_check(norm(i32_3 { -3, -4, -4 }) == 6);
    nlc_check(normalize(i32_3 { -2, 0, 0 }) == i32_3 { -1, 0, 0 });
    nlc_check(normalize(i32_3 { 0, 5, 0 }) == i32_3 { 0, 1, 0 });
    nlc_check(sqrdistance(i32_3 { -5, -6, 5 }, i32_3 { -8, -10, 1 }) == 41);
    nlc_check(distance(i32_3 { -5, -6, 5 }, i32_3 { -8, -10, 1 }) == 6);

    nlc_check(reflect(i32_3 { 2, -2, 0 }, i32_3 { 0, -1, 0 }) == i32_3 { 2, 2, 0 });
    nlc_check(reflect(i32_3 { 2, 0, 0 }, i32_3 { -1, 0, 0 }) == i32_3 { -2, 0, 0 });
    nlc_check(normalize_reflect(i32_3 { 2, -2, 0 }, i32_3 { 0, -5, 0 }) == i32_3 { 2, 2, 0 });
    nlc_check(normalize_reflect(i32_3 { 2, 0, 0 }, i32_3 { -8, 0, 0 }) == i32_3 { -2, 0, 0 });
    nlc_check(project(i32_3 { 2, 2, 0 }, i32_3 { -1, 0, 0 }) == i32_3 { 0, 2, 0 });
    nlc_check(normalize_project(i32_3 { 2, 2, 0 }, i32_3 { -6, 0, 0 }) == i32_3 { 0, 2, 0 });
}

nlc_test("u64_3 common functions") {
    nlc_check(min(u64_3 { 3, 5, 5 }, u64_3 { 6, 2, 3 }) == u64_3 { 3, 2, 3 });
    nlc_check(max(u64_3 { 3, 5, 5 }, u64_3 { 6, 2, 3 }) == u64_3 { 6, 5, 5 });
    nlc_check(clamp(u64_3 { 3, 5, 0 }, u64_3 { 6, 2, 1 }, u64_3 { 10, 4, 3 }) == u64_3 { 6, 4, 1 });
    nlc_check(sum(u64_3 { 5, 12, 3 }) == 20);
    nlc_check(dot(u64_3 { 1, 5, 2 }, u64_3 { 4, 2, 4 }) == 22);
    nlc_check(det(u64_3 { 6, 4, 3 }, u64_3 { 2, 3, 4 }, u64_3 { 1, 0, 8 }) == 87);

    nlc_check(sqrnorm(u64_3 { 3, 4, 4 }) == 41);
    nlc_check(norm(u64_3 { 3, 4, 4 }) == 6);
    nlc_check(normalize(u64_3 { 2, 0, 0 }) == u64_3 { 1, 0, 0 });
    nlc_check(normalize(u64_3 { 0, 0, 5 }) == u64_3 { 0, 0, 1 });
    nlc_check(sqrdistance(u64_3 { 5, 6, 5 }, u64_3 { 8, 10, 1 }) == 41);
    nlc_check(distance(u64_3 { 5, 6, 5 }, u64_3 { 8, 10, 1 }) == 6);

    nlc_check(project(u64_3 { 2, 2, 0 }, u64_3 { 1, 0, 0 }) == i64_3 { 0, 2, 0 });
    nlc_check(normalize_project(u64_3 { 2, 2, 0 }, u64_3 { 8, 0, 0 }) == i64_3 { 0, 2, 0 });
}

nlc_test("i64_3 common functions") {
    nlc_check(abs(i64_3 { -2, -4, 1 }) == i64_3 { 2, 4, 1 });
    nlc_check(abs(i64_3 { 2, 4, 0 }) == i64_3 { 2, 4, 0 });
    nlc_check(sign(i64_3 { -2, -4, 2 }) == i64_3 { -1, -1, 1 });
    nlc_check(sign(i64_3 { 0, 4, -5 }) == i64_3 { 0, 1, -1 });
    nlc_check(min(i64_3 { 3, -5, 5 }, i64_3 { 6, 2, -3 }) == i64_3 { 3, -5, -3 });
    nlc_check(max(i64_3 { 3, 5, -5 }, i64_3 { -6, 2, -5 }) == i64_3 { 3, 5, -5 });
    nlc_check(clamp(i64_3 { 3, 5, 0 }, i64_3 { -6, -4, -2 }, i64_3 { 10, -2, 4 }) ==
              i64_3 { 3, -2, 0 });
    nlc_check(sum(i64_3 { 5, -12, 3 }) == -4);
    nlc_check(dot(i64_3 { 1, -5, 2 }, i64_3 { 4, 2, 7 }) == 8);
    nlc_check(det(i64_3 { 1, -4, -3 }, i64_3 { -2, 3, -4 }, i64_3 { 1, 0, -8 }) == 65);

    nlc_check(sqrnorm(i64_3 { -3, -4, -4 }) == 41);
    nlc_check(norm(i64_3 { -3, -4, -4 }) == 6);
    nlc_check(normalize(i64_3 { -2, 0, 0 }) == i64_3 { -1, 0, 0 });
    nlc_check(normalize(i64_3 { 0, 5, 0 }) == i64_3 { 0, 1, 0 });
    nlc_check(sqrdistance(i64_3 { -5, -6, 5 }, i64_3 { -8, -10, 1 }) == 41);
    nlc_check(distance(i64_3 { -5, -6, 5 }, i64_3 { -8, -10, 1 }) == 6);

    nlc_check(reflect(i64_3 { 2, -2, 0 }, i64_3 { 0, -1, 0 }) == i64_3 { 2, 2, 0 });
    nlc_check(reflect(i64_3 { 2, 0, 0 }, i64_3 { -1, 0, 0 }) == i64_3 { -2, 0, 0 });
    nlc_check(normalize_reflect(i64_3 { 2, -2, 0 }, i64_3 { 0, -5, 0 }) == i64_3 { 2, 2, 0 });
    nlc_check(normalize_reflect(i64_3 { 2, 0, 0 }, i64_3 { -8, 0, 0 }) == i64_3 { -2, 0, 0 });
    nlc_check(project(i64_3 { 2, 2, 0 }, i64_3 { -1, 0, 0 }) == i64_3 { 0, 2, 0 });
    nlc_check(normalize_project(i64_3 { 2, 2, 0 }, i64_3 { -6, 0, 0 }) == i64_3 { 0, 2, 0 });
}

nlc_test("usize_3 common functions") {
    nlc_check(min(usize_3 { 3, 5, 5 }, usize_3 { 6, 2, 3 }) == usize_3 { 3, 2, 3 });
    nlc_check(max(usize_3 { 3, 5, 5 }, usize_3 { 6, 2, 3 }) == usize_3 { 6, 5, 5 });
    nlc_check(clamp(usize_3 { 3, 5, 0 }, usize_3 { 6, 2, 1 }, usize_3 { 10, 4, 3 }) ==
              usize_3 { 6, 4, 1 });
    nlc_check(sum(usize_3 { 5, 12, 3 }) == 20);
    nlc_check(dot(usize_3 { 1, 5, 2 }, usize_3 { 4, 2, 4 }) == 22);
    nlc_check(det(usize_3 { 6, 4, 3 }, usize_3 { 2, 3, 4 }, usize_3 { 1, 0, 8 }) == 87);

    nlc_check(sqrnorm(usize_3 { 3, 4, 4 }) == 41);
    nlc_check(norm(usize_3 { 3, 4, 4 }) == 6);
    nlc_check(normalize(usize_3 { 2, 0, 0 }) == usize_3 { 1, 0, 0 });
    nlc_check(normalize(usize_3 { 0, 0, 5 }) == usize_3 { 0, 0, 1 });
    nlc_check(sqrdistance(usize_3 { 5, 6, 5 }, usize_3 { 8, 10, 1 }) == 41);
    nlc_check(distance(usize_3 { 5, 6, 5 }, usize_3 { 8, 10, 1 }) == 6);

    nlc_check(project(usize_3 { 2, 2, 0 }, usize_3 { 1, 0, 0 }) == isize_3 { 0, 2, 0 });
    nlc_check(normalize_project(usize_3 { 2, 2, 0 }, usize_3 { 8, 0, 0 }) == isize_3 { 0, 2, 0 });
}

nlc_test("isize_3 common functions") {
    nlc_check(abs(isize_3 { -2, -4, 1 }) == isize_3 { 2, 4, 1 });
    nlc_check(abs(isize_3 { 2, 4, 0 }) == isize_3 { 2, 4, 0 });
    nlc_check(sign(isize_3 { -2, -4, 2 }) == isize_3 { -1, -1, 1 });
    nlc_check(sign(isize_3 { 0, 4, -5 }) == isize_3 { 0, 1, -1 });
    nlc_check(min(isize_3 { 3, -5, 5 }, isize_3 { 6, 2, -3 }) == isize_3 { 3, -5, -3 });
    nlc_check(max(isize_3 { 3, 5, -5 }, isize_3 { -6, 2, -5 }) == isize_3 { 3, 5, -5 });
    nlc_check(clamp(isize_3 { 3, 5, 0 }, isize_3 { -6, -4, -2 }, isize_3 { 10, -2, 4 }) ==
              isize_3 { 3, -2, 0 });
    nlc_check(sum(isize_3 { 5, -12, 3 }) == -4);
    nlc_check(dot(isize_3 { 1, -5, 2 }, isize_3 { 4, 2, 7 }) == 8);
    nlc_check(det(isize_3 { 1, -4, -3 }, isize_3 { -2, 3, -4 }, isize_3 { 1, 0, -8 }) == 65);

    nlc_check(sqrnorm(isize_3 { -3, -4, -4 }) == 41);
    nlc_check(norm(isize_3 { -3, -4, -4 }) == 6);
    nlc_check(normalize(isize_3 { -2, 0, 0 }) == isize_3 { -1, 0, 0 });
    nlc_check(normalize(isize_3 { 0, 5, 0 }) == isize_3 { 0, 1, 0 });
    nlc_check(sqrdistance(isize_3 { -5, -6, 5 }, isize_3 { -8, -10, 1 }) == 41);
    nlc_check(distance(isize_3 { -5, -6, 5 }, isize_3 { -8, -10, 1 }) == 6);

    nlc_check(reflect(isize_3 { 2, -2, 0 }, isize_3 { 0, -1, 0 }) == isize_3 { 2, 2, 0 });
    nlc_check(reflect(isize_3 { 2, 0, 0 }, isize_3 { -1, 0, 0 }) == isize_3 { -2, 0, 0 });
    nlc_check(normalize_reflect(isize_3 { 2, -2, 0 }, isize_3 { 0, -5, 0 }) == isize_3 { 2, 2, 0 });
    nlc_check(normalize_reflect(isize_3 { 2, 0, 0 }, isize_3 { -8, 0, 0 }) == isize_3 { -2, 0, 0 });
    nlc_check(project(isize_3 { 2, 2, 0 }, isize_3 { -1, 0, 0 }) == isize_3 { 0, 2, 0 });
    nlc_check(normalize_project(isize_3 { 2, 2, 0 }, isize_3 { -6, 0, 0 }) == isize_3 { 0, 2, 0 });
}

nlc_test("f32_3 common functions") {
    nlc_check(abs(f32_3 { -2, -4, 1 }) == f32_3 { 2, 4, 1 });
    nlc_check(abs(f32_3 { 2, 4, 0 }) == f32_3 { 2, 4, 0 });
    nlc_check(sign(f32_3 { -2, -4, 2 }) == f32_3 { -1, -1, 1 });
    nlc_check(sign(f32_3 { 0, 4, -5 }) == f32_3 { 1, 1, -1 });
    nlc_check(min(f32_3 { 3, -5, 5 }, f32_3 { 6, 2, -3 }) == f32_3 { 3, -5, -3 });
    nlc_check(max(f32_3 { 3, 5, -5 }, f32_3 { -6, 2, -5 }) == f32_3 { 3, 5, -5 });
    nlc_check(clamp(f32_3 { 3, 5, 0 }, f32_3 { -6, -4, -2 }, f32_3 { 10, -2, 4 }) ==
              f32_3 { 3, -2, 0 });

    nlc_check_float_equality(sum(f32_3 { 5, -12, 3 }), -4);
    nlc_check_float_equality(dot(f32_3 { 1, -5, 2 }, f32_3 { 4, 2, 7 }), 8);
    nlc_check_float_equality(det(f32_3 { 1, -4, -3 }, f32_3 { -2, 3, -4 }, f32_3 { 1, 0, -8 }), 65);

    nlc_check_float_equality(sqrnorm(f32_3 { -3, -4, -4 }), 41);
    nlc_check_float_equality(norm(f32_3 { -3, -4, -4 }), sqrt(41.f));
    nlc_check_float_equality(normalize(f32_3 { -2, 0, 0 }), (f32_3 { -1, 0, 0 }));
    nlc_check_float_equality(normalize(f32_3 { 0, 5, 0 }), (f32_3 { 0, 1, 0 }));
    nlc_check_float_equality(sqrdistance(f32_3 { -5, -6, 5 }, f32_3 { -8, -10, 1 }), 41);
    nlc_check_float_equality(distance(f32_3 { -5, -6, 5 }, f32_3 { -8, -10, 1 }), sqrt(41));

    nlc_check_float_equality(reflect(f32_3 { 2, -2, 0 }, f32_3 { 0, -1, 0 }), (f32_3 { 2, 2, 0 }));
    nlc_check_float_equality(reflect(f32_3 { 2, 0, 0 }, f32_3 { -1, 0, 0 }), (f32_3 { -2, 0, 0 }));
    nlc_check_float_equality(normalize_reflect(f32_3 { 2, -2, 0 }, f32_3 { 0, -5, 0 }),
                             (f32_3 { 2, 2, 0 }));
    nlc_check_float_equality(normalize_reflect(f32_3 { 2, 0, 0 }, f32_3 { -8, 0, 0 }),
                             (f32_3 { -2, 0, 0 }));
    nlc_check_float_equality(project(f32_3 { 2, 2, 0 }, f32_3 { -1, 0, 0 }), (f32_3 { 0, 2, 0 }));
    nlc_check_float_equality(normalize_project(f32_3 { 2, 2, 0 }, f32_3 { -6, 0, 0 }),
                             (f32_3 { 0, 2, 0 }));
}

nlc_test("f64_3 common functions") {
    nlc_check(abs(f64_3 { -2, -4, 1 }) == f64_3 { 2, 4, 1 });
    nlc_check(abs(f64_3 { 2, 4, 0 }) == f64_3 { 2, 4, 0 });
    nlc_check(sign(f64_3 { -2, -4, 2 }) == f64_3 { -1, -1, 1 });
    nlc_check(sign(f64_3 { 0, 4, -5 }) == f64_3 { 1, 1, -1 });
    nlc_check(min(f64_3 { 3, -5, 5 }, f64_3 { 6, 2, -3 }) == f64_3 { 3, -5, -3 });
    nlc_check(max(f64_3 { 3, 5, -5 }, f64_3 { -6, 2, -5 }) == f64_3 { 3, 5, -5 });
    nlc_check(clamp(f64_3 { 3, 5, 0 }, f64_3 { -6, -4, -2 }, f64_3 { 10, -2, 4 }) ==
              f64_3 { 3, -2, 0 });

    nlc_check_double_equality(sum(f64_3 { 5, -12, 3 }), -4);
    nlc_check_double_equality(dot(f64_3 { 1, -5, 2 }, f64_3 { 4, 2, 7 }), 8);
    nlc_check_double_equality(det(f64_3 { 1, -4, -3 }, f64_3 { -2, 3, -4 }, f64_3 { 1, 0, -8 }), 65);

    nlc_check_double_equality(sqrnorm(f64_3 { -3, -4, -4 }), 41);
    nlc_check_double_equality(norm(f64_3 { -3, -4, -4 }), static_cast<f64>(sqrt(41.f)));
    nlc_check_double_equality(normalize(f64_3 { -2, 0, 0 }), (f64_3 { -1, 0, 0 }));
    nlc_check_double_equality(normalize(f64_3 { 0, 5, 0 }), (f64_3 { 0, 1, 0 }));
    nlc_check_double_equality(sqrdistance(f64_3 { -5, -6, 5 }, f64_3 { -8, -10, 1 }), 41);
    nlc_check_double_equality(distance(f64_3 { -5, -6, 5 }, f64_3 { -8, -10, 1 }),
                              static_cast<f64>(sqrt(41)));

    nlc_check_double_equality(reflect(f64_3 { 2, -2, 0 }, f64_3 { 0, -1, 0 }), (f64_3 { 2, 2, 0 }));
    nlc_check_double_equality(reflect(f64_3 { 2, 0, 0 }, f64_3 { -1, 0, 0 }), (f64_3 { -2, 0, 0 }));
    nlc_check_double_equality(normalize_reflect(f64_3 { 2, -2, 0 }, f64_3 { 0, -5, 0 }),
                              (f64_3 { 2, 2, 0 }));
    nlc_check_double_equality(normalize_reflect(f64_3 { 2, 0, 0 }, f64_3 { -8, 0, 0 }),
                              (f64_3 { -2, 0, 0 }));
    nlc_check_double_equality(project(f64_3 { 2, 2, 0 }, f64_3 { -1, 0, 0 }), (f64_3 { 0, 2, 0 }));
    nlc_check_double_equality(normalize_project(f64_3 { 2, 2, 0 }, f64_3 { -6, 0, 0 }),
                              (f64_3 { 0, 2, 0 }));
}

nlc_test("u8_3 lerp") {
    nlc_check(scalar_lerp(u8_3 { 0, 0, 0 }, u8_3 { 255, 255, 255 }, 0.5f) == u8_3 { 127, 127, 127 });
    nlc_check(scalar_lerp(u8_3 { 0, 0, 0 }, u8_3 { 255, 255, 255 }, 0.5) == u8_3 { 127, 127, 127 });
}
