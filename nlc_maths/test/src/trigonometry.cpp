/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/range.hpp>
#include <nlc/maths/simple_maths.hpp>
#include <nlc/maths/trigonometry.hpp>
#include <nlc/test.hpp>

#include <math.h>

using nlc::half_pi;
using nlc::pi;
using nlc::quarter_pi;
using nlc::two_pi;

static constexpr auto max_sin_error = 2e-5f;
static constexpr auto max_cos_error = 5e-5f;

template<typename FA, typename FB>
auto compare([[maybe_unused]] FA && fa, FB && fb, f32 const start, f32 const end, f32 const max_err) -> void {
    constexpr auto steps = static_cast<u32>(10e4);
    auto const r = end - start;

    for (auto const i : nlc::range(0u, steps)) {
        auto const k = (r / static_cast<f32>(steps));
        auto const x = start + k * static_cast<f32>(i);
        nlc_check_msg(x >= start && x <= end, "x = ", x);

        auto const a = fa(x);
        auto const b = fb(x);

        auto const err = nlc::abs(a - b);
        nlc_check_msg(err <= max_err,
                      "error too high for #",
                      i,
                      " x=",
                      x,
                      " : ",
                      err,
                      " > ",
                      max_err,
                      " -- ",
                      a,
                      " ",
                      b);
    }
}

nlc_test("reduced range sin") {
    constexpr auto max_error = 6.15e-6f;

    nlc_check(nlc::reduced_range::sin(0.f) == 0.f);                               // sin(0) = 0
    nlc_check(nlc::abs(nlc::reduced_range::sin(pi<f32>)) <= max_error);           // sin(π) = 0
    nlc_check(nlc::abs(nlc::reduced_range::sin(half_pi<f32>) - 1) <= max_error);  // sin(π/2) = 1

    compare(nlc::reduced_range::sin, sinf, -pi<f32>, pi<f32>, max_error);
}

nlc_test("reduced range cos") {
    constexpr auto max_error = 5e-5f;

    nlc_check(nlc::reduced_range::cos(0.f) == 1.f);                           // cos(0) = 1
    nlc_check(nlc::abs(nlc::reduced_range::cos(pi<f32>) + 1) <= max_error);   // cos(π) = -1
    nlc_check(nlc::abs(nlc::reduced_range::cos(half_pi<f32>)) <= max_error);  // cos(π/2) = 0

    compare(nlc::reduced_range::cos, cosf, -pi<f32>, pi<f32>, max_error);
}

nlc_test("reduced range tan") {
    nlc_check(nlc::tan(pi<f32>) == 0.f);

    // For some reason the absolute error to std::tanf is higher than the one computed by sollya
    compare(nlc::reduced_range::tan, tanf, -quarter_pi<f32>, quarter_pi<f32>, 1.2e-7f);
}

nlc_test("reduced range atan") {
    nlc_check(nlc::reduced_range::tan(0.f) == 0.f);

    compare(nlc::reduced_range::atan, atanf, -1.f, 1.f, 3.5e-6f);
}

nlc_test("sin") {
    nlc_check(nlc::abs(nlc::sin(two_pi<f32>)) <= max_sin_error);                 // sin(2π) = 0
    nlc_check(nlc::abs(nlc::sin(pi<f32> + half_pi<f32>) + 1) <= max_sin_error);  // sin(3π/2) = -1

    compare(nlc::sin, sinf, -100.f, 100.f, max_sin_error);
}

nlc_test("cos") {
    nlc_check(nlc::abs(nlc::cos(two_pi<f32>) - 1) <= max_cos_error);         // cos(2π) = 1
    nlc_check(nlc::abs(nlc::cos(pi<f32> + half_pi<f32>)) <= max_cos_error);  // cos(3π/2) = 0

    compare(nlc::cos, cosf, -100.f, 100.f, max_cos_error);
}

nlc_test("cos_sin") {
    compare([](f32 const v) { return nlc::cos_sin(v).x; }, cosf, -100.f, 100.f, max_cos_error);
    compare([](f32 const v) { return nlc::cos_sin(v).y; }, sinf, -100.f, 100.f, max_sin_error);
}

nlc_test("atan") { compare(nlc::atan, atanf, -100.f, 100.f, 3.5e-6f); }

nlc_test("atan2") {
    constexpr auto steps = static_cast<u32>(10e2);
    constexpr f32 max_err = 5.e-6f;
    constexpr f32 start = -100.f;
    constexpr f32 end = 100.f;
    constexpr auto r = end - start;

    for (auto const i : nlc::range(0u, steps)) {
        auto const k = (r / static_cast<f32>(steps));
        auto const x = start + k * static_cast<f32>(i);
        nlc_check_msg(x >= start && x <= end, "x = ", x);

        for (auto const ii : nlc::range(0u, steps)) {
            auto const k2 = (r / static_cast<f32>(steps));
            auto const y = start + k2 * static_cast<f32>(ii);
            nlc_check_msg(y >= start && y <= end, "y = ", y);

            auto const a = atan2f(y, x);
            auto const b = nlc::atan2(y, x);

            auto const err = nlc::abs(a - b);
            // clang-format off
            nlc_check_msg(err <= max_err, "error too high for #", i, '#', ii, " x=", x, " : ", err, " > ", max_err, " -- ", a, " ", b);
            // clang-format on
        }
    }
}
