/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/maths/float_cmp.hpp>
#include <nlc/maths/vec4.hpp>
#include <nlc/maths/vec4_common.hpp>
#include <nlc/test.hpp>

#include "check_float.hpp"

using namespace nlc;

nlc_test("u8_4 operators") {
    u8_4 a { .x = 6, .y = 3, .z = 6, .w = 3 };
    u8_4 b { .x = 3, .y = 4, .z = 3, .w = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3 && b.z == 2 && b.w == 3, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2 && b.z == 1 && b.w == 2, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4 && a.z == 7 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5 && a.z == 8 && a.w == 5, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 2 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 1 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 2 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 1 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 1, 13, 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15 && a.z == 1 && a.w == 15, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8 && a.z == 2 && a.w == 8, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0 && a.z == 0 && a.w == 0, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 7, 13, 7, 13 };
    a %= u8_4 { 5, 4, 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 2 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check((u8_4 { 1, 13, 1, 13 } | u8_4 { 1, 2, 1, 2 }) == u8_4 { 1, 15, 1, 15 });
    nlc_check((u8_4 { 1, 13, 1, 13 } & u8_4 { 1, 3, 1, 3 }) == u8_4 { 1, 1, 1, 1 });
    nlc_check((u8_4 { 24, 13, 24, 13 } >> u8_4 { 2, 1, 2, 1 }) == u8_4 { 6, 6, 6, 6 });
    nlc_check((u8_4 { 1, 13, 1, 13 } << u8_4 { 3, 2, 3, 2 }) == u8_4 { 8, 52, 8, 52 });
    nlc_check((u8_4 { 1, 13, 1, 13 } ^ u8_4 { 3, 2, 3, 2 }) == u8_4 { 2, 15, 2, 15 });
    nlc_check((u8_4 { 7, 13, 7, 13 } % u8_4 { 5, 4, 5, 4 }) == u8_4 { 2, 1, 2, 1 });

    u8 const cst { 3 };
    a = { 1, 13, 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1 && a.z == 13 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
}

nlc_test("i8_4 operators") {
    i8_4 a { .x = -6, .y = 3, .z = -6, .w = 3 };
    i8_4 b { .x = 3, .y = -4, .z = 3, .w = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5 && b.z == 2 && b.w == -5, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6 && b.z == 1 && b.w == -6, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4 && a.z == -5 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5 && a.z == -4 && a.w == 5, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == 2 && a.w == -12,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 1 && a.w == 36, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == 2 && a.w == -12,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 1 && a.w == 36, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6 && a.z == -1 && a.w == 6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 1, 13, 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1 && a.z == 1 && a.w == -1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a <<= i8_4 { 2, 3, 2, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48 && a.z == 4 && a.w == -48,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a >>= i8_4 { 2, 3, 2, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a ^= i8_4 { 1, 7, 1, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3 && a.z == 0 && a.w == -3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 7, 13, 7, 13 };
    a %= i8_4 { 5, 4, 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 2 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check((i8_4 { 1, 13, 1, 13 } | i8_4 { 1, 2, 1, 2 }) == i8_4 { 1, 15, 1, 15 });
    nlc_check((i8_4 { 1, 13, 1, 13 } & i8_4 { 1, 3, 1, 3 }) == i8_4 { 1, 1, 1, 1 });
    nlc_check((i8_4 { 24, 13, 24, 13 } >> i8_4 { 2, 1, 2, 1 }) == i8_4 { 6, 6, 6, 6 });
    nlc_check((i8_4 { 1, 13, 1, 13 } << i8_4 { 3, 2, 3, 2 }) == i8_4 { 8, 52, 8, 52 });
    nlc_check((i8_4 { 1, 13, 1, 13 } ^ i8_4 { 3, 2, 3, 2 }) == i8_4 { 2, 15, 2, 15 });
    nlc_check((i8_4 { 7, 13, 7, 13 } % i8_4 { 5, 4, 5, 4 }) == i8_4 { 2, 1, 2, 1 });

    i8 const cst { 3 };
    a = { 1, 13, 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1 && a.z == 13 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
    nlc_check(a.w == a[3]);
}

nlc_test("u16_4 operators") {
    u16_4 a { .x = 6, .y = 3, .z = 6, .w = 3 };
    u16_4 b { .x = 3, .y = 4, .z = 3, .w = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3 && b.z == 2 && b.w == 3, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2 && b.z == 1 && b.w == 2, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4 && a.z == 7 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5 && a.z == 8 && a.w == 5, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 2 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 1 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 2 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 1 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 1, 13, 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15 && a.z == 1 && a.w == 15, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8 && a.z == 2 && a.w == 8, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0 && a.z == 0 && a.w == 0, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 7, 13, 7, 13 };
    a %= u16_4 { 5, 4, 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 2 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check((u16_4 { 1, 13, 1, 13 } | u16_4 { 1, 2, 1, 2 }) == u16_4 { 1, 15, 1, 15 });
    nlc_check((u16_4 { 1, 13, 1, 13 } & u16_4 { 1, 3, 1, 3 }) == u16_4 { 1, 1, 1, 1 });
    nlc_check((u16_4 { 24, 13, 24, 13 } >> u16_4 { 2, 1, 2, 1 }) == u16_4 { 6, 6, 6, 6 });
    nlc_check((u16_4 { 1, 13, 1, 13 } << u16_4 { 3, 2, 3, 2 }) == u16_4 { 8, 52, 8, 52 });
    nlc_check((u16_4 { 1, 13, 1, 13 } ^ u16_4 { 3, 2, 3, 2 }) == u16_4 { 2, 15, 2, 15 });
    nlc_check((u16_4 { 7, 13, 7, 13 } % u16_4 { 5, 4, 5, 4 }) == u16_4 { 2, 1, 2, 1 });

    u16 const cst { 3 };
    a = { 1, 13, 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1 && a.z == 13 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
    nlc_check(a.w == a[3]);
}

nlc_test("i16_4 operators") {
    i16_4 a { .x = -6, .y = 3, .z = -6, .w = 3 };
    i16_4 b { .x = 3, .y = -4, .z = 3, .w = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5 && b.z == 2 && b.w == -5, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6 && b.z == 1 && b.w == -6, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4 && a.z == -5 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5 && a.z == -4 && a.w == 5, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == 2 && a.w == -12,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 1 && a.w == 36, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == 2 && a.w == -12,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 1 && a.w == 36, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6 && a.z == -1 && a.w == 6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 1, 13, 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1 && a.z == 1 && a.w == -1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a <<= i16_4 { 2, 3, 2, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48 && a.z == 4 && a.w == -48,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a >>= i16_4 { 2, 3, 2, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a ^= i16_4 { 1, 7, 1, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3 && a.z == 0 && a.w == -3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 7, 13, 7, 13 };
    a %= i16_4 { 5, 4, 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 2 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check((i16_4 { 1, 13, 1, 13 } | i16_4 { 1, 2, 1, 2 }) == i16_4 { 1, 15, 1, 15 });
    nlc_check((i16_4 { 1, 13, 1, 13 } & i16_4 { 1, 3, 1, 3 }) == i16_4 { 1, 1, 1, 1 });
    nlc_check((i16_4 { 24, 13, 24, 13 } >> i16_4 { 2, 1, 2, 1 }) == i16_4 { 6, 6, 6, 6 });
    nlc_check((i16_4 { 1, 13, 1, 13 } << i16_4 { 3, 2, 3, 2 }) == i16_4 { 8, 52, 8, 52 });
    nlc_check((i16_4 { 1, 13, 1, 13 } ^ i16_4 { 3, 2, 3, 2 }) == i16_4 { 2, 15, 2, 15 });
    nlc_check((i16_4 { 7, 13, 7, 13 } % i16_4 { 5, 4, 5, 4 }) == i16_4 { 2, 1, 2, 1 });

    i16 const cst { 3 };
    a = { 1, 13, 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1 && a.z == 13 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
    nlc_check(a.w == a[3]);
}

nlc_test("u32_4 operators") {
    u32_4 a { .x = 6, .y = 3, .z = 6, .w = 3 };
    u32_4 b { .x = 3, .y = 4, .z = 3, .w = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3 && b.z == 2 && b.w == 3, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2 && b.z == 1 && b.w == 2, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4 && a.z == 7 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5 && a.z == 8 && a.w == 5, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 2 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 1 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 2 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 1 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 1, 13, 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15 && a.z == 1 && a.w == 15, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8 && a.z == 2 && a.w == 8, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0 && a.z == 0 && a.w == 0, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 7, 13, 7, 13 };
    a %= u32_4 { 5, 4, 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 2 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check((u32_4 { 1, 13, 1, 13 } | u32_4 { 1, 2, 1, 2 }) == u32_4 { 1, 15, 1, 15 });
    nlc_check((u32_4 { 1, 13, 1, 13 } & u32_4 { 1, 3, 1, 3 }) == u32_4 { 1, 1, 1, 1 });
    nlc_check((u32_4 { 24, 13, 24, 13 } >> u32_4 { 2, 1, 2, 1 }) == u32_4 { 6, 6, 6, 6 });
    nlc_check((u32_4 { 1, 13, 1, 13 } << u32_4 { 3, 2, 3, 2 }) == u32_4 { 8, 52, 8, 52 });
    nlc_check((u32_4 { 1, 13, 1, 13 } ^ u32_4 { 3, 2, 3, 2 }) == u32_4 { 2, 15, 2, 15 });
    nlc_check((u32_4 { 7, 13, 7, 13 } % u32_4 { 5, 4, 5, 4 }) == u32_4 { 2, 1, 2, 1 });

    u32 const cst { 3 };
    a = { 1, 13, 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1 && a.z == 13 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
    nlc_check(a.w == a[3]);
}

nlc_test("i32_4 operators") {
    i32_4 a { .x = -6, .y = 3, .z = -6, .w = 3 };
    i32_4 b { .x = 3, .y = -4, .z = 3, .w = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5 && b.z == 2 && b.w == -5, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6 && b.z == 1 && b.w == -6, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4 && a.z == -5 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5 && a.z == -4 && a.w == 5, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == 2 && a.w == -12,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 1 && a.w == 36, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == 2 && a.w == -12,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 1 && a.w == 36, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6 && a.z == -1 && a.w == 6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 1, 13, 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1 && a.z == 1 && a.w == -1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a <<= i32_4 { 2, 3, 2, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48 && a.z == 4 && a.w == -48,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a >>= i32_4 { 2, 3, 2, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a ^= i32_4 { 1, 7, 1, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3 && a.z == 0 && a.w == -3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 7, 13, 7, 13 };
    a %= i32_4 { 5, 4, 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 2 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check((i32_4 { 1, 13, 1, 13 } | i32_4 { 1, 2, 1, 2 }) == i32_4 { 1, 15, 1, 15 });
    nlc_check((i32_4 { 1, 13, 1, 13 } & i32_4 { 1, 3, 1, 3 }) == i32_4 { 1, 1, 1, 1 });
    nlc_check((i32_4 { 24, 13, 24, 13 } >> i32_4 { 2, 1, 2, 1 }) == i32_4 { 6, 6, 6, 6 });
    nlc_check((i32_4 { 1, 13, 1, 13 } << i32_4 { 3, 2, 3, 2 }) == i32_4 { 8, 52, 8, 52 });
    nlc_check((i32_4 { 1, 13, 1, 13 } ^ i32_4 { 3, 2, 3, 2 }) == i32_4 { 2, 15, 2, 15 });
    nlc_check((i32_4 { 7, 13, 7, 13 } % i32_4 { 5, 4, 5, 4 }) == i32_4 { 2, 1, 2, 1 });

    i32 const cst { 3 };
    a = { 1, 13, 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1 && a.z == 13 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
    nlc_check(a.w == a[3]);
}

nlc_test("u64_4 operators") {
    u64_4 a { .x = 6, .y = 3, .z = 6, .w = 3 };
    u64_4 b { .x = 3, .y = 4, .z = 3, .w = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3 && b.z == 2 && b.w == 3, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2 && b.z == 1 && b.w == 2, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4 && a.z == 7 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5 && a.z == 8 && a.w == 5, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 2 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 1 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 2 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 1 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 1, 13, 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15 && a.z == 1 && a.w == 15, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8 && a.z == 2 && a.w == 8, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0 && a.z == 0 && a.w == 0, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 7, 13, 7, 13 };
    a %= u64_4 { 5, 4, 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 2 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check((u64_4 { 1, 13, 1, 13 } | u64_4 { 1, 2, 1, 2 }) == u64_4 { 1, 15, 1, 15 });
    nlc_check((u64_4 { 1, 13, 1, 13 } & u64_4 { 1, 3, 1, 3 }) == u64_4 { 1, 1, 1, 1 });
    nlc_check((u64_4 { 24, 13, 24, 13 } >> u64_4 { 2, 1, 2, 1 }) == u64_4 { 6, 6, 6, 6 });
    nlc_check((u64_4 { 1, 13, 1, 13 } << u64_4 { 3, 2, 3, 2 }) == u64_4 { 8, 52, 8, 52 });
    nlc_check((u64_4 { 1, 13, 1, 13 } ^ u64_4 { 3, 2, 3, 2 }) == u64_4 { 2, 15, 2, 15 });
    nlc_check((u64_4 { 7, 13, 7, 13 } % u64_4 { 5, 4, 5, 4 }) == u64_4 { 2, 1, 2, 1 });

    u64 const cst { 3 };
    a = { 1, 13, 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1 && a.z == 13 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
    nlc_check(a.w == a[3]);
}

nlc_test("i64_4 operators") {
    i64_4 a { .x = -6, .y = 3, .z = -6, .w = 3 };
    i64_4 b { .x = 3, .y = -4, .z = 3, .w = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5 && b.z == 2 && b.w == -5, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6 && b.z == 1 && b.w == -6, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4 && a.z == -5 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5 && a.z == -4 && a.w == 5, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == 2 && a.w == -12,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 1 && a.w == 36, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == 2 && a.w == -12,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 1 && a.w == 36, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6 && a.z == -1 && a.w == 6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 1, 13, 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1 && a.z == 1 && a.w == -1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a <<= i64_4 { 2, 3, 2, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48 && a.z == 4 && a.w == -48,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a >>= i64_4 { 2, 3, 2, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a ^= i64_4 { 1, 7, 1, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3 && a.z == 0 && a.w == -3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 7, 13, 7, 13 };
    a %= i64_4 { 5, 4, 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 2 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check((i64_4 { 1, 13, 1, 13 } | i64_4 { 1, 2, 1, 2 }) == i64_4 { 1, 15, 1, 15 });
    nlc_check((i64_4 { 1, 13, 1, 13 } & i64_4 { 1, 3, 1, 3 }) == i64_4 { 1, 1, 1, 1 });
    nlc_check((i64_4 { 24, 13, 24, 13 } >> i64_4 { 2, 1, 2, 1 }) == i64_4 { 6, 6, 6, 6 });
    nlc_check((i64_4 { 1, 13, 1, 13 } << i64_4 { 3, 2, 3, 2 }) == i64_4 { 8, 52, 8, 52 });
    nlc_check((i64_4 { 1, 13, 1, 13 } ^ i64_4 { 3, 2, 3, 2 }) == i64_4 { 2, 15, 2, 15 });
    nlc_check((i64_4 { 7, 13, 7, 13 } % i64_4 { 5, 4, 5, 4 }) == i64_4 { 2, 1, 2, 1 });

    i64 const cst { 3 };
    a = { 1, 13, 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1 && a.z == 13 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
    nlc_check(a.w == a[3]);
}

nlc_test("usize_4 operators") {
    usize_4 a { .x = 6, .y = 3, .z = 6, .w = 3 };
    usize_4 b { .x = 3, .y = 4, .z = 3, .w = 4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == 3 && b.z == 2 && b.w == 3, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    --b;
    nlc_check_msg(b.x == 1 && b.y == 2 && b.z == 1 && b.w == 2, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    a++;
    nlc_check_msg(a.x == 7 && a.y == 4 && a.z == 7 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    ++a;
    nlc_check_msg(a.x == 8 && a.y == 5 && a.z == 8 && a.w == 5, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 2 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 1 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == 4 && a.z == 2 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 4 && a.z == 1 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 1, 13, 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == 15 && a.z == 1 && a.w == 15, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a <<= b;
    nlc_check_msg(a.x == 2 && a.y == 8 && a.z == 2 && a.w == 8, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a >>= b;
    nlc_check_msg(a.x == 1 && a.y == 2 && a.z == 1 && a.w == 2, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a ^= b;
    nlc_check_msg(a.x == 0 && a.y == 0 && a.z == 0 && a.w == 0, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 7, 13, 7, 13 };
    a %= usize_4 { 5, 4, 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 2 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check((usize_4 { 1, 13, 1, 13 } | usize_4 { 1, 2, 1, 2 }) == usize_4 { 1, 15, 1, 15 });
    nlc_check((usize_4 { 1, 13, 1, 13 } & usize_4 { 1, 3, 1, 3 }) == usize_4 { 1, 1, 1, 1 });
    nlc_check((usize_4 { 24, 13, 24, 13 } >> usize_4 { 2, 1, 2, 1 }) == usize_4 { 6, 6, 6, 6 });
    nlc_check((usize_4 { 1, 13, 1, 13 } << usize_4 { 3, 2, 3, 2 }) == usize_4 { 8, 52, 8, 52 });
    nlc_check((usize_4 { 1, 13, 1, 13 } ^ usize_4 { 3, 2, 3, 2 }) == usize_4 { 2, 15, 2, 15 });
    nlc_check((usize_4 { 7, 13, 7, 13 } % usize_4 { 5, 4, 5, 4 }) == usize_4 { 2, 1, 2, 1 });

    usize const cst { 3 };
    a = { 1, 13, 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1 && a.z == 13 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
    nlc_check(a.w == a[3]);
}

nlc_test("isize_4 operators") {
    isize_4 a { .x = -6, .y = 3, .z = -6, .w = 3 };
    isize_4 b { .x = 3, .y = -4, .z = 3, .w = -4 };

    b--;
    nlc_check_msg(b.x == 2 && b.y == -5 && b.z == 2 && b.w == -5, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    --b;
    nlc_check_msg(b.x == 1 && b.y == -6 && b.z == 1 && b.w == -6, "x=", b.x, ", y=", b.y, ", z=", b.z, ", w=", b.w);
    a++;
    nlc_check_msg(a.x == -5 && a.y == 4 && a.z == -5 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    ++a;
    nlc_check_msg(a.x == -4 && a.y == 5 && a.z == -4 && a.w == 5, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == 2 && a.w == -12,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a -= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 1 && a.w == 36, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 2 && a.y == -12 && a.z == 2 && a.w == -12,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a = a - b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 1 && a.y == 36 && a.z == 1 && a.w == 36, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = -a;
    nlc_check_msg(a.x == -1 && a.y == 6 && a.z == -1 && a.w == 6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 1, 13, 1, 13 };

    a |= b;
    nlc_check_msg(a.x == 1 && a.y == -1 && a.z == 1 && a.w == -1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a &= b;
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a <<= isize_4 { 2, 3, 2, 3 };
    nlc_check_msg(a.x == 4 && a.y == -48 && a.z == 4 && a.w == -48,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a >>= isize_4 { 2, 3, 2, 3 };
    nlc_check_msg(a.x == 1 && a.y == -6 && a.z == 1 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a ^= isize_4 { 1, 7, 1, 7 };
    nlc_check_msg(a.x == 0 && a.y == -3 && a.z == 0 && a.w == -3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = { 7, 13, 7, 13 };
    a %= isize_4 { 5, 4, 5, 4 };
    nlc_check_msg(a.x == 2 && a.y == 1 && a.z == 2 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check((isize_4 { 1, 13, 1, 13 } | isize_4 { 1, 2, 1, 2 }) == isize_4 { 1, 15, 1, 15 });
    nlc_check((isize_4 { 1, 13, 1, 13 } & isize_4 { 1, 3, 1, 3 }) == isize_4 { 1, 1, 1, 1 });
    nlc_check((isize_4 { 24, 13, 24, 13 } >> isize_4 { 2, 1, 2, 1 }) == isize_4 { 6, 6, 6, 6 });
    nlc_check((isize_4 { 1, 13, 1, 13 } << isize_4 { 3, 2, 3, 2 }) == isize_4 { 8, 52, 8, 52 });
    nlc_check((isize_4 { 1, 13, 1, 13 } ^ isize_4 { 3, 2, 3, 2 }) == isize_4 { 2, 15, 2, 15 });
    nlc_check((isize_4 { 7, 13, 7, 13 } % isize_4 { 5, 4, 5, 4 }) == isize_4 { 2, 1, 2, 1 });

    isize const cst { 3 };
    a = { 1, 13, 1, 13 };

    a += cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == 4 && a.y == 16 && a.z == 4 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == 3 && a.y == 39 && a.z == 3 && a.w == 39, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / cst;
    nlc_check_msg(a.x == 1 && a.y == 13 && a.z == 1 && a.w == 13, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 13 && a.y == 1 && a.z == 13 && a.w == 1, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
    nlc_check(a.w == a[3]);
}

nlc_test("f32_4 operators") {
    f32_4 a { .x = -6, .y = 3, .z = -6, .w = 3 };
    f32_4 b { .x = 3, .y = -4, .z = 3, .w = -4 };

    a = b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 6 && a.y == -8 && a.z == 6 && a.w == -8, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 9 && a.y == 16 && a.z == 9 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 6 && a.y == -8 && a.z == 6 && a.w == -8, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 9 && a.y == 16 && a.z == 9 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = -a;
    nlc_check_msg(a.x == -3 && a.y == 4 && a.z == -3 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    f32 const cst { 3 };
    a = { .x = -6, .y = 3, .z = -6, .w = 3 };

    a += cst;
    nlc_check_msg(a.x == -3 && a.y == 6 && a.z == -3 && a.w == 6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == -6 && a.w == 3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == -18 && a.y == 9 && a.z == -18 && a.w == 9,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a /= cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == -6 && a.w == 3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == -3 && a.y == 6 && a.z == -3 && a.w == 6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == -6 && a.w == 3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == -18 && a.y == 9 && a.z == -18 && a.w == 9,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a = a / cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == -6 && a.w == 3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 3 && a.y == -6 && a.z == 3 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
    nlc_check(a.w == a[3]);
}

nlc_test("f64_4 operators") {
    f64_4 a { .x = -6, .y = 3, .z = -6, .w = 3 };
    f64_4 b { .x = 3, .y = -4, .z = 3, .w = -4 };

    a = b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a += b;
    nlc_check_msg(a.x == 6 && a.y == -8 && a.z == 6 && a.w == -8, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= b;
    nlc_check_msg(a.x == 9 && a.y == 16 && a.z == 9 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a /= b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a + b;
    nlc_check_msg(a.x == 6 && a.y == -8 && a.z == 6 && a.w == -8, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * b;
    nlc_check_msg(a.x == 9 && a.y == 16 && a.z == 9 && a.w == 16, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a / b;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = +a;
    nlc_check_msg(a.x == 3 && a.y == -4 && a.z == 3 && a.w == -4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = -a;
    nlc_check_msg(a.x == -3 && a.y == 4 && a.z == -3 && a.w == 4, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    f64 const cst { 3 };
    a = { .x = -6, .y = 3, .z = -6, .w = 3 };

    a += cst;
    nlc_check_msg(a.x == -3 && a.y == 6 && a.z == -3 && a.w == 6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a -= cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == -6 && a.w == 3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a *= cst;
    nlc_check_msg(a.x == -18 && a.y == 9 && a.z == -18 && a.w == 9,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a /= cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == -6 && a.w == 3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = a + cst;
    nlc_check_msg(a.x == -3 && a.y == 6 && a.z == -3 && a.w == 6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a - cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == -6 && a.w == 3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);
    a = a * cst;
    nlc_check_msg(a.x == -18 && a.y == 9 && a.z == -18 && a.w == 9,
                  "x=",
                  a.x,
                  ", y=",
                  a.y,
                  ", z=",
                  a.z,
                  ", w=",
                  a.w);
    a = a / cst;
    nlc_check_msg(a.x == -6 && a.y == 3 && a.z == -6 && a.w == 3, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    a = nlc_swizzle(a, w, z, y, x);
    nlc_check_msg(a.x == 3 && a.y == -6 && a.z == 3 && a.w == -6, "x=", a.x, ", y=", a.y, ", z=", a.z, ", w=", a.w);

    nlc_check(a.x == a[0]);
    nlc_check(a.y == a[1]);
    nlc_check(a.z == a[2]);
    nlc_check(a.w == a[3]);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

nlc_test("u8_4 common functions") {
    nlc_check(min(u8_4 { 3, 5, 5, 2 }, u8_4 { 6, 2, 3, 1 }) == u8_4 { 3, 2, 3, 1 });
    nlc_check(max(u8_4 { 3, 5, 5, 2 }, u8_4 { 6, 2, 3, 4 }) == u8_4 { 6, 5, 5, 4 });
    nlc_check(clamp(u8_4 { 3, 5, 0, 1 }, u8_4 { 6, 2, 1, 2 }, u8_4 { 10, 4, 3, 4 }) ==
              u8_4 { 6, 4, 1, 2 });
    nlc_check(sum(u8_4 { 5, 12, 3, 1 }) == 21);
    nlc_check(dot(u8_4 { 1, 5, 2, 2 }, u8_4 { 4, 2, 4, 1 }) == 24);
    nlc_check(
        det(u8_4 { 2, 2, 0, 0 }, u8_4 { 2, 3, 0, 0 }, u8_4 { 1, 0, 2, 1 }, u8_4 { 0, 2, 1, 1 }) == 2);

    nlc_check(sqrnorm(u8_4 { 3, 4, 4, 5 }) == 66);
    nlc_check(norm(u8_4 { 3, 4, 4, 5 }) == 8);
    nlc_check(normalize(u8_4 { 2, 0, 0, 0 }) == u8_4 { 1, 0, 0, 0 });
    nlc_check(normalize(u8_4 { 0, 0, 0, 5 }) == u8_4 { 0, 0, 0, 1 });
    nlc_check(sqrdistance(u8_4 { 5, 6, 5, 1 }, u8_4 { 8, 10, 1, 6 }) == 66);
    nlc_check(distance(u8_4 { 5, 6, 5, 1 }, u8_4 { 8, 10, 1, 6 }) == 8);

    nlc_check(project(u8_4 { 2, 2, 0, 0 }, u8_4 { 1, 0, 0, 0 }) == i8_4 { 0, 2, 0, 0 });
    nlc_check(normalize_project(u8_4 { 2, 2, 0, 0 }, u8_4 { 8, 0, 0, 0 }) == i8_4 { 0, 2, 0, 0 });
}

nlc_test("i8_4 common functions") {
    nlc_check(abs(i8_4 { -2, -4, 1, 2 }) == i8_4 { 2, 4, 1, 2 });
    nlc_check(abs(i8_4 { 2, 4, 0, -2 }) == i8_4 { 2, 4, 0, 2 });
    nlc_check(sign(i8_4 { -2, -4, 2, 1 }) == i8_4 { -1, -1, 1, 1 });
    nlc_check(sign(i8_4 { 0, 4, -5, -3 }) == i8_4 { 0, 1, -1, -1 });
    nlc_check(min(i8_4 { 3, -5, 5, 2 }, i8_4 { 6, 2, -3, -1 }) == i8_4 { 3, -5, -3, -1 });
    nlc_check(max(i8_4 { 3, 5, -5, -2 }, i8_4 { -6, 2, -5, 4 }) == i8_4 { 3, 5, -5, 4 });
    nlc_check(clamp(i8_4 { 3, 5, 0, 1 }, i8_4 { -6, -4, -2, 2 }, i8_4 { 10, -2, 4, 2 }) ==
              i8_4 { 3, -2, 0, 2 });
    nlc_check(sum(i8_4 { 5, -12, 3, -5 }) == -9);
    nlc_check(dot(i8_4 { 1, -5, 2, -1 }, i8_4 { 4, 2, 7, 6 }) == 2);
    nlc_check(
        det(i8_4 { 2, 2, 0, 0 }, i8_4 { 2, 3, 0, 0 }, i8_4 { -1, 0, -2, 1 }, i8_4 { 0, -2, 1, -1 }) ==
        2);

    nlc_check(sqrnorm(i8_4 { -3, -4, -4, 5 }) == 66);
    nlc_check(norm(i8_4 { -3, -4, -4, 5 }) == 8);
    nlc_check(normalize(i8_4 { -2, 0, 0, 0 }) == i8_4 { -1, 0, 0, 0 });
    nlc_check(normalize(i8_4 { 0, 5, 0, 0 }) == i8_4 { 0, 1, 0, 0 });
    nlc_check(sqrdistance(i8_4 { -5, -6, 5, -1 }, i8_4 { -8, -10, 1, -6 }) == 66);
    nlc_check(distance(i8_4 { -5, -6, 5, -1 }, i8_4 { -8, -10, 1, -6 }) == 8);

    nlc_check(reflect(i8_4 { 2, -2, 0, 0 }, i8_4 { 0, -1, 0, 0 }) == i8_4 { 2, 2, 0, 0 });
    nlc_check(reflect(i8_4 { 2, 0, 0, 0 }, i8_4 { -1, 0, 0, 0 }) == i8_4 { -2, 0, 0, 0 });
    nlc_check(normalize_reflect(i8_4 { 2, -2, 0, 0 }, i8_4 { 0, -5, 0, 0 }) == i8_4 { 2, 2, 0, 0 });
    nlc_check(normalize_reflect(i8_4 { 2, 0, 0, 0 }, i8_4 { -8, 0, 0, 0 }) == i8_4 { -2, 0, 0, 0 });
    nlc_check(project(i8_4 { 2, 2, 0, 0 }, i8_4 { -1, 0, 0, 0 }) == i8_4 { 0, 2, 0, 0 });
    nlc_check(normalize_project(i8_4 { 2, 2, 0, 0 }, i8_4 { -6, 0, 0, 0 }) == i8_4 { 0, 2, 0, 0 });
}

nlc_test("u16_4 common functions") {
    nlc_check(min(u16_4 { 3, 5, 5, 2 }, u16_4 { 6, 2, 3, 1 }) == u16_4 { 3, 2, 3, 1 });
    nlc_check(max(u16_4 { 3, 5, 5, 2 }, u16_4 { 6, 2, 3, 4 }) == u16_4 { 6, 5, 5, 4 });
    nlc_check(clamp(u16_4 { 3, 5, 0, 1 }, u16_4 { 6, 2, 1, 2 }, u16_4 { 10, 4, 3, 4 }) ==
              u16_4 { 6, 4, 1, 2 });
    nlc_check(sum(u16_4 { 5, 12, 3, 1 }) == 21);
    nlc_check(dot(u16_4 { 1, 5, 2, 2 }, u16_4 { 4, 2, 4, 1 }) == 24);
    nlc_check(
        det(u16_4 { 2, 2, 0, 0 }, u16_4 { 2, 3, 0, 0 }, u16_4 { 1, 0, 2, 1 }, u16_4 { 0, 2, 1, 1 }) ==
        2);

    nlc_check(sqrnorm(u16_4 { 3, 4, 4, 5 }) == 66);
    nlc_check(norm(u16_4 { 3, 4, 4, 5 }) == 8);
    nlc_check(normalize(u16_4 { 2, 0, 0, 0 }) == u16_4 { 1, 0, 0, 0 });
    nlc_check(normalize(u16_4 { 0, 0, 0, 5 }) == u16_4 { 0, 0, 0, 1 });
    nlc_check(sqrdistance(u16_4 { 5, 6, 5, 1 }, u16_4 { 8, 10, 1, 6 }) == 66);
    nlc_check(distance(u16_4 { 5, 6, 5, 1 }, u16_4 { 8, 10, 1, 6 }) == 8);

    nlc_check(project(u16_4 { 2, 2, 0, 0 }, u16_4 { 1, 0, 0, 0 }) == i16_4 { 0, 2, 0, 0 });
    nlc_check(normalize_project(u16_4 { 2, 2, 0, 0 }, u16_4 { 8, 0, 0, 0 }) == i16_4 { 0, 2, 0, 0 });
}

nlc_test("i16_4 common functions") {
    nlc_check(abs(i16_4 { -2, -4, 1, 2 }) == i16_4 { 2, 4, 1, 2 });
    nlc_check(abs(i16_4 { 2, 4, 0, -2 }) == i16_4 { 2, 4, 0, 2 });
    nlc_check(sign(i16_4 { -2, -4, 2, 1 }) == i16_4 { -1, -1, 1, 1 });
    nlc_check(sign(i16_4 { 0, 4, -5, -3 }) == i16_4 { 0, 1, -1, -1 });
    nlc_check(min(i16_4 { 3, -5, 5, 2 }, i16_4 { 6, 2, -3, -1 }) == i16_4 { 3, -5, -3, -1 });
    nlc_check(max(i16_4 { 3, 5, -5, -2 }, i16_4 { -6, 2, -5, 4 }) == i16_4 { 3, 5, -5, 4 });
    nlc_check(clamp(i16_4 { 3, 5, 0, 1 }, i16_4 { -6, -4, -2, 2 }, i16_4 { 10, -2, 4, 2 }) ==
              i16_4 { 3, -2, 0, 2 });
    nlc_check(sum(i16_4 { 5, -12, 3, -5 }) == -9);
    nlc_check(dot(i16_4 { 1, -5, 2, -1 }, i16_4 { 4, 2, 7, 6 }) == 2);
    nlc_check(
        det(i16_4 { 2, 2, 0, 0 }, i16_4 { 2, 3, 0, 0 }, i16_4 { -1, 0, -2, 1 }, i16_4 { 0, -2, 1, -1 }) ==
        2);

    nlc_check(sqrnorm(i16_4 { -3, -4, -4, 5 }) == 66);
    nlc_check(norm(i16_4 { -3, -4, -4, 5 }) == 8);
    nlc_check(normalize(i16_4 { -2, 0, 0, 0 }) == i16_4 { -1, 0, 0, 0 });
    nlc_check(normalize(i16_4 { 0, 5, 0, 0 }) == i16_4 { 0, 1, 0, 0 });
    nlc_check(sqrdistance(i16_4 { -5, -6, 5, -1 }, i16_4 { -8, -10, 1, -6 }) == 66);
    nlc_check(distance(i16_4 { -5, -6, 5, -1 }, i16_4 { -8, -10, 1, -6 }) == 8);

    nlc_check(reflect(i16_4 { 2, -2, 0, 0 }, i16_4 { 0, -1, 0, 0 }) == i16_4 { 2, 2, 0, 0 });
    nlc_check(reflect(i16_4 { 2, 0, 0, 0 }, i16_4 { -1, 0, 0, 0 }) == i16_4 { -2, 0, 0, 0 });
    nlc_check(normalize_reflect(i16_4 { 2, -2, 0, 0 }, i16_4 { 0, -5, 0, 0 }) == i16_4 { 2, 2, 0, 0 });
    nlc_check(normalize_reflect(i16_4 { 2, 0, 0, 0 }, i16_4 { -8, 0, 0, 0 }) == i16_4 { -2, 0, 0, 0 });
    nlc_check(project(i16_4 { 2, 2, 0, 0 }, i16_4 { -1, 0, 0, 0 }) == i16_4 { 0, 2, 0, 0 });
    nlc_check(normalize_project(i16_4 { 2, 2, 0, 0 }, i16_4 { -6, 0, 0, 0 }) == i16_4 { 0, 2, 0, 0 });
}

nlc_test("u32_4 common functions") {
    nlc_check(min(u32_4 { 3, 5, 5, 2 }, u32_4 { 6, 2, 3, 1 }) == u32_4 { 3, 2, 3, 1 });
    nlc_check(max(u32_4 { 3, 5, 5, 2 }, u32_4 { 6, 2, 3, 4 }) == u32_4 { 6, 5, 5, 4 });
    nlc_check(clamp(u32_4 { 3, 5, 0, 1 }, u32_4 { 6, 2, 1, 2 }, u32_4 { 10, 4, 3, 4 }) ==
              u32_4 { 6, 4, 1, 2 });
    nlc_check(sum(u32_4 { 5, 12, 3, 1 }) == 21);
    nlc_check(dot(u32_4 { 1, 5, 2, 2 }, u32_4 { 4, 2, 4, 1 }) == 24);
    nlc_check(
        det(u32_4 { 2, 2, 0, 0 }, u32_4 { 2, 3, 0, 0 }, u32_4 { 1, 0, 2, 1 }, u32_4 { 0, 2, 1, 1 }) ==
        2);

    nlc_check(sqrnorm(u32_4 { 3, 4, 4, 5 }) == 66);
    nlc_check(norm(u32_4 { 3, 4, 4, 5 }) == 8);
    nlc_check(normalize(u32_4 { 2, 0, 0, 0 }) == u32_4 { 1, 0, 0, 0 });
    nlc_check(normalize(u32_4 { 0, 0, 0, 5 }) == u32_4 { 0, 0, 0, 1 });
    nlc_check(sqrdistance(u32_4 { 5, 6, 5, 1 }, u32_4 { 8, 10, 1, 6 }) == 66);
    nlc_check(distance(u32_4 { 5, 6, 5, 1 }, u32_4 { 8, 10, 1, 6 }) == 8);

    nlc_check(project(u32_4 { 2, 2, 0, 0 }, u32_4 { 1, 0, 0, 0 }) == i32_4 { 0, 2, 0, 0 });
    nlc_check(normalize_project(u32_4 { 2, 2, 0, 0 }, u32_4 { 8, 0, 0, 0 }) == i32_4 { 0, 2, 0, 0 });
}

nlc_test("i32_4 common functions") {
    nlc_check(abs(i32_4 { -2, -4, 1, 2 }) == i32_4 { 2, 4, 1, 2 });
    nlc_check(abs(i32_4 { 2, 4, 0, -2 }) == i32_4 { 2, 4, 0, 2 });
    nlc_check(sign(i32_4 { -2, -4, 2, 1 }) == i32_4 { -1, -1, 1, 1 });
    nlc_check(sign(i32_4 { 0, 4, -5, -3 }) == i32_4 { 0, 1, -1, -1 });
    nlc_check(min(i32_4 { 3, -5, 5, 2 }, i32_4 { 6, 2, -3, -1 }) == i32_4 { 3, -5, -3, -1 });
    nlc_check(max(i32_4 { 3, 5, -5, -2 }, i32_4 { -6, 2, -5, 4 }) == i32_4 { 3, 5, -5, 4 });
    nlc_check(clamp(i32_4 { 3, 5, 0, 1 }, i32_4 { -6, -4, -2, 2 }, i32_4 { 10, -2, 4, 2 }) ==
              i32_4 { 3, -2, 0, 2 });
    nlc_check(sum(i32_4 { 5, -12, 3, -5 }) == -9);
    nlc_check(dot(i32_4 { 1, -5, 2, -1 }, i32_4 { 4, 2, 7, 6 }) == 2);
    nlc_check(
        det(i32_4 { 2, 2, 0, 0 }, i32_4 { 2, 3, 0, 0 }, i32_4 { -1, 0, -2, 1 }, i32_4 { 0, -2, 1, -1 }) ==
        2);

    nlc_check(sqrnorm(i32_4 { -3, -4, -4, 5 }) == 66);
    nlc_check(norm(i32_4 { -3, -4, -4, 5 }) == 8);
    nlc_check(normalize(i32_4 { -2, 0, 0, 0 }) == i32_4 { -1, 0, 0, 0 });
    nlc_check(normalize(i32_4 { 0, 5, 0, 0 }) == i32_4 { 0, 1, 0, 0 });
    nlc_check(sqrdistance(i32_4 { -5, -6, 5, -1 }, i32_4 { -8, -10, 1, -6 }) == 66);
    nlc_check(distance(i32_4 { -5, -6, 5, -1 }, i32_4 { -8, -10, 1, -6 }) == 8);

    nlc_check(reflect(i32_4 { 2, -2, 0, 0 }, i32_4 { 0, -1, 0, 0 }) == i32_4 { 2, 2, 0, 0 });
    nlc_check(reflect(i32_4 { 2, 0, 0, 0 }, i32_4 { -1, 0, 0, 0 }) == i32_4 { -2, 0, 0, 0 });
    nlc_check(normalize_reflect(i32_4 { 2, -2, 0, 0 }, i32_4 { 0, -5, 0, 0 }) == i32_4 { 2, 2, 0, 0 });
    nlc_check(normalize_reflect(i32_4 { 2, 0, 0, 0 }, i32_4 { -8, 0, 0, 0 }) == i32_4 { -2, 0, 0, 0 });
    nlc_check(project(i32_4 { 2, 2, 0, 0 }, i32_4 { -1, 0, 0, 0 }) == i32_4 { 0, 2, 0, 0 });
    nlc_check(normalize_project(i32_4 { 2, 2, 0, 0 }, i32_4 { -6, 0, 0, 0 }) == i32_4 { 0, 2, 0, 0 });
}

nlc_test("u64_4 common functions") {
    nlc_check(min(u64_4 { 3, 5, 5, 2 }, u64_4 { 6, 2, 3, 1 }) == u64_4 { 3, 2, 3, 1 });
    nlc_check(max(u64_4 { 3, 5, 5, 2 }, u64_4 { 6, 2, 3, 4 }) == u64_4 { 6, 5, 5, 4 });
    nlc_check(clamp(u64_4 { 3, 5, 0, 1 }, u64_4 { 6, 2, 1, 2 }, u64_4 { 10, 4, 3, 4 }) ==
              u64_4 { 6, 4, 1, 2 });
    nlc_check(sum(u64_4 { 5, 12, 3, 1 }) == 21);
    nlc_check(dot(u64_4 { 1, 5, 2, 2 }, u64_4 { 4, 2, 4, 1 }) == 24);
    nlc_check(
        det(u64_4 { 2, 2, 0, 0 }, u64_4 { 2, 3, 0, 0 }, u64_4 { 1, 0, 2, 1 }, u64_4 { 0, 2, 1, 1 }) ==
        2);

    nlc_check(sqrnorm(u64_4 { 3, 4, 4, 5 }) == 66);
    nlc_check(norm(u64_4 { 3, 4, 4, 5 }) == 8);
    nlc_check(normalize(u64_4 { 2, 0, 0, 0 }) == u64_4 { 1, 0, 0, 0 });
    nlc_check(normalize(u64_4 { 0, 0, 0, 5 }) == u64_4 { 0, 0, 0, 1 });
    nlc_check(sqrdistance(u64_4 { 5, 6, 5, 1 }, u64_4 { 8, 10, 1, 6 }) == 66);
    nlc_check(distance(u64_4 { 5, 6, 5, 1 }, u64_4 { 8, 10, 1, 6 }) == 8);

    nlc_check(project(u64_4 { 2, 2, 0, 0 }, u64_4 { 1, 0, 0, 0 }) == i64_4 { 0, 2, 0, 0 });
    nlc_check(normalize_project(u64_4 { 2, 2, 0, 0 }, u64_4 { 8, 0, 0, 0 }) == i64_4 { 0, 2, 0, 0 });
}

nlc_test("i64_4 common functions") {
    nlc_check(abs(i64_4 { -2, -4, 1, 2 }) == i64_4 { 2, 4, 1, 2 });
    nlc_check(abs(i64_4 { 2, 4, 0, -2 }) == i64_4 { 2, 4, 0, 2 });
    nlc_check(sign(i64_4 { -2, -4, 2, 1 }) == i64_4 { -1, -1, 1, 1 });
    nlc_check(sign(i64_4 { 0, 4, -5, -3 }) == i64_4 { 0, 1, -1, -1 });
    nlc_check(min(i64_4 { 3, -5, 5, 2 }, i64_4 { 6, 2, -3, -1 }) == i64_4 { 3, -5, -3, -1 });
    nlc_check(max(i64_4 { 3, 5, -5, -2 }, i64_4 { -6, 2, -5, 4 }) == i64_4 { 3, 5, -5, 4 });
    nlc_check(clamp(i64_4 { 3, 5, 0, 1 }, i64_4 { -6, -4, -2, 2 }, i64_4 { 10, -2, 4, 2 }) ==
              i64_4 { 3, -2, 0, 2 });
    nlc_check(sum(i64_4 { 5, -12, 3, -5 }) == -9);
    nlc_check(dot(i64_4 { 1, -5, 2, -1 }, i64_4 { 4, 2, 7, 6 }) == 2);
    nlc_check(
        det(i64_4 { 2, 2, 0, 0 }, i64_4 { 2, 3, 0, 0 }, i64_4 { -1, 0, -2, 1 }, i64_4 { 0, -2, 1, -1 }) ==
        2);

    nlc_check(sqrnorm(i64_4 { -3, -4, -4, 5 }) == 66);
    nlc_check(norm(i64_4 { -3, -4, -4, 5 }) == 8);
    nlc_check(normalize(i64_4 { -2, 0, 0, 0 }) == i64_4 { -1, 0, 0, 0 });
    nlc_check(normalize(i64_4 { 0, 5, 0, 0 }) == i64_4 { 0, 1, 0, 0 });
    nlc_check(sqrdistance(i64_4 { -5, -6, 5, -1 }, i64_4 { -8, -10, 1, -6 }) == 66);
    nlc_check(distance(i64_4 { -5, -6, 5, -1 }, i64_4 { -8, -10, 1, -6 }) == 8);

    nlc_check(reflect(i64_4 { 2, -2, 0, 0 }, i64_4 { 0, -1, 0, 0 }) == i64_4 { 2, 2, 0, 0 });
    nlc_check(reflect(i64_4 { 2, 0, 0, 0 }, i64_4 { -1, 0, 0, 0 }) == i64_4 { -2, 0, 0, 0 });
    nlc_check(normalize_reflect(i64_4 { 2, -2, 0, 0 }, i64_4 { 0, -5, 0, 0 }) == i64_4 { 2, 2, 0, 0 });
    nlc_check(normalize_reflect(i64_4 { 2, 0, 0, 0 }, i64_4 { -8, 0, 0, 0 }) == i64_4 { -2, 0, 0, 0 });
    nlc_check(project(i64_4 { 2, 2, 0, 0 }, i64_4 { -1, 0, 0, 0 }) == i64_4 { 0, 2, 0, 0 });
    nlc_check(normalize_project(i64_4 { 2, 2, 0, 0 }, i64_4 { -6, 0, 0, 0 }) == i64_4 { 0, 2, 0, 0 });
}

nlc_test("usize_4 common functions") {
    nlc_check(min(usize_4 { 3, 5, 5, 2 }, usize_4 { 6, 2, 3, 1 }) == usize_4 { 3, 2, 3, 1 });
    nlc_check(max(usize_4 { 3, 5, 5, 2 }, usize_4 { 6, 2, 3, 4 }) == usize_4 { 6, 5, 5, 4 });
    nlc_check(clamp(usize_4 { 3, 5, 0, 1 }, usize_4 { 6, 2, 1, 2 }, usize_4 { 10, 4, 3, 4 }) ==
              usize_4 { 6, 4, 1, 2 });
    nlc_check(sum(usize_4 { 5, 12, 3, 1 }) == 21);
    nlc_check(dot(usize_4 { 1, 5, 2, 2 }, usize_4 { 4, 2, 4, 1 }) == 24);
    nlc_check(det(usize_4 { 2, 2, 0, 0 },
                  usize_4 { 2, 3, 0, 0 },
                  usize_4 { 1, 0, 2, 1 },
                  usize_4 { 0, 2, 1, 1 }) == 2);

    nlc_check(sqrnorm(usize_4 { 3, 4, 4, 5 }) == 66);
    nlc_check(norm(usize_4 { 3, 4, 4, 5 }) == 8);
    nlc_check(normalize(usize_4 { 2, 0, 0, 0 }) == usize_4 { 1, 0, 0, 0 });
    nlc_check(normalize(usize_4 { 0, 0, 0, 5 }) == usize_4 { 0, 0, 0, 1 });
    nlc_check(sqrdistance(usize_4 { 5, 6, 5, 1 }, usize_4 { 8, 10, 1, 6 }) == 66);
    nlc_check(distance(usize_4 { 5, 6, 5, 1 }, usize_4 { 8, 10, 1, 6 }) == 8);

    nlc_check(project(usize_4 { 2, 2, 0, 0 }, usize_4 { 1, 0, 0, 0 }) == isize_4 { 0, 2, 0, 0 });
    nlc_check(normalize_project(usize_4 { 2, 2, 0, 0 }, usize_4 { 8, 0, 0, 0 }) ==
              isize_4 { 0, 2, 0, 0 });
}

nlc_test("isize_4 common functions") {
    nlc_check(abs(isize_4 { -2, -4, 1, 2 }) == isize_4 { 2, 4, 1, 2 });
    nlc_check(abs(isize_4 { 2, 4, 0, -2 }) == isize_4 { 2, 4, 0, 2 });
    nlc_check(sign(isize_4 { -2, -4, 2, 1 }) == isize_4 { -1, -1, 1, 1 });
    nlc_check(sign(isize_4 { 0, 4, -5, -3 }) == isize_4 { 0, 1, -1, -1 });
    nlc_check(min(isize_4 { 3, -5, 5, 2 }, isize_4 { 6, 2, -3, -1 }) == isize_4 { 3, -5, -3, -1 });
    nlc_check(max(isize_4 { 3, 5, -5, -2 }, isize_4 { -6, 2, -5, 4 }) == isize_4 { 3, 5, -5, 4 });
    nlc_check(clamp(isize_4 { 3, 5, 0, 1 }, isize_4 { -6, -4, -2, 2 }, isize_4 { 10, -2, 4, 2 }) ==
              isize_4 { 3, -2, 0, 2 });
    nlc_check(sum(isize_4 { 5, -12, 3, -5 }) == -9);
    nlc_check(dot(isize_4 { 1, -5, 2, -1 }, isize_4 { 4, 2, 7, 6 }) == 2);
    nlc_check(det(isize_4 { 2, 2, 0, 0 },
                  isize_4 { 2, 3, 0, 0 },
                  isize_4 { -1, 0, -2, 1 },
                  isize_4 { 0, -2, 1, -1 }) == 2);

    nlc_check(sqrnorm(isize_4 { -3, -4, -4, 5 }) == 66);
    nlc_check(norm(isize_4 { -3, -4, -4, 5 }) == 8);
    nlc_check(normalize(isize_4 { -2, 0, 0, 0 }) == isize_4 { -1, 0, 0, 0 });
    nlc_check(normalize(isize_4 { 0, 5, 0, 0 }) == isize_4 { 0, 1, 0, 0 });
    nlc_check(sqrdistance(isize_4 { -5, -6, 5, -1 }, isize_4 { -8, -10, 1, -6 }) == 66);
    nlc_check(distance(isize_4 { -5, -6, 5, -1 }, isize_4 { -8, -10, 1, -6 }) == 8);

    nlc_check(reflect(isize_4 { 2, -2, 0, 0 }, isize_4 { 0, -1, 0, 0 }) == isize_4 { 2, 2, 0, 0 });
    nlc_check(reflect(isize_4 { 2, 0, 0, 0 }, isize_4 { -1, 0, 0, 0 }) == isize_4 { -2, 0, 0, 0 });
    nlc_check(normalize_reflect(isize_4 { 2, -2, 0, 0 }, isize_4 { 0, -5, 0, 0 }) ==
              isize_4 { 2, 2, 0, 0 });
    nlc_check(normalize_reflect(isize_4 { 2, 0, 0, 0 }, isize_4 { -8, 0, 0, 0 }) ==
              isize_4 { -2, 0, 0, 0 });
    nlc_check(project(isize_4 { 2, 2, 0, 0 }, isize_4 { -1, 0, 0, 0 }) == isize_4 { 0, 2, 0, 0 });
    nlc_check(normalize_project(isize_4 { 2, 2, 0, 0 }, isize_4 { -6, 0, 0, 0 }) ==
              isize_4 { 0, 2, 0, 0 });
}

nlc_test("f32_4 common functions") {
    nlc_check(abs(f32_4 { -2, -4, 1, 2 }) == f32_4 { 2, 4, 1, 2 });
    nlc_check(abs(f32_4 { 2, 4, 0, -2 }) == f32_4 { 2, 4, 0, 2 });
    nlc_check(sign(f32_4 { -2, -4, 2, 1 }) == f32_4 { -1, -1, 1, 1 });
    nlc_check(sign(f32_4 { 0, 4, -5, -3 }) == f32_4 { 1, 1, -1, -1 });
    nlc_check(min(f32_4 { 3, -5, 5, 2 }, f32_4 { 6, 2, -3, -1 }) == f32_4 { 3, -5, -3, -1 });
    nlc_check(max(f32_4 { 3, 5, -5, -2 }, f32_4 { -6, 2, -5, 4 }) == f32_4 { 3, 5, -5, 4 });
    nlc_check(clamp(f32_4 { 3, 5, 0, 1 }, f32_4 { -6, -4, -2, 2 }, f32_4 { 10, -2, 4, 2 }) ==
              f32_4 { 3, -2, 0, 2 });

    nlc_check_float_equality(sum(f32_4 { 5, -12, 3, -5 }), -9);
    nlc_check_float_equality(dot(f32_4 { 1, -5, 2, -1 }, f32_4 { 4, 2, 7, 6 }), 2);
    nlc_check_float_equality(det(f32_4 { 2, 2, 0, 0 },
                                 f32_4 { 2, 3, 0, 0 },
                                 f32_4 { -1, 0, -2, 1 },
                                 f32_4 { 0, -2, 1, -1 }),
                             2);

    nlc_check_float_equality(sqrnorm(f32_4 { -3, -4, -4, 5 }), 66);
    nlc_check_float_equality(norm(f32_4 { -3, -4, -4, 5 }), sqrt(66));
    nlc_check_float_equality(normalize(f32_4 { -2, 0, 0, 0 }), (f32_4 { -1, 0, 0, 0 }));
    nlc_check_float_equality(normalize(f32_4 { 0, 5, 0, 0 }), (f32_4 { 0, 1, 0, 0 }));
    nlc_check_float_equality(sqrdistance(f32_4 { -5, -6, 5, -1 }, f32_4 { -8, -10, 1, -6 }), 66);
    nlc_check_float_equality(distance(f32_4 { -5, -6, 5, -1 }, f32_4 { -8, -10, 1, -6 }), sqrt(66));

    nlc_check_float_equality(reflect(f32_4 { 2, -2, 0, 0 }, f32_4 { 0, -1, 0, 0 }),
                             (f32_4 { 2, 2, 0, 0 }));
    nlc_check_float_equality(reflect(f32_4 { 2, 0, 0, 0 }, f32_4 { -1, 0, 0, 0 }),
                             (f32_4 { -2, 0, 0, 0 }));
    nlc_check_float_equality(normalize_reflect(f32_4 { 2, -2, 0, 0 }, f32_4 { 0, -5, 0, 0 }),
                             (f32_4 { 2, 2, 0, 0 }));
    nlc_check_float_equality(normalize_reflect(f32_4 { 2, 0, 0, 0 }, f32_4 { -8, 0, 0, 0 }),
                             (f32_4 { -2, 0, 0, 0 }));
    nlc_check_float_equality(project(f32_4 { 2, 2, 0, 0 }, f32_4 { -1, 0, 0, 0 }),
                             (f32_4 { 0, 2, 0, 0 }));
    nlc_check_float_equality(normalize_project(f32_4 { 2, 2, 0, 0 }, f32_4 { -6, 0, 0, 0 }),
                             (f32_4 { 0, 2, 0, 0 }));
}

nlc_test("f64_4 common functions") {
    nlc_check(abs(f64_4 { -2, -4, 1, 2 }) == f64_4 { 2, 4, 1, 2 });
    nlc_check(abs(f64_4 { 2, 4, 0, -2 }) == f64_4 { 2, 4, 0, 2 });
    nlc_check(sign(f64_4 { -2, -4, 2, 1 }) == f64_4 { -1, -1, 1, 1 });
    nlc_check(sign(f64_4 { 0, 4, -5, -3 }) == f64_4 { 1, 1, -1, -1 });
    nlc_check(min(f64_4 { 3, -5, 5, 2 }, f64_4 { 6, 2, -3, -1 }) == f64_4 { 3, -5, -3, -1 });
    nlc_check(max(f64_4 { 3, 5, -5, -2 }, f64_4 { -6, 2, -5, 4 }) == f64_4 { 3, 5, -5, 4 });
    nlc_check(clamp(f64_4 { 3, 5, 0, 1 }, f64_4 { -6, -4, -2, 2 }, f64_4 { 10, -2, 4, 2 }) ==
              f64_4 { 3, -2, 0, 2 });

    nlc_check_double_equality(sum(f64_4 { 5, -12, 3, -5 }), -9);
    nlc_check_double_equality(dot(f64_4 { 1, -5, 2, -1 }, f64_4 { 4, 2, 7, 6 }), 2);
    nlc_check_double_equality(det(f64_4 { 2, 2, 0, 0 },
                                  f64_4 { 2, 3, 0, 0 },
                                  f64_4 { -1, 0, -2, 1 },
                                  f64_4 { 0, -2, 1, -1 }),
                              2);

    nlc_check_double_equality(sqrnorm(f64_4 { -3, -4, -4, 5 }), 66);
    nlc_check_double_equality(norm(f64_4 { -3, -4, -4, 5 }), static_cast<f64>(sqrt(66)));
    nlc_check_double_equality(normalize(f64_4 { -2, 0, 0, 0 }), (f64_4 { -1, 0, 0, 0 }));
    nlc_check_double_equality(normalize(f64_4 { 0, 5, 0, 0 }), (f64_4 { 0, 1, 0, 0 }));
    nlc_check_double_equality(sqrdistance(f64_4 { -5, -6, 5, -1 }, f64_4 { -8, -10, 1, -6 }), 66);
    nlc_check_double_equality(distance(f64_4 { -5, -6, 5, -1 }, f64_4 { -8, -10, 1, -6 }),
                              static_cast<f64>(sqrt(66)));

    nlc_check_double_equality(reflect(f64_4 { 2, -2, 0, 0 }, f64_4 { 0, -1, 0, 0 }),
                              (f64_4 { 2, 2, 0, 0 }));
    nlc_check_double_equality(reflect(f64_4 { 2, 0, 0, 0 }, f64_4 { -1, 0, 0, 0 }),
                              (f64_4 { -2, 0, 0, 0 }));
    nlc_check_double_equality(normalize_reflect(f64_4 { 2, -2, 0, 0 }, f64_4 { 0, -5, 0, 0 }),
                              (f64_4 { 2, 2, 0, 0 }));
    nlc_check_double_equality(normalize_reflect(f64_4 { 2, 0, 0, 0 }, f64_4 { -8, 0, 0, 0 }),
                              (f64_4 { -2, 0, 0, 0 }));
    nlc_check_double_equality(project(f64_4 { 2, 2, 0, 0 }, f64_4 { -1, 0, 0, 0 }),
                              (f64_4 { 0, 2, 0, 0 }));
    nlc_check_double_equality(normalize_project(f64_4 { 2, 2, 0, 0 }, f64_4 { -6, 0, 0, 0 }),
                              (f64_4 { 0, 2, 0, 0 }));
}

nlc_test("u8_4 lerp") {
    nlc_check(scalar_lerp(u8_4 { 0, 0, 0, 0 }, u8_4 { 255, 255, 255, 255 }, 0.5f) ==
              u8_4 { 127, 127, 127, 127 });
    nlc_check(scalar_lerp(u8_4 { 0, 0, 0, 0 }, u8_4 { 255, 255, 255, 255 }, 0.5) ==
              u8_4 { 127, 127, 127, 127 });
}
