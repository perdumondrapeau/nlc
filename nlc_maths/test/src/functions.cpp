/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/range.hpp>
#include <nlc/maths/functions.hpp>
#include <nlc/maths/simple_maths.hpp>
#include <nlc/test.hpp>

#include <math.h>

enum class error_type { absolute, relative };

template<typename FA, typename FB>
auto compare([[maybe_unused]] FA && fa,
             FB && fb,
             f32 const start,
             f32 const end,
             f32 const max_err,
             error_type error) -> void {
    constexpr auto steps = static_cast<u32>(10e4);
    auto const r = end - start;

    for (auto const i : nlc::range(0u, steps)) {
        auto const k = (r / static_cast<f32>(steps));
        auto const x = start + k * static_cast<f32>(i);
        nlc_check_msg(x >= start && x <= end, "x = ", x);

        auto const a = fa(x);
        auto const b = fb(x);

        auto err = nlc::abs(a - b);
        if (error == error_type::relative)
            err /= a;

        nlc_check_msg(err <= max_err,
                      "error too high for #",
                      i,
                      " x=",
                      x,
                      " : ",
                      err,
                      " > ",
                      max_err,
                      " -- ",
                      a,
                      " ",
                      b);
    }
}

nlc_test("exp") {
    constexpr auto max_error = 1e-5f;

    nlc_check(nlc::exp(0.f) == 1.f);

    compare(nlc::exp, expf, -10.f, 10.f, max_error, error_type::relative);
}

nlc_test("log") {
    constexpr auto max_error = 1.3e-5f;

    constexpr auto minf = -nlc::meta::limits<f32>::infinity;
    nlc_check(nlc::log(0.f) == minf);
    nlc_check(nlc::log(-0.1f) == minf);
    nlc_check(nlc::log(-100.f) == minf);
    nlc_check(nlc::log(-123.456e12f) == minf);
    nlc_check(nlc::abs(nlc::log(1.f)) - 1.f <= max_error);

    compare(nlc::log, logf, 1e-10f, 1.f, max_error, error_type::absolute);
}

nlc_test("sqrt") {
    constexpr auto max_error = 1e-12f;

    compare(nlc::sqrt, sqrtf, 1e-10f, 1000000.f, max_error, error_type::absolute);
}

nlc_test("sqrt") {
    constexpr auto max_error = 1e-3f;  // This is not very well thought.

    compare(nlc::rsqrt, [](f32 x) { return 1.f / sqrtf(x); }, 1e-10f, 100000.f, max_error, error_type::relative);
}
