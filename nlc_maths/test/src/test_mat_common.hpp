/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/range.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/maths/mat2x2.hpp>
#include <nlc/maths/mat3x3.hpp>
#include <nlc/maths/mat4x4.hpp>
#include <nlc/maths/vec2.hpp>
#include <nlc/maths/vec3.hpp>
#include <nlc/maths/vec4.hpp>
#include <nlc/test.hpp>

#include <memory.h>

namespace impl {

static nlc::standard_allocator allocator;

template<typename T, usize Dim> struct Traits final {};

template<typename T> struct Traits<T, 2> final {
    using MatType = nlc::mat2x2<T>;
    using VecType = nlc::vec2<T>;

    static auto make_zero_matrix() -> MatType { return { VecType { 0, 0 }, VecType { 0, 0 } }; }

    static auto get_axis(MatType & mat, usize const index) -> VecType & {
        nlc_check(index < 2);
        switch (index) {
            case 0: return mat.x_axis;
            case 1: return mat.y_axis;
            default: return mat.x_axis;
        }
    }
};

template<typename T> struct Traits<T, 3> final {
    using MatType = nlc::mat3x3<T>;
    using VecType = nlc::vec3<T>;

    static auto make_zero_matrix() -> MatType {
        return { VecType { 0, 0, 0 }, VecType { 0, 0, 0 }, VecType { 0, 0, 0 } };
    }

    static auto get_axis(MatType & mat, usize const index) -> VecType & {
        nlc_check(index < 3);
        switch (index) {
            case 0: return mat.x_axis;
            case 1: return mat.y_axis;
            case 2: return mat.z_axis;
            default: return mat.x_axis;
        }
    }
};

template<typename T> struct Traits<T, 4> final {
    using MatType = nlc::mat4x4<T>;
    using VecType = nlc::vec4<T>;

    static auto make_zero_matrix() -> MatType {
        return { VecType { 0, 0, 0, 0 },
                 VecType { 0, 0, 0, 0 },
                 VecType { 0, 0, 0, 0 },
                 VecType { 0, 0, 0, 0 } };
    }

    static auto get_axis(MatType & mat, usize const index) -> VecType & {
        nlc_check(index < 4);
        switch (index) {
            case 0: return mat.x_axis;
            case 1: return mat.y_axis;
            case 2: return mat.z_axis;
            case 3: return mat.w_axis;
            default: return mat.x_axis;
        }
    }
};

static auto get_axis_name(usize const index) -> char const * {
    nlc_check(index < 4);

    switch (index) {
        case 0: return "X";
        case 1: return "Y";
        case 2: return "Z";
        case 3: return "W";
        default: return "unknown";
    }
}

template<typename T, usize Dim>
auto are_matrices_equal(typename ::impl::Traits<T, Dim>::MatType const & lhs,
                        typename ::impl::Traits<T, Dim>::MatType const & rhs) -> bool {
    for (usize i : nlc::range(0u, Dim)) {
        for (usize j : nlc::range(0u, Dim)) {
            if (lhs[i][j] != rhs[i][j])
                return false;
        }
    }

    return true;
}

template<typename T, usize Dim>
auto are_vectors_equal(typename ::impl::Traits<T, Dim>::VecType const & lhs,
                       typename ::impl::Traits<T, Dim>::VecType const & rhs) -> bool {
    for (usize i : nlc::range(0u, Dim)) {
        if (lhs[i] != rhs[i])
            return false;
    }

    return true;
}

template<typename T, usize Dim>
auto check_matrix_value(typename Traits<T, Dim>::MatType const & value,
                        typename Traits<T, Dim>::MatType const & expected) -> void {
    auto const is_value_expected = are_matrices_equal<T, Dim>(value, expected);

    nlc::string_stream sstr { allocator };

    if (is_value_expected == false) {
        for (usize i : nlc::range(0u, Dim)) {
            for (usize j : nlc::range(0u, Dim)) {
                if (i != 0u || j != 0u)
                    sstr << '\n';

                auto const cmp_result = value[i][j] == expected[i][j] ? nlc::string_view { "OK" }
                                                                      : nlc::string_view { "FAIL" };
                sstr << "(" << j << ", " << i << ") " << cmp_result << ": expected "
                     << expected[i][j] << ", got " << value[i][j];
            }
        }
    }

    nlc_check_msg(is_value_expected, sstr.c_str());
}

template<typename T, usize Dim>
auto check_vector_value(typename Traits<T, Dim>::VecType const & value,
                        typename Traits<T, Dim>::VecType const & expected) -> void {
    auto const is_value_expected = are_vectors_equal<T, Dim>(value, expected);

    nlc::string_stream sstr { allocator };

    if (is_value_expected == false) {
        for (usize i : nlc::range(0u, Dim)) {
            if (i != 0u)
                sstr << '\n';

            auto const cmp_result =
                value[i] == expected[i] ? nlc::string_view { "OK" } : nlc::string_view { "FAIL" };
            sstr << nlc::string_view { get_axis_name(i), 1 } << " " << cmp_result << ": expected "
                 << expected[i] << ", got " << value[i];
        }
    }

    nlc_check_msg(is_value_expected, sstr.c_str());
}
}  // namespace impl

template<typename T, usize Dim> auto check_identity() -> void {
    auto const id = ::impl::Traits<T, Dim>::MatType::identity();

    for (usize i : nlc::range(0u, Dim)) {
        for (usize j : nlc::range(0u, Dim)) {
            auto const expected_value = i == j ? static_cast<T>(1) : static_cast<T>(0);
            nlc_check_msg(id[i][j] == expected_value,
                          "at (",
                          i,
                          ", ",
                          j,
                          "): got ",
                          id[i][j],
                          ", expected ",
                          expected_value);
        }
    }
}

template<typename T, usize Dim> auto check_accessors() -> void {
    using Traits = ::impl::Traits<T, Dim>;
    auto mat = Traits::make_zero_matrix();
    auto const * const mat_as_ptr = reinterpret_cast<T const *>(&mat);

    for (usize i : nlc::range(0u, Dim)) {
        for (usize j : nlc::range(0u, Dim)) {
            nlc_check_msg(&mat[i][j] == &mat_as_ptr[i * Dim + j],
                          "access to row ",
                          i,
                          ", column ",
                          j,
                          " is not equivalent to access to element ",
                          i * Dim + j,
                          " in memory");

            nlc_check_msg(&mat[i][j] == &Traits::get_axis(mat, i)[j],
                          "access to row ",
                          i,
                          ", column ",
                          j,
                          " is not equivalent to access to element ",
                          j,
                          " of ",
                          ::impl::get_axis_name(i),
                          " axis");
        }
    }
}

template<typename T>
auto check_matrix_value(nlc::mat2x2<T> const & value, nlc::mat2x2<T> const & expected) -> void {
    ::impl::check_matrix_value<T, 2>(value, expected);
}

template<typename T>
auto check_matrix_value(nlc::mat3x3<T> const & value, nlc::mat3x3<T> const & expected) -> void {
    ::impl::check_matrix_value<T, 3>(value, expected);
}

template<typename T>
auto check_matrix_value(nlc::mat4x4<T> const & value, nlc::mat4x4<T> const & expected) -> void {
    ::impl::check_matrix_value<T, 4>(value, expected);
}

template<typename T>
auto check_vector_value(nlc::vec2<T> const & value, nlc::vec2<T> const & expected) -> void {
    ::impl::check_vector_value<T, 2>(value, expected);
}

template<typename T>
auto check_vector_value(nlc::vec3<T> const & value, nlc::vec3<T> const & expected) -> void {
    ::impl::check_vector_value<T, 3>(value, expected);
}

template<typename T>
auto check_vector_value(nlc::vec4<T> const & value, nlc::vec4<T> const & expected) -> void {
    ::impl::check_vector_value<T, 4>(value, expected);
}
