/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/maths/mat4x4.hpp>
#include <nlc/maths/mat4x4_common.hpp>
#include <nlc/test.hpp>

#include "test_mat_common.hpp"

using namespace nlc;

nlc_test("mat4x4 identity") {
    check_identity<u8, 4>();
    check_identity<i8, 4>();
    check_identity<u16, 4>();
    check_identity<i16, 4>();
    check_identity<u32, 4>();
    check_identity<i32, 4>();
    check_identity<u64, 4>();
    check_identity<i64, 4>();
    check_identity<usize, 4>();
    check_identity<isize, 4>();
    check_identity<f32, 4>();
    check_identity<f64, 4>();
}

nlc_test("mat4x4 accessors") {
    check_accessors<i8, 4>();
    check_accessors<u16, 4>();
    check_accessors<i16, 4>();
    check_accessors<u32, 4>();
    check_accessors<i32, 4>();
    check_accessors<u64, 4>();
    check_accessors<i64, 4>();
    check_accessors<usize, 4>();
    check_accessors<isize, 4>();
    check_accessors<f32, 4>();
    check_accessors<f64, 4>();
}

nlc_test("u8_4x4 operators") {
    constexpr u8_4x4 a { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } };
    constexpr u8_4x4 b { { 5, 1, 0, 2 }, { 0, 4, 7, 1 }, { 2, 1, 2, 0 }, { 2, 0, 3, 1 } };
    constexpr u8_4x4 a_times_b_expected_res { { 16, 31, 21, 17 },
                                              { 31, 16, 73, 34 },
                                              { 12, 11, 26, 10 },
                                              { 7, 12, 31, 16 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u8_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } } *
                           u8_4 { 6, 2, 1, 3 },
                       u8_4 { 25, 42, 37, 28 });
    check_vector_value(u8_4 { 6, 2, 1, 3 } *
                           u8_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } },
                       u8_4 { 26, 52, 23, 24 });
}

nlc_test("u8_4x4 common functions") {
    check_matrix_value(nlc::transpose(
                           u8_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } }),
                       u8_4x4 { { 2, 6, 1, 0 }, { 4, 3, 0, 4 }, { 3, 4, 8, 1 }, { 1, 2, 3, 5 } });
    nlc_check_msg(nlc::det(u8_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { 1, 0, 2, 1 }, { 0, 2, 1, 1 } }) ==
                      2,
                  "det=",
                  nlc::det(u8_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { 1, 0, 2, 1 }, { 0, 2, 1, 1 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("i8_4x4 operators") {
    constexpr i8_4x4 a { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } };
    constexpr i8_4x4 b { { -5, -1, 0, 2 }, { 0, -4, 7, 1 }, { 2, 1, 2, 0 }, { 2, 0, -3, 1 } };
    constexpr i8_4x4 a_times_b_expected_res { { -4, 15, -21, -17 },
                                              { 31, 8, -73, 8 },
                                              { 0, -11, -6, 10 },
                                              { 1, -12, 29, -12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i8_4x4 { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } } *
                           i8_4 { 6, 2, 1, 3 },
                       i8_4 { 1, -42, 15, -2 });
    check_vector_value(i8_4 { 6, 2, 1, 3 } *
                           i8_4x4 { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } },
                       i8_4 { 10, -32, 7, -24 });
}

nlc_test("i8_4x4 common functions") {
    check_matrix_value(
        nlc::transpose(i8_4x4 { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } }),
        i8_4x4 { { 2, -6, 1, 0 }, { -4, -3, 0, -4 }, { 3, 4, -8, -1 }, { 1, 2, 3, -5 } });
    nlc_check_msg(
        nlc::det(i8_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }) == 2,
        "det=",
        nlc::det(i8_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }));

    check_matrix_value(
        nlc::inverse(i8_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }),
        i8_4x4 { { 1, -1, 0, 0 }, { -1, 1, 0, 0 }, { 0, -1, -1, -1 }, { 2, -3, -1, -2 } });
}

nlc_test("u16_4x4 operators") {
    constexpr u16_4x4 a { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } };
    constexpr u16_4x4 b { { 5, 1, 0, 2 }, { 0, 4, 7, 1 }, { 2, 1, 2, 0 }, { 2, 0, 3, 1 } };
    constexpr u16_4x4 a_times_b_expected_res { { 16, 31, 21, 17 },
                                               { 31, 16, 73, 34 },
                                               { 12, 11, 26, 10 },
                                               { 7, 12, 31, 16 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u16_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } } *
                           u16_4 { 6, 2, 1, 3 },
                       u16_4 { 25, 42, 37, 28 });
    check_vector_value(u16_4 { 6, 2, 1, 3 } *
                           u16_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } },
                       u16_4 { 26, 52, 23, 24 });
}

nlc_test("u16_4x4 common functions") {
    check_matrix_value(nlc::transpose(
                           u16_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } }),
                       u16_4x4 { { 2, 6, 1, 0 }, { 4, 3, 0, 4 }, { 3, 4, 8, 1 }, { 1, 2, 3, 5 } });
    nlc_check_msg(nlc::det(u16_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { 1, 0, 2, 1 }, { 0, 2, 1, 1 } }) ==
                      2,
                  "det=",
                  nlc::det(u16_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { 1, 0, 2, 1 }, { 0, 2, 1, 1 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("i16_4x4 operators") {
    constexpr i16_4x4 a { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } };
    constexpr i16_4x4 b { { -5, -1, 0, 2 }, { 0, -4, 7, 1 }, { 2, 1, 2, 0 }, { 2, 0, -3, 1 } };
    constexpr i16_4x4 a_times_b_expected_res { { -4, 15, -21, -17 },
                                               { 31, 8, -73, 8 },
                                               { 0, -11, -6, 10 },
                                               { 1, -12, 29, -12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i16_4x4 { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } } *
                           i16_4 { 6, 2, 1, 3 },
                       i16_4 { 1, -42, 15, -2 });
    check_vector_value(i16_4 { 6, 2, 1, 3 } * i16_4x4 { { 2, -4, 3, 1 },
                                                        { -6, -3, 4, 2 },
                                                        { 1, 0, -8, 3 },
                                                        { 0, -4, -1, -5 } },
                       i16_4 { 10, -32, 7, -24 });
}

nlc_test("i16_4x4 common functions") {
    check_matrix_value(
        nlc::transpose(i16_4x4 { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } }),
        i16_4x4 { { 2, -6, 1, 0 }, { -4, -3, 0, -4 }, { 3, 4, -8, -1 }, { 1, 2, 3, -5 } });
    nlc_check_msg(
        nlc::det(i16_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }) == 2,
        "det=",
        nlc::det(i16_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }));

    check_matrix_value(
        nlc::inverse(i16_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }),
        i16_4x4 { { 1, -1, 0, 0 }, { -1, 1, 0, 0 }, { 0, -1, -1, -1 }, { 2, -3, -1, -2 } });
}

nlc_test("u32_4x4 operators") {
    constexpr u32_4x4 a { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } };
    constexpr u32_4x4 b { { 5, 1, 0, 2 }, { 0, 4, 7, 1 }, { 2, 1, 2, 0 }, { 2, 0, 3, 1 } };
    constexpr u32_4x4 a_times_b_expected_res { { 16, 31, 21, 17 },
                                               { 31, 16, 73, 34 },
                                               { 12, 11, 26, 10 },
                                               { 7, 12, 31, 16 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u32_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } } *
                           u32_4 { 6, 2, 1, 3 },
                       u32_4 { 25, 42, 37, 28 });
    check_vector_value(u32_4 { 6, 2, 1, 3 } *
                           u32_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } },
                       u32_4 { 26, 52, 23, 24 });
}

nlc_test("u32_4x4 common functions") {
    check_matrix_value(nlc::transpose(
                           u32_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } }),
                       u32_4x4 { { 2, 6, 1, 0 }, { 4, 3, 0, 4 }, { 3, 4, 8, 1 }, { 1, 2, 3, 5 } });
    nlc_check_msg(nlc::det(u32_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { 1, 0, 2, 1 }, { 0, 2, 1, 1 } }) ==
                      2,
                  "det=",
                  nlc::det(u32_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { 1, 0, 2, 1 }, { 0, 2, 1, 1 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("i32_4x4 operators") {
    constexpr i32_4x4 a { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } };
    constexpr i32_4x4 b { { -5, -1, 0, 2 }, { 0, -4, 7, 1 }, { 2, 1, 2, 0 }, { 2, 0, -3, 1 } };
    constexpr i32_4x4 a_times_b_expected_res { { -4, 15, -21, -17 },
                                               { 31, 8, -73, 8 },
                                               { 0, -11, -6, 10 },
                                               { 1, -12, 29, -12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i32_4x4 { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } } *
                           i32_4 { 6, 2, 1, 3 },
                       i32_4 { 1, -42, 15, -2 });
    check_vector_value(i32_4 { 6, 2, 1, 3 } * i32_4x4 { { 2, -4, 3, 1 },
                                                        { -6, -3, 4, 2 },
                                                        { 1, 0, -8, 3 },
                                                        { 0, -4, -1, -5 } },
                       i32_4 { 10, -32, 7, -24 });
}

nlc_test("i32_4x4 common functions") {
    check_matrix_value(
        nlc::transpose(i32_4x4 { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } }),
        i32_4x4 { { 2, -6, 1, 0 }, { -4, -3, 0, -4 }, { 3, 4, -8, -1 }, { 1, 2, 3, -5 } });
    nlc_check_msg(
        nlc::det(i32_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }) == 2,
        "det=",
        nlc::det(i32_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }));

    check_matrix_value(
        nlc::inverse(i32_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }),
        i32_4x4 { { 1, -1, 0, 0 }, { -1, 1, 0, 0 }, { 0, -1, -1, -1 }, { 2, -3, -1, -2 } });
}

nlc_test("u64_4x4 operators") {
    constexpr u64_4x4 a { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } };
    constexpr u64_4x4 b { { 5, 1, 0, 2 }, { 0, 4, 7, 1 }, { 2, 1, 2, 0 }, { 2, 0, 3, 1 } };
    constexpr u64_4x4 a_times_b_expected_res { { 16, 31, 21, 17 },
                                               { 31, 16, 73, 34 },
                                               { 12, 11, 26, 10 },
                                               { 7, 12, 31, 16 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u64_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } } *
                           u64_4 { 6, 2, 1, 3 },
                       u64_4 { 25, 42, 37, 28 });
    check_vector_value(u64_4 { 6, 2, 1, 3 } *
                           u64_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } },
                       u64_4 { 26, 52, 23, 24 });
}

nlc_test("u64_4x4 common functions") {
    check_matrix_value(nlc::transpose(
                           u64_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } }),
                       u64_4x4 { { 2, 6, 1, 0 }, { 4, 3, 0, 4 }, { 3, 4, 8, 1 }, { 1, 2, 3, 5 } });
    nlc_check_msg(nlc::det(u64_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { 1, 0, 2, 1 }, { 0, 2, 1, 1 } }) ==
                      2,
                  "det=",
                  nlc::det(u64_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { 1, 0, 2, 1 }, { 0, 2, 1, 1 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("i64_4x4 operators") {
    constexpr i64_4x4 a { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } };
    constexpr i64_4x4 b { { -5, -1, 0, 2 }, { 0, -4, 7, 1 }, { 2, 1, 2, 0 }, { 2, 0, -3, 1 } };
    constexpr i64_4x4 a_times_b_expected_res { { -4, 15, -21, -17 },
                                               { 31, 8, -73, 8 },
                                               { 0, -11, -6, 10 },
                                               { 1, -12, 29, -12 } };
    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i64_4x4 { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } } *
                           i64_4 { 6, 2, 1, 3 },
                       i64_4 { 1, -42, 15, -2 });
    check_vector_value(i64_4 { 6, 2, 1, 3 } * i64_4x4 { { 2, -4, 3, 1 },
                                                        { -6, -3, 4, 2 },
                                                        { 1, 0, -8, 3 },
                                                        { 0, -4, -1, -5 } },
                       i64_4 { 10, -32, 7, -24 });
}

nlc_test("i64_4x4 common functions") {
    check_matrix_value(
        nlc::transpose(i64_4x4 { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } }),
        i64_4x4 { { 2, -6, 1, 0 }, { -4, -3, 0, -4 }, { 3, 4, -8, -1 }, { 1, 2, 3, -5 } });
    nlc_check_msg(
        nlc::det(i64_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }) == 2,
        "det=",
        nlc::det(i64_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }));

    check_matrix_value(
        nlc::inverse(i64_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }),
        i64_4x4 { { 1, -1, 0, 0 }, { -1, 1, 0, 0 }, { 0, -1, -1, -1 }, { 2, -3, -1, -2 } });
}

nlc_test("usize_4x4 operators") {
    constexpr usize_4x4 a { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } };
    constexpr usize_4x4 b { { 5, 1, 0, 2 }, { 0, 4, 7, 1 }, { 2, 1, 2, 0 }, { 2, 0, 3, 1 } };
    constexpr usize_4x4 a_times_b_expected_res { { 16, 31, 21, 17 },
                                                 { 31, 16, 73, 34 },
                                                 { 12, 11, 26, 10 },
                                                 { 7, 12, 31, 16 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(usize_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } } *
                           usize_4 { 6, 2, 1, 3 },
                       usize_4 { 25, 42, 37, 28 });
    check_vector_value(usize_4 { 6, 2, 1, 3 } *
                           usize_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } },
                       usize_4 { 26, 52, 23, 24 });
}

nlc_test("usize_4x4 common functions") {
    check_matrix_value(
        nlc::transpose(usize_4x4 { { 2, 4, 3, 1 }, { 6, 3, 4, 2 }, { 1, 0, 8, 3 }, { 0, 4, 1, 5 } }),
        usize_4x4 { { 2, 6, 1, 0 }, { 4, 3, 0, 4 }, { 3, 4, 8, 1 }, { 1, 2, 3, 5 } });
    nlc_check_msg(
        nlc::det(usize_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { 1, 0, 2, 1 }, { 0, 2, 1, 1 } }) == 2,
        "det=",
        nlc::det(usize_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { 1, 0, 2, 1 }, { 0, 2, 1, 1 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("isize_4x4 operators") {
    constexpr isize_4x4 a { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } };
    constexpr isize_4x4 b { { -5, -1, 0, 2 }, { 0, -4, 7, 1 }, { 2, 1, 2, 0 }, { 2, 0, -3, 1 } };
    constexpr isize_4x4 a_times_b_expected_res { { -4, 15, -21, -17 },
                                                 { 31, 8, -73, 8 },
                                                 { 0, -11, -6, 10 },
                                                 { 1, -12, 29, -12 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(isize_4x4 { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } } *
                           isize_4 { 6, 2, 1, 3 },
                       isize_4 { 1, -42, 15, -2 });
    check_vector_value(isize_4 { 6, 2, 1, 3 } * isize_4x4 { { 2, -4, 3, 1 },
                                                            { -6, -3, 4, 2 },
                                                            { 1, 0, -8, 3 },
                                                            { 0, -4, -1, -5 } },
                       isize_4 { 10, -32, 7, -24 });
}

nlc_test("isize_4x4 common functions") {
    check_matrix_value(
        nlc::transpose(
            isize_4x4 { { 2, -4, 3, 1 }, { -6, -3, 4, 2 }, { 1, 0, -8, 3 }, { 0, -4, -1, -5 } }),
        isize_4x4 { { 2, -6, 1, 0 }, { -4, -3, 0, -4 }, { 3, 4, -8, -1 }, { 1, 2, 3, -5 } });
    nlc_check_msg(
        nlc::det(isize_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }) == 2,
        "det=",
        nlc::det(isize_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }));

    check_matrix_value(
        nlc::inverse(isize_4x4 { { 2, 2, 0, 0 }, { 2, 3, 0, 0 }, { -1, 0, -2, 1 }, { 0, -2, 1, -1 } }),
        isize_4x4 { { 1, -1, 0, 0 }, { -1, 1, 0, 0 }, { 0, -1, -1, -1 }, { 2, -3, -1, -2 } });
}

nlc_test("f32_4x4 operators") {
    constexpr f32_4x4 a { { 2.f, -4.f, 3.f, 1.f },
                          { -6.f, -3.f, 4.f, 2.f },
                          { 1.f, 0.f, -8.f, 3.f },
                          { 0.f, -4.f, -1.f, -5.f } };
    constexpr f32_4x4 b { { -5.f, -1.f, 0.f, 2.f },
                          { 0.f, -4.f, 7.f, 1.f },
                          { 2.f, 1.f, 2.f, 0.f },
                          { 2.f, 0.f, -3.f, 1.f } };
    constexpr f32_4x4 a_times_b_expected_res { { -4.f, 15.f, -21.f, -17.f },
                                               { 31.f, 8.f, -73.f, 8.f },
                                               { 0.f, -11.f, -6.f, 10.f },
                                               { 1.f, -12.f, 29.f, -12.f } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(f32_4x4 { { 2.f, -4.f, 3.f, 1.f },
                                 { -6.f, -3.f, 4.f, 2.f },
                                 { 1.f, 0.f, -8.f, 3.f },
                                 { 0.f, -4.f, -1.f, -5.f } } *
                           f32_4 { 6.f, 2.f, 1.f, 3.f },
                       f32_4 { 1.f, -42.f, 15.f, -2.f });
    check_vector_value(f32_4 { 6.f, 2.f, 1.f, 3.f } * f32_4x4 { { 2.f, -4.f, 3.f, 1.f },
                                                                { -6.f, -3.f, 4.f, 2.f },
                                                                { 1.f, 0.f, -8.f, 3.f },
                                                                { 0.f, -4.f, -1.f, -5.f } },
                       f32_4 { 10.f, -32.f, 7.f, -24.f });
}

nlc_test("f32_4x4 common functions") {
    check_matrix_value(nlc::transpose(f32_4x4 { { 2.f, -4.f, 3.f, 1.f },
                                                { -6.f, -3.f, 4.f, 2.f },
                                                { 1.f, 0.f, -8.f, 3.f },
                                                { 0.f, -4.f, -1.f, -5.f } }),
                       f32_4x4 { { 2.f, -6.f, 1.f, 0.f },
                                 { -4.f, -3.f, 0.f, -4.f },
                                 { 3.f, 4.f, -8.f, -1.f },
                                 { 1.f, 2.f, 3.f, -5.f } });
    nlc_check_msg(nlc::det(f32_4x4 { { 2.f, 2.f, 0.f, 0.f },
                                     { 2.f, 3.f, 0.f, 0.f },
                                     { -1.f, 0.f, -2.f, 1.f },
                                     { 0.f, -2.f, 1.f, -1.f } }) == 2.f,
                  "det=",
                  nlc::det(f32_4x4 { { 2.f, 2.f, 0.f, 0.f },
                                     { 2.f, 3.f, 0.f, 0.f },
                                     { -1.f, 0.f, -2.f, 1.f },
                                     { 0.f, -2.f, 1.f, -1.f } }));

    check_matrix_value(nlc::inverse(f32_4x4 { { 2.f, 2.f, 0.f, 0.f },
                                              { 2.f, 3.f, 0.f, 0.f },
                                              { -1.f, 0.f, -2.f, 1.f },
                                              { 0.f, -2.f, 1.f, -1.f } }),
                       f32_4x4 { { 1.5f, -1.f, 0.f, 0.f },
                                 { -1.f, 1.f, 0.f, 0.f },
                                 { 0.5f, -1.f, -1.f, -1.f },
                                 { 2.5f, -3.f, -1.f, -2.f } });
}

nlc_test("f64_4x4 operators") {
    constexpr f64_4x4 a { { 2., -4., 3., 1. },
                          { -6., -3., 4., 2. },
                          { 1., 0., -8., 3. },
                          { 0., -4., -1., -5. } };
    constexpr f64_4x4 b { { -5., -1., 0., 2. },
                          { 0., -4., 7., 1. },
                          { 2., 1., 2., 0. },
                          { 2., 0., -3., 1. } };
    constexpr f64_4x4 a_times_b_expected_res { { -4., 15., -21., -17. },
                                               { 31., 8., -73., 8. },
                                               { 0., -11., -6., 10. },
                                               { 1., -12., 29., -12. } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(f64_4x4 { { 2., -4., 3., 1. },
                                 { -6., -3., 4., 2. },
                                 { 1., 0., -8., 3. },
                                 { 0., -4., -1., -5. } } *
                           f64_4 { 6., 2., 1., 3. },
                       f64_4 { 1., -42., 15., -2. });
    check_vector_value(f64_4 { 6., 2., 1., 3. } * f64_4x4 { { 2., -4., 3., 1. },
                                                            { -6., -3., 4., 2. },
                                                            { 1., 0., -8., 3. },
                                                            { 0., -4., -1., -5. } },
                       f64_4 { 10., -32., 7., -24. });
}

nlc_test("f64_4x4 common functions") {
    check_matrix_value(nlc::transpose(f64_4x4 { { 2., -4., 3., 1. },
                                                { -6., -3., 4., 2. },
                                                { 1., 0., -8., 3. },
                                                { 0., -4., -1., -5. } }),
                       f64_4x4 { { 2., -6., 1., 0. },
                                 { -4., -3., 0., -4. },
                                 { 3., 4., -8., -1. },
                                 { 1., 2., 3., -5. } });
    nlc_check_msg(nlc::det(f64_4x4 { { 2., 2., 0., 0. },
                                     { 2., 3., 0., 0. },
                                     { -1., 0., -2., 1. },
                                     { 0., -2., 1., -1. } }) == 2.,
                  "det=",
                  nlc::det(f64_4x4 { { 2., 2., 0., 0. },
                                     { 2., 3., 0., 0. },
                                     { -1., 0., -2., 1. },
                                     { 0., -2., 1., -1. } }));

    check_matrix_value(nlc::inverse(f64_4x4 { { 2., 2., 0., 0. },
                                              { 2., 3., 0., 0. },
                                              { -1., 0., -2., 1. },
                                              { 0., -2., 1., -1. } }),
                       f64_4x4 { { 1.5, -1., 0., 0. },
                                 { -1., 1., 0., 0. },
                                 { 0.5, -1., -1., -1. },
                                 { 2.5, -3., -1., -2. } });
}
