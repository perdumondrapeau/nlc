/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/maths/float_cmp.hpp>
#include <nlc/maths/mat3x3.hpp>
#include <nlc/maths/mat3x3_common.hpp>
#include <nlc/maths/vec3.hpp>
#include <nlc/test.hpp>

#include "test_mat_common.hpp"

using namespace nlc;

nlc_test("mat3x3 identity") {
    check_identity<u8, 3>();
    check_identity<i8, 3>();
    check_identity<u16, 3>();
    check_identity<i16, 3>();
    check_identity<u32, 3>();
    check_identity<i32, 3>();
    check_identity<u64, 3>();
    check_identity<i64, 3>();
    check_identity<usize, 3>();
    check_identity<isize, 3>();
    check_identity<f32, 3>();
    check_identity<f64, 3>();
}

nlc_test("mat3x3 accessors") {
    check_accessors<u8, 3>();
    check_accessors<i8, 3>();
    check_accessors<u16, 3>();
    check_accessors<i16, 3>();
    check_accessors<u32, 3>();
    check_accessors<i32, 3>();
    check_accessors<u64, 3>();
    check_accessors<i64, 3>();
    check_accessors<usize, 3>();
    check_accessors<isize, 3>();
    check_accessors<f32, 3>();
    check_accessors<f64, 3>();
}

nlc_test("u8_3x3 operators") {
    constexpr u8_3x3 a { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } };
    constexpr u8_3x3 b { { 5, 1, 0 }, { 0, 4, 7 }, { 2, 1, 2 } };
    constexpr u8_3x3 a_times_b_expected_res { { 16, 23, 19 }, { 31, 12, 72 }, { 12, 11, 26 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u8_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } } * u8_3 { 6, 2, 1 },
                       u8_3 { 25, 30, 34 });
    check_vector_value(u8_3 { 6, 2, 1 } * u8_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } },
                       u8_3 { 23, 46, 14 });
}

nlc_test("u8_3x3 common functions") {
    check_matrix_value(nlc::transpose(u8_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } }),
                       u8_3x3 { { 2, 6, 1 }, { 4, 3, 0 }, { 3, 4, 8 } });
    nlc_check_msg(nlc::det(u8_3x3 { { 6, 4, 3 }, { 2, 3, 4 }, { 1, 0, 8 } }) == 87,
                  "det=",
                  nlc::det(u8_3x3 { { 6, 4, 3 }, { 2, 3, 4 }, { 1, 0, 8 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("i8_3x3 operators") {
    constexpr i8_3x3 a { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } };
    constexpr i8_3x3 b { { 5, -1, 0 }, { 0, 4, 7 }, { -2, -1, -2 } };
    constexpr i8_3x3 a_times_b_expected_res { { 16, -17, 11 }, { -31, -12, 72 }, { 4, 11, -26 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i8_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } } * i8_3 { 6, -2, 1 },
                       i8_3 { 23, -18, 18 });
    check_vector_value(i8_3 { -6, -2, 1 } * i8_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } },
                       i8_3 { -1, 46, 14 });
}

nlc_test("i8_3x3 common functions") {
    check_matrix_value(nlc::transpose(i8_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } }),
                       i8_3x3 { { 2, -6, -1 }, { -4, -3, 0 }, { 3, 4, 8 } });
    nlc_check_msg(nlc::det(i8_3x3 { { 1, -4, -3 }, { -2, 3, -4 }, { 1, 0, -8 } }) == 65,
                  "det=",
                  nlc::det(i8_3x3 { { 1, -4, -3 }, { -2, 3, -4 }, { 1, 0, -8 } }));
    nlc_check_msg(nlc::det(i8_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }) == 2,
                  "det=",
                  nlc::det(i8_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }));
    check_matrix_value(nlc::inverse(i8_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }),
                       i8_3x3 { { -3, 1, 6 }, { 5, -2, -12 }, { 5, -2, -11 } });
}

nlc_test("u16_3x3 operators") {
    constexpr u16_3x3 a { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } };
    constexpr u16_3x3 b { { 5, 1, 0 }, { 0, 4, 7 }, { 2, 1, 2 } };
    constexpr u16_3x3 a_times_b_expected_res { { 16, 23, 19 }, { 31, 12, 72 }, { 12, 11, 26 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u16_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } } * u16_3 { 6, 2, 1 },
                       u16_3 { 25, 30, 34 });
    check_vector_value(u16_3 { 6, 2, 1 } * u16_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } },
                       u16_3 { 23, 46, 14 });
}

nlc_test("u16_3x3 common functions") {
    check_matrix_value(nlc::transpose(u16_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } }),
                       u16_3x3 { { 2, 6, 1 }, { 4, 3, 0 }, { 3, 4, 8 } });
    nlc_check_msg(nlc::det(u16_3x3 { { 6, 4, 3 }, { 2, 3, 4 }, { 1, 0, 8 } }) == 87,
                  "det=",
                  nlc::det(u16_3x3 { { 6, 4, 3 }, { 2, 3, 4 }, { 1, 0, 8 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("i16_3x3 operators") {
    constexpr i16_3x3 a { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } };
    constexpr i16_3x3 b { { 5, -1, 0 }, { 0, 4, 7 }, { -2, -1, -2 } };
    constexpr i16_3x3 a_times_b_expected_res { { 16, -17, 11 }, { -31, -12, 72 }, { 4, 11, -26 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i16_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } } * i16_3 { 6, -2, 1 },
                       i16_3 { 23, -18, 18 });
    check_vector_value(i16_3 { -6, -2, 1 } * i16_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } },
                       i16_3 { -1, 46, 14 });
}

nlc_test("i16_3x3 common functions") {
    check_matrix_value(nlc::transpose(i16_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } }),
                       i16_3x3 { { 2, -6, -1 }, { -4, -3, 0 }, { 3, 4, 8 } });
    nlc_check_msg(nlc::det(i16_3x3 { { 1, -4, -3 }, { -2, 3, -4 }, { 1, 0, -8 } }) == 65,
                  "det=",
                  nlc::det(i16_3x3 { { 1, -4, -3 }, { -2, 3, -4 }, { 1, 0, -8 } }));
    nlc_check_msg(nlc::det(i16_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }) == 2,
                  "det=",
                  nlc::det(i16_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }));
    check_matrix_value(nlc::inverse(i16_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }),
                       i16_3x3 { { -3, 1, 6 }, { 5, -2, -12 }, { 5, -2, -11 } });
}

nlc_test("u32_3x3 operators") {
    constexpr u32_3x3 a { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } };
    constexpr u32_3x3 b { { 5, 1, 0 }, { 0, 4, 7 }, { 2, 1, 2 } };
    constexpr u32_3x3 a_times_b_expected_res { { 16, 23, 19 }, { 31, 12, 72 }, { 12, 11, 26 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u32_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } } * u32_3 { 6, 2, 1 },
                       u32_3 { 25, 30, 34 });
    check_vector_value(u32_3 { 6, 2, 1 } * u32_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } },
                       u32_3 { 23, 46, 14 });
}

nlc_test("u32_3x3 common functions") {
    check_matrix_value(nlc::transpose(u32_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } }),
                       u32_3x3 { { 2, 6, 1 }, { 4, 3, 0 }, { 3, 4, 8 } });
    nlc_check_msg(nlc::det(u32_3x3 { { 6, 4, 3 }, { 2, 3, 4 }, { 1, 0, 8 } }) == 87,
                  "det=",
                  nlc::det(u32_3x3 { { 6, 4, 3 }, { 2, 3, 4 }, { 1, 0, 8 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("i32_3x3 operators") {
    constexpr i32_3x3 a { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } };
    constexpr i32_3x3 b { { 5, -1, 0 }, { 0, 4, 7 }, { -2, -1, -2 } };
    constexpr i32_3x3 a_times_b_expected_res { { 16, -17, 11 }, { -31, -12, 72 }, { 4, 11, -26 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i32_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } } * i32_3 { 6, -2, 1 },
                       i32_3 { 23, -18, 18 });
    check_vector_value(i32_3 { -6, -2, 1 } * i32_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } },
                       i32_3 { -1, 46, 14 });
}

nlc_test("i32_3x3 common functions") {
    check_matrix_value(nlc::transpose(i32_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } }),
                       i32_3x3 { { 2, -6, -1 }, { -4, -3, 0 }, { 3, 4, 8 } });
    nlc_check_msg(nlc::det(i32_3x3 { { 1, -4, -3 }, { -2, 3, -4 }, { 1, 0, -8 } }) == 65,
                  "det=",
                  nlc::det(i32_3x3 { { 1, -4, -3 }, { -2, 3, -4 }, { 1, 0, -8 } }));
    nlc_check_msg(nlc::det(i32_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }) == 2,
                  "det=",
                  nlc::det(i32_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }));
    check_matrix_value(nlc::inverse(i32_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }),
                       i32_3x3 { { -3, 1, 6 }, { 5, -2, -12 }, { 5, -2, -11 } });
}

nlc_test("u64_3x3 operators") {
    constexpr u64_3x3 a { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } };
    constexpr u64_3x3 b { { 5, 1, 0 }, { 0, 4, 7 }, { 2, 1, 2 } };
    constexpr u64_3x3 a_times_b_expected_res { { 16, 23, 19 }, { 31, 12, 72 }, { 12, 11, 26 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(u64_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } } * u64_3 { 6, 2, 1 },
                       u64_3 { 25, 30, 34 });
    check_vector_value(u64_3 { 6, 2, 1 } * u64_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } },
                       u64_3 { 23, 46, 14 });
}

nlc_test("u64_3x3 common functions") {
    check_matrix_value(nlc::transpose(u64_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } }),
                       u64_3x3 { { 2, 6, 1 }, { 4, 3, 0 }, { 3, 4, 8 } });
    nlc_check_msg(nlc::det(u64_3x3 { { 6, 4, 3 }, { 2, 3, 4 }, { 1, 0, 8 } }) == 87,
                  "det=",
                  nlc::det(u64_3x3 { { 6, 4, 3 }, { 2, 3, 4 }, { 1, 0, 8 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("i64_3x3 operators") {
    constexpr i64_3x3 a { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } };
    constexpr i64_3x3 b { { 5, -1, 0 }, { 0, 4, 7 }, { -2, -1, -2 } };
    constexpr i64_3x3 a_times_b_expected_res { { 16, -17, 11 }, { -31, -12, 72 }, { 4, 11, -26 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(i64_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } } * i64_3 { 6, -2, 1 },
                       i64_3 { 23, -18, 18 });
    check_vector_value(i64_3 { -6, -2, 1 } * i64_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } },
                       i64_3 { -1, 46, 14 });
}

nlc_test("i64_3x3 common functions") {
    check_matrix_value(nlc::transpose(i64_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } }),
                       i64_3x3 { { 2, -6, -1 }, { -4, -3, 0 }, { 3, 4, 8 } });
    nlc_check_msg(nlc::det(i64_3x3 { { 1, -4, -3 }, { -2, 3, -4 }, { 1, 0, -8 } }) == 65,
                  "det=",
                  nlc::det(i64_3x3 { { 1, -4, -3 }, { -2, 3, -4 }, { 1, 0, -8 } }));
    nlc_check_msg(nlc::det(i64_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }) == 2,
                  "det=",
                  nlc::det(i64_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }));
    check_matrix_value(nlc::inverse(i64_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }),
                       i64_3x3 { { -3, 1, 6 }, { 5, -2, -12 }, { 5, -2, -11 } });
}

nlc_test("usize_3x3 operators") {
    constexpr usize_3x3 a { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } };
    constexpr usize_3x3 b { { 5, 1, 0 }, { 0, 4, 7 }, { 2, 1, 2 } };
    constexpr usize_3x3 a_times_b_expected_res { { 16, 23, 19 }, { 31, 12, 72 }, { 12, 11, 26 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(usize_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } } * usize_3 { 6, 2, 1 },
                       usize_3 { 25, 30, 34 });
    check_vector_value(usize_3 { 6, 2, 1 } * usize_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } },
                       usize_3 { 23, 46, 14 });
}

nlc_test("usize_3x3 common functions") {
    check_matrix_value(nlc::transpose(usize_3x3 { { 2, 4, 3 }, { 6, 3, 4 }, { 1, 0, 8 } }),
                       usize_3x3 { { 2, 6, 1 }, { 4, 3, 0 }, { 3, 4, 8 } });
    nlc_check_msg(nlc::det(usize_3x3 { { 6, 4, 3 }, { 2, 3, 4 }, { 1, 0, 8 } }) == 87,
                  "det=",
                  nlc::det(usize_3x3 { { 6, 4, 3 }, { 2, 3, 4 }, { 1, 0, 8 } }));
    // No inverse for unsigned because of sign inversion
}

nlc_test("isize_3x3 operators") {
    constexpr isize_3x3 a { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } };
    constexpr isize_3x3 b { { 5, -1, 0 }, { 0, 4, 7 }, { -2, -1, -2 } };
    constexpr isize_3x3 a_times_b_expected_res { { 16, -17, 11 }, { -31, -12, 72 }, { 4, 11, -26 } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(isize_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } } * isize_3 { 6, -2, 1 },
                       isize_3 { 23, -18, 18 });
    check_vector_value(isize_3 { -6, -2, 1 } * isize_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } },
                       isize_3 { -1, 46, 14 });
}

nlc_test("isize_3x3 common functions") {
    check_matrix_value(nlc::transpose(isize_3x3 { { 2, -4, 3 }, { -6, -3, 4 }, { -1, 0, 8 } }),
                       isize_3x3 { { 2, -6, -1 }, { -4, -3, 0 }, { 3, 4, 8 } });
    nlc_check_msg(nlc::det(isize_3x3 { { 1, -4, -3 }, { -2, 3, -4 }, { 1, 0, -8 } }) == 65,
                  "det=",
                  nlc::det(isize_3x3 { { 1, -4, -3 }, { -2, 3, -4 }, { 1, 0, -8 } }));
    nlc_check_msg(nlc::det(isize_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }) == 2,
                  "det=",
                  nlc::det(isize_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }));
    check_matrix_value(nlc::inverse(isize_3x3 { { -4, -2, 0 }, { -11, 0, -6 }, { 0, -1, 1 } }),
                       isize_3x3 { { -3, 1, 6 }, { 5, -2, -12 }, { 5, -2, -11 } });
}

nlc_test("f32_3x3 operators") {
    constexpr f32_3x3 a { { 2.f, -4.f, 3.f }, { -6.f, -3.f, 4.f }, { -1.f, 0.f, 8.f } };
    constexpr f32_3x3 b { { 5.f, -1.f, 0.f }, { 0.f, 4.f, 7.f }, { -2.f, -1.f, -2.f } };
    constexpr f32_3x3 a_times_b_expected_res { { 16.f, -17.f, 11.f },
                                               { -31.f, -12.f, 72.f },
                                               { 4.f, 11.f, -26.f } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(f32_3x3 { { 2.f, -4.f, 3.f }, { -6.f, -3.f, 4.f }, { -1.f, 0.f, 8.f } } *
                           f32_3 { 6.f, -2.f, 1.f },
                       f32_3 { 23.f, -18.f, 18.f });
    check_vector_value(f32_3 { -6.f, -2.f, 1.f } *
                           f32_3x3 { { 2.f, -4.f, 3.f }, { -6.f, -3.f, 4.f }, { -1.f, 0.f, 8.f } },
                       f32_3 { -1.f, 46.f, 14.f });
}

nlc_test("f32_3x3 common functions") {
    check_matrix_value(nlc::transpose(
                           f32_3x3 { { 2.f, -4.f, 3.f }, { -6.f, -3.f, 4.f }, { -1.f, 0.f, 8.f } }),
                       f32_3x3 { { 2.f, -6.f, -1.f }, { -4.f, -3.f, 0.f }, { 3.f, 4.f, 8.f } });
    nlc_check_msg(nlc::det(f32_3x3 { { 1.f, -4.f, -3.f }, { -2.f, 3.f, -4.f }, { 1.f, 0.f, -8.f } }) ==
                      65.f,
                  "det=",
                  nlc::det(f32_3x3 { { 1.f, -4.f, -3.f }, { -2.f, 3.f, -4.f }, { 1.f, 0.f, -8.f } }));
    nlc_check_msg(nlc::det(f32_3x3 { { -4.f, -2.f, 0.f }, { -11.f, 0.f, -6.f }, { 0.f, -1.f, 1.f } }) ==
                      2.f,
                  "det=",
                  nlc::det(f32_3x3 { { -4.f, -2.f, 0.f }, { -11.f, 0.f, -6.f }, { 0.f, -1.f, 1.f } }));
    check_matrix_value(nlc::inverse(
                           f32_3x3 { { -4.f, -2.f, 0.f }, { -11.f, 0.f, -6.f }, { 0.f, -1.f, 1.f } }),
                       f32_3x3 { { -3.f, 1.f, 6.f }, { 5.5f, -2.f, -12.f }, { 5.5f, -2.f, -11.f } });
}

nlc_test("f64_3x3 operators") {
    constexpr f64_3x3 a { { 2., -4., 3. }, { -6., -3., 4. }, { -1., 0., 8. } };
    constexpr f64_3x3 b { { 5., -1., 0. }, { 0., 4., 7. }, { -2., -1., -2. } };
    constexpr f64_3x3 a_times_b_expected_res { { 16., -17., 11. },
                                               { -31., -12., 72. },
                                               { 4., 11., -26. } };

    check_matrix_value(a * b, a_times_b_expected_res);
    auto d = a;
    d *= b;
    check_matrix_value(d, a_times_b_expected_res);

    check_vector_value(f64_3x3 { { 2., -4., 3. }, { -6., -3., 4. }, { -1., 0., 8. } } *
                           f64_3 { 6., -2., 1. },
                       f64_3 { 23., -18., 18. });
    check_vector_value(f64_3 { -6., -2., 1. } *
                           f64_3x3 { { 2., -4., 3. }, { -6., -3., 4. }, { -1., 0., 8. } },
                       f64_3 { -1., 46., 14. });
}

nlc_test("f64_3x3 common functions") {
    check_matrix_value(nlc::transpose(f64_3x3 { { 2., -4., 3. }, { -6., -3., 4. }, { -1., 0., 8. } }),
                       f64_3x3 { { 2., -6., -1. }, { -4., -3., 0. }, { 3., 4., 8. } });
    nlc_check_msg(nlc::det(f64_3x3 { { 1., -4., -3. }, { -2., 3., -4. }, { 1., 0., -8. } }) == 65.,
                  "det=",
                  nlc::det(f64_3x3 { { 1., -4., -3. }, { -2., 3., -4. }, { 1., 0., -8. } }));
    nlc_check_msg(nlc::det(f64_3x3 { { -4., -2., 0. }, { -11., 0., -6. }, { 0., -1., 1. } }) == 2.,
                  "det=",
                  nlc::det(f64_3x3 { { -4., -2., 0. }, { -11., 0., -6. }, { 0., -1., 1. } }));
    check_matrix_value(nlc::inverse(f64_3x3 { { -4., -2., 0. }, { -11., 0., -6. }, { 0., -1., 1. } }),
                       f64_3x3 { { -3., 1., 6. }, { 5.5, -2., -12. }, { 5.5, -2., -11. } });
}
