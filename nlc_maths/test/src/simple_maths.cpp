/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/maths/float_cmp.hpp>
#include <nlc/maths/simple_maths.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test("min") {
    static_assert(min(5u, 1215u) == 5u);
    static_assert(min(1215u, 5u) == 5u);
    static_assert(min(-56, 235) == -56);
    static_assert(min(-13.37f, -42.f) == -42.f);
    static_assert(min(-13.37f, -42.f, 583.25f, -897.89f) == -897.89f);
}

nlc_test("max") {
    static_assert(max(5u, 1215u) == 1215u);
    static_assert(max(1215u, 5u) == 1215u);
    static_assert(max(-56, 235) == 235);
    static_assert(max(-13.37f, -42.f) == -13.37f);
    static_assert(max(-13.37f, -42.f, 583.25f, -897.89f) == 583.25f);
}

nlc_test("clamp") {
    nlc_check(clamp(5u, 9u, 1215u) == 9u);
    nlc_check(clamp(1215u, 5u, 1216u) == 1215u);
    nlc_check(clamp(-56, 235, 235) == 235);
    nlc_check(clamp(-13.37f, -897.89f, -42.f) == -42.0f);
    nlc_check(clamp(-13.37f, -42.f, 583.25f) == -13.37f);
}

nlc_test("lerp") {
    static_assert(lerp(5u, 1215u, 0u) == 5u);
    static_assert(lerp(1215u, 5u, 0u) == 1215u);
    static_assert(lerp(-56, 235, 1) == 235);
    static_assert(lerp(0.0f, -42.f, 0.5f) == -21.0f);
    static_assert(lerp(-13.37f, 583.25f, 1.0f) == 583.25f);
    static_assert(lerp(-10.0f, 10.0f, 45.5f) == 900.0f);
    static_assert(lerp(-10.0f, 10.0f, -5.5f) == -120.0f);
}

nlc_test("abs") {
    nlc_check(nlc::abs(-5) == 5);
    nlc_check(nlc::abs(45) == 45);
    nlc_check(nlc::abs(-16.4f) == 16.4f);
    nlc_check(nlc::abs(-13.) == 13.);
}

nlc_test("sign") {
    nlc_check(sign(0) == 0);
    nlc_check(sign(-4) == -1);
    nlc_check(sign(65) == 1);
    nlc_check(sign(0.f) == 1.f);
    nlc_check(sign(-5.4f) == -1.f);
    nlc_check(sign(65.54f) == 1.f);
    nlc_check(sign(0.) == 1.);
    nlc_check(sign(-4.254) == -1.0);
    nlc_check(sign(6.5) == 1.0);
}

nlc_test("copysign") {
    nlc_check(copysign(0, -2) == 0);
    nlc_check(copysign(-4, 1) == 4);
    nlc_check(copysign(65, -1) == -65);
    nlc_check(copysign(0.f, -0.f) == -0.f);
    nlc_check(copysign(-5.4f, -0.4f) == -5.4f);
    nlc_check(copysign(65.54f, -123213.f) == -65.54f);
    nlc_check(copysign(0., 0.) == 0.);
    nlc_check(copysign(-4.254, 2.) == 4.254);
    nlc_check(copysign(6.5, -23.) == -6.5);
}

nlc_test("is_finite") {
    static_assert(is_finite(42.f));
    static_assert(is_finite(42));
    static_assert(is_finite(42u));
    static_assert(is_finite("toto") == false);
    static_assert(is_finite(nlc::meta::limits<double>::infinity) == false);
    static_assert(is_finite(57.f * nlc::meta::limits<float>::infinity) == false);
}

nlc_test("floor") {
    nlc_check(floor(12.3f) == 12.f);
    nlc_check(floor(2.9999f) == 2.f);
    nlc_check(floor(-2.9f) == -3.f);
    nlc_check(floor(0.f) == 0.f);
    nlc_check(floor(-0.f) == 0.f);
}

nlc_test("ceil") {
    nlc_check(ceil(12.3f) == 13.f);
    nlc_check(ceil(2.0005f) == 3.f);
    nlc_check(ceil(-2.9f) == -2.f);
    nlc_check(ceil(0.f) == 0.f);
    nlc_check(ceil(-0.f) == 0.f);
}

nlc_test("trunc") {
    nlc_check(trunc(12.3f) == 12.f);
    nlc_check(trunc(2.0005f) == 2.f);
    nlc_check(trunc(2.9999f) == 2.f);
    nlc_check(trunc(-2.9f) == -2.f);
    nlc_check(trunc(0.f) == 0.f);
    nlc_check(trunc(-0.f) == 0.f);
}

nlc_test("round") {
    nlc_check(round(12.5f) == 13.f);
    nlc_check(round(12.4999f) == 12.f);
    nlc_check(round(2.0005f) == 2.f);
    nlc_check(round(2.9999f) == 3.f);
    nlc_check(round(-2.9f) == -3.f);
    nlc_check(round(0.f) == 0.f);
    nlc_check(round(-0.f) == 0.f);
}

nlc_test("mod") {
    constexpr auto relative_epsilon { 1E-5f };

    nlc_check(are_floats_close_relative_value_wise(mod(5.f, 3.f), 2.f, relative_epsilon));
    nlc_check(are_floats_close_relative_value_wise(mod(0.f, 4.f), 0.f, relative_epsilon));
    nlc_check(are_floats_close_relative_value_wise(mod(5.f, 2.5f), 0.f, relative_epsilon));
    nlc_check(are_floats_close_relative_value_wise(mod(3.7f, 1.2f), 0.1f, relative_epsilon));
    nlc_check(are_floats_close_relative_value_wise(mod(-2.5f, 0.6f), -0.1f, relative_epsilon));
    nlc_check(are_floats_close_relative_value_wise(mod(-2.5f, -0.6f), -0.1f, relative_epsilon));
    nlc_check(are_floats_close_relative_value_wise(mod(2.5f, -0.6f), 0.1f, relative_epsilon));
    nlc_check(are_floats_close_relative_value_wise(mod(-1.2f, 0.5f), -0.2f, relative_epsilon));
    nlc_check(are_floats_close_relative_value_wise(mod(1.2f, -0.5f), 0.2f, relative_epsilon));
}
