/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/basic_output.hpp>
#include <nlc/fundamentals/random.hpp>
#include <nlc/fundamentals/time.hpp>
#include <nlc/maths/functions.hpp>
#include <nlc/maths/trigonometry.hpp>

#include <math.h>

template<class T> inline auto do_not_optimize(T const & datum) {
    return reinterpret_cast<char const volatile &>(datum);
}

constexpr auto count = 1 << 10;

inline f32 values[count];

inline f32 buf[count];

#define bench(f, cycles)                                                           \
    do {                                                                           \
        nlc::print("Benchmarking " #f "...");                                      \
        auto const start = nlc::time::now();                                       \
        for (int i = 0; i < cycles; i++) {                                         \
            for (int j = 0; j < count; j++)                                        \
                buf[j] = f(values[j]);                                             \
            do_not_optimize(buf);                                                  \
        }                                                                          \
        auto const end = nlc::time::now();                                         \
        auto const dt = nlc::time::nanoseconds<f64>(end - start);                  \
        auto const throughput = cycles * count / dt;                               \
        nlc::print("    ", throughput * 1e3, "M/s (", dt / 1e6, "ms of runtime)"); \
    } while (false)

#define bench_xy(f, cycles)                                                        \
    do {                                                                           \
        nlc::print("Benchmarking " #f "...");                                      \
        auto const start = nlc::time::now();                                       \
        for (int i = 0; i < cycles; i++) {                                         \
            for (int j = 0; j < count; j++)                                        \
                buf[j] = f(values[j], values[count - j - 1]);                      \
            do_not_optimize(buf);                                                  \
        }                                                                          \
        auto const end = nlc::time::now();                                         \
        auto const dt = nlc::time::nanoseconds<f64>(end - start);                  \
        auto const throughput = cycles * count / dt;                               \
        nlc::print("    ", throughput * 1e3, "M/s (", dt / 1e6, "ms of runtime)"); \
    } while (false)

static auto fill_values(f32 min, f32 max) -> void {
    nlc::xoroshiro128plus rng { 0 };

    for (int i = 0; i < count; i++)
        values[i] = rng.in_range(min, max);
}

int main() {
    nlc::print("sin in [-π;π]");
    nlc::print("-------------");
    fill_values(-nlc::pi<f32>, nlc::pi<f32>);
    bench(sinf, 100000);
    bench(nlc::reduced_range::sin, 1000000);

    nlc::print("\nsin in [-100;100]");
    nlc::print("-----------------");
    fill_values(-100.f, 100.f);
    bench(sinf, 100000);
    bench(nlc::sin, 500000);

    nlc::print("\ncos in [-π;π]");
    nlc::print("-------------");
    fill_values(-nlc::pi<f32>, nlc::pi<f32>);
    bench(cosf, 100000);
    bench(nlc::reduced_range::cos, 1000000);

    nlc::print("\ncos in [-100;100]");
    nlc::print("-----------------");
    fill_values(-100.f, 100.f);
    bench(cosf, 100000);
    bench(nlc::cos, 500000);

    nlc::print("\ntan in [-π/4;π/4]");
    nlc::print("-------------");
    fill_values(-nlc::quarter_pi<f32>, nlc::quarter_pi<f32>);
    bench(tanf, 100000);
    bench(nlc::reduced_range::tan, 1000000);

    nlc::print("\ntan in [-100;100]");
    nlc::print("-----------------");
    fill_values(-100.f, 100.f);
    bench(tanf, 50000);
    bench(nlc::tan, 100000);

    nlc::print("\natan in [-1;1]");
    nlc::print("-------------");
    fill_values(-1.f, 1.f);
    bench(atanf, 100000);
    bench(nlc::reduced_range::atan, 1000000);

    nlc::print("\natan in [-100;100]");
    nlc::print("-----------------");
    fill_values(-100.f, 100.f);
    bench(atanf, 50000);
    bench(nlc::atan, 100000);

    nlc::print("\natan2 in [-100;100]");
    nlc::print("-----------------");
    fill_values(-100.f, 100.f);
    bench_xy(atan2f, 50000);
    bench_xy(nlc::atan2, 100000);

    nlc::print("\nlog functions");
    nlc::print("-----------------");
    fill_values(1e-10f, 100.f);
    bench(logf, 100000);
    bench(nlc::log, 1000000);

    nlc::print("\nexp functions");
    nlc::print("-----------------");
    fill_values(-100.f, 100.f);
    bench(expf, 100000);
    bench(nlc::exp, 1000000);

    nlc::print("\nsqrt");
    nlc::print("-----------------");
    fill_values(1e-10f, 100.f);
    bench(sqrtf, 100000);
#if defined(__GNUC__) || defined(__clang__)
    bench(__builtin_sqrtf, 100000);
#endif
    bench(nlc::sqrt, 100000);

    nlc::print("\nrsqrt");
    nlc::print("-----------------");
    fill_values(1e-10f, 100.f);
    bench(nlc::rsqrt, 1000000);

    return 0;
}
