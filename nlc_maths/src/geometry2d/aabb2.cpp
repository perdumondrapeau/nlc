/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <geometry2d/aabb2.hpp>

#include <simple_maths.hpp>

namespace nlc {

auto aabb2::from_points(span<f32_2 const> const points) -> aabb2 {
    if (points.is_empty())
        return {};
    aabb2 aabb { points.front(), points.front() };
    for (usize i = 1; i < points.size(); ++i)
        aabb = { ::nlc::min(aabb.min, points[i]), ::nlc::max(aabb.max, points[i]) };
    return aabb;
}

auto aabb2::merge(span<aabb2 const> const aabbs) -> aabb2 {
    if (aabbs.is_empty())
        return {};
    aabb2 aabb { aabbs.front() };
    for (usize i = 1; i < aabbs.size(); ++i)
        aabb = aabb2::merge(aabb, aabbs[i]);
    return aabb;
}

}  // namespace nlc
