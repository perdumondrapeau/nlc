/*  Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/maths/constants.hpp>
#include <nlc/meta/units.hpp>

namespace nlc {
namespace impl {
    struct RadianDim {};
    struct DegreeDim {};

    template<typename T = float> constexpr T rad_to_deg_factor = static_cast<T>(180) / nlc::pi<T>;
    template<typename T = float>
    constexpr T deg_to_rad_factor = static_cast<T>(1) / rad_to_deg_factor<T>;
}  // namespace impl

namespace units {
    using radians =
        nlc::meta::unit<nlc::meta::pair<nlc::meta::list<impl::RadianDim>, nlc::meta::list<>>, float>;
    template<typename T = float>
    using degrees =
        nlc::meta::unit<nlc::meta::pair<nlc::meta::list<impl::DegreeDim>, nlc::meta::list<>>, T>;
}  // namespace units

using namespace units;

template<typename T = float> constexpr inline auto rad_to_deg(radians const & rad) -> degrees<T> {
    return static_cast<T>(nlc::rm_unit(rad) * impl::rad_to_deg_factor<float>);
}

template<typename T = float> constexpr inline auto deg_to_rad(degrees<T> const & deg) -> radians {
    return static_cast<float>(nlc::rm_unit(deg)) * impl::deg_to_rad_factor<float>;
}
}  // namespace nlc

constexpr inline auto operator""_rad(long double const v) -> nlc::radians {
    return static_cast<float>(v);
}

constexpr inline auto operator""_rad(unsigned long long int const v) -> nlc::radians {
    return static_cast<float>(v);
}

constexpr inline auto operator""_deg(long double const v) -> nlc::degrees<float> {
    return static_cast<float>(v);
}

constexpr inline auto operator""_deg(unsigned long long int const v) -> nlc::degrees<float> {
    return static_cast<float>(v);
}
