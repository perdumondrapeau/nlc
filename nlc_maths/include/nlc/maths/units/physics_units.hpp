/*  Copyright © 2020-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/maths/vec2.hpp>
#include <nlc/meta/list.hpp>
#include <nlc/meta/list/pair.hpp>
#include <nlc/meta/units.hpp>

namespace nlc {

namespace units {
    template<typename T = float> using seconds = meta::seconds<T>;
    template<typename T = float> using meters = meta::meters<T>;
    template<typename T = float> using newtons = meta::newtons<T>;
    template<typename T = float> using kilograms = meta::kilograms<T>;

    template<typename T = float> using meters_per_second = meta::meters_per_second<T>;
    template<typename T = float>
    using meters_per_second_squared =
        meta::unit<meta::pair<meta::list<meta::meter_dim>, meta::list<meta::second_dim, meta::second_dim>>, T>;
    template<typename T = float>
    using kilograms_per_second =
        meta::unit<meta::pair<meta::list<meta::kilogram_dim>, meta::list<meta::second_dim>>, T>;

    template<typename T = float> using friction_unit = kilograms_per_second<T>;

    // tech-debt:unit2d
    template<typename T = float> using pos2 = vec2<T>;
    template<typename T = float> using vel2 = vec2<T>;
    template<typename T = float> using acc2 = vec2<T>;
}  // namespace units

template<typename T> constexpr auto rm_unit(T & v) -> T & { return v; }
template<typename T> constexpr auto rm_unit(vec2<T> & v) -> vec2<T> & { return v; }
template<typename T> constexpr auto rm_unit(vec2<T> const v) -> vec2<T> const { return v; }

using namespace units;
}  // namespace nlc
