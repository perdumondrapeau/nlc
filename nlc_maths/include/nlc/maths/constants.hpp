/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

namespace nlc {

// Values are taken from Sollya (https://www.sollya.org/)

template<typename T>
constexpr inline T quarter_pi = static_cast<T>(0.78539816339744830961566084581987572104929234984377);

template<typename T>
constexpr inline T half_pi = static_cast<T>(1.57079632679489661923132169163975144209858469968754);

template<typename T>
constexpr inline T pi = static_cast<T>(3.1415926535897932384626433832795028841971693993751);

template<typename T>
constexpr inline T two_pi = static_cast<T>(6.2831853071795864769252867665590057683943387987502);
}  // namespace nlc
