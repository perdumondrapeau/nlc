/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include "impl/swizzling.hpp"
namespace nlc {

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> struct vec2 final {
    T x;
    T y;

    template<typename U> static constexpr vec2 cast(vec2<U> const vec) {
        return { static_cast<T>(vec.x), static_cast<T>(vec.y) };
    }

    [[nodiscard]] constexpr T const operator[](usize const i) const { return (&x)[i]; }
    [[nodiscard]] constexpr T & operator[](usize const i) { return (&x)[i]; }

    constexpr vec2 & operator++();
    constexpr vec2 operator++(int);
    constexpr vec2 & operator--();
    constexpr vec2 operator--(int);

    constexpr vec2 & operator+=(vec2 const);
    constexpr vec2 & operator-=(vec2 const);
    constexpr vec2 & operator*=(vec2 const);
    constexpr vec2 & operator/=(vec2 const);
    constexpr vec2 & operator%=(vec2 const);
    constexpr vec2 & operator|=(vec2 const);
    constexpr vec2 & operator&=(vec2 const);
    constexpr vec2 & operator^=(vec2 const);
    constexpr vec2 & operator<<=(vec2 const);  // /!\ beware, values must not be negative
    constexpr vec2 & operator>>=(vec2 const);  // /!\ beware, values must not be negative

    constexpr vec2 & operator+=(T const);
    constexpr vec2 & operator-=(T const);
    constexpr vec2 & operator*=(T const);
    constexpr vec2 & operator/=(T const);
    constexpr vec2 & operator%=(T const);
    constexpr vec2 & operator|=(T const);
    constexpr vec2 & operator&=(T const);
    constexpr vec2 & operator^=(T const);
    constexpr vec2 & operator<<=(T const);  // /!\ beware, values must not be negative
    constexpr vec2 & operator>>=(T const);  // /!\ beware, values must not be negative
};

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> constexpr vec2<T> & vec2<T>::operator++() {
    ++x;
    ++y;
    return *this;
}

template<class T> constexpr vec2<T> vec2<T>::operator++(int) {
    return { static_cast<T>(x++), static_cast<T>(y++) };
}

template<class T> constexpr vec2<T> & vec2<T>::operator--() {
    --x;
    --y;
    return *this;
}

template<class T> constexpr vec2<T> vec2<T>::operator--(int) {
    return { static_cast<T>(x--), static_cast<T>(y--) };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> [[nodiscard]] constexpr vec2<T> operator+(vec2<T> const vec) {
    return { static_cast<T>(+vec.x), static_cast<T>(+vec.y) };
}

template<class T> [[nodiscard]] constexpr vec2<T> operator-(vec2<T> const vec) {
    return { static_cast<T>(-vec.x), static_cast<T>(-vec.y) };
}

template<class T> [[nodiscard]] constexpr vec2<T> operator~(vec2<T> const vec) {
    return { static_cast<T>(~vec.x), static_cast<T>(~vec.y) };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

#if (defined(__GNUC__) && !defined(__clang__))
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wconversion"
#endif

#define IMPLEMENT_OPERATOR(OP)                                                        \
    template<class T> constexpr vec2<T> & vec2<T>::operator OP(vec2<T> const other) { \
        x OP other.x;                                                                 \
        y OP other.y;                                                                 \
        return *this;                                                                 \
    }                                                                                 \
    template<class T> constexpr vec2<T> & vec2<T>::operator OP(T const other) {       \
        x OP other;                                                                   \
        y OP other;                                                                   \
        return *this;                                                                 \
    }

IMPLEMENT_OPERATOR(+=)
IMPLEMENT_OPERATOR(-=)
IMPLEMENT_OPERATOR(*=)
IMPLEMENT_OPERATOR(/=)
IMPLEMENT_OPERATOR(%=)
IMPLEMENT_OPERATOR(|=)
IMPLEMENT_OPERATOR(&=)
IMPLEMENT_OPERATOR(^=)
IMPLEMENT_OPERATOR(<<=)  // /!\ beware, values must not be negative
IMPLEMENT_OPERATOR(>>=)  // /!\ beware, values must not be negative

#undef IMPLEMENT_OPERATOR

#if (defined(__GNUC__) && !defined(__clang__))
#    pragma GCC diagnostic pop
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////

#define IMPLEMENT_OPERATOR(OP)                                                       \
    template<class T>                                                                \
    [[nodiscard]] constexpr vec2<T> operator OP(vec2<T> const a, vec2<T> const b) {  \
        return { static_cast<T>(a.x OP b.x), static_cast<T>(a.y OP b.y) };           \
    }                                                                                \
    template<class T>                                                                \
    [[nodiscard]] constexpr vec2<T> operator OP(vec2<T> const vec, T const scalar) { \
        return { static_cast<T>(vec.x OP scalar), static_cast<T>(vec.y OP scalar) }; \
    }                                                                                \
    template<class T>                                                                \
    [[nodiscard]] constexpr vec2<T> operator OP(T const scalar, vec2<T> const vec) { \
        return { static_cast<T>(scalar OP vec.x), static_cast<T>(scalar OP vec.y) }; \
    }

IMPLEMENT_OPERATOR(+)
IMPLEMENT_OPERATOR(-)
IMPLEMENT_OPERATOR(*)
IMPLEMENT_OPERATOR(/)
IMPLEMENT_OPERATOR(%)
IMPLEMENT_OPERATOR(|)
IMPLEMENT_OPERATOR(&)
IMPLEMENT_OPERATOR(^)
IMPLEMENT_OPERATOR(<<)  // /!\ beware, values must not be negative
IMPLEMENT_OPERATOR(>>)  // /!\ beware, values must not be negative

#undef IMPLEMENT_OPERATOR

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> [[nodiscard]] constexpr bool operator==(vec2<T> const a, vec2<T> const b) {
    return a.x == b.x && a.y == b.y;
}

template<class T> [[nodiscard]] constexpr bool operator!=(vec2<T> const a, vec2<T> const b) {
    return a.x != b.x || a.y != b.y;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<vec_component_indices first, vec_component_indices second, typename T>
[[nodiscard]] constexpr auto swizzle(T const vec) {
    return vec2<decltype(vec.x)> { vec[static_cast<usize>(first)], vec[static_cast<usize>(second)] };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc

using i8_2 = nlc::vec2<i8>;
using u8_2 = nlc::vec2<u8>;

using i16_2 = nlc::vec2<i16>;
using u16_2 = nlc::vec2<u16>;

using i32_2 = nlc::vec2<i32>;
using u32_2 = nlc::vec2<u32>;

using i64_2 = nlc::vec2<i64>;
using u64_2 = nlc::vec2<u64>;

using isize_2 = nlc::vec2<isize>;
using usize_2 = nlc::vec2<usize>;

using f32_2 = nlc::vec2<float>;
using f64_2 = nlc::vec2<double>;
