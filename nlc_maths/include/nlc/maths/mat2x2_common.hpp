/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

#include "mat2x2.hpp"
#include "vec2.hpp"
#include "vec2_common.hpp"

namespace nlc {

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T> constexpr mat2x2<T> & operator*=(mat2x2<T> & a, mat2x2<T> const & b) {
    a = a * b;
    return a;
}

template<typename T> constexpr mat2x2<T> operator*(mat2x2<T> const & a, mat2x2<T> const & b) {
    mat2x2<T> result;
    for (usize i = 0; i < 2; ++i)
        for (usize j = 0; j < 2; ++j)
            result[i][j] = dot({ a[0][j], a[1][j] }, b[i]);
    return result;
}

template<typename T>
[[nodiscard]] constexpr vec2<T> operator*(mat2x2<T> const & mat, vec2<T> const vec) {
    return { dot({ mat[0].x, mat[1].x }, vec), dot({ mat[0].y, mat[1].y }, vec) };
}

template<typename T>
[[nodiscard]] constexpr vec2<T> operator*(vec2<T> const vec, mat2x2<T> const & mat) {
    return { dot(mat[0], vec), dot(mat[1], vec) };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T> [[nodiscard]] constexpr mat2x2<T> transpose(mat2x2<T> const & mat) {
    return { { mat[0][0], mat[1][0] }, { mat[0][1], mat[1][1] } };
}

template<typename T> [[nodiscard]] constexpr meta::make_signed<T> det(mat2x2<T> const & mat) {
    return det(mat[0], mat[1]);
}

template<typename T> [[nodiscard]] constexpr mat2x2<T> inverse(mat2x2<T> const & mat) {
    auto const d = det(mat);
    return { { static_cast<T>(mat[1][1] / d), static_cast<T>(mat[0][1] / -d) },
             { static_cast<T>(mat[1][0] / -d), static_cast<T>(mat[0][0] / d) } };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

[[nodiscard]] inline bool are_floats_close_ulp_wise(f32_2x2 const & a,
                                                    f32_2x2 const & b,
                                                    usize const max_ulps = 0u) {
    return are_floats_close_ulp_wise(a.x_axis, b.x_axis, max_ulps) &&
           are_floats_close_ulp_wise(a.y_axis, b.y_axis, max_ulps);
}

[[nodiscard]] inline bool are_floats_close_ulp_wise(f64_2x2 const & a,
                                                    f64_2x2 const & b,
                                                    usize const max_ulps = 0u) {
    return are_floats_close_ulp_wise(a.x_axis, b.x_axis, max_ulps) &&
           are_floats_close_ulp_wise(a.y_axis, b.y_axis, max_ulps);
}

[[nodiscard]] inline bool
are_floats_close_absolute_value_wise(f32_2x2 const & a,
                                     f32_2x2 const & b,
                                     f32 const absolute_epsilon = meta::limits<f32>::epsilon) {
    return are_floats_close_absolute_value_wise(a.x_axis, b.x_axis, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.y_axis, b.y_axis, absolute_epsilon);
}

[[nodiscard]] inline bool
are_floats_close_value_wise(f64_2x2 const & a,
                            f64_2x2 const & b,
                            f64 const absolute_epsilon = meta::limits<f64>::epsilon) {
    return are_floats_close_absolute_value_wise(a.x_axis, b.x_axis, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.y_axis, b.y_axis, absolute_epsilon);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc
