/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include "constants.hpp"
#include "simple_maths.hpp"
#include "vec2.hpp"

namespace nlc {

namespace impl {
    inline auto map_even_odd_to_sign(u32 const v) -> f32 {
        impl::float_bits res { .value = 1.f };
        res.bits |= (v << 31u);
        return res.value;
    }
}  // namespace impl

namespace reduced_range {

    // Minimax polynomials taken from http://gallium.inria.fr/blog/fast-vectorizable-math-approx/

    // Correct only in [-π; π]
    // Absolute error bounded by 6e-6
    // Continuous error
    inline auto sin(f32 const x) -> f32 {
        nlc_assert_msg(x >= -pi<f32> && x <= pi<f32>, nlc_dump_var(x));
        auto const x2 = x * x;
        return x * (0.9999793767f +
                    x2 * (-0.1666243672f + x2 * (8.3089787513e-3f +
                                                 x2 * (-1.9264918228e-4f + x2 * 2.1478401777e-6f))));
    }

    // Correct only in [-π; π]
    // Absolute error bounded by 5e-5
    // Continuous error
    inline auto cos(f32 const x) -> f32 {
        nlc_assert_msg(x >= -pi<f32> && x <= pi<f32>, nlc_dump_var(x));
        auto const x2 = x * x;
        return 1.f + x2 * (-0.4998515820f + x2 * (4.1518035216e-2f +
                                                  x2 * (-1.3422947025e-3f + x2 * 1.8929864824e-5f)));
    }

    // Correct only in [-π/4; π/4]
    // Absolute error bounded by 1.8e-8
    // Discontinuous error
    // Generated in sollya with:
    // fpminimax(tan(x), [|1,3,5,7,9,11,13|], [|single...|], [0;pi/4], floating, relative)
    inline auto tan(f32 const x) -> f32 {
        nlc_assert_msg(x >= -quarter_pi<f32> && x <= quarter_pi<f32>, nlc_dump_var(x));
        auto const x2 = x * x;
        return x * (1.f + x2 * (0.333331525325775146484375f +
                                x2 * (0.1333887577056884765625f +
                                      x2 * (5.3406536579132080078125e-2f +
                                            x2 * (2.444359846413135528564453125e-2f +
                                                  x2 * (3.102063201367855072021484375e-3f +
                                                        x2 * 9.394395165145397186279296875e-3f))))));
    }

    // Correct only in [-1; 1]
    // Absolute error bounded by 3.5e-6
    // Discontinuous error
    // Generated in sollya with:
    // fpminimax(atan(x), [|1,3,5,7,9,11|], [|single...|], [0;1], floating, relative)
    inline auto atan(f32 const x) -> f32 {
        nlc_assert_msg(x >= -1.f && x <= 1.f, nlc_dump_var(x));
        auto const x2 = x * x;
        return x * (0.999995648860931396484375f +
                    x2 * (-0.3329949676990509033203125f +
                          x2 * (0.19563795626163482666015625f +
                                x2 * (-0.121243648231029510498046875f +
                                      x2 * (5.7481847703456878662109375e-2f +
                                            x2 * (-1.3482107780873775482177734375e-2f))))));
    }
}  // namespace reduced_range

inline auto sin(f32 const x) -> f32 {
    constexpr auto range = pi<f32>;
    auto const k = static_cast<i32>(x / range);
    auto const y = x - static_cast<f32>(k) * range;
    auto const s = reduced_range::sin(y);
    return impl::map_even_odd_to_sign(k & 1) * s;
}

inline auto cos(f32 const x) -> f32 {
    constexpr auto range = pi<f32>;
    auto const k = static_cast<i32>(x / range);
    auto const y = x - static_cast<f32>(k) * range;
    auto const c = reduced_range::cos(y);
    return impl::map_even_odd_to_sign(k & 1) * c;
}

// This can benefit of some optimizations that are not done yet
inline auto cos_sin(f32 const x) -> f32_2 { return { cos(x), sin(x) }; }

inline f32 tan(f32 x) {
    // Implemented following "Even faster maths functions" by Robin Green

    auto const s = sign(x);
    x = abs(x);

    constexpr auto range = quarter_pi<f32>;
    auto const k = static_cast<u32>(x / range);
    auto const y = x - static_cast<f32>(k) * range;

    auto const z = [=] {
        switch (k & 0b11) {  // k % 4
            case 0: return reduced_range::tan(y);
            case 1: return 1.f / reduced_range::tan(range - y);
            case 2: return -1.f / reduced_range::tan(y);
            case 3: return -reduced_range::tan(range - y);
            default: nlc_unreachable;
        }
    }();
    return z * s;
}

inline auto atan(f32 const x) -> f32 {
    // atan(x) = π/2 - atan(1/x)
    // and our atan approximation is defined on [-1; 1]
    if (x < -1.f || x > 1.f) {
        // atan is an odd function, so atan(-x) = atan(1/x) - π/2
        // the result of atan(1.f / x) will be correct sign-wise, we just need to apply it to π/2
        auto const offset = copysign(half_pi<f32>, x);

        return offset - reduced_range::atan(1.f / x);
    }

    return reduced_range::atan(x);
}

inline auto atan2(f32 const y, f32 const x) -> f32 {
    if (x != 0.f) {
        if (abs(x) >= abs(y)) {
            // atan2(y,x) = atan(y/x) if x > 0
            // atan2(y,x) = atan(y/x) + π if x < 0 and y >= 0
            // atan2(y,x) = atan(y/x) - π if x < 0 and y < 0
            auto const offset = copysign(pi<f32>, y) * static_cast<f32>(x < 0.f);
            return reduced_range::atan(y / x) + offset;
        }

        // atan2(y,x) = π/2 - atan(x/y) if y > 0
        // atan2(y,x) = -π/2 - atan(x/y) if y < 0
        return copysign(half_pi<f32>, y) - reduced_range::atan(x / y);
    }

    if (y != 0.f)
        return copysign(half_pi<f32>, y);

    return 0.f;
}

}  // namespace nlc
