/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

#include "vec3.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace nlc {
template<typename T> struct mat3x3 final {
    vec3<T> x_axis;
    vec3<T> y_axis;
    vec3<T> z_axis;

    [[nodiscard]] static constexpr mat3x3 identity() {
        return { { 1, 0, 0 }, { 0, 1, 0 }, { 0, 0, 1 } };
    }

    [[nodiscard]] constexpr vec3<T> const operator[](usize const i) const { return (&x_axis)[i]; }
    [[nodiscard]] constexpr vec3<T> & operator[](usize const i) { return (&x_axis)[i]; }
};

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T>
[[nodiscard]] constexpr bool operator==(mat3x3<T> const & a, mat3x3<T> const & b) {
    return a.x_axis == b.x_axis && a.y_axis == b.y_axis && a.z_axis == b.z_axis;
}

template<class T>
[[nodiscard]] constexpr bool operator!=(mat3x3<T> const & a, mat3x3<T> const & b) {
    return a.x_axis != b.x_axis || a.y_axis != b.y_axis || a.z_axis != b.z_axis;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc

using i8_3x3 = nlc::mat3x3<i8>;
using u8_3x3 = nlc::mat3x3<u8>;

using i16_3x3 = nlc::mat3x3<i16>;
using u16_3x3 = nlc::mat3x3<u16>;

using i32_3x3 = nlc::mat3x3<i32>;
using u32_3x3 = nlc::mat3x3<u32>;

using i64_3x3 = nlc::mat3x3<i64>;
using u64_3x3 = nlc::mat3x3<u64>;

using isize_3x3 = nlc::mat3x3<isize>;
using usize_3x3 = nlc::mat3x3<usize>;

using f32_3x3 = nlc::mat3x3<f32>;
using f64_3x3 = nlc::mat3x3<f64>;
