/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

namespace nlc {

enum class vec_component_indices { x = 0, y = 1, z = 2, w = 3 };

#define NLC_SWIZZLE2(vec, first, second) \
    ::nlc::swizzle<::nlc::vec_component_indices::first, ::nlc::vec_component_indices::second>(vec)

#define NLC_SWIZZLE3(vec, first, second, third)          \
    ::nlc::swizzle<::nlc::vec_component_indices::first,  \
                   ::nlc::vec_component_indices::second, \
                   ::nlc::vec_component_indices::third>(vec)

#define NLC_SWIZZLE4(vec, first, second, third, fourth)  \
    ::nlc::swizzle<::nlc::vec_component_indices::first,  \
                   ::nlc::vec_component_indices::second, \
                   ::nlc::vec_component_indices::third,  \
                   ::nlc::vec_component_indices::fourth>(vec)

#define NLC_GET_SWIZZLE_MACRO(_1, _2, _3, _4, _5, NAME, ...) NAME
#define nlc_swizzle(...)                                                             \
    NLC_GET_SWIZZLE_MACRO(__VA_ARGS__, NLC_SWIZZLE4, NLC_SWIZZLE3, NLC_SWIZZLE2, , ) \
    (__VA_ARGS__)

}  // namespace nlc
