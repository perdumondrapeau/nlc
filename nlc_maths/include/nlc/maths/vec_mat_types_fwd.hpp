/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace nlc {
template<typename> struct vec2;
template<typename> struct vec3;
template<typename> struct vec4;
template<typename> struct mat2x2;
template<typename> struct mat3x3;
template<typename> struct mat4x4;
}  // namespace nlc

// vec2 ////////////////////////////////////////////////////////////////////////////////////////////

using i8_2 = nlc::vec2<i8>;
using u8_2 = nlc::vec2<u8>;

using i16_2 = nlc::vec2<i16>;
using u16_2 = nlc::vec2<u16>;

using i32_2 = nlc::vec2<i32>;
using u32_2 = nlc::vec2<u32>;

using i64_2 = nlc::vec2<i64>;
using u64_2 = nlc::vec2<u64>;

using isize_2 = nlc::vec2<isize>;
using usize_2 = nlc::vec2<usize>;

using f32_2 = nlc::vec2<f32>;
using f64_2 = nlc::vec2<f64>;

// vec3 ////////////////////////////////////////////////////////////////////////////////////////////

using i8_3 = nlc::vec3<i8>;
using u8_3 = nlc::vec3<u8>;

using i16_3 = nlc::vec3<i16>;
using u16_3 = nlc::vec3<u16>;

using i32_3 = nlc::vec3<i32>;
using u32_3 = nlc::vec3<u32>;

using i64_3 = nlc::vec3<i64>;
using u64_3 = nlc::vec3<u64>;

using isize_3 = nlc::vec3<isize>;
using usize_3 = nlc::vec3<usize>;

using f32_3 = nlc::vec3<f32>;
using f64_3 = nlc::vec3<f64>;

// vec4 ////////////////////////////////////////////////////////////////////////////////////////////

using i8_4 = nlc::vec4<i8>;
using u8_4 = nlc::vec4<u8>;

using i16_4 = nlc::vec4<i16>;
using u16_4 = nlc::vec4<u16>;

using i32_4 = nlc::vec4<i32>;
using u32_4 = nlc::vec4<u32>;

using i64_4 = nlc::vec4<i64>;
using u64_4 = nlc::vec4<u64>;

using isize_4 = nlc::vec4<isize>;
using usize_4 = nlc::vec4<usize>;

using f32_4 = nlc::vec4<f32>;
using f64_4 = nlc::vec4<f64>;

// mat2x2 //////////////////////////////////////////////////////////////////////////////////////////

using i8_2x2 = nlc::mat2x2<i8>;
using u8_2x2 = nlc::mat2x2<u8>;

using i16_2x2 = nlc::mat2x2<i16>;
using u16_2x2 = nlc::mat2x2<u16>;

using i32_2x2 = nlc::mat2x2<i32>;
using u32_2x2 = nlc::mat2x2<u32>;

using i64_2x2 = nlc::mat2x2<i64>;
using u64_2x2 = nlc::mat2x2<u64>;

using isize_2x2 = nlc::mat2x2<isize>;
using usize_2x2 = nlc::mat2x2<usize>;

using f32_2x2 = nlc::mat2x2<f32>;
using f64_2x2 = nlc::mat2x2<f64>;

// mat3x3 //////////////////////////////////////////////////////////////////////////////////////////

using i8_3x3 = nlc::mat3x3<i8>;
using u8_3x3 = nlc::mat3x3<u8>;

using i16_3x3 = nlc::mat3x3<i16>;
using u16_3x3 = nlc::mat3x3<u16>;

using i32_3x3 = nlc::mat3x3<i32>;
using u32_3x3 = nlc::mat3x3<u32>;

using i64_3x3 = nlc::mat3x3<i64>;
using u64_3x3 = nlc::mat3x3<u64>;

using isize_3x3 = nlc::mat3x3<isize>;
using usize_3x3 = nlc::mat3x3<usize>;

using f32_3x3 = nlc::mat3x3<f32>;
using f64_3x3 = nlc::mat3x3<f64>;

// mat4x4 //////////////////////////////////////////////////////////////////////////////////////////

using i8_4x4 = nlc::mat4x4<i8>;
using u8_4x4 = nlc::mat4x4<u8>;

using i16_4x4 = nlc::mat4x4<i16>;
using u16_4x4 = nlc::mat4x4<u16>;

using i32_4x4 = nlc::mat4x4<i32>;
using u32_4x4 = nlc::mat4x4<u32>;

using i64_4x4 = nlc::mat4x4<i64>;
using u64_4x4 = nlc::mat4x4<u64>;

using isize_4x4 = nlc::mat4x4<isize>;
using usize_4x4 = nlc::mat4x4<usize>;

using f32_4x4 = nlc::mat4x4<f32>;
using f64_4x4 = nlc::mat4x4<f64>;

////////////////////////////////////////////////////////////////////////////////////////////////////
