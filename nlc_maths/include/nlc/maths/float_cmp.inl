/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "simple_maths.hpp"

namespace nlc {
// Inspired from
// https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition
#define CMP_FLOAT_ULP_WISE_IMPL(a, b, max_ulps, float_type, int_type)                                \
    static_assert(sizeof(float_type) == sizeof(int_type));                                           \
    constexpr auto zero = static_cast<float_type>(0);                                                \
    if ((a < zero) != (b < zero))                                                                    \
        return a == b;                                                                               \
    auto const diff = reinterpret_cast<int_type const &>(a) - reinterpret_cast<int_type const &>(b); \
                                                                                                     \
    return static_cast<usize>(abs(diff)) <= max_ulps

#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic push
#    if defined(__clang__)
#        pragma GCC diagnostic ignored "-Wundefined-reinterpret-cast"  // reinterpret_cast from
                                                                       // 'float_t' to 'int_t &'
                                                                       // has undefined behavior
#    elif defined(__GNUC__)
#        pragma GCC diagnostic ignored "-Wstrict-aliasing"  // dereferencing type-punned pointer
                                                            // will break strict-aliasing rules
#    endif
#endif
inline auto are_floats_close_ulp_wise(f32 const a, f32 const b, usize const max_ulps) -> bool {
    CMP_FLOAT_ULP_WISE_IMPL(a, b, max_ulps, f32, i32);
}

inline auto are_floats_close_ulp_wise(f64 const a, f64 const b, usize const max_ulps) -> bool {
    CMP_FLOAT_ULP_WISE_IMPL(a, b, max_ulps, f64, i64);
}
#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic pop
#endif

#undef CMP_FLOAT_ULP_WISE_IMPL

#define CMP_FLOAT_VALUE_WISE_IMPL(a, b, relative_epsilon) \
    auto const abs_a = abs(a);                            \
    auto const abs_b = abs(b);                            \
    auto const largest = (abs_b > abs_a) ? abs_b : abs_a; \
    return abs(a - b) <= largest * relative_epsilon

inline auto are_floats_close_relative_value_wise(f32 const a,
                                                 f32 const b,
                                                 f32 const relative_epsilon) -> bool {
    CMP_FLOAT_VALUE_WISE_IMPL(a, b, relative_epsilon);
}

inline auto are_floats_close_relative_value_wise(f64 const a,
                                                 f64 const b,
                                                 f64 const relative_epsilon) -> bool {
    CMP_FLOAT_VALUE_WISE_IMPL(a, b, relative_epsilon);
}

#undef CMP_FLOAT_VALUE_WISE_IMPL

#define CMP_FLOAT_RELATIVE_VALUE_WISE_IMPL(a, b, absolute_epsilon) \
    return abs(a - b) <= absolute_epsilon

inline auto are_floats_close_absolute_value_wise(f32 const a,
                                                 f32 const b,
                                                 f32 const absolute_epsilon) -> bool {
    CMP_FLOAT_RELATIVE_VALUE_WISE_IMPL(a, b, absolute_epsilon);
}

inline auto are_floats_close_absolute_value_wise(f64 const a,
                                                 f64 const b,
                                                 f64 const absolute_epsilon) -> bool {
    CMP_FLOAT_RELATIVE_VALUE_WISE_IMPL(a, b, absolute_epsilon);
}

#undef CMP_FLOAT_RELATIVE_VALUE_WISE_IMPL
}  // namespace nlc
