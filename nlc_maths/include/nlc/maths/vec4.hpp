/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include "impl/swizzling.hpp"

namespace nlc {

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> struct vec4 final {
    T x;
    T y;
    T z;
    T w;

    template<typename U> static constexpr vec4 cast(vec4<U> const vec) {
        return { static_cast<T>(vec.x),
                 static_cast<T>(vec.y),
                 static_cast<T>(vec.z),
                 static_cast<T>(vec.w) };
    }

    [[nodiscard]] constexpr T const operator[](usize const i) const { return (&x)[i]; }
    [[nodiscard]] constexpr T & operator[](usize const i) { return (&x)[i]; }

    constexpr vec4 & operator++();
    constexpr vec4 operator++(int);
    constexpr vec4 & operator--();
    constexpr vec4 operator--(int);

    constexpr vec4 & operator+=(vec4 const);
    constexpr vec4 & operator-=(vec4 const);
    constexpr vec4 & operator*=(vec4 const);
    constexpr vec4 & operator/=(vec4 const);
    constexpr vec4 & operator%=(vec4 const);
    constexpr vec4 & operator|=(vec4 const);
    constexpr vec4 & operator&=(vec4 const);
    constexpr vec4 & operator^=(vec4 const);
    constexpr vec4 & operator<<=(vec4 const);  // /!\ beware, values must not be negative
    constexpr vec4 & operator>>=(vec4 const);  // /!\ beware, values must not be negative

    constexpr vec4 & operator+=(T const);
    constexpr vec4 & operator-=(T const);
    constexpr vec4 & operator*=(T const);
    constexpr vec4 & operator/=(T const);
    constexpr vec4 & operator%=(T const);
    constexpr vec4 & operator|=(T const);
    constexpr vec4 & operator&=(T const);
    constexpr vec4 & operator^=(T const);
    constexpr vec4 & operator<<=(T const);  // /!\ beware, values must not be negative
    constexpr vec4 & operator>>=(T const);  // /!\ beware, values must not be negative
};

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> constexpr vec4<T> & vec4<T>::operator++() {
    ++x;
    ++y;
    ++z;
    ++w;
    return *this;
}

template<class T> constexpr vec4<T> vec4<T>::operator++(int) {
    return { static_cast<T>(x++), static_cast<T>(y++), static_cast<T>(z++), static_cast<T>(w++) };
}

template<class T> constexpr vec4<T> & vec4<T>::operator--() {
    --x;
    --y;
    --z;
    --w;
    return *this;
}

template<class T> constexpr vec4<T> vec4<T>::operator--(int) {
    return { static_cast<T>(x--), static_cast<T>(y--), static_cast<T>(z--), static_cast<T>(w--) };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> [[nodiscard]] constexpr vec4<T> operator+(vec4<T> const vec) {
    return { static_cast<T>(+vec.x),
             static_cast<T>(+vec.y),
             static_cast<T>(+vec.z),
             static_cast<T>(+vec.w) };
}

template<class T> [[nodiscard]] constexpr vec4<T> operator-(vec4<T> const vec) {
    return { static_cast<T>(-vec.x),
             static_cast<T>(-vec.y),
             static_cast<T>(-vec.z),
             static_cast<T>(-vec.w) };
}

template<class T> [[nodiscard]] constexpr vec4<T> operator~(vec4<T> const vec) {
    return { static_cast<T>(~vec.x),
             static_cast<T>(~vec.y),
             static_cast<T>(~vec.z),
             static_cast<T>(~vec.w) };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

#if (defined(__GNUC__) && !defined(__clang__))
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wconversion"
#endif

#define IMPLEMENT_OPERATOR(OP)                                                        \
    template<class T> constexpr vec4<T> & vec4<T>::operator OP(vec4<T> const other) { \
        x OP other.x;                                                                 \
        y OP other.y;                                                                 \
        z OP other.z;                                                                 \
        w OP other.w;                                                                 \
        return *this;                                                                 \
    }                                                                                 \
    template<class T> constexpr vec4<T> & vec4<T>::operator OP(T const scalar) {      \
        x OP scalar;                                                                  \
        y OP scalar;                                                                  \
        z OP scalar;                                                                  \
        w OP scalar;                                                                  \
        return *this;                                                                 \
    }

IMPLEMENT_OPERATOR(+=)
IMPLEMENT_OPERATOR(-=)
IMPLEMENT_OPERATOR(*=)
IMPLEMENT_OPERATOR(/=)
IMPLEMENT_OPERATOR(%=)
IMPLEMENT_OPERATOR(|=)
IMPLEMENT_OPERATOR(&=)
IMPLEMENT_OPERATOR(^=)
IMPLEMENT_OPERATOR(<<=)  // /!\ beware, values must not be negative
IMPLEMENT_OPERATOR(>>=)  // /!\ beware, values must not be negative

#undef IMPLEMENT_OPERATOR

#if (defined(__GNUC__) && !defined(__clang__))
#    pragma GCC diagnostic pop
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////

#define IMPLEMENT_OPERATOR(OP)                                                       \
    template<class T>                                                                \
    [[nodiscard]] constexpr vec4<T> operator OP(vec4<T> const a, vec4<T> const b) {  \
        return { static_cast<T>(a.x OP b.x),                                         \
                 static_cast<T>(a.y OP b.y),                                         \
                 static_cast<T>(a.z OP b.z),                                         \
                 static_cast<T>(a.w OP b.w) };                                       \
    }                                                                                \
    template<class T>                                                                \
    [[nodiscard]] constexpr vec4<T> operator OP(vec4<T> const vec, T const scalar) { \
        return { static_cast<T>(vec.x OP scalar),                                    \
                 static_cast<T>(vec.y OP scalar),                                    \
                 static_cast<T>(vec.z OP scalar),                                    \
                 static_cast<T>(vec.w OP scalar) };                                  \
    }                                                                                \
    template<class T>                                                                \
    [[nodiscard]] constexpr vec4<T> operator OP(T const scalar, vec4<T> const vec) { \
        return { static_cast<T>(scalar OP vec.x),                                    \
                 static_cast<T>(scalar OP vec.y),                                    \
                 static_cast<T>(scalar OP vec.z),                                    \
                 static_cast<T>(scalar OP vec.w) };                                  \
    }

IMPLEMENT_OPERATOR(+)
IMPLEMENT_OPERATOR(-)
IMPLEMENT_OPERATOR(*)
IMPLEMENT_OPERATOR(/)
IMPLEMENT_OPERATOR(%)
IMPLEMENT_OPERATOR(|)
IMPLEMENT_OPERATOR(&)
IMPLEMENT_OPERATOR(^)
IMPLEMENT_OPERATOR(<<)  // /!\ beware, values must not be negative
IMPLEMENT_OPERATOR(>>)  // /!\ beware, values must not be negative

#undef IMPLEMENT_OPERATOR

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> [[nodiscard]] constexpr bool operator==(vec4<T> const a, vec4<T> const b) {
    return a.x == b.x && a.y == b.y && a.z == b.z && a.w == b.w;
}

template<class T> [[nodiscard]] constexpr bool operator!=(vec4<T> const a, vec4<T> const b) {
    return a.x != b.x || a.y != b.y || a.z != b.z || a.w != b.w;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<vec_component_indices first, vec_component_indices second, vec_component_indices third, vec_component_indices fourth, typename T>
[[nodiscard]] constexpr auto swizzle(T const vec) {
    return vec4<decltype(vec.x)> { vec[static_cast<usize>(first)],
                                   vec[static_cast<usize>(second)],
                                   vec[static_cast<usize>(third)],
                                   vec[static_cast<usize>(fourth)] };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc

using i8_4 = nlc::vec4<i8>;
using u8_4 = nlc::vec4<u8>;

using i16_4 = nlc::vec4<i16>;
using u16_4 = nlc::vec4<u16>;

using i32_4 = nlc::vec4<i32>;
using u32_4 = nlc::vec4<u32>;

using i64_4 = nlc::vec4<i64>;
using u64_4 = nlc::vec4<u64>;

using isize_4 = nlc::vec4<isize>;
using usize_4 = nlc::vec4<usize>;

using f32_4 = nlc::vec4<f32>;
using f64_4 = nlc::vec4<f64>;
