/*  Copyright © 2021-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

/*
The MIT License (MIT)
Copyright (c) 2015 Jacques-Henri Jourdan <jourgun@gmail.com>
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
// The MIT license applies to the following functions, modified to suit nlc style:
// exp, log

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/meta/numeric_limits.hpp>
#include "simple_maths.hpp"

namespace nlc {

namespace impl {
    // Workaround a lack of optimization in gcc
    // Do *NOT* modify these variable declaractions
    inline f32 exp_cst1_f = 2139095040.f;
    inline f32 exp_cst2_f = 0.f;
}  // namespace impl

/* Relative error bounded by 1e-5 for normalized outputs
Returns invalid outputs for nan inputs
Continuous error */
inline auto exp(f32 const val) -> f32 {
    auto const val2 = 12102203.1615614f * val + 1065353216.f;
    auto const val3 = val2 < impl::exp_cst1_f ? val2 : impl::exp_cst1_f;
    auto const val4 = val3 > impl::exp_cst2_f ? val3 : impl::exp_cst2_f;
    auto const val4i = static_cast<u32>(val4);
    auto const xu = impl::float_bits { .bits = val4i & 0x7F800000 };
    auto const xu2 = impl::float_bits { .bits = (val4i & 0x7FFFFF) | 0x3F800000 };
    auto const b = xu2.value;

    /* Generated in Sollya with:
       > f=remez(1-x*exp(-(x-1)*log(2)),
                 [|(x-1)*(x-2), (x-1)*(x-2)*x, (x-1)*(x-2)*x*x|],
                 [1.000001,1.999999], exp(-(x-1)*log(2)));
       > plot(exp((x-1)*log(2))/(f+x)-1, [1,2]);
       > f+x;
    */
    return xu.value *
           (0.509871020f +
            b * (0.312146713f + b * (0.166617139f + b * (-2.190619930e-3f + b * 1.3555747234e-2f))));
}

/* Absolute error bounded by 1e-5 for normalized inputs
   Returns a finite number for +inf input
   Returns -inf for nan and <= 0 inputs.
   Continuous error. */
inline auto log(f32 const val) -> f32 {
    auto valu = impl::float_bits { .value = val };
    auto const exp = static_cast<f32>(valu.bits >> 23);
    /* -89.970756366f = -127 * log(2) + constant term of polynomial bellow. */
    auto const addcst = val > 0 ? -89.970756366f : -meta::limits<f32>::infinity;
    valu.bits = (valu.bits & 0x7FFFFF) | 0x3F800000;
    auto const x = valu.value;

    /* Generated in Sollya using:
      > f = remez(log(x)-(x-1)*log(2),
              [|1,(x-1)*(x-2), (x-1)*(x-2)*x, (x-1)*(x-2)*x*x,
                (x-1)*(x-2)*x*x*x|], [1,2], 1, 1e-8);
      > plot(f+(x-1)*log(2)-log(x), [1,2]);
      > f+(x-1)*log(2)
   */
    return x * (3.529304993f +
                x * (-2.461222105f + x * (1.130626167f + x * (-0.288739945f + x * 3.110401639e-2f)))) +
           (addcst + 0.6931471805f * exp);
}

#if NLC_CPU_X64
// To avoid including <xmmintrin.h> which is expensive
typedef float __v4sf __attribute__((__vector_size__(16)));
typedef float __m128 __attribute__((__vector_size__(16), __aligned__(16)));

// __builtin_sqrtf calls into libc's sqrtf which is slow, bypass with intrinsics
inline auto sqrt(f32 x) -> f32 {
    nlc_assert(x >= 0.f);
    __m128 temp = { x, 0, 0, 0 };
    temp = __builtin_ia32_sqrtss(reinterpret_cast<__v4sf>(temp));
    return temp[0];
}

inline auto rsqrt(f32 x) -> f32 {
    nlc_assert(x > 0.f);
    __m128 temp = { x, 0, 0, 0 };
    temp = __builtin_ia32_rsqrtss(reinterpret_cast<__v4sf>(temp));
    return temp[0];
}
#else
inline auto sqrt(f32 x) -> f32 {
    nlc_assert(x >= 0.f);
    return __builtin_sqrtf(x);
}

inline auto rsqrt(f32 x) -> f32 {
    nlc_assert(x > 0.f);
    return 1.f / sqrt(x);
}
#endif

}  // namespace nlc
