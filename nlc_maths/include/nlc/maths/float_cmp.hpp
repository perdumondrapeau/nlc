/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/meta/numeric_limits.hpp>

namespace nlc {
// Returns true if there are at most `max_ulps` distinct floats between `a` and `b`
inline auto are_floats_close_ulp_wise(f32 const a, f32 const b, usize const max_ulps = 0u) -> bool;
inline auto are_floats_close_ulp_wise(f64 const a, f64 const b, usize const max_ulps = 0u) -> bool;

// Returns true if the difference between `a` and `b` is less or equal to `relative_epsilon` * the
// largest value between `a` and `b`
inline auto
are_floats_close_relative_value_wise(f32 const a,
                                     f32 const b,
                                     f32 const relative_epsilon = meta::limits<f32>::epsilon) -> bool;
inline auto
are_floats_close_relative_value_wise(f64 const a,
                                     f64 const b,
                                     f64 const relative_epsilon = meta::limits<f64>::epsilon) -> bool;

// Returns true if the difference between `a` and `b` is less or equal to `absolute_epsilon`
inline auto
are_floats_close_absolute_value_wise(f32 const a,
                                     f32 const b,
                                     f32 const absolute_epsilon = meta::limits<f32>::epsilon) -> bool;
inline auto
are_floats_close_absolute_value_wise(f64 const a,
                                     f64 const b,
                                     f64 const absolute_epsilon = meta::limits<f64>::epsilon) -> bool;

}  // namespace nlc

#include "float_cmp.inl"
