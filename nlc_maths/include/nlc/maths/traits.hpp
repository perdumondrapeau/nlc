/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

namespace nlc {

template<class T> struct vec2;
template<class T> struct vec3;
template<class T> struct vec4;

namespace impl {
    template<typename T> struct dimension final {
        static constexpr usize value = 1;
    };
    template<typename T> struct dimension<vec2<T>> final {
        static constexpr usize value = 2;
    };
    template<typename T> struct dimension<vec3<T>> final {
        static constexpr usize value = 3;
    };
    template<typename T> struct dimension<vec4<T>> final {
        static constexpr usize value = 4;
    };
}  // namespace impl

template<typename T> inline constexpr auto dimension = impl::dimension<T>::value;

namespace impl {
    template<typename T> struct underlying_type final {
        using type = T;
    };
    template<typename T> struct underlying_type<vec2<T>> final {
        using type = T;
    };
    template<typename T> struct underlying_type<vec3<T>> final {
        using type = T;
    };
    template<typename T> struct underlying_type<vec4<T>> final {
        using type = T;
    };
}  // namespace impl

template<typename T> using underlying_type = typename impl::underlying_type<T>::type;

}  // namespace nlc
