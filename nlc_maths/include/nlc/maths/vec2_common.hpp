/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "float_cmp.hpp"
#include "functions.hpp"
#include "simple_maths.hpp"
#include "vec2.hpp"

namespace nlc {

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> [[nodiscard]] inline vec2<T> abs(vec2<T> const vec) {
    return { abs(vec.x), abs(vec.y) };
}

template<class T> [[nodiscard]] inline vec2<T> sign(vec2<T> const vec) {
    return { sign(vec.x), sign(vec.y) };
}

template<class T> [[nodiscard]] constexpr vec2<T> min(vec2<T> const a, vec2<T> const b) {
    return { min(a.x, b.x), min(a.y, b.y) };
}

template<class T> [[nodiscard]] constexpr vec2<T> max(vec2<T> const a, vec2<T> const b) {
    return { max(a.x, b.x), max(a.y, b.y) };
}

template<class T>
[[nodiscard]] constexpr vec2<T> clamp(vec2<T> const vec, vec2<T> const vmin, vec2<T> const vmax) {
    return { clamp(vec.x, vmin.x, vmax.x), clamp(vec.y, vmin.y, vmax.y) };
}

template<class T> [[nodiscard]] constexpr T sum(vec2<T> const vec) {
    return static_cast<T>(vec.x + vec.y);
}

template<class T> [[nodiscard]] constexpr T sqrnorm(vec2<T> const vec) {
    return static_cast<T>(vec.x * vec.x + vec.y * vec.y);
}

template<class T> [[nodiscard]] constexpr T dot(vec2<T> const a, vec2<T> const b) {
    return static_cast<T>(a.x * b.x + a.y * b.y);
}

template<class T>
[[nodiscard]] constexpr meta::make_signed<T> det(vec2<T> const a, vec2<T> const b) {
    if constexpr (meta::is_floating<T>) {
        return a.x * b.y - a.y * b.x;
    } else {
        using S = meta::make_signed<T>;
        return static_cast<S>(a.x * b.y) - static_cast<S>(a.y * b.x);
    }
}

template<class T> [[nodiscard]] inline T norm(vec2<T> const vec) {
    return static_cast<T>(sqrt(static_cast<f32>(sqrnorm(vec))));
}

template<class T> [[nodiscard]] inline vec2<T> normalize(vec2<T> const vec) {
    return vec / norm(vec);
}

template<class T>
[[nodiscard]] constexpr vec2<meta::make_signed<T>> project(vec2<T> const vec, vec2<T> const normal) {
    using S = meta::make_signed<T>;
    return vec2<S>::cast(vec) - vec2<S>::cast(normal * dot(vec, normal));
}

template<class T> [[nodiscard]] constexpr vec2<T> reflect(vec2<T> const vec, vec2<T> const normal) {
    using S = meta::make_signed<T>;
    return vec2<S>::cast(vec) - vec2<S>(normal * dot(vec, normal * static_cast<T>(2)));
}

template<class T>
[[nodiscard]] inline vec2<meta::make_signed<T>> normalize_project(vec2<T> const vec,
                                                                  vec2<T> const normal) {
    return project(vec, normalize(normal));
}

template<class T>
[[nodiscard]] inline vec2<T> normalize_reflect(vec2<T> const vec, vec2<T> const normal) {
    return reflect(vec, normalize(normal));
}

template<class T> [[nodiscard]] inline T sqrdistance(vec2<T> const a, vec2<T> const b) {
    if constexpr (meta::is_unsigned_integer<T>) {
        auto const x = a.x > b.x ? a.x - b.x : b.x - a.x;
        auto const y = a.y > b.y ? a.y - b.y : b.y - a.y;
        return static_cast<T>(x * x + y * y);
    } else {
        return sqrnorm(a - b);
    }
}

template<class T> [[nodiscard]] inline T distance(vec2<T> const a, vec2<T> const b) {
    return static_cast<T>(sqrt(static_cast<f32>(sqrdistance(a, b))));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
[[nodiscard]] inline auto scalar_lerp(vec2<T> const from, vec2<T> const to, f32 const t) -> vec2<T> {
    return vec2<T>::cast(lerp(f32_2::cast(from), f32_2::cast(to), t));
}

template<typename T>
[[nodiscard]] inline auto scalar_lerp(vec2<T> const from, vec2<T> const to, f64 const t) -> vec2<T> {
    return vec2<T>::cast(lerp(f64_2::cast(from), f64_2::cast(to), t));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

[[nodiscard]] inline f32 rnorm(f32_2 const vec) {
    return static_cast<f32>(rsqrt(static_cast<f32>(sqrnorm(vec))));
}

[[nodiscard]] inline f64 rnorm(f64_2 const vec) {
    return static_cast<f64>(rsqrt(static_cast<f32>(sqrnorm(vec))));
}

[[nodiscard]] inline f32_2 normalize(f32_2 const vec) { return vec * rnorm(vec); }

[[nodiscard]] inline f64_2 normalize(f64_2 const vec) { return vec * rnorm(vec); }

////////////////////////////////////////////////////////////////////////////////////////////////////

[[nodiscard]] inline bool are_floats_close_ulp_wise(f32_2 const a,
                                                    f32_2 const b,
                                                    usize const max_ulps = 0u) {
    return are_floats_close_ulp_wise(a.x, b.x, max_ulps) &&
           are_floats_close_ulp_wise(a.y, b.y, max_ulps);
}

[[nodiscard]] inline bool are_floats_close_ulp_wise(f64_2 const a,
                                                    f64_2 const b,
                                                    usize const max_ulps = 0u) {
    return are_floats_close_ulp_wise(a.x, b.x, max_ulps) &&
           are_floats_close_ulp_wise(a.y, b.y, max_ulps);
}

[[nodiscard]] inline bool
are_floats_close_absolute_value_wise(f32_2 const a,
                                     f32_2 const b,
                                     f32 const absolute_epsilon = meta::limits<f32>::epsilon) {
    return are_floats_close_absolute_value_wise(a.x, b.x, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.y, b.y, absolute_epsilon);
}

[[nodiscard]] inline bool
are_floats_close_absolute_value_wise(f64_2 const a,
                                     f64_2 const b,
                                     f64 const absolute_epsilon = meta::limits<f64>::epsilon) {
    return are_floats_close_absolute_value_wise(a.x, b.x, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.y, b.y, absolute_epsilon);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename STREAM, typename T> void format(STREAM & entry, vec2<T> const vec) {
    append(entry, "{", vec.x, ", ", vec.y, '}');
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc
