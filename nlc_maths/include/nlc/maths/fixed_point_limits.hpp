/*  Copyright © 2023-2023
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/traits.hpp>
#include "fixed_point.hpp"

namespace nlc::meta {

template<typename T> struct limits;

template<> template<typename U, u8 exp> struct limits<fixed_point<U, exp>> final {
    static constexpr fixed_point<U, exp> min { is_signed_integer<U>
                                                   ? static_cast<U>(1) << (sizeof(U) * 8 - 1)
                                                   : 0 };
    static constexpr fixed_point<U, exp> max { is_signed_integer<U> ? ~static_cast<U>(0) >> 1
                                                                    : ~static_cast<U>(0) };
    static constexpr fixed_point<U, exp> epsilon { 1 };
};

}  // namespace nlc::meta
