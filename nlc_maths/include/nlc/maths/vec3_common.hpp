/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "float_cmp.hpp"
#include "functions.hpp"
#include "simple_maths.hpp"
#include "vec2.hpp"
#include "vec2_common.hpp"
#include "vec3.hpp"

namespace nlc {

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> [[nodiscard]] inline vec3<T> abs(vec3<T> const vec) {
    return { abs(vec.x), abs(vec.y), abs(vec.z) };
}

template<class T> [[nodiscard]] inline vec3<T> sign(vec3<T> const vec) {
    return { sign(vec.x), sign(vec.y), sign(vec.z) };
}

template<class T> [[nodiscard]] constexpr vec3<T> min(vec3<T> const a, vec3<T> const b) {
    return { min(a.x, b.x), min(a.y, b.y), min(a.z, b.z) };
}

template<class T> [[nodiscard]] constexpr vec3<T> max(vec3<T> const a, vec3<T> const b) {
    return { max(a.x, b.x), max(a.y, b.y), max(a.z, b.z) };
}

template<class T>
[[nodiscard]] constexpr vec3<T> clamp(vec3<T> const vec, vec3<T> const vmin, vec3<T> const vmax) {
    return { clamp(vec.x, vmin.x, vmax.x), clamp(vec.y, vmin.y, vmax.y), clamp(vec.z, vmin.z, vmax.z) };
}

template<class T> [[nodiscard]] constexpr T sum(vec3<T> const vec) {
    return static_cast<T>(vec.x + vec.y + vec.z);
}

template<class T> [[nodiscard]] constexpr T sqrnorm(vec3<T> const vec) {
    return static_cast<T>(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
}

template<class T> [[nodiscard]] constexpr T dot(vec3<T> const a, vec3<T> const b) {
    return static_cast<T>(a.x * b.x + a.y * b.y + a.z * b.z);
}

template<class T>
[[nodiscard]] constexpr meta::make_signed<T> det(vec3<T> const a, vec3<T> const b, vec3<T> const c) {
    vec2<T> const x_axis { b.x, c.x };
    vec2<T> const y_axis { b.y, c.y };
    vec2<T> const z_axis { b.z, c.z };
    if constexpr (meta::is_floating<T>) {
        return a.x * det(y_axis, z_axis) - a.y * det(x_axis, z_axis) + a.z * det(x_axis, y_axis);
    } else {
        using S = meta::make_signed<T>;
        return static_cast<S>(static_cast<S>(a.x) * det(y_axis, z_axis) -
                              static_cast<S>(a.y) * det(x_axis, z_axis) +
                              static_cast<S>(a.z) * det(x_axis, y_axis));
    }
}

template<class T> [[nodiscard]] inline T norm(vec3<T> const vec) {
    return static_cast<T>(sqrt(static_cast<f32>(sqrnorm(vec))));
}

template<class T> [[nodiscard]] inline vec3<T> normalize(vec3<T> const vec) {
    return vec / norm(vec);
}

template<class T>
[[nodiscard]] constexpr vec3<meta::make_signed<T>> cross(vec3<T> const a, vec3<T> const b) {
    using S = meta::make_signed<T>;
    return { static_cast<S>(a.y * b.z) - static_cast<S>(a.z * b.y),
             static_cast<S>(a.z * b.x) - static_cast<S>(a.x * b.z),
             static_cast<S>(a.x * b.y) - static_cast<S>(a.y * b.x) };
}

template<class T>
[[nodiscard]] constexpr vec3<meta::make_signed<T>> project(vec3<T> const vec, vec3<T> const normal) {
    using V = vec3<meta::make_signed<T>>;
    return V::cast(vec) - V::cast(normal * dot(vec, normal));
}

template<class T> [[nodiscard]] constexpr vec3<T> reflect(vec3<T> const vec, vec3<T> const normal) {
    using V = vec3<meta::make_signed<T>>;
    return V::cast(vec) - V::cast(normal * dot(vec, normal * static_cast<T>(2)));
}

template<class T>
[[nodiscard]] inline vec3<meta::make_signed<T>> normalize_project(vec3<T> const vec,
                                                                  vec3<T> const normal) {
    return project(vec, normalize(normal));
}

template<class T>
[[nodiscard]] inline vec3<T> normalize_reflect(vec3<T> const vec, vec3<T> const normal) {
    return reflect(vec, normalize(normal));
}

template<class T> [[nodiscard]] inline T sqrdistance(vec3<T> const a, vec3<T> const b) {
    if constexpr (meta::is_unsigned_integer<T>) {
        auto const x = a.x > b.x ? a.x - b.x : b.x - a.x;
        auto const y = a.y > b.y ? a.y - b.y : b.y - a.y;
        auto const z = a.z > b.z ? a.z - b.z : b.z - a.z;
        return static_cast<T>(x * x + y * y + z * z);
    } else {
        return sqrnorm(a - b);
    }
}

template<class T> [[nodiscard]] inline T distance(vec3<T> const a, vec3<T> const b) {
    return static_cast<T>(sqrt(static_cast<f32>(sqrdistance(a, b))));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
[[nodiscard]] inline auto scalar_lerp(vec3<T> const from, vec3<T> const to, f32 const t) -> vec3<T> {
    return vec3<T>::cast(lerp(f32_3::cast(from), f32_3::cast(to), t));
}

template<typename T>
[[nodiscard]] inline auto scalar_lerp(vec3<T> const from, vec3<T> const to, f64 const t) -> vec3<T> {
    return vec3<T>::cast(lerp(f64_3::cast(from), f64_3::cast(to), t));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

[[nodiscard]] inline f32 rnorm(f32_3 const vec) {
    return static_cast<f32>(rsqrt(static_cast<f32>(sqrnorm(vec))));
}

[[nodiscard]] inline f64 rnorm(f64_3 const vec) {
    return static_cast<f64>(rsqrt(static_cast<f32>(sqrnorm(vec))));
}

[[nodiscard]] inline vec3<f32> normalize(f32_3 const vec) { return vec * rnorm(vec); }

[[nodiscard]] inline vec3<f64> normalize(f64_3 const vec) { return vec * rnorm(vec); }

////////////////////////////////////////////////////////////////////////////////////////////////////

[[nodiscard]] inline bool are_floats_close_ulp_wise(f32_3 const a,
                                                    f32_3 const b,
                                                    usize const max_ulps = 0u) {
    return are_floats_close_ulp_wise(a.x, b.x, max_ulps) &&
           are_floats_close_ulp_wise(a.y, b.y, max_ulps) &&
           are_floats_close_ulp_wise(a.z, b.z, max_ulps);
}

[[nodiscard]] inline bool are_floats_close_ulp_wise(f64_3 const a,
                                                    f64_3 const b,
                                                    usize const max_ulps = 0u) {
    return are_floats_close_ulp_wise(a.x, b.x, max_ulps) &&
           are_floats_close_ulp_wise(a.y, b.y, max_ulps) &&
           are_floats_close_ulp_wise(a.z, b.z, max_ulps);
}

[[nodiscard]] inline bool
are_floats_close_absolute_value_wise(f32_3 const a,
                                     f32_3 const b,
                                     f32 const absolute_epsilon = meta::limits<f32>::epsilon) {
    return are_floats_close_absolute_value_wise(a.x, b.x, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.y, b.y, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.z, b.z, absolute_epsilon);
}

[[nodiscard]] inline bool
are_floats_close_absolute_value_wise(f64_3 const a,
                                     f64_3 const b,
                                     f64 const absolute_epsilon = meta::limits<f64>::epsilon) {
    return are_floats_close_absolute_value_wise(a.x, b.x, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.y, b.y, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.z, b.z, absolute_epsilon);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename STREAM, typename T> void format(STREAM & entry, vec3<T> const vec) {
    append(entry, "{", vec.x, ", ", vec.y, ", ", vec.z, '}');
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc
