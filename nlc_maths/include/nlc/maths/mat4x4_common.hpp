/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

#include "mat4x4.hpp"
#include "vec2.hpp"
#include "vec3.hpp"
#include "vec4.hpp"
#include "vec4_common.hpp"

namespace nlc {

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T> constexpr mat4x4<T> & operator*=(mat4x4<T> & a, mat4x4<T> const & b) {
    a = a * b;
    return a;
}

template<typename T> constexpr mat4x4<T> operator*(mat4x4<T> const & a, mat4x4<T> const & b) {
    mat4x4<T> result;
    for (usize i = 0; i < 4; ++i)
        for (usize j = 0; j < 4; ++j)
            result[i][j] = dot({ a[0][j], a[1][j], a[2][j], a[3][j] }, b[i]);
    return result;
}

template<typename T>
[[nodiscard]] constexpr vec2<T> operator*(mat4x4<T> const & mat, vec2<T> const vec) {
    return { mat[0].x * vec.x + mat[1].x * vec.y + mat[2].x + mat[3].x,
             mat[0].y * vec.x + mat[1].y * vec.y + mat[2].y + mat[3].y };
}

template<typename T>
[[nodiscard]] constexpr vec2<T> operator*(vec2<T> const vec, mat4x4<T> const & mat) {
    return { mat[0].x * vec.x + mat[0].y * vec.y + mat[0].z + mat[0].w,
             mat[1].x * vec.x + mat[1].y * vec.y + mat[1].z + mat[0].w };
}

template<typename T>
[[nodiscard]] constexpr vec3<T> operator*(mat4x4<T> const & mat, vec3<T> const vec) {
    return { mat[0].x * vec.x + mat[1].x * vec.y + mat[2].x * vec.z + mat[3].x,
             mat[0].y * vec.x + mat[1].y * vec.y + mat[2].y * vec.z + mat[3].y,
             mat[0].z * vec.x + mat[1].z * vec.y + mat[2].z * vec.z + mat[3].z };
}

template<typename T>
[[nodiscard]] constexpr vec3<T> operator*(vec3<T> const vec, mat4x4<T> const & mat) {
    return { mat[0].x * vec.x + mat[0].y * vec.y + mat[0].z * vec.z + mat[0].w,
             mat[1].x * vec.x + mat[1].y * vec.y + mat[1].z * vec.z + mat[1].w,
             mat[2].x * vec.x + mat[2].y * vec.y + mat[2].z * vec.z + mat[2].w };
}

template<typename T>
[[nodiscard]] constexpr vec4<T> operator*(mat4x4<T> const & mat, vec4<T> const vec) {
    return { dot(vec, { mat[0].x, mat[1].x, mat[2].x, mat[3].x }),
             dot(vec, { mat[0].y, mat[1].y, mat[2].y, mat[3].y }),
             dot(vec, { mat[0].z, mat[1].z, mat[2].z, mat[3].z }),
             dot(vec, { mat[0].w, mat[1].w, mat[2].w, mat[3].w }) };
}

template<typename T>
[[nodiscard]] constexpr vec4<T> operator*(vec4<T> const vec, mat4x4<T> const & mat) {
    return { dot(mat[0], vec), dot(mat[1], vec), dot(mat[2], vec), dot(mat[3], vec) };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T> [[nodiscard]] constexpr mat4x4<T> transpose(mat4x4<T> const & mat) {
    return { { mat[0][0], mat[1][0], mat[2][0], mat[3][0] },
             { mat[0][1], mat[1][1], mat[2][1], mat[3][1] },
             { mat[0][2], mat[1][2], mat[2][2], mat[3][2] },
             { mat[0][3], mat[1][3], mat[2][3], mat[3][3] } };
}

template<typename T> [[nodiscard]] constexpr meta::make_signed<T> det(mat4x4<T> const & mat) {
    return det(mat[0], mat[1], mat[2], mat[3]);
}

template<typename T> [[nodiscard]] constexpr mat4x4<T> inverse(mat4x4<T> const & mat) {
    auto const d = det(mat);

#define GENERATE_FOR_ONE_AXIS(name, first, second, third, sign_even, sign_odd)                     \
    auto const x_##name = nlc_swizzle(mat[0], first, second, third);                               \
    auto const y_##name = nlc_swizzle(mat[1], first, second, third);                               \
    auto const z_##name = nlc_swizzle(mat[2], first, second, third);                               \
    auto const w_##name = nlc_swizzle(mat[3], first, second, third);                               \
    vec4<T> const name##_axis = { static_cast<T>(det(y_##name, z_##name, w_##name) / sign_even d), \
                                  static_cast<T>(det(x_##name, z_##name, w_##name) / sign_odd d),  \
                                  static_cast<T>(det(x_##name, y_##name, w_##name) / sign_even d), \
                                  static_cast<T>(det(x_##name, y_##name, z_##name) / sign_odd d) };

    GENERATE_FOR_ONE_AXIS(x, y, z, w, , -)
    GENERATE_FOR_ONE_AXIS(y, x, z, w, -, )
    GENERATE_FOR_ONE_AXIS(z, x, y, w, , -)
    GENERATE_FOR_ONE_AXIS(w, x, y, z, -, )
#undef GENERATE_FOR_ONE_AXIS

    return { x_axis, y_axis, z_axis, w_axis };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

[[nodiscard]] inline bool are_floats_close_ulp_wise(f32_4x4 const & a,
                                                    f32_4x4 const & b,
                                                    usize const max_ulps = 0u) {
    return are_floats_close_ulp_wise(a.x_axis, b.x_axis, max_ulps) &&
           are_floats_close_ulp_wise(a.y_axis, b.y_axis, max_ulps) &&
           are_floats_close_ulp_wise(a.z_axis, b.z_axis, max_ulps) &&
           are_floats_close_ulp_wise(a.w_axis, b.w_axis, max_ulps);
}

[[nodiscard]] inline bool are_floats_close_ulp_wise(f64_4x4 const & a,
                                                    f64_4x4 const & b,
                                                    usize const max_ulps = 0u) {
    return are_floats_close_ulp_wise(a.x_axis, b.x_axis, max_ulps) &&
           are_floats_close_ulp_wise(a.y_axis, b.y_axis, max_ulps) &&
           are_floats_close_ulp_wise(a.z_axis, b.z_axis, max_ulps) &&
           are_floats_close_ulp_wise(a.w_axis, b.w_axis, max_ulps);
}

[[nodiscard]] inline bool
are_floats_close_absolute_value_wise(f32_4x4 const & a,
                                     f32_4x4 const & b,
                                     f32 const absolute_epsilon = meta::limits<f32>::epsilon) {
    return are_floats_close_absolute_value_wise(a.x_axis, b.x_axis, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.y_axis, b.y_axis, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.z_axis, b.z_axis, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.w_axis, b.w_axis, absolute_epsilon);
}

[[nodiscard]] inline bool
are_floats_close_absolute_value_wise(f64_4x4 const & a,
                                     f64_4x4 const & b,
                                     f64 const absolute_epsilon = meta::limits<f64>::epsilon) {
    return are_floats_close_absolute_value_wise(a.x_axis, b.x_axis, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.y_axis, b.y_axis, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.z_axis, b.z_axis, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.w_axis, b.w_axis, absolute_epsilon);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc
