/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "float_cmp.hpp"
#include "functions.hpp"
#include "simple_maths.hpp"
#include "vec3.hpp"
#include "vec3_common.hpp"
#include "vec4.hpp"

namespace nlc {

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> [[nodiscard]] inline vec4<T> abs(vec4<T> const vec) {
    return { abs(vec.x), abs(vec.y), abs(vec.z), abs(vec.w) };
}

template<class T> [[nodiscard]] inline vec4<T> sign(vec4<T> const vec) {
    return { sign(vec.x), sign(vec.y), sign(vec.z), sign(vec.w) };
}

template<class T> [[nodiscard]] constexpr vec4<T> min(vec4<T> const a, vec4<T> const b) {
    return { min(a.x, b.x), min(a.y, b.y), min(a.z, b.z), min(a.w, b.w) };
}

template<class T> [[nodiscard]] constexpr vec4<T> max(vec4<T> const a, vec4<T> const b) {
    return { max(a.x, b.x), max(a.y, b.y), max(a.z, b.z), max(a.w, b.w) };
}

template<class T>
[[nodiscard]] constexpr vec4<T> clamp(vec4<T> const vec, vec4<T> const vmin, vec4<T> const vmax) {
    return { clamp(vec.x, vmin.x, vmax.x),
             clamp(vec.y, vmin.y, vmax.y),
             clamp(vec.z, vmin.z, vmax.z),
             clamp(vec.w, vmin.w, vmax.w) };
}

template<class T> [[nodiscard]] constexpr T sum(vec4<T> const vec) {
    return static_cast<T>(vec.x + vec.y + vec.z + vec.w);
}

template<class T> [[nodiscard]] constexpr T sqrnorm(vec4<T> const vec) {
    return static_cast<T>(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z + vec.w * vec.w);
}

template<class T> [[nodiscard]] constexpr T dot(vec4<T> const a, vec4<T> const b) {
    return static_cast<T>(a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w);
}

template<class T>
[[nodiscard]] constexpr meta::make_signed<T>
det(vec4<T> const a, vec4<T> const b, vec4<T> const c, vec4<T> const d) {
    vec3<T> const x_axis { b.x, c.x, d.x };
    vec3<T> const y_axis { b.y, c.y, d.y };
    vec3<T> const z_axis { b.z, c.z, d.z };
    vec3<T> const w_axis { b.w, c.w, d.w };

    using S = meta::make_signed<T>;
    return static_cast<S>(static_cast<S>(a.x) * det(y_axis, z_axis, w_axis) -
                          static_cast<S>(a.y) * det(x_axis, z_axis, w_axis) +
                          static_cast<S>(a.z) * det(x_axis, y_axis, w_axis) -
                          static_cast<S>(a.w) * det(x_axis, y_axis, z_axis));
}

template<class T> [[nodiscard]] inline T norm(vec4<T> const vec) {
    return static_cast<T>(sqrt(static_cast<f32>(sqrnorm(vec))));
}

template<class T> [[nodiscard]] inline vec4<T> normalize(vec4<T> const vec) {
    return vec / norm(vec);
}

template<class T>
[[nodiscard]] constexpr vec4<meta::make_signed<T>> project(vec4<T> const vec, vec4<T> const normal) {
    using S = meta::make_signed<T>;
    return vec4<S>::cast(vec) - vec4<S>::cast(normal * dot(vec, normal));
}

template<class T>
[[nodiscard]] constexpr vec4<meta::make_signed<T>> reflect(vec4<T> const vec, vec4<T> const normal) {
    using S = meta::make_signed<T>;
    return vec4<S>::cast(vec) - vec4<S>::cast(normal) * dot(vec, normal * static_cast<T>(2));
}

template<class T>
[[nodiscard]] inline vec4<meta::make_signed<T>> normalize_project(vec4<T> const vec,
                                                                  vec4<T> const normal) {
    return project(vec, normalize(normal));
}

template<class T>
[[nodiscard]] inline vec4<meta::make_signed<T>> normalize_reflect(vec4<T> const vec,
                                                                  vec4<T> const normal) {
    return reflect(vec, normalize(normal));
}

template<class T> [[nodiscard]] inline T sqrdistance(vec4<T> const a, vec4<T> const b) {
    if constexpr (meta::is_unsigned_integer<T>) {
        auto const x = a.x > b.x ? a.x - b.x : b.x - a.x;
        auto const y = a.y > b.y ? a.y - b.y : b.y - a.y;
        auto const z = a.z > b.z ? a.z - b.z : b.z - a.z;
        auto const w = a.w > b.w ? a.w - b.w : b.w - a.w;
        return static_cast<T>(x * x + y * y + z * z + w * w);
    } else {
        return sqrnorm(a - b);
    }
}

template<class T> [[nodiscard]] inline T distance(vec4<T> const a, vec4<T> const b) {
    return static_cast<T>(sqrt(static_cast<f32>(sqrdistance(a, b))));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T>
[[nodiscard]] inline auto scalar_lerp(vec4<T> const from, vec4<T> const to, f32 const t) -> vec4<T> {
    return vec4<T>::cast(lerp(f32_4::cast(from), f32_4::cast(to), t));
}

template<typename T>
[[nodiscard]] inline auto scalar_lerp(vec4<T> const from, vec4<T> const to, f64 const t) -> vec4<T> {
    return vec4<T>::cast(lerp(f64_4::cast(from), f64_4::cast(to), t));
}

////////////////////////////////////////////////////////////////////////////////////////////////////

[[nodiscard]] inline f32 rnorm(f32_4 const vec) {
    return static_cast<f32>(rsqrt(static_cast<f32>(sqrnorm(vec))));
}

[[nodiscard]] inline f64 rnorm(f64_4 const vec) {
    return static_cast<f64>(rsqrt(static_cast<f32>(sqrnorm(vec))));
}

[[nodiscard]] inline vec4<f32> normalize(f32_4 const vec) { return vec * rnorm(vec); }

[[nodiscard]] inline vec4<f64> normalize(f64_4 const vec) { return vec * rnorm(vec); }

////////////////////////////////////////////////////////////////////////////////////////////////////

[[nodiscard]] inline bool are_floats_close_ulp_wise(f32_4 const a,
                                                    f32_4 const b,
                                                    usize const max_ulps = 0u) {
    return are_floats_close_ulp_wise(a.x, b.x, max_ulps) &&
           are_floats_close_ulp_wise(a.y, b.y, max_ulps) &&
           are_floats_close_ulp_wise(a.z, b.z, max_ulps) &&
           are_floats_close_ulp_wise(a.w, b.w, max_ulps);
}

[[nodiscard]] inline bool are_floats_close_ulp_wise(f64_4 const a,
                                                    f64_4 const b,
                                                    usize const max_ulps = 0u) {
    return are_floats_close_ulp_wise(a.x, b.x, max_ulps) &&
           are_floats_close_ulp_wise(a.y, b.y, max_ulps) &&
           are_floats_close_ulp_wise(a.z, b.z, max_ulps) &&
           are_floats_close_ulp_wise(a.w, b.w, max_ulps);
}

[[nodiscard]] inline bool
are_floats_close_absolute_value_wise(f32_4 const a,
                                     f32_4 const b,
                                     f32 const absolute_epsilon = meta::limits<f32>::epsilon) {
    return are_floats_close_absolute_value_wise(a.x, b.x, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.y, b.y, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.z, b.z, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.w, b.w, absolute_epsilon);
}

[[nodiscard]] inline bool
are_floats_close_absolute_value_wise(f64_4 const a,
                                     f64_4 const b,
                                     f64 const absolute_epsilon = meta::limits<f64>::epsilon) {
    return are_floats_close_absolute_value_wise(a.x, b.x, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.y, b.y, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.z, b.z, absolute_epsilon) &&
           are_floats_close_absolute_value_wise(a.w, b.w, absolute_epsilon);
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename STREAM, typename T> void format(STREAM & entry, vec4<T> const vec) {
    append(entry, "{", vec.x, ", ", vec.y, ", ", vec.z, ", ", vec.w, '}');
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc
