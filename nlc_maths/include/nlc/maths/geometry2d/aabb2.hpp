/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/span.hpp>

#include "../simple_maths.hpp"
#include "../vec2.hpp"
#include "../vec2_common.hpp"
#include "../vec4.hpp"

namespace nlc {

// This struct represents a simple axis-aligned bouding box.
struct aabb2 final {
    f32_2 min { 0.f, 0.f };
    f32_2 max { 0.f, 0.f };

    static constexpr auto from_f32_4(f32_4 const vec4) -> aabb2 {
        return aabb2 { { vec4.x, vec4.y }, { vec4.z, vec4.w } };
    }

    static constexpr auto from_pos_and_size(f32_2 const position, f32_2 const aabb_size) -> aabb2 {
        return { position, position + aabb_size };
    }

    static constexpr auto from_centered_pos_and_size(f32_2 const position,
                                                     f32_2 const aabb_size) -> aabb2 {
        auto const half_size = aabb_size * 0.5f;
        return { position - half_size, position + half_size };
    }

    static auto from_points(span<f32_2 const> const points) -> aabb2;

    constexpr auto operator==(aabb2 const & rhs) const -> bool {
        return min == rhs.min && max == rhs.max;
    }

    constexpr auto operator!=(aabb2 const & rhs) const -> bool { return !operator==(rhs); }

    constexpr auto is_valid() const -> bool { return min.x < max.x && min.y < max.y; }
    constexpr auto is_empty() const -> bool { return perimeter() == 0.0f; }
    constexpr auto center() const -> f32_2 { return (min + max) * 0.5f; }
    constexpr auto size() const -> f32_2 { return max - min; }

    constexpr auto surface_area() const -> float {
        auto const aabb_size { max - min };
        return aabb_size.x * aabb_size.y;
    }

    constexpr auto perimeter() const -> float {
        auto const aabb_size { max - min };
        return aabb_size.x + aabb_size.y;
    }

    constexpr auto contains(f32_2 const point) const -> bool {
        return point.x >= min.x && point.x <= max.x && point.y >= min.y && point.y <= max.y;
    }

    constexpr auto contains(aabb2 const & aabb) const -> bool {
        return aabb.min.x >= min.x && aabb.max.x <= max.x && aabb.min.y >= min.y &&
               aabb.max.y <= max.y;
    }

    constexpr auto overlaps(aabb2 const & aabb) const -> bool {
        return aabb.min.x <= max.x && aabb.max.x >= min.x && aabb.min.y >= max.y &&
               aabb.max.y <= min.y;
    }

    constexpr auto intersects_ray(f32_2 const origin,
                                  f32_2 const direction,
                                  float & tnear,
                                  float & tfar) const -> bool {
        f32_2 const tbot = (min - origin) * direction;
        f32_2 const ttop = (max - origin) * direction;
        f32_2 const tmin = ::nlc::min(tbot, ttop);
        f32_2 const tmax = ::nlc::max(tbot, ttop);

        tnear = ::nlc::max(tmin.x, tmin.y);
        tfar = ::nlc::min(tmax.x, tmax.y);

        return (tfar > 0.0f && tnear <= tfar);
    }

    auto distance(f32_2 const point) const -> float {
        f32_2 const closest_point = ::nlc::clamp(point, min, max);
        return ::nlc::distance(point, closest_point);
    }

    static constexpr auto merge(aabb2 const & lhs, aabb2 const & rhs) -> aabb2 {
        return { ::nlc::min(lhs.min, rhs.min), ::nlc::max(lhs.max, rhs.max) };
    }

    static auto merge(span<aabb2 const> const aabbs) -> aabb2;
};

}  // namespace nlc
