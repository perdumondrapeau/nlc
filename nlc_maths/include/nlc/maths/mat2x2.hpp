/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

#include "vec2.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace nlc {
template<typename T> struct mat2x2 final {
    vec2<T> x_axis;
    vec2<T> y_axis;

    [[nodiscard]] static constexpr mat2x2 identity() { return { { 1, 0 }, { 0, 1 } }; }

    [[nodiscard]] constexpr vec2<T> const operator[](usize const i) const { return (&x_axis)[i]; }
    [[nodiscard]] constexpr vec2<T> & operator[](usize const i) { return (&x_axis)[i]; }
};

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T>
[[nodiscard]] constexpr bool operator==(mat2x2<T> const & a, mat2x2<T> const & b) {
    return a.x_axis == b.x_axis && a.y_axis == b.y_axis;
}

template<class T>
[[nodiscard]] constexpr bool operator!=(mat2x2<T> const & a, mat2x2<T> const & b) {
    return a.x_axis != b.x_axis || a.y_axis != b.y_axis;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc

using i8_2x2 = nlc::mat2x2<i8>;
using u8_2x2 = nlc::mat2x2<u8>;

using i16_2x2 = nlc::mat2x2<i16>;
using u16_2x2 = nlc::mat2x2<u16>;

using i32_2x2 = nlc::mat2x2<i32>;
using u32_2x2 = nlc::mat2x2<u32>;

using i64_2x2 = nlc::mat2x2<i64>;
using u64_2x2 = nlc::mat2x2<u64>;

using isize_2x2 = nlc::mat2x2<isize>;
using usize_2x2 = nlc::mat2x2<usize>;

using f32_2x2 = nlc::mat2x2<f32>;
using f64_2x2 = nlc::mat2x2<f64>;
