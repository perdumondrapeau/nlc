/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include "impl/swizzling.hpp"

namespace nlc {

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> struct vec3 final {
    T x;
    T y;
    T z;

    template<typename U> static constexpr vec3 cast(vec3<U> const vec) {
        return { static_cast<T>(vec.x), static_cast<T>(vec.y), static_cast<T>(vec.z) };
    }

    [[nodiscard]] constexpr T const operator[](usize const i) const { return (&x)[i]; }
    [[nodiscard]] constexpr T & operator[](usize const i) { return (&x)[i]; }

    constexpr vec3 & operator++();
    constexpr vec3 operator++(int);
    constexpr vec3 & operator--();
    constexpr vec3 operator--(int);

    constexpr vec3 & operator+=(vec3 const);
    constexpr vec3 & operator-=(vec3 const);
    constexpr vec3 & operator*=(vec3 const);
    constexpr vec3 & operator/=(vec3 const);
    constexpr vec3 & operator%=(vec3 const);
    constexpr vec3 & operator|=(vec3 const);
    constexpr vec3 & operator&=(vec3 const);
    constexpr vec3 & operator^=(vec3 const);
    constexpr vec3 & operator<<=(vec3 const);  // /!\ beware, values must not be negative
    constexpr vec3 & operator>>=(vec3 const);  // /!\ beware, values must not be negative

    constexpr vec3 & operator+=(T const);
    constexpr vec3 & operator-=(T const);
    constexpr vec3 & operator*=(T const);
    constexpr vec3 & operator/=(T const);
    constexpr vec3 & operator%=(T const);
    constexpr vec3 & operator|=(T const);
    constexpr vec3 & operator&=(T const);
    constexpr vec3 & operator^=(T const);
    constexpr vec3 & operator<<=(T const);  // /!\ beware, values must not be negative
    constexpr vec3 & operator>>=(T const);  // /!\ beware, values must not be negative
};

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> constexpr vec3<T> & vec3<T>::operator++() {
    ++x;
    ++y;
    ++z;
    return *this;
}

template<class T> constexpr vec3<T> vec3<T>::operator++(int) {
    return { static_cast<T>(x++), static_cast<T>(y++), static_cast<T>(z++) };
}

template<class T> constexpr vec3<T> & vec3<T>::operator--() {
    --x;
    --y;
    --z;
    return *this;
}

template<class T> constexpr vec3<T> vec3<T>::operator--(int) {
    return { static_cast<T>(x--), static_cast<T>(y--), static_cast<T>(z--) };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> [[nodiscard]] constexpr vec3<T> operator+(vec3<T> const vec) {
    return { static_cast<T>(+vec.x), static_cast<T>(+vec.y), static_cast<T>(+vec.z) };
}

template<class T> [[nodiscard]] constexpr vec3<T> operator-(vec3<T> const vec) {
    return { static_cast<T>(-vec.x), static_cast<T>(-vec.y), static_cast<T>(-vec.z) };
}

template<class T> [[nodiscard]] constexpr vec3<T> operator~(vec3<T> const vec) {
    return { static_cast<T>(~vec.x), static_cast<T>(~vec.y), static_cast<T>(~vec.z) };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

#if (defined(__GNUC__) && !defined(__clang__))
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wconversion"
#endif

#define IMPLEMENT_OPERATOR(OP)                                                        \
    template<class T> constexpr vec3<T> & vec3<T>::operator OP(vec3<T> const other) { \
        x OP other.x;                                                                 \
        y OP other.y;                                                                 \
        z OP other.z;                                                                 \
        return *this;                                                                 \
    }                                                                                 \
    template<class T> constexpr vec3<T> & vec3<T>::operator OP(T const scalar) {      \
        x OP scalar;                                                                  \
        y OP scalar;                                                                  \
        z OP scalar;                                                                  \
        return *this;                                                                 \
    }

IMPLEMENT_OPERATOR(+=)
IMPLEMENT_OPERATOR(-=)
IMPLEMENT_OPERATOR(*=)
IMPLEMENT_OPERATOR(/=)
IMPLEMENT_OPERATOR(%=)
IMPLEMENT_OPERATOR(|=)
IMPLEMENT_OPERATOR(&=)
IMPLEMENT_OPERATOR(^=)
IMPLEMENT_OPERATOR(<<=)  // /!\ beware, values must not be negative
IMPLEMENT_OPERATOR(>>=)  // /!\ beware, values must not be negative

#undef IMPLEMENT_OPERATOR

#if (defined(__GNUC__) && !defined(__clang__))
#    pragma GCC diagnostic pop
#endif

////////////////////////////////////////////////////////////////////////////////////////////////////

#define IMPLEMENT_OPERATOR(OP)                                                                         \
    template<class T>                                                                                  \
    [[nodiscard]] constexpr vec3<T> operator OP(vec3<T> const a, vec3<T> const b) {                    \
        return { static_cast<T>(a.x OP b.x), static_cast<T>(a.y OP b.y), static_cast<T>(a.z OP b.z) }; \
    }                                                                                                  \
    template<class T>                                                                                  \
    [[nodiscard]] constexpr vec3<T> operator OP(vec3<T> const vec, T const scalar) {                   \
        return { static_cast<T>(vec.x OP scalar),                                                      \
                 static_cast<T>(vec.y OP scalar),                                                      \
                 static_cast<T>(vec.z OP scalar) };                                                    \
    }                                                                                                  \
    template<class T>                                                                                  \
    [[nodiscard]] constexpr vec3<T> operator OP(T const scalar, vec3<T> const vec) {                   \
        return { static_cast<T>(scalar OP vec.x),                                                      \
                 static_cast<T>(scalar OP vec.y),                                                      \
                 static_cast<T>(scalar OP vec.z) };                                                    \
    }

IMPLEMENT_OPERATOR(+)
IMPLEMENT_OPERATOR(-)
IMPLEMENT_OPERATOR(*)
IMPLEMENT_OPERATOR(/)
IMPLEMENT_OPERATOR(%)
IMPLEMENT_OPERATOR(|)
IMPLEMENT_OPERATOR(&)
IMPLEMENT_OPERATOR(^)
IMPLEMENT_OPERATOR(<<)  // /!\ beware, values must not be negative
IMPLEMENT_OPERATOR(>>)  // /!\ beware, values must not be negative

#undef IMPLEMENT_OPERATOR

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T> [[nodiscard]] constexpr bool operator==(vec3<T> const a, vec3<T> const b) {
    return a.x == b.x && a.y == b.y && a.z == b.z;
}

template<class T> [[nodiscard]] constexpr bool operator!=(vec3<T> const a, vec3<T> const b) {
    return a.x != b.x || a.y != b.y || a.z != b.z;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<vec_component_indices first, vec_component_indices second, vec_component_indices third, typename T>
[[nodiscard]] constexpr auto swizzle(T const vec) {
    return vec3<decltype(vec.x)> { vec[static_cast<usize>(first)],
                                   vec[static_cast<usize>(second)],
                                   vec[static_cast<usize>(third)] };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc

using i8_3 = nlc::vec3<i8>;
using u8_3 = nlc::vec3<u8>;

using i16_3 = nlc::vec3<i16>;
using u16_3 = nlc::vec3<u16>;

using i32_3 = nlc::vec3<i32>;
using u32_3 = nlc::vec3<u32>;

using i64_3 = nlc::vec3<i64>;
using u64_3 = nlc::vec3<u64>;

using isize_3 = nlc::vec3<isize>;
using usize_3 = nlc::vec3<usize>;

using f32_3 = nlc::vec3<f32>;
using f64_3 = nlc::vec3<f64>;
