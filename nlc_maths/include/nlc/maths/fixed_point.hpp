/*  Copyright © 2022-2023
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

#include <nlc/dialect/basic_output.hpp>

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace nlc::meta {
template<typename T> struct limits;
}  // namespace nlc::meta

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace nlc::impl {
template<typename T> struct upper_type_impl;
template<> struct upper_type_impl<i8> final {
    using type = i16;
};
template<> struct upper_type_impl<i16> final {
    using type = i32;
};
template<> struct upper_type_impl<i32> final {
    using type = i64;
};
template<> struct upper_type_impl<u8> final {
    using type = u16;
};
template<> struct upper_type_impl<u16> final {
    using type = u32;
};
template<> struct upper_type_impl<u32> final {
    using type = u64;
};
template<typename T> using upper_type = typename upper_type_impl<T>::type;
}  // namespace nlc::impl

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace nlc {

template<typename T, u8 exponent> struct fixed_point final {
    static constexpr u8 half_size = sizeof(T) << 2;
    static constexpr T integer_mask = ~static_cast<T>(0) << exponent;
    static constexpr T fraction_mask = ~integer_mask;
    static constexpr T higher_mask = ~static_cast<T>(0) << half_size;
    static constexpr T lower_mask = ~higher_mask;
    static constexpr T sign_mask = 1 << (sizeof(T) * 8 - 1);

    T _mantissa;

    constexpr fixed_point() = default;
    constexpr fixed_point(fixed_point const &) = default;
    constexpr fixed_point(fixed_point &&) = default;

    constexpr fixed_point & operator=(fixed_point const &) = default;
    constexpr fixed_point & operator=(fixed_point &&) = default;

    template<typename U> [[nodiscard]] static constexpr auto from(U const value) -> fixed_point {
        return { static_cast<T>(value * static_cast<U>(static_cast<T>(1) << exponent)) };
    }

    template<typename U> [[nodiscard]] constexpr auto to() const -> U {
        return static_cast<U>(_mantissa) / static_cast<U>(static_cast<T>(1) << exponent);
    }

    [[nodiscard]] constexpr fixed_point<T, exponent> operator++();
    [[nodiscard]] constexpr fixed_point<T, exponent> operator++(int);
    [[nodiscard]] constexpr fixed_point<T, exponent> operator--();
    [[nodiscard]] constexpr fixed_point<T, exponent> operator--(int);

    constexpr fixed_point & operator+=(fixed_point const);
    constexpr fixed_point & operator-=(fixed_point const);
    constexpr fixed_point & operator*=(fixed_point const);
    constexpr fixed_point & operator/=(fixed_point const);

    template<typename U, u8 exp>
    friend constexpr auto floor(fixed_point<U, exp> const) -> fixed_point<U, exp>;
    template<typename U, u8 exp>
    friend constexpr auto ceil(fixed_point<U, exp> const) -> fixed_point<U, exp>;
    template<typename U, u8 exp>
    friend constexpr auto trunc(fixed_point<U, exp> const) -> fixed_point<U, exp>;
    template<typename U, u8 exp>
    friend constexpr auto round(fixed_point<U, exp> const) -> fixed_point<U, exp>;

    template<typename U, u8 exp>
    friend constexpr auto operator+(fixed_point<U, exp> const a,
                                    fixed_point<U, exp> const b) -> fixed_point<U, exp>;
    template<typename U, u8 exp>
    friend constexpr auto operator-(fixed_point<U, exp> const a,
                                    fixed_point<U, exp> const b) -> fixed_point<U, exp>;
    template<typename U, u8 exp>
    friend constexpr auto operator*(fixed_point<U, exp> const a,
                                    fixed_point<U, exp> const b) -> fixed_point<U, exp>;
    template<typename U, u8 exp>
    friend constexpr auto operator/(fixed_point<U, exp> const a,
                                    fixed_point<U, exp> const b) -> fixed_point<U, exp>;

    template<typename U> friend struct meta::limits;

  private:
    constexpr fixed_point(T const value)
        : _mantissa(value) {}
};

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, u8 exponent>
[[nodiscard]] constexpr fixed_point<T, exponent> fixed_point<T, exponent>::operator++() {
    _mantissa += 1 << exponent;
    return *this;
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr fixed_point<T, exponent> fixed_point<T, exponent>::operator++(int) {
    auto const temp = *this;
    _mantissa += 1 << exponent;
    return temp;
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr fixed_point<T, exponent> fixed_point<T, exponent>::operator--() {
    _mantissa -= 1 << exponent;
    return *this;
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr fixed_point<T, exponent> fixed_point<T, exponent>::operator--(int) {
    auto const temp = *this;
    _mantissa -= 1 << exponent;
    return temp;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator+(fixed_point<T, exponent> const fp) -> fixed_point<T, exponent> {
    return { static_cast<T>(+fp._mantissa) };
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator-(fixed_point<T, exponent> const fp) -> fixed_point<T, exponent> {
    return { static_cast<T>(-fp._mantissa) };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, u8 exponent>
constexpr auto fixed_point<T, exponent>::operator+=(fixed_point<T, exponent> const fp)
    -> fixed_point<T, exponent> & {
    _mantissa += fp._mantissa;
    return *this;
}

template<typename T, u8 exponent>
constexpr auto fixed_point<T, exponent>::operator-=(fixed_point<T, exponent> const fp)
    -> fixed_point<T, exponent> & {
    _mantissa -= fp._mantissa;
    return *this;
}

template<typename T, u8 exponent>
constexpr auto fixed_point<T, exponent>::operator*=(fixed_point<T, exponent> const fp)
    -> fixed_point<T, exponent> & {
    *this = *this * fp;
    return *this;
}

template<typename T, u8 exponent>
constexpr auto fixed_point<T, exponent>::operator/=(fixed_point<T, exponent> const fp)
    -> fixed_point<T, exponent> & {
    *this = *this / fp;
    return *this;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator+(fixed_point<T, exponent> const a,
                                       fixed_point<T, exponent> const b) -> fixed_point<T, exponent> {
    return { a._mantissa + b._mantissa };
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator-(fixed_point<T, exponent> const a,
                                       fixed_point<T, exponent> const b) -> fixed_point<T, exponent> {
    return { a._mantissa - b._mantissa };
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator*(fixed_point<T, exponent> const a,
                                       fixed_point<T, exponent> const b) -> fixed_point<T, exponent> {
    if constexpr (sizeof(T) < 8) {
        auto const result = (static_cast<impl::upper_type<T>>(a._mantissa) *
                             static_cast<impl::upper_type<T>>(b._mantissa)) >>
                            exponent;
        return { static_cast<T>(result) };
    } else {
        auto const higher_a = a._mantissa >> fixed_point<T, exponent>::half_size;
        auto const lower_a = a._mantissa & fixed_point<T, exponent>::lower_mask;
        auto const higher_b = b._mantissa >> fixed_point<T, exponent>::half_size;
        auto const lower_b = b._mantissa & fixed_point<T, exponent>::lower_mask;

        return { (higher_a * higher_b << fixed_point<T, exponent>::half_size) + higher_a * lower_b +
                 lower_a * higher_b + (lower_a * lower_b >> fixed_point<T, exponent>::half_size) };
    }
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator/(fixed_point<T, exponent> const a,
                                       fixed_point<T, exponent> const b) -> fixed_point<T, exponent> {
    static_assert(sizeof(T) < 8, "division are not supported on type bigger than 32bits");
    auto const result = (static_cast<impl::upper_type<T>>(a._mantissa) << exponent) /
                        static_cast<impl::upper_type<T>>(b._mantissa);
    return { static_cast<T>(result) };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator==(fixed_point<T, exponent> const a,
                                        fixed_point<T, exponent> const b) -> bool {
    return a._mantissa == b._mantissa;
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator!=(fixed_point<T, exponent> const a,
                                        fixed_point<T, exponent> const b) -> bool {
    return a._mantissa != b._mantissa;
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator<(fixed_point<T, exponent> const a,
                                       fixed_point<T, exponent> const b) -> bool {
    return a._mantissa < b._mantissa;
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator<=(fixed_point<T, exponent> const a,
                                        fixed_point<T, exponent> const b) -> bool {
    return a._mantissa <= b._mantissa;
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator>(fixed_point<T, exponent> const a,
                                       fixed_point<T, exponent> const b) -> bool {
    return a._mantissa > b._mantissa;
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto operator>=(fixed_point<T, exponent> const a,
                                        fixed_point<T, exponent> const b) -> bool {
    return a._mantissa >= b._mantissa;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto floor(fixed_point<T, exponent> const x) -> fixed_point<T, exponent> {
    return { x._mantissa & fixed_point<T, exponent>::integer_mask };
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto ceil(fixed_point<T, exponent> const x) -> fixed_point<T, exponent> {
    return { (x._mantissa + fixed_point<T, exponent>::fraction_mask) &
             fixed_point<T, exponent>::integer_mask };
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto trunc(fixed_point<T, exponent> const x) -> fixed_point<T, exponent> {
    if (x._mantissa < 0)
        return { (x._mantissa + fixed_point<T, exponent>::fraction_mask) &
                 fixed_point<T, exponent>::integer_mask };
    else
        return { x._mantissa & fixed_point<T, exponent>::integer_mask };
}

template<typename T, u8 exponent>
[[nodiscard]] constexpr auto round(fixed_point<T, exponent> const x) -> fixed_point<T, exponent> {
    return { (x._mantissa + (fixed_point<T, exponent>::fraction_mask >> 1)) &
             fixed_point<T, exponent>::integer_mask };
}

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename STREAM, typename T, u8 exponent>
void format(STREAM & entry, fixed_point<T, exponent> const fp) {
    append(entry, fp.template to<f64>());
}

}  // namespace nlc
