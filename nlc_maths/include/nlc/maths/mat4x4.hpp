/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

#include "vec4.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace nlc {
template<typename T> struct mat4x4 final {
    vec4<T> x_axis;
    vec4<T> y_axis;
    vec4<T> z_axis;
    vec4<T> w_axis;

    [[nodiscard]] static constexpr mat4x4 identity() {
        return { { 1, 0, 0, 0 }, { 0, 1, 0, 0 }, { 0, 0, 1, 0 }, { 0, 0, 0, 1 } };
    }

    [[nodiscard]] constexpr vec4<T> const operator[](usize const i) const { return (&x_axis)[i]; }
    [[nodiscard]] constexpr vec4<T> & operator[](usize const i) { return (&x_axis)[i]; }
};

////////////////////////////////////////////////////////////////////////////////////////////////////

template<class T>
[[nodiscard]] constexpr bool operator==(mat4x4<T> const & a, mat4x4<T> const & b) {
    return a.x_axis == b.x_axis && a.y_axis == b.y_axis && a.z_axis == b.z_axis && a.w_axis == b.w_axis;
}

template<class T>
[[nodiscard]] constexpr bool operator!=(mat4x4<T> const & a, mat4x4<T> const & b) {
    return a.x_axis != b.x_axis || a.y_axis != b.y_axis || a.z_axis != b.z_axis || a.w_axis != b.w_axis;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc

using i8_4x4 = nlc::mat4x4<i8>;
using u8_4x4 = nlc::mat4x4<u8>;

using i16_4x4 = nlc::mat4x4<i16>;
using u16_4x4 = nlc::mat4x4<u16>;

using i32_4x4 = nlc::mat4x4<i32>;
using u32_4x4 = nlc::mat4x4<u32>;

using i64_4x4 = nlc::mat4x4<i64>;
using u64_4x4 = nlc::mat4x4<u64>;

using isize_4x4 = nlc::mat4x4<isize>;
using usize_4x4 = nlc::mat4x4<usize>;

using f32_4x4 = nlc::mat4x4<f32>;
using f64_4x4 = nlc::mat4x4<f64>;
