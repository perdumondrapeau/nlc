/*  Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

// /!\ If you make a modification in this file, test your work on godbolt with x64, ARM, clang, gcc
// All function should be constexpr we wait c++23 to be able to do that

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/meta/numeric_limits.hpp>
#include <nlc/meta/traits.hpp>

namespace nlc {

////////////////////////////////////////////////////////////////////////////////////////////////////

#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wunused-value"
#endif
template<typename... Args> constexpr auto min(Args &&... values) {
    auto min = (values, ...);
    ((values < min ? min = values : 0), ...);
    return min;
}

template<typename... Args> constexpr auto max(Args &&... values) {
    auto max = (values, ...);
    ((max < values ? max = values : 0), ...);
    return max;
}
#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic pop
#endif

template<typename T> constexpr auto min(T const a, T const b) -> T { return a > b ? b : a; }
template<typename T> constexpr auto max(T const a, T const b) -> T { return a < b ? b : a; }

template<typename T> inline auto clamp(T const v, T const min_v, T const max_v) -> T {
    nlc_assert(min_v <= max_v);
    return min(max(v, min_v), max_v);
}

template<typename T> constexpr auto abs(T const v) -> T {
    return static_cast<T>(v < static_cast<T>(0) ? -v : v);
}

template<typename T> constexpr auto sign(T const v) -> T {
    return static_cast<T>((static_cast<T>(0) < v) - (v < static_cast<T>(0)));
}

template<typename T> constexpr auto copysign(T const dst, T const src) -> T {
    return static_cast<T>(src < static_cast<T>(0) ? -abs(dst) : abs(dst));
}

template<typename T, typename U> constexpr auto lerp(T const from, T const to, U const t) -> T {
    return static_cast<T>(from + t * (to - from));
}

template<typename T> constexpr auto is_finite(T const v) -> bool {
    if constexpr (nlc::meta::is_floating<T>)
        return nlc::meta::limits<T>::is_finite(v);
    else
        return nlc::meta::is_arithmetic<T>;
}

inline auto floor(f32 const x) -> f32 { return static_cast<f32>(__builtin_floorf(x)); }

inline auto ceil(f32 const x) -> f32 { return static_cast<f32>(__builtin_ceilf(x)); }

inline auto trunc(f32 const x) -> f32 { return static_cast<f32>(__builtin_truncf(x)); }

inline auto round(f32 const x) -> f32 {
    /*
     * Explicit SSE implementation, but including smmintrin.h is very expansive
    auto temp = _mm_set_ss(x);
    temp = _mm_round_ps(temp, _MM_FROUND_TO_NEAREST_INT);
    return _mm_cvtss_f32(temp);
    */

    // Unfortunately, GCC calls into libc with its builtin roundf.
    return __builtin_roundf(x);
}

inline auto mod(f32 const x, f32 const y) -> f32 { return x - trunc(x / y) * y; }

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace impl {
    union float_bits {
        float value;
        u32 bits;
    };

    union double_bits {
        double value;
        u64 bits;
    };
}  // namespace impl

inline auto abs(float const v) -> float {
    impl::float_bits tmp { .value = v };
    tmp.bits &= 0x7fff'ffff;  // erase bit sign
    return tmp.value;
}

inline auto abs(double const v) -> double {
    impl::double_bits tmp { .value = v };
    tmp.bits &= 0x7fff'ffff'ffff'ffff;  // erase bit sign
    return tmp.value;
}

inline auto sign(float const v) -> float {
    impl::float_bits tmp { .value = v };
    tmp.bits &= 0x8000'0000;  // select bit sign
    tmp.bits |= 0x3f80'0000;  // one
    return tmp.value;
}

inline auto sign(double const v) -> double {
    impl::double_bits tmp { .value = v };
    tmp.bits &= 0x8000'0000'0000'0000;  // select bit sign
    tmp.bits |= 0x3ff0'0000'0000'0000;  // one
    return tmp.value;
}

inline auto copysign(float const dst, float const src) -> float {
    impl::float_bits tmp_dst { .value = dst };
    impl::float_bits tmp_src { .value = src };
    tmp_dst.bits &= 0x7fff'ffff;  // erase bit sign
    tmp_src.bits &= 0x8000'0000;  // select bit sign
    tmp_dst.bits |= tmp_src.bits;
    return tmp_dst.value;
}

inline auto copysign(double const dst, double const src) -> double {
    impl::double_bits tmp_dst { .value = dst };
    impl::double_bits tmp_src { .value = src };
    tmp_dst.bits &= 0x7fff'ffff'ffff'ffff;  // erase bit sign
    tmp_src.bits &= 0x8000'0000'0000'0000;  // select bit sign
    tmp_dst.bits |= tmp_src.bits;
    return tmp_dst.value;
}

////////////////////////////////////////////////////////////////////////////////////////////////////

}  // namespace nlc
