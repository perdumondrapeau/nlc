#!/usr/bin/env bash

# Copyright © 2022-2022
#             Alexis A. D. COLIN,
#             Antoine VUGLIANO,
#             Gaëtan CHAMPARNAUD,
#             Geoffrey L. TOURON,
#             Grégoire A. P. BADIN
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
# - subsequentversions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence
# is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.

include_patterns=(.cpp$ .hpp$ .inl$)
exclude_patterns=(/external/ /subprojects/)
files_to_check=()
discarded_files=()

while IFS= read -r line; do
    if [[ ! $line =~ ^(M|A).(.*)$ ]]; then
        echo "input line '$line' is not valid and will be ignored"
        continue
    fi

    file=${BASH_REMATCH[2]}

    # Avoid false negatives on files that have been moved / deleted
    if [[ ! -f "$file" ]]; then
        if [[ ! " ${discarded_files[*]} " =~ " ${file} " ]]; then
            echo "Discarding '$file' because it does not exist anymore"
            discarded_files+=("$file")
        fi
        continue
    fi

    # Avoid doublons
    if [[ ! " ${files_to_check[*]} " =~ " ${file} " ]]; then
        files_to_check+=("$file")
    fi
done < $1

echo "${#files_to_check[@]} file(s) have been added or modified"

files_to_format=()
for file in "${files_to_check[@]}"; do
    for ex_pat in "${exclude_patterns[@]}"; do
        if [[ $file =~ $ex_pat ]]; then
            # file matches an exclude pattern
            echo "Discarding '$file' because it matches exclude pattern '$ex_pat'"
            continue 2
        fi
    done

    matches=False
    for inc_pat in "${include_patterns[@]}"; do
        if [[ $file =~ $inc_pat ]]; then
            matches=True
            # file matches include pattern and no exlude pattern -> run clang-format
            echo "Checking format of '$file'"
            clang-format --dry-run -Werror $file 2> /dev/null
            if [[ $? != 0 ]]; then
                echo "/!\ not formatted"
                files_to_format+=($file)
            fi
            break
        fi
    done

    if [[ $matches == False ]]; then
        echo "Discarding '$file' because it doesn't match any include pattern from '${include_patterns[*]}'"
    fi
done

if [[ ${#files_to_format[@]} > 0 ]]; then
    echo "${#files_to_format[@]} modified files are not clang-formatted"
    for file in "${files_to_format[@]}"; do
        echo $file
    done
fi

exit ${#files_to_format[@]}
