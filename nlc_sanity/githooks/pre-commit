#!/usr/bin/env bash

# Copyright © 2022-2022
#             Alexis A. D. COLIN,
#             Antoine VUGLIANO,
#             Gaëtan CHAMPARNAUD,
#             Geoffrey L. TOURON,
#             Grégoire A. P. BADIN
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
# - subsequentversions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence
# is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.

# Called by "git commit" with no arguments.  The hook should
# exit with non-zero status after issuing an appropriate message if
# it wants to stop the commit.

if git rev-parse --verify HEAD >/dev/null 2>&1
then
    against=HEAD
else
    # Initial commit: diff against an empty tree object
    against=$(git hash-object -t tree /dev/null)
fi

# Redirect output to stderr.
exec 1>&2

# Look for 'NOCOMMIT' in cached changes
if [ $(git diff --cached $against | grep NOCOMMIT --line-buffered | wc -l) != 0 ]
then
    echo "NOCOMMIT found: "
    for file in $(git diff --cached --name-only $against)
    do
        cat $file | grep NOCOMMIT --label $file --with-filename --line-number
    done
    exit 1
fi

exit 0
