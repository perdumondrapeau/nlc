#!python3

# Copyright © 2022-2022
#             Alexis A. D. COLIN,
#             Antoine VUGLIANO,
#             Gaëtan CHAMPARNAUD,
#             Geoffrey L. TOURON,
#             Grégoire A. P. BADIN
# Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
# - subsequentversions of the EUPL (the "Licence");
# You may not use this work except in compliance with the Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software distributed under the Licence
# is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the Licence for the specific language governing permissions and limitations under the Licence.

import argparse
import logging
import os
import stat
import subprocess
import tempfile


def set_git_hooks_path(repo_path, hooks_path):
    logging.info(
        f'Setting git config value `core.hooksPath` to "{hooks_path}"')
    os.chdir(repo_path)
    subprocess.run(
        ['git', 'config', 'core.hooksPath', hooks_path], check=True)


def add_mode_flags(path, flags):
    combined_flags = os.stat(path).st_mode | flags
    os.chmod(path, combined_flags)


def set_git_hooks_rights(hooks_path):
    logging.info('Giving execution rights to hooks and scripts')
    os.chdir(hooks_path)
    additional_mode_flags = stat.S_IRUSR | stat.S_IXUSR
    add_mode_flags('pre-commit', additional_mode_flags)
    add_mode_flags('pre-push', additional_mode_flags)
    add_mode_flags('validate_clang_format.sh', additional_mode_flags)


# Used to check whether the argument is a directory
def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Simple script to set core.hooksPath config value and give execution rights to hooks and scripts')
    parser.add_argument('--repo-root',
                        type=dir_path,
                        default=[''],
                        help='the root of the git repo')
    parser.add_argument('--log-level', '-l',
                        type=str,
                        nargs='?',
                        choices=['debug', 'info',
                                 'warning', 'error', 'critical'],
                        default='warning',
                        help='the minimal log level that is displayed')
    args = parser.parse_args()

    log_level = getattr(logging, args.log_level.upper(), None)
    logging.basicConfig(
        level=log_level, filename=f'{tempfile.gettempdir()}/githooks_setup.log', filemode='w')

    repo_path = args.repo_root
    hooks_path = os.path.join(repo_path, 'githooks')

    set_git_hooks_path(repo_path, hooks_path)
    set_git_hooks_rights(hooks_path)
    logging.info('Git hooks have been setup !')
