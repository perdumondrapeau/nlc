/* Copyright © 2020-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

// OS defines
#if defined(_WIN64)
#    define NLC_OS_WINDOWS 1
#elif defined(__linux__)
#    define NLC_OS_LINUX 1
#elif defined(__NetBSD__) || defined(__OpenBSD__) || defined(__FreeBSD__)
#    define NLC_OS_BSD 1
#else
#    error "Unsupported OS"
#endif

#ifndef NLC_OS_WINDOWS
#    define NLC_OS_WINDOWS 0
#endif
#ifndef NLC_OS_LINUX
#    define NLC_OS_LINUX 0
#endif
#ifndef NLC_OS_BSD
#    define NLC_OS_BSD 0
#endif

// CPU arch defines
#if defined(__aarch64__)
#    define NLC_CPU_ARM 1
#elif defined(__x86_64__)
#    define NLC_CPU_X64 1
#else
#    error "Unsupported CPU architecture"
#endif

#ifndef NLC_CPU_ARM
#    define NLC_CPU_ARM 0
#endif
#ifndef NLC_CPU_X64
#    define NLC_CPU_X64 0
#endif

// Compiler defines
#if defined(__clang__)
#    define NLC_COMPILER_CLANG 1
#    if defined(_MSC_VER)
#        define NLC_COMPILER_CLANG_CL 1
#    endif
#elif defined(__GNUC__)
#    define NLC_COMPILER_GCC 1
#else
#    error "Unsupported compiler"
#endif

#ifndef NLC_COMPILER_CLANG_CL
#    define NLC_COMPILER_CLANG_CL 0
#endif
#ifndef NLC_COMPILER_CLANG
#    define NLC_COMPILER_CLANG 0
#endif
#ifndef NLC_COMPILER_GCC
#    define NLC_COMPILER_GCC 0
#endif

// Sanitizer defines
#if NLC_COMPILER_CLANG
#    if __has_feature(address_sanitizer)
#        define NLC_ASAN 1
#    else
#        define NLC_ASAN 0
#    endif
#elif NLC_COMPILER_GCC
#    if defined(__SANITIZE_ADDRESS__)
#        define NLC_ASAN 1
#    else
#        define NLC_ASAN 0
#    endif
#endif
