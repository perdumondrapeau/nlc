/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "assert.hpp"

#include "nlc/debug/stacktrace.hpp"

#include "basic_output.hpp"
#include "string_view.hpp"

namespace nlc {

static assert_callback_type assert_callback = nullptr;

auto set_assert_callback(assert_callback_type cb) -> void { assert_callback = cb; }

auto reset_assert_callback() -> void { assert_callback = nullptr; }

// from nlc_test's hooks.cpp
namespace test {
    using assert_hook_t = void (*)(debug::frame_ptr last_frame,
                                   char const * condition,
                                   char const * file,
                                   char const * func,
                                   int line,
                                   char const * data);

    extern assert_hook_t assert_hook;
}  // namespace test

// from assert.hpp

auto assert_occurred(char const * info, char const * file, char const * func, int line, char const * data)
    -> bool {
    if (assert_callback != nullptr) {
        // do not disrupt user if they want to handle asserts
        return assert_callback(info, file, func, line, data);
    }

    if (test::assert_hook != nullptr) {
        // intercept the assert into the test framework
        test::assert_hook(returnaddress(), info, file, func, line, data);
        nlc_unreachable;
    }

    // default assert handling
    warn(file, ':', line, ": ", func, ": Assertion '", info, "' failed.");
    if (data != nullptr)
        warn(data);

    auto constexpr trace_max_depth = 512;
    debug::frame_ptr trace[trace_max_depth];
    if (auto const frame_depth = debug::capture_stacktrace(returnaddress(), trace, trace_max_depth);
        frame_depth > 0) {
        warn("\n----- stacktrace -----");

        for (auto i = 0; i < trace_max_depth; ++i) {
            auto constexpr buffer_len = 1024;
            char buffer[buffer_len];
            auto const frame = debug::decode_frame(trace[i]);
            if (str_len(frame.symbol) > 0) {
                if (debug::retrieve_line_of_code(frame, buffer, buffer_len))
                    warn(buffer);
                warn("  at ", frame.filename, ':', frame.line, ':', frame.column, " in ", frame.symbol);
            }
        }
    }
    return true;
}
}  // namespace nlc
