/* Copyright © 2019-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "expected.hpp"

#include "arithmetic_types.hpp"
#include "string_view.hpp"

namespace nlc::errors_impl {
auto get_error_count() -> u32 & {
    static thread_local u32 error_count = 0u;
    return error_count;
}

#if !defined(NDEBUG)
auto get_error_name(char const * pretty_function) -> string_view {
    auto const s = string_view::from_c_str(pretty_function);

    auto const size = s.size();
    char const * c = pretty_function + size;
    nlc_assert(*c == 0);

    usize i = 0;
    do {
        --c;
        ++i;
    } while (*c != ' ');

#    if NLC_COMPILER_GCC || NLC_COMPILER_CLANG
    // format: ` arg:auto = os::PathDoesNotExist]`
    // we'll find the last space

    i -= sizeof("]");
#    else
    // format: `::operator ()<struct os::PathDoesNotExist>(struct os::PathDoesNotExist) const`
    // we'll find the second last space

    do {
        --c;
        ++i;
    } while (*c != ' ');

    i -= sizeof(") const");
#    endif

    ++c;
    return { c, i };
}
#endif
}  // namespace nlc::errors_impl
