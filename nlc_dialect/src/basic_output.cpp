/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "basic_output.hpp"

#include <assert.h>
#include <stdio.h>

#include <nlc/sanity/platforms.hpp>

#include "assert.hpp"  // only for breakpoint, Assert is forbidden here
#include "buffer.hpp"

#if NLC_OS_WINDOWS
#    include <io.h>
#    define WRITE(file, buf, size) _write(_fileno(file), buf, static_cast<u32>(size))
#else
#    include <unistd.h>
#    define WRITE(file, buf, size) write(fileno(file), buf, static_cast<usize>(size))
#endif

namespace nlc {

static thread_local auto _buffer = buffer<char, 16'536> {};
static thread_local auto buffer_ptr = _buffer.ptr();

log_entry::log_entry()
    : _last(++buffer_ptr) {}

log_entry::~log_entry() { clear(); }

auto log_entry::display(out_stream const o) -> void {
    auto output = [o]() {
#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wdisabled-macro-expansion"
#endif
        if (o == out_stream::out)
            return stdout;
        assert(o == out_stream::err);
        return stderr;
#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic pop
#endif
    }();

    buffer_ptr[0] = '\n';
    if (WRITE(output, _last, buffer_ptr - _last + 1) < 0)
        breakpoint();
}

auto log_entry::append(bool b) -> void { append(b ? "true" : "false"); }

auto log_entry::clear() -> void { buffer_ptr = _last - 1; }

auto log_entry::c_str() const -> char const * {
    buffer_ptr[0] = '\0';
    return _last;
}

auto log_entry::append(char * str) -> void { append(const_cast<char const *>(str)); }

auto log_entry::append(char const * str) -> void {
    while (*str != '\0') {
        assert(buffer_ptr != _buffer.ptr() + _buffer.size());
        *(buffer_ptr++) = *(str++);
    }
}

auto log_entry::append(void const * const p, usize const size) -> void {
    memcpy(buffer_ptr, p, size);
    buffer_ptr += size;
}

auto log_entry::append(char const c) -> void {
    [[maybe_unused]] auto const remaining_size =
        static_cast<usize>((_buffer.ptr() + _buffer.size()) - buffer_ptr);
    assert(remaining_size >= 2);
    *(buffer_ptr++) = c;
}

auto log_entry::append(codepoint_type cp) -> void {
    [[maybe_unused]] auto const remaining_size =
        static_cast<usize>((_buffer.ptr() + _buffer.size()) - buffer_ptr);
    auto const size = codepoint_size(cp);
    assert(remaining_size >= 1 + size);
    auto const input = cp.value();
    auto const ptr = reinterpret_cast<char const *>(&input);
    for (auto i = 0u; i < size; ++i)
        *(buffer_ptr++) = *(ptr + i);
}

#define IMPLEMENT_APPEND(TYPE, FMT)                                                           \
    auto log_entry::append(TYPE const v) -> void {                                            \
        auto const remaining_size =                                                           \
            static_cast<usize>((_buffer.ptr() + _buffer.size()) - buffer_ptr);                \
        [[maybe_unused]] auto const ret = snprintf(buffer_ptr, remaining_size, FMT, v);       \
        assert(ret >= 0);                               /* Error occured ? */                 \
        assert(ret < static_cast<int>(remaining_size)); /* Not enouth memory was remaining in \
                                                           buffer*/                           \
        buffer_ptr += ret;                                                                    \
    }

IMPLEMENT_APPEND(i8, prii8)
IMPLEMENT_APPEND(i16, prii16)
IMPLEMENT_APPEND(i32, prii32)
IMPLEMENT_APPEND(i64, prii64)
IMPLEMENT_APPEND(u8, priu8)
IMPLEMENT_APPEND(u16, priu16)
IMPLEMENT_APPEND(u32, priu32)
IMPLEMENT_APPEND(u64, priu64)
IMPLEMENT_APPEND(f64, "%f")
IMPLEMENT_APPEND(void *, "%p")
#undef IMPLEMENT_APPEND

auto log_entry::append(f32 const v) -> void { return append(static_cast<f64>(v)); }

auto log_entry::get_buffer(usize const size) -> buffer_type {
    assert(size <= static_cast<usize>((_buffer.ptr() + _buffer.size()) - buffer_ptr));
    auto ptr = buffer_ptr;
    buffer_ptr += size;
    return buffer_type { ptr, size };
}

auto log_entry::commit_buffer(buffer_type const & buffer, usize const size) -> void {
    assert(size <= buffer.size);
    assert(buffer.ptr + buffer.size == buffer_ptr);
    buffer_ptr = buffer.ptr + size;
}

}  // namespace nlc
