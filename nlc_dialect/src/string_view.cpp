/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "string_view.hpp"

#include <memory.h>

#include "assert.hpp"
#include "basic_output.hpp"
#include "memory.hpp"

namespace nlc {

string_view::string_view(iterator _begin, iterator _end)
    : string_view(_begin.ptr(), static_cast<bytes<usize>>(_end - _begin)) {
    nlc_assert(_begin <= _end);
}

auto compare(string_view const l, string_view const r) -> comparison_result {
    if (l.ptr() == r.ptr() && l.size() == r.size())
        return comparison_result::equal;
    return str_cmp(l.ptr(), rm_unit(l.size()), r.ptr(), rm_unit(r.size()));
}

auto fast_compare(string_view const l, string_view const r) -> comparison_result {
    if (l.size() < r.size())
        return comparison_result::less;
    if (l.size() > r.size())
        return comparison_result::more;
    if (l.ptr() == r.ptr())
        return comparison_result::equal;

    auto const cmp = memcmp(l.ptr(), r.ptr(), l.size().v);
    if (cmp < 0)
        return comparison_result::less;
    if (cmp > 0)
        return comparison_result::more;

    return comparison_result::equal;
}

auto make_null_terminated(string_view const str, char * buf, [[maybe_unused]] bytes<usize> size) -> void {
    nlc_assert(rm_unit(str.size()) + 1 < size);
    memcpy(buf, str.ptr(), str.size());
    buf[rm_unit(str.size())] = '\0';
}

}  // namespace nlc
