/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "utf8.hpp"

namespace nlc {

// utf8 //////////////////////////////////////////////////////////////////

template<typename T>
[[nodiscard]] static auto get_codepoint_of_size(T * str, usize const n) -> codepoint_type {
    using underlying_type = codepoint_type::underlying_type;
    switch (n) {
        case 4: {
            return codepoint_type(
                static_cast<underlying_type>(((0x07 & str[0]) << 18) | ((0x3f & str[1]) << 12) |
                                             ((0x3f & str[2]) << 6) | (0x3f & (str[3]))));
        }
        case 3:
            return codepoint_type(static_cast<underlying_type>(
                ((0x0f & str[0]) << 12) | ((0x3f & str[1]) << 6) | (0x3f & str[2])));
        case 2:
            return codepoint_type(
                static_cast<underlying_type>(((0x1f & str[0]) << 6) | (0x3f & str[1])));
            // case 1:
        default: return codepoint_type(static_cast<underlying_type>(str[0]));
    }
}

// ----------------------------------------

auto str_size(char const * str) -> usize {
    if (str == nullptr)
        return 0;
    usize res = 0;
    for (; *str != '\0'; ++str)
        ++res;
    return res;
}

auto str_len(char const * str) -> usize {
    usize res = 0;
    while (*str != '\0') {
        auto const cp_size = first_codepoint_size(str);
        str += cp_size;
        ++res;
    }
    return res;
}

auto str_len(char const * str, usize const n) -> usize {
    usize res = 0;
    auto const end = str + n;
    while (str != end) {
        auto const cp_size = first_codepoint_size(str);
        str += cp_size;
        ++res;
    }
    return res;
}

// ----------------------------------------

auto str_cmp(char const * const s1, char const * const s2) -> comparison_result {
    auto str1 = reinterpret_cast<unsigned char const *>(s1);
    auto str2 = reinterpret_cast<unsigned char const *>(s2);

    while (*str1 != '\0' && *str2 != '\0') {
        if (*str1 < *str2)
            return comparison_result::less;
        else if (*str1 > *str2)
            return comparison_result::more;
        ++str1;
        ++str2;
    }

    if (*str1 != '\0')
        return comparison_result::more;
    else if (*str2 != '\0')
        return comparison_result::less;
    else
        return comparison_result::equal;
}

auto str_cmp(char const * const s1, usize const n1, char const * const s2, usize const n2) -> comparison_result {
    auto str1 = reinterpret_cast<unsigned char const *>(s1);
    auto str2 = reinterpret_cast<unsigned char const *>(s2);
    auto const end1 = str1 + n1;
    auto const end2 = str2 + n2;

    while (str1 != end1 && str2 != end2) {
        if (*str1 < *str2)
            return comparison_result::less;
        else if (*str1 > *str2)
            return comparison_result::more;
        ++str1;
        ++str2;
    }

    if (str1 != end1)
        return comparison_result::more;
    else if (str2 != end2)
        return comparison_result::less;
    else
        return comparison_result::equal;
}

// ----------------------------------------

auto codepoint(char const * const str) -> codepoint_type {
    return get_codepoint_of_size(str, first_codepoint_size(str));
}

auto codepoint(char const * const str, char const ** ptr) -> codepoint_type {
    auto const cp_size = first_codepoint_size(str);
    *ptr = str + cp_size;
    return get_codepoint_of_size(str, cp_size);
}

auto codepoint_size(codepoint_type const c) -> usize {
    if (0 == (static_cast<codepoint_type::underlying_type>(0xffffff80) & c.value()))
        return 1u;
    else if (0 == (static_cast<codepoint_type::underlying_type>(0xfffff800) & c.value()))
        return 2u;
    else if (0 == (static_cast<codepoint_type::underlying_type>(0xffff0000) & c.value()))
        return 3u;
    else /* (0 == (static_cast<codepoint_type::underlying_type>(0xffe00000)  & chr))*/
        return 4u;
}

auto cat_to_string(char * const str, codepoint_type const cp) -> char * {
    switch (codepoint_size(cp)) {
        case 2:
            str[0] = static_cast<char>(0xc0 | (cp.value() >> 6));
            str[1] = static_cast<char>(0x80 | (cp.value() & 0x3f));
            return str + 2;
        case 3:
            str[0] = static_cast<char>(0xe0 | (cp.value() >> 12));
            str[1] = static_cast<char>(0x80 | ((cp.value() >> 6) & 0x3f));
            str[2] = static_cast<char>(0x80 | (cp.value() & 0x3f));
            return str + 3;
        case 4:
            str[0] = static_cast<char>(0xf0 | (cp.value() >> 18));
            str[1] = static_cast<char>(0x80 | ((cp.value() >> 12) & 0x3f));
            str[2] = static_cast<char>(0x80 | ((cp.value() >> 6) & 0x3f));
            str[3] = static_cast<char>(0x80 | (cp.value() & 0x3f));
            return str + 4;
        default: str[0] = static_cast<char>(cp.value()); return str + 1;
    }
}

}  // namespace nlc
