/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <string.h>

#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/memory.hpp>
#include <nlc/test.hpp>

using namespace nlc;

struct Test {
    int a;
    char const * b;
};

static auto format(log_entry & p, Test const & t) {
    p.append(t.a);
    p.append('(');
    p.append(t.b);
    p.append(')');
}

struct Test2 {};

static auto format(log_entry & p, Test2 const &) {
    auto buf = p.get_buffer(usize(10));
    memcpy(buf.ptr, "Avion supersonique", bytes<usize>(6));
    p.commit_buffer(buf, 6);
}

nlc_test("log entry") {
    auto p1 = log_entry();
    p1.append("coucou ");
    p1.append(5);
    p1.append("bid voeij", usize(4));
    {
        auto p2 = log_entry();
        p2.append("Pardon je passe");
        auto t = Test { 9, "Lol" };
        p2.append(t);

        p2.display(out_stream::err);
        nlc_check(strcmp(p2.c_str(), "Pardon je passe9(Lol)") == 0);
    }

    p1.append(Test2 {});
    p1.append("casimir !");

    p1.display(out_stream::err);
    nlc_check(strcmp(p1.c_str(), "coucou 5bid Avion casimir !") == 0);
}

nlc_test_todo("warn");
nlc_test_todo("print");
