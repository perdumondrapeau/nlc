/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/traits.hpp>
#include <nlc/test.hpp>

#include <stdio.h>

using namespace nlc;

nlc_test("is_integral") {
    nlc_check(meta::is_integral<byte>);
    nlc_check(meta::is_integral<i8>);
    nlc_check(meta::is_integral<i16>);
    nlc_check(meta::is_integral<i32>);
    nlc_check(meta::is_integral<i64>);
    nlc_check(meta::is_integral<u8>);
    nlc_check(meta::is_integral<u16>);
    nlc_check(meta::is_integral<u32>);
    nlc_check(meta::is_integral<u64>);
    nlc_check(!meta::is_integral<f32>);
    nlc_check(!meta::is_integral<f64>);
    nlc_check(meta::is_integral<isize>);
    nlc_check(meta::is_integral<usize>);
}

nlc_test("is_signed_integer or is_unsigned_integer") {
    nlc_check(meta::is_unsigned_integer<byte>);
    nlc_check(meta::is_signed_integer<i8>);
    nlc_check(meta::is_signed_integer<i16>);
    nlc_check(meta::is_signed_integer<i32>);
    nlc_check(meta::is_signed_integer<i64>);
    nlc_check(meta::is_unsigned_integer<u8>);
    nlc_check(meta::is_unsigned_integer<u16>);
    nlc_check(meta::is_unsigned_integer<u32>);
    nlc_check(meta::is_unsigned_integer<u64>);
    nlc_check(meta::is_signed_integer<isize>);
    nlc_check(meta::is_unsigned_integer<usize>);
}

nlc_test("make_signed") {
    nlc_check(meta::is_same<meta::make_signed<byte>, i8>);
    nlc_check(meta::is_same<meta::make_signed<i8>, i8>);
    nlc_check(meta::is_same<meta::make_signed<u8>, i8>);
    nlc_check(meta::is_same<meta::make_signed<i16>, i16>);
    nlc_check(meta::is_same<meta::make_signed<u16>, i16>);
    nlc_check(meta::is_same<meta::make_signed<i32>, i32>);
    nlc_check(meta::is_same<meta::make_signed<u32>, i32>);
    nlc_check(meta::is_same<meta::make_signed<i64>, i64>);
    nlc_check(meta::is_same<meta::make_signed<u64>, i64>);
}

nlc_test("make_unsigned") {
    nlc_check(meta::is_same<meta::make_unsigned<byte>, u8>);
    nlc_check(meta::is_same<meta::make_unsigned<i8>, u8>);
    nlc_check(meta::is_same<meta::make_unsigned<u8>, u8>);
    nlc_check(meta::is_same<meta::make_unsigned<i16>, u16>);
    nlc_check(meta::is_same<meta::make_unsigned<u16>, u16>);
    nlc_check(meta::is_same<meta::make_unsigned<i32>, u32>);
    nlc_check(meta::is_same<meta::make_unsigned<u32>, u32>);
    nlc_check(meta::is_same<meta::make_unsigned<i64>, u64>);
    nlc_check(meta::is_same<meta::make_unsigned<u64>, u64>);
}

nlc_test("is_floating") {
    nlc_check(!meta::is_floating<byte>);
    nlc_check(!meta::is_floating<i8>);
    nlc_check(!meta::is_floating<i16>);
    nlc_check(!meta::is_floating<i32>);
    nlc_check(!meta::is_floating<i64>);
    nlc_check(!meta::is_floating<u8>);
    nlc_check(!meta::is_floating<u16>);
    nlc_check(!meta::is_floating<u32>);
    nlc_check(!meta::is_floating<u64>);
    nlc_check(meta::is_floating<f32>);
    nlc_check(meta::is_floating<f64>);
    nlc_check(!meta::is_floating<isize>);
    nlc_check(!meta::is_floating<usize>);
}

nlc_test("sizeof") {
    nlc_check(sizeof(byte) == 1);
    nlc_check(sizeof(i8) == 1);
    nlc_check(sizeof(i16) == 2);
    nlc_check(sizeof(i32) == 4);
    nlc_check(sizeof(i64) == 8);
    nlc_check(sizeof(u8) == 1);
    nlc_check(sizeof(u16) == 2);
    nlc_check(sizeof(u32) == 4);
    nlc_check(sizeof(u64) == 8);
    nlc_check(sizeof(f32) == 4);
    nlc_check(sizeof(f64) == 8);
    nlc_check(sizeof(isize) == sizeof(void *));
    nlc_check(sizeof(usize) == sizeof(void *));
}

nlc_test("alignof") {
    nlc_check(alignof(byte) == 1);
    nlc_check(alignof(i8) == 1);
    nlc_check(alignof(i16) == 2);
    nlc_check(alignof(i32) == 4);
    nlc_check(alignof(i64) == 8);
    nlc_check(alignof(u8) == 1);
    nlc_check(alignof(u16) == 2);
    nlc_check(alignof(u32) == 4);
    nlc_check(alignof(u64) == 8);
    nlc_check(alignof(f32) == 4);
    nlc_check(alignof(f64) == 8);
    nlc_check(alignof(isize) == alignof(void *));
    nlc_check(alignof(usize) == alignof(void *));
}

// Test our format specifiers
nlc_test("format specifiers") {
    printf(prii8, i8(0));
    printf(prii16, i16(0));
    printf(prii32, i32(0));
    printf(prii64, i64(0));
    printf(priisize, isize(0));
    printf(priu8, u8(0));
    printf(priu16, u16(0));
    printf(priu32, u32(0));
    printf(priu64, u64(0));
    printf(priusize, usize(0));
}
