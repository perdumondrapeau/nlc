/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/expected.hpp>
#include <nlc/test.hpp>

using namespace nlc::logging;

class failure_1 {};
class failure_2 {};
class failure_3 {};
class failure_4 {};

template<typename T>
using expect_all_failures = nlc::expected<T, failure_1, failure_2, failure_3, failure_4>;

static auto can_fail(int arg) -> nlc::expected<int, failure_1, failure_2, failure_3> {
    switch (arg) {
        case 1: return failure_1 {};
        case 2: return failure_2 {};
        case 3: return failure_3 {};
    }
    return arg;
}

static auto can_fail_2(int arg) -> nlc::expected<int, failure_1, failure_4, failure_3, failure_2> {
    if (arg < 5)
        return can_fail(arg);
    return failure_4 {};
}

nlc_test("success") {
    {
        auto v = can_fail(8);
        nlc_check(v.succeeded());
        nlc_check(!v.failed());
        nlc_check(!v.failed_with<failure_1>());
        nlc_check(!v.failed_with<failure_2>());
        nlc_check(!v.failed_with<failure_3>());
        nlc_check(v.get() == 8);
    }
    {
        auto v = can_fail_2(0);
        nlc_check(v.succeeded());
        nlc_check(!v.failed());
        nlc_check(!v.failed_with<failure_1>());
        nlc_check(!v.failed_with<failure_2>());
        nlc_check(!v.failed_with<failure_3>());
        nlc_check(!v.failed_with<failure_4>());
        nlc_check(v.get() == 0);
    }
    {
        auto v = can_fail_2(4);
        nlc_check(v.succeeded());
        nlc_check(!v.failed());
        nlc_check(!v.failed_with<failure_1>());
        nlc_check(!v.failed_with<failure_2>());
        nlc_check(!v.failed_with<failure_3>());
        nlc_check(!v.failed_with<failure_4>());
        nlc_check(v.get() == 4);
    }
}

nlc_test("failure") {
    {
        auto v = can_fail(1);
        nlc_check(!v.succeeded());
        nlc_check(v.failed());
        nlc_check(v.failed_with<failure_1>());
        nlc_check(!v.failed_with<failure_2>());
        nlc_check(!v.failed_with<failure_3>());
    }
    {
        auto v = can_fail(2);
        nlc_check(!v.succeeded());
        nlc_check(v.failed());
        nlc_check(!v.failed_with<failure_1>());
        nlc_check(v.failed_with<failure_2>());
        nlc_check(!v.failed_with<failure_3>());
    }
    {
        auto v = can_fail(3);
        nlc_check(!v.succeeded());
        nlc_check(v.failed());
        nlc_check(!v.failed_with<failure_1>());
        nlc_check(!v.failed_with<failure_2>());
        nlc_check(v.failed_with<failure_3>());
    }
    {
        auto v = can_fail_2(1);
        nlc_check(!v.succeeded());
        nlc_check(v.failed());
        nlc_check(v.failed_with<failure_1>());
        nlc_check(!v.failed_with<failure_2>());
        nlc_check(!v.failed_with<failure_3>());
        nlc_check(!v.failed_with<failure_4>());
    }
    {
        auto v = can_fail_2(2);
        nlc_check(!v.succeeded());
        nlc_check(v.failed());
        nlc_check(!v.failed_with<failure_1>());
        nlc_check(v.failed_with<failure_2>());
        nlc_check(!v.failed_with<failure_3>());
        nlc_check(!v.failed_with<failure_4>());
    }
    {
        auto v = can_fail_2(3);
        nlc_check(!v.succeeded());
        nlc_check(v.failed());
        nlc_check(!v.failed_with<failure_1>());
        nlc_check(!v.failed_with<failure_2>());
        nlc_check(v.failed_with<failure_3>());
        nlc_check(!v.failed_with<failure_4>());
    }
    {
        auto v = can_fail_2(5);
        nlc_check(!v.succeeded());
        nlc_check(v.failed());
        nlc_check(!v.failed_with<failure_1>());
        nlc_check(!v.failed_with<failure_2>());
        nlc_check(!v.failed_with<failure_3>());
        nlc_check(v.failed_with<failure_4>());
    }
}

nlc_test("forward error") {
    {
        expect_all_failures<int> const intErr { failure_1 {} };
        auto const stringErr =
            [&intErr]() -> nlc::expected<int, failure_1, failure_2, failure_3, failure_4> {
            return intErr.forward_error();
        }();
        nlc_check(stringErr.failed());
        nlc_check(stringErr.failed_with<failure_1>());
    }
    {
        nlc::expected<int, failure_1> const intErr { failure_1 {} };
        auto const doubleErr = [&intErr]() -> nlc::expected<double, failure_2, failure_4, failure_1> {
            return intErr.forward_error();
        }();
        nlc_check(doubleErr.failed());
        nlc_check(doubleErr.failed_with<failure_1>());
    }
    {
        nlc::expected<int, failure_1, failure_4, failure_3> const intErr { failure_3 {} };
        auto const boolErr = [&intErr]() -> nlc::expected<bool, failure_3, failure_2> {
            return intErr.forward_error();
        }();
        nlc_check(boolErr.failed());
        nlc_check(boolErr.failed_with<failure_3>());
    }
}

#if !defined(NDEBUG)
namespace foobar {
struct SomeError {};
struct AnotherOne {};
}  // namespace foobar

using namespace foobar;

nlc_test("get error info") {
    {
        nlc::expected<void, SomeError, AnotherOne> const res = SomeError {};
        nlc_check(res.get_error_name() == "foobar::SomeError");

        nlc::expected<void, SomeError, AnotherOne> const res2 = AnotherOne {};
        nlc_check(res2.get_error_name() == "foobar::AnotherOne");

        nlc::expected<void, SomeError, AnotherOne> const res3 = {};
        nlc_expect_assert(res3.get_error_name());
    }
    {
        nlc::expected<u32, SomeError, AnotherOne> const res = SomeError {};
        nlc_check(res.get_error_name() == "foobar::SomeError");

        nlc::expected<u32, SomeError, AnotherOne> const res2 = AnotherOne {};
        nlc_check(res2.get_error_name() == "foobar::AnotherOne");

        nlc::expected<u32, SomeError, AnotherOne> const res3 = 123u;
        nlc_check(res3.get() == 123u);
        nlc_expect_assert(res3.get_error_name());
    }
}
#endif
