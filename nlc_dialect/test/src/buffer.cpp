/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/buffer.hpp>
#include <nlc/dialect/range.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/numeric_limits.hpp>
#include <nlc/meta/traits.hpp>
#include <nlc/test.hpp>

using namespace nlc;

struct movable_int final {
    static inline constexpr int moved_value { meta::limits<int>::min };
    int value;

    constexpr movable_int(int v)
        : value { v } {}

    movable_int(movable_int const & other) { operator=(other); }
    movable_int(movable_int && other) { operator=(nlc_move(other)); }

    auto operator=(movable_int const & other) -> movable_int & {
        value = other.value;
        return *this;
    }

    auto operator=(movable_int && other) -> movable_int & {
        if (&other != this) {
            value = other.value;
            other.value = moved_value;
        }
        return *this;
    }

    auto operator==(movable_int const & other) -> bool { return value == other.value; }
};

nlc_test("basic usage") {
    constexpr i32 values[] = { 5, -4, 0, 3 };
    constexpr auto value_count { meta::array_len<decltype(values)> };

    buffer<i32, value_count> ints;
    for (auto i : range<usize>(0u, value_count))
        ints[i] = values[i];

    for (auto i : range<usize>(0u, value_count))
        nlc_check_msg(ints[i] == values[i], "expected ", values[i], ", got ", ints[i]);
}

nlc_test("init with default value") {
    constexpr i32 default_int = -18;

    buffer<i32, 6> ints { default_int };
    for (auto const i : ints)
        nlc_check_msg(i == default_int, "expected ", default_int, ", got ", i);

    struct Dummy final {
        char c;
        float f;
    };
    constexpr Dummy default_dummy { .c = 'a', .f = 13.12f };

    buffer<Dummy, 16> dummies { default_dummy };
    for (auto const & d : dummies) {
        nlc_check_msg(d.c == default_dummy.c && d.f == default_dummy.f,
                      "expected {",
                      default_dummy.c,
                      ", ",
                      default_dummy.f,
                      "}, got {",
                      d.c,
                      ", ",
                      d.f,
                      "}");
    }
}

nlc_test("copy") {
    constexpr i32 values[] = { 5, -4, 0, 3 };
    constexpr auto value_count { meta::array_len<decltype(values)> };

    buffer<i32, value_count> src;
    for (auto i : range<usize>(0u, value_count))
        src[i] = values[i];

    decltype(src) dst;
    src.copy_to(dst);

    for (auto i : range<usize>(0u, value_count)) {
        nlc_check_msg(src[i] == values[i], "at ", i, ": expected ", values[i], ", got ", src[i]);
        nlc_check_msg(dst[i] == values[i], "at ", i, ": expected ", values[i], ", got ", dst[i]);
    }
}

nlc_test("move") {
    constexpr movable_int values[] = { 3, 584, -65, 2350256, 0 };
    constexpr auto value_count { meta::array_len<decltype(values)> };

    buffer<movable_int, value_count> src;
    for (auto i : range<usize>(0u, value_count))
        src[i] = values[i];

    decltype(src) dst { nlc_move(src) };
    for (auto i : range<usize>(0u, value_count)) {
        nlc_check_msg(src[i] == movable_int::moved_value,
                      "at ",
                      i,
                      ": expected ",
                      movable_int::moved_value,
                      ", got ",
                      src[i].value);
        nlc_check_msg(dst[i] == values[i],
                      "at ",
                      i,
                      ": expected ",
                      values[i].value,
                      ", got ",
                      dst[i].value);
    }
}
