#include <nlc/dialect/span.hpp>

auto get_sum_and_mult_by_two(nlc::span<int> arg) -> int;
auto get_sum_and_mult_by_two(nlc::span<int> arg) -> int {
    int res = 0;

    for (auto & v : arg) {
        res += v;
        v *= 2;
    }

    return res;
}
