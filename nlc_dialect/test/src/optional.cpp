/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/types.hpp>
#include <nlc/test.hpp>

using namespace nlc;

struct RefCounter {
    RefCounter() { ++count; }
    RefCounter(RefCounter const &) = delete;
    RefCounter(RefCounter &&) { ++count; }

    ~RefCounter() {
        nlc_check(count > 0u);
        --count;
    }

    static u32 count;
};

struct ResCounter {
    ResCounter()
        : isOwningRes { true } {
        ++count;
    }
    ResCounter(ResCounter const &) = delete;
    ResCounter(ResCounter && other) {
        isOwningRes = other.isOwningRes;
        other.isOwningRes = false;
    }

    ~ResCounter() {
        nlc_check(!isOwningRes || count > 0u);
        if (isOwningRes) {
            --count;
        }
    }

    static u32 count;
    bool isOwningRes;
};

struct MoveAndDtorCounter {
    MoveAndDtorCounter() = default;
    MoveAndDtorCounter(int, float, char const *) {}  // Dummy
    MoveAndDtorCounter(MoveAndDtorCounter const &) = default;
    MoveAndDtorCounter(MoveAndDtorCounter &&) { ++move_count; }
    ~MoveAndDtorCounter() { ++dtor_count; }

    MoveAndDtorCounter & operator=(MoveAndDtorCounter const &) { return *this; }
    MoveAndDtorCounter & operator=(MoveAndDtorCounter &&) {
        ++move_count;
        return *this;
    }

    static void reset() {
        move_count = 0u;
        dtor_count = 0u;
    }

    static u32 move_count;
    static u32 dtor_count;
};

struct MoveConstructed final {
    MoveConstructed() = delete;
    MoveConstructed(int) {}
    MoveConstructed(MoveConstructed const &) = delete;
    MoveConstructed(MoveConstructed &&) = default;
};

u32 RefCounter::count = 0u;
u32 ResCounter::count = 0u;
u32 MoveAndDtorCounter::move_count = 0u;
u32 MoveAndDtorCounter::dtor_count = 0u;

nlc_test("initialisation") {
    auto a = optional(12);
    nlc_check(meta::is_same<decltype(a), optional<int>>);
    nlc_check(a != null);
    nlc_check(*a == 12);

    auto b = optional(23u);
    nlc_check(meta::is_same<decltype(b), optional<unsigned int>>);
    nlc_check(*b == 23u);

    auto c = optional(45.f);
    nlc_check(meta::is_same<decltype(c), optional<float>>);
    nlc_check(*c == 45.f);

    auto d = optional(bytes<int>(123));
    nlc_check(meta::is_same<decltype(d), optional<bytes<int>>>);
    nlc_check(*d == byte(123));
}

nlc_test("comparison") {
    auto a = optional(12);
    auto b = optional(14);
    auto c = optional(12);
    auto d = optional<int>(null);
    auto e = optional<int>(null);
    nlc_check(*a == *c);
    nlc_check(*c == *a);
    nlc_check(*b != *c);
    nlc_check(*c != *b);
    nlc_check(a == c);
    nlc_check(c == a);
    nlc_check(b != c);
    nlc_check(c != b);
    nlc_check(d != a);
    nlc_check(a != d);
    nlc_check(d == e);
    nlc_check(e == d);
    nlc_check(e == null);
    nlc_check(null == e);
    nlc_check(e != 1);
    nlc_check(1 != e);
}

nlc_test("set null") {
    auto a = optional(12);
    auto b = optional(14);
    auto c = optional<int>(null);
    nlc_check(*a + 2 == b);
    b = null;
    nlc_check(b == null);
    nlc_check(*a + 2 != b);
    nlc_check(b == c);
}

nlc_test("assign from value") {
    auto a = optional(12);
    auto b = optional<int>(null);
    auto c = optional<int>(null);
    b = 12;
    nlc_check(b != null);
    nlc_check(a == b);
}

nlc_test("construct from rvalue optional") {
    auto a = optional(12);
    auto b = optional<int>(null);

    auto c = optional(nlc_move(a));
    nlc_check(a == null);
    nlc_check(c == 12);

    auto d = optional(nlc_move(b));
    nlc_check(b == null);
    nlc_check(d == null);
}

nlc_test("assign from rvalue optional to not null optional") {
    auto a = optional(12);
    auto b = optional<int>(null);

    auto c = optional(14);
    nlc_check(c == 14);
    c = nlc_move(a);
    nlc_check(c == 12);
    nlc_check(a == null);

    auto d = optional(15);
    nlc_check(d == 15);
    d = nlc_move(b);
    nlc_check(d == null);
    nlc_check(b == null);
}

nlc_test("assign from rvalue optional to null optional") {
    auto a = optional(12);
    auto b = optional<int>(null);

    auto c = optional<int>(null);
    nlc_check(c == null);
    c = nlc_move(a);
    nlc_check(c == 12);
    nlc_check(a == null);

    auto d = optional<int>(null);
    nlc_check(d == null);
    d = nlc_move(b);
    nlc_check(d == null);
    nlc_check(b == null);
}

nlc_test("destruction of null") {
    nlc_check(RefCounter::count == 0u);
    {
        auto f = optional<RefCounter>(null);
        nlc_check(RefCounter::count == 0u);
        f = RefCounter {};
        nlc_check(RefCounter::count == 1u);
        f = null;
        nlc_check(RefCounter::count == 0u);
    }
    nlc_check(RefCounter::count == 0u);
}

nlc_test("access on null") {
    struct B {
        int b;
    };
    auto a = optional<int>(null);
    auto b = optional<B>(null);
    nlc_check(a == null);
    nlc_expect_assert(*a = 3);
    nlc_check(b == null);
    nlc_expect_assert(b->b = 3);
}

nlc_test("destruction of value") {
    nlc_check(RefCounter::count == 0u);
    {
        auto f = optional<RefCounter>(null);
        nlc_check(RefCounter::count == 0u);
        f = RefCounter {};
        nlc_check(RefCounter::count == 1u);
    }
    nlc_check(RefCounter::count == 0u);

    {
        ResCounter counter;
        nlc_check(counter.isOwningRes);
        nlc_check(ResCounter::count == 1u);

        optional<ResCounter> f { nlc_move(counter) };
        nlc_check(!counter.isOwningRes);
        nlc_check(f->isOwningRes);
        nlc_check(ResCounter::count == 1u);
    }

    nlc_check(ResCounter::count == 0u);

    {
        optional<ResCounter> f { null };
        {
            nlc_check(ResCounter::count == 0u);
            ResCounter counter;
            nlc_check(counter.isOwningRes);
            nlc_check(ResCounter::count == 1u);

            f = nlc_move(counter);
            nlc_check(!counter.isOwningRes);
            nlc_check(f->isOwningRes);
            nlc_check(ResCounter::count == 1u);
        }
        nlc_check(ResCounter::count == 1u);
    }

    nlc_check(ResCounter::count == 0u);
}

nlc_test("assign by move") {
    {
        optional<ResCounter> f { null };
        {
            nlc_check(ResCounter::count == 0u);
            ResCounter counter;
            nlc_check(counter.isOwningRes);
            nlc_check(ResCounter::count == 1u);

            f = nlc_move(counter);
            nlc_check(!counter.isOwningRes);
            nlc_check(f->isOwningRes);
            nlc_check(ResCounter::count == 1u);
        }
        nlc_check(ResCounter::count == 1u);
    }

    nlc_check(ResCounter::count == 0u);
}

nlc_test("assign to rvalue") {
    MoveAndDtorCounter::reset();
    {
        optional<MoveAndDtorCounter> f = null;
        nlc_check(MoveAndDtorCounter::move_count == 0u);
        nlc_check(MoveAndDtorCounter::dtor_count == 0u);

        f = MoveAndDtorCounter {};
        nlc_check(MoveAndDtorCounter::move_count == 1u);
        nlc_check(MoveAndDtorCounter::dtor_count == 1u);
    }

    nlc_check(MoveAndDtorCounter::move_count == 1u);
    nlc_check(MoveAndDtorCounter::dtor_count == 2u);
}

nlc_test("assign with set") {
    MoveAndDtorCounter::reset();
    {
        optional<MoveAndDtorCounter> f = null;
        nlc_check(MoveAndDtorCounter::move_count == 0u);
        nlc_check(MoveAndDtorCounter::dtor_count == 0u);

        f.set(42, 3.7f, "foo");
        nlc_check(MoveAndDtorCounter::move_count == 0u);
        nlc_check(MoveAndDtorCounter::dtor_count == 0u);
    }

    nlc_check(MoveAndDtorCounter::move_count == 0u);
    nlc_check(MoveAndDtorCounter::dtor_count == 1u);
}

nlc_test("extract_value") {
    // Bad way of extracting the object : opt2 is in an unknown state
    optional<MoveConstructed> opt2(12);
    [[maybe_unused]] MoveConstructed extracted2 = nlc_move(*opt2);
    nlc_check(opt2 != nlc::null);

    // Good way to do it
    optional<MoveConstructed> opt1(12);
    [[maybe_unused]] MoveConstructed extracted1 = *nlc_move(opt1);
    nlc_check(opt1 == nlc::null);
}
