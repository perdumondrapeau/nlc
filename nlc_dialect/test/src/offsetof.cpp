/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/offsetof.hpp>

template<typename T> struct ThreeFields final {
    T a;
    T b;
    T c;
};

auto main() -> int {
    static_assert(nlc_offsetof(ThreeFields<i16>, a) == 0u, "nlc_offsetof ThreeFields<i16>::a is not 0");
    static_assert(nlc_offsetof(ThreeFields<i16>, b) == 2u, "nlc_offsetof ThreeFields<i16>::b is not 2");
    static_assert(nlc_offsetof(ThreeFields<i16>, c) == 4u, "nlc_offsetof ThreeFields<i16>::c is not 4");

    static_assert(nlc_offsetof(ThreeFields<bool>, a) == 0u,
                  "nlc_offsetof ThreeFields<bool>::a is not 0");
    static_assert(nlc_offsetof(ThreeFields<bool>, b) == 1u,
                  "nlc_offsetof ThreeFields<bool>::b is not 1");
    static_assert(nlc_offsetof(ThreeFields<bool>, c) == 2u,
                  "nlc_offsetof ThreeFields<bool>::c is not 2");

    static_assert(nlc_offsetof(ThreeFields<f32>, a) == 0u, "nlc_offsetof ThreeFields<f32>::a is not 0");
    static_assert(nlc_offsetof(ThreeFields<f32>, b) == 4u, "nlc_offsetof ThreeFields<f32>::b is not 4");
    static_assert(nlc_offsetof(ThreeFields<f32>, c) == 8u, "nlc_offsetof ThreeFields<f32>::c is not 8");

    constexpr auto ptr_size = sizeof(usize);
    static_assert(nlc_offsetof(ThreeFields<char const *>, a) == 0u * ptr_size,
                  "nlc_offsetof ThreeFields<char const*>::a is not 0");
    static_assert(nlc_offsetof(ThreeFields<char const *>, b) == 1u * ptr_size,
                  "nlc_offsetof ThreeFields<char const*>::b is not 4");
    static_assert(nlc_offsetof(ThreeFields<char const *>, c) == 2u * ptr_size,
                  "nlc_offsetof ThreeFields<char const*>::c is not 8");

    static_assert(nlc_offsetof(ThreeFields<ThreeFields<int> const *>, a) == 0u * ptr_size,
                  "nlc_offsetof ThreeFields<ThreeFields<int> const*>::a is not 0");
    static_assert(nlc_offsetof(ThreeFields<ThreeFields<int> const *>, b) == 1u * ptr_size,
                  "nlc_offsetof ThreeFields<ThreeFields<int> const*>::b is not 4");
    static_assert(nlc_offsetof(ThreeFields<ThreeFields<int> const *>, c) == 2u * ptr_size,
                  "nlc_offsetof ThreeFields<ThreeFields<int> const*>::c is not 8");
}
