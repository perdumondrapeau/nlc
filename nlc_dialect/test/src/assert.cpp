/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/sanity/debug_only.hpp>
#include <nlc/test.hpp>

#include <string.h>

struct Expected final {
    char const * condition;
    char const * data;
};

static auto expected = Expected {};
static int nlc_test_assert_count = 0u;

static auto assert_occurred(char const * condition, char const *, char const *, int, char const * data)
    -> bool {
    ++nlc_test_assert_count;
    nlc_check(strcmp(condition, expected.condition) == 0);
    nlc_check(data == expected.data || strcmp(data, expected.data) == 0);
    return false;
}

nlc_test("Assert") {
    nlc::set_assert_callback(&assert_occurred);

    nlc_test_assert_count = 0;
    nlc_assert(true);
    nlc_check(nlc_test_assert_count == 0);

    nlc_test_assert_count = 0;
    expected = Expected { "false", nullptr };
    nlc_assert(false);
    nlc_check(nlc_test_assert_count == 1);

    nlc::reset_assert_callback();
}

nlc_test("AssertMsg") {
    nlc::set_assert_callback(&assert_occurred);
    nlc_test_assert_count = 0;
    nlc_assert_msg(true, "Coucou");
    nlc_check(nlc_test_assert_count == 0);

    nlc_test_assert_count = 0;
    expected = Expected { "false", "Coucou" };
    nlc_assert_msg(false, "Coucou");
    nlc_check(nlc_test_assert_count == 1);

    [[maybe_unused]] auto i = 2;

    nlc_test_assert_count = 0;
    expected = Expected { "i == 3", "i égal 3" };
    nlc_assert_msg(i == 3, "i égal ", 3);
    nlc_check(nlc_test_assert_count == 1);

    nlc_test_assert_count = 0;
    expected = Expected { "2 * i == 6", "i vaut 2" };
    nlc_assert_msg(2 * i == 6, "i vaut ", i);
    nlc_check(nlc_test_assert_count == 1);

    auto const s = "Bateau";

    nlc_test_assert_count = 0;
    expected = Expected { "false", s };
    nlc_assert_msg(false, s);
    nlc_check(nlc_test_assert_count == 1);

    nlc::reset_assert_callback();
}

nlc_test("AssertMsg") {
    nlc::set_assert_callback(&assert_occurred);
    nlc_test_assert_count = 0;
    nlc_assert_msg(true, "Coucou");
    nlc_check(nlc_test_assert_count == 0);

    nlc_test_assert_count = 0;
    expected = Expected { "false", "Coucou" };
    nlc_assert_msg(false, "Coucou");
    nlc_check(nlc_test_assert_count == 1);

    [[maybe_unused]] auto i = 2;

    nlc_test_assert_count = 0;
    expected = Expected { "i == 3", "i égal 3" };
    nlc_assert_msg(i == 3, "i égal ", 3);
    nlc_check(nlc_test_assert_count == 1);

    nlc::reset_assert_callback();
}

nlc_test("AssertMsg") {
    nlc::set_assert_callback(&assert_occurred);
    nlc_test_assert_count = 0;
    nlc_assert_msg(true, "Coucou");
    nlc_check(nlc_test_assert_count == 0);

    nlc_test_assert_count = 0;
    expected = Expected { "false", "Coucou" };
    nlc_assert_msg(false, "Coucou");
    nlc_check(nlc_test_assert_count == 1);

    [[maybe_unused]] auto i = 2;

    nlc_test_assert_count = 0;
    expected = Expected { "i == 3", "i égal 3" };
    nlc_assert_msg(i == 3, "i égal ", 3);
    nlc_check(nlc_test_assert_count == 1);

    nlc::reset_assert_callback();
}

nlc_test("DumpVar") {
    nlc::set_assert_callback(&assert_occurred);

    auto const s = "Bateau";

    nlc_test_assert_count = 0;
    expected = Expected { "strcmp(s, \"Avion\") == 0", "Raté !\n - s : Bateau\n - strlen(s) : 6" };
    nlc_assert_msg(strcmp(s, "Avion") == 0, "Raté !", nlc_dump_var(s), nlc_dump_var(strlen(s)));
    nlc_check(nlc_test_assert_count == 1);

    nlc_test_assert_count = 0;
    expected = Expected { "strcmp(s, \"Avion\") == 0", "Raté !\n - s : Bateau\n - strlen(s) : 6" };
    nlc_assert_msg(strcmp(s, "Avion") == 0, nlc_dump_var(s), "Raté !", nlc_dump_var(strlen(s)));
    nlc_check(nlc_test_assert_count == 1);

    nlc::reset_assert_callback();
}
