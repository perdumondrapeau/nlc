/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <assert.h>

#include <nlc/dialect/range.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test("iterate over range") {
    [[maybe_unused]] auto i = 0;
    for (auto j : range(0, 5)) {
        static_assert(meta::is_same<decltype(j), int>);
        assert(j == i++);
    }
    assert(i == 5);
}

nlc_test("iterate over null length range") {
    for (auto j : range(3u, 3u)) {
        static_assert(meta::is_same<decltype(j), unsigned int>);
        assert(false);
    }
}

nlc_test("iterate over null length reverse_range") {
    for (auto j : reverse_range(3u, 3u)) {
        static_assert(meta::is_same<decltype(j), unsigned int>);
        assert(false);
    }
}

nlc_test("iterate over reverse_range") {
    [[maybe_unused]] auto i = 7;
    for (auto j : reverse_range(7, 3)) {
        static_assert(meta::is_same<decltype(j), int>);
        assert(j == i--);
    }
    assert(i == 3);
}
