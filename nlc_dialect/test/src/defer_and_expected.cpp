/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/expected.hpp>
#include <nlc/test.hpp>

using namespace nlc;

static auto reg_1 = 0;
static auto reg_2 = 0;
static auto reg_3 = 0;
static auto reg_4 = 0;
static auto reg_5 = 0;
static auto reg_6 = 0;

class too_large {};
class asked_to_fail {};

static auto can_fail_1(int arg) -> expected<int, too_large> {
    auto v = 2 * arg;
    defer { reg_1 = arg; };
    error_defer { reg_2 = v; };
    success_defer { reg_3 = v + arg; };

    if (v > 10)
        return too_large {};
    return arg + 1;
}

static auto please_fail(bool b) -> expected<void, asked_to_fail> {
    if (b)
        return asked_to_fail {};
    return nlc::null;
}

static auto fail_and_propagate(int arg, bool fail, bool propagate) -> expected<int, asked_to_fail> {
    auto v = arg / 2;
    defer { ++reg_1; };
    error_defer { ++reg_2; };
    success_defer { ++reg_3; };

    auto tmp = please_fail(fail);

    defer { ++reg_4; };
    error_defer { ++reg_5; };
    success_defer { ++reg_6; };

    if (tmp.failed_with<asked_to_fail>()) {
        if (propagate)
            return asked_to_fail {};
        else
            return v - 1;
    }

    return v + 1;
}

nlc_test("defer and expected with no error propagation (success)") {
    reg_1 = 0;
    reg_2 = 0;
    reg_3 = 0;
    auto res = can_fail_1(3);
    nlc_check(res.succeeded());
    nlc_check(res.get() == 4);
    nlc_check(reg_1 == 3);
    nlc_check(reg_2 == 0);
    nlc_check(reg_3 == 9);
}

nlc_test("defer and expected with no error propagation (failure)") {
    reg_1 = 0;
    reg_2 = 0;
    reg_3 = 0;
    auto res = can_fail_1(7);
    nlc_check(res.failed_with<too_large>());
    nlc_check(reg_1 == 7);
    nlc_check(reg_2 == 14);
    nlc_check(reg_3 == 0);
}

nlc_test("defer and expected with error propagation (1)") {
    reg_1 = 0;
    reg_2 = 0;
    reg_3 = 0;
    reg_4 = 0;
    reg_5 = 0;
    reg_6 = 0;
    auto res = fail_and_propagate(7, false, false);
    nlc_check(res.succeeded());
    nlc_check(res.get() == 4);
    nlc_check(reg_1 == 1);
    nlc_check(reg_2 == 0);
    nlc_check(reg_3 == 1);
    nlc_check(reg_4 == 1);
    nlc_check(reg_5 == 0);
    nlc_check(reg_6 == 1);
}

nlc_test("defer and expected with error propagation (2)") {
    reg_1 = 0;
    reg_2 = 0;
    reg_3 = 0;
    reg_4 = 0;
    reg_5 = 0;
    reg_6 = 0;
    auto res = fail_and_propagate(5, false, true);
    nlc_check(res.succeeded());
    nlc_check(res.get() == 3);
    nlc_check(reg_1 == 1);
    nlc_check(reg_2 == 0);
    nlc_check(reg_3 == 1);
    nlc_check(reg_4 == 1);
    nlc_check(reg_5 == 0);
    nlc_check(reg_6 == 1);
}

nlc_test("defer and expected with error propagation (3)") {
    reg_1 = 0;
    reg_2 = 0;
    reg_3 = 0;
    reg_4 = 0;
    reg_5 = 0;
    reg_6 = 0;
    auto res = fail_and_propagate(7, true, false);
    nlc_check(res.succeeded());
    nlc_check(res.get() == 2);
    nlc_check(reg_1 == 1);
    nlc_check(reg_2 == 0);
    nlc_check(reg_3 == 1);
    nlc_check(reg_4 == 1);
    nlc_check(reg_5 == 0);
    nlc_check(reg_6 == 1);
}

nlc_test("defer and expected with error propagation (4)") {
    reg_1 = 0;
    reg_2 = 0;
    reg_3 = 0;
    reg_4 = 0;
    reg_5 = 0;
    reg_6 = 0;
    auto res = fail_and_propagate(5, true, true);
    nlc_check(res.failed());
    nlc_check(reg_1 == 1);
    nlc_check(reg_2 == 1);
    nlc_check(reg_3 == 0);
    nlc_check(reg_4 == 1);
    nlc_check(reg_5 == 1);
    nlc_check(reg_6 == 0);
}
