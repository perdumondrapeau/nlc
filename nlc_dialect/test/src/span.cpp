/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/span.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/test.hpp>

using namespace nlc;

template<typename T>
void testEqualSpans([[maybe_unused]] span<T> const & lhs, [[maybe_unused]] span<T> const & rhs) {
    nlc_check(lhs.ptr() == rhs.ptr());
    nlc_check(lhs.begin() == rhs.begin());
    nlc_check(lhs.end() == rhs.end());
    nlc_check(lhs.front() == rhs.front());
    nlc_check(lhs.back() == rhs.back());
    nlc_check(lhs.is_empty() == rhs.is_empty());
    nlc_check(lhs.size() == rhs.size());

    for (usize i = 0u; i < lhs.size(); ++i) {
        nlc_check(lhs[i] == rhs[i]);
    }
}

template<typename T> void testStackArraySpan() {
    static_assert(meta::is_same<meta::rm_const<T>, int>);

    int values[] = { 45, 62, 128, -789 };
    constexpr auto count = sizeof(values) / sizeof(values[0]);

    // Full span
    {
        span<T> fullSpan { values };

        nlc_check(fullSpan.begin() == &values[0]);
        nlc_check(*fullSpan.begin() == values[0]);
        nlc_check(fullSpan.end() == &values[0] + count);
        nlc_check(fullSpan.front() == values[0]);
        nlc_check(fullSpan.back() == values[count - 1]);
        nlc_check(fullSpan.size() == count);
        nlc_check(!fullSpan.is_empty());
        nlc_check(fullSpan.ptr() == &values[0]);

        for (usize i = 0u; i < count; ++i) {
            nlc_check(values[i] == fullSpan[i]);
        }

        // Non-const part
        if constexpr (meta::is_same<meta::rm_const<T>, T>) {
            fullSpan[2] = -12;
            nlc_check(values[2] == -12);
            fullSpan.front() = 2;
            nlc_check(values[0] == 2);
            fullSpan.back() = 0;
            nlc_check(values[count - 1] == 0);
            *fullSpan.begin() = 18;
            nlc_check(values[0] == 18);
            *fullSpan.ptr() = 7;
            nlc_check(values[0] == 7);
        }
    }

    // Partial span
    {
        constexpr auto halfCount = count / 2;
        static_assert(halfCount > 0);

        span<T> partialSpan { &values[0], halfCount };
        nlc_check(partialSpan.size() == halfCount);
        nlc_check(!partialSpan.is_empty());
    }

    // 1-element span
    {
        span<T> elemSpan { values[1] };
        nlc_check(elemSpan.size() == 1);
        nlc_check(elemSpan.front() == values[1]);
    }

    // Copy & move semantics
    {
        span<T> fullSpan { values };
        auto copy { fullSpan };
        testEqualSpans(copy, fullSpan);

        span<T> moved { nlc_move(fullSpan) };
        testEqualSpans(moved, copy);

        span<T> dummy { &values[2], &values[2] + 2 };
        copy = dummy;
        testEqualSpans(copy, dummy);
        moved = nlc_move(dummy);
        testEqualSpans(moved, copy);
    }
}

nlc_test("empty span") {
    span<int> s;
    nlc_check(s.is_empty());
    nlc_check(s.begin() == s.end());
}

nlc_test("mutable span") { testStackArraySpan<int>(); }

nlc_test("const span") { testStackArraySpan<int const>(); }

nlc_test("span_cast") {
    {
        auto constexpr count = 5;
        u32 ints[count] = {};
        auto const s = make_span(ints);
        nlc_check(span_cast<byte>(s).ptr() == static_cast<void *>(&ints));
        nlc_check(span_cast<byte>(s).size() == count * sizeof(u32));
    }

    {
        byte bytes[5] = {};  // not divisible by sizeof(u32)
        [[maybe_unused]] auto const s = make_span(bytes);
        nlc_expect_assert(span_cast<u32>(s));
    }
}

nlc_test("subspan") {
    int values[] = { 45, 62, 128, -789 };

    auto const full_span { make_span(values) };
    auto const right_half_span { full_span.subspan(2u) };
    nlc_check(right_half_span.size() == 2u);
    nlc_check(right_half_span.front() == full_span[2u]);

    auto const left_half_span { full_span.subspan(0u, 2u) };
    nlc_check(left_half_span.size() == 2u);
    nlc_check(left_half_span.back() == full_span[1u]);

    nlc_expect_assert(full_span.subspan(5u));      // begin out-of-bound
    nlc_expect_assert(full_span.subspan(5u, 7u));  // begin and end out-of-bound
    nlc_expect_assert(full_span.subspan(3u, 7u));  // end out-of-bound
    nlc_expect_assert(full_span.subspan(1u, 0u));  // begin > end
}
