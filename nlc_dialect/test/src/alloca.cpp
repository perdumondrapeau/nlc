/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/alloca.hpp>
#include <nlc/dialect/range.hpp>
#include <nlc/test.hpp>

auto get_sum_and_mult_by_two(nlc::span<int>) -> int;

nlc_test("alloca") {
    static auto constexpr size = 5;
    volatile auto previous = 3;
    auto array = nlc_stack_allocation(int, size);
    volatile auto next = 19;
    nlc_check(array.size() == size);

    auto const begin = array.begin();
    auto const end = array.end();

    for (auto i : nlc::range(0, array.size()))
        array[i] = static_cast<int>(i) + 1;

    nlc_check(get_sum_and_mult_by_two(array) == 1 + 2 + 3 + 4 + 5);

    nlc_check(array.begin() == begin);
    nlc_check(array.end() == end);
    nlc_check(previous == 3);
    nlc_check(next == 19);

    for (auto i : nlc::range(0, array.size()))
        nlc_check(array[i] == 2 * static_cast<int>(i + 1));
}
