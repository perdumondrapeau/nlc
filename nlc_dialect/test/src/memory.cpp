/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/buffer.hpp>
#include <nlc/dialect/memory.hpp>
#include <nlc/dialect/types.hpp>
#include <nlc/meta/traits.hpp>
#include <nlc/meta/units.hpp>
#include <nlc/test.hpp>

static inline auto checked_memmove(void * dest, void const * from, nlc::bytes<usize> size) -> void {
    // dest and from may overlap, so we keep track of the original values
    byte * const from_copy = new byte[nlc::rm_unit(size)];
    nlc::memcpy(from_copy, from, size);

    nlc::memmove(dest, from, size);

    auto const * const dest_byte_array = reinterpret_cast<byte const *>(dest);
    for (auto i = 0u; i < nlc::rm_unit(size); ++i) {
        nlc_check(dest_byte_array[i] == from_copy[i]);
    }

    delete[] from_copy;
}

static inline auto checked_memcpy(void * dest, void const * from, nlc::bytes<usize> size) -> void {
    nlc::memcpy(dest, from, size);

    auto const * const dest_byte_array = reinterpret_cast<byte const *>(dest);
    auto const * const from_byte_array = reinterpret_cast<byte const *>(from);
    for (auto i = 0u; i < nlc::rm_unit(size); ++i) {
        nlc_check(dest_byte_array[i] == from_byte_array[i]);
    }
}

template<typename T>
static inline auto check_memory(T const * dest, T const value, nlc::bytes<usize> const count) {
    for (auto i = 0u; i < nlc::rm_unit(count); ++i) {
        if constexpr (nlc::meta::is_arithmetic<T>)
            nlc_check_msg(dest[i] == value, "at ", i, ": got ", dest[i], ", expected ", value);
        else
            nlc_check_msg(dest[i] == value, "at ", i);
    }
}

template<typename T>
static inline auto checked_set_memory(nlc::span<T> dest, T const value) -> void {
    nlc::set_memory(dest, value);
    check_memory(dest.begin(), value, dest.size());
}

template<typename T>
static inline auto checked_set_memory(T * dest, T const & value, nlc::bytes<usize> const count) -> void {
    nlc::set_memory(dest, value, count);
    check_memory(dest, value, count);
}

template<typename T, usize count>
static inline auto checked_set_memory(T (&dest)[count], T const & value) -> void {
    nlc::set_memory(dest, value);
    check_memory(dest, value, count);
}

template<typename T>
static inline auto checked_memset(nlc::span<T> dest, byte const value) -> void {
    nlc::memset(dest, value);
    check_memory(reinterpret_cast<byte *>(dest.begin()), value, dest.size() * sizeof(T));
}

template<typename T>
static inline auto checked_memset(T * dest, byte const value, nlc::bytes<usize> const count) -> void {
    nlc::memset(dest, value, count);
    check_memory(reinterpret_cast<byte *>(dest), value, count * sizeof(T));
}

template<typename T, usize count>
static inline auto checked_memset(T (&dest)[count], byte const value) -> void {
    nlc::memset(dest, value);
    check_memory(reinterpret_cast<byte *>(dest), value, count * sizeof(T));
}

nlc_test("memmove") {
    nlc::buffer<u8, 256> buffer;
    for (auto i = 0u; i < buffer.size(); ++i) {
        buffer[i] = static_cast<u8>(i);
    }

    // Move [70, 79] to [50, 59] -> no overlap
    checked_memmove(buffer.ptr() + 50, buffer.ptr() + 70, 10);
    // Move [175, 249] to [125, 199] -> overlap
    checked_memmove(buffer.ptr() + 125, buffer.ptr() + 175, 75);
}

nlc_test("memcpy") {
    // Uninitialized memory
    {
        constexpr auto buffers_size = 512;
        nlc::buffer<u8, 512 / sizeof(u8)> src;
        nlc::buffer<double, 512 / sizeof(double)> dst;

        checked_memset<u8>(src.ptr(), 12, src.size());
        checked_memcpy(dst.ptr(), src.ptr(), buffers_size);
    }

    // Initialized memory
    i32 src[] = { -578, 0, 54, 36 };
    constexpr auto src_byte_size = sizeof(src);
    nlc::buffer<u32, src_byte_size / sizeof(i32)> dst;

    checked_memcpy(dst.ptr(), src, src_byte_size);
}

nlc_test("memset") {
    {
        nlc::buffer<u8, 512> buffer;
        checked_memset<u8>(buffer.ptr(), 0, buffer.size());
        checked_memset<u8>(buffer.ptr() + 100, 42, 217);
    }
    {
        constexpr auto buffer_size = 1024;
        float * const buffer = new float[buffer_size];
        auto const full_span = nlc::span(buffer, buffer + buffer_size);
        auto const sub_span = nlc::span(buffer + 680, buffer + 712);

        checked_memset(full_span, 32u);
        checked_memset(sub_span, 62u);

        delete[] buffer;
    }
    {
        char buffer[256];
        checked_memset(buffer, 'o');
    }
}

nlc_test("set_memory") {
    {
        nlc::buffer<u8, 512> buffer;
        checked_set_memory<u8>(buffer.ptr(), 0u, buffer.size());
        checked_set_memory<u8>(buffer.ptr() + 100, 42u, 217);
    }
    {
        constexpr auto buffer_size = 1024;
        float * const buffer = new float[buffer_size];
        auto const full_span = nlc::span(buffer, buffer + buffer_size);
        auto const sub_span = nlc::span(buffer + 680, buffer + 712);

        checked_set_memory(full_span, 2.5f);
        checked_set_memory(sub_span, 6.2f);

        delete[] buffer;
    }
    {
        struct Custom final {
            u32 i;
            u16 j;
            u8 k;
            u8 l;

            auto operator==(Custom const & rhs) const -> bool {
                return rhs.i == i && rhs.j == j && rhs.k == k && rhs.l == l;
            }
        };

        static_assert(sizeof(Custom) == 8u);

        static Custom const target { 56u, 3u, 127u, 5u };
        Custom items[64];
        checked_set_memory(nlc::make_span(items), target);

        for (auto const & item : items)
            nlc_check(item == target);
    }
    {
        char buffer[256];
        checked_set_memory(buffer, 'o');
    }
}
