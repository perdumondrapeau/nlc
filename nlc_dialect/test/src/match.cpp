/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/match.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test("match with fixed type functions") {
    [[maybe_unused]] auto v = variant<int, char, bool>(3);
    nlc_check(match(v)([](int) { return 1; },  //
                       [](char) { return 2; },
                       [](bool) { return 3; }) == 1);

    v = false;
    nlc_check(match(v)([](int) { return 1; },  //
                       [](char) { return 2; },
                       [](bool) { return 3; }) == 3);

    v = '3';
    nlc_check(match(v)([](int) { return 1; },  //
                       [](char) { return 2; },
                       [](bool) { return 3; }) == 2);
}

nlc_test("match with deduced type function") {
    [[maybe_unused]] auto v1 = variant<int, bool>(3);
    [[maybe_unused]] auto const v2 = v1;

    nlc_check(match(v1)([](auto && v) {
                  if constexpr (meta::is_rref<decltype(v)>)
                      return 1;
                  else if constexpr (meta::is_const_ref<decltype(v)>)
                      return 2;
                  else
                      return 3;
              }) == 3);

    nlc_check(match(v2)([](auto && v) {
                  if constexpr (meta::is_rref<decltype(v)>)
                      return 1;
                  else if constexpr (meta::is_const_ref<decltype(v)>)
                      return 2;
                  else
                      return 3;
              }) == 2);

    nlc_check(match(nlc_move(v1))([](auto && v) {
                  if constexpr (meta::is_rref<decltype(v)>)
                      return 1;
                  else if constexpr (meta::is_const_ref<decltype(v)>)
                      return 2;
                  else
                      return 3;
              }) == 1);
}

nlc_test("match on valueness") {
    [[maybe_unused]] auto v1 = variant<int, bool>(3);
    [[maybe_unused]] auto const v2 = v1;

    nlc_check(match(variant<int, bool>(4))([](auto &&) { return 1; },
                                           [](auto &) { return 2; },
                                           [](auto const &) { return 3; }) == 1);

    nlc_check(match(v1)([](auto &&) { return 1; },
                        [](auto &) { return 2; },
                        [](auto const &) { return 3; }) == 2);

    nlc_check(match(nlc_move(v1))([](auto &&) { return 1; },
                                  [](auto &) { return 2; },
                                  [](auto const &) { return 3; }) == 1);

    nlc_check(match(v2)([](auto &&) { return 1; },
                        [](auto &) { return 2; },
                        [](auto const &) { return 3; }) == 3);
}

nlc_test("match with captures") {
    [[maybe_unused]] auto i = 3;
    [[maybe_unused]] auto v1 = variant<int, bool>(false);
    match(v1)([&i](int) { i = 1; }, [&i](bool) { i = 2; });
    nlc_check(i == 2);
    v1 = 3;
    match(v1)([&i](int) { i = 1; }, [&i](bool) { i = 2; });
    nlc_check(i == 1);
}

nlc_test("match on two variants") {
    [[maybe_unused]] auto v1 = variant<int, bool>(3);
    [[maybe_unused]] auto v2 = variant<float, char>(3.f);

    nlc_check(match(v1, v2)([](int, float) { return 1; },
                            [](bool, char) { return 2; },
                            [](int, char) { return 3; },
                            [](bool, float) { return 4; }) == 1);

    v1 = true;

    nlc_check(match(v1, v2)([](int, float) { return 1; },
                            [](bool, char) { return 2; },
                            [](int, char) { return 3; },
                            [](bool, float) { return 4; }) == 4);

    v2 = 'c';
    nlc_check(match(v1, v2)([](int, float) { return 1; },
                            [](bool, char) { return 2; },
                            [](int, char) { return 3; },
                            [](bool, float) { return 4; }) == 2);

    v1 = 6;
    nlc_check(match(v1, v2)([](int, float) { return 1; },
                            [](bool, char) { return 2; },
                            [](int, char) { return 3; },
                            [](bool, float) { return 4; }) == 3);
}
