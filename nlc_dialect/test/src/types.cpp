/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/types.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test("size_of arithmetic types") {
    nlc_check(size_of<byte> == bytes<usize>(1));
    nlc_check(size_of<i8> == bytes<usize>(1));
    nlc_check(size_of<i16> == bytes<usize>(2));
    nlc_check(size_of<i32> == bytes<usize>(4));
    nlc_check(size_of<i64> == bytes<usize>(8));
    nlc_check(size_of<u8> == bytes<usize>(1));
    nlc_check(size_of<u16> == bytes<usize>(2));
    nlc_check(size_of<u32> == bytes<usize>(4));
    nlc_check(size_of<u64> == bytes<usize>(8));
    nlc_check(size_of<f32> == bytes<usize>(4));
    nlc_check(size_of<f64> == bytes<usize>(8));
}

nlc_test_todo("bytes<T> operations");
