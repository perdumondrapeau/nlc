/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/variant.hpp>
#include <nlc/test.hpp>

using namespace nlc;

struct Sum {
    Sum(int a, int b)
        : value(a + b) {}
    int value;
};

static auto destruct_test_1 = 0;
static auto destruct_test_2 = 0;

struct DestructTest1 {
    DestructTest1() = default;
    DestructTest1(DestructTest1 &&) = default;
    DestructTest1(DestructTest1 const &) = default;
    auto operator=(DestructTest1 &&) -> DestructTest1 & = default;
    auto operator=(DestructTest1 const &) -> DestructTest1 & = default;
    ~DestructTest1() { ++destruct_test_1; }
};

struct DestructTest2 {
    DestructTest2() = default;
    DestructTest2(DestructTest2 &&) = default;
    DestructTest2(DestructTest2 const &) = default;
    auto operator=(DestructTest2 &&) -> DestructTest2 & = default;
    auto operator=(DestructTest2 const &) -> DestructTest2 & = default;
    ~DestructTest2() { ++destruct_test_2; }
};

static auto copy_count = 0;
static auto move_count = 0;
static auto construct_by_copy = 0;
static auto construct_by_move = 0;

struct CopyMove {
    CopyMove() = default;
    CopyMove(CopyMove &&) { ++construct_by_move; }
    CopyMove(CopyMove const &) { ++construct_by_copy; }
    auto operator=(CopyMove &&) -> CopyMove & = default;
    auto operator=(CopyMove const &) -> CopyMove & = default;
};

enum class test_pass {
    first_pass,
    second_pass,
    third_pass,
};

nlc_test("assignation from rvalue") {
    auto v = variant<int, float, Sum>(4);
    nlc_check(v.contains<int>());
    nlc_check(!v.contains<float>());
    nlc_check(!v.contains<Sum>());
    nlc_check(v.as<int>() == 4);
    nlc_check(v.is<0>());
    nlc_check(!v.is<1>());
    nlc_check(!v.is<2>());
    nlc_check(v.get<0>() == 4);

    v = 3.f;
    nlc_check(!v.contains<int>());
    nlc_check(v.contains<float>());
    nlc_check(!v.contains<Sum>());
    nlc_check(v.as<float>() == 3.f);
    nlc_check(!v.is<0>());
    nlc_check(v.is<1>());
    nlc_check(!v.is<2>());
    nlc_check(v.get<1>() == 3.f);

    v = Sum(1, 4);
    nlc_check(!v.contains<int>());
    nlc_check(!v.contains<float>());
    nlc_check(v.contains<Sum>());
    nlc_check(v.as<Sum>().value == 5);
    nlc_check(!v.is<0>());
    nlc_check(!v.is<1>());
    nlc_check(v.is<2>());
    nlc_check(v.get<2>().value == 5);

    v = 8;
    nlc_check(v.contains<int>());
    nlc_check(!v.contains<float>());
    nlc_check(!v.contains<Sum>());
    nlc_check(v.as<int>() == 8);
    nlc_check(v.is<0>());
    nlc_check(!v.is<1>());
    nlc_check(!v.is<2>());
    nlc_check(v.get<0>() == 8);
}

nlc_test("assignation from rvalue") {
    auto v = variant<int, float, Sum>(3);
    nlc_check(v.contains<int>());
    {
        auto v2 = 7.f;
        v = v2;
        nlc_check(!v.contains<int>());
        nlc_check(v.contains<float>());
        nlc_check(!v.contains<Sum>());
        nlc_check(v.as<float>() == 7.f);
        nlc_check(!v.is<0>());
        nlc_check(v.is<1>());
        nlc_check(!v.is<2>());
        nlc_check(v.get<1>() == 7.f);
    }
    {
        auto v2 = Sum(-3, 1);
        v = v2;
        nlc_check(!v.contains<int>());
        nlc_check(!v.contains<float>());
        nlc_check(v.contains<Sum>());
        nlc_check(v.as<Sum>().value == -2);
        nlc_check(!v.is<0>());
        nlc_check(!v.is<1>());
        nlc_check(v.is<2>());
        nlc_check(v.get<2>().value == -2);
    }
    {
        auto v2 = 5;
        v = v2;
        nlc_check(v.contains<int>());
        nlc_check(!v.contains<float>());
        nlc_check(!v.contains<Sum>());
        nlc_check(v.as<int>() == 5);
        nlc_check(v.is<0>());
        nlc_check(!v.is<1>());
        nlc_check(!v.is<2>());
        nlc_check(v.get<0>() == 5);
    }
}

nlc_test("assignation from rvalue variant") {
    auto v = variant<int, float, Sum>(3);
    nlc_check(v.contains<int>());

    v = variant<int, float, Sum>(8.f);
    nlc_check(!v.contains<int>());
    nlc_check(v.contains<float>());
    nlc_check(!v.contains<Sum>());
    nlc_check(v.as<float>() == 8.f);
    nlc_check(!v.is<0>());
    nlc_check(v.is<1>());
    nlc_check(!v.is<2>());
    nlc_check(v.get<1>() == 8.f);

    v = variant<int, float, Sum>(-1);
    nlc_check(v.contains<int>());
    nlc_check(!v.contains<float>());
    nlc_check(!v.contains<Sum>());
    nlc_check(v.as<int>() == -1);
    nlc_check(v.is<0>());
    nlc_check(!v.is<1>());
    nlc_check(!v.is<2>());
    nlc_check(v.get<0>() == -1);

    v = variant<int, float, Sum>(Sum(-19, 9));
    nlc_check(!v.contains<int>());
    nlc_check(!v.contains<float>());
    nlc_check(v.contains<Sum>());
    nlc_check(v.as<Sum>().value == -10);
    nlc_check(!v.is<0>());
    nlc_check(!v.is<1>());
    nlc_check(v.is<2>());
    nlc_check(v.get<2>().value == -10);
}

nlc_test("assignation from const lvalue variant") {
    auto v = variant<int, float, Sum>(3);
    nlc_check(v.contains<int>());
    {
        auto const v2 = variant<int, float, Sum>(10.f);
        v = v2;
        nlc_check(!v.contains<int>());
        nlc_check(v.contains<float>());
        nlc_check(!v.contains<Sum>());
        nlc_check(v.as<float>() == 10.f);
        nlc_check(!v.is<0>());
        nlc_check(v.is<1>());
        nlc_check(!v.is<2>());
        nlc_check(v.get<1>() == 10.f);
    }

    {
        auto const v2 = variant<int, float, Sum>(-3);
        v = v2;
        nlc_check(v.contains<int>());
        nlc_check(!v.contains<float>());
        nlc_check(!v.contains<Sum>());
        nlc_check(v.as<int>() == -3);
        nlc_check(v.is<0>());
        nlc_check(!v.is<1>());
        nlc_check(!v.is<2>());
        nlc_check(v.get<0>() == -3);
    }

    {
        auto const v2 = variant<int, float, Sum>(Sum(-16, 9));
        v = v2;
        nlc_check(!v.contains<int>());
        nlc_check(!v.contains<float>());
        nlc_check(v.contains<Sum>());
        nlc_check(v.as<Sum>().value == -7);
        nlc_check(!v.is<0>());
        nlc_check(!v.is<1>());
        nlc_check(v.is<2>());
        nlc_check(v.get<2>().value == -7);
    }
}

nlc_test("assignation from mutable lvalue variant") {
    auto v = variant<int, float, Sum>(3);
    nlc_check(v.contains<int>());
    {
        auto v2 = variant<int, float, Sum>(14.f);
        v = v2;
        nlc_check(!v.contains<int>());
        nlc_check(v.contains<float>());
        nlc_check(!v.contains<Sum>());
        nlc_check(v.as<float>() == 14.f);
        nlc_check(!v.is<0>());
        nlc_check(v.is<1>());
        nlc_check(!v.is<2>());
        nlc_check(v.get<1>() == 14.f);
    }

    {
        auto v2 = variant<int, float, Sum>(9);
        v = v2;
        nlc_check(v.contains<int>());
        nlc_check(!v.contains<float>());
        nlc_check(!v.contains<Sum>());
        nlc_check(v.as<int>() == 9);
        nlc_check(v.is<0>());
        nlc_check(!v.is<1>());
        nlc_check(!v.is<2>());
        nlc_check(v.get<0>() == 9);
    }

    {
        auto v2 = variant<int, float, Sum>(Sum(16, 9));
        v = v2;
        nlc_check(!v.contains<int>());
        nlc_check(!v.contains<float>());
        nlc_check(v.contains<Sum>());
        nlc_check(v.as<Sum>().value == 25);
        nlc_check(!v.is<0>());
        nlc_check(!v.is<1>());
        nlc_check(v.is<2>());
        nlc_check(v.get<2>().value == 25);
    }
}

nlc_test("copy construction from const variant") {
    {
        auto const v2 = variant<int, float, Sum>(14.f);
        auto v3 = v2;
        nlc_check(!v3.contains<int>());
        nlc_check(v3.contains<float>());
        nlc_check(!v3.contains<Sum>());
        nlc_check(v3.as<float>() == 14.f);
        nlc_check(!v3.is<0>());
        nlc_check(v3.is<1>());
        nlc_check(!v3.is<2>());
        nlc_check(v3.get<1>() == 14.f);
    }
    {
        auto const v2 = variant<int, float, Sum>(9);
        auto v3 = v2;
        nlc_check(v3.contains<int>());
        nlc_check(!v3.contains<float>());
        nlc_check(!v3.contains<Sum>());
        nlc_check(v3.as<int>() == 9);
        nlc_check(v3.is<0>());
        nlc_check(!v3.is<1>());
        nlc_check(!v3.is<2>());
        nlc_check(v3.get<0>() == 9);
    }
    {
        auto const v2 = variant<int, float, Sum>(Sum(16, 9));
        auto v3 = v2;
        nlc_check(!v3.contains<int>());
        nlc_check(!v3.contains<float>());
        nlc_check(v3.contains<Sum>());
        nlc_check(v3.as<Sum>().value == 25);
        nlc_check(!v3.is<0>());
        nlc_check(!v3.is<1>());
        nlc_check(v3.is<2>());
        nlc_check(v3.get<2>().value == 25);
    }
}

nlc_test("move construction from const variant") {
    {
        auto const v2 = variant<int, float, Sum>(14.f);
        variant<int, float, Sum> v3 { nlc_move(v2) };
        nlc_check(!v3.contains<int>());
        nlc_check(v3.contains<float>());
        nlc_check(!v3.contains<Sum>());
        nlc_check(v3.as<float>() == 14.f);
        nlc_check(!v3.is<0>());
        nlc_check(v3.is<1>());
        nlc_check(!v3.is<2>());
        nlc_check(v3.get<1>() == 14.f);
    }
    {
        auto const v2 = variant<int, float, Sum>(9);
        variant<int, float, Sum> v3 { nlc_move(v2) };
        nlc_check(v3.contains<int>());
        nlc_check(!v3.contains<float>());
        nlc_check(!v3.contains<Sum>());
        nlc_check(v3.as<int>() == 9);
        nlc_check(v3.is<0>());
        nlc_check(!v3.is<1>());
        nlc_check(!v3.is<2>());
        nlc_check(v3.get<0>() == 9);
    }
    {
        auto const v2 = variant<int, float, Sum>(Sum(16, 9));
        variant<int, float, Sum> v3 { nlc_move(v2) };
        nlc_check(!v3.contains<int>());
        nlc_check(!v3.contains<float>());
        nlc_check(v3.contains<Sum>());
        nlc_check(v3.as<Sum>().value == 25);
        nlc_check(!v3.is<0>());
        nlc_check(!v3.is<1>());
        nlc_check(v3.is<2>());
        nlc_check(v3.get<2>().value == 25);
    }
}

nlc_test("move construction from mutable variant") {
    {
        auto v2 = variant<int, float, Sum>(14.f);
        variant<int, float, Sum> v3 { nlc_move(v2) };
        nlc_check(!v3.contains<int>());
        nlc_check(v3.contains<float>());
        nlc_check(!v3.contains<Sum>());
        nlc_check(v3.as<float>() == 14.f);
        nlc_check(!v3.is<0>());
        nlc_check(v3.is<1>());
        nlc_check(!v3.is<2>());
        nlc_check(v3.get<1>() == 14.f);
    }
    {
        auto v2 = variant<int, float, Sum>(9);
        variant<int, float, Sum> v3 { nlc_move(v2) };
        nlc_check(v3.contains<int>());
        nlc_check(!v3.contains<float>());
        nlc_check(!v3.contains<Sum>());
        nlc_check(v3.as<int>() == 9);
        nlc_check(v3.is<0>());
        nlc_check(!v3.is<1>());
        nlc_check(!v3.is<2>());
        nlc_check(v3.get<0>() == 9);
    }
    {
        auto v2 = variant<int, float, Sum>(Sum(16, 9));
        variant<int, float, Sum> v3 { nlc_move(v2) };
        nlc_check(!v3.contains<int>());
        nlc_check(!v3.contains<float>());
        nlc_check(v3.contains<Sum>());
        nlc_check(v3.as<Sum>().value == 25);
        nlc_check(!v3.is<0>());
        nlc_check(!v3.is<1>());
        nlc_check(v3.is<2>());
        nlc_check(v3.get<2>().value == 25);
    }
}

nlc_test("construction from lvalue") {
    {
        auto const v2 = 14.f;
        variant<int, float, Sum> v3 { v2 };
        nlc_check(!v3.contains<int>());
        nlc_check(v3.contains<float>());
        nlc_check(!v3.contains<Sum>());
        nlc_check(v3.as<float>() == 14.f);
        nlc_check(!v3.is<0>());
        nlc_check(v3.is<1>());
        nlc_check(!v3.is<2>());
        nlc_check(v3.get<1>() == 14.f);
    }
    {
        auto const v2 = 9;
        variant<int, float, Sum> v3 { v2 };
        nlc_check(v3.contains<int>());
        nlc_check(!v3.contains<float>());
        nlc_check(!v3.contains<Sum>());
        nlc_check(v3.as<int>() == 9);
        nlc_check(v3.is<0>());
        nlc_check(!v3.is<1>());
        nlc_check(!v3.is<2>());
        nlc_check(v3.get<0>() == 9);
    }
    {
        auto const v2 = Sum(16, 9);
        variant<int, float, Sum> v3 { v2 };
        nlc_check(!v3.contains<int>());
        nlc_check(!v3.contains<float>());
        nlc_check(v3.contains<Sum>());
        nlc_check(v3.as<Sum>().value == 25);
        nlc_check(!v3.is<0>());
        nlc_check(!v3.is<1>());
        nlc_check(v3.is<2>());
        nlc_check(v3.get<2>().value == 25);
    }
}

nlc_test("destruction") {
    nlc_check(destruct_test_1 == 0);
    nlc_check(destruct_test_2 == 0);
    {
        // Take caution to destructors of temporary values used to assign the variant
        auto v2 = variant<int, DestructTest1, DestructTest2>(4);
        nlc_check(destruct_test_1 == 0);
        nlc_check(destruct_test_2 == 0);
        v2 = DestructTest1();
        nlc_check(destruct_test_1 == 1);
        nlc_check(destruct_test_2 == 0);
        v2 = 5;
        nlc_check(destruct_test_1 == 2);
        nlc_check(destruct_test_2 == 0);
        v2 = DestructTest2();
        nlc_check(destruct_test_1 == 2);
        nlc_check(destruct_test_2 == 1);
        v2 = DestructTest2();
        nlc_check(destruct_test_1 == 2);
        nlc_check(destruct_test_2 == 2);
        v2 = DestructTest1();
        nlc_check(destruct_test_1 == 3);
        nlc_check(destruct_test_2 == 3);
    }
    nlc_check(destruct_test_1 == 4);
    nlc_check(destruct_test_2 == 3);
}

nlc_test("index and index_of") {
    auto v = variant<int, float, Sum>(3);
    nlc_check(v.contains<int>());
    v = 4.f;
    switch (v.index()) {
        case decltype(v)::index_of<int>: nlc_check(false); break;
        case decltype(v)::index_of<float>: nlc_check(true); break;
        case decltype(v)::index_of<Sum>: nlc_check(false); break;
        default: nlc_check(false); break;
    }

    auto ve = enum_variant<test_pass, int, float, double>(3.);
    switch (ve.index()) {
        case test_pass::first_pass: nlc_check(false); break;
        case test_pass::second_pass: nlc_check(false); break;
        case test_pass::third_pass: {
            nlc_check(ve.as<double>() == 3.);
            nlc_check(ve.is<test_pass::third_pass>());
        } break;
    }
}

nlc_test("copy and move") {
    {
        copy_count = move_count = construct_by_copy = construct_by_move = 0;
        [[maybe_unused]] auto c1 = variant<CopyMove, int> { CopyMove {} };
        nlc_check(copy_count == 0);
        nlc_check(move_count == 0);
        nlc_check(construct_by_copy == 0);
        nlc_check(construct_by_move == 1);
        copy_count = move_count = construct_by_copy = construct_by_move = 0;
        [[maybe_unused]] auto c2 = c1.as<CopyMove>();
        nlc_check(copy_count == 0);
        nlc_check(move_count == 0);
        nlc_check(construct_by_copy == 1);
        nlc_check(construct_by_move == 0);
        copy_count = move_count = construct_by_copy = construct_by_move = 0;
        [[maybe_unused]] auto c3 = nlc_move(c1).as<CopyMove>();
        nlc_check(copy_count == 0);
        nlc_check(move_count == 0);
        nlc_check(construct_by_copy == 0);
        nlc_check(construct_by_move == 1);
    }
    {
        copy_count = move_count = construct_by_copy = construct_by_move = 0;
        [[maybe_unused]] auto c1 = variant<CopyMove, int> { CopyMove {} };
        nlc_check(copy_count == 0);
        nlc_check(move_count == 0);
        nlc_check(construct_by_copy == 0);
        nlc_check(construct_by_move == 1);
        copy_count = move_count = construct_by_copy = construct_by_move = 0;
        [[maybe_unused]] auto c2 = c1.get<0>();
        nlc_check(copy_count == 0);
        nlc_check(move_count == 0);
        nlc_check(construct_by_copy == 1);
        nlc_check(construct_by_move == 0);
        copy_count = move_count = construct_by_copy = construct_by_move = 0;
        [[maybe_unused]] auto c3 = nlc_move(c1).get<0>();
        nlc_check(copy_count == 0);
        nlc_check(move_count == 0);
        nlc_check(construct_by_copy == 0);
        nlc_check(construct_by_move == 1);
    }
}

nlc_test("comparisons") {
    variant<u32, bool, char> v1 { 123456u };
    variant<u32, bool, char> v2 { 'a' };
    nlc_check(v1 != v2);
    nlc_check(!(v1 == v2));
    v1 = 'a';
    nlc_check(v1 == v2);
    nlc_check(!(v1 != v2));
    v2 = false;
    nlc_check(v1 != v2);
    nlc_check(!(v1 == v2));
    v1 = true;
    nlc_check(v1 != v2);
    nlc_check(!(v1 == v2));
    v2 = true;
    nlc_check(v1 == v2);
    nlc_check(!(v1 != v2));
}
