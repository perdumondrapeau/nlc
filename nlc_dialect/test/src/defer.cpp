/* Copyright © 2019-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/defer.hpp>
#include <nlc/test.hpp>

[[maybe_unused]] static auto counter = 0;
[[maybe_unused]] static auto error_count = 0;
[[maybe_unused]] static auto success_count = 0;

static auto raise_error() { ++nlc::errors_impl::get_error_count(); }

nlc_test("defer when success") {
    {
        counter = 0;
        error_count = 0;
        success_count = 0;

        defer { ++counter; };
        error_defer { ++error_count; };
        success_defer { ++success_count; };

        nlc_check(error_count == 0);
        nlc_check(success_count == 0);
        nlc_check(counter == 0);
    }
    nlc_check(error_count == 0);
    nlc_check(success_count == 1);
    nlc_check(counter == 1);
}

nlc_test("defer when error") {
    {
        counter = 0;
        error_count = 0;
        success_count = 0;

        defer { ++counter; };
        error_defer { ++error_count; };
        success_defer { ++success_count; };

        raise_error();

        nlc_check(error_count == 0);
        nlc_check(success_count == 0);
        nlc_check(counter == 0);
    }
    nlc_check(error_count == 1);
    nlc_check(success_count == 0);
    nlc_check(counter == 1);
}
