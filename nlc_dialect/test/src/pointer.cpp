/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/pointer.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/test.hpp>

using namespace nlc;
using namespace nlc::meta;

struct Foo {
    int a;
    Foo(int a_)
        : a { a_ } {}
    [[maybe_unused]] auto operator==(Foo const & other) const { return a == other.a; }

    auto increment() -> void { ++a; }
};

nlc_test("construction from raw pointer") {
    auto a = 123;
    auto const b = 456;
    Foo foo(12);

    auto pa = pointer(&a);
    auto pb = pointer(&b);
    auto ptr = pointer(&foo);

    nlc_check(is_same<decltype(pa), pointer<int>>);
    nlc_check(is_same<decltype(pb), pointer<int const>>);
    nlc_check(is_same<decltype(ptr), pointer<Foo>>);
}

nlc_test("comparisons") {
    auto a = 4, b = 4;
    auto pa = pointer(&a), pa2 = pointer(&a), pb = pointer(&b);
    nlc_check(pa == &a);
    nlc_check(&a == pa);
    nlc_check(pa != &b);
    nlc_check(&b != pa);
    nlc_check(pa == pa2);
    nlc_check(pa2 == pa);
    nlc_check(pa != pb);
    nlc_check(pb != pa);
}

nlc_test("construction by copy") {
    auto a = 123;
    auto pa = pointer(&a);
    auto pa2 = pa;
    nlc_check(is_same<decltype(pa), decltype(pa2)>);
    nlc_check(pa == pa2);
    nlc_check(pa2 == &a);
}

nlc_test("construction by move") {
    auto a = 123;
    auto pa = pointer(&a);
    auto pa2 = nlc_move(pa);
    nlc_check(is_same<decltype(pa), decltype(pa2)>);
    nlc_check(pa == pa2);
    nlc_check(pa2 == &a);
}

nlc_test("assignation") {
    auto a = 3, b = 123;
    auto pa = pointer(&a), pb = pointer(&b);

    nlc_check(pa == &a);
    nlc_check(pb == &b);
    nlc_check(pb != &a);

    pb = pa;
    nlc_check(pa == &a);
    nlc_check(pb == &a);
}

nlc_test("dereferencement") {
    auto a = 3;
    auto pa = pointer(&a);
    nlc_check(*pa == 3);
    *pa = 5;
    nlc_check(a == 5);

    auto b = Foo(10);
    auto pb = pointer(&b);
    nlc_check(pb->a == 10);
    pb->a = 15;
    nlc_check(b.a == 15);
}

nlc_test("dereferencement of non-const object") {
    auto a = Foo { 3 };
    auto const pa = pointer(&a);
    pa->increment();
    nlc_check(*pa == 4);
    (*pa).increment();
    nlc_check(*pa == 5);
}

nlc_test("get") {
    auto a = Foo { 3 };
    auto const pa = pointer(&a);
    nlc_check(pa.get() == &a);
    pa.get()->increment();
    nlc_check(pa->a == 4);
}

nlc_test("construction on invalid ptr") {
    nlc_expect_assert([]() {
        auto a = 0;
        auto b = &a;
        b = nullptr;
        [[maybe_unused]] auto const c = pointer(b);
    }());
}

nlc_test("construction on invalid ptr") {
    nlc_expect_assert([]() {
        auto a = 0;
        auto b = &a;
        b = nullptr;
        auto c = pointer(&a);
        c = b;
    }());
}
