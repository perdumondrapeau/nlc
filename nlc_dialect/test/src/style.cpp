/* Copyright © 2025-2025
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/style.hpp>

using namespace nlc;
using namespace nlc::style;

static auto test_style(string_view name, common_style style, common_style reset) {
    print(name, ": ", style, "styled", reset, "norm", style, "styled", reset_all, "norm");
}

static auto test_color(string_view name, common_style color, common_style reset) {
    print(color, name, reset, name, color, name, reset_all, name);
}

auto main() -> int {
    test_style("bold", bold, no_bold);
    test_style("underline", underline, no_underline);
    test_style("inverse", inverse, no_inverse);

    test_color("black", bg::black, bg::by_default);
    test_color("red", bg::red, bg::by_default);
    test_color("green", bg::green, bg::by_default);
    test_color("yellow", bg::yellow, bg::by_default);
    test_color("blue", bg::blue, bg::by_default);
    test_color("magenta", bg::magenta, bg::by_default);
    test_color("cyan", bg::cyan, bg::by_default);
    test_color("white", bg::white, bg::by_default);

    test_color("grey", bg::grey, bg::by_default);
    test_color("bright_red", bg::bright_red, bg::by_default);
    test_color("bright_green", bg::bright_green, bg::by_default);
    test_color("bright_yellow", bg::bright_yellow, bg::by_default);
    test_color("bright_blue", bg::bright_blue, bg::by_default);
    test_color("bright_magenta", bg::bright_magenta, bg::by_default);
    test_color("bright_cyan", bg::bright_cyan, bg::by_default);
    test_color("bright_white", bg::bright_white, bg::by_default);

    test_color("black", fg::black, fg::by_default);
    test_color("red", fg::red, fg::by_default);
    test_color("green", fg::green, fg::by_default);
    test_color("yellow", fg::yellow, fg::by_default);
    test_color("blue", fg::blue, fg::by_default);
    test_color("magenta", fg::magenta, fg::by_default);
    test_color("cyan", fg::cyan, fg::by_default);
    test_color("white", fg::white, fg::by_default);

    test_color("grey", fg::grey, fg::by_default);
    test_color("bright_red", fg::bright_red, fg::by_default);
    test_color("bright_green", fg::bright_green, fg::by_default);
    test_color("bright_yellow", fg::bright_yellow, fg::by_default);
    test_color("bright_blue", fg::bright_blue, fg::by_default);
    test_color("bright_magenta", fg::bright_magenta, fg::by_default);
    test_color("bright_cyan", fg::bright_cyan, fg::by_default);
    test_color("bright_white", fg::bright_white, fg::by_default);

    print(fg::custom(238, 76, 171), "custom foreground", fg::by_default);
    print(bg::custom(198, 176, 171), "custom background", bg::by_default);
}
