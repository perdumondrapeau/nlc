/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/utf8.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test("str_size") {
    nlc_check(str_size(nullptr) == 0u);
    nlc_check(str_size("") == 0u);
    nlc_check(str_size("a") == 1u);
    nlc_check(str_size("abcd") == 4u);
    nlc_check(str_size("éèù") == 6u);
}

nlc_test("str_len") {
    nlc_check(str_len("") == 0u);
    nlc_check(str_len("a") == 1u);
    nlc_check(str_len("abcd") == 4u);
    nlc_check(str_len("éèù") == 3u);
    nlc_check(str_len("", 0) == 0u);
    nlc_check(str_len("abec", 3) == 3u);
    nlc_check(str_len("éeéb", 5) == 3u);
}

nlc_test("str_cmp") {
    nlc_check(str_cmp("", "") == comparison_result::equal);
    nlc_check(str_cmp("a", "a") == comparison_result::equal);
    nlc_check(str_cmp("abcd", "abcd") == comparison_result::equal);
    nlc_check(str_cmp("abc", "abcd") == comparison_result::less);
    nlc_check(str_cmp("abcd", "abc") == comparison_result::more);
    nlc_check(str_cmp("abcd", "bcde") == comparison_result::less);
    nlc_check(str_cmp("bcde", "abcd") == comparison_result::more);
}

nlc_test("codepoints") {
    constexpr codepoint_type a { 'u' };
    [[maybe_unused]] constexpr auto b { a.value() };

    nlc_check(codepoint("") == '\0');
    nlc_check(codepoint("a") == 'a');
    nlc_check(codepoint("à") == U'\u00E0');
    nlc_check(codepoint("abcd") == 'a');
    nlc_check(codepoint("àbcd") == u'\u00E0');
    [[maybe_unused]] char const * s0 = "abcd";
    [[maybe_unused]] char const * s1 = "àbcd";
    [[maybe_unused]] char const * c0 = nullptr;
    nlc_check(codepoint(s0, &c0) == u'a');
    nlc_check(c0 == s0 + 1);
    nlc_check(codepoint(s1, &c0) == U'\u00E0');
    nlc_check(c0 == s1 + 2);
}

nlc_test("is_valid_utf8") {
    // 4 bytes
    constexpr char invalid_4_bytes[3] = { static_cast<char>(0b1111'0000),
                                          static_cast<char>(0b1000'0000),
                                          static_cast<char>(0b1000'0000) };
    static_assert(first_codepoint_size(invalid_4_bytes) == 4u);
    // is_valid_utf8(invalid_4_bytes) would overflow
    static_assert(is_valid_utf8(invalid_4_bytes, 3u) == false);

    constexpr char const * valid_4_bytes = "𒀁";
    static_assert(first_codepoint_size(valid_4_bytes) == 4u);
    nlc_check(is_valid_utf8(valid_4_bytes));
    static_assert(is_valid_utf8(valid_4_bytes, 4u));

    // 3 bytes
    constexpr char invalid_3_bytes[2] = { static_cast<char>(0b1110'0000),
                                          static_cast<char>(0b1000'0000) };
    static_assert(first_codepoint_size(invalid_3_bytes) == 3u);
    // is_valid_utf8(invalid_3_bytes) would overflow
    static_assert(is_valid_utf8(invalid_3_bytes, 2u) == false);

    constexpr char const * valid_3_bytes = "इ";
    static_assert(first_codepoint_size(valid_3_bytes) == 3u);
    nlc_check(is_valid_utf8(valid_3_bytes));
    static_assert(is_valid_utf8(valid_3_bytes, 3u));

    // 2 bytes
    constexpr char invalid_2_bytes[1] = { static_cast<char>(0b1100'0000) };
    static_assert(first_codepoint_size(invalid_2_bytes) == 2u);
    // is_valid_utf8(invalid_2_bytes) would overflow
    static_assert(is_valid_utf8(invalid_2_bytes, 1u) == false);

    constexpr char const * valid_2_bytes = "Ŋ";
    static_assert(first_codepoint_size(valid_2_bytes) == 2u);
    nlc_check(is_valid_utf8(valid_2_bytes));
    static_assert(is_valid_utf8(valid_2_bytes, 2u));

    // 1 byte
    constexpr char invalid_1_byte[1] = { static_cast<char>(0b1000'0001) };
    static_assert(first_codepoint_size(invalid_1_byte) == 1u);
    nlc_check(is_valid_utf8(invalid_1_byte) == false);
    static_assert(is_valid_utf8(invalid_1_byte, 1u) == false);

    constexpr char const * valid_1_byte = "a";
    static_assert(first_codepoint_size(valid_1_byte) == 1u);
    nlc_check(is_valid_utf8(valid_1_byte));
    static_assert(is_valid_utf8(valid_1_byte, 1u));
}
