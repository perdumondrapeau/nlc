/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/tuple.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/test.hpp>

using namespace nlc;

[[maybe_unused]] static auto sum4(int a, int b, float c, unsigned d) {
    return a + b + static_cast<int>(c) + static_cast<int>(d);
}

auto constexpr to_float = [](auto a) { return static_cast<float>(a); };

nlc_test("empty tuple") {
    auto t = tuple();
    nlc_check(meta::is_same<decltype(t), tuple<>>);
    nlc_check(meta::is_same<decltype(t)::types, meta::list<>>);
}

nlc_test("tuple of size 1") {
    auto t = tuple(1);
    nlc_check(meta::is_same<decltype(t), tuple<int>>);
    nlc_check(meta::is_same<decltype(t)::types, meta::list<int>>);
    nlc_check(meta::is_same<decltype(t.get<0>()), int &>);
    nlc_check(t.get<0>() == 1);
}

nlc_test("non empty tuple") {
    auto t0 = tuple(2, 4, 6.f, 8u);
    nlc_check(meta::is_same<decltype(t0), tuple<int, int, float, unsigned int>>);
    nlc_check(meta::is_same<decltype(t0)::types, meta::list<int, int, float, unsigned int>>);
    nlc_check(fold([](auto l, auto r) { return static_cast<int>(l) + static_cast<int>(r); }, 3, t0) ==
              23);
    nlc_check(apply(sum4, t0) == 20);
    nlc_check(t0.get<1>() == 4);
    nlc_check(meta::is_same<decltype(t0.get<3>()), unsigned int &>);

    auto t1 = transform(t0, to_float);
    nlc_check(meta::is_same<decltype(t1), tuple<float, float, float, float>>);
}

// static_assert(meta::is_same<magic<decltype(to_float), int, int, int>, tuple<float, float,
// float>>);

/*
auto sum2 = [] (int a, int b) { return a + b; };
auto sum3 = [] (auto a, auto b, auto c) { return a + b + c; };
static_assert(meta::is_same<ret_info<decltype(sum2)>::return_type<int, int>, int>);
static_assert(meta::is_same<ret_info<decltype(sum3)>::return_type<int, int, int>, int>);
static_assert(meta::is_same<ret_info<decltype(sum3)>::return_type<float, float, float>, float>);
static_assert(meta::is_same<ret_info<decltype(sum4)>::return_type<int, int, int, int>, int>);
*/
