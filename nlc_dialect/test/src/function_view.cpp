/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/function_view.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/test.hpp>

using namespace nlc;

[[maybe_unused]] static auto sum(int a, int b) { return a + b; }

[[maybe_unused]] static auto applyOp(function_view<int(int, int)> foo, int a, int b) {
    return foo(a, b);
}

nlc_test("function_view") {
    nlc_check(sum(4, 3) == 7);
    nlc_check(applyOp(sum, 3, 5) == 8);
    nlc_check(applyOp([](auto a, auto b) { return a * b; }, 2, 9) == 18);
    [[maybe_unused]] auto i = 4;
    nlc_check(applyOp([i](auto a, auto b) { return a * (b + i); }, 2, -1) == 6);
}

[[maybe_unused]] static auto test_sum_in_span(nlc::span<nlc::function_view<int(int, int)>> foos,
                                              int a,
                                              int b) {
    for (auto const & foo : foos)
        nlc_check(foo(a, b) == a + b);
}

nlc_test("function_view over simple function pointer") {
    {
        nlc::function_view<int(int, int)> foos[] = { sum };
        test_sum_in_span(foos, 2, 76);
    }
    {
        nlc::function_view<int(int, int)> foos[] = { &sum };
        test_sum_in_span(foos, 2, 21);
    }
    nlc_check(true);
}
