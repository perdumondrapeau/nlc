/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/traits.hpp>

#include "assert.hpp"

namespace nlc {

template<typename T> class range final {
    static_assert(meta::is_integral<T>);

  public:
    class iterator final {
        friend class range;

      public:
        auto operator*() const -> T { return value; }
        auto operator++() -> iterator & {
            ++value;
            return *this;
        }
        auto operator!=(iterator const & o) { return value != o.value; }

      private:
        iterator(T v)
            : value(v) {}

      private:
        T value;
    };

  public:
    range(T b, T e)
        : _begin(b)
        , _end(e) {
        nlc_assert(b <= e);
    }

    auto begin() const { return iterator(_begin); }
    auto end() const { return iterator(_end); }

  private:
    T _begin;
    T _end;
};

template<typename T> class reverse_range final {
    static_assert(meta::is_integral<T>);

  public:
    class iterator final {
        friend class reverse_range;

      public:
        auto operator*() const -> T { return value; }
        auto operator++() -> iterator & {
            --value;
            return *this;
        }
        auto operator!=(iterator const & o) { return o.value != value; }

      private:
        iterator(T v)
            : value(v) {}

      private:
        T value;
    };

  public:
    reverse_range(T b, T e)
        : _begin(b)
        , _end(e) {
        nlc_assert(e <= b);
    }

    auto begin() const { return iterator(_begin); }
    auto end() const { return iterator(_end); }

  private:
    T _begin;
    T _end;
};

template<typename T, typename Q> range(T, Q) -> range<meta::rm_const_ref<Q>>;
template<typename T, typename Q> reverse_range(T, Q) -> reverse_range<meta::rm_const_ref<Q>>;

}  // namespace nlc
