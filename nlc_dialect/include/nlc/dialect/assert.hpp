/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/sanity/platforms.hpp>

#include "basic_output.hpp"

#ifdef NDEBUG
#    define breakpoint() (void)0
#else
#    define breakpoint() __builtin_trap()
#endif

#define nlc_unreachable                                    \
    do {                                                   \
        nlc_assert_msg(false, "reached unreachable code"); \
        __builtin_unreachable();                           \
    } while (false)

namespace nlc {

[[nodiscard]] auto assert_occurred(char const * condition,
                                   char const * file,
                                   char const * func,
                                   int line,
                                   char const * data) -> bool;

using assert_callback_type = decltype(assert_occurred) *;
auto set_assert_callback(assert_callback_type) -> void;
auto reset_assert_callback() -> void;

namespace assert_impl {
#ifndef NDEBUG
    template<typename T> struct assert_var {
        char const * name;
        T const & var;
    };

    template<typename T> auto format(log_entry & entry, assert_var<T> const & arg) -> void {
        entry.append("\n - ");
        entry.append(arg.name);
        entry.append(" : ");
        entry.append(arg.var);
    }

    template<typename> struct is_assert_var_impl final {
        static constexpr auto value = false;
    };
    template<typename T> struct is_assert_var_impl<assert_var<T> > final {
        static constexpr auto value = true;
    };
    template<typename T> inline constexpr auto is_assert_var = is_assert_var_impl<T>::value;
#endif
}  // namespace assert_impl

}  // namespace nlc

#ifdef NDEBUG
#    define nlc_assert(cond)    (void)0
#    define nlc_assert_msg(...) (void)0
#    define nlc_dump_var(arg)   (void)0
#else
#    define nlc_assert(cond)                                                                  \
        do {                                                                                  \
            if (!(cond)) {                                                                    \
                if (::nlc::assert_occurred(#cond, __FILE__, __FUNCTION__, __LINE__, nullptr)) \
                    breakpoint();                                                             \
            }                                                                                 \
        } while (false)

#    define nlc_assert_msg(cond, ...)                                                  \
        do {                                                                           \
            if (!(cond)) {                                                             \
                auto nlc_assert_msg_entry = ::nlc::log_entry {};                       \
                [&nlc_assert_msg_entry](auto... nlc_assert_msg_args) {                 \
                    ((::nlc::assert_impl::is_assert_var<decltype(nlc_assert_msg_args)> \
                          ? static_cast<void>(0)                                       \
                          : nlc_assert_msg_entry.append(nlc_assert_msg_args)),         \
                     ...);                                                             \
                    ((::nlc::assert_impl::is_assert_var<decltype(nlc_assert_msg_args)> \
                          ? nlc_assert_msg_entry.append(nlc_assert_msg_args)           \
                          : static_cast<void>(0)),                                     \
                     ...);                                                             \
                }(__VA_ARGS__);                                                        \
                if (::nlc::assert_occurred(#cond,                                      \
                                           __FILE__,                                   \
                                           __FUNCTION__,                               \
                                           __LINE__,                                   \
                                           nlc_assert_msg_entry.c_str()))              \
                    breakpoint();                                                      \
            }                                                                          \
        } while (false)

#    define nlc_dump_var(arg) \
        ::nlc::assert_impl::assert_var<decltype(arg)> { #arg, arg }
#endif
