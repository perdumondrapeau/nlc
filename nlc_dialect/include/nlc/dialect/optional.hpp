/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/basics.hpp>

#include "arithmetic_types.hpp"
#include "assert.hpp"
#include "memory.hpp"
#include "new.hpp"
#include "null.hpp"

namespace nlc {

template<typename T> class [[nodiscard]] optional final {
    // Members
    uninitialized<T> _value;
    bool _present;

  public:
    // Constructors
    constexpr optional() = delete;
    constexpr optional(null_type);
    constexpr optional(optional const & other);
    constexpr optional(optional && rvalue);
    template<typename... Args> constexpr optional(Args &&... args);
    ~optional();

    // Assignements
    constexpr auto operator=(optional const & other) & -> optional &;
    constexpr auto operator=(optional && other) & -> optional &;
    constexpr auto operator=(null_type) & -> optional &;
    template<typename... Args> constexpr auto operator=(Args &&... args) & -> optional &;
    template<typename... Args> constexpr auto set(Args &&... args) & -> void;

    // Comparisons
    template<typename U>
    friend constexpr auto operator==(optional<U> const & lhs, optional<U> const & rhs) -> bool;
    template<typename U>
    friend constexpr auto operator==(optional<U> const & lhs, null_type) -> bool;
    template<typename U>
    friend constexpr auto operator==(null_type, optional<U> const & rhs) -> bool;
    template<typename U>
    friend constexpr auto operator!=(optional<U> const & lhs, optional<U> const & rhs) -> bool;
    template<typename U>
    friend constexpr auto operator!=(optional<U> const & lhs, null_type) -> bool;
    template<typename U>
    friend constexpr auto operator!=(null_type, optional<U> const & rhs) -> bool;
    template<typename U, typename V>
    friend constexpr auto operator==(optional<U> const & lhs, V const & rhs) -> bool;
    template<typename U, typename V>
    friend constexpr auto operator==(U const & lhs, optional<V> const & rhs) -> bool;
    template<typename U, typename V>
    friend constexpr auto operator!=(optional<U> const & lhs, V const & rhs) -> bool;
    template<typename U, typename V>
    friend constexpr auto operator!=(U const & lhs, optional<V> const & rhs) -> bool;

    // Accessors
    [[nodiscard]] constexpr auto operator*() const & -> T const &;
    [[nodiscard]] constexpr auto operator*() & -> T &;
    [[nodiscard]] constexpr auto operator*() && -> T;

    [[nodiscard]] constexpr auto operator->() const -> T const *;
    [[nodiscard]] constexpr auto operator->() -> T *;

    [[nodiscard]] constexpr auto extract() -> T;
};

// Constructors --------------------------------------------------------

template<typename T>
constexpr optional<T>::optional(null_type)
    : _present { false } {}

template<typename T>
constexpr optional<T>::optional(optional const & other)
    : _present { other._present } {
    if (_present)
        _value.construct(other._value.value());
}

template<typename T>
constexpr optional<T>::optional(optional && other)
    : _present { other._present } {
    if (_present) {
        _value.copy_memory_from(other._value);
        other._present = false;
    }
}

template<typename T>
template<typename... Args>
constexpr optional<T>::optional(Args &&... args)
    : _present { true } {
    _value.construct(nlc_fwd(args)...);
}

template<typename T> optional<T>::~optional() {
    if (_present)
        _value.destroy();
}

// Assignements --------------------------------------------------------

template<typename T> constexpr auto optional<T>::operator=(optional const & other) & -> optional & {
    if (_present) {
        if (other._present) {
            _value.value() = other._value.value();
        } else {
            _value.destroy();
            _present = false;
        }
    } else {
        if (other._present) {
            _value.construct(other._value.value());
            _present = true;
        } else {
            // nothing to do
        }
    }

    return *this;
}

template<typename T> constexpr auto optional<T>::operator=(optional && other) & -> optional & {
    if (_present)
        _value.destroy();

    _present = other._present;

    if (other._present) {
        _value.copy_memory_from(other._value);
        other._present = false;
    }

    return *this;
}

template<typename T> constexpr auto optional<T>::operator=(null_type) & -> optional & {
    if (_present) {
        _value.destroy();
        _present = false;
    }

    return *this;
}

template<typename T>
template<typename... Args>
constexpr auto optional<T>::operator=(Args &&... args) & -> optional & {
    set(nlc_fwd(args)...);
    return *this;
}

template<typename T>
template<typename... Args>
constexpr auto optional<T>::set(Args &&... args) & -> void {
    if (_present)
        _value.destroy();

    _value.construct(nlc_fwd(args)...);

    _present = true;
}

// Comparisons ---------------------------------------------------------

template<typename T>
[[nodiscard]] constexpr auto operator==(optional<T> const & lhs, optional<T> const & rhs) -> bool {
    if (lhs._present) {
        if (rhs._present)
            return lhs._value.value() == rhs._value.value();
        return false;
    }
    return !rhs._present;
}

template<typename T>
[[nodiscard]] constexpr auto operator==(optional<T> const & lhs, null_type) -> bool {
    return !lhs._present;
}

template<typename T>
[[nodiscard]] constexpr auto operator==(null_type, optional<T> const & rhs) -> bool {
    return !rhs._present;
}

template<typename T>
[[nodiscard]] constexpr auto operator!=(optional<T> const & lhs, optional<T> const & rhs) -> bool {
    return !operator==(lhs, rhs);
}

template<typename T>
[[nodiscard]] constexpr auto operator!=(optional<T> const & lhs, null_type) -> bool {
    return lhs._present;
}

template<typename T>
[[nodiscard]] constexpr auto operator!=(null_type, optional<T> const & rhs) -> bool {
    return rhs._present;
}

template<typename U, typename V>
[[nodiscard]] constexpr auto operator==(optional<U> const & lhs, V const & rhs) -> bool {
    return lhs._present && (*lhs == rhs);
}

template<typename U, typename V>
[[nodiscard]] constexpr auto operator==(U const & lhs, optional<V> const & rhs) -> bool {
    return rhs._present && (lhs == *rhs);
}

template<typename U, typename V>
[[nodiscard]] constexpr auto operator!=(optional<U> const & lhs, V const & rhs) -> bool {
    return (lhs._present == false) || (*lhs != rhs);
}
template<typename U, typename V>
[[nodiscard]] constexpr auto operator!=(U const & lhs, optional<V> const & rhs) -> bool {
    return (rhs._present == false) || (lhs != *rhs);
}

// Accessors -----------------------------------------------------------

template<typename T> constexpr auto optional<T>::operator*() const & -> T const & {
    nlc_assert(_present);
    return _value.value();
}

template<typename T> constexpr auto optional<T>::operator*() & -> T & {
    nlc_assert(_present);
    return _value.value();
}

template<typename T> constexpr auto optional<T>::operator*() && -> T { return extract(); }

template<typename T> constexpr auto optional<T>::operator->() const -> T const * {
    nlc_assert(_present);
    return _value.address();
}

template<typename T> constexpr auto optional<T>::operator->() -> T * {
    nlc_assert(_present);
    return _value.address();
}

template<typename T> constexpr auto optional<T>::extract() -> T {
    nlc_assert(_present);
    auto res = nlc_move(_value).value();
    _present = false;
    _value.destroy();
    return res;
}

// Deduction guides ----------------------------------------------------

template<typename T> optional(T) -> optional<T>;

}  // namespace nlc
