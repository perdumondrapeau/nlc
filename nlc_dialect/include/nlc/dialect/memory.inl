/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/meta/units.hpp>

namespace nlc {

namespace impl {
    template<typename T>
    static inline auto set_memory(T * const dest, T const value, bytes<usize> const count) -> void {
        auto * const typed_dest = reinterpret_cast<T *>(dest);
        for (auto i = 0u; i < rm_unit(count); ++i)
            typed_dest[i] = value;
    }
}  // namespace impl

template<typename T>
inline auto set_memory(T * const dest, T const value, bytes<usize> const count) -> void {
    static_assert(sizeof(T) == 1u || sizeof(T) == 2u || sizeof(T) == 4u || sizeof(T) == 8u);

    if (rm_unit(count) == 0u)
        return;

    nlc_assert(dest != nullptr);

#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic push
#    if defined(__clang__)
#        pragma GCC diagnostic ignored "-Wundefined-reinterpret-cast"  // reinterpret_cast from
                                                                       // 'float_t' to 'int_t &' has
                                                                       // undefined behavior
#    elif defined(__GNUC__)
#        pragma GCC diagnostic ignored "-Wstrict-aliasing"  // dereferencing type-punned pointer
                                                            // will break strict-aliasing rules
#    endif
#endif
    if constexpr (sizeof(T) == 1u)
        memset(reinterpret_cast<void *>(dest), reinterpret_cast<byte const &>(value), count);
    else if constexpr (sizeof(T) == 2u)
        impl::set_memory(reinterpret_cast<u16 *>(dest), reinterpret_cast<u16 const &>(value), count);
    else if constexpr (sizeof(T) == 4u)
        impl::set_memory(reinterpret_cast<u32 *>(dest), reinterpret_cast<u32 const &>(value), count);
    else if constexpr (sizeof(T) == 8u)
        impl::set_memory(reinterpret_cast<u64 *>(dest), reinterpret_cast<u64 const &>(value), count);
#if defined(__GNUC__) || defined(__clang__)
#    pragma GCC diagnostic pop
#endif
}

template<typename T> inline auto set_memory(span<T> const dest, T const value) -> void {
    set_memory(dest.begin(), value, dest.size());
}

template<typename T, usize count> inline auto set_memory(T (&array)[count], T const value) -> void {
    set_memory(array, value, count);
}
}  // namespace nlc
