/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/basics.hpp>
#include <nlc/meta/traits.hpp>

#include "arithmetic_types.hpp"
#include "assert.hpp"

namespace nlc {

template<typename T> class span final {
  public:
    using iterator = T *;
    using const_iterator = T const *;

  public:
    span(T * begin, usize count)
        : _begin(begin)
        , _end { _begin + count } {}
    span(T * begin, T * end)
        : _begin(begin)
        , _end(end) {}
    template<usize N>
    span(T (&array)[N])
        : _begin(array)
        , _end(array + N) {}
    explicit span(T & ref)
        : _begin(&ref)
        , _end(&ref + 1) {}
    constexpr span()
        : _begin(static_cast<T *>(nullptr))
        , _end(static_cast<T *>(nullptr)) {}
    span(span &&) = default;
    span(span const &) = default;
    auto operator=(span &&) -> span & = default;
    auto operator=(span const &) -> span & = default;

    [[nodiscard]] operator span<T const>() const { return span<T const> { _begin, _end }; }

  public:
    [[nodiscard]] auto begin() const { return _begin; }
    [[nodiscard]] auto end() const { return _end; }

    [[nodiscard]] auto subspan(usize const begin) const -> span {
        nlc_assert_msg(begin <= size(), nlc_dump_var(begin), nlc_dump_var(size()));
        return { _begin + begin, _end };
    }

    [[nodiscard]] auto subspan(usize const begin, usize const end) const -> span {
        nlc_assert_msg(begin <= size(), nlc_dump_var(begin), nlc_dump_var(size()));
        nlc_assert_msg(end <= size(), nlc_dump_var(end), nlc_dump_var(size()));
        nlc_assert_msg(begin <= end, nlc_dump_var(begin), nlc_dump_var(end));
        return { _begin + begin, _begin + end };
    }

  public:
    [[nodiscard]] auto operator[](usize index) const -> T & {
        nlc_assert_msg(index < size(), nlc_dump_var(index), nlc_dump_var(size()));
        return _begin[index];
    }
    [[nodiscard]] auto front() const -> T & {
        nlc_assert(size() > 0u);
        return _begin[0u];
    }
    [[nodiscard]] auto back() const -> T & {
        nlc_assert(size() > 0u);
        return _begin[size() - 1u];
    }

  public:
    [[nodiscard]] auto size() const { return static_cast<usize>(_end - _begin); }
    [[nodiscard]] auto is_empty() const { return (size() == 0u); }
    [[nodiscard]] auto ptr() const -> T * { return _begin; }

  private:
    T * _begin;
    T * _end;
};
template<typename It> span(It b, It) -> span<meta::rm_ref<decltype(*b)>>;

template<typename T> inline auto make_span(T & container) {
    if constexpr (meta::is_array<T>)
        return span(container + 0, container + meta::array_len<T>);
    else {
        return span(container.begin(), container.end());
    }
}

template<typename To, typename From> inline auto span_cast(span<From> const from) -> span<To> {
    if constexpr (sizeof(From) >= sizeof(To)) {
        auto constexpr ratio = sizeof(From) / sizeof(To);
        static_assert(sizeof(From) / ratio == sizeof(To), "type sizes are not multiples");
        return { reinterpret_cast<To *>(from.ptr()), from.size() * ratio };
    } else {
        auto constexpr ratio = sizeof(To) / sizeof(From);
        static_assert(sizeof(From) * ratio == sizeof(To), "type sizes are not multiples");
        auto const res = span { reinterpret_cast<To *>(from.ptr()), from.size() / ratio };
        nlc_assert(res.size() * ratio == from.size());
        return res;
    }
}

}  // namespace nlc
