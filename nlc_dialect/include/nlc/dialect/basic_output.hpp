/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "arithmetic_types.hpp"
#include "utf8.hpp"

namespace nlc {

enum class out_stream {
    out,
    err,
};

class log_entry final {
  public:
    log_entry();
    ~log_entry();

    auto display(out_stream) -> void;
    auto clear() -> void;
    auto c_str() const -> char const *;

  public:
    auto append(bool) -> void;
    auto append(char *) -> void;
    auto append(codepoint_type) -> void;
    auto append(char const *) -> void;
    auto append(void const *, usize) -> void;
    auto append(char) -> void;
    auto append(i8) -> void;
    auto append(i16) -> void;
    auto append(i32) -> void;
    auto append(i64) -> void;
    auto append(u8) -> void;
    auto append(u16) -> void;
    auto append(u32) -> void;
    auto append(u64) -> void;
    auto append(f32) -> void;
    auto append(f64) -> void;
    auto append(void *) -> void;
    template<typename T> auto append(T * ptr) -> void { append(reinterpret_cast<void *>(ptr)); }
    template<usize size> auto append(char const (&str)[size]) -> void { append(str, size - 1); }
    template<typename T> auto append(T const & other) -> void { format(*this, other); }

  public:
    struct buffer_type {
        char * ptr;
        usize const size;
    };
    auto get_buffer(usize) -> buffer_type;
    auto commit_buffer(buffer_type const &, usize) -> void;

  private:
    char * const _last;
};

template<typename... T> auto append(log_entry & entry, T &&... args) -> void {
    (entry.append(args), ...);
}

namespace logging {

    template<typename... T> auto print(T &&... args) -> void {
        auto entry = log_entry {};
        append(entry, args...);
        entry.display(out_stream::out);
    }

    template<typename... T> auto warn(T &&... args) -> void {
        auto entry = log_entry {};
        append(entry, args...);
        entry.display(out_stream::err);
    }
}  // namespace logging

using namespace logging;

}  // namespace nlc
