/* Copyright © 2025-2025
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "string_view.hpp"

namespace nlc {

//-----------------------------------------------------------

namespace style {

    struct common_style {
        string_view code;
    };

    template<typename T> auto format(T & out, common_style style) -> void {
        out.append(style.code);
    }

    //-----------------------------------------------------------

    struct custom_color {
        u8 code, r, g, b;
    };

    template<typename T> auto format(T & out, custom_color color) -> void {
        append(out, "\x1b[", color.code, ";2;", color.r, ';', color.g, ';', color.b, 'm');
    }

    //-----------------------------------------------------------

    auto constexpr reset_all = common_style { "\x1b[0m" };

    auto constexpr bold = common_style { "\x1b[1m" };
    auto constexpr no_bold = common_style { "\x1b[22m" };

    auto constexpr underline = common_style { "\x1b[4m" };
    auto constexpr no_underline = common_style { "\x1b[24m" };

    auto constexpr inverse = common_style { "\x1b[7m" };
    auto constexpr no_inverse = common_style { "\x1b[27m" };

    //-----------------------------------------------------------

    namespace bg {
        auto constexpr by_default = common_style { "\x1b[49m" };

        auto constexpr black = common_style { "\x1b[40m" };
        auto constexpr red = common_style { "\x1b[41m" };
        auto constexpr green = common_style { "\x1b[42m" };
        auto constexpr yellow = common_style { "\x1b[43m" };
        auto constexpr blue = common_style { "\x1b[44m" };
        auto constexpr magenta = common_style { "\x1b[45m" };
        auto constexpr cyan = common_style { "\x1b[46m" };
        auto constexpr white = common_style { "\x1b[47m" };

        auto constexpr grey = common_style { "\x1b[100m" };
        auto constexpr bright_red = common_style { "\x1b[101m" };
        auto constexpr bright_green = common_style { "\x1b[102m" };
        auto constexpr bright_yellow = common_style { "\x1b[103m" };
        auto constexpr bright_blue = common_style { "\x1b[104m" };
        auto constexpr bright_magenta = common_style { "\x1b[105m" };
        auto constexpr bright_cyan = common_style { "\x1b[106m" };
        auto constexpr bright_white = common_style { "\x1b[107m" };

        constexpr auto custom(u8 r, u8 g, u8 b) -> custom_color { return { 48, r, g, b }; }
    }  // namespace bg

    //-----------------------------------------------------------

    namespace fg {
        auto constexpr by_default = common_style { "\x1b[39m" };

        auto constexpr black = common_style { "\x1b[30m" };
        auto constexpr red = common_style { "\x1b[31m" };
        auto constexpr green = common_style { "\x1b[32m" };
        auto constexpr yellow = common_style { "\x1b[33m" };
        auto constexpr blue = common_style { "\x1b[34m" };
        auto constexpr magenta = common_style { "\x1b[35m" };
        auto constexpr cyan = common_style { "\x1b[36m" };
        auto constexpr white = common_style { "\x1b[37m" };
        auto constexpr grey = common_style { "\x1b[90m" };

        auto constexpr bright_red = common_style { "\x1b[91m" };
        auto constexpr bright_green = common_style { "\x1b[92m" };
        auto constexpr bright_yellow = common_style { "\x1b[93m" };
        auto constexpr bright_blue = common_style { "\x1b[94m" };
        auto constexpr bright_magenta = common_style { "\x1b[95m" };
        auto constexpr bright_cyan = common_style { "\x1b[96m" };
        auto constexpr bright_white = common_style { "\x1b[97m" };

        constexpr auto custom(u8 r, u8 g, u8 b) -> custom_color { return { 38, r, g, b }; }
    }  // namespace fg

    //-----------------------------------------------------------

    constexpr auto spaces(usize count) -> string_view {
        auto constexpr data =
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "
            "                                                                    "_strv;
        nlc_assert(count < data.size());
        return { data.ptr(), count };
    }

    constexpr auto tabulations(usize count) -> string_view {
        auto constexpr data =
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"
            "\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"_strv;
        nlc_assert(count < data.size());
        return { data.ptr(), count };
    }

    constexpr auto dots(usize count) -> string_view {
        auto constexpr data =
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."
            "...................................................................."_strv;
        nlc_assert(count < data.size());
        return { data.ptr(), count };
    }
}  // namespace style

}  // namespace nlc
