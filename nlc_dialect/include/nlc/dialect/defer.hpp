/* Copyright © 2019-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/basics.hpp>

#include "impl/error_count.hpp"
#include "arithmetic_types.hpp"

#define NLC_DEFER_CONCATENATE_IMPL(s1, s2) s1##s2
#define NLC_DEFER_CONCATENATE(s1, s2)      NLC_DEFER_CONCATENATE_IMPL(s1, s2)

namespace nlc::defer_impl {

enum class defer_trigger {
    on_error,
    on_success,
    both,
};

template<defer_trigger> class defer_tag {};

template<defer_trigger trigger, typename F> struct defer_handler final {
    defer_handler(F && f)
        : _callback(nlc_fwd(f))
        , _error_count(errors_impl::get_error_count()) {}

    ~defer_handler() {
        if constexpr (trigger == defer_trigger::both) {
            _callback();
        } else if constexpr (trigger == defer_trigger::on_error) {
            if (_error_count < errors_impl::get_error_count())
                _callback();
        } else {
            if (_error_count >= errors_impl::get_error_count())
                _callback();
        }
    }

    F _callback;
    u32 _error_count;
};

template<defer_trigger trigger, typename F> auto operator+(defer_tag<trigger>, F && f) {
    return defer_handler<trigger, F>(nlc_fwd(f));
}

#define defer                                         \
    auto NLC_DEFER_CONCATENATE(DEFER_, __COUNTER__) = \
        ::nlc::defer_impl::defer_tag< ::nlc::defer_impl::defer_trigger::both> {} + [&]()
#define error_defer                                   \
    auto NLC_DEFER_CONCATENATE(DEFER_, __COUNTER__) = \
        ::nlc::defer_impl::defer_tag< ::nlc::defer_impl::defer_trigger::on_error> {} + [&]()
#define success_defer                                 \
    auto NLC_DEFER_CONCATENATE(DEFER_, __COUNTER__) = \
        ::nlc::defer_impl::defer_tag< ::nlc::defer_impl::defer_trigger::on_success> {} + [&]()

}  // namespace nlc::defer_impl
