/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/units.hpp>

#include "arithmetic_types.hpp"

namespace nlc {

template<typename T> using bytes = meta::bytes<T>;
template<typename T> [[nodiscard]] inline auto operator+(char * p, bytes<T> s) { return p + s.v; }
template<typename T> [[nodiscard]] inline auto operator-(char * p, bytes<T> s) { return p - s.v; }
template<typename T> [[nodiscard]] inline auto operator+(void * p, bytes<T> s) {
    return reinterpret_cast<byte *>(p) + s.v;
}
template<typename T> [[nodiscard]] inline auto operator-(void * p, bytes<T> s) {
    return reinterpret_cast<byte *>(p) - s.v;
}
template<typename T> [[nodiscard]] inline auto operator+(byte * p, bytes<T> s) { return p + s.v; }
template<typename T> [[nodiscard]] inline auto operator-(byte * p, bytes<T> s) { return p - s.v; }
template<typename T> [[nodiscard]] inline auto operator+(char const * p, bytes<T> s) {
    return p + s.v;
}
template<typename T> [[nodiscard]] inline auto operator-(char const * p, bytes<T> s) {
    return p - s.v;
}
template<typename T> [[nodiscard]] inline auto operator+(byte const * p, bytes<T> s) {
    return p + s.v;
}
template<typename T> [[nodiscard]] inline auto operator-(byte const * p, bytes<T> s) {
    return p - s.v;
}
template<typename T> inline auto operator+=(char *& p, bytes<T> s) { return p += s.v; }
template<typename T> inline auto operator-=(char *& p, bytes<T> s) { return p -= s.v; }
template<typename T> inline auto operator+=(void *& p, bytes<T> s) {
    auto * cp = reinterpret_cast<byte *>(p);
    cp += s.v;
    p = reinterpret_cast<void *>(cp);
    return p;
}
template<typename T> inline auto operator-=(void *& p, bytes<T> s) {
    auto * cp = reinterpret_cast<byte *>(p);
    cp -= s.v;
    p = reinterpret_cast<void *>(cp);
    return p;
}
template<typename T> inline auto operator+=(byte *& p, bytes<T> s) { return p += s.v; }
template<typename T> inline auto operator-=(byte *& p, bytes<T> s) { return p -= s.v; }
template<typename T> inline auto operator+=(char const *& p, bytes<T> s) { return p += s.v; }
template<typename T> inline auto operator-=(char const *& p, bytes<T> s) { return p -= s.v; }
template<typename T> inline auto operator+=(byte const *& p, bytes<T> s) { return p += s.v; }
template<typename T> inline auto operator-=(byte const *& p, bytes<T> s) { return p -= s.v; }

template<typename T>
inline constexpr auto size_of =
    meta::unit<meta::pair<meta::list<meta::byte_dim>, meta::list<>>, usize>(sizeof(T));
template<>
inline constexpr auto size_of<void> =
    meta::unit<meta::pair<meta::list<meta::byte_dim>, meta::list<>>, usize>(0);
}  // namespace nlc
