/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "assert.hpp"
#include "string_iterator.hpp"
#include "types.hpp"
#include "utf8.hpp"

namespace nlc {

//-----------------------------------------------------------

class string_view final {
  public:
    using iterator = string_iterator;

  public:
    string_view() = default;
    string_view(string_view &&) = default;
    string_view(string_view const &) = default;
    auto operator=(string_view &&) -> string_view & = default;
    auto operator=(string_view const &) -> string_view & = default;

    [[nodiscard]] static inline auto from_c_str(char const * str) {
        return string_view(str, str_size(str));
    }

  public:
    constexpr string_view(char const * ptr, bytes<usize> size)
        : _data(ptr)
        , _size(size) {
        nlc_assert(size == 0u || is_valid_utf8(ptr, size.v));
    }

    string_view(iterator _begin, iterator _end);
    template<usize size>
    constexpr string_view(char const (&str)[size])
        : string_view(str, bytes<usize>(size - 1)) {}

  public:
    [[nodiscard]] constexpr auto size() const -> bytes<usize> { return _size; }
    [[nodiscard]] constexpr auto is_empty() const -> bool { return size() == 0u; }
    [[nodiscard]] constexpr auto ptr() const -> char const * { return _data; }

  public:
    [[nodiscard]] auto begin() const { return iterator(_data); }
    [[nodiscard]] auto end() const { return iterator(_data + _size); }

    [[nodiscard]] auto substring(usize begin) const -> string_view {
        nlc_assert(begin <= _size);
        return { _data + begin, _data + _size };
    }

    [[nodiscard]] auto substring(usize begin, usize end) const -> string_view {
        nlc_assert(begin <= _size && begin <= end);
        return { _data + begin, _data + end };
    }

  private:
    char const * _data = "";
    bytes<usize> _size = 0u;
};

//-----------------------------------------------------------

template<typename T> void format(T & stream, string_view const v) {
    stream.append(v.ptr(), rm_unit(v.size()));
}

//-----------------------------------------------------------

[[nodiscard]] auto compare(string_view const, string_view const) -> comparison_result;
[[nodiscard]] auto fast_compare(string_view const, string_view const) -> comparison_result;
[[nodiscard]] inline auto operator==(string_view const l, string_view const r) -> bool {
    return fast_compare(l, r) == comparison_result::equal;
}
[[nodiscard]] inline auto operator!=(string_view const l, string_view const r) -> bool {
    return fast_compare(l, r) != comparison_result::equal;
}

//-----------------------------------------------------------

[[nodiscard]] inline auto length(string_view const str) {
    return str_len(str.ptr(), rm_unit(str.size()));
}
[[nodiscard]] inline auto size(string_view const str) { return str.size(); }

//-----------------------------------------------------------

auto make_null_terminated(string_view const, char *, bytes<usize>) -> void;
template<auto size> auto make_null_terminated(string_view const str, char (&buf)[size]) -> void {
    make_null_terminated(str, buf, bytes<usize>(size));
}

//-----------------------------------------------------------

}  // namespace nlc

//-----------------------------------------------------------

[[nodiscard]] inline constexpr auto operator""_strv(char const * str, usize size) -> nlc::string_view {
    return { str, size };
}
