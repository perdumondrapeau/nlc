/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "nlc/meta/basics.hpp"
#include "arithmetic_types.hpp"
#include "span.hpp"
#include "types.hpp"

namespace nlc {

auto memmove(void * dest, void const * from, bytes<usize> size) -> void;

auto memcpy(void * dest, void const * from, bytes<usize> size) -> void;

auto memset(void * const dest, byte const value, bytes<usize> const count) -> void;
template<typename T> inline auto memset(span<T> dest, byte const value) -> void {
    memset(reinterpret_cast<void *>(dest.begin()), value, dest.size() * sizeof(T));
}
template<typename T, usize count> inline auto memset(T (&array)[count], byte const value) -> void {
    memset(reinterpret_cast<void *>(array), value, count);
}

template<typename T> class [[nodiscard]] uninitialized final {
  public:
    uninitialized() = default;  // delete all constructors and assignement

    template<typename... Ts> auto construct(Ts &&... args) -> void {
        new (const_cast<meta::rm_const<T> *>(address())) T { nlc_fwd(args)... };
    }
    auto destroy() -> void { address()->~T(); }
    auto copy_memory_from(uninitialized const & rhs) -> void {
        memcpy(_data, rhs._data, sizeof(T));
    }

    [[nodiscard]] constexpr auto value() & -> T & { return *address(); }
    [[nodiscard]] constexpr auto value() const & -> T const & { return *address(); }
    [[nodiscard]] constexpr auto value() && -> T { return nlc_move(*address()); }

    [[nodiscard]] constexpr auto address() const -> T const * {
        return reinterpret_cast<T const *>(_data);
    }
    [[nodiscard]] constexpr auto address() -> T * { return reinterpret_cast<T *>(_data); }

  private:
    alignas(T) char _data[sizeof(T)];
};

template<typename T>
inline auto set_memory(T * const dest, T const value, bytes<usize> const count) -> void;
template<typename T> inline auto set_memory(span<T> const dest, T const value) -> void;
template<typename T, usize count> inline auto set_memory(T (&array)[count], T const value) -> void;

}  // namespace nlc

#include "memory.inl"
