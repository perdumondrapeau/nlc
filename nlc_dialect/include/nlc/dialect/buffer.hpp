/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/basics.hpp>
#include <nlc/meta/logic.hpp>
#include <nlc/meta/tags.hpp>

#include "arithmetic_types.hpp"
#include "assert.hpp"
#include "memory.hpp"

namespace nlc {

// buffer /////////////////////////////////////////////////////////////

template<typename T, usize _size, typename index_type_ = meta::default_type> class buffer final {
  public:
    using index_type = meta::if_default<index_type_, usize>;

  public:
    buffer() = default;
    buffer(buffer const &) = delete;
    buffer(T const & default_value) {
        for (usize i = 0u; i < _size; ++i)
            operator[](i) = default_value;
    }

    buffer(buffer && other) { operator=(nlc_move(other)); }

    auto operator=(buffer const &) -> buffer & = delete;

    auto operator=(buffer && other) -> buffer & {
        nlc_assert(this != &other);
        for (index_type i = 0u; i < _size; ++i)
            operator[](i) = nlc_move(other.operator[](i));

        return *this;
    }

  public:
    [[nodiscard]] static constexpr auto size() -> usize { return _size; }

    constexpr auto copy_to(buffer & rhs) const -> void {
        memcpy(rhs._data, _data, _size * sizeof(T));
    }

    [[nodiscard]] auto ptr() { return reinterpret_cast<T *>(_data); }
    [[nodiscard]] auto ptr() const { return reinterpret_cast<const T *>(_data); }

    [[nodiscard]] auto begin() -> T * { return ptr(); }
    [[nodiscard]] auto begin() const -> T const * { return ptr(); }
    [[nodiscard]] auto end() -> T * { return ptr() + _size; }
    [[nodiscard]] auto end() const -> T const * { return ptr() + _size; }

    // [[nodiscard]] auto data()  { return span(ptr(), _size); }
    // [[nodiscard]] auto data() const  { return span(ptr(), _size); }

    [[nodiscard]] auto operator[](index_type i) -> T & {
        auto const index = static_cast<usize>(i);
        nlc_assert(index < _size);
        return reinterpret_cast<T *>(&_data)[index];
    }
    [[nodiscard]] constexpr auto operator[](index_type i) const -> const T & {
        auto const index = static_cast<usize>(i);
        nlc_assert(index < _size);
        return reinterpret_cast<T const *>(&_data)[index];
    }

  private:
    byte _data[_size * sizeof(T)];
};

//--------------------------------------------------------------------
// copy

template<typename T, typename S1, usize s1, typename S2, usize s2>
auto memcpy(buffer<T, s1, S1> & dest, [[maybe_unused]] buffer<T, s2, S2> const & from) {
    nlc_assert(dest.size() >= from.size() /*, "{} >= {}", dest.size(), from.size()*/);
    memcpy(dest.ptr() /*, from.ptr(), from.size() * size_of<T>*/);
}

template<typename T, typename S1, usize s1, typename S2, usize s2>
auto memcpy(buffer<T, s1, S1> & dest, buffer<T, s2, S2> const & from, usize size) {
    nlc_assert(size <= dest.size() && size <= from.size());
    memcpy(dest.ptr(), from.ptr(), size * size_of<T>);
}

//--------------------------------------------------------------------
// swap

template<typename T, usize s, typename S1, typename S2>
auto memswap(buffer<T, s, S1> & lhs, buffer<T, s, S2> & rhs) {
    buffer<T, s> tmp;
    memcpy(tmp, lhs);
    memcpy(lhs, rhs);
    memcpy(rhs, tmp);
}

//--------------------------------------------------------------------

}  // namespace nlc
