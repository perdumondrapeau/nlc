/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/basics.hpp>
#include <nlc/meta/list.hpp>
#include <nlc/meta/list/contains.hpp>
#include <nlc/meta/list/get.hpp>
#include <nlc/meta/list/index_of.hpp>
#include <nlc/meta/traits.hpp>

#include "arithmetic_types.hpp"
#include "assert.hpp"
#include "new.hpp"
#include "null.hpp"

namespace nlc {

namespace variant_impl {
    template<typename... T> constexpr usize max(T &&... values) {
        usize res = 0ul;
        ((values > res ? res = values : 0), ...);
        return res;
    }
}  // namespace variant_impl

template<typename index_type_, typename... Ts> class enum_variant final {
  public:
    using index_type = index_type_;
    using types = meta::list<Ts...>;
    template<auto index> using get_type = meta::get<static_cast<meta::comptime_int>(index), types>;
    template<typename T>
    static constexpr index_type index_of = static_cast<index_type>(meta::index_of<T, types>);
    static_assert(types::size < 256);
    // check if no doublons

  public:
    // Constructor and assignations

    enum_variant() = delete;
    ~enum_variant() { clear(); }

    enum_variant(enum_variant && v) {
        usize it = 0ull;
        ((it++ == static_cast<usize>(v._index) ? new (as_unchecked<Ts>()) Ts(nlc_move(v.as<Ts>())),
          static_cast<void>(0)
                                               : static_cast<void>(0)),
         ...);
        _index = v._index;
    }

    enum_variant(enum_variant const & v) {
        usize it = 0ull;
        ((it++ == static_cast<usize>(v._index) ? new (as_unchecked<Ts>()) Ts(v.as<Ts>()),
          static_cast<void>(0)
                                               : static_cast<void>(0)),
         ...);
        _index = v._index;
    }

    auto operator=(enum_variant && v) -> enum_variant & {
        if (_index == v._index) {
            usize it = 0ull;
            ((it++ == static_cast<usize>(_index) ? as<Ts>() = nlc_move(v.as<Ts>()),
              static_cast<void>(0)
                                                 : static_cast<void>(0)),
             ...);
        } else {
            clear();
            usize it = 0ull;
            ((it++ == static_cast<usize>(v._index) ? create<Ts>(nlc_move(v.as<Ts>())),
              static_cast<void>(0)

                                                   : static_cast<void>(0)),
             ...);
        }
        return *this;
    }

    auto operator=(enum_variant const & v) -> enum_variant & {
        if (_index == v._index) {
            usize it = 0ull;
            ((it++ == static_cast<usize>(_index) ? as<Ts>() = v.as<Ts>(),
              static_cast<void>(0)
                                                 : static_cast<void>(0)),
             ...);
        } else {
            clear();
            usize it = 0ull;
            ((it++ == static_cast<usize>(v._index) ? create<Ts>(v.as<Ts>()),
              static_cast<void>(0)

                                                   : static_cast<void>(0)),
             ...);
        }
        return *this;
    }

    template<typename T>
    enum_variant(T && v)
        requires meta::contains<types, meta::rm_const_ref<T>>
    {
        using type = meta::rm_const_ref<T>;
        create<type>(nlc_fwd(v));
    }

    template<typename T>
        requires meta::contains<types, meta::rm_const_ref<T>>
    auto operator=(T && v) -> enum_variant & {
        using type = meta::rm_const_ref<T>;
        constexpr auto index = index_of<type>;
        if (index == _index) {
            as<type>() = nlc_fwd(v);
        } else {
            clear();
            create<type>(nlc_fwd(v));
        }
        return *this;
    }

    template<typename T, typename... Args> auto construct(Args &&... args) -> T & {
        using type = meta::rm_const_ref<T>;
        clear();
        create<type>(nlc_fwd(args)...);
        return *as_unchecked<type>();
    }

    auto operator==(enum_variant const & other) const -> bool {
        if (_index != other._index)
            return false;

        usize it = 0ull;
        auto const res = (... || (it++ == static_cast<usize>(_index)
                                      ? *as_unchecked<Ts>() == *other.as_unchecked<Ts>()
                                      : false));
        return res;
    }

    auto operator!=(enum_variant const & other) const -> bool {
        if (_index != other._index)
            return true;

        usize it = 0ull;
        auto const res = (... || (it++ == static_cast<usize>(_index)
                                      ? *as_unchecked<Ts>() != *other.as_unchecked<Ts>()
                                      : false));
        return res;
    }
    // By type consultation

    template<typename T> auto contains() const { return _index == index_of<T>; }

    template<typename T> auto as() & -> T & {
        return const_cast<T &>(static_cast<enum_variant const *>(this)->as<T>());
    }

    template<typename T> auto as() const & -> T const & {
        nlc_assert((_index == index_of<T>));
        return *as_unchecked<T>();
    }

    template<typename T> auto as() && -> T {
        return nlc_move(static_cast<enum_variant &>(*this).as<T>());
    }

    // By index consultation

    auto index() const { return _index; }

    template<index_type index> auto is() const {
        static_assert(index < static_cast<index_type>(types::size));
        return _index == index;
    }

    template<index_type index> auto get() const & -> get_type<index> const & {
        nlc_assert((_index == index));
        return *as_unchecked<get_type<index>>();
    }

    template<index_type index> auto get() & -> get_type<index> & {
        return const_cast<get_type<index> &>(static_cast<enum_variant const *>(this)->get<index>());
    }

    template<index_type index> auto get() && -> get_type<index> {
        return nlc_move(static_cast<enum_variant &>(*this).get<index>());
    }

  private:
    template<typename T, typename... Args> auto create(Args &&... args) -> void {
        new (as_unchecked<T>()) T(nlc_fwd(args)...);
        _index = index_of<T>;
    }

    // MSVC gets confused when trying to emit destructor calls, with this indirection it's OK.
    template<typename T> auto destroy() -> void { as_unchecked<T>()->~T(); }

    auto clear() {
        usize it = 0ull;
        ((it++ == static_cast<usize>(_index) ? destroy<Ts>() : static_cast<void>(0)), ...);
    }

    template<typename T> auto as_unchecked() -> T * { return reinterpret_cast<T *>(_storage); }

    template<typename T> auto as_unchecked() const -> T const * {
        return reinterpret_cast<T const *>(_storage);
    }

  private:
    alignas(variant_impl::max(alignof(Ts)...)) byte _storage[variant_impl::max(sizeof(Ts)...)];
    index_type _index;
};

template<typename... T> using variant = enum_variant<u8, T...>;
template<typename... T> using optional_variant = enum_variant<u8, null_type, T...>;

}  // namespace nlc
