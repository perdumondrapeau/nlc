#include "arithmetic_types.hpp"

namespace nlc {

namespace impl {
    /*
     * original code: https://github.com/sheredom/utf8.h/blob/master/utf8.h
     *     by Orson Peters
     *     in public domain
     *
     * modifications by
     *          Alexis A. D. COLIN,
     *          Antoine VUGLIANO,
     *          Gaëtan CHAMPARNAUD,
     *          Geoffrey L. TOURON,
     *          Grégoire A. P. BADIN
     *     under the EUPL
     */

    //
    // This is free and unencumbered software released into the public domain.
    //
    // Anyone is free to copy, modify, publish, use, compile, sell, or
    // distribute this software, either in source code form or as a compiled
    // binary, for any purpose, commercial or non-commercial, and by any
    // means.
    //
    // In jurisdictions that recognize copyright laws, the author or authors
    // of this software dedicate any and all copyright interest in the
    // software to the public domain. We make this dedication for the benefit
    // of the public at large and to the detriment of our heirs and
    // successors. We intend this dedication to be an overt act of
    // relinquishment in perpetuity of all present and future rights to this
    // software under copyright law.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    // EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    // IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
    // OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
    // ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    // OTHER DEALINGS IN THE SOFTWARE.
    //
    // For more information, please refer to <http://unlicense.org/>
    constexpr auto check_first_codepoint(char const * s, usize const remaining_size = 0u) -> int {
        if (0xf0 == (0xf8 & *s)) {
            // ensure there is enough following bytes
            if (remaining_size != 0u && remaining_size < 4u)
                return 0u;

            // ensure each of the 3 following bytes in this 4-byte
            // utf8 codepoint began with 0b10xxxxxx
            if ((0x80 != (0xc0 & s[1])) || (0x80 != (0xc0 & s[2])) || (0x80 != (0xc0 & s[3])))
                return 0;

            // ensure that our utf8 codepoint ended after 4 bytes
            if (remaining_size > 4u && 0x80 == (0xc0 & s[4]))
                return 0;

            // ensure that the top 5 bits of this 4-byte utf8
            // codepoint were not 0, as then we could have used
            // one of the smaller encodings
            if ((0 == (0x07 & s[0])) && (0 == (0x30 & s[1])))
                return 0;

            // 4-byte utf8 code point (began with 0b11110xxx)
            return 4;
        } else if (0xe0 == (0xf0 & *s)) {
            // ensure there is enough following bytes
            if (remaining_size != 0u && remaining_size < 3u)
                return 0u;

            // ensure each of the 2 following bytes in this 3-byte
            // utf8 codepoint began with 0b10xxxxxx
            if ((0x80 != (0xc0 & s[1])) || (0x80 != (0xc0 & s[2])))
                return 0;

            // ensure that our utf8 codepoint ended after 3 bytes
            if (remaining_size > 3 && 0x80 == (0xc0 & s[3]))
                return 0;

            // ensure that the top 5 bits of this 3-byte utf8
            // codepoint were not 0, as then we could have used
            // one of the smaller encodings
            if ((0 == (0x0f & s[0])) && (0 == (0x20 & s[1])))
                return 0;

            // 3-byte utf8 code point (began with 0b1110xxxx)
            return 3;
        } else if (0xc0 == (0xe0 & *s)) {
            // ensure there is enough following bytes
            if (remaining_size != 0u && remaining_size < 2u)
                return 0u;

            // ensure the 1 following byte in this 2-byte
            // utf8 codepoint began with 0b10xxxxxx
            if (0x80 != (0xc0 & s[1]))
                return 0;

            // ensure that our utf8 codepoint ended after 2 bytes
            if (remaining_size > 2 && 0x80 == (0xc0 & s[2]))
                return 0;

            // ensure that the top 4 bits of this 2-byte utf8
            // codepoint were not 0, as then we could have used
            // one of the smaller encodings
            if (0 == (0x1e & s[0]))
                return 0;

            // 2-byte utf8 code point (began with 0b110xxxxx)
            return 2;
        } else if (0x00 == (0x80 & *s)) {
            // 1-byte ascii (began with 0b0xxxxxxx)
            return 1;
        }

        // we have an invalid 0b1xxxxxxx utf8 code point entry
        return 0;
    }
}  // namespace impl

constexpr auto first_codepoint_size(char const * str) -> usize {
    if (0xf0 == (0xf8 & str[0]))
        return 4;
    if (0xe0 == (0xf0 & str[0]))
        return 3;
    if (0xc0 == (0xe0 & str[0]))
        return 2;
    return 1;
}

constexpr auto is_valid_utf8(char const * s) -> bool {
    while (*s != '\0') {
        auto const advance = impl::check_first_codepoint(s);
        if (advance == 0)
            return false;
        s += advance;
    }

    return true;
}

constexpr auto is_valid_utf8(char const * s, usize size) -> bool {
    auto cur_size = 0u;
    auto cur = static_cast<usize>(0u);
    while (cur < size && *s != '\0') {
        auto next_cp_size = first_codepoint_size(s);
        if (next_cp_size + cur_size > size)
            return false;

        auto const advance = impl::check_first_codepoint(s, size - cur);
        if (advance == 0)
            return false;
        s += advance;
        cur += next_cp_size;
    }
    return true;
}

}  // namespace nlc
