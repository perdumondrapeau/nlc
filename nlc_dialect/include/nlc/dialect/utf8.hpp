/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "arithmetic_types.hpp"

namespace nlc {

enum class comparison_result { less, more, equal };

struct codepoint_type final {
    using underlying_type = char32_t;

    explicit constexpr codepoint_type(underlying_type const cp)
        : v { cp } {}

    constexpr auto value() const { return v; }

    underlying_type v;
};

#define OP(op)                                                                               \
    inline auto operator op(codepoint_type lhs, codepoint_type rhs) {                        \
        return lhs.value() op rhs.value();                                                   \
    }                                                                                        \
    inline auto operator op(codepoint_type lhs, char rhs) {                                  \
        return lhs.value() op static_cast<codepoint_type::underlying_type>(rhs);             \
    }                                                                                        \
    inline auto operator op(char lhs, codepoint_type rhs) {                                  \
        return static_cast<codepoint_type::underlying_type>(lhs) op rhs.value();             \
    }                                                                                        \
    inline auto operator op(codepoint_type lhs, char16_t rhs) { return lhs.value() op rhs; } \
    inline auto operator op(char16_t lhs, codepoint_type rhs) { return lhs op rhs.value(); } \
    inline auto operator op(codepoint_type lhs, char32_t rhs) { return lhs.value() op rhs; } \
    inline auto operator op(char32_t lhs, codepoint_type rhs) { return lhs op rhs.value(); }

OP(==)
OP(!=)
OP(<)
OP(>)
OP(<=)
OP(>=)
#undef OP

[[nodiscard]] auto str_size(char const *) -> usize;
[[nodiscard]] auto str_len(char const *) -> usize;
[[nodiscard]] auto str_len(char const *, usize) -> usize;

[[nodiscard]] auto str_cmp(char const *, char const *) -> comparison_result;
[[nodiscard]] auto str_cmp(char const *, usize, char const *, usize) -> comparison_result;

[[nodiscard]] auto codepoint(char const *) -> codepoint_type;
[[nodiscard]] auto codepoint(char const *, char const **) -> codepoint_type;
[[nodiscard]] auto codepoint_size(codepoint_type) -> usize;
[[nodiscard]] constexpr auto first_codepoint_size(char const * str) -> usize;

auto cat_to_string(char *, codepoint_type) -> char *;

[[nodiscard]] constexpr auto is_valid_utf8(char const * s) -> bool;
[[nodiscard]] constexpr auto is_valid_utf8(char const * s, usize size) -> bool;

}  // namespace nlc

#include "utf8.inl"
