/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/basics.hpp>
#include <nlc/meta/traits.hpp>

namespace nlc {

template<class T> class function_view;
template<class T> class function;

template<class Ret, class... Args> class function_view<Ret(Args...)> final {
    template<typename T> friend class function;

    template<typename> struct is_function {
        static constexpr auto v = false;
    };
    template<typename T> struct is_function<function<T>> {
        static constexpr auto v = true;
    };

    using proxy_signature = Ret(void const * const *, Args...);

  public:
    using signature_type = Ret(Args...);

    function_view(function_view const &) = default;
    function_view & operator=(function_view const &) = default;

    template<class F>
        requires(!is_function<F>::v)
    function_view(F const & f)
        : _ptr(reinterpret_cast<void const *>(&f))  // Take the adress of the callable object
        , _pf([](void const * const * ptr, Args... args) -> Ret {
            auto const mut_ptr = const_cast<void *>(*ptr);
            // retrieve the callable object address
            auto const callable = reinterpret_cast<meta::rm_ref<F> *>(mut_ptr);
            return (*callable)(nlc_fwd(args)...);
        }) {}

    function_view(signature_type * f)
        : _ptr(reinterpret_cast<void const *>(f))  // Directly copy the adress of the function
        , _pf([](void const * const * ptr, Args... args) -> Ret {
            auto const mut_ptr = const_cast<void *>(*ptr);
            // retrieve the function pointer
            auto const callable = reinterpret_cast<signature_type *>(mut_ptr);
            return callable(nlc_fwd(args)...);
        }) {}

    auto operator()(Args... args) const -> Ret { return _pf(&_ptr, nlc_fwd(args)...); }

  private:
    function_view(void const * ptr, proxy_signature * const pf)
        : _ptr(ptr)
        , _pf(pf) {}

  private:
    void const * _ptr;
    proxy_signature * _pf;
};

}  // namespace nlc
