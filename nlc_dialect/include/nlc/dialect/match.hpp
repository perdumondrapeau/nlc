/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/list/get.hpp>
#include <nlc/meta/overloaded.hpp>

#include "variant.hpp"

namespace nlc {

namespace impl_match {

    // Pre compute typelist products

    template<typename, typename> struct product;

#include "match.inl"

    // Implement the vtable generaton

    template<typename... Ts, typename V1, typename V2, typename F>
    decltype(auto) aux(meta::list<Ts...>, V1 && v1, V2 && v2, F && fun) {
        using ret = decltype(fun(v1.template get<0>(), v2.template get<0>()));
        using func_type = ret (* const)(decltype(v1), decltype(v2), decltype(fun));

        static constexpr func_type vtable[] = { static_cast<func_type>(
            [](decltype(v1) a, decltype(v2) b, decltype(fun) f) -> ret {
                return f(nlc_fwd(a).template as<meta::get<0, Ts>>(),
                         nlc_fwd(b).template as<meta::get<1, Ts>>());
            })... };
        return vtable[v1.index() * meta::rm_const_ref<V2>::types::size + v2.index()](nlc_fwd(v1),
                                                                                     nlc_fwd(v2),
                                                                                     nlc_fwd(fun));
    }

    template<typename... Ts, typename V, typename F>
    decltype(auto) aux(meta::list<Ts...>, V && v, F && fun) {
        using ret = decltype(fun(v.template get<0>()));
        using func_type = ret (* const)(decltype(v), decltype(fun));
        static constexpr func_type vtable[] = { [](decltype(v) a, decltype(fun) f) -> ret {
            return f(nlc_fwd(a).template as<Ts>());
        }... };
        return vtable[v.index()](nlc_fwd(v), nlc_fwd(fun));
    }

}  // namespace impl_match

// One variant

template<typename V> decltype(auto) match(V && v) {
    return [&v](auto &&... f) -> decltype(auto) {
        return impl_match::aux(typename meta::rm_const_ref<V>::types {},
                               forward<V>(v),
                               meta::overloaded { nlc_fwd(f)... });
    };
}

// Two variants

template<typename V1, typename V2> decltype(auto) match(V1 && v1, V2 && v2) {
    return [&v1, &v2](auto &&... f) -> decltype(auto) {
        return impl_match::aux(typename impl_match::product<typename meta::rm_const_ref<V1>::types,
                                                            typename meta::rm_const_ref<V2>::types>::type {},
                               forward<V1>(v1),
                               forward<V2>(v2),
                               meta::overloaded { nlc_fwd(f)... });
    };
}

}  // namespace nlc
