/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/sanity/platforms.hpp>

// Types less than 32bits are promoted to int in va_args.
#define prii8  "%d"
#define prii16 "%d"
#define prii32 "%d"
#define priu8  "%d"
#define priu16 "%d"
#define priu32 "%u"

using byte = unsigned char;

using i8 = signed char;
using i16 = signed short int;
using i32 = signed int;
#if NLC_OS_WINDOWS
using i64 = signed long long int;
#    define prii64 "%lld"
#else
using i64 = signed long int;  // we dont use long long to be compatible with the stl
#    define prii64 "%ld"
#endif

using u8 = unsigned char;
using u16 = unsigned short int;
using u32 = unsigned int;
#if NLC_OS_WINDOWS
using u64 = unsigned long long int;
#    define priu64 "%llu"
#else
using u64 = unsigned long int;  // we dont use long long to be compatible with the stl
#    define priu64 "%lu"
#endif

using f32 = float;
using f64 = double;

using isize = i64;
using usize = u64;
#define priusize priu64
#define priisize prii64

inline constexpr auto operator""_b(unsigned long long int n) -> byte {
    return static_cast<byte>(n);
}

inline constexpr auto operator""_i8(unsigned long long int n) -> i8 { return static_cast<i8>(n); }
inline constexpr auto operator""_i16(unsigned long long int n) -> i16 {
    return static_cast<i16>(n);
}
inline constexpr auto operator""_i32(unsigned long long int n) -> i32 {
    return static_cast<i32>(n);
}
inline constexpr auto operator""_i64(unsigned long long int n) -> i64 {
    return static_cast<i64>(n);
}

inline constexpr auto operator""_u8(unsigned long long int n) -> u8 { return static_cast<u8>(n); }
inline constexpr auto operator""_u16(unsigned long long int n) -> u16 {
    return static_cast<u16>(n);
}
inline constexpr auto operator""_u32(unsigned long long int n) -> u32 {
    return static_cast<u32>(n);
}
inline constexpr auto operator""_u64(unsigned long long int n) -> u64 {
    return static_cast<u64>(n);
}

inline constexpr auto operator""_f32(long double n) -> f32 { return static_cast<f32>(n); }
inline constexpr auto operator""_f64(long double n) -> f64 { return static_cast<f64>(n); }

inline constexpr auto operator""_isize(unsigned long long int n) -> isize {
    return static_cast<isize>(n);
}
inline constexpr auto operator""_usize(unsigned long long int n) -> usize {
    return static_cast<usize>(n);
}
