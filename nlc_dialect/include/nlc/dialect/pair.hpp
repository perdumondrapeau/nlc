/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/basics.hpp>
#include <nlc/meta/traits.hpp>

namespace nlc {

template<typename T1, typename T2> struct pair final {
    T1 first;
    T2 second;

    [[nodiscard]] constexpr pair() = default;
    [[nodiscard]] constexpr pair(pair &&) = default;
    [[nodiscard]] constexpr pair(const pair &) = default;
    template<typename Q1, typename Q2>
    [[nodiscard]] constexpr pair(Q1 && arg1, Q2 && arg2)
        : first(nlc_fwd(arg1))
        , second(nlc_fwd(arg2)) {}
    auto operator=(pair &&) & -> pair & = default;
    auto operator=(const pair &) & -> pair & = default;
};

template<typename T1, typename T2>
pair(T1, T2) -> pair<meta::rm_const_ref<T1>, meta::rm_const_ref<T2>>;

}  // namespace nlc
