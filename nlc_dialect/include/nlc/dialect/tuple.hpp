/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/basics.hpp>
#include <nlc/meta/list.hpp>
#include <nlc/meta/list/foreach.hpp>
#include <nlc/meta/list/range.hpp>

#include "arithmetic_types.hpp"

namespace nlc {

// tuple ------------------------------------------------------------------------

template<typename... Ts> class tuple final {
  public:
    static constexpr auto size = 0;
    using types = meta::list<>;

  public:
    template<usize i> auto get() const { static_assert(i < size); }
    template<usize i> auto get() { static_assert(i < 0); }
};

// -------------------------------------------

template<typename T, typename... Ts> class tuple<T, Ts...> final {
  public:
    static constexpr auto size = 1 + sizeof...(Ts);
    using types = meta::list<T, Ts...>;

  public:
    tuple() = default;
    template<typename Arg, typename... Args>
    tuple(Arg && first, Args &&... next)
        : _first(nlc_fwd(first))
        , _next(nlc_fwd(next)...) {}

  public:
    template<usize i> decltype(auto) get() const {
        static_assert(i < size);
        if constexpr (i == 0)
            return (_first);
        else
            return _next.template get<i - 1>();
    }

    template<usize i> decltype(auto) get() {
        static_assert(i < size);
        if constexpr (i == 0)
            return (_first);
        else
            return _next.template get<i - 1>();
    }

  private:
    T _first;
    tuple<Ts...> _next;
};

// -------------------------------------------

template<typename T> class tuple<T> final {
  public:
    static constexpr auto size = 1;
    using types = meta::list<T>;

  public:
    tuple() = default;
    template<typename Arg>
    explicit tuple(Arg && value)
        : _data(nlc_fwd(value)) {}

    template<usize i> decltype(auto) get() {
        static_assert(i < size);
        return (_data);
    }
    template<usize i> decltype(auto) get() const {
        static_assert(i < size);
        return (_data);
    }

  private:
    T _data;
};

// -------------------------------------------

template<typename... Ts> tuple(Ts...) -> tuple<Ts...>;

namespace impl {
    template<typename T, template<typename...> typename f> struct tuple_from_type_list {};
    template<typename... Ts, template<typename...> typename f>
    struct tuple_from_type_list<meta::list<Ts...>, f> {
        using type = tuple<f<Ts>...>;
    };
}  // namespace impl
template<typename T, template<typename...> typename f = meta::identity>
using tuple_from_type_list = typename impl::tuple_from_type_list<T, f>::type;

// get --------------------------------------------------------------------------

template<usize index, typename... Ts> decltype(auto) get(tuple<Ts...> & t) {
    return t.template get<index>();
}
template<usize index, typename... Ts> decltype(auto) get(tuple<Ts...> && t) {
    return nlc_move(t.template get<index>());
}
template<usize index, typename... Ts> decltype(auto) get(const tuple<Ts...> & t) {
    return t.template get<index>();
}

// apply ------------------------------------------------------------------------

namespace impl {
    template<typename F, typename T, auto... vs>
    auto apply_on_tuple_impl(F && f, T && t, meta::list<meta::Value<vs>...>) {
        return f(get<vs>(nlc_fwd(t))...);
    }
}  // namespace impl

template<typename F, typename... Ts> constexpr auto apply(F && f, tuple<Ts...> & t) {
    return impl::apply_on_tuple_impl(f, t, meta::range<0, tuple<Ts...>::size>());
}
template<typename F, typename... Ts> constexpr auto apply(F && f, tuple<Ts...> && t) {
    return impl::apply_on_tuple_impl(nlc_move(f), nlc_move(t), meta::range<0, tuple<Ts...>::size>());
}
template<typename F, typename... Ts> constexpr auto apply(F && f, const tuple<Ts...> & t) {
    return impl::apply_on_tuple_impl(f, t, meta::range<0, tuple<Ts...>::size>());
}

// forEach ----------------------------------------------------------------------

template<typename F, typename... T> constexpr auto forEach(tuple<T...> & t, F && f) {
    meta::for_each<meta::range<0, tuple<T...>::size>>([&](auto i) { f(get<i>(t)); });
}
template<typename F, typename... T> constexpr auto forEach(tuple<T...> && t, F && f) {
    meta::for_each<meta::range<0, tuple<T...>::size>>([&](auto i) { f(nlc_move(get<i>(t))); });
}
template<typename F, typename... T> constexpr auto forEach(const tuple<T...> & t, F && f) {
    meta::for_each<meta::range<0, tuple<T...>::size>>([&](auto i) { f(get<i>(t)); });
}

// fold -------------------------------------------------------------------------

template<typename F, typename V, typename... T>
constexpr auto fold(F && f, V && v, tuple<T...> const & t) {
    auto res = v;
    meta::for_each<meta::range<0, tuple<T...>::size>>([&](auto i) { res = f(res, get<i>(t)); });
    return res;
}

// transform --------------------------------------------------------------------

template<typename F, typename... T> constexpr auto transform(tuple<T...> && t, F && f) {
    return apply([&f](auto &&... args) { return tuple(f(nlc_move(args))...); }, t);
}
template<typename F, typename... T> constexpr auto transform(tuple<T...> const & t, F && f) {
    return apply([&f](auto &&... args) { return tuple(f(nlc_fwd(args))...); }, t);
}
template<typename F, typename... T> constexpr auto transform(tuple<T...> & t, F && f) {
    return apply([&f](auto &&... args) { return tuple(f(nlc_fwd(args))...); }, t);
}

}  // namespace nlc
