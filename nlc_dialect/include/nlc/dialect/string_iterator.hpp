/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "types.hpp"
#include "utf8.hpp"

namespace nlc {

class string_iterator final {
  public:
    string_iterator(char const * ptr)
        : _ptr(ptr) {}

    string_iterator(byte const * ptr)
        : _ptr { reinterpret_cast<char const *>(ptr) } {}

  public:
    auto operator++() -> string_iterator & {
        _ptr += first_codepoint_size(_ptr);
        return *this;
    }
    auto operator++(int) -> string_iterator {
        auto res = *this;
        ++(*this);
        return res;
    }
    auto operator+(usize n) -> string_iterator {
        auto res = *this;
        while (n-- > 0)
            ++res;
        return res;
    }
    auto operator+=(usize n) -> string_iterator & {
        *this = *this + n;
        return *this;
    }
    [[nodiscard]] auto operator*() const -> codepoint_type const { return codepoint(_ptr); }
    [[nodiscard]] auto operator!=(string_iterator const & rhs) const -> bool {
        return _ptr != rhs._ptr;
    }
    [[nodiscard]] auto operator==(string_iterator const & rhs) const -> bool {
        return _ptr == rhs._ptr;
    }
    [[nodiscard]] auto operator>(string_iterator const & rhs) const -> bool {
        return _ptr > rhs._ptr;
    }
    [[nodiscard]] auto operator>=(string_iterator const & rhs) const -> bool {
        return _ptr >= rhs._ptr;
    }
    [[nodiscard]] auto operator<(string_iterator const & rhs) const -> bool {
        return _ptr < rhs._ptr;
    }
    [[nodiscard]] auto operator<=(string_iterator const & rhs) const -> bool {
        return _ptr <= rhs._ptr;
    }

    auto ptr() const -> char const * { return _ptr; }
    auto value() const { return codepoint(_ptr).value(); }

    operator char const *() const { return ptr(); }

  private:
    char const * _ptr;
};

//-----------------------------------------------------------

inline auto operator-(string_iterator lhs, string_iterator rhs) -> bytes<isize> {
    return bytes<isize>(lhs.ptr() - rhs.ptr());
}

}  // namespace nlc
