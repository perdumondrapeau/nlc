/* Copyright © 2019-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/meta/list.hpp>
#include <nlc/meta/list/contains.hpp>
#include <nlc/meta/list/index_of.hpp>
#include <nlc/meta/traits.hpp>

#include "impl/error_count.hpp"
#include "arithmetic_types.hpp"
#include "assert.hpp"
#include "new.hpp"
#include "null.hpp"

#if !defined(NDEBUG)
#    include "string_view.hpp"
#endif

namespace nlc {

// ------------------------------------------------------

namespace errors_impl {
#if !defined(NDEBUG)
    auto get_error_name(char const * pretty_function) -> string_view;
#endif
}  // namespace errors_impl

// ------------------------------------------------------

template<typename... tags> struct forwarded_error final {
    u32 error_index;
};

template<typename T, typename... tags> class expected final {
    static_assert((meta::is_empty_type<tags> && ...));

  public:
    // Alias and constants ----------------------------------

    using value_type = T;
    using error_codes = meta::list<tags...>;
    static constexpr auto success = error_codes::size;

    // Constructors -----------------------------------------

    [[nodiscard]] constexpr expected(value_type && rhs)
        : _error_code(success) {
        new (reinterpret_cast<value_type *>(_storage)) value_type(nlc_move(rhs));
    }

    [[nodiscard]] constexpr expected(value_type const & rhs)
        : _error_code(success) {
        new (reinterpret_cast<value_type *>(_storage)) value_type(rhs);
    }

    template<typename Q, typename... otags>
    [[nodiscard]] constexpr expected(expected<Q, otags...> const & rhs) {
        if (rhs.succeeded()) {
            _error_code = success;
            new (reinterpret_cast<value_type *>(_storage)) value_type(rhs.get());
        } else {
            _take_error_code(rhs);
            ++errors_impl::get_error_count();
        }
    }

    template<typename Q, typename... otags>
    [[nodiscard]] constexpr expected(expected<Q, otags...> && rhs) {
        if (rhs.succeeded()) {
            _error_code = success;
            new (reinterpret_cast<value_type *>(_storage)) value_type(nlc_move(rhs).get());
        } else {
            _take_error_code(rhs);
            ++errors_impl::get_error_count();
        }
    }

    template<typename... otags>
    [[nodiscard]] constexpr expected(forwarded_error<otags...> const & error) {
        _take_error_code(error);
        ++errors_impl::get_error_count();
    }

    template<typename tag>
    [[nodiscard]] constexpr expected(tag)
        requires meta::contains<error_codes, tag>
        : _error_code(meta::index_of<tag, error_codes>) {
        ++errors_impl::get_error_count();
    }

    ~expected() {
        if (succeeded())
            reinterpret_cast<value_type *>(_storage)->~value_type();
        else
            --errors_impl::get_error_count();
    }

    // Access to status and error ---------------------------

    [[nodiscard]] constexpr auto succeeded() const { return _error_code == success; }
    [[nodiscard]] constexpr auto failed() const { return _error_code != success; }
    template<typename tag> [[nodiscard]] constexpr auto failed_with() const {
        return _error_code == meta::index_of<tag, error_codes>;
    }

    // Access to value in case of success -------------------

    [[nodiscard]] auto get() const & -> value_type const & {
        nlc_assert_msg(succeeded(), "error: ", get_error_name());
        return *reinterpret_cast<value_type const *>(_storage);
    }

    [[nodiscard]] auto get() & -> value_type & {
        nlc_assert_msg(succeeded(), "error: ", get_error_name());
        return *reinterpret_cast<value_type *>(_storage);
    }

    [[nodiscard]] auto get() && -> value_type {
        nlc_assert_msg(succeeded(), "error: ", get_error_name());
        return nlc_move(*reinterpret_cast<value_type *>(_storage));
    }

    // Automatic conversion to value in case of success -----

    [[nodiscard]] operator const value_type &() const & {
        nlc_assert_msg(succeeded(), "error: ", get_error_name());
        return *reinterpret_cast<value_type const *>(_storage);
    }

    [[nodiscard]] operator value_type &() & {
        nlc_assert_msg(succeeded(), "error: ", get_error_name());
        return *reinterpret_cast<value_type *>(_storage);
    }

    [[nodiscard]] operator value_type() && {
        nlc_assert_msg(succeeded(), "error: ", get_error_name());
        return nlc_move(*reinterpret_cast<value_type *>(_storage));
    }

    // Helper to forward error to another expected type -----

    [[nodiscard]] constexpr auto forward_error() const -> forwarded_error<tags...> {
        nlc_assert(failed());
        return forwarded_error<tags...> { _error_code };
    }

#if !defined(NDEBUG)
    [[nodiscard]] inline constexpr auto get_error_name() const {
        nlc_assert(failed());
        string_view info;

        auto aux = [&](auto arg) {
            using tag = decltype(arg);
            if (failed_with<tag>())
                info = errors_impl::get_error_name(__PRETTY_FUNCTION__);
        };

        (aux(tags {}), ...);
        nlc_assert(info.ptr() != nullptr);

        return info;
    }
#endif

  private:
    template<typename Q, typename... otags>
    constexpr auto _take_error_code(expected<Q, otags...> const & rhs) {
        auto aux = [&](auto arg) {
            using tag = decltype(arg);
            if (rhs.template failed_with<tag>())
                _error_code = meta::index_of<tag, error_codes>;
        };
        (aux(otags {}), ...);
    }

    template<typename... otags>
    constexpr auto _take_error_code(forwarded_error<otags...> const & error) {
        auto const aux = [this, &error](auto arg) {
            using tag = decltype(arg);
            if constexpr (meta::contains<error_codes, tag>) {
                if (meta::index_of<tag, meta::list<otags...>> == error.error_index) {
                    _error_code = meta::index_of<tag, error_codes>;
                }
            }
        };
        (aux(otags {}), ...);
        nlc_assert(failed());
    }

  private:
    alignas(value_type) byte _storage[sizeof(value_type)];
    u32 _error_code;
};

// ------------------------------------------------------

template<typename... tags> class expected<void, tags...> final {
    static_assert((meta::is_empty_type<tags> && ...));

  public:
    // Alias and constants ----------------------------------

    using value_type = void;
    using error_codes = meta::list<tags...>;
    static constexpr auto success = error_codes::size;

    // Constructors -----------------------------------------

    [[nodiscard]] constexpr expected()
        : _error_code(success) {}

    [[nodiscard]] constexpr expected(null_type)
        : expected() {}

    template<typename... otags>
    [[nodiscard]] constexpr expected(expected<void, otags...> const & rhs) {
        if (rhs.succeeded()) {
            _error_code = success;
        } else {
            _take_error_code(rhs);
            ++errors_impl::get_error_count();
        }
    }

    template<typename tag>
    [[nodiscard]] constexpr expected(tag)
        requires meta::contains<error_codes, tag>
        : _error_code(meta::index_of<tag, error_codes>) {
        ++errors_impl::get_error_count();
    }

    template<typename... otags>
    [[nodiscard]] constexpr expected(forwarded_error<otags...> const & error) {
        _take_error_code<otags...>(error);
        ++errors_impl::get_error_count();
    }

    ~expected() {
        if (failed())
            --errors_impl::get_error_count();
    }

    // Access to status and error ---------------------------

    [[nodiscard]] constexpr auto succeeded() const { return _error_code == success; }
    [[nodiscard]] constexpr auto failed() const { return _error_code != success; }

    template<typename tag> [[nodiscard]] constexpr auto failed_with() const {
        return _error_code == meta::index_of<tag, error_codes>;
    }

    // Helper to forward error to another expected type -----

    [[nodiscard]] constexpr auto forward_error() const -> forwarded_error<tags...> {
        nlc_assert(failed());
        return forwarded_error<tags...> { _error_code };
    }

#if !defined(NDEBUG)
    [[nodiscard]] inline constexpr auto get_error_name() const {
        nlc_assert(failed());
        string_view info;

        auto aux = [&](auto arg) {
            using tag = decltype(arg);
            if (failed_with<tag>())
                info = errors_impl::get_error_name(__PRETTY_FUNCTION__);
        };

        (aux(tags {}), ...);
        nlc_assert(info.ptr() != nullptr);

        return info;
    }
#endif

  private:
    template<typename Q, typename... otags>
    constexpr auto _take_error_code(expected<Q, otags...> const & rhs) {
        auto aux = [&](auto arg) {
            using tag = decltype(arg);
            if (rhs.template failed_with<tag>())
                _error_code = meta::index_of<tag, error_codes>;
        };
        (aux(otags {}), ...);
    }

    template<typename... otags>
    constexpr auto _take_error_code(forwarded_error<otags...> const & error) {
        auto const aux = [this, &error](auto arg) {
            using tag = decltype(arg);
            if constexpr (meta::contains<error_codes, tag>) {
                if (meta::index_of<tag, meta::list<otags...>> == error.error_index) {
                    _error_code = meta::index_of<tag, error_codes>;
                }
            }
        };
        (aux(otags {}), ...);
        nlc_assert(failed());
    }

  private:
    u32 _error_code;
};

// ------------------------------------------------------

}  // namespace nlc
