/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "assert.hpp"

namespace nlc {
template<typename T> class [[nodiscard]] pointer final {
  public:
    using pointee_type = T;

  public:
    // Default constructors and assignements ----------------
    constexpr pointer() = delete;  // Disable default construction
    constexpr pointer(pointer const &) = default;
    constexpr pointer(pointer &&) = default;
    constexpr auto operator=(pointer const &) & -> pointer & = default;
    constexpr auto operator=(pointer &&) & -> pointer & = default;

    // Constructor and assignement from pointer -------------
    constexpr pointer(T * const ptr)
        : _ptr { ptr } {
        nlc_assert(nullptr != ptr);
    }

    constexpr auto operator=(T * const ptr) -> pointer & {
        nlc_assert(nullptr != ptr);
        _ptr = ptr;
        return *this;
    }

    // Disable construction and assignement from nullptr ----
    constexpr explicit pointer(decltype(nullptr)) = delete;
    constexpr auto operator=(decltype(nullptr)) -> pointer & = delete;

    // Accessor and dereferencing ---------------------------
    [[nodiscard]] constexpr auto operator*() const -> T & { return *_ptr; }
    [[nodiscard]] constexpr auto operator->() const -> T * { return _ptr; }
    constexpr auto get() const -> T * { return _ptr; }

    // Comparison operators ---------------------------------
    [[nodiscard]] friend constexpr auto operator==(pointer const & lhs, pointer const & rhs) -> bool {
        return lhs._ptr == rhs._ptr;
    }

    [[nodiscard]] friend constexpr auto operator==(T * const lhs, pointer const & rhs) -> bool {
        return lhs == rhs._ptr;
    }

    [[nodiscard]] friend constexpr auto operator==(pointer const & lhs, T * const rhs) -> bool {
        return lhs._ptr == rhs;
    }

    [[nodiscard]] friend constexpr auto operator!=(pointer const & lhs, pointer const & rhs) {
        return lhs._ptr != rhs._ptr;
    }

    [[nodiscard]] friend constexpr auto operator!=(T * const lhs, pointer const & rhs) -> bool {
        return lhs != rhs._ptr;
    }

    [[nodiscard]] friend constexpr auto operator!=(pointer const & lhs, T * rhs) -> bool {
        return lhs._ptr != rhs;
    }

  private:
    T * _ptr;
};

template<typename T> pointer(T *) -> pointer<T>;

}  // namespace nlc
