/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/containers/vector.hpp>
#include <nlc/containers/vector/byte_stream.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/align.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/file_stream.hpp>
#include <nlc/test.hpp>

#include "impl/foo.hpp"
#include "impl/int_pair.hpp"

using namespace nlc;

namespace g {
// workaround for msvc shadowing vector's implementation
constexpr auto count = 33u;
}  // namespace g

nlc_test("create_and_reserve") {
    standard_allocator malloc;

    auto const vec = vector<i32>::create_and_reserve(malloc, 17u);
    nlc_check(vec.capacity() >= 17u);
    nlc_check(vec.is_empty());
}

nlc_test("create_and_append") {
    standard_allocator malloc;

    auto const vec1 = vector<i32>::create_and_append(malloc, 13);
    nlc_check(vec1.size() == 1);
    nlc_check(vec1.front() == 13);

    auto const vec2 = vector<int_pair>::create_and_append(malloc, 16, 32);
    nlc_check(vec2.size() == 1);
    nlc_check(vec2.front() == int_pair { 16, 32 });

    auto const vec3 = vector<Foo>::create_and_append(malloc, 12u);
    nlc_check(vec3.size() == 1);
    nlc_check(vec3.front()._a == 12u);

    auto const vec4 = vector<Foo>::create_and_append(malloc, false, 16u, 3.2f);
    nlc_check(vec4.size() == 1);
    nlc_check(vec4.front()._a == 16u);
}

nlc_test("create_and_append_many") {
    standard_allocator malloc;

    auto const vec1 = vector<i32>::create_and_append_many(malloc, 13, 10, 12);
    nlc_check(vec1.size() == 3);
    nlc_check(vec1[0] == 13);
    nlc_check(vec1[1] == 10);
    nlc_check(vec1[2] == 12);

    auto const vec2 =
        vector<int_pair>::create_and_append_many(malloc, int_pair { 16, 32 }, int_pair { 12, -5 });
    nlc_check(vec2.size() == 2);
    nlc_check(vec2[0] == int_pair { 16, 32 });
    nlc_check(vec2[1] == int_pair { 12, -5 });

    auto const vec3 = vector<Foo>::create_and_append_many(malloc, 12u, 15u, 0u);
    nlc_check(vec3.size() == 3);
    nlc_check(vec3[0]._a == 12u);
    nlc_check(vec3[1]._a == 15u);
    nlc_check(vec3[2]._a == 0u);

    auto const vec4 =
        vector<Foo>::create_and_append_many(malloc, Foo { false, 16u, 3.2f }, Foo { true, 8u, 5.687f });
    nlc_check(vec4.size() == 2);
    nlc_check(vec4[0]._a == 16u);
    nlc_check(vec4[1]._a == 8u);
}

nlc_test("create_and_fill") {
    standard_allocator malloc;

    auto const vec1 = vector<i32>::create_and_fill(malloc, 13, 5);
    nlc_check(vec1.size() == 5);
    for (usize i = 0u; i < vec1.size(); ++i)
        nlc_check_msg(vec1[i] == 13, "vec[", i, "] = ", vec1[i]);

    static int_pair const pair { 16, 32 };
    auto const vec2 = vector<int_pair>::create_and_fill(malloc, pair, 4);
    nlc_check(vec2.size() == 4);
    for (usize i = 0u; i < vec2.size(); ++i)
        nlc_check_msg(vec2[i] == pair, "vec[", i, "] = {", vec2[i].a, ", ", vec2[i].b, "}");

    auto const vec3 = vector<Foo>::create_and_fill(malloc, 12u, 6);
    nlc_check(vec3.size() == 6);
    for (usize i = 0u; i < vec3.size(); ++i)
        nlc_check_msg(vec3[i] == Foo { 12u }, "vec[", i, "] = ", vec3[i]._a);

    auto const vec4 = vector<Foo>::create_and_fill(malloc, { false, 16u, 3.2f }, 17);
    nlc_check(vec4.size() == 17);
    for (usize i = 0u; i < vec4.size(); ++i)
        nlc_check_msg(vec4[i]._a == 16u, "vec[", i, "] = ", vec4[i]._a);
}

nlc_test("resize") {
    standard_allocator malloc;

    vector<i32> vec1(malloc);
    vec1.resize(5, 13);
    nlc_check(vec1.size() == 5);
    for (usize i = 0u; i < vec1.size(); ++i)
        nlc_check_msg(vec1[i] == 13, "vec[", i, "] = ", vec1[i]);
    vec1.resize(3);
    nlc_check(vec1.size() == 3);

    static int_pair const pair { 16, 32 };
    vector<int_pair> vec2(malloc);
    vec2.resize(4, pair);
    nlc_check(vec2.size() == 4);
    for (usize i = 0u; i < vec2.size(); ++i)
        nlc_check_msg(vec2[i] == pair, "vec[", i, "] = {", vec2[i].a, ", ", vec2[i].b, "}");
    vec2.resize(2, 0, 0);
    nlc_check(vec2.size() == 2);

    vector<Foo> vec3(malloc);
    vec3.resize(6, 12u);
    nlc_check(vec3.size() == 6);
    for (usize i = 0u; i < vec3.size(); ++i)
        nlc_check_msg(vec3[i] == Foo { 12u }, "vec[", i, "] = ", vec3[i]._a);
    vec3.resize(4, 0u);
    nlc_check(vec3.size() == 4);

    vector<Foo> vec4(malloc);
    vec4.resize(17, false, 16u, 3.2f);
    nlc_check(vec4.size() == 17);
    for (usize i = 0u; i < vec4.size(); ++i)
        nlc_check_msg(vec4[i]._a == 16u, "vec[", i, "] = ", vec4[i]._a);
    vec4.resize(10, false, 0u, 0.0f);
    nlc_check(vec4.size() == 10);
}

nlc_test("append") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    for (auto i = 0u; i < g::count; ++i) {
        nlc_check(v.append(i) == i);
    }
    nlc_check(v.size() == g::count);
    nlc_check(v.capacity() >= v.size());
    [[maybe_unused]] auto const cap = v.capacity();
    nlc_check((cap & (cap - 1)) == 0);

    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v[i] == i);

    nlc_check(v.front() == 0u);
    nlc_check(v.back() == g::count - 1u);
}

nlc_test("append ctor") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    for (auto i = 0u; i < g::count; ++i) {
        v.append(i % 2 == 0, i, static_cast<float>(i) * 123.456f);
        nlc_check(v[i] == i);
    }
    nlc_check(v.size() == g::count);
    nlc_check(v.capacity() >= v.size());
    [[maybe_unused]] auto const cap = v.capacity();
    nlc_check((cap & (cap - 1)) == 0);

    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v[i] == i);
}

nlc_test("append_many") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    for (auto i = 0u; i < g::count; ++i) {
        v.append_many(0u, 1u, 2u);
    }
    nlc_check(v.size() == 3 * g::count);
    nlc_check(v.capacity() >= v.size());
    [[maybe_unused]] auto const cap = v.capacity();
    nlc_check((cap & (cap - 1)) == 0);

    for (auto i = 0u; i < g::count * 3; i += 3) {
        nlc_check(v[i] == 0u);
        nlc_check(v[i + 1] == 1u);
        nlc_check(v[i + 2] == 2u);
    }
}

nlc_test("append_from empty") {
    nlc::standard_allocator allocator;
    vector<unsigned> u(allocator);
    u.append_from(nlc::span<unsigned>());
    nlc_check(u.is_empty());

    vector<unsigned> v(allocator);
    v.append_from(nlc_move(u));
    nlc_check(v.is_empty());
}

nlc_test("append_from_no_grow empty") {
    nlc::standard_allocator allocator;
    vector<unsigned> u(allocator);
    u.append_from_no_grow(nlc::span<unsigned>());
    nlc_check(u.is_empty());

    vector<unsigned> v(allocator);
    v.append_from_no_grow(nlc_move(u));
    nlc_check(v.is_empty());
}

nlc_test("append_from & copy") {
    nlc::standard_allocator allocator;
    vector<unsigned> u(allocator);
    u.append_many(0u, 1u, 2u);

    vector<unsigned> v(allocator);
    v.append_many(3u, 4u, 5u);

    u.append_from(make_span(v));

    nlc_check(u.size() == 6u);
    nlc_check(v.size() == 3u);
    for (auto i = 0u; i < u.size(); ++i)
        nlc_check(u[i] == i);
    for (auto i = 0u; i < v.size(); ++i)
        nlc_check(v[i] == i + 3u);
}

nlc_test("append_from & move") {
    nlc::standard_allocator allocator;
    vector<unsigned> u(allocator);
    u.append_many(0u, 1u, 2u);

    vector<unsigned> v(allocator);
    v.append_many(3u, 4u, 5u);

    u.append_from(nlc_move(v));

    nlc_check(u.size() == 6u);
    nlc_check(v.is_empty());
    for (auto i = 0u; i < u.size(); ++i)
        nlc_check(u[i] == i);
}

nlc_test("remove_unordered") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    for (auto i = 0u; i < g::count; ++i) {
        v.append(i);
        nlc_check(v[i] == i);
    }
    nlc_check(Foo::_instances != 0);
    nlc_check(v.size() == static_cast<usize>(g::count));
    nlc_check(v.capacity() >= v.size());
    [[maybe_unused]] auto const cap = v.capacity();
    nlc_check((cap & (cap - 1)) == 0);

    for (auto i = 0u; i < g::count - 1; ++i) {
        v.remove_unordered(0);
        nlc_check(v.size() == static_cast<usize>(g::count) - i - 1);
        nlc_check(v[0] == g::count - i - 1);
    }
    nlc_check(v.size() == 1u);
}

nlc_test("remove_ordered") {
    {
        nlc::standard_allocator allocator;
        vector<Foo> v(allocator);

        v.append_many(0u, 1u, 2u, 3u, 4u, 5u, 6u, 7u);
        v.remove_ordered(2);
        nlc_check(v.size() == 7u);
        nlc_check(Foo::_instances != 0);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 3u);
        nlc_check(v[3] == 4u);
        nlc_check(v[4] == 5u);
        nlc_check(v[5] == 6u);
        nlc_check(v[6] == 7u);
        v.remove_ordered(6);
        nlc_check(v.size() == 6u);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 3u);
        nlc_check(v[3] == 4u);
        nlc_check(v[4] == 5u);
        nlc_check(v[5] == 6u);
        v.remove_ordered(0);
        nlc_check(v.size() == 5u);
        nlc_check(v[0] == 1u);
        nlc_check(v[1] == 3u);
        nlc_check(v[2] == 4u);
        nlc_check(v[3] == 5u);
        nlc_check(v[4] == 6u);
    }
    nlc_check(Foo::_instances == 0);
}

nlc_test("extract_ordered") {
    {
        nlc::standard_allocator allocator;
        vector<Foo> v(allocator);

        v.append_many(0u, 1u, 2u, 3u, 4u, 5u, 6u, 7u);
        nlc_check(v.extract_ordered(2) == 2u);
        nlc_check(v.size() == 7u);
        nlc_check(Foo::_instances != 0);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 3u);
        nlc_check(v[3] == 4u);
        nlc_check(v[4] == 5u);
        nlc_check(v[5] == 6u);
        nlc_check(v[6] == 7u);
        nlc_check(v.extract_ordered(6) == 7u);
        nlc_check(v.size() == 6u);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 3u);
        nlc_check(v[3] == 4u);
        nlc_check(v[4] == 5u);
        nlc_check(v[5] == 6u);
        nlc_check(v.extract_ordered(0) == 0u);
        nlc_check(v.size() == 5u);
        nlc_check(v[0] == 1u);
        nlc_check(v[1] == 3u);
        nlc_check(v[2] == 4u);
        nlc_check(v[3] == 5u);
        nlc_check(v[4] == 6u);
    }
    nlc_check(Foo::_instances == 0);
}

nlc_test("extract_unordered") {
    {
        nlc::standard_allocator allocator;
        vector<Foo> v(allocator);

        v.append_many(0u, 1u, 2u, 3u, 4u, 5u, 6u, 7u);
        nlc_check(v.extract_unordered(2) == 2u);
        nlc_check(v.size() == 7u);
        nlc_check(Foo::_instances != 0);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 7u);
        nlc_check(v[3] == 3u);
        nlc_check(v[4] == 4u);
        nlc_check(v[5] == 5u);
        nlc_check(v[6] == 6u);
        nlc_check(v.extract_unordered(6) == 6u);
        nlc_check(v.size() == 6u);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 7u);
        nlc_check(v[3] == 3u);
        nlc_check(v[4] == 4u);
        nlc_check(v[5] == 5u);
        nlc_check(v.extract_unordered(0) == 0u);
        nlc_check(v.size() == 5u);
        nlc_check(v[0] == 5u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 7u);
        nlc_check(v[3] == 3u);
        nlc_check(v[4] == 4u);
    }
    nlc_check(Foo::_instances == 0);
}

nlc_test("operator[] non const") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    for (auto i = 0u; i < g::count; ++i) {
        v.append(0u);
    }
    nlc_check(Foo::_instances != 0);
    for (auto i = 0u; i < g::count; ++i) {
        v[i] = i;
    }
    nlc_check(v.size() == g::count);
    nlc_check(v.capacity() >= v.size());
    [[maybe_unused]] auto const cap = v.capacity();
    nlc_check((cap & (cap - 1)) == 0);

    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v[i] == i);
}

nlc_test("iteration") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    for (auto i = 0u; i < g::count; ++i)
        v.append(i);

    [[maybe_unused]] u32 n = 0;
    for (auto i : v)
        nlc_check(i == n++);

    n = 0;
    for (auto i : const_cast<vector<Foo> const &>(v))
        nlc_check(i == n++);

    for (auto & i : v)
        i._a += 123456;

    n = 0;
    for (auto i : v)
        nlc_check(i == n++ + 123456);
}

nlc_test("Copy ctor empty") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);
    vector<Foo> w(v);
    nlc_check(v.size() == w.size());
    nlc_check(v.capacity() == w.capacity());
}

nlc_test("Copy ctor with elements") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    for (auto i = 0u; i < g::count; ++i) {
        v.append(i);
    }

    vector<Foo> w(v);
    nlc_check(v.size() == w.size());
    nlc_check(v.capacity() == w.capacity());

    for (auto i = 0u; i < g::count; ++i) {
        nlc_check(v[i] == w[i]);
    }
}

nlc_test("Move ctor empty") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);
    vector<Foo> w(nlc_move(v));
    nlc_check(v.size() == w.size());
    nlc_check(v.capacity() == w.capacity());
}

nlc_test("Move ctor with elements") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    for (auto i = 0u; i < g::count; ++i) {
        v.append(i);
    }

    vector<Foo> w(nlc_move(v));
    nlc_check(v.size() == 0u);
    nlc_check(v.capacity() == 0u);
    nlc_check(w.size() == g::count);

    for (auto i = 0u; i < g::count; ++i) {
        nlc_check(w[i] == i);
    }
}

nlc_test("Move operator=") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);
    vector<Foo> w(allocator);
    w = nlc_move(v);
    nlc_check(v.size() == w.size());
    nlc_check(v.capacity() == w.capacity());
}

nlc_test("Move operator= with elements") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);
    vector<Foo> w(allocator);

    for (auto i = 0u; i < g::count; ++i) {
        v.append(i);
        w.append(i * 123);
    }

    w = nlc_move(v);
    nlc_check(v.size() == 0u);
    nlc_check(v.capacity() == 0u);
    nlc_check(w.size() == g::count);

    for (auto i = 0u; i < g::count; ++i) {
        nlc_check(w[i] == i);
    }
}

nlc_test("extract_last") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    for (auto i = 0u; i < g::count; ++i) {
        v.append(i);
        nlc_check(v[i] == i);
    }

    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v.extract_last() == g::count - i - 1);
    nlc_check(v.is_empty());
}

nlc_test("reserve") {
    auto constexpr reserve_size = 13u;

    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    v.reserve(reserve_size);
    nlc_check(v.size() == 0u);
    nlc_check(v.capacity() == ceil_power_of_two(reserve_size));

    for (auto i = 0u; i < reserve_size; ++i) {
        v.append(i);
        nlc_check(v[i] == i);
    }
    nlc_check(v.size() == reserve_size);
    nlc_check(v.capacity() == ceil_power_of_two(reserve_size));

    for (auto i = 0u; i < g::count - reserve_size; ++i) {
        v.append(i + reserve_size);
    }
    nlc_check(v.size() == g::count);

    v.reserve(g::count * 3);
    nlc_check(v.size() == g::count);
    nlc_check(v.capacity() == ceil_power_of_two(g::count * 3));
    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v[i] == i);
}

nlc_test("xyz_no_grow") {
    auto constexpr reserve_size = 13u;

    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    v.reserve(reserve_size);
    nlc_check(v.size() == 0u);
    nlc_check(v.capacity() == ceil_power_of_two(reserve_size));

    for (auto i = 0u; i < reserve_size; ++i) {
        v.append(i);
        nlc_check(v[i] == i);
    }
    nlc_check(v.size() == reserve_size);
    nlc_check(v.capacity() == ceil_power_of_two(reserve_size));

    for (auto i = 0u; i < g::count - reserve_size; ++i) {
        v.append(i + reserve_size);
    }
    nlc_check(v.size() == g::count);

    v.reserve(g::count * 3);
    nlc_check(v.size() == g::count);
    nlc_check(v.capacity() == ceil_power_of_two(g::count * 3));
    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v[i] == i);
}

nlc_test("clear_release_memory") {
    {
        nlc::standard_allocator allocator;
        vector<Foo> v(allocator);

        v.reserve(6u);
        v.clear_release_memory();
        v.reserve(12u);
    }
    nlc_check(Foo::_instances == 0);
}

nlc_test("release_unused_memory") {
    nlc::standard_allocator allocator;
    vector<Foo> v(allocator);

    v.append_many(0u, 1u, 2u, 3u, 4u, 5u);
    nlc_check(v.capacity() == 8u);
    v.remove_unordered(0);
    v.remove_unordered(0);
    nlc_check(v.capacity() == 8u);
    v.release_unused_memory();
    nlc_check(v.capacity() == 4u);

    v.append(6u);
    nlc_check(v.capacity() == 8u);
}

struct A final {
    static int c_default, c_copy, c_move, c_destr;
    A() { ++c_default; }
    A(A &&) { ++c_move; }
    A(A const &) { ++c_copy; }
    ~A() { ++c_destr; }

    static void reset() {
        c_default = 0;
        c_copy = 0;
        c_move = 0;
        c_destr = 0;
    }
};

int A::c_default = 0;
int A::c_move = 0;
int A::c_copy = 0;
int A::c_destr = 0;

nlc_test("check no constructor is called on a move") {
    nlc::standard_allocator allocator;

    A::reset();
    {
        auto v1 = vector<A>::create_and_fill(allocator, {}, 10);
        nlc_check(A::c_default == 1);
        nlc_check(A::c_copy == 10);
        nlc_check(A::c_move == 0);
        nlc_check(A::c_destr == 1);
        auto v2 = nlc_move(v1);
        nlc_check(A::c_default == 1);
        nlc_check(A::c_copy == 10);
        nlc_check(A::c_move == 0);
        nlc_check(A::c_destr == 1);
        v2 = vector<A>::create_and_fill(allocator, {}, 5);
        nlc_check(A::c_default == 2);
        nlc_check(A::c_copy == 15);
        nlc_check(A::c_move == 0);
        nlc_check(A::c_destr == 12);
    }
    nlc_check(A::c_default == 2);
    nlc_check(A::c_copy == 15);
    nlc_check(A::c_move == 0);
    nlc_check(A::c_destr == 17);
}

nlc_test("check constructors are called on copy") {
    nlc::standard_allocator allocator;

    A::reset();
    {
        auto v1 = vector<A>::create_and_fill(allocator, {}, 10);
        nlc_check(A::c_default == 1);
        nlc_check(A::c_copy == 10);
        nlc_check(A::c_move == 0);
        nlc_check(A::c_destr == 1);
        auto v2 = v1;
        nlc_check(A::c_default == 1);
        nlc_check(A::c_copy == 20);
        nlc_check(A::c_move == 0);
        nlc_check(A::c_destr == 1);
        v2 = vector<A>::create_and_fill(allocator, {}, 5);
        nlc_check(A::c_default == 2);
        nlc_check(A::c_copy == 25);
        nlc_check(A::c_move == 0);
        nlc_check(A::c_destr == 12);
    }
    nlc_check(A::c_default == 2);
    nlc_check(A::c_copy == 25);
    nlc_check(A::c_move == 0);
    nlc_check(A::c_destr == 27);
}

struct dummy {
    int a = 0;
};

static auto to_byte_stream(nlc::output_byte_stream & out, dummy const & d) -> void {
    out.write(d.a);
}
static auto from_byte_stream(nlc::input_byte_stream & out, dummy & d) -> void { out.read(d.a); }

struct dummy_bulk {
    int a = 0;
};

template<> inline constexpr auto nlc::is_bulk_byte_streamable<dummy_bulk> = true;

nlc_test("streaming") {
    nlc::standard_allocator allocator;

    nlc_test_case("integral type") {
        auto v = vector<int>::create_and_append_many(allocator, 13, 12, 1312, 0xACAB);

        {  // scope to force file flush on destructor
            auto tmp = nlc::output_file_stream::open(allocator, "nlc_vec_out_test", 4096);
            nlc_check(tmp.succeeded());
            auto & out = tmp.get();

            out.write(v);

            nlc_check(out.any_error_occurred() == false);
        }

        auto tmp2 = nlc::input_file_stream::open(allocator, "nlc_vec_out_test", 4096);
        nlc_check(tmp2.succeeded());
        auto & in = tmp2.get();

        v.clear_release_memory();
        nlc_check(v.size() == 0);
        in.read(v);
        nlc_check(in.any_error_occurred() == false);
        nlc_check(v.size() == 4);
        nlc_check(v[0] == 13);
        nlc_check(v[1] == 12);
        nlc_check(v[2] == 1312);
        nlc_check(v[3] == 0xACAB);
    };

    nlc_test_case("custom type") {
        auto v = vector<dummy>::create_and_append_many(allocator, 13, 12, 1312, 0xACAB);
        {  // scope to force file flush on destructor
            auto tmp = nlc::output_file_stream::open(allocator, "nlc_vec_out_test", 4096);
            nlc_check(tmp.succeeded());
            auto & out = tmp.get();

            out.write(v);

            nlc_check(out.any_error_occurred() == false);
        }

        auto tmp2 = nlc::input_file_stream::open(allocator, "nlc_vec_out_test", 4096);
        nlc_check(tmp2.succeeded());
        auto & in = tmp2.get();

        v.clear_release_memory();
        nlc_check(v.size() == 0);
        in.read(v);
        nlc_check(in.any_error_occurred() == false);
        nlc_check(v.size() == 4);
        nlc_check(v[0].a == 13);
        nlc_check(v[1].a == 12);
        nlc_check(v[2].a == 1312);
        nlc_check(v[3].a == 0xACAB);
    };

    nlc_test_case("custom type bulk") {
        auto v = vector<dummy_bulk>::create_and_append_many(allocator, 13, 12, 1312, 0xACAB);
        {  // scope to force file flush on destructor
            auto tmp = nlc::output_file_stream::open(allocator, "nlc_vec_out_test", 4096);
            nlc_check(tmp.succeeded());
            auto & out = tmp.get();

            out.write(v);

            nlc_check(out.any_error_occurred() == false);
        }

        auto tmp2 = nlc::input_file_stream::open(allocator, "nlc_vec_out_test", 4096);
        nlc_check(tmp2.succeeded());
        auto & in = tmp2.get();

        v.clear_release_memory();
        nlc_check(v.size() == 0);
        in.read(v);
        nlc_check(in.any_error_occurred() == false);
        nlc_check(v.size() == 4);
        nlc_check(v[0].a == 13);
        nlc_check(v[1].a == 12);
        nlc_check(v[2].a == 1312);
        nlc_check(v[3].a == 0xACAB);
    };
}
