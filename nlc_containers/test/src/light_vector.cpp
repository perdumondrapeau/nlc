/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/containers/light_vector.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/align.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/test.hpp>

#include "impl/foo.hpp"
#include "impl/int_pair.hpp"

using namespace nlc;

namespace g {
// workaround for msvc shadowing light_vector's implementation
constexpr auto count = 33u;
}  // namespace g

nlc_test("create_and_reserve") {
    standard_allocator malloc;

    auto vec = light_vector<i32>::create_and_reserve(malloc, 17u);
    nlc_check(vec.capacity() >= 17u);
    nlc_check(vec.is_empty());
    vec.clear_release_memory(malloc);
}

nlc_test("create_and_append") {
    standard_allocator malloc;

    auto vec1 = light_vector<i32>::create_and_append(malloc, 13);
    nlc_check(vec1.size() == 1);
    nlc_check(vec1.front() == 13);
    vec1.clear_release_memory(malloc);

    auto vec2 = light_vector<int_pair>::create_and_append(malloc, 16, 32);
    nlc_check(vec2.size() == 1);
    nlc_check(vec2.front() == int_pair { 16, 32 });
    vec2.clear_release_memory(malloc);

    auto vec3 = light_vector<Foo>::create_and_append(malloc, 12u);
    nlc_check(vec3.size() == 1);
    nlc_check(vec3.front()._a == 12u);
    vec3.clear_release_memory(malloc);

    auto vec4 = light_vector<Foo>::create_and_append(malloc, false, 16u, 3.2f);
    nlc_check(vec4.size() == 1);
    nlc_check(vec4.front()._a == 16u);
    vec4.clear_release_memory(malloc);
}

nlc_test("create_and_append_many") {
    standard_allocator malloc;

    auto vec1 = light_vector<i32>::create_and_append_many(malloc, 13, 10, 12);
    nlc_check(vec1.size() == 3);
    nlc_check(vec1[0] == 13);
    nlc_check(vec1[1] == 10);
    nlc_check(vec1[2] == 12);
    vec1.clear_release_memory(malloc);

    auto vec2 = light_vector<int_pair>::create_and_append_many(malloc,
                                                               int_pair { 16, 32 },
                                                               int_pair { 12, -5 });
    nlc_check(vec2.size() == 2);
    nlc_check(vec2[0] == int_pair { 16, 32 });
    nlc_check(vec2[1] == int_pair { 12, -5 });
    vec2.clear_release_memory(malloc);

    auto vec3 = light_vector<Foo>::create_and_append_many(malloc, 12u, 15u, 0u);
    nlc_check(vec3.size() == 3);
    nlc_check(vec3[0]._a == 12u);
    nlc_check(vec3[1]._a == 15u);
    nlc_check(vec3[2]._a == 0u);
    vec3.clear_release_memory(malloc);

    auto vec4 = light_vector<Foo>::create_and_append_many(malloc,
                                                          Foo { false, 16u, 3.2f },
                                                          Foo { true, 8u, 5.687f });
    nlc_check(vec4.size() == 2);
    nlc_check(vec4[0]._a == 16u);
    nlc_check(vec4[1]._a == 8u);
    vec4.clear_release_memory(malloc);
}

nlc_test("create_and_fill") {
    standard_allocator malloc;

    auto vec1 = light_vector<i32>::create_and_fill(malloc, 13, 5);
    nlc_check(vec1.size() == 5);
    for (usize i = 0u; i < vec1.size(); ++i)
        nlc_check_msg(vec1[i] == 13, "vec[", i, "] = ", vec1[i]);
    vec1.clear_release_memory(malloc);

    static int_pair const pair { 16, 32 };
    auto vec2 = light_vector<int_pair>::create_and_fill(malloc, pair, 4);
    nlc_check(vec2.size() == 4);
    for (usize i = 0u; i < vec2.size(); ++i)
        nlc_check_msg(vec2[i] == pair, "vec[", i, "] = {", vec2[i].a, ", ", vec2[i].b, "}");
    vec2.clear_release_memory(malloc);

    auto vec3 = light_vector<Foo>::create_and_fill(malloc, 12u, 6);
    nlc_check(vec3.size() == 6);
    for (usize i = 0u; i < vec3.size(); ++i)
        nlc_check_msg(vec3[i] == Foo { 12u }, "vec[", i, "] = ", vec3[i]._a);
    vec3.clear_release_memory(malloc);

    auto vec4 = light_vector<Foo>::create_and_fill(malloc, { false, 16u, 3.2f }, 17);
    nlc_check(vec4.size() == 17);
    for (usize i = 0u; i < vec4.size(); ++i)
        nlc_check_msg(vec4[i]._a == 16u, "vec[", i, "] = ", vec4[i]._a);
    vec4.clear_release_memory(malloc);
}

nlc_test("resize") {
    standard_allocator malloc;

    light_vector<i32> vec1;
    vec1.resize(malloc, 5, 13);
    nlc_check(vec1.size() == 5);
    for (usize i = 0u; i < vec1.size(); ++i)
        nlc_check_msg(vec1[i] == 13, "vec[", i, "] = ", vec1[i]);
    vec1.resize(malloc, 3);
    nlc_check(vec1.size() == 3);
    vec1.clear_release_memory(malloc);

    static int_pair const pair { 16, 32 };
    light_vector<int_pair> vec2;
    vec2.resize(malloc, 4, pair);
    nlc_check(vec2.size() == 4);
    for (usize i = 0u; i < vec2.size(); ++i)
        nlc_check_msg(vec2[i] == pair, "vec[", i, "] = {", vec2[i].a, ", ", vec2[i].b, "}");
    vec2.resize(malloc, 2, 0, 0);
    nlc_check(vec2.size() == 2);
    vec2.clear_release_memory(malloc);

    light_vector<Foo> vec3;
    vec3.resize(malloc, 6, 12u);
    nlc_check(vec3.size() == 6);
    for (usize i = 0u; i < vec3.size(); ++i)
        nlc_check_msg(vec3[i] == Foo { 12u }, "vec[", i, "] = ", vec3[i]._a);
    vec3.resize(malloc, 4, 0u);
    nlc_check(vec3.size() == 4);
    vec3.clear_release_memory(malloc);

    light_vector<Foo> vec4;
    vec4.resize(malloc, 17, false, 16u, 3.2f);
    nlc_check(vec4.size() == 17);
    for (usize i = 0u; i < vec4.size(); ++i)
        nlc_check_msg(vec4[i]._a == 16u, "vec[", i, "] = ", vec4[i]._a);
    vec4.resize(malloc, 10, false, 0u, 0.0f);
    nlc_check(vec4.size() == 10);
    vec4.clear_release_memory(malloc);
}

nlc_test("append") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};

    for (auto i = 0u; i < g::count; ++i) {
        nlc_check(v.append(allocator, i) == i);
    }
    nlc_check(v.size() == g::count);
    nlc_check(v.capacity() >= v.size());
    [[maybe_unused]] auto const cap = v.capacity();
    nlc_check((cap & (cap - 1)) == 0);

    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v[i] == i);

    nlc_check(v.front() == 0u);
    nlc_check(v.back() == g::count - 1u);

    v.clear_release_memory(allocator);
}

nlc_test("append_ctor") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};

    for (auto i = 0u; i < g::count; ++i) {
        v.append(allocator, i % 2 == 0, i, static_cast<float>(i) * 123.456f);
        nlc_check(v[i] == i);
    }
    nlc_check(v.size() == g::count);
    nlc_check(v.capacity() >= v.size());
    [[maybe_unused]] auto const cap = v.capacity();
    nlc_check((cap & (cap - 1)) == 0);

    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v[i] == i);

    v.clear_release_memory(allocator);
}

nlc_test("append_many") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};

    for (auto i = 0u; i < g::count; ++i) {
        v.append_many(allocator, 0u, 1u, 2u);
    }
    nlc_check(v.size() == 3 * g::count);
    nlc_check(v.capacity() >= v.size());
    [[maybe_unused]] auto const cap = v.capacity();
    nlc_check((cap & (cap - 1)) == 0);

    for (auto i = 0u; i < g::count * 3; i += 3) {
        nlc_check(v[i] == 0u);
        nlc_check(v[i + 1] == 1u);
        nlc_check(v[i + 2] == 2u);
    }

    v.clear_release_memory(allocator);
}

nlc_test("append_from empty") {
    nlc::standard_allocator allocator;
    light_vector<unsigned> u;
    u.append_from(allocator, nlc::span<unsigned>());
    nlc_check(u.is_empty());

    light_vector<unsigned> v;
    v.append_from(allocator, nlc_move(u));
    nlc_check(v.is_empty());
}

nlc_test("append_from_no_grow empty") {
    nlc::standard_allocator allocator;
    light_vector<unsigned> u;
    u.append_from_no_grow(nlc::span<unsigned>());
    nlc_check(u.is_empty());

    light_vector<unsigned> v;
    v.append_from_no_grow(allocator, nlc_move(u));
    nlc_check(v.is_empty());
}

nlc_test("append_from_and_copy") {
    nlc::standard_allocator allocator;
    light_vector<unsigned> u;
    u.append_many(allocator, 0u, 1u, 2u);

    light_vector<unsigned> v;
    v.append_many(allocator, 3u, 4u, 5u);

    u.append_from(allocator, make_span(v));

    nlc_check(u.size() == 6u);
    nlc_check(v.size() == 3u);
    for (auto i = 0u; i < u.size(); ++i)
        nlc_check(u[i] == i);
    for (auto i = 0u; i < v.size(); ++i)
        nlc_check(v[i] == i + 3u);

    u.clear_release_memory(allocator);
    v.clear_release_memory(allocator);
}

nlc_test("append_from_and_move") {
    nlc::standard_allocator allocator;
    light_vector<unsigned> u;
    u.append_many(allocator, 0u, 1u, 2u);

    light_vector<unsigned> v;
    v.append_many(allocator, 3u, 4u, 5u);

    u.append_from(allocator, nlc_move(v));

    nlc_check(u.size() == 6u);
    nlc_check(v.is_empty());
    for (auto i = 0u; i < u.size(); ++i)
        nlc_check(u[i] == i);

    u.clear_release_memory(allocator);
}

nlc_test("remove_unordered") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};

    for (auto i = 0u; i < g::count; ++i) {
        v.append(allocator, i);
        nlc_check(v[i] == i);
    }
    nlc_check(v.size() == static_cast<usize>(g::count));
    nlc_check(v.capacity() >= v.size());
    [[maybe_unused]] auto const cap = v.capacity();
    nlc_check((cap & (cap - 1)) == 0);

    for (auto i = 0u; i < g::count - 1; ++i) {
        v.remove_unordered(0);
        nlc_check(v.size() == static_cast<usize>(g::count) - i - 1);
        nlc_check(v[0] == g::count - i - 1);
    }
    nlc_check(v.size() == 1u);

    v.clear_release_memory(allocator);
}

nlc_test("remove_ordered") {
    {
        nlc::standard_allocator allocator;
        light_vector<Foo> v {};

        v.append_many(allocator, 0u, 1u, 2u, 3u, 4u, 5u, 6u, 7u);
        v.remove_ordered(2);
        nlc_check(v.size() == 7u);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 3u);
        nlc_check(v[3] == 4u);
        nlc_check(v[4] == 5u);
        nlc_check(v[5] == 6u);
        nlc_check(v[6] == 7u);
        v.remove_ordered(6);
        nlc_check(v.size() == 6u);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 3u);
        nlc_check(v[3] == 4u);
        nlc_check(v[4] == 5u);
        nlc_check(v[5] == 6u);
        v.remove_ordered(0);
        nlc_check(v.size() == 5u);
        nlc_check(v[0] == 1u);
        nlc_check(v[1] == 3u);
        nlc_check(v[2] == 4u);
        nlc_check(v[3] == 5u);
        nlc_check(v[4] == 6u);

        v.clear_release_memory(allocator);
    }
    nlc_check(Foo::_instances == 0);
}

nlc_test("extract_ordered") {
    {
        nlc::standard_allocator allocator;
        light_vector<Foo> v {};

        v.append_many(allocator, 0u, 1u, 2u, 3u, 4u, 5u, 6u, 7u);
        nlc_check(v.extract_ordered(2) == 2u);
        nlc_check(v.size() == 7u);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 3u);
        nlc_check(v[3] == 4u);
        nlc_check(v[4] == 5u);
        nlc_check(v[5] == 6u);
        nlc_check(v[6] == 7u);
        nlc_check(v.extract_ordered(6) == 7u);
        nlc_check(v.size() == 6u);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 3u);
        nlc_check(v[3] == 4u);
        nlc_check(v[4] == 5u);
        nlc_check(v[5] == 6u);
        nlc_check(v.extract_ordered(0) == 0u);
        nlc_check(v.size() == 5u);
        nlc_check(v[0] == 1u);
        nlc_check(v[1] == 3u);
        nlc_check(v[2] == 4u);
        nlc_check(v[3] == 5u);
        nlc_check(v[4] == 6u);

        v.clear_release_memory(allocator);
    }
    nlc_check(Foo::_instances == 0);
}

nlc_test("extract_unordered") {
    {
        nlc::standard_allocator allocator;
        light_vector<Foo> v {};

        v.append_many(allocator, 0u, 1u, 2u, 3u, 4u, 5u, 6u, 7u);
        nlc_check(v.extract_unordered(2) == 2u);
        nlc_check(v.size() == 7u);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 7u);
        nlc_check(v[3] == 3u);
        nlc_check(v[4] == 4u);
        nlc_check(v[5] == 5u);
        nlc_check(v[6] == 6u);
        nlc_check(v.extract_unordered(6) == 6u);
        nlc_check(v.size() == 6u);
        nlc_check(v[0] == 0u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 7u);
        nlc_check(v[3] == 3u);
        nlc_check(v[4] == 4u);
        nlc_check(v[5] == 5u);
        nlc_check(v.extract_unordered(0) == 0u);
        nlc_check(v.size() == 5u);
        nlc_check(v[0] == 5u);
        nlc_check(v[1] == 1u);
        nlc_check(v[2] == 7u);
        nlc_check(v[3] == 3u);
        nlc_check(v[4] == 4u);

        v.clear_release_memory(allocator);
    }
    nlc_check(Foo::_instances == 0);
}

nlc_test("operator[]") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};

    for (auto i = 0u; i < g::count; ++i) {
        v.append(allocator, 0u);
    }
    for (auto i = 0u; i < g::count; ++i) {
        v[i] = i;
    }
    nlc_check(v.size() == g::count);
    nlc_check(v.capacity() >= v.size());
    [[maybe_unused]] auto const cap = v.capacity();
    nlc_check((cap & (cap - 1)) == 0);

    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v[i] == i);

    v.clear_release_memory(allocator);
}

nlc_test("iteration") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};

    for (auto i = 0u; i < g::count; ++i)
        v.append(allocator, i);

    [[maybe_unused]] u32 n = 0;
    for (auto i : v)
        nlc_check(i == n++);

    n = 0;
    for (auto i : const_cast<light_vector<Foo> const &>(v))
        nlc_check(i == n++);

    for (auto & i : v)
        i._a += 123456;

    n = 0;
    for (auto i : v)
        nlc_check(i == n++ + 123456);

    v.clear_release_memory(allocator);
}

nlc_test("copy_ctor_empty") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};
    light_vector<Foo> w(v.copy(allocator));
    nlc_check(v.size() == w.size());
    nlc_check(v.capacity() == w.capacity());
}

nlc_test("copy_ctor_used") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};

    for (auto i = 0u; i < g::count; ++i) {
        v.append(allocator, i);
    }

    light_vector<Foo> w(v.copy(allocator));
    nlc_check(v.size() == w.size());
    nlc_check(v.capacity() == w.capacity());

    for (auto i = 0u; i < g::count; ++i) {
        nlc_check(v[i] == w[i]);
    }

    v.clear_release_memory(allocator);
    w.clear_release_memory(allocator);
}

nlc_test("move_ctor_empty") {
    light_vector<Foo> v {};
    light_vector<Foo> w(nlc_move(v));
    nlc_check(v.size() == w.size());
    nlc_check(v.capacity() == w.capacity());
}

nlc_test("move_ctor_used") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};

    for (auto i = 0u; i < g::count; ++i) {
        v.append(allocator, i);
    }

    light_vector<Foo> w(nlc_move(v));
    nlc_check(v.size() == 0u);
    nlc_check(v.capacity() == 0u);
    nlc_check(w.size() == g::count);

    for (auto i = 0u; i < g::count; ++i) {
        nlc_check(w[i] == i);
    }

    w.clear_release_memory(allocator);
}

nlc_test("move operator=") {
    light_vector<Foo> v {};
    light_vector<Foo> w {};
    w = nlc_move(v);
    nlc_check(v.size() == w.size());
    nlc_check(v.capacity() == w.capacity());
}

nlc_test("move operator= used") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};
    light_vector<Foo> w {};

    for (auto i = 0u; i < g::count; ++i) {
        v.append(allocator, i);
        // w.append(allocator, i * 123); w must be empty to set a new content
    }

    w = nlc_move(v);
    nlc_check(v.size() == 0u);
    nlc_check(v.capacity() == 0u);
    nlc_check(w.size() == g::count);

    for (auto i = 0u; i < g::count; ++i) {
        nlc_check(w[i] == i);
    }

    w.clear_release_memory(allocator);
}

nlc_test("extract_last") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};

    for (auto i = 0u; i < g::count; ++i) {
        v.append(allocator, i);
        nlc_check(v[i] == i);
    }

    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v.extract_last() == g::count - i - 1);
    nlc_check(v.is_empty());

    v.clear_release_memory(allocator);
}

nlc_test("reserve") {
    nlc::standard_allocator allocator;
    auto constexpr reserve_size = 13u;

    light_vector<Foo> v {};

    v.reserve(allocator, reserve_size);
    nlc_check(v.size() == 0u);
    nlc_check(v.capacity() == ceil_power_of_two(reserve_size));

    for (auto i = 0u; i < reserve_size; ++i) {
        v.append(allocator, i);
        nlc_check(v[i] == i);
    }
    nlc_check(v.size() == reserve_size);
    nlc_check(v.capacity() == ceil_power_of_two(reserve_size));

    for (auto i = 0u; i < g::count - reserve_size; ++i) {
        v.append(allocator, i + reserve_size);
    }
    nlc_check(v.size() == g::count);

    v.reserve(allocator, g::count * 3);
    nlc_check(v.size() == g::count);
    nlc_check(v.capacity() == ceil_power_of_two(g::count * 3));
    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v[i] == i);

    v.clear_release_memory(allocator);
}

nlc_test("xyz_no_grow") {
    nlc::standard_allocator allocator;
    auto constexpr reserve_size = 13u;

    light_vector<Foo> v {};

    v.reserve(allocator, reserve_size);
    nlc_check(v.size() == 0u);
    nlc_check(v.capacity() == ceil_power_of_two(reserve_size));

    for (auto i = 0u; i < reserve_size; ++i) {
        v.append(allocator, i);
        nlc_check(v[i] == i);
    }
    nlc_check(v.size() == reserve_size);
    nlc_check(v.capacity() == ceil_power_of_two(reserve_size));

    for (auto i = 0u; i < g::count - reserve_size; ++i) {
        v.append(allocator, i + reserve_size);
    }
    nlc_check(v.size() == g::count);

    v.reserve(allocator, g::count * 3);
    nlc_check(v.size() == g::count);
    nlc_check(v.capacity() == ceil_power_of_two(g::count * 3));
    for (auto i = 0u; i < g::count; ++i)
        nlc_check(v[i] == i);

    v.clear_release_memory(allocator);
}

nlc_test("clear_release_memory") {
    {
        nlc::standard_allocator allocator;
        light_vector<Foo> v {};

        v.reserve(allocator, 6u);
        v.clear_release_memory(allocator);
        v.reserve(allocator, 12u);

        v.clear_release_memory(allocator);
    }
    nlc_check(Foo::_instances == 0);
}

nlc_test("release_unused_memory") {
    nlc::standard_allocator allocator;
    light_vector<Foo> v {};

    v.append_many(allocator, 0u, 1u, 2u, 3u, 4u, 5u);
    nlc_check(v.capacity() == 8u);
    v.remove_unordered(0);
    v.remove_unordered(0);
    nlc_check(v.capacity() == 8u);
    v.release_unused_memory(allocator);
    nlc_check(v.capacity() == 4u);

    v.append(allocator, 6u);
    nlc_check(v.capacity() == 8u);

    v.clear_release_memory(allocator);
}
