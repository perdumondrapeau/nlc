/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/shuffle.hpp>
#include <nlc/containers/hashmap.hpp>
#include <nlc/containers/vector.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/range.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/random.hpp>
#include <nlc/meta/traits.hpp>
#include <nlc/test.hpp>

#include "impl/int_pair.hpp"

using namespace nlc;

static_assert(nlc::meta::is_polymorphic<nlc::hashmap<u32, u32>> == false);
#if defined(NDEBUG)
static_assert(sizeof(nlc::hashmap<u32, u32>) == 24);
#else
static_assert(sizeof(nlc::hashmap<u32, u32>) == 32);
#endif

nlc_test("basic_usage") {
    standard_allocator malloc;
    hashmap<i32, i32> h(malloc);
    auto & v = h.insert(1, 2);
    nlc_check(h.get(1) == 2);
    v = 36;
    nlc_check(h.get(1) == 36);
    h.insert(3, 4);
    h.insert(5, 6);
    h.insert(7, 8);
    h.try_insert(9, 10);
    nlc_check(h.size() == 5u);
    h.remove(5);
    h.try_remove(1);
    nlc_check(h.size() == 3u);
    h.update(3, 789);
    nlc_check(h.try_update(1, 789) == false);
    nlc_check(h.extract(3) == 789);
    h.clear();

    // Move-assign to an empty map
    h = decltype(h) { malloc };
    nlc_check(h.size() == 0u);

    // iterators
    h.insert(3, 2);
    h.insert(5, 6);
    for (auto it = h.begin(); it != h.end(); ++it) {
        nlc_check(it->first == 3 || it->first == 5);
        nlc_check(it->second == 2 || it->second == 6);
    }

    for (auto it = h.begin(); it != h.end(); it++) {
        nlc_check(it->first == 3 || it->first == 5);
        nlc_check(it->second == 2 || it->second == 6);
    }

    auto const iterators = malloc.allocate<decltype(h.begin())>(h.size());
    defer { malloc.free(iterators); };

    auto it = h.begin();
    for (auto i = 0u; i < h.size(); ++i) {
        iterators[i] = it++;
        nlc_check(iterators[i]->first == 3 || iterators[i]->first == 5);
        nlc_check(iterators[i]->second == 2 || iterators[i]->second == 6);
    }
}

nlc_test("create_and_reserve") {
    standard_allocator malloc;
    auto const map = hashmap<i32, i32>::create_and_reserve(malloc, 17u);
    nlc_check(map.capacity() >= 17u);
    nlc_check(map.is_empty());
}

nlc_test("create_and_emplace") {
    standard_allocator malloc;
    auto const map1 = hashmap<i32, i32>::create_and_emplace(malloc, 13, 12);
    auto const try_get_re1 = map1.try_get(13);
    nlc_check(try_get_re1.has_value());
    nlc_check(try_get_re1.value() == 12);

    auto const map2 = hashmap<i32, int_pair>::create_and_emplace(malloc, 1337, 16, 32);
    auto const try_get_res2 = map2.try_get(1337);
    nlc_check(try_get_res2.has_value());
    nlc_check(try_get_res2.value().a == 16 && try_get_res2.value().b == 32);
}

nlc_test("reserve") {
    standard_allocator malloc;
    hashmap<i32, i32> map { malloc };

    map.reserve(20);
    auto const initial_capacity = map.capacity();
    nlc_check(initial_capacity >= 20);
    for (i32 i : range(0, 20))
        map.insert(i, i + 10);
    nlc_check(initial_capacity == map.capacity());
}

nlc_test("grow") {
    standard_allocator malloc;
    hashmap<u32, u32> map { malloc };

    constexpr u32 size = 12456;

    for (u32 i : range(0u, size)) {
        map.insert(i, i);
    }
    nlc_check(map.size() == size);

    u32 i = 0;
    for (auto const & [k, v] : map) {
        nlc_check(k == v);
        i += 1;
    }
    nlc_check(i == size);

    i = 0;
    while (i < size) {
        nlc_check(map.get(i) == i);
        ++i;
    }
}

nlc_test("copy") {
    standard_allocator malloc;
    hashmap<u32, u32> map { malloc };

    auto a { map };

    nlc_check(a.size() == 0);

    a.insert(1, 1);
    a.insert(2, 2);
    a.insert(3, 3);

    auto b = a;

    nlc_check(b.size() == 3);
    nlc_check(b.get(1) == 1);
    nlc_check(b.get(2) == 2);
    nlc_check(b.get(3) == 3);
}

nlc_test("reserve with existing elements") {
    standard_allocator malloc;
    hashmap<u32, u32> map { malloc };

    map.insert(0, 0);
    nlc_check(map.size() == 1);
    nlc_check(map.capacity() == impl::ht::minimal_capacity);
    map.insert(1, 1);
    map.insert(2, 2);
    map.insert(3, 3);

    map.reserve(65);
    nlc_check(map.size() == 4);
    nlc_check(map.capacity() == 128);
}

nlc_test("remove") {
    standard_allocator malloc;
    hashmap<u32, u32> map { malloc };

    for (auto i : range(0u, 16u)) {
        map.insert(i, i);
    }

    for (auto i : range(0u, 16u)) {
        if (i % 3 == 0) {
            map.remove(i);
        }
    }
    nlc_check(map.size() == 10);

    for (auto const & [k, v] : map) {
        nlc_check(k == v);
        nlc_check(k % 3 != 0);
    }

    for (auto i : range(0u, 16u)) {
        if (i % 3 == 0) {
            nlc_check(!map.contains(i));
        } else {
            nlc_check(map.get(i) == i);
        }
    }
}

nlc_test("reverse removes") {
    standard_allocator malloc;
    hashmap<u32, u32> map { malloc };

    for (auto i : range(0u, 16u)) {
        map.insert(i, i);
    }

    for (auto i : reverse_range(16u, 0u)) {
        map.remove(i - 1);
        nlc_check(!map.contains(i - 1));
        for (auto j : range(0, i - 1)) {
            nlc_check(map.get(j) == j);
        }
    }

    nlc_check(map.is_empty());
}

nlc_test("multiple removes on same metadata") {
    standard_allocator malloc;
    hashmap<u32, u32> map { malloc };

    for (auto i : range(0u, 16u)) {
        map.insert(i, i);
    }

    map.remove(7);
    map.remove(15);
    map.remove(14);
    map.remove(13);
    nlc_check(!map.contains(7));
    nlc_check(!map.contains(15));
    nlc_check(!map.contains(14));
    nlc_check(!map.contains(13));

    for (auto i : range(0u, 13u)) {
        if (i == 7) {
            nlc_check(!map.contains(i));
        } else {
            nlc_check(map.get(i) == i);
        }
    }

    map.insert(15, 15);
    map.insert(13, 13);
    map.insert(14, 14);
    map.insert(7, 7);
    for (auto i : range(0u, 16u)) {
        nlc_check(map.get(i) == i);
    }
}

nlc_test("insert and remove loop in random order") {
    standard_allocator malloc;
    hashmap<u32, u32> map { malloc };

    vector<u32> keys { malloc };

    auto constexpr size = 32u;
    auto constexpr iterations = 100u;

    for (auto i : range(0u, size)) {
        keys.append(i);
    }

    auto rng = nlc::xoroshiro128plus { 123456789 };

    for ([[maybe_unused]] auto i : range(0u, iterations)) {
        shuffle(rng, make_span(keys));

        for (auto const & key : keys) {
            map.insert(key, key);
        }
        nlc_check(map.size() == size);

        for (auto const & key : keys) {
            map.remove(key);
        }
        nlc_check(map.is_empty());
    }
}

nlc_test("remove 200k elements in random order") {
    auto constexpr n = 200u * 1000u;

    standard_allocator malloc;
    hashmap<u32, u32> map { malloc };

    vector<u32> keys { malloc };
    keys.reserve(n);

    for (auto i : range(0u, n)) {
        keys.append(i);
    }

    auto rng = nlc::xoroshiro128plus { 123456789 };
    shuffle(rng, make_span(keys));

    for (auto const & key : keys) {
        map.insert(key, key);
    }

    shuffle(rng, make_span(keys));
    for (auto i : range(0u, n)) {
        auto const key = keys[i];
        map.remove(key);
    }

    nlc_check(map.is_empty());
}

nlc_test("update") {
    standard_allocator malloc;
    hashmap<u32, u32> map { malloc };

    for (auto i : range(0u, 16u)) {
        map.insert(i, i);
    }

    for (auto i : range(0u, 16u)) {
        nlc_check(map.get(i) == i);
    }

    for (auto i : range(0u, 16u)) {
        map.update(i, i * 16 + 1);
    }

    for (auto i : range(0u, 16u)) {
        nlc_check(map.get(i) == i * 16 + 1);
    }
}

nlc_test("swap") {
    standard_allocator malloc;
    hashmap<i32, i32> map1 { malloc };
    hashmap<i32, i32> map2 { malloc };

    for (auto i : range(0, 16)) {
        map1.insert(i, i);
        map2.insert(i, -i);
    }

    for (auto i : range(0, 16)) {
        nlc_check(map1.get(i) == i);
        nlc_check(map2.get(i) == -i);
    }

    map1.swap(map2);

    for (auto i : range(0, 16)) {
        nlc_check(map1.get(i) == -i);
        nlc_check(map2.get(i) == i);
    }
}
