/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/containers/ring_buffer.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/range.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/test.hpp>

#include "impl/int_pair.hpp"

namespace {
struct Movable final {
    Movable() { ++default_construct_count; }
    Movable(Movable const &) = default;
    Movable(Movable && other) { *this = nlc_move(other); }
    ~Movable() { ++destruct_count; }

    Movable & operator=(Movable const &) = default;
    Movable & operator=(Movable && other) {
        nlc_check(other.is_valid);
        is_valid = true;
        other.is_valid = false;
        ++move_count;
        return *this;
    }

    static void reset_counters() {
        default_construct_count = 0u;
        move_count = 0u;
        destruct_count = 0u;
    }

    bool is_valid { true };

    static inline usize default_construct_count = 0u;
    static inline usize move_count = 0u;
    static inline usize destruct_count = 0u;
};
}  // namespace

static_assert(nlc::ring_buffer<int, 16u>::capacity() == 16u);
static_assert(nlc::ring_buffer<int, 13u>::capacity() == 16u);
static_assert(nlc::ring_buffer<int, 37u>::capacity() == 64u);

nlc_test("Simple use") {
    nlc::ring_buffer<int, 8u> buffer;

    nlc_check(buffer.size() == 0u);
    nlc_check(buffer.capacity() == 8u);
    nlc_check(buffer.is_empty());
    nlc_check(buffer.is_full() == false);
    nlc_check(buffer.begin() == buffer.end());
    nlc_expect_assert(buffer.front());
    nlc_expect_assert(buffer.back());

    buffer.push(2);
    buffer.push(4);
    buffer.push(-3);

    nlc_check(buffer.size() == 3u);
    nlc_check(buffer.is_empty() == false);
    nlc_check(buffer.is_full() == false);
    nlc_check(buffer.begin() != buffer.end());
    nlc_check(buffer.front() == 2);
    nlc_check(buffer.back() == -3);

    for (auto i = 0; i < 5; ++i) {
        nlc_check(buffer.size() == static_cast<usize>(3 + i));
        buffer.push(i);
        nlc_check(buffer.size() == static_cast<usize>(3 + i + 1));
        nlc_check(buffer.back() == i);
    }

    nlc_check(buffer.size() == 8u);

    for (auto i = 0u; i < 5u; ++i) {
        buffer.pop();
        nlc_check(buffer.back() == 4);
    }

    nlc_check(buffer.size() == 3u);

    buffer.push();

    nlc_check(buffer.size() == 4u);
}

nlc_test("Multiple rounds") {
    nlc::ring_buffer<int, 128u, u8> buffer;

    for (auto j = 0u; j < 15u; ++j) {
        // Fill buffer
        for (auto i = 0; i < 100; ++i) {
            buffer.push(i);
        }

        nlc_check(buffer.is_empty() == false);
        nlc_check(buffer.size() == 100u);
        nlc_check(buffer.front() == 0);
        nlc_check(buffer.back() == 99);

        // Clear buffer
        for (auto i = 0u; i < 100u; ++i) {
            buffer.pop();
        }

        nlc_check(buffer.is_empty());
        nlc_check(buffer.begin() == buffer.end());
    }

    // Fill buffer
    for (auto i = 0; i < 100; ++i) {
        buffer.push(i);
    }
}

nlc_test("Movable objet is not copied when poped") {
    Movable::reset_counters();
    nlc::ring_buffer<Movable, 10u> buffer;
    buffer.push();
    auto object = buffer.pop();

    nlc_check(object.is_valid);
    nlc_check(Movable::default_construct_count == 1u);
    nlc_check(Movable::move_count != 0u);
    nlc_check(Movable::destruct_count != 0u);
}

nlc_test("Movable object is correctly destroyed when poped") {
    Movable::reset_counters();
    {
        nlc::ring_buffer<Movable, 10u> buffer;
        buffer.push();
        buffer.pop();
    }

    nlc_check(Movable::default_construct_count == 1u);
    nlc_check(Movable::move_count != 0u);
    nlc_check(Movable::destruct_count != 0u);
}

nlc_test("Iteration") {
    nlc::ring_buffer<int, 8u> buffer;

    for (auto i = 0u; i < 8u; ++i)
        buffer.push(-static_cast<int>(i));

    // Iterator-based iteration
    int i = 0;
    for (auto it = buffer.begin(); it != buffer.end(); ++it)
        nlc_check(*it == i--);

    // Range-based const iteration
    i = 0;
    for (auto const & v : buffer)
        nlc_check(v == i--);

    // Range-based non-const iteration
    i = 0;
    for (auto & v : buffer) {
        --v;
        nlc_check(v + 1 == i--);
    }
}

nlc_test("create_and_push") {
    auto rb1 = nlc::ring_buffer<i32, 16u>::create_and_push(13);
    nlc_check(rb1.size() == 1);
    nlc_check(rb1.front() == 13);

    auto rb2 = nlc::ring_buffer<int_pair, 16u>::create_and_push(16, 32);
    nlc_check(rb2.size() == 1);
    nlc_check(rb2.front() == int_pair { 16, 32 });
}

nlc_test("create_and_push_many") {
    constexpr i32 expected_values1[] = { 13, 10, 12 };
    auto rb1 = nlc::ring_buffer<i32, 16u>::create_and_push_many(13, 10, 12);
    nlc_check(rb1.size() == 3);
    for (auto i = 0u; auto const v : rb1)
        nlc_check(v == expected_values1[i++]);

    constexpr int_pair expected_values2[] = { int_pair { 16, 32 }, int_pair { 12, -5 } };
    auto rb2 = nlc::ring_buffer<int_pair, 16u>::create_and_push_many(int_pair { 16, 32 },
                                                                     int_pair { 12, -5 });
    nlc_check(rb2.size() == 2);
    for (auto i = 0u; auto const v : rb2) {
        nlc_check(v.a == expected_values2[i].a && v.b == expected_values2[i].b);
        ++i;
    }
}

nlc_test("create_and_fill") {
    auto rb1 = nlc::ring_buffer<i32, 16u>::create_and_fill(13, 5);
    nlc_check(rb1.size() == 5);
    for (auto const v : rb1)
        nlc_check(v == 13);

    static int_pair const pair { 16, 32 };
    auto rb2 = nlc::ring_buffer<int_pair, 16u>::create_and_fill(pair, 4);
    nlc_check(rb2.size() == 4);
    for (auto const v : rb2)
        nlc_check(v == pair);
}

nlc_test("get_spans - single span") {
    auto const rb = nlc::ring_buffer<i32, 16u>::create_and_fill(13, 5);
    auto const spans { rb.get_spans() };
    nlc_check_msg(spans.first.size() == 5,
                  "expected 5 elements in the first span, got ",
                  spans.first.size());
    nlc_check_msg(spans.second.is_empty(),
                  "expected second span to be empty, it contains ",
                  spans.second.size(),
                  " elements");
    for (auto const v : rb)
        nlc_check_msg(v == 13, "expected 13, got ", v);
}

nlc_test("get_spans - two spans") {
    auto rb = nlc::ring_buffer<usize, 4u>::create_and_fill(5, 3);
    // Ensure the first element is not at the beginning of the buffer
    while (rb.is_empty() == false)
        rb.pop();

    // Insert elements to create a loop
    for (auto i : nlc::range(0u, 3u))
        rb.push(i);

    auto const spans { rb.get_spans() };
    nlc_check_msg(spans.first.size() == 1,
                  "expected 2 elements in the first span, got ",
                  spans.first.size());
    nlc_check_msg(spans.second.size() == 2,
                  "expected 2 elements in the second span, got ",
                  spans.second.size());

    for (usize i = 0u; auto const v : spans.first) {
        nlc_check_msg(v == i, "expected ", i, ", got ", v);
        ++i;
    }

    for (usize i = spans.first.size(); auto const v : spans.second) {
        nlc_check_msg(v == i, "expected ", i, ", got ", v);
        ++i;
    }
}

nlc_test("get_spans - two spans, full buffer") {
    auto rb = nlc::ring_buffer<usize, 4u>::create_and_fill(5, 2);
    // Ensure the first element is not at the beginning of the buffer
    while (rb.is_empty() == false)
        rb.pop();

    // Fill the buffer
    for (auto i : nlc::range(0u, rb.capacity()))
        rb.push(i);

    auto const spans { rb.get_spans() };
    nlc_check_msg(spans.first.size() == 2,
                  "expected 2 elements in the first span, got ",
                  spans.first.size());
    nlc_check_msg(spans.second.size() == 2,
                  "expected 2 elements in the second span, got ",
                  spans.second.size());

    for (usize i = 0u; auto const v : spans.first) {
        nlc_check_msg(v == i, "expected ", i, ", got ", v);
        ++i;
    }

    for (usize i = spans.first.size(); auto const v : spans.second) {
        nlc_check_msg(v == i, "expected ", i, ", got ", v);
        ++i;
    }
}
