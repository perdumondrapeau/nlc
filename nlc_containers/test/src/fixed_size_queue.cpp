/* Copyright © 2024-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/containers/fixed_size_queue.hpp>
#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/test.hpp>

using namespace nlc;

namespace {
struct complex_struct final {
    explicit complex_struct(float x)
        : a(static_cast<int>(x))
        , b(x)
        , c(x < 0.0f) {}

    int a;
    float b;
    bool c;
};
}  // namespace

static standard_allocator global_allocator;

nlc_test("fixed_size_queue init") {
    fixed_size_queue<bool> bool_queue(global_allocator, 13);
    fixed_size_queue<complex_struct> complex_queue(global_allocator, 999);
}

nlc_test("fixed_size_queue bool") {
    fixed_size_queue<bool> local_queue(global_allocator, 8);

    nlc_check(local_queue.size() == 0);
    nlc_check(local_queue.is_empty());
    nlc_check(local_queue.is_full() == false);

    local_queue.push(true);
    local_queue.push(false);

    nlc_check(local_queue.size() == 2);

    nlc_check(local_queue.pop() == true);
    nlc_check(local_queue.pop() == false);

    nlc_check(local_queue.size() == 0);
    nlc_check(local_queue.is_empty());
    nlc_check(local_queue.is_full() == false);
}

nlc_test("fixed_size_queue complex_struct") {
    fixed_size_queue<complex_struct> local_queue(global_allocator, 8);

    nlc_check(local_queue.size() == 0);
    nlc_check(local_queue.is_empty());
    nlc_check(local_queue.is_full() == false);

    local_queue.push(0.0f);
    local_queue.push(1.3f);

    nlc_check(local_queue.size() == 2);
    nlc_check(local_queue.is_empty() == false);

    auto value = local_queue.pop();
    nlc_check(value.a == 0 && value.c == false);
    value = local_queue.pop();
    nlc_check(value.a == 1 && value.c == false);

    nlc_check(local_queue.size() == 0);
    nlc_check(local_queue.is_empty());
    nlc_check(local_queue.is_full() == false);
}

nlc_test("fixed_size_queue int fill 2 times") {
    fixed_size_queue<int> local_queue(global_allocator, 4);

    nlc_check(local_queue.size() == 0);
    nlc_check(local_queue.is_empty());
    nlc_check(local_queue.is_full() == false);

    local_queue.push(13);
    local_queue.push(14);
    local_queue.push(15);
    nlc_check(local_queue.is_full() == false);
    local_queue.push(16);
    nlc_check(local_queue.is_full());

    nlc_check(local_queue.front() == 13);
    nlc_check(local_queue.back() == 16);

    nlc_check(local_queue.pop() == 13);
    nlc_check(local_queue.pop() == 14);
    nlc_check(local_queue.pop() == 15);
    nlc_check(local_queue.is_empty() == false);
    nlc_check(local_queue.pop() == 16);
    nlc_check(local_queue.is_empty());

    local_queue.push(23);
    local_queue.push(24);
    local_queue.push(25);
    nlc_check(local_queue.is_full() == false);
    local_queue.push(26);
    nlc_check(local_queue.is_full());

    nlc_check(local_queue.front() == 23);
    nlc_check(local_queue.back() == 26);

    nlc_check(local_queue.pop() == 23);
    nlc_check(local_queue.pop() == 24);
    nlc_check(local_queue.pop() == 25);
    nlc_check(local_queue.is_empty() == false);
    nlc_check(local_queue.pop() == 26);
    nlc_check(local_queue.is_empty());

    nlc_check(local_queue.size() == 0);
    nlc_check(local_queue.is_empty());
}
