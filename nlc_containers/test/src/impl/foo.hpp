/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

struct Foo final {
    static inline isize _instances = 0;

    Foo(Foo const & other)
        : _a { other._a } {
        ++_instances;
    }
    Foo(Foo && other)
        : _a { other._a } {
        ++_instances;
    }
    Foo(u32 const a)
        : _a { a } {
        ++_instances;
    }
    // https://stackoverflow.com/questions/52263141/maybe-unused-and-constructors
    Foo(bool b [[maybe_unused]], [[maybe_unused]] u32 i, [[maybe_unused]] float f)
        : Foo(i) {}
    ~Foo() { --_instances; }

    auto operator=(Foo const & other) { _a = other._a; }
    auto operator=(Foo && other) { _a = other._a; }

    auto operator==(Foo const & other) const { return _a == other._a; }
    auto operator==(u32 const other) const { return _a == other; }

    u32 _a;
};
