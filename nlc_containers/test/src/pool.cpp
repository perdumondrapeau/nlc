/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/containers/pool.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/meta/id.hpp>
#include <nlc/test.hpp>

struct index_desc final {
    using underlying_type = u32;
    static constexpr auto invalid_value = static_cast<underlying_type>(-1);
};

using test_id = nlc::meta::id<index_desc>;

struct Miou final {
    int value;
};

nlc_test("Simple use") {
    nlc::standard_allocator allocator;
    nlc::pool<Miou, test_id> pool { allocator, 4 };

    auto elem_a = pool.create(1);
    auto elem_b = pool.create(2);
    auto elem_c = pool.create(3);
    auto elem_d = pool.create(4);
    auto elem_e = pool.create(5);
    auto elem_f = pool.create(6);

    nlc_check(pool[elem_a].value == 1);
    nlc_check(pool[elem_b].value == 2);
    nlc_check(pool[elem_c].value == 3);
    nlc_check(pool[elem_d].value == 4);
    nlc_check(pool[elem_e].value == 5);
    nlc_check(pool[elem_f].value == 6);

    pool.destroy(elem_b);
    pool.destroy(elem_c);
    pool.destroy(elem_e);

    nlc_check(pool[elem_f].value == 6);

    auto elem_g = pool.create(7);

    nlc_check(pool[elem_g].value == 7);

    pool.destroy(elem_a);
    pool.destroy(elem_d);
    pool.destroy(elem_f);
    pool.destroy(elem_g);
}
