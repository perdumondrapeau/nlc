/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/algo/shuffle.hpp>
#include <nlc/containers/hashset.hpp>
#include <nlc/containers/vector.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/range.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/random.hpp>
#include <nlc/meta/traits.hpp>
#include <nlc/test.hpp>

#include "impl/int_pair.hpp"

using namespace nlc;

static_assert(nlc::meta::is_polymorphic<nlc::hashset<u32>> == false);
#if defined(NDEBUG)
static_assert(sizeof(nlc::hashset<u32>) == 24);
#else
static_assert(sizeof(nlc::hashset<u32>) == 32);
#endif

nlc_test("basic_usage") {
    standard_allocator malloc;
    hashset<i32> h(malloc);
    h.insert(1);
    nlc_check(h.contains(1));
    h.insert(3);
    h.insert(5);
    h.insert(7);
    h.try_insert(9);
    nlc_check(h.size() == 5u);
    h.remove(5);
    h.try_remove(1);
    nlc_check(h.size() == 3u);
    h.clear();

    // Move-assign to an empty set
    h = decltype(h) { malloc };
    nlc_check(h.size() == 0u);

    // iterators
    h.insert(3);
    h.insert(5);
    for (auto it = h.begin(); it != h.end(); ++it) {
        nlc_check(*it == 3 || *it == 5);
    }

    for (auto it = h.begin(); it != h.end(); it++) {
        nlc_check(*it == 3 || *it == 5);
    }

    auto const iterators = malloc.allocate<decltype(h.begin())>(h.size());
    defer { malloc.free(iterators); };

    auto it = h.begin();
    for (auto i = 0u; i < h.size(); ++i) {
        iterators[i] = it++;
        nlc_check(*iterators[i] == 3 || *iterators[i] == 5);
    }
}

nlc_test("create_and_reserve") {
    standard_allocator malloc;
    auto const set = hashset<i32>::create_and_reserve(malloc, 17u);
    nlc_check(set.capacity() >= 17u);
    nlc_check(set.is_empty());
}

nlc_test("create_and_insert") {
    standard_allocator malloc;
    auto const set1 = hashset<i32>::create_and_insert(malloc, 13);
    nlc_check(set1.contains(13));

    auto const set2 = hashset<int_pair>::create_and_insert(malloc, int_pair { 16, 32 });
    nlc_check(set2.contains(int_pair { 16, 32 }));
}

nlc_test("reserve") {
    standard_allocator malloc;
    hashset<i32> set { malloc };

    set.reserve(20);
    auto const initial_capacity = set.capacity();
    nlc_check(initial_capacity >= 20);
    for (i32 i : range(0, 20))
        set.insert(i);
    nlc_check(initial_capacity == set.capacity());
}

nlc_test("grow") {
    standard_allocator malloc;
    hashset<u32> set { malloc };

    constexpr u32 size = 12456;

    for (u32 i : range(0u, size)) {
        set.insert(i);
    }
    nlc_check(set.size() == size);
}

nlc_test("copy") {
    standard_allocator malloc;
    hashset<u32> set { malloc };

    auto a { set };

    nlc_check(a.size() == 0);

    a.insert(1);
    a.insert(2);
    a.insert(3);

    auto b = a;

    nlc_check(b.size() == 3);
    nlc_check(b.contains(1));
    nlc_check(b.contains(2));
    nlc_check(b.contains(3));
}

nlc_test("reserve with existing elements") {
    standard_allocator malloc;
    hashset<u32> set { malloc };

    set.insert(0);
    nlc_check(set.size() == 1);
    nlc_check(set.capacity() == impl::ht::minimal_capacity);
    set.insert(1);
    set.insert(2);
    set.insert(3);

    set.reserve(65);
    nlc_check(set.size() == 4);
    nlc_check(set.capacity() == 128);
}

nlc_test("remove") {
    standard_allocator malloc;
    hashset<u32> set { malloc };

    for (auto i : range(0u, 16u)) {
        set.insert(i);
    }

    for (auto i : range(0u, 16u)) {
        if (i % 3 == 0) {
            set.remove(i);
        }
    }
    nlc_check(set.size() == 10);

    for (auto const & k : set) {
        nlc_check(k % 3 != 0);
    }

    for (auto i : range(0u, 16u)) {
        bool const mod = i % 3 == 0;
        nlc_check(set.contains(i) != mod);
    }
}

nlc_test("reverse removes") {
    standard_allocator malloc;
    hashset<u32> set { malloc };

    for (auto i : range(0u, 16u)) {
        set.insert(i);
    }

    for (auto i : reverse_range(16u, 0u)) {
        set.remove(i - 1);
        nlc_check(!set.contains(i - 1));
        for (auto j : range(0, i - 1)) {
            nlc_check(set.contains(j));
        }
    }

    nlc_check(set.is_empty());
}

nlc_test("multiple removes on same metadata") {
    standard_allocator malloc;
    hashset<u32> set { malloc };

    for (auto i : range(0u, 16u)) {
        set.insert(i);
    }

    set.remove(7);
    set.remove(15);
    set.remove(14);
    set.remove(13);
    nlc_check(!set.contains(7));
    nlc_check(!set.contains(15));
    nlc_check(!set.contains(14));
    nlc_check(!set.contains(13));

    for (auto i : range(0u, 13u)) {
        nlc_check(set.contains(i) == (i != 7));
    }

    set.insert(15);
    set.insert(13);
    set.insert(14);
    set.insert(7);
    for (auto i : range(0u, 16u)) {
        nlc_check(set.contains(i));
    }
}

nlc_test("insert and remove loop in random order") {
    standard_allocator malloc;
    hashset<u32> set { malloc };

    vector<u32> keys { malloc };

    auto constexpr size = 32u;
    auto constexpr iterations = 100u;

    for (auto i : range(0u, size)) {
        keys.append(i);
    }

    auto rng = nlc::xoroshiro128plus { 123456789 };

    for ([[maybe_unused]] auto i : range(0u, iterations)) {
        shuffle(rng, make_span(keys));

        for (auto const & key : keys) {
            set.insert(key);
        }
        nlc_check(set.size() == size);

        for (auto const & key : keys) {
            set.remove(key);
        }
        nlc_check(set.is_empty());
    }
}

nlc_test("remove 200k elements in random order") {
    auto constexpr n = 200u * 1000u;

    standard_allocator malloc;
    hashset<u32> set { malloc };

    vector<u32> keys { malloc };
    keys.reserve(n);

    for (auto i : range(0u, n)) {
        keys.append(i);
    }

    auto rng = nlc::xoroshiro128plus { 123456789 };
    shuffle(rng, make_span(keys));

    for (auto const & key : keys) {
        set.insert(key);
    }

    shuffle(rng, make_span(keys));
    for (auto i : range(0u, n)) {
        auto const key = keys[i];
        set.remove(key);
    }

    nlc_check(set.is_empty());
}

nlc_test("swap") {
    standard_allocator malloc;
    hashset<i32> set1 { malloc };
    hashset<i32> set2 { malloc };

    for (auto i : range(0, 16)) {
        set1.insert(i);
        set2.insert(-i);
    }

    for (auto i : range(0, 16)) {
        nlc_check(set1.contains(i));
        nlc_check(set2.contains(-i));
    }

    set1.swap(set2);

    for (auto i : range(0, 16)) {
        nlc_check(set1.contains(-i));
        nlc_check(set2.contains(i));
    }
}
