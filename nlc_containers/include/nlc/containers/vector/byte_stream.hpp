/* Copyright © 2025-2025
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/fundamentals/byte_stream.hpp>

#include "../vector.hpp"

namespace nlc {
template<typename T, typename _index_type>
auto to_byte_stream(nlc::output_byte_stream & out, vector<T, _index_type> const & v) -> void {
    out.write(static_cast<u32>(v.size()));

    if constexpr (is_bulk_byte_streamable<T>)
        out.write(span<byte const> { reinterpret_cast<byte const *>(v.data()), v.size() * sizeof(T) });
    else {
        static_assert(output_byte_streamable<T>,
                      "You should either define is_bulk_byte_streamable to true if it is adequate, "
                      "or implement to_byte_stream for your type");
        for (auto const & elem : v)
            out.write(elem);
    }
}

template<typename T, typename _index_type>
auto from_byte_stream(nlc::input_byte_stream & in, vector<T, _index_type> & v) -> void {
    v.clear();

    auto const new_size = in.read<u32>();
    v.resize(new_size);

    if constexpr (is_bulk_byte_streamable<T>)
        in.read(span { reinterpret_cast<byte *>(v.data()), v.size() * sizeof(T) });
    else {
        static_assert(input_byte_streamable<T>,
                      "You should either define is_bulk_byte_streamable to true if it is adequate, "
                      "or implement from_byte_stream for your type");
        for (auto & elem : v)
            in.read(elem);
    }
}
}  // namespace nlc
