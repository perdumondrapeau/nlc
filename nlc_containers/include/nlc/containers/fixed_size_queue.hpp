/* Copyright © 2024-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/new.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/align.hpp>
#include <nlc/fundamentals/allocator.hpp>

namespace nlc {

template<typename T> class fixed_size_queue final {
    allocator * _allocator;
    T * _data;
    u32 _allocated_capacity;
    u32 _capacity;
    u32 _front_index;
    u32 _back_index;

    [[nodiscard]] constexpr auto _mask(u32 const index) const -> u32 {
        return index & (_capacity - 1u);
    }

    auto _destroy_objects() -> void {
        for (u32 i = _front_index; i != _back_index; ++i)
            _data[_mask(i)].~T();
    }

  public:
    fixed_size_queue(allocator & alloc, usize const capacity) {
        _allocator = &alloc;
        _capacity = static_cast<u32>(ceil_power_of_two(capacity));
        auto const mem = _allocator->allocate<T>(_capacity);
        _data = mem.ptr();
        _allocated_capacity = static_cast<u32>(mem.size());
        _front_index = 0;
        _back_index = 0;
    }

    ~fixed_size_queue() {
        _destroy_objects();
        _allocator->free<T>({ _data, _allocated_capacity });
    }

    fixed_size_queue(fixed_size_queue const &) = delete;
    fixed_size_queue & operator=(fixed_size_queue const &) = delete;

    fixed_size_queue(fixed_size_queue && other) {
        _allocator = other._allocator;
        _data = other._data;
        _allocated_capacity = other._allocated_capacity;
        _capacity = other._capacity;
        _front_index = other._front_index;
        _back_index = other._back_index;

        other._data = nullptr;
        other._allocated_capacity = 0;
        other._capacity = 0;
        other._front_index = 0;
        other._back_index = 0;
    }

    fixed_size_queue & operator=(fixed_size_queue && other) {
        nlc_assert(this != &other);
        nlc_assert(_allocator == other._allocator);

        _destroy_objects();

        _data = nlc_move(other._data);
        _allocated_capacity = other._allocated_capacity;
        _capacity = other._capacity;
        _front_index = other._front_index;
        _back_index = other._back_index;

        other._data = nullptr;
        other._capacity = 0;
        other._front_index = 0;
        other._back_index = 0;

        return *this;
    }

    [[nodiscard]] auto size() const -> usize { return _back_index - _front_index; }
    [[nodiscard]] auto capacity() const -> usize { return _capacity; }
    [[nodiscard]] auto is_empty() const -> bool { return _front_index == _back_index; }
    [[nodiscard]] auto is_full() const -> bool { return size() == _capacity; }

    template<typename... Args> auto push(Args &&... args) -> T & {
        nlc_assert(is_full() == false);
        T & item = _data[_mask(_back_index++)];
        new (&item) T(nlc_fwd(args)...);
        return item;
    }

    auto pop() -> T {
        nlc_assert(is_empty() == false);
        auto const loc = &_data[_mask(_front_index++)];
        T item = nlc_move(*loc);
        loc->~T();
        return item;
    }

    [[nodiscard]] auto front() const -> T const & {
        nlc_assert(is_empty() == false);
        return _data[_mask(_front_index)];
    }
    [[nodiscard]] auto front() -> T & {
        nlc_assert(is_empty() == false);
        return _data[_mask(_front_index)];
    }

    [[nodiscard]] auto back() const -> T const & {
        nlc_assert(is_empty() == false);
        return _data[_mask(_back_index - 1)];
    }
    [[nodiscard]] auto back() -> T & {
        nlc_assert(is_empty() == false);
        return _data[_mask(_back_index - 1)];
    }
};

}  // namespace nlc
