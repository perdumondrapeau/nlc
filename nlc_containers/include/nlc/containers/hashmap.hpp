/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/pair.hpp>
#include <nlc/dialect/types.hpp>
#include <nlc/fundamentals/align.hpp>
#include <nlc/meta/basics.hpp>

#include "impl/hashtable.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace nlc {
template<typename key_type, typename value_type, u64 max_load_percentage = impl::ht::default_max_load_percentage>
class hashmap final : public hashtable<key_type, pair<key_type, value_type>, max_load_percentage> {
    using parent = hashtable<key_type, pair<key_type, value_type>, max_load_percentage>;

  public:
    using entry_type = pair<key_type, value_type>;

  public:
    hashmap(allocator & alloc)
        : parent { alloc } {}

    ~hashmap() = default;

    hashmap(hashmap const &) = default;
    hashmap(hashmap &&) = default;

    [[nodiscard]] static auto create_and_reserve(allocator & alloc, usize const size) -> hashmap {
        hashmap res { alloc };
        res.reserve(size);
        return res;
    }

    template<typename... Args>
    [[nodiscard]] static auto create_and_emplace(allocator & alloc,
                                                 key_type const & key,
                                                 Args &&... args) -> hashmap {
        hashmap res { alloc };
        res.emplace(key, nlc_fwd(args)...);
        return res;
    }

    auto operator=(hashmap const & other) -> hashmap & {
        parent::operator=(other);

        return *this;
    }

    auto operator=(hashmap && other) -> hashmap & {
        parent::operator=(nlc_move(other));
        return *this;
    }

    auto insert(key_type const & k, value_type const & v) -> value_type & {
        this->grow_if_needed(1);
        return insert_no_grow(k, v);
    }

    auto insert(key_type const & k, value_type && v) -> value_type & {
        this->grow_if_needed(1);
        return insert_no_grow(k, nlc_move(v));
    }

    auto insert(key_type && k, value_type const & v) -> value_type & {
        nlc_assert(this->contains(k) == false);
        this->grow_if_needed(1);
        auto const ptr = this->insert_uninitialized(k);
        new (ptr) entry_type { nlc_move(k), v };
        return ptr->second;
    }

    auto insert(key_type && k, value_type && v) -> value_type & {
        nlc_assert(this->contains(k) == false);
        this->grow_if_needed(1);
        auto const ptr = this->insert_uninitialized(k);
        new (ptr) entry_type { nlc_move(k), nlc_move(v) };
        return ptr->second;
    }

    auto try_insert(key_type const & k, value_type const & v) -> bool {
        this->grow_if_needed(1);
        auto const ptr = this->try_insert_uninitialized(k);

        if (nullptr == ptr)
            return false;

        new (ptr) entry_type { k, v };
        return true;
    }

    auto try_insert(key_type const & k, value_type && v) -> bool {
        this->grow_if_needed(1);
        auto const ptr = this->try_insert_uninitialized(k);

        if (nullptr == ptr)
            return false;

        new (ptr) entry_type { k, nlc_move(v) };
        return true;
    }

    auto try_insert(key_type && k, value_type const & v) -> bool {
        this->grow_if_needed(1);
        auto const ptr = this->try_insert_uninitialized(k);

        if (nullptr == ptr)
            return false;

        new (ptr) entry_type { nlc_move(k), v };
        return true;
    }

    auto try_insert(key_type && k, value_type && v) -> bool {
        this->grow_if_needed(1);
        auto const ptr = this->try_insert_uninitialized(k);

        if (nullptr == ptr)
            return false;

        new (ptr) entry_type { nlc_move(k), nlc_move(v) };
        return true;
    }

    template<typename... Args> auto emplace(key_type const & k, Args &&... args) -> value_type & {
        nlc_assert(!this->contains(k));
        this->grow_if_needed(1);
        auto const ptr = this->insert_uninitialized(k);
        new (ptr) entry_type { k, value_type { nlc_fwd(args)... } };
        return ptr->second;
    }

    template<typename... Args> auto try_emplace(key_type const & k, Args &&... args) -> bool {
        this->grow_if_needed(1);
        auto const ptr = this->try_insert_uninitialized(k);
        if (!ptr)
            return false;

        new (ptr) entry_type { k, value_type { nlc_fwd(args)... } };
        return true;
    }

    [[nodiscard]] auto extract(key_type const & k) -> value_type {
        nlc_assert(this->contains(k));

        auto const idx = this->get_entry_idx(k);
        nlc_assert(impl::ht::invalid_index != idx);

        this->_metadata[idx].remove();
        auto & entry = this->entries()[idx];
        value_type value { nlc_move(entry.second) };
        entry.~entry_type();
        --this->_size;

        this->invalidate_iterators();

        return value;
    }

    auto update(key_type const & k, value_type const & v) -> value_type & {
        auto & value = get_mut(k);
        value = v;

        this->invalidate_iterators();
        return value;
    }

    auto update(key_type const & k, value_type && v) -> value_type & {
        auto & value = get_mut(k);
        value = nlc_move(v);

        this->invalidate_iterators();
        return value;
    }

    auto try_update(key_type const & k, value_type const & v) -> bool {
        auto const res = try_get_mut(k);
        if (res.has_value()) {
            res.value() = v;

            this->invalidate_iterators();
        }

        return res.has_value();
    }

    auto try_update(key_type const & k, value_type && v) -> bool {
        auto const res = try_get_mut(k);
        if (res.has_value()) {
            res.value() = nlc_move(v);

            this->invalidate_iterators();
        }

        return res.has_value();
    }

    [[nodiscard]] auto get(key_type const & k) const -> value_type const & {
        nlc_assert(this->contains(k));

        auto const h = hash(k);
        auto const mask = this->_capacity() - 1;
        auto const fingerprint = parent::meta_data_type::make_fingerprint(h);

        u32 idx = static_cast<u32>(h.value()) & mask;
        auto * meta = &this->_metadata[idx];

        while (!meta->is_empty()) {
            if (meta->used && meta->fingerprint == fingerprint) {
                entry_type * const entry = this->entries() + idx;
                if (entry->first == k) {
                    return entry->second;
                }
            }

            idx = (idx + 1) & mask;
            meta = &this->_metadata[idx];
        }

        nlc_unreachable;
    }

    [[nodiscard]] auto get_mut(key_type const & k) -> value_type & {
        // Recommended implementation from Effective C++.
        return const_cast<value_type &>(const_cast<const hashmap *>(this)->get(k));
    }

    [[nodiscard]] auto try_get(key_type const & k) const -> impl::ht::try_result<value_type const> {
        return get_value_ptr(k);
    }

    [[nodiscard]] auto try_get_mut(key_type const & k) -> impl::ht::try_result<value_type> {
        // Recommended implementation from Effective C++.
        return const_cast<value_type *>(const_cast<const hashmap *>(this)->get_value_ptr(k));
    }

    template<typename... Args>
    [[nodiscard]] auto get_or_insert(key_type const & k, Args &&... args) -> value_type & {
        this->grow_if_needed(1);
        return get_or_insert_no_grow(k, nlc_fwd(args)...);
    }

    template<typename... Args>
    [[nodiscard]] auto get_or_insert_no_grow(key_type const & k, Args &&... args) -> value_type & {
        auto const h = hash(k);
        auto const mask = this->_capacity() - 1;
        auto const fingerprint = parent::meta_data_type::make_fingerprint(h);

        u32 idx = static_cast<u32>(h.value()) & mask;
        auto * meta = &this->_metadata[idx];

        while (meta->is_empty() == false) {
            if (meta->used && meta->fingerprint == fingerprint) {
                entry_type * const entry = this->entries() + idx;
                if (entry->first == k) {
                    return entry->second;
                }
            }

            idx = (idx + 1) & mask;
            meta = &this->_metadata[idx];
        }

        if (meta->is_tombstone() == false) {
            nlc_assert(this->_available > 0);
            --this->_available;
        }

        meta->fill(fingerprint);
        ++this->_size;

        auto const ptr = this->entries() + idx;
        new (ptr) entry_type { k, value_type { nlc_fwd(args)... } };

        return ptr->second;
    }

    auto insert_or_update(key_type const & k, value_type const & v) -> value_type & {
        // TODO avoid reassignment if this triggers an insert
        auto & value = get_or_insert(k, v);
        value = v;
        return value;
    }

    auto insert_or_update_no_grow(key_type const & k, value_type const & v) -> value_type & {
        // TODO avoid reassignment if this triggers an insert
        auto & value = get_or_insert_no_grow(k, v);
        value = v;
        return value;
    }

    auto insert_no_grow(key_type const & k, value_type const & v) -> value_type & {
        nlc_assert(this->contains(k) == false);
        nlc_assert(this->_available > 0);

        auto const ptr = this->insert_uninitialized(k);
        new (ptr) entry_type { k, v };
        return ptr->second;
    }

    auto insert_no_grow(key_type const & k, value_type && v) -> value_type & {
        nlc_assert(this->contains(k) == false);
        nlc_assert(this->_available > 0);

        auto const ptr = this->insert_uninitialized(k);
        new (ptr) entry_type { k, nlc_move(v) };
        return ptr->second;
    }

    [[nodiscard]] auto get_value_ptr(key_type const & k) const -> value_type * {
        if (this->_size == 0) {
            return nullptr;
        }

        auto const h = hash(k);
        auto const mask = this->_capacity() - 1;
        auto const fingerprint = parent::meta_data_type::make_fingerprint(h);

        u32 idx = static_cast<u32>(h.value()) & mask;
        auto * meta = &this->_metadata[idx];

        while (meta->is_empty() == false) {
            if (meta->used && meta->fingerprint == fingerprint) {
                entry_type * const entry = this->entries() + idx;
                if (entry->first == k) {
                    return &entry->second;
                }
            }

            idx = (idx + 1) & mask;
            meta = &this->_metadata[idx];
        }

        return nullptr;
    }
};
}  // namespace nlc

////////////////////////////////////////////////////////////////////////////////////////////////////
