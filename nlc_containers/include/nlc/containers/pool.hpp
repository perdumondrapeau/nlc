/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/allocator.hpp>

namespace nlc {

// _index_type must be a nlc::meta::id
template<typename T, typename _index_type> class pool final {
    static_assert(_index_type::descriptor_type::invalid_value != 0);
    using index_type = _index_type;

    allocator & _allocator;
    span<T> _data;
    index_type _first_free_index = index_type::invalid();
    u32 _size = 0;
#ifndef NDEBUG
    u32 _count = 0;
#endif

  public:
    // You can pass 0 as capacity, but then you must reserve a capacity before using the pool
    pool(allocator & a, usize const capacity)
        : _allocator(a) {
        if (capacity != 0)
            reserve(capacity);
    }

    ~pool() {
        nlc_assert(_count == 0);
        _allocator.free(_data);
    }

    template<typename... Args> auto create(Args &&... args) -> index_type {
        nlc_assert_msg(_data.ptr() != nullptr, "You forgot to reserve before using a nlc::pool");
#ifndef NDEBUG
        ++_count;
#endif
        auto const index = [this]() -> index_type {
            if (_first_free_index.is_valid()) {
                auto const free_index = _first_free_index;
                _first_free_index =
                    *reinterpret_cast<index_type *>(_data.ptr() + _first_free_index.value());
                return free_index;
            } else {
                if (_size < _data.size())
                    reserve(_data.size() + (_data.size() >> 1));
                return index_type { static_cast<typename index_type::underlying_type>(_size++) };
            }
        }();

        new (_data.ptr() + index.value()) T { forward<Args>(args)... };
        return index;
    }

    auto destroy(index_type const index) -> void {
#ifndef NDEBUG
        --_count;
#endif
        _data[index.value()].~T();
        *reinterpret_cast<index_type *>(_data.ptr() + index.value()) = _first_free_index;
        _first_free_index = index;
    }

    auto operator[](index_type const index) const -> T const & { return _data[index.value()]; }

    auto operator[](index_type const index) -> T & { return _data[index.value()]; }

    auto reserve(usize const capacity) {
        nlc_assert(capacity > _data.size());
        if (_data.ptr() == nullptr)
            _data = _allocator.allocate<T>(capacity < 4 ? 4 : capacity);
        else
            _data = _allocator.reallocate(_data, capacity);
    }
};

}  // namespace nlc
