/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/pair.hpp>
#include <nlc/dialect/types.hpp>
#include <nlc/fundamentals/align.hpp>
#include <nlc/meta/basics.hpp>

#include "impl/hashtable.hpp"

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace nlc {
template<typename key_type, u64 max_load_percentage = impl::ht::default_max_load_percentage>
class hashset final : public hashtable<key_type, key_type, max_load_percentage> {
    using parent = hashtable<key_type, key_type, max_load_percentage>;

  public:
    hashset(allocator & alloc)
        : parent { alloc } {}

    ~hashset() = default;

    hashset(hashset const &) = default;
    hashset(hashset &&) = default;

    [[nodiscard]] static auto create_and_reserve(allocator & alloc, usize const size) -> hashset {
        hashset res { alloc };
        res.reserve(size);
        return res;
    }

    [[nodiscard]] static auto create_and_insert(allocator & alloc, key_type const & k) -> hashset {
        hashset res { alloc };
        res.insert(k);
        return res;
    }

    auto operator=(hashset const & other) -> hashset & {
        parent::operator=(other);

        return *this;
    }

    auto operator=(hashset && other) -> hashset & {
        parent::operator=(nlc_move(other));
        return *this;
    }

    auto insert(key_type const & k) -> void {
        this->grow_if_needed(1);
        insert_no_grow(k);
    }

    auto insert(key_type && k) -> void {
        nlc_assert(!this->contains(k));
        this->grow_if_needed(1);
        auto const ptr = this->insert_uninitialized(k);
        new (ptr) key_type { nlc_move(k) };
    }

    auto try_insert(key_type const & k) -> bool {
        this->grow_if_needed(1);
        auto const ptr = this->try_insert_uninitialized(k);

        if (nullptr == ptr)
            return false;

        new (ptr) key_type { k };
        return true;
    }

    auto try_insert(key_type && k) -> bool {
        this->grow_if_needed(1);
        auto const ptr = this->try_insert_uninitialized(k);

        if (nullptr == ptr)
            return false;

        new (ptr) key_type { nlc_move(k) };
        return true;
    }

    auto insert_no_grow(key_type const & k) -> void {
        nlc_assert(!this->contains(k));
        nlc_assert(this->_available > 0);

        auto const ptr = this->insert_uninitialized(k);
        new (ptr) key_type { k };
    }
};
}  // namespace nlc

////////////////////////////////////////////////////////////////////////////////////////////////////
