/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/buffer.hpp>
#include <nlc/dialect/new.hpp>
#include <nlc/dialect/pair.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/align.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/logic.hpp>
#include <nlc/meta/numeric_limits.hpp>
#include <nlc/meta/tags.hpp>
#include <nlc/meta/traits.hpp>

namespace nlc {

// Ref: https://www.snellman.net/blog/archive/2016-12-13-ring-buffers/
template<typename T, usize _buffer_size, typename _index_type = usize> class ring_buffer final {
  public:
    using data_type = T;
    using index_type = meta::if_default<_index_type, usize>;
    static_assert(meta::is_unsigned_integer<index_type>);

  private:
    static inline constexpr usize buffer_size = ceil_power_of_two(_buffer_size);
    static_assert(buffer_size > 0u);
    // We must not use the top bit of the index_type
    // buffer_size - 1u -> maximum index
    static_assert(buffer_size - 1u <= meta::limits<index_type>::max >> 1u,
                  "the index type is not big enough for this buffer size");

    nlc::buffer<T, buffer_size> buffer;
    index_type read_index { 0u };
    index_type write_index { 0u };

    template<typename element_type, typename container_type> class iterator_impl final {
        friend class ring_buffer;

        container_type & container;
        index_type index;

        iterator_impl(container_type & _container, index_type _index)
            : container { _container }
            , index { _index } {}

      public:
        auto operator*() const -> element_type & { return container.buffer[container.mask(index)]; }
        auto operator++() -> iterator_impl & {
            ++index;
            return *this;
        }
        auto operator==(iterator_impl const & rhs) const { return index == rhs.index; }
        auto operator!=(iterator_impl const & rhs) const { return operator==(rhs) == false; }
    };

    [[nodiscard]] static constexpr auto mask(index_type i) -> index_type {
        return i & (buffer_size - 1u);
    }

  public:
    using iterator = iterator_impl<T, ring_buffer>;
    using const_iterator = iterator_impl<T const, ring_buffer const>;

    ring_buffer() = default;
    ~ring_buffer() { destroy_objects(); }

    ring_buffer(ring_buffer const &) = delete;
    ring_buffer & operator=(ring_buffer const &) = delete;

    ring_buffer(ring_buffer && other) {
        read_index = other.read_index;
        write_index = other.write_index;

        for (index_type i = read_index; i != write_index; ++i)
            buffer[mask(i)] = nlc_move(other.buffer[mask(i)]);

        other.destroy_objects();
        other.read_index = 0u;
        other.write_index = 0u;
    }

    ring_buffer & operator=(ring_buffer && other) {
        nlc_assert(this != &other);
        destroy_objects();
        read_index = other.read_index;
        write_index = other.write_index;

        for (index_type i = read_index; i != write_index; ++i)
            buffer[mask(i)] = nlc_move(other.buffer[mask(i)]);

        other.destroy_objects();
        other.read_index = 0u;
        other.write_index = 0u;

        return *this;
    }

    template<typename... Args>
    [[nodiscard]] static auto create_and_push(Args &&... args) -> ring_buffer {
        ring_buffer res;
        res.push(nlc_fwd(args)...);
        return res;
    }

    template<typename... Args>
    [[nodiscard]] static auto create_and_push_many(Args &&... elements) -> ring_buffer {
        ring_buffer res;
        (res.push(nlc_fwd(elements)), ...);
        return res;
    }

    [[nodiscard]] static auto create_and_fill(T const & element, usize const count) -> ring_buffer {
        ring_buffer res;
        nlc_assert_msg(count <= buffer_size, nlc_dump_var(count), nlc_dump_var(buffer_size));
        for (auto i = 0u; i < count; ++i)
            res.push(element);
        return res;
    }

    template<typename... Args> auto push(Args &&... args) -> T & {
        nlc_assert(is_full() == false);
        T & item = buffer[mask(write_index++)];
        new (&item) T(nlc_fwd(args)...);
        return item;
    }

    auto pop() -> T {
        nlc_assert(is_empty() == false);
        auto const loc = &buffer[mask(read_index++)];
        T elm = nlc_move(*loc);
        loc->~T();
        return elm;
    }

    [[nodiscard]] auto front() const -> T const & {
        return const_cast<ring_buffer *>(this)->front();
    }
    [[nodiscard]] auto front() -> T & {
        nlc_assert(is_empty() == false);
        return *begin();
    }

    [[nodiscard]] auto back() const -> T const & { return const_cast<ring_buffer *>(this)->back(); }
    [[nodiscard]] auto back() -> T & {
        nlc_assert(is_empty() == false);
        return buffer[mask(write_index - 1)];
    }

    [[nodiscard]] auto begin() -> iterator { return { *this, read_index }; }
    [[nodiscard]] auto begin() const -> const_iterator { return { *this, read_index }; }
    [[nodiscard]] auto end() -> iterator { return { *this, write_index }; }
    [[nodiscard]] auto end() const -> const_iterator { return { *this, write_index }; }

    [[nodiscard]] auto size() const -> usize {
        // Cast to index_type to ensure overflow wrapping
        return static_cast<usize>(static_cast<index_type>(write_index - read_index));
    }
    [[nodiscard]] static constexpr auto capacity() -> usize { return buffer_size; }

    [[nodiscard]] auto is_empty() const -> bool { return read_index == write_index; }
    [[nodiscard]] auto is_full() const -> bool { return size() == capacity(); }

    [[nodiscard]] auto get_spans() const -> pair<span<T const>, span<T const>> {
        auto const full_span { make_span(buffer) };
        auto const masked_read_index { mask(read_index) };
        auto const masked_write_index { mask(write_index) };

        // When buffer is full, masked read & write indices overlap
        if (masked_read_index <= masked_write_index && is_full() == false)
            return { full_span.subspan(masked_read_index, masked_write_index), span<T const> {} };
        return { full_span.subspan(masked_read_index, capacity()),
                 full_span.subspan(0u, masked_write_index) };
    }

  private:
    auto destroy_objects() {
        for (index_type i = read_index; i != write_index; ++i)
            buffer[mask(i)].~T();
    }
};

}  // namespace nlc
