/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/memory.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/align.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/meta/traits.hpp>

#include "impl/vector.hpp"

namespace nlc {

template<typename T, typename _index_type = usize> class light_vector final {
  public:
    using data_type = T;
    using index_type = meta::if_default<_index_type, usize>;
    static_assert(meta::is_unsigned_integer<index_type>);
    static_assert(nlc::meta::is_ref<data_type> == false,
                  "nlc container doesn't support reference types");

  private:
    T * _data = nullptr;

    struct Header final {
        u32 capacity = 0u;
        u32 size = 0u;
    };

    static constexpr auto alignment_value =
        alignment { alignof(Header) > alignof(T) ? alignof(Header) : alignof(T) };

    [[nodiscard]] static constexpr auto compute_buffer_size(usize const capacity) -> bytes<usize> {
        return size_of<Header> + capacity * size_of<T>;
    }

    [[nodiscard]] static constexpr auto compute_capacity(usize const buffer_size) -> u32 {
        return static_cast<u32>((buffer_size - sizeof(Header)) / sizeof(T));
    }

  public:  // Construction, destruction, assignment and copy
    light_vector() = default;

    light_vector(light_vector && other)
        : _data { other._data } {
        other._data = nullptr;
    }

    light_vector(light_vector const &) = delete;

    [[nodiscard]] static auto create_and_reserve(allocator & alloc, usize const size) -> light_vector {
        light_vector res;
        res.reserve(alloc, size);
        return res;
    }

    template<typename... Args>
    [[nodiscard]] static auto create_and_append(allocator & alloc, Args &&... args) -> light_vector {
        light_vector res;
        res.append(alloc, nlc_fwd(args)...);
        return res;
    }

    template<typename... Args>
    [[nodiscard]] static auto create_and_append_many(allocator & alloc,
                                                     Args &&... elements) -> light_vector {
        light_vector res;
        res.reserve(alloc, sizeof...(Args));
        res.append_many_no_grow(nlc_fwd(elements)...);
        return res;
    }

    [[nodiscard]] static auto create_and_fill(allocator & alloc,
                                              T const & element,
                                              usize const count) -> light_vector {
        light_vector res;

        res.reserve(alloc, count);
        for (auto i = 0u; i < count; ++i)
            res.append_no_grow(element);

        return res;
    }

    auto operator=(light_vector && rhs) -> light_vector & {
        nlc_assert(this != &rhs);
        nlc_assert(_data == nullptr);
        _data = rhs._data;
        rhs._data = nullptr;
        return *this;
    }

    auto operator=(light_vector const &) -> light_vector & = delete;

    ~light_vector() {
        nlc_assert_msg(_data == nullptr, "light_vector must be manually freed before its destruction");
    }

    auto copy(allocator & allocator) const {
        auto res = light_vector<T, index_type> {};
        if (_data != nullptr) {
            auto const old_header = &header();
            res.bootstrap(allocator, ceil_power_of_two(old_header->size));

            auto const new_header = &res.header();
            if constexpr (meta::is_trivially_copyable<T>)
                memcpy(new_header, old_header, compute_buffer_size(old_header.size));
            else {
                new_header->size = old_header->size;
                auto const end = res._data + new_header->size;
                for (auto dest = res._data, from = _data; dest != end; ++dest, ++from)
                    new (dest) T(*from);
            }
        }
        return res;
    }

  public:  // Inspection
    [[nodiscard]] auto data() const -> T const * { return _data; }
    [[nodiscard]] auto data() -> T * { return _data; }
    [[nodiscard]] auto size() const -> usize { return _data == nullptr ? 0 : header().size; }
    [[nodiscard]] auto capacity() const -> usize {
        return _data == nullptr ? 0 : header().capacity;
    }
    [[nodiscard]] auto is_empty() const -> bool { return size() == 0u; }

  public:  // Element access
    [[nodiscard]] auto operator[](index_type const i) const -> T const & {
        return impl::vector::element_at(_data, i, size());
    }

    [[nodiscard]] auto operator[](index_type const i) -> T & {
        return impl::vector::element_at(_data, i, size());
    }

    [[nodiscard]] auto front() const -> T const & { return impl::vector::front(_data, size()); }

    [[nodiscard]] auto front() -> T & { return impl::vector::front(_data, size()); }

    [[nodiscard]] auto back() const -> T const & { return impl::vector::back(_data, size()); }

    [[nodiscard]] auto back() -> T & { return impl::vector::back(_data, size()); }

  public:  // Iteration
    [[nodiscard]] auto begin() const -> T const * { return _data; }
    [[nodiscard]] auto end() const -> T const * { return _data + size(); }
    [[nodiscard]] auto begin() -> T * { return _data; }
    [[nodiscard]] auto end() -> T * { return _data + size(); }

  public:  // Manipulation
    auto reserve(allocator & allocator, usize new_capacity) -> void {
        if (capacity() >= new_capacity)
            return;

        new_capacity = ceil_power_of_two(new_capacity);
        if (_data == nullptr) {
            bootstrap(allocator, new_capacity);
        } else {
            auto hdr = &header();
            // WARNING the reallocate at next line invalidates hdr
            auto const new_span = allocator.reallocate(span { reinterpret_cast<byte *>(hdr),
                                                              compute_buffer_size(hdr->capacity).v },
                                                       compute_buffer_size(new_capacity),
                                                       alignment_value);
            hdr = reinterpret_cast<Header *>(new_span.ptr());
            hdr->capacity = compute_capacity(new_span.size());
            _data = reinterpret_cast<T *>(hdr + 1);
        }
    }

    template<typename... Args>
    auto resize(allocator & allocator, usize const new_size, Args &&... args) -> void {
        auto const current_size = size();
        if (new_size > current_size) {
            reserve(allocator, new_size);
            for (usize i = current_size; i < new_size; ++i)
                new (_data + i) T { args... };
        } else {
            if constexpr (meta::is_trivially_destructible<T> == false) {
                for (usize i = new_size; i < current_size; ++i)
                    _data[i].~T();
            }
        }
        if (_data != nullptr)
            header().size = static_cast<u32>(new_size);
    }

    auto remove_unordered(index_type const i) -> void {
        impl::vector::remove_unordered<false>(_data, i, header().size);
    }

    auto remove_ordered(index_type const i) -> void {
        impl::vector::remove_ordered<false>(_data, i, header().size);
    }

    auto remove_last() -> void {
        auto & mutable_size = header().size;
        nlc_assert(mutable_size > 0);
        _data[--mutable_size].~T();
    }

    [[nodiscard]] auto extract_ordered(index_type const i) -> T {
        return impl::vector::remove_ordered<true>(_data, i, header().size);
    }

    [[nodiscard]] auto extract_unordered(index_type const i) -> T {
        return impl::vector::remove_unordered<true>(_data, i, header().size);
    }

    [[nodiscard]] auto extract_last() -> T {
        auto & mutable_size = header().size;
        nlc_assert(mutable_size > 0);
        auto elm = nlc_move(_data[--mutable_size]);
        _data[mutable_size].~T();
        return elm;
    }

    template<typename... Args> auto append(allocator & allocator, Args &&... args) -> T & {
        grow_if_needed(allocator, 1);

        auto const ptr = _data + header().size++;
        new (ptr) T { nlc_fwd(args)... };

        return *ptr;
    }

    template<typename... Args> auto append_no_grow(Args &&... args) -> T & {
        nlc_assert(capacity() > header().size);

        auto const ptr = _data + header().size++;
        new (ptr) T { nlc_fwd(args)... };

        return *ptr;
    }

    template<typename... Args> auto append_many(allocator & allocator, Args &&... args) -> void {
        grow_if_needed(allocator, sizeof...(args));
        (append_no_grow(nlc_fwd(args)), ...);
    }

    template<typename... Args> auto append_many_no_grow(Args &&... args) -> void {
        nlc_assert(capacity() >= size() + sizeof...(args));
        (append_no_grow(nlc_fwd(args)), ...);
    }

    auto append_from(allocator & allocator, span<T> const values) -> void {
        reserve(allocator, size() + values.size());
        append_from_no_grow(span<T const>(values));
    }

    auto append_from(allocator & allocator, span<T const> const values) -> void {
        reserve(allocator, size() + values.size());
        append_from_no_grow(values);
    }

    auto append_from_no_grow(span<T> const values) -> void {
        append_from_no_grow(span<T const>(values));
    }

    auto append_from_no_grow(span<T const> const values) -> void {
        nlc_assert(capacity() >= size() + values.size());
        if (values.is_empty())
            return;

        if constexpr (meta::is_trivially_copyable<T>)
            impl::vector::append_from(end(),
                                      header().size,
                                      values.begin(),
                                      static_cast<decltype(header().size)>(values.size()));
        else {
            for (auto & v : values)
                append_no_grow(v);
        }
    }

    auto append_from(allocator & allocator, light_vector && rhs) -> void {
        reserve(allocator, size() + rhs.size());
        append_from_no_grow(allocator, nlc_move(rhs));
    }

    auto append_from_no_grow(allocator & allocator, light_vector && rhs) -> void {
        nlc_assert(capacity() >= size() + rhs.size());
        if (rhs.is_empty())
            return;

        impl::vector::append_from(end(),
                                  header().size,
                                  rhs.begin(),
                                  static_cast<decltype(header().size)>(rhs.size()));
        rhs.clear_release_memory(allocator);
    }

    auto clear() -> void {
        if (nullptr != _data) {
            destroy_all();
            header().size = 0u;
        }
    }

    auto clear_release_memory(allocator & allocator) -> void {
        if (nullptr != _data) {
            destroy_all();
            deallocate(allocator);
        }
    }

    auto release_unused_memory(allocator & allocator) -> void {
        if (_data == nullptr)
            return;

        auto hdr = &header();
        if (hdr->size == 0) {
            deallocate(allocator);
            return;
        }

        auto const new_span = allocator.reallocate(span { reinterpret_cast<byte *>(hdr),
                                                          compute_buffer_size(hdr->capacity).v },
                                                   compute_buffer_size(hdr->size),
                                                   alignment_value);
        hdr = reinterpret_cast<Header *>(new_span.ptr());
        hdr->capacity = compute_capacity(new_span.size());
        _data = reinterpret_cast<T *>(hdr + 1);
    }

  private:
    [[nodiscard]] auto header() const -> Header & {
        nlc_assert(_data != nullptr);
        return *reinterpret_cast<Header *>(reinterpret_cast<byte *>(_data) - sizeof(Header));
    }

    auto destroy_all() -> void {
        if constexpr (!meta::is_trivially_destructible<T>) {
            for (auto & v : *this)
                v.~T();
        }
    }

    auto grow_if_needed(allocator & allocator, usize const new_elements) -> void {
        if (_data == nullptr) {
            bootstrap(allocator, ceil_power_of_two(new_elements));
        } else {
            auto hdr = &header();
            auto new_size = hdr->size + new_elements;
            if (new_size > hdr->capacity) {
                new_size = ceil_power_of_two(new_size);
                // WARNING the reallocate at next line invalidates hdr
                auto const new_span =
                    allocator.reallocate(span { reinterpret_cast<byte *>(hdr),
                                                compute_buffer_size(hdr->capacity).v },
                                         compute_buffer_size(new_size),
                                         alignment_value);

                hdr = reinterpret_cast<Header *>(new_span.ptr());
                hdr->capacity = compute_capacity(new_span.size());
                _data = reinterpret_cast<T *>(hdr + 1);
            }
        }
    }

    auto deallocate(allocator & allocator) -> void {
        nlc_assert(_data != nullptr);
        auto hdr = &header();
        allocator.free(span { reinterpret_cast<byte *>(hdr), compute_buffer_size(hdr->capacity).v });
        _data = nullptr;
    }

    auto bootstrap(allocator & allocator, usize const count) -> void {
        auto const new_span = allocator.allocate(compute_buffer_size(count), alignment_value);
        auto const hdr = reinterpret_cast<Header *>(new_span.ptr());
        hdr->size = 0;
        hdr->capacity = compute_capacity(new_span.size());
        _data = reinterpret_cast<T *>(hdr + 1);
    }
};  // namespace nlc

}  // namespace nlc
