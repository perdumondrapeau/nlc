/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/memory.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/fundamentals/align.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/traits.hpp>

#include "impl/vector.hpp"

namespace nlc {

template<typename T, typename _index_type = usize> class vector final {
  public:
    using data_type = T;
    using index_type = meta::if_default<_index_type, usize>;
    static_assert(meta::is_unsigned_integer<index_type>);
    static_assert(nlc::meta::is_ref<data_type> == false,
                  "nlc container doesn't support reference types");

  private:
    T * _data = nullptr;
    u32 _size = 0;
    u32 _capacity = 0;
    allocator * _allocator = nullptr;

    [[nodiscard]] static constexpr auto buffer_size(usize const count) -> bytes<usize> {
        return count * size_of<T>;
    }

  public:  // Construction, destruction, assignment and copy
    explicit vector(allocator & alloc)
        : _allocator { &alloc } {}

    vector(vector && other)
        : _data { other._data }
        , _size { other._size }
        , _capacity { other._capacity }
        , _allocator { other._allocator } {
        other._data = nullptr;
        other._size = 0;
        other._capacity = 0;
    }

    // TODO delete
    vector(vector const & other)
        : _size { other._size }
        , _allocator { other._allocator } {
        if (other._size == 0)
            return;

        auto const new_span = _allocator->allocate<T>(other._capacity);
        _data = new_span.ptr();
        _capacity = static_cast<decltype(_capacity)>(new_span.size());

        if constexpr (meta::is_trivially_copyable<T>)
            memcpy(_data, other._data, buffer_size(_size));
        else {
            auto const end = _data + _size;
            for (auto dest = _data, from = other._data; dest != end; ++dest, ++from)
                new (dest) T(*from);
        }
    }

    [[nodiscard]] static auto create_and_reserve(allocator & alloc, usize const capacity) -> vector {
        vector res { alloc };
        res.reserve(capacity);
        return res;
    }

    template<typename... Args>
    [[nodiscard]] static auto create_and_append(allocator & alloc, Args &&... args) -> vector {
        vector res { alloc };
        res.append(nlc_fwd(args)...);
        return res;
    }

    template<typename... Args>
    [[nodiscard]] static auto create_and_append_many(allocator & alloc, Args &&... elements) -> vector {
        vector res { alloc };
        res.reserve(sizeof...(elements));
        res.append_many_no_grow(nlc_fwd(elements)...);
        return res;
    }

    [[nodiscard]] static auto create_and_fill(allocator & alloc,
                                              T const & element,
                                              usize const count) -> vector {
        vector res { alloc };

        res.reserve(count);
        for (auto i = 0u; i < count; ++i)
            res.append_no_grow(element);

        return res;
    }

    auto operator=(vector && other) -> vector & {
        nlc_assert(this != &other);
        nlc_assert(_allocator == other._allocator);
        clear_release_memory();
        _data = other._data;
        _size = other._size;
        _capacity = other._capacity;
        other._data = nullptr;
        other._size = 0;
        other._capacity = 0;
        return *this;
    }

    ~vector() {
        if (_data != nullptr) {
            destroy_all();
            deallocate();
        }
    }

    auto copy() -> vector & { return *this; }

  public:  // Inspection
    [[nodiscard]] auto data() const -> T const * { return _data; }
    [[nodiscard]] auto data() -> T * { return _data; }
    [[nodiscard]] auto size() const -> usize { return _size; }
    [[nodiscard]] auto capacity() const -> usize { return _capacity; }
    [[nodiscard]] auto is_empty() const -> bool { return size() == 0u; }

  public:  // Element access
    [[nodiscard]] auto operator[](index_type const i) const -> T const & {
        return impl::vector::element_at(_data, i, size());
    }

    [[nodiscard]] auto operator[](index_type const i) -> T & {
        return impl::vector::element_at(_data, i, size());
    }

    [[nodiscard]] auto front() const -> T const & { return impl::vector::front(_data, size()); }

    [[nodiscard]] auto front() -> T & { return impl::vector::front(_data, size()); }

    [[nodiscard]] auto back() const -> T const & { return impl::vector::back(_data, size()); }

    [[nodiscard]] auto back() -> T & { return impl::vector::back(_data, size()); }

  public:  // Iteration
    [[nodiscard]] auto begin() const -> T const * { return _data; }
    [[nodiscard]] auto end() const -> T const * { return _data + _size; }
    [[nodiscard]] auto begin() -> T * { return _data; }
    [[nodiscard]] auto end() -> T * { return _data + _size; }

  public:  // Manipulation
    auto reserve(usize const new_capacity) -> void {
        if (_capacity >= new_capacity)
            return;

        auto const requested_capacity = ceil_power_of_two(new_capacity);
        auto const new_span = _allocator->reallocate(span { _data, _capacity }, requested_capacity);
        _data = new_span.ptr();
        _capacity = static_cast<decltype(_capacity)>(new_span.size());
    }

    template<typename... Args> auto resize(usize const new_size, Args &&... args) -> void {
        if (new_size > _size) {
            reserve(new_size);
            for (usize i = _size; i < new_size; ++i)
                new (_data + i) T { args... };
        } else {
            if constexpr (meta::is_trivially_destructible<T> == false) {
                for (usize i = new_size; i < _size; ++i)
                    _data[i].~T();
            }
        }
        _size = static_cast<u32>(new_size);
    }

    auto remove_unordered(index_type const i) -> void {
        impl::vector::remove_unordered<false>(_data, i, _size);
    }

    auto remove_ordered(index_type const i) -> void {
        impl::vector::remove_ordered<false>(_data, i, _size);
    }

    auto remove_last() -> void {
        nlc_assert(_size > 0);
        _data[--_size].~T();
    }

    [[nodiscard]] auto extract_ordered(index_type const i) -> T {
        return impl::vector::remove_ordered<true>(_data, i, _size);
    }

    [[nodiscard]] auto extract_unordered(index_type const i) -> T {
        return impl::vector::remove_unordered<true>(_data, i, _size);
    }

    [[nodiscard]] auto extract_last() -> T {
        nlc_assert(_size > 0);
        auto elm = nlc_move(_data[--_size]);
        _data[_size].~T();
        return elm;
    }

    template<typename... Args> auto append(Args &&... args) -> T & {
        grow_if_needed(1);

        auto const ptr = _data + _size++;
        new (ptr) T { nlc_fwd(args)... };

        return *ptr;
    }

    template<typename... Args> auto append_no_grow(Args &&... args) -> T & {
        nlc_assert(capacity() > _size);

        auto const ptr = _data + _size++;
        new (ptr) T { nlc_fwd(args)... };

        return *ptr;
    }

    template<typename... Args> auto append_many(Args &&... args) -> void {
        grow_if_needed(sizeof...(args));
        (append_no_grow(nlc_fwd(args)), ...);
    }

    template<typename... Args> auto append_many_no_grow(Args &&... args) -> void {
        nlc_assert(capacity() >= _size + sizeof...(args));
        (append_no_grow(nlc_fwd(args)), ...);
    }

    auto append_from(span<T> const values) -> void {
        reserve(_size + values.size());
        append_from_no_grow(span<T const>(values));
    }

    auto append_from(span<T const> const values) -> void {
        reserve(_size + values.size());
        append_from_no_grow(values);
    }

    auto append_from_no_grow(span<T> const values) -> void {
        append_from_no_grow(span<T const>(values));
    }

    auto append_from_no_grow(span<T const> const values) -> void {
        nlc_assert(capacity() >= _size + values.size());

        if constexpr (meta::is_trivially_copyable<T>) {
            impl::vector::append_from(_data + _size,
                                      _size,
                                      values.ptr(),
                                      static_cast<decltype(_size)>(values.size()));
        } else {
            for (auto & v : values)
                append_no_grow(v);
        }
    }

    auto append_from(vector && other) -> void {
        reserve(_size + other._size);
        append_from_no_grow(nlc_move(other));
    }

    auto append_from_no_grow(vector && other) -> void {
        nlc_assert(capacity() >= _size + other.size());
        impl::vector::append_from(_data + _size, _size, other._data, other._size);
        other.clear_release_memory();
    }

    auto clear() -> void {
        destroy_all();
        _size = 0u;
    }

    auto clear_release_memory() -> void {
        if (nullptr != _data) {
            destroy_all();
            deallocate();
            _capacity = 0;
            _size = 0;
        }
    }

    auto release_unused_memory() -> void {
        if (_data == nullptr)
            return;

        if (_size == 0) {
            deallocate();
            return;
        }

        auto const new_span = _allocator->reallocate(span { _data, _capacity }, _size);
        _data = new_span.ptr();
        _capacity = static_cast<decltype(_capacity)>(new_span.size());
    }

  private:
    auto destroy_all() -> void {
        if constexpr (meta::is_trivially_destructible<T> == false) {
            for (auto & v : *this)
                v.~T();
        }
    }

    auto grow_if_needed(usize const new_elements) -> void {
        auto new_size = _size + new_elements;
        if (new_size > _capacity) {
            new_size = ceil_power_of_two(new_size);
            span<T> new_span;
            if (_data == nullptr)
                new_span = _allocator->allocate<T>(new_size);
            else
                new_span = _allocator->reallocate(span { _data, _capacity }, new_size);
            _data = new_span.ptr();
            _capacity = static_cast<decltype(_capacity)>(new_span.size());
        }
    }

    auto deallocate() -> void {
        nlc_assert(_data != nullptr);
        _allocator->free(span { _data, _capacity });
        _data = nullptr;
    }
};
}  // namespace nlc
