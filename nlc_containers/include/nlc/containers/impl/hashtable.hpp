/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/memory.hpp>
#include <nlc/dialect/pair.hpp>
#include <nlc/dialect/types.hpp>
#include <nlc/fundamentals/align.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/hash.hpp>
#include <nlc/meta/traits.hpp>

////////////////////////////////////////////////////////////////////////////////////////////////////

namespace nlc {
namespace impl::ht {
    using fingerprint_type = u8;

    constexpr u32 invalid_index = 0xFFFF'FFFF;
    constexpr auto default_max_load_percentage = 80u;
    constexpr u32 minimal_capacity = 8;

    [[nodiscard]] constexpr auto is_under_max_load_factor(u32 const size,
                                                          u32 const cap,
                                                          u64 const maxPercentage) {
        return size * 100ull < maxPercentage * cap;
    }

    [[nodiscard]] constexpr auto capacity_for_size(u32 const size, u64 const max_percentage) -> u32 {
        auto new_cap = (size * 100ull) / max_percentage;
        nlc_assert(new_cap < invalid_index);

        new_cap = ceil_power_of_two(static_cast<u32>(new_cap + 1));
        nlc_assert(is_under_max_load_factor(size, static_cast<u32>(new_cap), max_percentage));

        return static_cast<u32>(new_cap);
    }

    template<typename T> [[nodiscard]] auto get_key(T const * const key) -> T const & {
        return *key;
    }

    template<typename K, typename V>
    [[nodiscard]] auto get_key(nlc::pair<K, V> const * const pair) -> K const & {
        return pair->first;
    }

    struct meta_data final {
      private:
        static constexpr int fingerprint_bitcount = 7;
        static constexpr u8 fingerprint_mask = (1u << fingerprint_bitcount) - 1u;
        static constexpr fingerprint_type tombstone = 1;

      public:
        bool used : 1 = false;
        fingerprint_type fingerprint : fingerprint_bitcount = 0;

        [[nodiscard]] static constexpr auto make_fingerprint(hash_result const h) -> fingerprint_type {
            return static_cast<fingerprint_type>(
                h.value() >> (sizeof(hash_result::underlying_type) * 8 - fingerprint_bitcount));
        }

        [[nodiscard]] auto is_empty() const -> bool { return used == false && fingerprint == 0; }
        [[nodiscard]] auto is_tombstone() const -> bool {
            return used == false && fingerprint == tombstone;
        }

        void fill(fingerprint_type const fp) {
            nlc_assert(used == false && (fingerprint == 0 || fingerprint == tombstone));
            used = true;
            fingerprint = fp & fingerprint_mask;  // Explicitly dropping bits to content GCC
        }

        void remove() {
            nlc_assert(used && is_tombstone() == false);
            used = false;
            fingerprint = tombstone;
        }

        void reset() {
            used = false;
            fingerprint = 0;
        }
    };
    static_assert(sizeof(meta_data) == 1);
    static_assert(alignof(meta_data) == 1);

    template<typename T> class [[nodiscard]] try_result final {
      public:
        try_result() = default;
        ~try_result() = default;

        try_result(const try_result &) = default;
        try_result(try_result &&) = default;

        try_result & operator=(try_result const &) = default;
        try_result & operator=(try_result &&) = default;

        try_result(T * const ptr)
            : _ptr { ptr } {}

        [[nodiscard]] auto has_value() const -> bool {
#ifndef NDEBUG
            _checked = true;
#endif
            return nullptr != _ptr;
        }

        [[nodiscard]] auto value() const -> T & {
#ifndef NDEBUG
            nlc_assert(_checked);
#endif
            nlc_assert(has_value());
            return *_ptr;
        }

      private:
        T * _ptr = nullptr;
#ifndef NDEBUG
        mutable bool _checked = false;
#endif
    };

    template<typename T> constexpr auto swap(T & a, T & b) -> void {
        auto tmp = T { nlc_move(a) };
        a = nlc_move(b);
        b = nlc_move(tmp);
    }
}  // namespace impl::ht

////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename key_type, typename entry_type, u64 max_load_percentage> class hashtable {
    static_assert(max_load_percentage < 100, "Max load over 99% not supported.");

  protected:
    using meta_data_type = impl::ht::meta_data;

    struct Header final {
        entry_type * entries = nullptr;
        u32 capacity = 0;
    };

    template<typename T> class iterator_impl final {
      public:
        friend class hashtable;

        iterator_impl(hashtable const & hm, u32 const idx)
            : _hm { &hm }
            , _idx { idx }
#ifndef NDEBUG
            , _timestamp { hm._timestamp }
#endif
        {
            nlc_assert(idx <= hm._capacity());
        }
        ~iterator_impl() = default;

        iterator_impl(iterator_impl const &) = default;
        iterator_impl(iterator_impl &&) = default;
        iterator_impl & operator=(iterator_impl const &) = default;
        iterator_impl & operator=(iterator_impl &&) = default;

        [[nodiscard]] auto operator!=(iterator_impl const & other) const -> bool {
#ifndef NDEBUG
            nlc_assert(is_valid());
#endif
            return !operator==(other);
        }

        [[nodiscard]] auto operator==(iterator_impl const & other) const -> bool {
#ifndef NDEBUG
            nlc_assert(is_valid());
#endif
            return _hm == other._hm && _idx == other._idx;
        }

        auto operator++() -> iterator_impl & {
#ifndef NDEBUG
            nlc_assert(is_valid());
#endif
            nlc_assert(_idx < _hm->_capacity());

            u32 idx = _idx + 1;
            meta_data_type * meta = &_hm->_metadata[idx];
            auto const cap = _hm->_capacity();
            while (idx < cap) {
                if (meta->used) {
                    break;
                }
                ++idx;
                ++meta;
            }

            _idx = idx;
            return *this;
        }

        auto operator++(int) -> iterator_impl {
            auto const it = *this;
            operator++();
            return it;
        }

        [[nodiscard]] auto operator*() const -> T & {
#ifndef NDEBUG
            nlc_assert(is_valid());
#endif
            nlc_assert(_idx < _hm->_capacity());
            return _hm->entries()[_idx];
        }

        [[nodiscard]] auto operator->() const -> T * { return &operator*(); }

      private:
        hashtable const * _hm { nullptr };
        u32 _idx = 0;

#ifndef NDEBUG
        u32 _timestamp = 0;

        [[nodiscard]] auto is_valid() const -> bool { return _timestamp == _hm->_timestamp; }
#endif
    };

  protected:
    meta_data_type * _metadata = nullptr;
    u32 _size = 0;
    u32 _available = 0;
    allocator * _allocator = nullptr;
#ifndef NDEBUG
    u32 _timestamp = 0;
#endif

  public:
    using iterator = iterator_impl<entry_type>;
    using const_iterator = iterator_impl<entry_type const>;

    hashtable(allocator & alloc)
        : _allocator { &alloc } {}

    ~hashtable() {
        destroy_entries<false>();
        deallocate();
    }

    hashtable(hashtable const & other)
        : _size { other._size }
        , _available { other._available }
        , _allocator { other._allocator } {
        if (other._metadata == nullptr) {
            nlc_assert(other._size == 0);
            nlc_assert(other._available == 0);
            return;
        }

        auto const & other_hdr = other.header();
        auto const cap = other_hdr.capacity;
        allocate(cap);
        auto & hdr = header();
        memcpy(_metadata, other._metadata, cap * sizeof(meta_data_type));

        auto entry = hdr.entries;
        auto other_entry = other_hdr.entries;
        auto const meta_end = _metadata + hdr.capacity;
        for (auto meta = _metadata; meta != meta_end; ++meta, ++entry, ++other_entry) {
            if (meta->used) {
                new (entry) entry_type { *other_entry };
            }
        }
    }

    hashtable(hashtable && other)
        : _metadata { other._metadata }
        , _size { other._size }
        , _available { other._available }
        , _allocator { other._allocator } {
        other._metadata = nullptr;
        other._size = 0;
        other._available = 0;
        other.invalidate_iterators();
    }

    auto operator=(hashtable const & other) -> hashtable & {
        hashtable table(other);
        *this = nlc_move(table);

        return *this;
    }

    auto operator=(hashtable && other) -> hashtable & {
        nlc_assert(this != &other);
        nlc_assert(_allocator == other._allocator);
        destroy_entries<false>();
        deallocate();

        _metadata = other._metadata;
        _size = other._size;
        _available = other._available;
        invalidate_iterators();
        other._size = 0;
        other._metadata = nullptr;
        other._available = 0;
        other.invalidate_iterators();

        return *this;
    }

    auto swap(hashtable & other) -> void {
        nlc_assert(_allocator == other._allocator);
        impl::ht::swap(_metadata, other._metadata);
        impl::ht::swap(_size, other._size);
        impl::ht::swap(_available, other._available);
    }

    [[nodiscard]] auto size() const -> usize { return _size; }

    [[nodiscard]] auto size_bytes() const -> bytes<usize> {
        if (nullptr == _metadata)
            return 0;
        return sizeof(Header) + _capacity() * (sizeof(entry_type) + sizeof(meta_data_type)) +
               alignof(entry_type) - 1;
    }

    [[nodiscard]] auto is_empty() const -> bool { return size() == 0u; }

    [[nodiscard]] auto capacity() const -> usize { return _capacity(); }

    [[nodiscard]] auto load_percentage() const -> float {
        auto const cap = _capacity();
        if (cap == 0) {
            return 0.f;
        }
        return (load() * 100.f) / static_cast<float>(cap);
    }

    [[nodiscard]] auto begin() const -> const_iterator { return { *this, get_first_idx() }; }

    [[nodiscard]] auto end() const -> const_iterator { return { *this, _capacity() }; }

    [[nodiscard]] auto begin() -> iterator { return { *this, get_first_idx() }; }

    [[nodiscard]] auto end() -> iterator { return { *this, _capacity() }; }

    [[nodiscard]] auto cbegin() const -> const_iterator { return { *this, get_first_idx() }; }

    [[nodiscard]] auto cend() const -> const_iterator { return { *this, _capacity() }; }

    auto remove(key_type const & k) -> void {
        auto const idx = get_entry_idx(k);
        nlc_assert(impl::ht::invalid_index != idx);

        _metadata[idx].remove();
        entries()[idx].~entry_type();
        --_size;

        invalidate_iterators();
    }

    auto try_remove(key_type const & k) -> bool {
        auto const idx = get_entry_idx(k);
        if (impl::ht::invalid_index == idx)
            return false;

        _metadata[idx].remove();
        entries()[idx].~entry_type();
        --_size;

        invalidate_iterators();

        return true;
    }

    auto clear() -> void {
        destroy_entries<true>();
        _size = 0;
        _available = static_cast<u32>((_capacity() * max_load_percentage) / 100ull);
    }

    auto clear_release_memory() -> void {
        clear();
        deallocate();
    }

    [[nodiscard]] auto contains(key_type const & k) const -> bool {
        auto const idx = get_entry_idx(k);
        return idx != impl::ht::invalid_index;
    }

    [[nodiscard]] auto insert_uninitialized(key_type const & k) -> entry_type * {
        auto const h = hash(k);
        auto const mask = _capacity() - 1;
        auto const fingerprint = meta_data_type::make_fingerprint(h);

        u32 idx = static_cast<u32>(h.value()) & mask;
        meta_data_type * meta = &_metadata[idx];

        while (meta->used) {
            idx = (idx + 1) & mask;
            meta = &_metadata[idx];
        }

        if (meta->is_tombstone() == false) {
            nlc_assert(_available > 0);
            --_available;
        }

        meta->fill(fingerprint);

        ++_size;

        return entries() + idx;
    }

    [[nodiscard]] auto try_insert_uninitialized(key_type const & k) -> entry_type * {
        auto const h = hash(k);
        auto const mask = _capacity() - 1;
        auto const fingerprint = meta_data_type::make_fingerprint(h);

        u32 idx = static_cast<u32>(h.value()) & mask;
        meta_data_type * meta = &_metadata[idx];

        u32 insert_idx = impl::ht::invalid_index;
        while (meta->is_empty() == false) {
            if (meta->used && meta->fingerprint == fingerprint) {
                entry_type * const entry = entries() + idx;
                if (impl::ht::get_key(entry) == k) {
                    return nullptr;
                }
            } else if (insert_idx == impl::ht::invalid_index && meta->is_tombstone()) {
                insert_idx = idx;
            }

            idx = (idx + 1) & mask;
            meta = &_metadata[idx];
        }

        if (insert_idx != impl::ht::invalid_index) {
            idx = insert_idx;
            meta = &_metadata[idx];
            nlc_assert(meta->is_tombstone());
        } else {
            nlc_assert(meta->is_tombstone() == false);
            nlc_assert(_available > 0);
            --_available;
        }

        ++_size;
        meta->fill(fingerprint);
        entry_type * const entry = entries() + idx;

        return entry;
    }

    auto reserve(usize const size) -> void {
        nlc_assert(size > 0u);
        if (impl::ht::is_under_max_load_factor(static_cast<u32>(size),
                                               this->_capacity(),
                                               max_load_percentage) == false) {
            grow(impl::ht::capacity_for_size(static_cast<u32>(size), max_load_percentage));
        }
    }

  protected:
    [[nodiscard]] auto header() const -> Header & {
        nlc_assert(_metadata != nullptr);
        return *reinterpret_cast<Header *>(reinterpret_cast<usize>(_metadata) - sizeof(Header));
    }

    [[nodiscard]] auto _capacity() const -> u32 {
        if (_metadata == nullptr)
            return 0;

        return header().capacity;
    }

    [[nodiscard]] auto entries() const -> entry_type * { return header().entries; }

    auto invalidate_iterators() -> void {
#ifndef NDEBUG
        ++_timestamp;
#endif
    }

    [[nodiscard]] auto load() const -> u32 {
        auto const max_load = (_capacity() * max_load_percentage) / 100ull;
        nlc_assert(max_load >= _available);
        return static_cast<u32>(max_load - _available);
    }

    auto compute_meta_size(u32 const capacity) -> usize {
        return sizeof(Header) + capacity * sizeof(meta_data_type);
    }

    static constexpr auto entries_alignment_padding = alignof(entry_type) - 1;

    auto compute_entries_size(u32 const capacity) -> usize {
        return capacity * sizeof(entry_type) + entries_alignment_padding;  // to be able to align
                                                                           // correctly
    }

    auto allocate(u32 const capacity) -> void {
        nlc_assert(nullptr == _metadata);

        // We're grouping the metadata and the entries in a single allocation.
        // This will run faster, and result in less memory fragmentation.
        // There is a small memory overhead in case it's needed to align the
        // entries correctly.
        // We're hiding a precomputed pointer to the entries just before the
        // actual metadata.
        auto const meta_size = compute_meta_size(capacity);
        auto const entry_size = compute_entries_size(capacity);
        auto const alloc_size = meta_size + entry_size;

        auto const buffer = reinterpret_cast<usize>(
            _allocator->allocate(bytes<usize>(alloc_size), align_of<Header>).ptr());
        static_assert(alignof(meta_data_type) == 1, "this needs to hold");
        auto const metadata = reinterpret_cast<meta_data_type *>(buffer + sizeof(Header));

        auto entry_ptr = buffer + meta_size;  // get past the metadata
        entry_ptr = (entry_ptr + entries_alignment_padding) &
                    ~entries_alignment_padding;  // round entry_ptr up to be aligned on
                                                 // alignof(entry_type)
        nlc_assert(entry_ptr + capacity * sizeof(entry_type) <= buffer + alloc_size);

        auto const header = reinterpret_cast<Header *>(buffer);
        header->entries = reinterpret_cast<entry_type *>(entry_ptr);  // store precomputed entry
                                                                      // pointer
        header->capacity = capacity;
        _metadata = metadata;
    }

    auto deallocate() -> void {
        if (_metadata == nullptr)
            return;

        auto const capacity = _capacity();
        auto const meta_size = compute_meta_size(capacity);
        auto const entry_size = compute_entries_size(capacity);
        auto const alloc_size = meta_size + entry_size;

        _allocator->free(span<byte> { reinterpret_cast<byte *>(&header()), alloc_size });

        _metadata = nullptr;
        _available = 0;
    }

    template<bool reset_meta_data> auto destroy_entries() -> void {
        if (_size > 0) {
            if constexpr (meta::is_trivially_destructible<entry_type> && reset_meta_data) {
                memset(static_cast<void *>(_metadata), 0, header().capacity);
            } else {
                auto entry = entries();
                auto const meta_end = _metadata + _capacity();
                for (auto meta = _metadata; meta != meta_end; ++meta, ++entry) {
                    if (meta->used)
                        entry->~entry_type();

                    if constexpr (reset_meta_data)
                        meta->reset();
                }
            }
        }
    }

    [[nodiscard]] auto get_first_idx() const -> u32 {
        auto meta = _metadata;
        auto const end = _metadata + _capacity();

        while (meta < end) {
            if (meta->used)
                break;
            ++meta;
        }

        return static_cast<u32>(meta - _metadata);
    }

    [[nodiscard]] auto get_entry_idx(key_type const & k) const -> u32 {
        if (_size == 0) {
            return impl::ht::invalid_index;
        }

        auto const h = hash(k);
        auto const mask = _capacity() - 1;
        auto const fingerprint = meta_data_type::make_fingerprint(h);

        u32 idx = static_cast<u32>(h.value()) & mask;
        meta_data_type * meta = &_metadata[idx];

        while (meta->is_empty() == false) {
            if (meta->used && meta->fingerprint == fingerprint) {
                entry_type * const entry = entries() + idx;
                if (impl::ht::get_key(entry) == k) {
                    return idx;
                }
            }

            idx = (idx + 1) & mask;
            meta = &_metadata[idx];
        }

        return impl::ht::invalid_index;
    }

    auto grow_if_needed(u32 const insert_count) -> void {
        if (insert_count > _available) {
            grow(impl::ht::capacity_for_size(load() + insert_count, max_load_percentage));
        }
    }

    auto grow(u32 capacity) -> void {
        nlc_assert((capacity & (capacity - 1)) == 0);
        capacity = capacity < impl::ht::minimal_capacity ? impl::ht::minimal_capacity : capacity;

        hashtable map { *_allocator };
        map.allocate(capacity);

        memset(static_cast<void *>(map._metadata), 0, capacity);

        // TODO optimize the rehash
        map._available = static_cast<u32>((capacity * max_load_percentage) / 100ull);

        if (_size != 0) {
            auto entry = entries();
            auto const meta_end = _metadata + this->_capacity();
            for (auto meta = _metadata; meta != meta_end; ++meta, ++entry) {
                if (meta->used) {
                    auto const ptr = map.insert_uninitialized(impl::ht::get_key(entry));
                    new (ptr) entry_type { nlc_move(*entry) };
                    entry->~entry_type();
                }
            }
        }
        nlc_assert(map._size == _size);
        _size = 0;

        invalidate_iterators();

        swap(map);
    }
};
}  // namespace nlc

////////////////////////////////////////////////////////////////////////////////////////////////////
