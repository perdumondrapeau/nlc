/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/memory.hpp>
#include <nlc/dialect/types.hpp>
#include <nlc/meta/logic.hpp>
#include <nlc/meta/traits.hpp>

namespace nlc {

#if defined(__GNUC__) && !defined(__clang__)
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wclass-memaccess"
#endif

namespace impl::vector {
    template<typename T, typename index_type, typename size_type>
    [[nodiscard]] inline auto element_at(T * const data,
                                         index_type const i,
                                         [[maybe_unused]] size_type const size) -> T & {
        nlc_assert(static_cast<size_type>(i) < size);
        return *(data + i);
    }

    template<typename T, typename size_type>
    [[nodiscard]] inline auto front(T * const data, [[maybe_unused]] size_type const size) -> T & {
        nlc_assert(size > 0u);
        return *data;
    }

    template<typename T, typename size_type>
    [[nodiscard]] inline auto back(T * const data, size_type const size) -> T & {
        nlc_assert(size > 0u);
        auto const idx = size - 1;
        return *(data + idx);
    }

    template<bool return_value, typename T, typename index_type, typename size_type>
    [[nodiscard]] inline auto remove_ordered(T * const data, index_type const i, size_type & mutable_size)
        -> nlc::meta::if_cond<return_value, T, void> {
        nlc_assert(static_cast<size_type>(i) < mutable_size);

        auto const remove = [&]() {
            data[i].~T();

            --mutable_size;
            if (mutable_size != static_cast<size_type>(i)) {
                memmove(data + i, data + i + 1, (mutable_size - i) * sizeof(T));
            }
        };

        if constexpr (return_value) {
            auto ret = nlc_move(data[i]);
            remove();
            return ret;
        } else {
            remove();
        }
    }

    template<bool return_value, typename T, typename index_type, typename size_type>
    [[nodiscard]] inline auto remove_unordered(T * const data, index_type const i, size_type & mutable_size)
        -> nlc::meta::if_cond<return_value, T, void> {
        nlc_assert(static_cast<size_type>(i) < mutable_size);

        --mutable_size;

        auto const remove = [&]() {
            if (mutable_size == static_cast<size_type>(i)) {
                data[i].~T();
            } else {
                data[i] = nlc_move(data[mutable_size]);
                data[mutable_size].~T();
            }
        };

        if constexpr (return_value) {
            auto ret = nlc_move(data[i]);
            remove();
            return ret;
        } else {
            remove();
        }
    }

    template<typename T, typename size_type>
    inline auto append_from(T * const data_end,
                            size_type & mutable_size,
                            T const * const data_to_append,
                            size_type const data_to_append_size) -> void {
        memcpy(data_end, data_to_append, data_to_append_size * sizeof(T));
#if defined(__GNUC__) && !defined(__clang__)
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Warray-bounds"
#endif
        mutable_size += data_to_append_size;
#if defined(__GNUC__) && !defined(__clang__)
#    pragma GCC diagnostic pop
#endif
    }
}  // namespace impl::vector

#if defined(__GNUC__) && !defined(__clang__)
#    pragma GCC diagnostic pop
#endif

}  // namespace nlc
