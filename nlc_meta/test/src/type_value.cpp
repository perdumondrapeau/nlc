/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/meta/traits.hpp>
#include <nlc/meta/type_value.hpp>

using namespace nlc::meta;

static_assert(is_greater<4, 2>);
static_assert(!is_greater<3, 3>);
static_assert(!is_greater<3, 5>);
static_assert(is_less<2, 4>);
static_assert(!is_less<2, 2>);
static_assert(!is_less<5, 2>);
static_assert(is_equal<3, 3>);
static_assert(!is_equal<3, 5>);

static_assert(is_empty_type<Value<3>>);

static_assert(value<3> == value<3>);
static_assert(value<3> == 3);
static_assert(value<3> != value<4>);
static_assert(value<3> != 4);
static_assert(value<3> < value<4>);
static_assert(value<0> == value<'\0'>);

static_assert(type<int> == type<int>);
static_assert(type<float> != type<int>);
static_assert(!(type<int> == type<float>));
static_assert(!(type<int> != type<int>));

int main() { return 0; }
