/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/meta/detect.hpp>
#include <nlc/meta/is_same.hpp>
#include <nlc/meta/traits.hpp>
#include <nlc/meta/units.hpp>

#include "../utils.hpp"

using namespace nlc::meta;

static_assert(is_same<impl::simplify<pair<list<int, char, float>, list<char, short>>>,
                      pair<list<int, float>, list<short>>>);
static_assert(impl::is_equivalent_dim<pair<list<int, float>, list<char, short>>,
                                      pair<list<float, int>, list<short, char>>>);
static_assert(is_trivially_destructible<meters<>>);
static_assert(is_trivially_copyable<meters<>>);

template<typename T, typename U> using can_assign = decltype(nlc::declval<T> = nlc::declval<U>);

static_assert(is_detected<can_assign, meters<>, meters<>>);
static_assert(!is_detected<can_assign, meters<>, seconds<>>);
static_assert(!is_detected<can_assign, float, seconds<>>);

using t1 = unit<pair<list<int, double>, list<char, float, short>>, int>;
using t2 = unit<pair<list<double, int>, list<float, char, short>>, int>;
using t3 = unit<pair<list<double, int>, list<char, short>>, int>;
static_assert(is_equivalent<t1, t2>);

static auto f1(t1 v) { return 2 * v.v; }
static auto f2(t2 v) { return 3 * v.v; }

int main() {
    static auto d = meters<>(1.5f);
    assert(d == 1.5f);
    // d = seconds(3);

    [[maybe_unused]] meters<char> m;  // default_contructible

    auto d_int = meters<int>(0);
    assert(d_int == 0);
    d_int = nlc::unit_cast<int>(d);
    assert(d_int == 1);

    auto t = seconds<>(2);

    auto s = d / t;
    static_assert(is_equivalent<decltype(s), meters_per_second<>>);

    auto s2 = 2.f * s;
    static_assert(is_equivalent<decltype(s2), meters_per_second<>>);

    auto s3 = s / t * unit<pair<list<pixel_dim>, list<>>>(3) / meters<>(2);
    static_assert(
        is_equivalent<decltype(s3), unit<pair<list<pixel_dim>, list<second_dim, second_dim>>>>);

    auto s4 = -s3;
    static_assert(is_equivalent<decltype(s4), decltype(s3)>);
    assert(nlc::rm_unit(s4) == -nlc::rm_unit(s3));

    auto v1 = t1(1);
    auto v2 = t2(2);

    assert(f1(v1) == 2);
    assert(f1(v2) == 4);
    assert(f2(v1) == 3);
    assert(f2(v2) == 6);

    return 0;
}
