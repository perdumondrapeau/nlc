/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "logic.hpp"

namespace nlc::meta {

//-------------------------------------------------------------------------

namespace impl {
    template<typename T>
    concept Defaultable = requires { T::default_value; };

    template<typename T>
    concept Invalidable = requires { T::invalid_value; };
}  // namespace impl

//-------------------------------------------------------------------------

/* struct Description final {
 *     using underlying_type = uint;
 *     static constexpr underlying_type invalid_value = 0;
 *     static constexpr underlying_type default_value = invalid_value;
 * }
 */

template<typename Descriptor, typename Self = default_type> class id {
  public:
    using self_type = if_default<Self, id>;
    using descriptor_type = Descriptor;
    using underlying_type = typename descriptor_type::underlying_type;

    template<typename = void>
        requires impl::Defaultable<descriptor_type>
    [[nodiscard]] static constexpr auto default_value() {
        return descriptor_type::default_value;
    }

    template<typename = void>
        requires impl::Invalidable<descriptor_type>
    static constexpr auto invalid() {
        return self_type(descriptor_type::invalid_value);
    }

  public:
    constexpr explicit id(underlying_type const & value)
        : _value(value) {}

    template<typename = void>
        requires impl::Defaultable<descriptor_type>
    constexpr id()
        : _value(default_value()) {}

    constexpr id(id const &) = default;
    constexpr auto operator=(id const &) -> id & = default;

  public:
    [[nodiscard]] constexpr decltype(auto) value() const { return (_value); }
    [[nodiscard]] constexpr decltype(auto) value() { return (_value); }

  public:
    template<typename = void>
        requires impl::Invalidable<descriptor_type>
    [[nodiscard]] constexpr auto is_valid() const -> bool {
        return _value != invalid()._value;
    }

    template<typename = void>
        requires impl::Invalidable<descriptor_type>
    [[nodiscard]] constexpr auto is_invalid() const -> bool {
        return _value == invalid()._value;
    }

  private:
    underlying_type _value;
};

//-------------------------------------------------------------------------

#define OP(op)                                                                             \
    template<typename D, typename T>                                                       \
    [[nodiscard]] constexpr auto operator op(id<D, T> const & lhs, id<D, T> const & rhs) { \
        return lhs.value() op rhs.value();                                                 \
    }

OP(==)
OP(!=)
OP(<)
OP(>)
OP(<=)
OP(>=)

#undef OP

//-------------------------------------------------------------------------

}  // namespace nlc::meta
