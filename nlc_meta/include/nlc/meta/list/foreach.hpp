/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../list.hpp"

#include "boxing.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L> struct for_each_impl;
    template<typename... Ts> struct for_each_impl<list<Ts...>> {
        template<typename F> static auto foo(F && f) { (f(box<Ts> {}), ...); }
    };
}  // namespace impl
template<typename L, typename F> auto for_each(F && f) {
    impl::for_each_impl<L>::foo(forward<F>(f));
}

}  // namespace nlc::meta
