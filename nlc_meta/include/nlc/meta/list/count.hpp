/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "../basics.hpp"
#include "../is_same.hpp"
#include "../list.hpp"

namespace nlc::meta {

namespace impl {
    template<typename L, typename T> struct count_impl;
    template<typename... Ts, typename T> struct count_impl<list<Ts...>, T> {
        static constexpr auto value = static_cast<comptime_int>(((is_same<T, Ts> ? 1 : 0) + ... + 0));
    };
}  // namespace impl
template<typename T, typename L> inline constexpr auto count = impl::count_impl<L, T>::value;

}  // namespace nlc::meta
