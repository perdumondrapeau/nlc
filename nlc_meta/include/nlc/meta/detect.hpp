/* Copyright © 2024-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "basics.hpp"
#include "tags.hpp"

namespace nlc::meta {

namespace impl {
    template<typename AlwaysVoid, template<typename...> typename Op, typename... Args>
    struct detector {
        static constexpr bool value = false;
        using type = error_type;
    };

    template<template<typename...> typename Op, typename... Args>
    struct detector<void_type<Op<Args...>>, Op, Args...> {
        static constexpr bool value = true;
        using type = Op<Args...>;
    };
}  // namespace impl

template<template<typename...> class Op, typename... Args>
inline constexpr auto is_detected = impl::detector<void, Op, Args...>::value;

template<template<typename...> class Op, typename... Args>
using detected = typename impl::detector<void, Op, Args...>::type;

}  // namespace nlc::meta
