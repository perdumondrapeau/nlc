/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/fundamentals/unique_ptr.hpp>
#include <nlc/test.hpp>

#include "../utils.hpp"

using namespace nlc;

static auto a = 0;

class A {
  public:
    A(int v)
        : _v(v) {
        a += v;
    }
    virtual ~A();

  private:
    int _v;
};

A::~A() { a += 2 * _v; }

class B : public A {
  public:
    B(int v)
        : A(v) {}
    ~B() override;
};

B::~B() {}

nlc_test("basic_usage") {
    nlc_check(a == 0);
    {
        [[maybe_unused]] auto ptr = make_unique<A>(*std_allocator(), 2);
        nlc_check(a == 2);
    }
    nlc_check(a == 6);
    {
        auto ptr = make_unique<A>(*std_allocator(), 3);
        nlc_check(a == 9);
        ptr = make_unique<B>(*std_allocator(), 1);
        nlc_check(a == 16);
    }
    nlc_check(a == 18);

    {
        auto ptr = make_unique<int>(*std_allocator(), 12);
        nlc_check(ptr.get() != nullptr);
        ptr.reset();
        nlc_check(ptr.get() == nullptr);
    }
}

nlc_test("nullptr constructor & comparisons") {
    unique_ptr<int> null_ptr { nullptr };
    nlc_check(null_ptr == nullptr);

    auto non_null_ptr = make_unique<int>(*std_allocator(), 12);
    nlc_check(non_null_ptr != nullptr);

    non_null_ptr = nullptr;
    nlc_check(non_null_ptr == nullptr);
}
