/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/test.hpp>

#include "../utils.hpp"

using namespace nlc;

nlc_test("basic_usage") {
    standard_allocator malloc;

    auto foo = string(malloc, "foo", bytes<usize>(3));
    auto bar = string(malloc, "bar", bytes<usize>(3));

    nlc_check(hash(foo) == hash("foo", 3));
    nlc_check(hash(foo) != hash(bar));
    nlc_check(hash(foo) == hash(string_view(foo)));
    nlc_check(hash(bar) == hash(string_view(bar)));
}
