/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/fundamentals/allocator.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/test.hpp>

#include "../utils.hpp"

using namespace nlc;

static_assert(1_align == align_of<u8>);
static_assert(2_align == align_of<u16>);
static_assert(4_align == align_of<u32>);
static_assert(8_align == align_of<u64>);

namespace {
struct alignas(256) Foo {
    u8 bytes[1024];
};
}  // namespace

static_assert(align_of<Foo> == alignment(256));
static_assert(align_of<Foo>.v == 256);

nlc_test("basic_usage") {
    auto allocator = std_allocator();
    auto f = allocator->allocate<Foo>();
    nlc_check((reinterpret_cast<usize>(&f[0]) & 0xFF) == 0);
    for (auto & b : f[0].bytes) {
        b = 0x34;
    }
    allocator->free(f);
}

nlc_test("safety") {
    auto allocator = std_allocator();
    auto f = allocator->allocate<Foo>(1);
    defer { allocator->free(f); };

    nlc_test_case("different allocator free") {
        standard_allocator other;
        nlc_expect_assert(other.free(f));
    };

    nlc_test_case("different allocator reallocate") {
        standard_allocator other;
        nlc_expect_assert(other.reallocate(f, 2));
    };
}
