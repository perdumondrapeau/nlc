/* Copyright © 2025-2025
Alexis A. D. COLIN,
Antoine VUGLIANO,
Gaëtan CHAMPARNAUD,
Geoffrey L. TOURON,
Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/fundamentals/allocator/monolithic.hpp>
#include <nlc/test.hpp>

using namespace nlc;

constexpr auto size = 35'678_usize;

nlc_test("basic_usage") {
    monolithic_allocator alloc;

    auto const m = alloc.allocate<byte>(size);

    nlc_check(m.size() == size);

    m[0] = 13;
    m[size - 1] = 12;

    alloc.free(m);
}

nlc_test("realloc") {
    constexpr auto max = 1'000'000'000;
    monolithic_allocator alloc { max };

    auto m = alloc.allocate<byte>(size);

    m[0] = 13;
    m[size - 1] = 12;

    nlc_test_case("bigger") {
        m = alloc.reallocate(m, size * 2);
        nlc_check(m.size() == size * 2);

        m[size * 2 - 1] = 131;
    };

    nlc_test_case("smaller") {
        m = alloc.reallocate(m, size / 2);
        nlc_check(m.size() == size / 2);

        m[size / 2 - 1] = 131;
    };

    nlc_test_case("same size") {
        m = alloc.reallocate(m, size);
        nlc_check(m.size() == size);
    };

    nlc_test_case("too big") { nlc_expect_assert(alloc.reallocate(m, size * max)); };

    m[0] = 13;
    m[m.size() - 1] = 12;

    alloc.free(m);
}
