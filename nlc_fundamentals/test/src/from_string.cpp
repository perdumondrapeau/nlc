/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/fundamentals/from_string.hpp>
#include <nlc/meta/numeric_limits.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test("int") {
    nlc_check(from_string<i32>("0") == 0);
    nlc_check(from_string<i32>("12") == 12);
    nlc_check(from_string<i32>("-41") == -41);
    nlc_check(from_string<i32>("3.") == null);
    nlc_check(from_string<i32>("3.6") == null);
    nlc_check(from_string<i32>(".6") == null);
    nlc_check(from_string<i32>("") == null);
    nlc_check(from_string<i32>("32a") == null);
    nlc_check(from_string<i32>("-2147483648") == nlc::meta::limits<i32>::min);
    nlc_check(from_string<i32>("2147483647") == nlc::meta::limits<i32>::max);

    nlc_check(from_string<i64>("-9223372036854775808") == nlc::meta::limits<i64>::min);
    nlc_check(from_string<i64>("9223372036854775807") == nlc::meta::limits<i64>::max);
}

nlc_test("unsigned int") {
    nlc_check(from_string<u32>("0") == 0u);
    nlc_check(from_string<u32>("12") == 12u);
    nlc_check(from_string<u32>("-41") == null);
    nlc_check(from_string<u32>("3.") == null);
    nlc_check(from_string<u32>("3.6") == null);
    nlc_check(from_string<u32>(".6") == null);
    nlc_check(from_string<u32>("") == null);
    nlc_check(from_string<u32>("32a") == null);
    nlc_check(from_string<u32>("4294967295") == nlc::meta::limits<u32>::max);

    nlc_check(from_string<u64>("18446744073709551615") == nlc::meta::limits<u64>::max);
}

nlc_test("float") {
    nlc_check(from_string<f32>("0") == 0.f);
    nlc_check(from_string<f32>("12") == 12.f);
    nlc_check(from_string<f32>("-41") == -41.f);
    nlc_check(from_string<f32>("3.") == 3.f);
    nlc_check(from_string<f32>("3.6") == 3.6f);
    nlc_check(from_string<f32>(".6") == 0.6f);
    nlc_check(from_string<f32>("") == null);
    nlc_check(from_string<f32>("32a") == null);
}
