/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/fundamentals/byte_stream.hpp>
#include <nlc/fundamentals/hash_stream.hpp>
#include <nlc/test.hpp>

#include "../utils.hpp"

nlc_test("integral types") {
    auto & allocator = *std_allocator();
    nlc::hash_stream hasher { allocator };

    hasher.write(1_u8);
    hasher.write(1_u16);
    hasher.write(1_u32);
    hasher.write(1_u64);
    hasher.write(1_i8);
    hasher.write(1_i16);
    hasher.write(1_i32);
    hasher.write(1_i64);
    hasher.write(true);
    hasher.write('c');
}

struct dummy {
    int a = 0;
};

static auto to_byte_stream(nlc::output_byte_stream & out, dummy const & d) -> void {
    out.write(d.a);
}
static auto from_byte_stream(nlc::input_byte_stream & out, dummy & d) -> void { out.read(d.a); }

// This tests only demonstrates that the API for custom streaming works and gets called correctly
// It only supports streaming one integer back and forth in the most simplistic way
static int temp = -1;

struct dummy_out : public nlc::output_byte_stream {
    using nlc::output_byte_stream::write;

    auto write(nlc::span<byte const> bytes) -> void override {
        auto ptr = reinterpret_cast<int const *>(bytes.ptr());
        temp = *ptr;
    }
};

struct dummy_in : public nlc::input_byte_stream {
    using nlc::input_byte_stream::read;

    auto read(nlc::span<byte> bytes) -> void override {
        auto ptr = reinterpret_cast<int *>(bytes.ptr());
        *ptr = temp;
    }
};

nlc_test("custom streaming") {
    dummy d { .a = 1312 };

    dummy_out out;
    nlc_check(temp == -1);
    out.write(d);
    nlc_check(d.a == 1312);
    nlc_check(temp == 1312);
    d.a = 0;

    dummy_in in;
    in.read(d);
    nlc_check(d.a == 1312);
    nlc_check(temp == 1312);
}
