/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/read_write_file.hpp>
#include <nlc/test.hpp>

#include <memory.h>
#include <stdio.h>

constexpr auto filename = "data/read_write_1.txt";
constexpr nlc::string_view expected_data = "Hello world! Viva la revolution!";

nlc_test("read & write file") {
    nlc::write_file(filename,
                    { reinterpret_cast<byte const *>(expected_data.ptr()), expected_data.size().v });

    auto std_alloc = nlc::standard_allocator {};
    auto const data = read_file(std_alloc, filename);
    nlc_assert_msg(data->size() == nlc::length(expected_data),
                   nlc_dump_var(data->size()),
                   nlc_dump_var(nlc::string_view(reinterpret_cast<const char *>(data->begin()),
                                                 reinterpret_cast<const char *>(data->end()))),
                   nlc_dump_var(expected_data));
    nlc_check(memcmp(data->begin(), expected_data.ptr(), data->size()) == 0);

    remove(filename);
}
