/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/fundamentals/function.hpp>
#include <nlc/test.hpp>

#include "../utils.hpp"

using namespace nlc;

static auto nothing_ret_4() { return 4; }

static auto destr_count = 0;
template<auto padding> struct Destr final {
    int value;
    u8 _padding[padding] = { 0 };
    ~Destr() { ++destr_count; }
    Destr(Destr const &) = default;
    Destr(int v)
        : value { v } {}
};

nlc_test("function") {
    auto no_alloc = no_allocator {};
    auto std_alloc = standard_allocator {};

    {
        nlc_check(nothing_ret_4() == 4);
        auto foo = function(no_alloc, nothing_ret_4);
        nlc_check(foo() == 4);
    }

    {
        auto l = [](auto a, auto b) { return a + b; };
        static_assert(sizeof(l) < sizeof(void *));
        auto foo = function<int(int, int)>(no_alloc, l);
        nlc_check(foo(4, 5) == 9);
    }

    {
        auto foo = function<int(int, int)>(no_alloc, [](auto a, auto b) { return a * b; });
        nlc_check(foo(3, 5) == 15);
    }

    auto i = 4;
    {
        auto foo = function<int(int)>(no_alloc, [&i](auto a) { return i * a; });
        nlc_check(foo(5) == 20);

        auto bar = nlc_move(foo);
        nlc_check(bar(6) == 24);

        auto blah = function<int(int)>(no_alloc, [](auto a) { return a; });
        blah = nlc_move(bar);
        nlc_check(blah(2) == 8);
    }

    // Function that return a reference
    {
        int c = 8;
        auto foo = function<int &(int &)>(no_alloc, [](auto & a) -> int & { return a; });
        nlc_check(foo(c) == 8);
        foo(c) = 4;
        nlc_check(c == 4);
    }

    auto const d1 = 8.;
    auto const d2 = 4.;
    {
        auto l = [d1, &d2](auto d3) { return d1 * d2 - d3; };
        static_assert(sizeof(l) > sizeof(void *));
        auto foo = function<double(double)>(std_alloc, l);
        nlc_check(foo(20.) == 12.);
    }

    [[maybe_unused]] auto expected_destr_count = 0;
    {
        auto foo = function<int(int)>(no_alloc, [d = Destr<1> { 3 }](auto v) { return v * d.value; });
        expected_destr_count = destr_count + 1;
        nlc_check(foo(4) == 12);
    }
    nlc_check(destr_count == expected_destr_count);

    {
        auto foo =
            function<int(int)>(std_alloc, [d = Destr<100> { 5 }](auto v) { return d.value - v; });
        expected_destr_count = destr_count + 1;
        nlc_check(foo(7) == -2);

        [[maybe_unused]] auto view = function_view(foo);
        nlc_check(view(19) == -14);

        auto bar = nlc_move(foo);
        nlc_check(bar(2) == 3);

        auto blah =
            function<int(int)>(std_alloc, [d = Destr<100> { 8 }](auto v) { return d.value - v; });
        nlc_check(blah(12) == -4);
        blah = nlc_move(bar);
        expected_destr_count = destr_count + 1;
        nlc_check(blah(12) == -7);
    }
    nlc_check(destr_count == expected_destr_count);

    {
        auto const f = function<int(int)> { [](auto a) { return 3 * a; } };
        [[maybe_unused]] auto const v = function_view(f);
        nlc_check(v(4) == 12);
    }
}
