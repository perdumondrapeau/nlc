/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/fundamentals/string_stream.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test("basic_usage") {
    auto std_alloc = standard_allocator();

    {
        auto ss = string_stream { std_alloc };
        nlc_check(string_view(ss) == "");
        ss << 'C';
        nlc_check(string_view(ss) == "C");
        ss << "oucou";
        nlc_check(string_view(ss) == "Coucou");
        ss << " à tout·e·s !";
        nlc_check(string_view(ss) == "Coucou à tout·e·s !");
    }

    {
        auto ss = string_stream { std_alloc };
        ss << i8(3);
        ss << i16(-35);
        ss << i32(21);
        ss << i64(-42);
        ss << u8(6);
        ss << u16(27);
        ss << u32(458);
        ss << u64(1337);
        ss << ' ';
        ss << true;
        ss << " string ";
        nlc::string cut_string(std_alloc, "cut string");
        ss.append(cut_string.c_str(), static_cast<usize>(cut_string.size()) - 1u);  // cut strin
        ss << nlc::string_view(" string view");
        nlc_check(string_view(ss) == "3-3521-426274581337 true string cut strin string view");
    }

    {
        nlc::string_view const buffer { "Hello world!" };
        auto ss = string_stream { std_alloc };
        ss << buffer.substring(0u, 4u);
        nlc_check(string_view(ss) == "Hell");
        ss << buffer.substring(4u, buffer.size().v);
        nlc_check(string_view(ss) == "Hello world!");
        ss << buffer.substring(4u, 7u);
        nlc_check(string_view(ss) == "Hello world!o w");
    }
}

nlc_test("clear") {
    auto std_alloc = standard_allocator();
    nlc::string_stream sstr { std_alloc };
    sstr << 3;
    nlc_check(sstr.size() > 0u);

    auto const capacity_before_clear = sstr.capacity();
    sstr.clear();
    nlc_check(sstr.size() == 0u);
    nlc_check(sstr.capacity() == capacity_before_clear);

    sstr.reserve(256u);
    auto const capacity_before_clear_reduce_memory = sstr.capacity();
    sstr << 128;
    sstr.clear_reduce_memory();
    nlc_check(sstr.size() == 0u);
    nlc_check(sstr.capacity() < capacity_before_clear_reduce_memory);

    sstr << 1337;
    nlc_check(sstr.size() > 0u);
    nlc_check(sstr.capacity() > 0u);
}
