/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/fundamentals/align.hpp>

using namespace nlc;

static_assert(ceil_power_of_two(0u) == 0u);
static_assert(ceil_power_of_two(1u) == 1u);
static_assert(ceil_power_of_two(2u) == 2u);
static_assert(ceil_power_of_two(3u) == 4u);
static_assert(ceil_power_of_two(17u) == 32u);
static_assert(ceil_power_of_two(72u) == 128u);
static_assert(ceil_power_of_two(256u) == 256u);
static_assert(ceil_power_of_two(2000u) == 2048u);

static_assert(ceil_to_multiple(0u, 1u) == 0u);
static_assert(ceil_to_multiple(1u, 1u) == 1u);
static_assert(ceil_to_multiple(0u, 3u) == 0u);
static_assert(ceil_to_multiple(1u, 3u) == 3u);
static_assert(ceil_to_multiple(16u, 3u) == 18u);
