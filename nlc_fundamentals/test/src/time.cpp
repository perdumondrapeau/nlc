/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/fundamentals/time.hpp>
#include <nlc/sanity/platforms.hpp>
#include <nlc/test.hpp>

#if NLC_OS_WINDOWS
extern "C" void Sleep(u32 milli);
#else
#    include <unistd.h>
#endif

using namespace nlc;
using namespace nlc::time;

nlc_test("now") {
    auto const start = now();

    nlc::print("start is");
    nlc::print(seconds(start), "s");
    nlc::print(milliseconds(start), "ms");
    nlc::print(microseconds(start), "us");
    nlc::print(nanoseconds(start), "ns");

    auto const end = now();
    nlc_check(end >= start);
    nlc::print("end is ", microseconds(end), "us");
    nlc::print("sum is ", microseconds(end + start), "us");
    auto const diff = end - start;
    nlc::print("diff is ", microseconds(diff), "us");
    nlc::print("diff*2 is ", microseconds(diff * 2u), "us");
    nlc::print("diff/2 is ", microseconds(diff / 2u), "us");
}

nlc_test("sleep") {
    auto const start = now();

#if NLC_OS_WINDOWS
    Sleep(80);
#else
    usleep(80 * 1000);
#endif

    auto const end = now();
    nlc_check(end - start > 50_ms);  // can't assume sleep is precise
}

nlc_test("conversion") {
    nlc_check(rm_unit(123_ns) == 123u);
    nlc_check(rm_unit(123_us) == 123u * ns_per_us);
    nlc_check(rm_unit(123_ms) == 123u * ns_per_ms);
    nlc_check(rm_unit(123_s) == 123u * ns_per_s);
}

nlc_test("time from scalar") {
    nlc_check(from_seconds(2.4f) == 2400_ms);
    nlc_check(from_milliseconds(2.4f) == 2400_us);
    nlc_check(from_microseconds(2.4f) == 2400_ns);
    nlc_check(from_nanoseconds(2.4f) == 2_ns);  // Smallest dt is 1ns

    nlc_check(seconds<float>(from_seconds(1.25f)) == 1.25f);
    nlc_check(milliseconds<float>(from_milliseconds(1.25f)) == 1.25f);
    nlc_check(microseconds<float>(from_microseconds(1.25f)) == 1.25f);
    nlc_check(nanoseconds<float>(from_nanoseconds(1.25f)) == 1.f);  // Smallest dt is 1ns
}
