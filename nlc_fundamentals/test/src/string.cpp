/* Copyright © 2019-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/test.hpp>
#include <string.h>

using namespace nlc;

nlc_test("empty strings") {
    auto check_empty_string = [](string const & s) {
        nlc_check(s.is_empty());
        nlc_check(rm_unit(s.size()) == 0u);
        nlc_check(s.c_str() != nullptr);
        nlc_check(s.begin() == s.end());

        nlc_check(strlen(s.c_str()) == 0);
        nlc_check(strcmp(s.c_str(), "") == 0);

        auto const v = string_view(s);
        nlc_check(v.is_empty());
        nlc_check(rm_unit(v.size()) == 0u);
        nlc_check(v.ptr() == s.c_str());
        nlc_check(v.begin() == v.end());
    };

    auto std_alloc = standard_allocator {};

    auto null_cstr = static_cast<char const *>(nullptr);

    check_empty_string(string::empty);
    check_empty_string(string { std_alloc, "" });
    check_empty_string(string { std_alloc, null_cstr, null_cstr });
    check_empty_string(string { std_alloc, string_view {} });
    check_empty_string(string { std_alloc, string_view::from_c_str(nullptr) });
    check_empty_string(string::from_c_str(std_alloc, nullptr));
    check_empty_string(string::from_c_str(std_alloc, ""));
    check_empty_string(string::from_c_str(std_alloc, null_cstr));
}
