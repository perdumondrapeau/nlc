/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/range.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/dialect/utf8.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/string.hpp>
#include <nlc/fundamentals/string_manip.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test_todo("starts_with");

nlc_test("ends_with") {
    nlc_check(ends_with("foo.txt", ".txt"));
    nlc_check(ends_with("bababac", "babac"));
    nlc_check(ends_with("bababac", "baba") == false);
}

nlc_test("levenshtein_distance") {
    constexpr nlc::string_view foo { "foo" };
    constexpr nlc::string_view foo_bis { "foo" };
    nlc::standard_allocator allocator;

#define check_levenshtein(a, b, d)                            \
    nlc_check_msg(levenshtein_distance(allocator, a, b) == d, \
                  "expected ",                                \
                  d,                                          \
                  ", got ",                                   \
                  levenshtein_distance(allocator, a, b));     \
    nlc_check_msg(levenshtein_distance(allocator, b, a) == d, \
                  "expected ",                                \
                  d,                                          \
                  ", got ",                                   \
                  levenshtein_distance(allocator, b, a))

    check_levenshtein(foo, foo, 0u);
    check_levenshtein(foo, foo_bis, 0u);
    check_levenshtein("", "", 0u);
    check_levenshtein("", "foo", 3u);
    check_levenshtein("foo", "", 3u);
    check_levenshtein("foo", "foo", 0u);
    check_levenshtein("foo", "foot", 1u);
    check_levenshtein("foo", "oo", 1u);
    check_levenshtein("foo", "boo", 1u);
    check_levenshtein("foo", "bar", 3u);
    check_levenshtein("foo", "barbaz", 6u);
    check_levenshtein(R"("¯\_(ツ)_/¯")", R"("¯\_(ツツ)_/¯")", 1u);
    check_levenshtein(R"("don't forget

    line breaks!")",
                      R"("don't forget



    line breaks!")",
                      2u);

#undef check_levenshtein
}

nlc_test_todo("find");
nlc_test_todo("find_last");
nlc_test_todo("contains");
nlc_test_todo("get_codepoint_at");

nlc_test("get_splice_count") {
    struct InputsAndResults {
        nlc::string_view string;
        nlc::codepoint_type seprator;
        usize expected_result;
    };

    InputsAndResults const inputs_and_expected_results[] = {
        { "", nlc::codepoint_type { 'a' }, 0u },
        { "test", nlc::codepoint_type { 'a' }, 1u },
        { "testa", nlc::codepoint_type { 'a' }, 2u },
        { "test.dummy", nlc::codepoint_type { '.' }, 2u },
        { ";test;dummy;", nlc::codepoint(";"), 4u },
        { "¯\\_(ツ)_/¯", nlc::codepoint("ツ"), 2u },
        { "ツ¯ツ\\_(ツ)_/¯ツ", nlc::codepoint("ツ"), 5u },
        { "Æ¯ツ\\ਔ_(ß)_ʥ/¯ƕ", nlc::codepoint("ʥ"), 2u },
        { "..........", nlc::codepoint_type { '.' }, 11u },
    };

    for (auto const & [str, sep, count] : inputs_and_expected_results) {
        auto const res { nlc::get_splice_count(str, sep) };
        nlc_check_msg(res == count, "expected ", count, " slice(s), got ", res);
    }
}

nlc_test("get_first_splice") {
    struct InputsAndResults {
        nlc::string_view string;
        nlc::codepoint_type seprator;
        nlc::string_view expected_result;
    };

    InputsAndResults const inputs_and_expected_results[] = {
        { "", nlc::codepoint_type { 'a' }, "" },
        { "test", nlc::codepoint_type { 'a' }, "test" },
        { "test", nlc::codepoint_type { 's' }, "te" },
        { ".test", nlc::codepoint_type { '.' }, "" },
        { "test.", nlc::codepoint_type { '.' }, "test" },
        { "..", nlc::codepoint_type { '.' }, "" },
        { "Æ¯ツ\\ਔ_(ß)_ʥ/¯ƕ", nlc::codepoint("ʥ"), "Æ¯ツ\\ਔ_(ß)_" },
    };

    for (auto const & [str, sep, res] : inputs_and_expected_results) {
        auto const slice = nlc::get_first_splice(str, sep);

        char sep_buffer[sizeof(nlc::codepoint_type::underlying_type)];
        char const * const sep_end = nlc::cat_to_string(sep_buffer, sep);
        nlc_check_msg(slice == res,
                      "expected '",
                      res,
                      "' when splitting '",
                      str,
                      "' with separtor '",
                      nlc::string_view { sep_buffer, static_cast<usize>(sep_end - sep_buffer) },
                      "', got '",
                      slice,
                      "'");
    }
}

constexpr usize max_splice_count { 10u };
struct SplitStringInputAndResult final {
    nlc::string_view string;
    nlc::codepoint_type separator;
    nlc::string_view expected_results[max_splice_count];
    usize expected_result_count;
};

static auto check_splices(nlc::codepoint_type const separator,
                          span<string_view const> expected_result,
                          span<string_view const> const splices) -> void {
    nlc_check_msg(splices.size() == expected_result.size(),
                  "expected ",
                  expected_result.size(),
                  " slices, got ",
                  splices.size());

    for (auto i : nlc::range(0u, splices.size())) {
        auto const slice = splices[i];
        nlc_check(nlc::contains(slice, separator) == false);
        nlc_check_msg(slice == expected_result[i],
                      "expected '",
                      expected_result[i],
                      "' for slice ",
                      i,
                      ", got '",
                      slice,
                      "'");
    }
}

nlc_test("[try_]split_string - success") {
    nlc::standard_allocator allocator;

    SplitStringInputAndResult const inputs_and_expected_results[] = {
        { "", nlc::codepoint_type { 'a' }, { "" }, 0u },
        { "test", nlc::codepoint_type { 'a' }, { "test" }, 1u },
        { "testa", nlc::codepoint_type { 'a' }, { "test", "" }, 2u },
        { "test.dummy", nlc::codepoint_type { '.' }, { "test", "dummy" }, 2u },
        { "test..dummy", nlc::codepoint_type { '.' }, { "test", "", "dummy" }, 3u },
        { ";test;dummy;", nlc::codepoint(";"), { "", "test", "dummy", "" }, 4u },
        { "¯\\_(ツ)_/¯", nlc::codepoint("ツ"), { "¯\\_(", ")_/¯" }, 2u },
        { "ツ¯ツ\\_(ツ)_/¯ツ", nlc::codepoint("ツ"), { "", "¯", "\\_(", ")_/¯", "" }, 5u },
        { "Æ¯ツ\\ਔ_(ß)_ʥ/¯ƕ", nlc::codepoint("ʥ"), { "Æ¯ツ\\ਔ_(ß)_", "/¯ƕ" }, 2u },
    };

    nlc::string_view buffer[max_splice_count];

    for (auto const & [str, sep, res, res_count] : inputs_and_expected_results) {
        auto const split_string_res { nlc::split_string(str, sep, buffer) };
        check_splices(sep, span { res, res_count }, split_string_res);

        auto const try_split_string_res { nlc::try_split_string(str, sep, buffer) };
        nlc_assert(try_split_string_res.succeeded());
        check_splices(sep, span { res, res_count }, try_split_string_res.get());
    }
}

nlc_test("[try_]split_string - failure") {
    nlc::string_view unsufficient_buffer[2];
    constexpr nlc::string_view separators_string { "........" };
    auto const dot { nlc::codepoint(".") };

    nlc_expect_assert(nlc::split_string(separators_string, dot, unsufficient_buffer));

    auto const expected_fail { nlc::try_split_string(separators_string, dot, unsufficient_buffer) };
    nlc_check(expected_fail.failed_with<nlc::buffer_too_small>());
}
