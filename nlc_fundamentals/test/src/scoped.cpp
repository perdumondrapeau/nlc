/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/scoped.hpp>
#include <nlc/test.hpp>

using namespace nlc;

static auto counter = 0;
static auto global_value = 0;

static auto foo(int & v) {
    ++counter;
    global_value = v;
}

struct Bar final {
    Bar(int a)
        : _a(a) {}
    Bar(Bar && a)
        : _a(a._a) {
        a._a /= 2;
    }
    ~Bar() {
        ++counter;
        global_value = _a;
    }
    int _a;
};

nlc_test("basic_usage") {
    auto std_alloc = standard_allocator {};

    {  // Test direct call
        counter = 0;
        global_value = 0;
        auto s = scoped(3, foo);
        nlc_assert(counter == 0);
        nlc_assert(global_value == 0);
    }
    nlc_assert(counter == 1);
    nlc_assert(global_value == 3);

    {  // Test call with lambda
        counter = 0;
        global_value = 0;
        auto s = scoped(5, [](auto & v) { foo(v); });
        nlc_assert(counter == 0);
        nlc_assert(global_value == 0);
    }
    nlc_assert(counter == 1);
    nlc_assert(global_value == 5);

    {  // Test call with payload
        counter = 0;
        global_value = 0;
        auto s = scoped(7, [a = 3](auto & v) {
            foo(v);
            global_value *= a;
        });
        nlc_assert(counter == 0);
        nlc_assert(global_value == 0);
    }
    nlc_assert(counter == 1);
    nlc_assert(global_value == 21);

    {  // Test call with payload
        counter = 0;
        global_value = 0;
        auto a = 4;
        auto s = scoped(9, [&a](auto & v) {
            foo(v);
            global_value *= a;
        });

        a = 2;
        nlc_assert(counter == 0);
        nlc_assert(global_value == 0);
    }
    nlc_assert(counter == 1);
    nlc_assert(global_value == 18);

    // {  // Test call with too large payload
    //     counter = 0;
    //     value = 0;
    //     auto a = 4;
    //     auto b = 6;
    //     auto s = scoped(3, [&a, &b](auto v) {
    //         foo(v);
    //         value *= a + b;
    //     });
    //
    //     a = 2;
    //     Assert(counter == 0);
    //     Assert(value == 0);
    // }
    // Assert(counter == 1);
    // Assert(value == 24);

    {  // Test call with too large payload
        counter = 0;
        global_value = 0;
        auto a = 4;
        auto b = 6;
        auto s = scoped(3, function<void(int &)>(std_alloc, [&a, &b](auto v) {
                            foo(v);
                            global_value *= a + b;
                        }));

        a = 2;
        nlc_assert(counter == 0);
        nlc_assert(global_value == 0);
    }
    nlc_assert(counter == 1);
    nlc_assert(global_value == 24);

    {  // Call destructor
        counter = 0;
        global_value = 0;
        auto s = scoped<Bar>(4, [](auto & b) { b.~Bar(); });
        nlc_assert(counter == 0);
        nlc_assert(global_value == 0);
    }
    nlc_assert(counter == 1);
    nlc_assert(global_value == 4);

    {  // Call destructor
        counter = 0;
        global_value = 0;
        auto s = scoped(Bar { 6 }, [](auto & b) { b.~Bar(); });
        nlc_assert(counter == 1);
        nlc_assert(global_value == 3);
    }
    nlc_assert(counter == 2);
    nlc_assert(global_value == 6);

    {  // Move scoped
        counter = 0;
        global_value = 0;
        auto s1 = scoped(5, foo);
        {
            auto s2 = scoped(7, foo);
            nlc_assert(counter == 0);
            nlc_assert(global_value == 0);
            s1 = nlc_move(s2);
            nlc_assert(counter == 1);
            nlc_assert(global_value == 5);
            counter = 0;
            global_value = 0;
        }
        nlc_assert(counter == 0);
        nlc_assert(global_value == 0);
    }
    nlc_assert(counter == 1);
    nlc_assert(global_value == 7);
}
