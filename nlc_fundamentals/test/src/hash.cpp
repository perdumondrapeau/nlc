/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/fundamentals/hash.hpp>
#include <nlc/meta/id.hpp>
#include <nlc/test.hpp>

using namespace nlc;

// Hash defined as external function
struct Foo {
    u32 a = 123;
};

[[nodiscard]] constexpr auto hash(Foo const & foo) { return hash(foo.a); }

// Hash defined as method
struct Bar {
    u32 a = 123;

    [[nodiscard]] constexpr inline auto hash() const { return nlc::hash(a); }
};

// Hash defined as both method and external function -> the external function prevails
struct FooBar {
    u32 a = 123;

    [[nodiscard]] inline auto hash() const {
        nlc_check(false);
        return nlc::hash(a);
    }
};

[[maybe_unused]] [[nodiscard]] static auto hash(const FooBar & fooBar) { return hash(fooBar.a); }

namespace impl {
struct FooIdDef final {
    using underlying_type = int;
};
}  // namespace impl
using FooId = meta::id<::impl::FooIdDef>;

nlc_test("basic_usage") {
    hash_result a[] = {
        hash(i8(123)), hash(u8(123)),  hash(i16(123)), hash(u16(123)),
        hash(123),     hash(u32(123)), hash(i64(123)), hash(u64(123)),
    };

    for ([[maybe_unused]] auto b : a) {
        nlc_check(b == hash(123));
    }

    nlc_check(hash(Foo {}) == hash(123));
    nlc_check(hash(Bar {}) == hash(123));
    nlc_check(hash(FooBar {}) == hash(123));

    [[maybe_unused]] u32 b = 123;
    nlc_check(hash(&b) != hash(b));

    constexpr FooId id { 123 };
    nlc_check(hash(id) == hash(123));
}
