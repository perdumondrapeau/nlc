/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/memory.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/hash_stream.hpp>
#include <nlc/fundamentals/random.hpp>
#include <nlc/test.hpp>

using namespace nlc;

nlc_test("unit f32") {
    xoroshiro128plus rng { 0xA1C2A3B };

    for (int i = 0; i < 2000; ++i) {
        [[maybe_unused]] auto const f = rng.unit_f32();
        nlc_assert(f >= 0.f && f <= 1.f);
    }
}

nlc_test("unit f64") {
    xoroshiro128plus rng { 0xA1C2A3B };

    for (int i = 0; i < 2000; ++i) {
        [[maybe_unused]] auto const f = rng.unit_f64();
        nlc_assert(f >= 0. && f <= 1.);
    }
}

nlc_test("basic_usage") {
    xoroshiro128plus rng { 123 };
    u32 hist[10] = { 0 };

    for (u32 i = 0; i < 20000; ++i) {
        auto const idx = rng.less_than(10);
        nlc_assert(idx < 10);
        ++hist[idx];
    }

    nlc_assert(hist[0] == 2013);
    nlc_assert(hist[1] == 1975);
    nlc_assert(hist[2] == 1990);
    nlc_assert(hist[3] == 1973);
    nlc_assert(hist[4] == 2010);
    nlc_assert(hist[5] == 2025);
    nlc_assert(hist[6] == 2004);
    nlc_assert(hist[7] == 2064);
    nlc_assert(hist[8] == 2005);
    nlc_assert(hist[9] == 1941);

    for (int i = 0; i < 2000; ++i) {
        [[maybe_unused]] auto const n = rng.in_range(i64(-123), i64(456));
        nlc_assert(n >= -123 && n < 456);
    }

    for (int i = 0; i < 2000; ++i) {
        [[maybe_unused]] auto const n = rng.in_range(u64(123), u64(456));
        nlc_assert(n >= 123ull && n < 456ull);
    }

    for (int i = 0; i < 2000; ++i) {
        [[maybe_unused]] auto const n = rng.in_range(u32(123), u32(456));
        nlc_assert(n >= 123u && n < 456u);
    }

    for (int i = 0; i < 2000; ++i) {
        [[maybe_unused]] auto const n = rng.in_range(123u, 456u);
        nlc_assert(n >= 123u && n < 456u);
    }

    for (int i = 0; i < 2000; ++i) {
        [[maybe_unused]] auto const f = rng.in_range(-12.3f, 45.6f);
        nlc_assert(f >= -12.3f && f < 45.6f);
    }
}

nlc_test("floating-point determinism") {
    nlc::standard_allocator allocator;
    nlc::hash_stream hasher { allocator };
    xoroshiro128plus rng { 1312 };

    u32 u1;
    u64 u2;

    // First make sure the unit functions give bit-level identical results
    for (int i = 0; i < 20'000; ++i) {
        auto f = rng.unit_f32();
        nlc::memcpy(&u1, &f, sizeof(f));
        hasher.write(u1);

        auto d = rng.unit_f64();
        nlc::memcpy(&u2, &d, sizeof(d));
        hasher.write(u2);
    }

    auto digest = hasher.digest().value();
    nlc_check_msg(digest == 2703779916109685780u, "digest=", digest);

    // We noticed on aarch64 that, depending on compilers and optimization level, `in_range` outputs
    // could differ by one bit due to floating point settings, probably FMA usage
    for (int i = 0; i < 20'000; ++i) {
        auto f = rng.in_range(6.f, 12.f);
        nlc::memcpy(&u1, &f, sizeof(f));
        hasher.write(u1);

        auto d = rng.unit_f64();
        nlc::memcpy(&u2, &d, sizeof(d));
        hasher.write(u2);
    }

    digest = hasher.digest().value();
    nlc_check_msg(digest == 13201327520217429717u, "digest=", digest);
}
