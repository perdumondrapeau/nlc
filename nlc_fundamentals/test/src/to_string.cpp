/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/from_string.hpp>
#include <nlc/fundamentals/to_string.hpp>
#include <nlc/test.hpp>

auto test = [](auto & alloc, auto n) {
    using T = decltype(n);
    auto s = nlc::to_string(alloc, n);
    auto res = nlc::from_string<T>(s);
    nlc_check(res == n);
};

nlc_test("basic_usage") {
    auto std_alloc = nlc::standard_allocator {};

    test(std_alloc, 43);
    test(std_alloc, -3);
    test(std_alloc, 0);
    test(std_alloc, 43u);
    test(std_alloc, 0u);
    test(std_alloc, 1.42);
    test(std_alloc, 1.42f);
}
