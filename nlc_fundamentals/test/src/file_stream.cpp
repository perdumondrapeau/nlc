/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/fundamentals/allocator/standard.hpp>
#include <nlc/fundamentals/file_stream.hpp>
#include <nlc/fundamentals/read_write_file.hpp>
#include <nlc/test.hpp>

#include <memory.h>

using namespace nlc;

constexpr char expected_data[] = "Hello world! Viva la revolution!";
constexpr auto expected_data_size = sizeof(expected_data) - 1;

constexpr nlc::string_view filename = "data/output_file_stream_test.bin";

nlc_test("basics") {
    auto std_alloc = standard_allocator {};

    {
        auto res = output_file_stream::open(std_alloc, filename, 1024);
#ifndef NDEBUG
        nlc_check_msg(res.succeeded(), "error: ", res.get_error_name());
#else
        nlc_check(res.succeeded());
#endif
        auto & out = res.get();
        defer { nlc_check(out.any_error_occurred() == false); };

        out.write(expected_data_size);
        out.write(expected_data);
    }

    auto data = nlc::read_file(std_alloc, filename);
    nlc_check(*reinterpret_cast<usize *>(data->begin()) == expected_data_size);
    nlc_check(memcmp(data->begin() + sizeof(usize), expected_data, data->size() - sizeof(usize)) == 0);

    {
        auto res = input_file_stream::open(std_alloc, filename, 1024);
        auto & in = res.get();
#ifndef NDEBUG
        nlc_check_msg(res.succeeded(), "error: ", res.get_error_name());
#else
        nlc_check(res.succeeded());
#endif
        defer { nlc_check(in.any_error_occurred() == false); };

        usize size = 1312;
        in.read(size);
        nlc_check_msg(size == expected_data_size, size);

        auto s = std_alloc.allocate(size, 1);
        defer { std_alloc.free(s); };
        in.read(s);

        nlc_check(memcmp(s.begin(), expected_data, s.size()) == 0);
    }
}

nlc_test("reset") {
    auto std_alloc = standard_allocator {};

    {
        auto res = output_file_stream::open(std_alloc, filename, 1024);
        nlc_check(res.succeeded());
        auto & out = res.get();
        out.write("something wrong");
        out.reset();
        out.write(expected_data);
        nlc_check(out.any_error_occurred() == false);
    }

    auto data = nlc::read_file(std_alloc, filename);
    nlc_check(memcmp(data->begin(), expected_data, data->size()) == 0);
}

#if NLC_OS_WINDOWS
nlc_test("path_not_found") {
    auto std_alloc = standard_allocator {};

    {
        auto res = output_file_stream::open(std_alloc, "Z:\\this path does not exist", 1024);
        nlc_check(res.failed());
        nlc_check(res.failed_with<path_not_found>());
    }

    {
        auto res = input_file_stream::open(std_alloc, "Z:\\this path does not exist", 1024);
        nlc_check(res.failed());
        nlc_check(res.failed_with<path_not_found>());
    }
}

nlc_test("bad_path_name") {
    auto std_alloc = standard_allocator {};

    {
        auto res = output_file_stream::open(std_alloc, "bad <> path name", 1024);
        nlc_check(res.failed());
        // No idea why it doesn't give bad_path_name error ¯\_(ツ)_/¯
        nlc_check(res.failed_with<unknown_error>());
    }

    {
        auto res = input_file_stream::open(std_alloc, "bad <> path name", 1024);
        nlc_check(res.failed());
        // No idea why it doesn't give bad_path_name error ¯\_(ツ)_/¯
        nlc_check(res.failed_with<unknown_error>());
    }
}
#else
nlc_test("path_not_found") {
    auto std_alloc = standard_allocator {};

    {
        auto res = output_file_stream::open(std_alloc, "/does/not/exist", 1024);
        nlc_check(res.failed());
        nlc_check(res.failed_with<path_not_found>());
    }

    {
        auto res = input_file_stream::open(std_alloc, "/does/not/exist", 1024);
        nlc_check(res.failed());
        nlc_check(res.failed_with<path_not_found>());
    }
}
#endif
