/* Copyright © 2022-2022
Alexis A. D. COLIN,
Antoine VUGLIANO,
Gaëtan CHAMPARNAUD,
Geoffrey L. TOURON,
Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/dialect/assert.hpp>
#include <nlc/fundamentals/allocator/page.hpp>
#include <nlc/test.hpp>

using namespace nlc;

namespace {
struct Foo1024 final {
    u8 bytes[1024];
};
struct Foo8 final {
    u8 byte;
};
}  // namespace

nlc_test("basic_usage") {
    page_allocator arena;
    auto f_a = arena.create<Foo1024>();
    auto f_b = arena.create<Foo8>();
    f_b->byte = 56u;
    nlc_check(f_b->byte == 56u);
    arena.destroy(f_a);
    arena.destroy(f_b);
}
