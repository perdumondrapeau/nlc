/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/fundamentals/report_error_to_user.hpp>
#include <nlc/test.hpp>

using namespace nlc;

static int call_count = 0;

static auto custom_report_error_callback(log_entry & entry, bool const) -> void {
    entry.display(out_stream::err);
    ++call_count;
}

nlc_test("report_error_to_user") {
    report_error_to_user(true, "should not exit");
    nlc_check(report_retryable_error_to_user(true, "should not exit") == retryable_error_result::ok);

    set_report_error_callback(custom_report_error_callback);
    report_error_to_user("should not exit");
    report_retryable_error_to_user("should not exit");
    report_error_to_user(true, "should not exit");
    nlc_check(report_retryable_error_to_user(true, "should not exit") == retryable_error_result::ok);
    report_error_to_user(false, "should not exit");
    nlc_check(report_retryable_error_to_user(false, "should not exit") == retryable_error_result::retry);
    reset_report_error_callback();
    nlc_check(call_count == 4);
}
