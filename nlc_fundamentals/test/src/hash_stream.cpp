/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include <nlc/fundamentals/hash_stream.hpp>
#include <nlc/test.hpp>

#include "../utils.hpp"

nlc_test("basic usage") {
    auto & allocator = *std_allocator();

    constexpr i32 const numbers[] = { -1, 2, 3, 4, 5, 67890 };
    auto h1 = nlc::hash(numbers, sizeof(numbers));

    nlc::hash_stream hasher { allocator };
    hasher.write(nlc::span(reinterpret_cast<byte const *>(&numbers), sizeof(numbers)));
    auto h2 = hasher.digest();
    nlc_check(h1 == h2);

    hasher.reset();
    hasher.write(nlc::span(reinterpret_cast<byte const *>(&numbers), sizeof(numbers)));
    h2 = hasher.digest();
    nlc_check(h1 == h2);

    hasher.reset();
    hasher.write(numbers[0]);
    hasher.write(numbers[1]);
    hasher.write(numbers[2]);
    hasher.write(numbers[3]);
    hasher.write(numbers[4]);
    hasher.write(numbers[5]);
    h2 = hasher.digest();
    nlc_check(h1 == h2);

    h1 = nlc::hash(nlc::string_view { "abcdefhijklmnopqrstuvwxyz1234567890" });
    hasher.reset();
    hasher.write("abc");
    hasher.write("defhijklmnopqrstuvwxyz1234567890");
    h2 = hasher.digest();
    nlc_check(h1 == h2);
}
