/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "file_stream.hpp"

#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/memory.hpp>
#include <nlc/sanity/platforms.hpp>
#include "nlc/dialect/assert.hpp"

#if NLC_OS_WINDOWS
/* #    if 0 */
/* #        include <Windows.h> */
/* #    else */
// Define necessary stuff to avoid including windows.h
extern "C" {
#    define WINBASEAPI __declspec(dllimport)
#    define WINAPI     __stdcall

#    define GENERIC_WRITE         (0x40000000L)
#    define GENERIC_READ          (0x80000000L)
#    define CREATE_ALWAYS         2
#    define OPEN_EXISTING         3
#    define FILE_ATTRIBUTE_NORMAL 0x00000080
#    define FILE_BEGIN            0

#    define ERROR_PATH_NOT_FOUND 3L
#    define ERROR_ACCESS_DENIED  5L
#    define ERROR_BAD_PATHNAME   161L

typedef int BOOL;
typedef long LONG;
typedef unsigned long DWORD;
typedef void * HANDLE;
typedef LONG * PLONG;
typedef const char *LPCSTR, *PCSTR;
typedef const void * LPCVOID;
typedef void * LPVOID;
typedef DWORD * LPDWORD;

#    if NLC_COMPILER_CLANG_CL
#        pragma GCC diagnostic push
#        pragma GCC diagnostic ignored "-Wlanguage-extension-token"
#    endif
typedef __int64 LONG_PTR, *PLONG_PTR;
#    if NLC_COMPILER_CLANG_CL
#        pragma GCC diagnostic pop
#    endif
#    define INVALID_HANDLE_VALUE (reinterpret_cast<HANDLE>(LONG_PTR(-1)))

struct _SECURITY_ATTRIBUTES;
typedef _SECURITY_ATTRIBUTES * LPSECURITY_ATTRIBUTES;

struct _OVERLAPPED;
typedef _OVERLAPPED * LPOVERLAPPED;

WINBASEAPI DWORD WINAPI GetLastError();
WINBASEAPI HANDLE WINAPI CreateFileA(LPCSTR lpFileName,
                                     DWORD dwDesiredAccess,
                                     DWORD dwShareMode,
                                     LPSECURITY_ATTRIBUTES lpSecurityAttributes,
                                     DWORD dwCreationDisposition,
                                     DWORD dwFlagsAndAttributes,
                                     HANDLE hTemplateFile);
WINBASEAPI BOOL WINAPI WriteFile(HANDLE hFile,
                                 LPCVOID lpBuffer,
                                 DWORD nNumberOfBytesToWrite,
                                 LPDWORD lpNumberOfBytesWritten,
                                 LPOVERLAPPED lpOverlapped);
WINBASEAPI BOOL WINAPI ReadFile(HANDLE hFile,
                                LPVOID lpBuffer,
                                DWORD nNumberOfBytesToRead,
                                LPDWORD lpNumberOfBytesRead,
                                LPOVERLAPPED lpOverlapped);
WINBASEAPI BOOL WINAPI CloseHandle(HANDLE hObject);
WINBASEAPI DWORD WINAPI SetFilePointer(HANDLE hFile,
                                       LONG lDistanceToMove,
                                       PLONG lpDistanceToMoveHigh,
                                       DWORD dwMoveMethod);
WINBASEAPI BOOL WINAPI SetEndOfFile(HANDLE hFile);
}
/* #    endif */
#elif NLC_OS_LINUX || NLC_OS_BSD
#    include <errno.h>
#    include <fcntl.h>
#    include <string.h>  // strerror
#    include <sys/stat.h>
#    include <unistd.h>
#else
#    error unsupported
#endif

#include "allocator.hpp"

namespace nlc {

#if NLC_OS_WINDOWS
[[maybe_unused]] static void format(nlc::log_entry & l, DWORD const v) {
    // TODO get an error message instead
    l.append(static_cast<u32>(v));
}
#endif

// Returns wether an error occurred
[[nodiscard]] static auto issue_write_command(usize const file, span<byte const> const bytes) -> bool {
#if NLC_OS_WINDOWS
    auto const handle = reinterpret_cast<HANDLE>(file);
    DWORD bytes_written = 0;
    [[maybe_unused]] auto const res =
        WriteFile(handle, bytes.ptr(), static_cast<DWORD>(bytes.size()), &bytes_written, nullptr);

    if (res == 0) {
#    if !defined(NDEBUG)
        nlc::warn("Error during WriteFile : GetLastError() == ", GetLastError());
#    endif
        return true;
    }

    nlc_assert_msg(bytes_written == bytes.size(),
                   nlc_dump_var(bytes_written),
                   nlc_dump_var(bytes.size()));
    return false;

#elif NLC_OS_LINUX || NLC_OS_BSD
    auto const fd = static_cast<int>(file);

    auto const write_count = write(fd, bytes.begin(), bytes.size());
    if (write_count == -1) {
#    if !defined(NDEBUG)
        nlc::warn("Error during write : ", strerror(errno));
#    endif
        return true;
    }

    nlc_assert(write_count == static_cast<isize>(bytes.size()));
    return false;
#else
#    error unsupported
#endif
}

using open_file_result = expected<usize, access_denied, bad_path_name, path_not_found, unknown_error>;

static auto open_file_write(char const * const path) -> open_file_result {
#if NLC_OS_WINDOWS
    auto const handle = CreateFileA(path,
                                    GENERIC_WRITE,
                                    0,  // No sharing of file with other processes
                                    nullptr,
                                    CREATE_ALWAYS,
                                    FILE_ATTRIBUTE_NORMAL,  // No async, system-buffered (easier)
                                    nullptr);

    if (handle == INVALID_HANDLE_VALUE) {
        auto const err = GetLastError();
        switch (err) {
            case ERROR_ACCESS_DENIED: return access_denied {};
            case ERROR_BAD_PATHNAME: return bad_path_name {};
            case ERROR_PATH_NOT_FOUND: return path_not_found {};
            default:
#    if !defined(NDEBUG)
                nlc::warn("Unknown error opening '", path, "' for write: GetLastError() == ", err);
#    endif
                return unknown_error {};
        }
    }
    return reinterpret_cast<usize>(handle);
#elif NLC_OS_LINUX || NLC_OS_BSD
    auto const mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    auto const fd = open(path, O_CREAT | O_TRUNC | O_WRONLY, mode);

    if (fd == -1) {
        switch (errno) {
            case EACCES: return access_denied {};
            case EINVAL: return bad_path_name {};
            case ENOENT: return path_not_found {};
            default:
#    if !defined(NDEBUG)
                nlc::warn("Unknown error opening '", path, "' for write : ", errno, " : ", strerror(errno));
#    endif
                return unknown_error {};
        }
        nlc_unreachable;
    }

    return static_cast<usize>(fd);
#else
#    error unsupported
#endif
}

[[maybe_unused]] static auto open_file_read(char const * const path) -> open_file_result {
#if NLC_OS_WINDOWS
    auto const handle = CreateFileA(path,
                                    GENERIC_READ,
                                    0,  // No sharing of file with other processes
                                    nullptr,
                                    OPEN_EXISTING,
                                    FILE_ATTRIBUTE_NORMAL,  // No async, system-buffered (easier)
                                    nullptr);

    if (handle == INVALID_HANDLE_VALUE) {
        auto const err = GetLastError();
        switch (err) {
            case ERROR_ACCESS_DENIED: return access_denied {};
            case ERROR_BAD_PATHNAME: return bad_path_name {};
            case ERROR_PATH_NOT_FOUND: return path_not_found {};
            default:
#    if !defined(NDEBUG)
                nlc::warn("Unknown error opening '", path, "' for read : GetLastError() == ", err);
#    endif
                return unknown_error {};
        }
    }
    return reinterpret_cast<usize>(handle);
#elif NLC_OS_LINUX || NLC_OS_BSD
    auto const fd = open(path, O_RDONLY);

    if (fd == -1) {
        switch (errno) {
            case EACCES: return access_denied {};
            case EINVAL: return bad_path_name {};
            case ENOENT: return path_not_found {};
            default:
#    if !defined(NDEBUG)
                nlc::warn("Unknown error opening '", path, "' for read : ", errno, " : ", strerror(errno));
#    endif
                return unknown_error {};
        }
        nlc_unreachable;
    }

    return static_cast<usize>(fd);
#else
#    error unsupported
#endif
}

static auto close_file(usize const file) -> void {
#if NLC_OS_WINDOWS
    auto const handle = reinterpret_cast<HANDLE>(file);
    [[maybe_unused]] auto res = CloseHandle(handle);
    nlc_assert_msg(res != 0, nlc_dump_var(GetLastError()));
#elif NLC_OS_LINUX || NLC_OS_BSD
    auto const fd = static_cast<int>(file);
    [[maybe_unused]] auto res = close(fd);
    nlc_assert_msg(res == 0, nlc_dump_var(strerror(errno)));
#else
#    error unsupported
#endif
}

static auto reset_file(usize const file) -> void {
#if NLC_OS_WINDOWS
    auto const handle = reinterpret_cast<HANDLE>(file);
    [[maybe_unused]] auto const res = SetFilePointer(handle, 0, nullptr, FILE_BEGIN);
    nlc_assert_msg(res == 0, nlc_dump_var(GetLastError()));
    [[maybe_unused]] auto const res2 = SetEndOfFile(handle);
    nlc_assert_msg(res2 != 0, nlc_dump_var(GetLastError()));
#elif NLC_OS_LINUX || NLC_OS_BSD
    auto const fd = static_cast<int>(file);
    [[maybe_unused]] auto res = lseek(fd, 0, SEEK_SET);
    nlc_assert_msg(res == 0, nlc_dump_var(strerror(errno)));
    res = ftruncate(fd, 0);
    nlc_assert_msg(res == 0, nlc_dump_var(strerror(errno)));
#else
#    error unsupported
#endif
}

[[nodiscard]] auto output_file_stream::open(allocator & alloc_,
                                            nlc::string_view const filepath,
                                            usize const buffer_size) -> create_result {
    static constexpr auto max_path_length = 512;
    char path[max_path_length];
    make_null_terminated(filepath, path);

    auto const file = open_file_write(path);
    if (file.failed())
        return file.forward_error();

    nlc_assert(buffer_size >= 256);  // come on
    auto const buffer = alloc_.allocate<byte>(buffer_size);

    return output_file_stream { alloc_, file.get(), buffer };
}

output_file_stream::output_file_stream(allocator & alloc_, usize const file_, span<byte> const buffer_)
    : alloc { alloc_ }
    , buffer { buffer_ }
    , file { file_ } {
    nlc_assert(file != no_file);
}

output_file_stream::output_file_stream(output_file_stream && other)
    : alloc { other.alloc }
    , buffer { other.buffer }
    , cursor { other.cursor }
    , file { other.file }
    , error_occurred { other.error_occurred } {
    other.buffer = {};
    other.cursor = 0;
    other.file = no_file;
    other.error_occurred = false;
#if !defined(NDEBUG)
    error_checked = other.error_checked;
    other.error_checked = true;  // other is likely a temporary object that will be destroyed
#endif
}

output_file_stream::~output_file_stream() {
    nlc_assert_msg(error_checked,
                   "status of the stream was not checked after usage, but it's mandatory because "
                   "writes "
                   "can fail; use `any_error_occurred`");

    if (file != no_file) {
        flush();
        close_file(file);
        alloc.free(buffer);
    }
}

auto output_file_stream::write(span<byte const> const bytes) -> void {
#if !defined(NDEBUG)
    // Eventually this function will do a write that can error, so we reset its status just in case
    error_checked = false;
#endif

    auto remaining_size = bytes.size();

    if (remaining_size >= buffer.size()) {
        if (cursor != 0) {
            error_occurred = issue_write_command(file, { buffer.ptr(), cursor });
            cursor = 0;
        }
        error_occurred = issue_write_command(file, bytes);
        return;
    }

    auto * input = bytes.ptr();
    auto const free_size = (buffer.size() - cursor);
    if (remaining_size < free_size) {
        memcpy(buffer.ptr() + cursor, input, remaining_size);
        cursor += bytes.size();
        return;
    }

    memcpy(buffer.ptr() + cursor, input, free_size);
    error_occurred = issue_write_command(file, buffer);
    input += free_size;

    remaining_size -= free_size;
    memcpy(buffer.ptr(), input, remaining_size);

    cursor = remaining_size;
}

auto output_file_stream::reset() -> void {
    flush();
    reset_file(file);
    cursor = 0;
}

auto output_file_stream::flush() -> void {
    if (cursor != 0)
        error_occurred = issue_write_command(file, { buffer.ptr(), cursor });
}

auto output_file_stream::any_error_occurred() const -> bool {
#if !defined(NDEBUG)
    error_checked = true;
#endif
    return error_occurred;
}

///////////////////////////////////////////////////////////////////////////////

[[nodiscard]] auto input_file_stream::open(allocator & alloc_,
                                           nlc::string_view const filepath,
                                           usize const buffer_size) -> create_result {
    static constexpr auto max_path_length = 512;
    char path[max_path_length];
    make_null_terminated(filepath, path);

    auto const file = open_file_read(path);
    if (file.failed())
        return file.forward_error();

    nlc_assert(buffer_size >= 256);  // come on
    auto const buffer = alloc_.allocate<byte>(buffer_size);

    return input_file_stream { alloc_, file.get(), buffer };
}

input_file_stream::input_file_stream(allocator & alloc_, usize const file_, span<byte> const buffer_)
    : alloc { alloc_ }
    , buffer { buffer_ }
    , file { file_ } {
    nlc_assert(file != no_file);
}

input_file_stream::input_file_stream(input_file_stream && other)
    : alloc { other.alloc }
    , buffer { other.buffer }
    , pending { other.pending }
    , file { other.file }
    , error_occurred { other.error_occurred } {
    other.buffer = {};
    other.pending = {};
    other.file = no_file;
    other.error_occurred = false;
#if !defined(NDEBUG)
    error_checked = other.error_checked;
    other.error_checked = true;  // other is likely a temporary object that will be destroyed
#endif
}

input_file_stream::~input_file_stream() {
    nlc_assert_msg(error_checked,
                   "status of the stream was not checked after usage, but it's mandatory because "
                   "reads can fail; use `any_error_occurred`");

    if (file != no_file) {
        close_file(file);
        alloc.free(buffer);
    }
}

// Returns wether an error occurred
[[nodiscard]] static auto issue_read_command(usize const file, span<byte> const bytes) -> isize {
#if NLC_OS_WINDOWS
    auto const handle = reinterpret_cast<HANDLE>(file);
    DWORD bytes_read = 0;
    [[maybe_unused]] auto const res = ReadFile(handle,
                                               reinterpret_cast<LPVOID>(bytes.ptr()),
                                               static_cast<DWORD>(bytes.size()),
                                               &bytes_read,
                                               nullptr);

    if (res == 0) {
#    if !defined(NDEBUG)
        nlc::warn("Error during WriteFile : GetLastError() == ", GetLastError());
#    endif
        return -1;
    }

    nlc_assert_msg(bytes_read <= static_cast<DWORD>(bytes.size()),
                   nlc_dump_var(bytes_read),
                   nlc_dump_var(bytes.size()));
    return bytes_read;

#elif NLC_OS_LINUX || NLC_OS_BSD
    auto const fd = static_cast<int>(file);

    auto const read_count = read(fd, bytes.begin(), bytes.size());
    if (read_count == -1) {
#    if !defined(NDEBUG)
        nlc::warn("Error during read : ", strerror(errno));
#    endif
        return -1;
    }

    nlc_assert(read_count <= static_cast<isize>(bytes.size()));
    return read_count;
#else
#    error unsupported
#endif
}

auto input_file_stream::read(span<byte> const bytes) -> void {
#if !defined(NDEBUG)
    // Eventually this function will do a read that can error, so we reset its status just in case
    error_checked = false;
#endif

    auto dest = bytes;
    auto remaining_size = dest.size();
    if (pending.size() != 0) {
        auto const size = pending.size() < remaining_size ? pending.size() : remaining_size;
        nlc::memcpy(dest.ptr(), pending.ptr(), size);
        pending = { pending.ptr() + size, pending.size() - size };
        dest = { dest.ptr() + size, dest.size() - size };
        remaining_size -= size;
    }

    if (remaining_size == 0)
        return;

    nlc_assert(pending.size() == 0);

    auto const passthrough = remaining_size >= buffer.size();

    if (passthrough) {
        auto const read_count = issue_read_command(file, dest);
        if (read_count == -1 || static_cast<usize>(read_count) < dest.size())
            error_occurred = true;

        return;
    }

    auto const read_count = issue_read_command(file, buffer);
    if (read_count == -1) {
        error_occurred = true;
        return;
    }
    pending = { buffer.ptr(), static_cast<usize>(read_count) };
    nlc_assert_msg(remaining_size < pending.size(),
                   nlc_dump_var(remaining_size),
                   nlc_dump_var(pending.size()));

    nlc::memcpy(dest.ptr(), pending.ptr(), remaining_size);
    pending = { pending.ptr() + remaining_size, pending.size() - remaining_size };
}

auto input_file_stream::reset() -> void {
    reset_file(file);
    pending = {};
}

auto input_file_stream::any_error_occurred() const -> bool {
#if !defined(NDEBUG)
    error_checked = true;
#endif
    return error_occurred;
}

}  // namespace nlc
