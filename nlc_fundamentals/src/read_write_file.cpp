/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "read_write_file.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/memory.hpp>

#include <fcntl.h>
#include <sys/stat.h>

#if NLC_OS_WINDOWS
#    include <io.h>
#elif __GNUC__
#    include <unistd.h>
#else
#    error "unknown platform"
#endif

namespace nlc {

static constexpr auto max_path_length = 512;

auto read_file(allocator & allocator, char const * path) -> scoped<span<byte>> {
#if NLC_OS_WINDOWS
    auto file = -1;
    [[maybe_unused]] auto const open_err =
        ::_sopen_s(&file, path, _O_RDONLY | _O_BINARY, _SH_DENYWR, _S_IREAD);
    nlc_assert_msg(file >= 0, nlc_dump_var(path));
    nlc_assert_msg(open_err == 0, nlc_dump_var(path), nlc_dump_var(open_err));

    struct ::stat file_stats;
    [[maybe_unused]] auto const stat_err = ::fstat(file, &file_stats);
    nlc_assert_msg(stat_err == 0, nlc_dump_var(path), nlc_dump_var(stat_err));

    auto const size = static_cast<usize>(file_stats.st_size);

    auto const buf = allocator.allocate<byte>(size + 1);
    nlc_assert_msg(buf.ptr() != nullptr, nlc_dump_var(path));

    [[maybe_unused]] auto const read_count = ::_read(file, buf.ptr(), static_cast<unsigned int>(size));
    nlc_assert_msg(static_cast<unsigned long>(read_count) == size,
                   nlc_dump_var(path),
                   nlc_dump_var(read_count),
                   nlc_dump_var(size));
    // We add a final byte with value 0 to play nice with loading text files.
    // This way, we cant reuse the buffer as a C string directly.
    buf[size] = 0;

    [[maybe_unused]] auto const close_err = ::_close(file);
    nlc_assert_msg(close_err == 0, nlc_dump_var(path), nlc_dump_var(close_err));
#else
    auto const file = ::open(path, O_RDONLY);
    nlc_assert_msg(file >= 0, nlc_dump_var(path), nlc_dump_var(file));

    struct ::stat file_stats;
    [[maybe_unused]] auto const stat_err = ::fstat(file, &file_stats);
    nlc_assert_msg(stat_err == 0, nlc_dump_var(path), nlc_dump_var(stat_err));

    auto const size = static_cast<usize>(file_stats.st_size);

    auto const buf = allocator.allocate<byte>(size + 1);
    nlc_assert_msg(buf.ptr() != nullptr, nlc_dump_var(path));

    [[maybe_unused]] auto const read_count = ::read(file, buf.ptr(), size);
    nlc_assert_msg(read_count == static_cast<isize>(size),
                   nlc_dump_var(path),
                   nlc_dump_var(read_count),
                   nlc_dump_var(size));
    // We add a final byte with value 0 to play nice with loading text files.
    // This way, we can reuse the buffer as a C string directly.
    buf[size] = 0;

    [[maybe_unused]] auto const close_err = ::close(file);
    nlc_assert_msg(close_err == 0, nlc_dump_var(path), nlc_dump_var(close_err));
#endif

    return scoped(span { buf.ptr(), size }, [&allocator](auto & v) {
        // We hide the \0 from the user by only exposing size, but need to free with the correct
        // buffer size.
        allocator.free(span<byte> { v.ptr(), v.size() + 1 });
    });
}

auto read_file(allocator & alloc, string_view const path) -> scoped<span<byte>> {
    char buf[max_path_length];
    make_null_terminated(path, buf);
    return read_file(alloc, buf);
}

auto write_file(char const * path, span<byte const> const & data) -> void {
#if NLC_OS_WINDOWS
    auto file = -1;
    [[maybe_unused]] auto const open_err =
        ::_sopen_s(&file, path, _O_CREAT | _O_TRUNC | _O_WRONLY, _SH_DENYRW, _S_IREAD | _S_IWRITE);
    nlc_assert_msg(file >= 0, nlc_dump_var(path));
    nlc_assert_msg(open_err == 0, nlc_dump_var(path), nlc_dump_var(open_err));

    [[maybe_unused]] auto const write_count =
        ::_write(file, data.begin(), static_cast<unsigned int>(data.size()));
    nlc_assert_msg(write_count == static_cast<isize>(data.size()),
                   nlc_dump_var(path),
                   nlc_dump_var(write_count),
                   nlc_dump_var(data.size()));

    [[maybe_unused]] auto const close_err = ::_close(file);
    nlc_assert_msg(close_err == 0, nlc_dump_var(path), nlc_dump_var(close_err));
#else
    auto const mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    auto const file = ::open(path, O_CREAT | O_TRUNC | O_WRONLY, mode);
    nlc_assert_msg(file >= 0, nlc_dump_var(path), nlc_dump_var(file));

    [[maybe_unused]] auto const write_count = ::write(file, data.begin(), data.size());
    nlc_assert_msg(write_count == static_cast<isize>(data.size()),
                   nlc_dump_var(path),
                   nlc_dump_var(write_count),
                   nlc_dump_var(data.size()));

    [[maybe_unused]] auto const close_err = close(file);
    nlc_assert_msg(close_err == 0, nlc_dump_var(path), nlc_dump_var(close_err));
#endif
}

auto write_file(string_view const path, span<byte const> const & data) -> void {
    char buf[max_path_length];
    make_null_terminated(path, buf);
    return write_file(buf, data);
}

}  // namespace nlc
