/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "time.hpp"

#include <nlc/sanity/platforms.hpp>

#if NLC_OS_WINDOWS

// Define necessary stuff to avoid including windows.h
#    define WINAPI __stdcall
extern "C" {
typedef int BOOL;
typedef long LONG;
typedef unsigned long DWORD;

#    if NLC_COMPILER_CLANG_CL
#        pragma GCC diagnostic push
#        pragma GCC diagnostic ignored "-Wlanguage-extension-token"
#    endif
typedef signed __int64 LONGLONG;
#    if NLC_COMPILER_CLANG_CL
#        pragma GCC diagnostic pop
#    endif

typedef union _LARGE_INTEGER {
    struct {
        DWORD LowPart;
        LONG HighPart;
    } u;
    LONGLONG QuadPart;
} LARGE_INTEGER, *PLARGE_INTEGER;

BOOL WINAPI QueryPerformanceFrequency(LARGE_INTEGER * lpFrequency);
BOOL WINAPI QueryPerformanceCounter(LARGE_INTEGER * lpPerformanceCount);
}
#else
#    include <time.h>
#endif

namespace nlc::time {

#if NLC_OS_WINDOWS

struct state final {
    LARGE_INTEGER freq;
    LARGE_INTEGER start;
};

static auto init() -> state {
    state s;
    QueryPerformanceFrequency(&s.freq);
    QueryPerformanceCounter(&s.start);

    return s;
}

static auto get_start() -> state {
    static auto s = init();
    return s;
}

// https://docs.microsoft.com/en-us/windows/win32/sysinfo/acquiring-high-resolution-time-stamps#using-qpc-in-native-code
auto now() -> duration {
    LARGE_INTEGER counter;
    QueryPerformanceCounter(&counter);

    auto const s = get_start();
    LARGE_INTEGER elapsed;
    elapsed.QuadPart = counter.QuadPart - s.start.QuadPart;
    elapsed.QuadPart *= ns_per_s;
    elapsed.QuadPart /= s.freq.QuadPart;

    return static_cast<u64>(elapsed.QuadPart);
}

#else

static auto init() -> u64 {
    timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return static_cast<u64>(ts.tv_sec) * ns_per_s + static_cast<u64>(ts.tv_nsec);
}

static auto get_start() -> u64 {
    static auto s = init();
    return s;
}

auto now() -> duration {
    timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    auto const t = static_cast<u64>(ts.tv_sec) * ns_per_s + static_cast<u64>(ts.tv_nsec);

    return t - get_start();
}

#endif

// never used
static auto force_init = get_start();

}  // namespace nlc::time
