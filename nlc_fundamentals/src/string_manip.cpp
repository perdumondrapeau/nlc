/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "string_manip.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/defer.hpp>
#include <nlc/dialect/string_iterator.hpp>
#include <nlc/dialect/utf8.hpp>

#include <memory.h>

#include "allocator.hpp"

namespace nlc {

auto starts_with(string_view const str, string_view const prefix) -> bool {
    if (prefix.size() > str.size())
        return false;

    return memcmp(str.ptr(), prefix.ptr(), prefix.size().v) == 0;
}

auto ends_with(string_view const str, string_view const suffix) -> bool {
    if (suffix.size() > str.size())
        return false;

    return memcmp(str.ptr() + (str.size() - suffix.size()).v, suffix.ptr(), suffix.size().v) == 0;
}

[[nodiscard]] auto levenshtein_distance(allocator & allocator,
                                        string_view const a,
                                        string_view const b) -> usize {
    constexpr auto erase_cost { 1u };
    constexpr auto insert_cost { 1u };
    constexpr auto substitute_cost { 1u };

    // Shortcut optimizations / degenerate cases.
    if (a.ptr() == b.ptr() && a.size() == b.size())
        return 0u;
    if (a == b)
        return 0u;

    auto const a_len { length(a) };
    auto const b_len { length(b) };

    if (a.size() == 0u)
        return b_len;
    if (b.size() == 0u)
        return a_len;

    auto const buffer_line_size { a_len + 1u };
    auto const buffer = allocator.allocate<usize>(buffer_line_size * 2u);
    defer { allocator.free(buffer); };

    span<usize> first_line { buffer.begin(), buffer_line_size };
    span<usize> second_line { buffer.ptr() + buffer_line_size, buffer_line_size };

    // Initialization
    for (auto i = 0u; i <= a_len; ++i)
        first_line[i] = i;

    for (usize j = 1u; auto b_it : b) {
        defer { ++j; };

        // Prepare output line
        second_line[0u] = j;

        // Fill output line
        for (usize i = 1u; auto a_it : a) {
            defer { ++i; };

            auto const needs_substitution { a_it.value() != b_it.value() };

            auto const erase_value { second_line[i - 1u] + erase_cost };
            auto const insert_value { first_line[i] + insert_cost };
            auto const substitute_value { first_line[i - 1u] +
                                          (needs_substitution ? substitute_cost : 0u) };

            auto const min = [](usize const u, usize const v) { return u < v ? u : v; };
            second_line[i] = min(erase_value, min(insert_value, substitute_value));
        }

        // Swap lines
        auto const tmp { first_line };
        first_line = second_line;
        second_line = tmp;
    }

    // Since we swap before exiting the loop, the result lies in the first line
    return first_line[a_len];
}

[[nodiscard]] auto find(string_view s, codepoint_type c) -> string_iterator {
    auto it = s.begin();
    for (auto const end = s.end(); it != end; ++it)
        if (*it == c)
            break;
    return it;
}

[[nodiscard]] auto find_last(string_view s, codepoint_type c) -> string_iterator {
    auto const end = s.end();
    auto res = end;
    for (auto it = s.begin(); it != end; ++it)
        if (*it == c)
            res = it;
    return res;
}

auto contains(string_view s, codepoint_type c) -> bool {
    auto it = s.begin();
    for (auto const end = s.end(); it != end; ++it)
        if (*it == c)
            return true;
    return false;
}

auto get_codepoint_at(string_view const str, usize const index) -> string_iterator {
    auto const endIt = str.end();
    auto it = str.begin();

    for (auto i = 0u; i < index; ++i) {
        if (it == endIt) {
            break;
        }
        ++it;
    }

    return it;
}

auto get_splice_count(string_view const string, codepoint_type const separator) -> usize {
    if (string.is_empty())
        return 0u;

    usize count { 1u };

    for (auto it = string.begin(); it != string.end(); ++it) {
        if (*it == separator)
            ++count;
    }

    return count;
}

auto get_first_splice(string_view const string, codepoint_type const separator) -> string_view {
    for (auto it = string.begin(); it != string.end(); ++it) {
        if (*it == separator)
            return { string.begin(), it };
    }

    return string;
}

namespace impl {
    static auto try_split_string(string_view const string,
                                 codepoint_type const separator,
                                 span<string_view> const output_buffer) -> isize {
        // Early return, if the input string in empty the output is empty too
        if (string.is_empty())
            return 0u;

        auto splice_count = 0u;
        auto splice_start = string.begin();
        auto it = string.begin();

        while (it != string.end()) {
            if (*it == separator) {
                if (splice_count >= output_buffer.size())
                    return -1;
                output_buffer[splice_count++] = { splice_start, it };
                ++it;
                splice_start = it;
            } else
                ++it;
        }

        if (splice_count >= output_buffer.size())
            return -1;
        output_buffer[splice_count++] = { splice_start, it };

        return splice_count;
    }
}  // namespace impl

auto try_split_string(string_view const string,
                      codepoint_type const separator,
                      span<string_view> const output_buffer) -> expected<span<string_view>, buffer_too_small> {
    auto const splice_count { impl::try_split_string(string, separator, output_buffer) };
    if (splice_count == -1)
        return buffer_too_small {};
    return span { output_buffer.ptr(), static_cast<usize>(splice_count) };
}

auto split_string(string_view const string,
                  codepoint_type const separator,
                  span<string_view> const output_buffer) -> span<string_view> {
    auto const splice_count { impl::try_split_string(string, separator, output_buffer) };
    nlc_assert_msg(splice_count >= 0,
                   "split_string: output_buffer is too small to contain all the splices");
    return { output_buffer.ptr(), static_cast<usize>(splice_count) };
}

/*
auto getFront(string_view str, split_options options) -> string_view;
auto frontChar(string_view) -> codepoint_type;
auto backChar(string_view) -> codepoint_type;
auto popFrontChar(string_view) -> string_view;

auto removeQuotes(string_view str, string_view quotes) -> string_view;
auto toInt(string_view, int base) -> int;
auto toFloat(string_view) -> float;
auto toDouble(string_view) -> double;
*/

}  // namespace nlc
