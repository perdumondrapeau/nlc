/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "from_string.hpp"

#include <nlc/dialect/assert.hpp>

#include <errno.h>
#include <stdlib.h>

namespace nlc {

#define FROM_STRING_INTEGER(TYPE, FUNC)                                        \
    template<> auto from_string<TYPE>(string_view const s) -> optional<TYPE> { \
        if (s.size() > 0u) {                                                   \
            errno = 0;                                                         \
            char * end_ptr = nullptr;                                          \
            auto const value = FUNC(s.begin(), &end_ptr, 10);                  \
            if (end_ptr == s.end().ptr() && errno == 0)                        \
                return static_cast<TYPE>(value);                               \
            else                                                               \
                errno = 0;                                                     \
        }                                                                      \
        return null;                                                           \
    }

FROM_STRING_INTEGER(i8, strtol)
FROM_STRING_INTEGER(i16, strtol)
FROM_STRING_INTEGER(i32, strtol)
FROM_STRING_INTEGER(i64, strtoll)

#undef FROM_STRING_INTEGER

// Negative numbers are accepted as valid input and can't be detected without inspecting the input
// string characters
#define FROM_STRING_INTEGER(TYPE, FUNC)                                        \
    template<> auto from_string<TYPE>(string_view const s) -> optional<TYPE> { \
        if (s.size() == 0u)                                                    \
            return null;                                                       \
        auto const c = *s.begin();                                             \
        if (c >= '0' && c <= '9') {                                            \
            errno = 0;                                                         \
            char * end_ptr = nullptr;                                          \
            auto const value = FUNC(s.begin(), &end_ptr, 10);                  \
            if (end_ptr == s.end().ptr() && errno == 0)                        \
                return static_cast<TYPE>(value);                               \
            else                                                               \
                errno = 0;                                                     \
        }                                                                      \
        return null;                                                           \
    }

FROM_STRING_INTEGER(u8, strtoul)
FROM_STRING_INTEGER(u16, strtoul)
FROM_STRING_INTEGER(u32, strtoul)
FROM_STRING_INTEGER(u64, strtoull)

#undef FROM_STRING_INTEGER

#define FROM_STRING_FLOAT(TYPE, FUNC)                                          \
    template<> auto from_string<TYPE>(string_view const s) -> optional<TYPE> { \
        if (s.size() > 0u) {                                                   \
            errno = 0;                                                         \
            char * end_ptr = nullptr;                                          \
            auto const value = FUNC(s.begin(), &end_ptr);                      \
            if (end_ptr == s.end().ptr() && errno == 0)                        \
                return value;                                                  \
            else                                                               \
                errno = 0;                                                     \
        }                                                                      \
        return null;                                                           \
    }

FROM_STRING_FLOAT(f32, strtof)
FROM_STRING_FLOAT(f64, strtod)
#undef FROM_STRING_FLOAT

}  // namespace nlc
