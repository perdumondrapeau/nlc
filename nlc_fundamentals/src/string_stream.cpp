/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "string_stream.hpp"

#include <stdio.h>

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/memory.hpp>

namespace nlc {

static constexpr auto min_not_null_capacity = 8;
static constexpr auto number_space_allocation = bytes<usize>(512);

string_stream::string_stream(allocator & a)
    : _allocator(&a)
    , _data(nullptr)
    , _size(0)
    , _capacity(min_not_null_capacity) {
    _data = _allocator->allocate<char>(_capacity.v).ptr();
    _data[_size.v] = '\0';
    nlc_assert(str_size(_data) == _size);
}

string_stream::~string_stream() { _allocator->free(span<char> { _data, _capacity.v }); }

auto string_stream::reserve(bytes<usize> const size) -> void {
    auto const alloc_size = size.v + 1u;
    if (_capacity < alloc_size) {
        auto const old = _capacity.v;
        while (_capacity < alloc_size) {
            _capacity *= 2u;
        }

        _data = reinterpret_cast<char *>(
            _allocator->reallocate(span<char>(_data, old), _capacity.v).ptr());
    }
}

auto string_stream::resize(bytes<usize> const size) -> void {
    if (size > _size) {
        reserve(size);
    }

    _size = size;
    _data[_size.v] = '\0';
}

auto string_stream::append(bool b) -> void { append(b ? "true" : "false"); }

auto string_stream::append(char * str) -> void { append(const_cast<char const *>(str)); }

auto string_stream::append(char const * str) -> void {
    auto const size = str_size(str);
    append(str, size);
}

auto string_stream::append(void const * const p, usize const s) -> void {
    auto const size = bytes<usize>(s);
    reserve(_size + size);
    memcpy(_data + _size, p, size);
    _size += size;
    _data[_size.v] = '\0';
    nlc_assert(str_size(_data) == _size);
}

auto string_stream::append(char c) -> void {
    nlc_assert(str_size(_data) == _size);
    reserve(_size + size_of<char>);
    _data[_size.v] = c;
    _size += size_of<char>;
    _data[_size.v] = '\0';
    nlc_assert(str_size(_data) == _size);
}

#define IMPLEMENT_APPEND(TYPE, FMT)                                                              \
    auto string_stream::append(TYPE v) -> void {                                                 \
        nlc_assert(str_size(_data) == _size);                                                    \
        reserve(_size + number_space_allocation);                                                \
        auto const ret = snprintf(_data + _size, number_space_allocation.v, FMT, v);             \
        nlc_assert(ret >= 0); /* Error occured ? */                                              \
        auto const written_size = bytes<usize>(static_cast<usize>(ret));                         \
        nlc_assert(written_size < number_space_allocation); /* Sufficient memory was allocated ? \
                                                             */                                  \
        _size += written_size;                                                                   \
        _data[_size.v] = '\0';                                                                   \
        nlc_assert(str_size(_data) == _size);                                                    \
    }

IMPLEMENT_APPEND(i8, prii8)
IMPLEMENT_APPEND(i16, prii16)
IMPLEMENT_APPEND(i32, prii32)
IMPLEMENT_APPEND(i64, prii64)
IMPLEMENT_APPEND(u8, priu8)
IMPLEMENT_APPEND(u16, priu16)
IMPLEMENT_APPEND(u32, priu32)
IMPLEMENT_APPEND(u64, priu64)
IMPLEMENT_APPEND(f64, "%f")

#undef IMPLEMENT_APPEND

auto string_stream::append(f32 v) -> void { append(static_cast<double>(v)); }

auto string_stream::append(codepoint_type cp) -> void {
    nlc_assert(str_size(_data) == _size);
    auto const cp_size = codepoint_size(cp);
    reserve(_size.v + cp_size);
    cat_to_string(_data + _size, cp);
    _size.v += cp_size;
    _data[_size.v] = '\0';
    nlc_assert(str_size(_data) == _size);
}

auto string_stream::append(string_view const str) -> void {
    nlc_assert(str_size(_data) == _size);
    append(str.ptr(), str.size().v);
}

auto string_stream::clear() -> void { resize(0u); }

auto string_stream::clear_reduce_memory() -> void {
    // Reduce allocation to min_not_null_capacity
    if (_capacity.v > min_not_null_capacity) {
        _data = reinterpret_cast<char *>(
            _allocator->reallocate(span<char> { _data, _capacity.v }, min_not_null_capacity).ptr());
        _capacity = min_not_null_capacity;
    }
    resize(0u);
}

}  // namespace nlc
