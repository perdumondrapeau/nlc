/* Copyright © 2020-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "allocator/impl/virtual_alloc.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/types.hpp>

#if NLC_OS_WINDOWS
#    ifndef __MINGW32__
#        include <Windows.h>
#    else
#        include <windows.h>
#    endif
#    include <errhandlingapi.h>
#    include <memoryapi.h>
#else
#    include <errno.h>
#    include <string.h>
#    include <sys/mman.h>
#    include <unistd.h>
#endif

namespace nlc::impl {

namespace {
    usize page_size = 0;
}  // namespace

#if NLC_OS_WINDOWS
[[noreturn]] static void break_output_exit() {
    LPVOID msg_buffer;
    auto const error = GetLastError();

    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |
                      FORMAT_MESSAGE_IGNORE_INSERTS,
                  nullptr,
                  error,
                  MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                  reinterpret_cast<LPTSTR>(&msg_buffer),
                  0,
                  nullptr);

    nlc_assert_msg(false, "VirtualAlloc or VirtualFree error: ", reinterpret_cast<char *>(msg_buffer));

    LocalFree(msg_buffer);
    exit(EXIT_FAILURE);
}

usize get_page_size() {
    if (page_size == 0) {
        SYSTEM_INFO systemInfo;
        GetSystemInfo(&systemInfo);
        page_size = systemInfo.dwPageSize;
    }
    return page_size;
}

void * virtual_reserve(usize const size) {
    void * const ptr = VirtualAlloc(nullptr, size, MEM_RESERVE, PAGE_READWRITE);
    if (ptr == nullptr)
        break_output_exit();
    return ptr;
}

void * virtual_alloc(usize const size) {
    void * const ptr = VirtualAlloc(nullptr, size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
    if (ptr == nullptr)
        break_output_exit();
    return ptr;
}

void virtual_free(void * const ptr, usize const) {
    if (!VirtualFree(ptr, 0, MEM_RELEASE))
        break_output_exit();
}

void virtual_touch(void * const ptr, usize const size) {
    if (VirtualAlloc(ptr, size, MEM_COMMIT, PAGE_READWRITE) == nullptr)
        break_output_exit();
}

void virtual_untouch(void * const ptr, usize const size) {
    if (!VirtualFree(ptr, size, MEM_DECOMMIT))
        break_output_exit();
}
#else
usize get_page_size() {
    if (page_size == 0)
        page_size = static_cast<usize>(sysconf(_SC_PAGESIZE));
    return page_size;
}

void * virtual_reserve(usize const size) {
#    if NLC_OS_BSD
    void * const ptr = mmap(nullptr, size, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
#    else
    void * const ptr =
        mmap(nullptr, size, PROT_NONE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_NORESERVE, -1, 0);
#    endif
    if (ptr == MAP_FAILED)
        nlc_assert_msg(false, "virtual_reserve error", nlc_dump_var(strerror(errno)));
    return ptr;
}

void * virtual_alloc(usize const size) {
    void * const ptr = mmap(nullptr, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (ptr == MAP_FAILED)
        nlc_assert_msg(false, "virtual_alloc error", nlc_dump_var(strerror(errno)));
    return ptr;
}

void virtual_free(void * const ptr, usize const size) {
    if (munmap(ptr, size) != 0)
        nlc_assert_msg(false,
                       "virtual_free error",
                       nlc_dump_var(ptr),
                       nlc_dump_var(size),
                       nlc_dump_var(strerror(errno)));
}

void virtual_touch(void * const ptr, usize const size) {
    if (mprotect(ptr, size, PROT_READ | PROT_WRITE) != 0)
        nlc_assert_msg(false, "virtual_touch error", nlc_dump_var(strerror(errno)));
}

void virtual_untouch(void * const ptr, usize const size) {
    if (madvise(ptr, size, MADV_DONTNEED) != 0 || mprotect(ptr, size, PROT_NONE) != 0)
        nlc_assert_msg(false, "virtual_untouch error", nlc_dump_var(strerror(errno)));
}
#endif

}  // namespace nlc::impl
