/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "allocator/standard.hpp"

#include <nlc/dialect/assert.hpp>

#ifdef _WIN32
#    include <malloc.h>
#else
#    include <stdlib.h>
#endif
namespace nlc {

[[nodiscard]] auto standard_allocator::_allocate(bytes<usize> const size,
                                                 alignment const align) -> nlc::span<byte> {
#ifdef _WIN32
    auto mem = _aligned_malloc(size.v, align.v);
#else
    void * mem = nullptr;
    [[maybe_unused]] auto err = posix_memalign(&mem, align.v < 8 ? 8 : align.v, size.v);
    nlc_assert(err == 0);
#endif
    nlc_assert(mem != nullptr);
    return { reinterpret_cast<byte *>(mem), size.v };
}

[[nodiscard]] auto
standard_allocator::_reallocate(nlc::span<byte> const old,
                                bytes<usize> const size,
                                [[maybe_unused]] alignment const align) -> nlc::span<byte> {
    auto const ptr = old.ptr();
#ifdef _WIN32
    return { reinterpret_cast<byte *>(_aligned_realloc(ptr, size.v, align.v)), size.v };
#else
    return { reinterpret_cast<byte *>(realloc(ptr, size.v)), size.v };
#endif
}

auto standard_allocator::_free(nlc::span<byte> const memory) -> void {
    auto const ptr = memory.ptr();
#ifdef _WIN32
    _aligned_free(ptr);
#else
    ::free(ptr);
#endif
}
}  // namespace nlc
