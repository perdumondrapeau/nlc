/* Copyright © 2025-2025
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "allocator/monolithic.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/memory.hpp>
#include "allocator/impl/virtual_alloc.hpp"

namespace nlc {

monolithic_allocator::monolithic_allocator(usize max_bytes)
    : allocator {} {
    nlc_assert(max_bytes > 0);
    auto const page_size = impl::get_page_size();
    max_reserved_pages = max_bytes / page_size;
}

[[nodiscard]] auto monolithic_allocator::_allocate(bytes<usize> const size, alignment const)
    -> nlc::span<byte> {
    auto const page_size = impl::get_page_size();
    auto const ptr = reinterpret_cast<byte *>(impl::virtual_reserve(max_reserved_pages * page_size));

    usize const needed_pages = (size.v + page_size - 1) / page_size;
    usize const allocated_size = needed_pages * page_size;
    impl::virtual_touch(ptr, allocated_size);

    return { ptr, size.v };
}

[[nodiscard]] auto monolithic_allocator::_reallocate(nlc::span<byte> const old,
                                                     bytes<usize> const size,
                                                     alignment const align) -> nlc::span<byte> {
    if (old.ptr() == nullptr)
        return _allocate(size, align);

    auto const page_size = impl::get_page_size();

    usize const used_pages = (old.size() + page_size - 1) / page_size;
    auto const capacity = used_pages * page_size;

    if (size.v > capacity) {
        // only touch pages not committed yet
        auto const new_pages = (size.v - capacity + page_size - 1) / page_size;
        nlc_assert_msg(new_pages <= max_reserved_pages,
                       "exceeding monolithic_allocator capacity",
                       nlc_dump_var(max_reserved_pages),
                       nlc_dump_var(new_pages));
        impl::virtual_touch(old.ptr() + capacity, new_pages * page_size);
    }

    return { old.ptr(), size.v };
}

auto monolithic_allocator::_free(nlc::span<byte> const memory) -> void {
    if (memory.ptr() != nullptr) {
        auto const page_size = impl::get_page_size();
        usize const used_pages = (memory.size() + page_size - 1) / page_size;
        impl::virtual_free(memory.ptr(), used_pages * page_size);
    }
}

}  // namespace nlc
