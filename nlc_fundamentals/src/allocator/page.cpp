/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "allocator/page.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/memory.hpp>
#include "allocator/impl/virtual_alloc.hpp"

namespace nlc {

[[nodiscard]] auto page_allocator::_allocate(bytes<usize> const size, alignment const)
    -> nlc::span<byte> {
    usize const page_size = impl::get_page_size();
    usize const needed_pages = (size.v + page_size - 1) / page_size;
    usize const allocated_size = needed_pages * page_size;
    auto * const ptr = reinterpret_cast<byte *>(impl::virtual_alloc(allocated_size));
    impl::virtual_touch(reinterpret_cast<char *>(ptr), allocated_size);
    nlc::span<byte> const memory { ptr, size.v };
    return memory;
}

[[nodiscard]] auto page_allocator::_reallocate(nlc::span<byte> const old,
                                               bytes<usize> const size,
                                               alignment const align) -> nlc::span<byte> {
    auto const new_memory = _allocate(size, align);
    nlc::memcpy(new_memory.ptr(), old.ptr(), old.size());
    free(old);
    return new_memory;
}

auto page_allocator::_free(nlc::span<byte> const memory) -> void {
    if (memory.ptr() != nullptr) {
        auto const page_size = impl::get_page_size();
        auto const needed_pages = (memory.size() + page_size - 1) / page_size;
        auto const allocated_size = needed_pages * page_size;
        impl::virtual_free(memory.ptr(), allocated_size);
    }
}

}  // namespace nlc
