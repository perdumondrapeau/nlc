/* Copyright © 2022-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "allocator/arena.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/memory.hpp>
#include "allocator/impl/virtual_alloc.hpp"

namespace nlc {

arena_allocator::arena_allocator(usize const max_capacity) {
    usize const page_size = impl::get_page_size();
    _size = 0;
    _owned_pages = (max_capacity + page_size - 1) / page_size;
    _used_pages = 0;
    _capacity = 0;

    _ptr = max_capacity != 0
               ? reinterpret_cast<byte *>(impl::virtual_reserve(_owned_pages * page_size))
               : nullptr;
}

arena_allocator::~arena_allocator() {
    if (_owned_pages != 0) {
        nlc_assert(_ptr != nullptr);
        clear();
        impl::virtual_free(_ptr, _owned_pages * impl::get_page_size());
    }
}

[[nodiscard]] auto arena_allocator::_allocate(bytes<usize> const size,
                                              alignment const align) -> nlc::span<byte> {
    nlc_assert_msg(_used_pages < _owned_pages,
                   nlc_dump_var(_used_pages),
                   " < ",
                   nlc_dump_var(_owned_pages));
    auto const align_size = [](usize const value, usize const wanted_alignment) -> usize {
        auto const mask = wanted_alignment - 1;
        return value & mask ? (value + mask) & ~mask : value;
    };
    usize const page_size = impl::get_page_size();
    auto const aligned_arena_size = align_size(_size, align.v);
    auto const needed_size = aligned_arena_size + size.v;
    if (needed_size >= _capacity) {
        auto const geometric_wanted_capacity = _capacity + (_capacity >> 1);
        auto const wanted_capacity =
            geometric_wanted_capacity < needed_size ? needed_size : geometric_wanted_capacity;
        usize const needed_pages = (wanted_capacity + page_size - 1) / page_size;
        auto const previous_used_pages = _used_pages;
        _used_pages = needed_pages < _owned_pages ? needed_pages : _owned_pages;
        impl::virtual_touch(reinterpret_cast<char *>(_ptr) + _capacity,
                            (_used_pages - previous_used_pages) * page_size);
        _capacity = _used_pages * page_size;
    }
    nlc::span<byte> const memory { _ptr + aligned_arena_size, size.v };
    _size = needed_size;
    nlc_assert_msg(_size <= _capacity, nlc_dump_var(_size), " <= ", nlc_dump_var(_capacity));
    return memory;
}

[[nodiscard]] auto arena_allocator::_reallocate(nlc::span<byte> const old,
                                                bytes<usize> const size,
                                                alignment const align) -> nlc::span<byte> {
    auto const new_memory = _allocate(size, align);
    nlc::memcpy(new_memory.ptr(), old.ptr(), old.size());
    _free(old);
    return new_memory;
}

void arena_allocator::clear() {
    nlc_assert_msg(allocation_count() == 0, nlc_dump_var(allocation_count()));
    _size = 0;
}

}  // namespace nlc
