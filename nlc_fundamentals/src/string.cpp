/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "string.hpp"

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/basic_output.hpp>
#include <nlc/dialect/memory.hpp>

namespace nlc {

auto string::size() const -> bytes<usize> {
    if (_data == nullptr)
        return bytes<usize>(0u);
    return *reinterpret_cast<bytes<usize> const *>(_data);
}

auto string::c_str() const -> char const * {
    if (_data == nullptr)
        return "";
    return reinterpret_cast<char const *>(_data) + sizeof(bytes<usize>);
}

string::string(empty_string_t)
    : _data { nullptr }
    , _allocator { nullptr } {}

string::~string() {
    if (_data != nullptr)
        _allocator->free(span<byte> { _data, size().v });
}

string::string(allocator & alloc, char const * str, bytes<usize> s)
    : _data(nullptr)
    , _allocator(&alloc) {
    if (s > bytes<usize>(0u)) {
        _data = _allocator->allocate<byte>(s.v + size_of<bytes<usize>>.v + 1).ptr();
        *reinterpret_cast<bytes<usize> *>(_data) = s;
        auto c_str = reinterpret_cast<char *>(_data) + sizeof(bytes<usize>);
        memcpy(c_str, str, s);
        c_str[s.v] = '\0';
    }
}

string::string(allocator & alloc, string_iterator const & begin, string_iterator const & end)
    : string(alloc, string_view(begin, end)) {}

string::string(allocator & alloc, string_view const str)
    : string(alloc, str.ptr(), str.size()) {}

string::string(string && other)
    : _data(other._data)
    , _allocator(other._allocator) {
    other._data = nullptr;
}

auto string::operator=(string && other) -> string & {
    nlc_assert(this != &other);
    nlc_assert(_allocator == other._allocator);
    if (_data != nullptr)
        _allocator->free(span<byte> { _data, size().v });
    _data = other._data;
    other._data = nullptr;
    return *this;
}

void format(log_entry & l, string const & s) { l.append(s.c_str(), s.size().v); }

}  // namespace nlc
