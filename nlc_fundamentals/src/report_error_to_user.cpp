/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "report_error_to_user.hpp"

#include <nlc/dialect/basic_output.hpp>

#include <stdlib.h>

namespace nlc {

static report_callback_type report_error_callback = impl::default_report_error_callback;

auto set_report_error_callback(report_callback_type const callback) -> void {
    report_error_callback = callback;
}
auto reset_report_error_callback() -> void {
    report_error_callback = impl::default_report_error_callback;
}

namespace impl {
    auto report_error_to_user(log_entry & entry, bool const is_retryable) -> void {
        ::nlc::report_error_callback(entry, is_retryable);
    }

    [[noreturn]] auto default_report_error_callback(log_entry & entry, bool const) -> void {
        entry.display(out_stream::err);
        abort();
    }
}  // namespace impl

}  // namespace nlc
