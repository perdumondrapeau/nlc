/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "hash.hpp"

#include <nlc/dialect/string_view.hpp>
#include <nlc/sanity/platforms.hpp>

#include "allocator.hpp"
#include "hash_stream.hpp"

#define XXH_INLINE_ALL
#define XXH_ACCEPT_NULL_INPUT_POINTER 0
#include <xxhash.h>

#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic push
#    pragma GCC diagnostic ignored "-Wused-but-marked-unused"
#    pragma GCC diagnostic ignored "-Wdisabled-macro-expansion"
#endif

namespace nlc {

[[nodiscard]] auto hash(void const * data, bytes<u64> len) -> hash_result {
    return hash_result { XXH3_64bits(data, len.v) };
}

[[nodiscard]] auto hash(void const * data, bytes<usize> len, u64 seed) -> hash_result {
    return hash_result { XXH3_64bits_withSeed(data, len.v, seed) };
}

[[nodiscard]] auto hash(span<byte> const s) -> hash_result { return hash(s.ptr(), s.size()); }
[[nodiscard]] auto hash(span<byte const> const s) -> hash_result { return hash(s.ptr(), s.size()); }
[[nodiscard]] auto hash(string_view const s) -> hash_result { return hash(s.ptr(), s.size()); }

// Implementation of hash_stream is here to use xxh3 more easily

hash_stream::hash_stream(allocator & alloc_, u64 s)
    : alloc { alloc_ }
    , state { alloc.create<XXH3_state_t>() }
    , seed { s } {
    reset();
}

hash_stream::~hash_stream() { alloc.destroy(reinterpret_cast<XXH3_state_t *>(state)); }

auto hash_stream::write(span<byte const> bytes) -> void {
    XXH3_64bits_update(static_cast<XXH3_state_t *>(state), bytes.ptr(), bytes.size());
}

auto hash_stream::reset() -> void {
    XXH3_64bits_reset_withSeed(static_cast<XXH3_state_t *>(state), seed);
}

auto hash_stream::digest() -> hash_result {
    auto const h = XXH3_64bits_digest(static_cast<XXH3_state_t *>(state));

    return hash_result { h };
}
}  // namespace nlc

#if NLC_COMPILER_CLANG
#    pragma GCC diagnostic pop
#endif
