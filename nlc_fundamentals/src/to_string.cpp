/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "to_string.hpp"

#include <stdio.h>

#include <nlc/dialect/assert.hpp>

namespace nlc {

#define IMPLEMENT_APPEND(TYPE, FMT)                                                            \
    template<> auto to_string<TYPE>(allocator & alloc, TYPE v) -> string {                     \
        constexpr auto buf_size = 512;                                                         \
        char buf[buf_size];                                                                    \
        [[maybe_unused]] auto const ret = snprintf(buf, buf_size, FMT, v);                     \
        nlc_assert(ret >= 0);                         /* Error occured ? */                    \
        nlc_assert(ret < static_cast<int>(buf_size)); /* Sufficient memory was  allocated ? */ \
        return string(alloc, buf, bytes<usize>(static_cast<usize>(ret)));                      \
    }

IMPLEMENT_APPEND(i8, prii8)
IMPLEMENT_APPEND(i16, prii16)
IMPLEMENT_APPEND(i32, prii32)
IMPLEMENT_APPEND(i64, prii64)
IMPLEMENT_APPEND(u8, priu8)
IMPLEMENT_APPEND(u16, priu16)
IMPLEMENT_APPEND(u32, priu32)
IMPLEMENT_APPEND(u64, priu64)
IMPLEMENT_APPEND(f64, "%f")

template<> auto to_string<float>(allocator & alloc, float v) -> string {
    return to_string<double>(alloc, static_cast<double>(v));
}

#undef IMPLEMENT_APPEND

}  // namespace nlc
