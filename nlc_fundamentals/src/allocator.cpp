/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#include "allocator.hpp"
#include "nlc/dialect/assert.hpp"

#ifndef NDEBUG
#    include "nlc/dialect/types.hpp"
#    include "time.hpp"
#endif

namespace nlc {

// This definition is needed to avoid -Wweak-vtables.
// If we made allocator an abstract class, its vtable would end up in every translation unit.
allocator::~allocator() {
#ifndef NDEBUG
    nlc_assert_msg(_count == 0, nlc_dump_var(_count));
#endif
}

#ifndef NDEBUG
namespace {
    struct header final {
        bytes<usize> size;
        alignment align;
        allocator const * alloc;
        time::duration time;
    };

    template<typename T> constexpr auto max(T const a, T const b) -> T { return a < b ? b : a; }
}  // namespace

auto allocator::allocate(bytes<usize> size, alignment align) -> span<byte> {
    auto const real_align = max(align, align_of<header>);
    auto const hdr_size = max(align.v, size_of<header>.v);
    auto const real_size = size.v + hdr_size;

    auto const alloc = _allocate(real_size, real_align);
    // increment after allocation is really done
    // in tests, we can nlc_expect_assert on this and don't want to register a failing allocation
    ++_count;

    auto & hdr = *reinterpret_cast<header *>(alloc.ptr() + hdr_size - size_of<header>);
    nlc_assert((reinterpret_cast<usize>(&hdr) & (align_of<header>.v - 1)) == 0);
    hdr = {
        .size = size,
        .align = align,
        .alloc = this,
        .time = time::now(),
    };

    return alloc.subspan(hdr_size);
}

auto allocator::reallocate(span<byte> old, bytes<usize> size, alignment align) -> span<byte> {
    if (old.ptr() == nullptr)
        return allocate(size, align);

    auto & old_hdr = *reinterpret_cast<header *>(old.ptr() - size_of<header>);
    nlc_assert_msg(old_hdr.alloc == this, "reallocate called on different allocator instance");

    auto const real_align = max(align, align_of<header>);
    auto const hdr_size = max(align.v, size_of<header>.v);
    auto const real_size = size.v + hdr_size;

    auto const old_size = old.size() + hdr_size;
    auto const real_old = span { old.ptr() - hdr_size, old_size };

    auto const alloc = _reallocate(real_old, real_size, real_align);

    // TODO check consistency with old alloc
    auto & hdr = *reinterpret_cast<header *>(alloc.ptr() + hdr_size - size_of<header>);
    nlc_assert((reinterpret_cast<usize>(&hdr) & (align_of<header>.v - 1)) == 0);
    hdr = {
        .size = size,
        .align = align,
        .alloc = this,
        .time = time::now(),
    };

    return alloc.subspan(hdr_size);
}

auto allocator::free(span<byte> memory, alignment align) -> void {
    if (memory.ptr() == nullptr)
        return;

    auto & hdr = *reinterpret_cast<header *>(memory.ptr() - size_of<header>);
    nlc_assert_msg(hdr.alloc == this, "free called on different allocator instance");
    --_count;

    auto const hdr_size = max(align.v, size_of<header>.v);
    auto const real_size = memory.size() + hdr_size;

    _free({ memory.ptr() - hdr_size, real_size });
}
#endif

}  // namespace nlc
