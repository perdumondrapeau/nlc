/* Copyright © 2021-2022
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/span.hpp>
#include <nlc/dialect/types.hpp>
#include "../allocator.hpp"

namespace nlc {

class standard_allocator final : public allocator {
  public:
    standard_allocator() = default;
    standard_allocator(standard_allocator &&) = delete;
    standard_allocator(standard_allocator const &) = delete;
    auto operator=(standard_allocator &&) -> standard_allocator & = delete;
    auto operator=(standard_allocator const &) -> standard_allocator & = delete;

    using allocator::allocate;
    using allocator::free;
    using allocator::reallocate;

  protected:
    [[nodiscard]] auto _allocate(bytes<usize> size, alignment align) -> nlc::span<byte> override;
    [[nodiscard]] auto _reallocate(nlc::span<byte> old,
                                   bytes<usize> size,
                                   alignment align) -> nlc::span<byte> override;
    auto _free(nlc::span<byte> memory) -> void override;
};

}  // namespace nlc
