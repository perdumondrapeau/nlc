/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/optional.hpp>
#include <nlc/dialect/string_view.hpp>

namespace nlc {

template<typename T> auto from_string(string_view const) -> optional<T>;

template<> auto from_string<i8>(string_view const) -> optional<i8>;
template<> auto from_string<i16>(string_view const) -> optional<i16>;
template<> auto from_string<i32>(string_view const) -> optional<i32>;
template<> auto from_string<i64>(string_view const) -> optional<i64>;
template<> auto from_string<u8>(string_view const) -> optional<u8>;
template<> auto from_string<u16>(string_view const) -> optional<u16>;
template<> auto from_string<u32>(string_view const) -> optional<u32>;
template<> auto from_string<u64>(string_view const) -> optional<u64>;
template<> auto from_string<f32>(string_view const) -> optional<f32>;
template<> auto from_string<f64>(string_view const) -> optional<f64>;

}  // namespace nlc
