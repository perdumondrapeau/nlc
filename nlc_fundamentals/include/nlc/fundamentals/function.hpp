/* Copyright © 2019-2024
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/function_view.hpp>
#include <nlc/dialect/new.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/traits.hpp>

#include "allocator.hpp"

namespace nlc {

template<typename> class function;

template<typename Ret, typename... Args> class function<Ret(Args...)> final {
  public:
    template<typename F> struct internal_type {
        allocator * alloc;
        F func;
    };

  public:
    using signature_type = Ret(Args...);
    using view_type = function_view<signature_type>;

    function(function const &) = delete;
    function(function && other)
        : _ptr(other._ptr)
        , _pf(other._pf)
        , _dtr(other._dtr) {
        other._ptr = nullptr;
        other._pf = nullptr;
        other._dtr = nullptr;
    }

    ~function() { destruct(); }

    function & operator=(function const &) = delete;
    function & operator=(function && other) {
        nlc_assert(this != &other);
        destruct();

        _ptr = other._ptr;
        _pf = other._pf;
        _dtr = other._dtr;

        other._ptr = nullptr;
        other._pf = nullptr;
        other._dtr = nullptr;

        return *this;
    }

    // Construct from function ptr
    function(allocator &, meta::add_ptr<signature_type> f)
        : _ptr(reinterpret_cast<void *>(f))
        , _pf([](void const * const * ptr, Args... args) -> Ret {
            return (reinterpret_cast<meta::add_ptr<signature_type>>(const_cast<void *>(*ptr)))(
                nlc_fwd(args)...);
        })
        , _dtr(nullptr) {}

    template<typename F> function(allocator & alloc, F && f) {
        if constexpr (sizeof(meta::rm_const_ref<F>) > sizeof(_ptr)) {
            init_with_alloc(alloc, nlc_fwd(f));
        } else {
            static_cast<void>(alloc);
            init_without_alloc(nlc_fwd(f));
        }
    }

    // Construct from function ptr
    function(meta::add_ptr<signature_type> f)
        : _ptr(reinterpret_cast<void *>(f))
        , _pf([](void const * const * ptr, Args... args) -> Ret {
            return (reinterpret_cast<meta::add_ptr<signature_type>>(const_cast<void *>(*ptr)))(
                nlc_fwd(args)...);
        })
        , _dtr(nullptr) {}

    template<typename F> function(F && f) { init_without_alloc(f); }

    auto operator()(Args... args) const -> Ret {
        return _pf(const_cast<void const * const *>(&_ptr), nlc_fwd(args)...);
    }

    operator view_type() const { return view_type(_ptr, _pf); }

  private:
#if NLC_CPU_ARM && NLC_COMPILER_GCC
    // On gcc starting version 12.2.0 (Debian 12.2.0-14) aarch64-linux-gnu we hit a weird optimizer
    // bug that will cause `_ptr` to stay zeroed no matter what, causing a crash when the code later
    // tries to use it for a deleter.
#    pragma GCC push_options
#    pragma GCC optimize("O1")
#endif
    auto bamboozle_gcc() { _dtr = nullptr; }
#if NLC_CPU_ARM && NLC_COMPILER_GCC
#    pragma GCC pop_options
#endif

    template<typename F> auto init_without_alloc(F && f) {
        using func_type = meta::rm_const_ref<F>;
        static_assert(sizeof(func_type) <= sizeof(_ptr));

        new (&_ptr) func_type(nlc_fwd(f));
        _pf = [](void const * const * ptr, Args... args) -> Ret {
            return (*static_cast<func_type *>(reinterpret_cast<void *>(const_cast<void **>(ptr))))(
                nlc_fwd(args)...);
        };

        bamboozle_gcc();

        if constexpr (!meta::is_trivially_destructible<func_type>) {
            _dtr = [](void ** ptr) { reinterpret_cast<func_type *>(ptr)->~func_type(); };
        }
    }

    template<typename F> auto init_with_alloc(allocator & alloc, F && f) {
        using func_type = meta::rm_const_ref<F>;
        static_assert(sizeof(func_type) > sizeof(_ptr));

        _ptr = alloc.create<internal_type<func_type>>(&alloc, nlc_fwd(f));
        _pf = [](void const * const * ptr, Args... args) -> Ret {
            return reinterpret_cast<internal_type<func_type> *>(const_cast<void *>(*ptr))
                ->func(nlc_fwd(args)...);
        };

        _dtr = [](void ** p) {
            auto ptr = reinterpret_cast<internal_type<func_type> *>(*p);
            ptr->alloc->destroy(ptr);
        };
    }

    void destruct() {
        if (_dtr != nullptr) {
            _dtr(&_ptr);
        }
    }

  private:
    mutable void * _ptr = nullptr;
    meta::add_ptr<Ret(void const * const *, Args...)> _pf = nullptr;
    meta::add_ptr<void(void **)> _dtr = nullptr;
};

template<typename allocator, typename Ret, typename... Args>
function(allocator &, Ret (*)(Args...)) -> function<Ret(Args...)>;
template<typename Ret, typename... Args>
function_view(function<Ret(Args...)> const &) -> function_view<Ret(Args...)>;
template<typename Ret, typename... Args>
function_view(function<Ret(Args...)> &) -> function_view<Ret(Args...)>;

}  // namespace nlc
