/* Copyright © 2021-2025
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/traits.hpp>

namespace nlc {

class output_byte_stream;
class input_byte_stream;

// Concepts to detect types that have custom streaming implementation
template<typename T>
concept output_byte_streamable =
    requires(output_byte_stream & o, T const & v) { to_byte_stream(o, v); };

template<typename T>
concept input_byte_streamable = requires(input_byte_stream & i, T & v) { from_byte_stream(i, v); };

// Trait to implement on client types that can be streamed as bytes in bulk. Containers will
// use it to optimize streaming for example.
//
// Example usage:
// template<> inline constexpr auto nlc::is_bulk_byte_streamable<MyType> = true;
template<typename T> inline constexpr auto is_bulk_byte_streamable = false;

// Default definition for integral types
template<typename T>
    requires meta::is_integral<T>
inline constexpr auto is_bulk_byte_streamable<T> = true;

class input_byte_stream {
  public:
    virtual ~input_byte_stream() = default;

    virtual auto read(span<byte> bytes) -> void = 0;
    virtual auto reset() -> void {}

    // Return read object as a value
    template<typename T, typename... Args> auto read(Args &&... args) -> T {
        T tmp { nlc_fwd(args)... };
        read(tmp);
        return tmp;
    }

    // Read object inplace
    template<typename T>
        requires meta::is_integral<T>
    auto read(T & x) -> void {
        read(span(reinterpret_cast<byte *>(&x), sizeof(x)));
    }

    template<input_byte_streamable T> auto read(T & t) { from_byte_stream(*this, t); }
};

class output_byte_stream {
  public:
    virtual ~output_byte_stream() = default;

    virtual auto write(span<byte const> bytes) -> void = 0;
    virtual auto reset() -> void {}

    template<typename T>
        requires meta::is_integral<T>
    auto write(T const & x) -> void {
        write(span(reinterpret_cast<byte const *>(&x), sizeof(x)));
    }

    auto write(string_view const s) -> void {
        write(span(reinterpret_cast<byte const *>(s.ptr()), s.size().v));
    }

    template<output_byte_streamable T> auto write(T const & t) { to_byte_stream(*this, t); }
};

}  // namespace nlc
