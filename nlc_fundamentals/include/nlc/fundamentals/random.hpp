/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/assert.hpp>

#if defined(_MSC_VER) && !defined(__clang__)
// msvc has no __int128
extern "C" {
unsigned __int64 _umul128(unsigned __int64 _Multiplier,
                          unsigned __int64 _Multiplicand,
                          unsigned __int64 * _HighProduct);
}
#    pragma intrinsic(_umul128)
#endif

// Our PRNG is xoroshiro128+.
// see http://prng.di.unimi.it/

namespace nlc {

inline u64 rotl(const u64 x, int k) { return (x << k) | (x >> (64 - k)); }

struct xoroshiro128plus final {
    u64 _state[2];

    xoroshiro128plus(u64 seed) {
        // Initialize state with a u64 seed by SplitMix64.
        u64 z = (seed += 0x9e3779b97f4a7c15);
        z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
        z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
        _state[0] = z ^ (z >> 31);

        z = (seed += 0x9e3779b97f4a7c15);
        z = (z ^ (z >> 30)) * 0xbf58476d1ce4e5b9;
        z = (z ^ (z >> 27)) * 0x94d049bb133111eb;
        _state[1] = z ^ (z >> 31);
    }

  private:
    auto next() -> u64 {
        u64 const s0 = _state[0];
        u64 s1 = _state[1];
        u64 const result = s0 + s1;

        s1 ^= s0;
        _state[0] = rotl(s0, 24) ^ s1 ^ (s1 << 16);
        _state[1] = rotl(s1, 37);

        return result;
    }

  public:
    // Return a random u64 in [0;range[
    auto less_than(u64 max) -> u64 {
        // Method from https://www.pcg-random.org/posts/bounded-rands.html

#if defined(_MSC_VER) && !defined(__clang__)
#    pragma warning(push)
#    pragma warning(disable : 4146)
        u64 x = next();
        u64 m;
        u64 l = _umul128(x, max, &m);
        if (l < max) {
            u64 t = -max;
            if (t >= max) {
                t -= max;
                if (t >= max)
                    t %= max;
            }
            while (l < t) {
                x = next();
                l = _umul128(x, max, &m);
            }
        }
        return m;
#    pragma warning(pop)
#else
        using u128 = __uint128_t;

        u64 x = next();
        u128 m = u128(x) * u128(max);
        u64 l = u64(m);
        if (l < max) {
            u64 t = -max;
            if (t >= max) {
                t -= max;
                if (t >= max)
                    t %= max;
            }
            while (l < t) {
                x = next();
                m = u128(x) * u128(max);
                l = u64(m);
            }
        }
        return static_cast<u64>(m >> 64);
#endif
    }

    auto in_range(u64 lo, u64 hi) -> u64 {
        nlc_assert(lo <= hi);
        return less_than(hi - lo) + lo;
    }

    auto in_range(u32 lo, u32 hi) -> u32 {
        nlc_assert(lo <= hi);
        return static_cast<u32>(less_than(hi - lo)) + lo;
    }

    auto in_range(i64 lo, i64 hi) -> i64 {
        nlc_assert(lo <= hi);
        return static_cast<i64>(less_than(static_cast<u64>(hi - lo))) + lo;
    }

    // Return random f64 in [0;1[
    auto unit_f64() -> f64 { return static_cast<f64>(next() >> 11) * 0x1.p-53; }
    // Return random f32 in [0;1[
    auto unit_f32() -> f32 { return static_cast<f32>(next() >> 40) * 0x1.p-24f; }

    // TODO pretty sure this is not statistically correct
    auto in_range(f32 lo, f32 hi) -> f32 {
        nlc_assert(lo <= hi);
        return unit_f32() * (hi - lo) + lo;
    }

    auto in_range(f64 lo, f64 hi) -> f64 {
        nlc_assert(lo <= hi);
        return unit_f64() * (hi - lo) + lo;
    }

    auto get_u64() -> u64 { return next(); }
    auto get_u32() -> u32 { return static_cast<u32>(next() >> 32); }
    auto get_u16() -> u16 { return static_cast<u16>(next() >> 48); }
    auto get_u8() -> u8 { return static_cast<u8>(next() >> 56); }
    auto get_bool() -> bool { return (next() >> 63) & 1; }
};

}  // namespace nlc
