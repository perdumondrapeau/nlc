/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/string_view.hpp>

#include "allocator.hpp"

namespace nlc {

class string_stream final {
  public:
    string_stream(allocator &);
    ~string_stream();

  public:
    auto reserve(bytes<usize>) -> void;
    auto resize(bytes<usize>) -> void;
    auto append(bool) -> void;
    auto append(char *) -> void;
    auto append(char const *) -> void;
    auto append(void const *, usize) -> void;
    auto append(char) -> void;
    auto append(i8) -> void;
    auto append(i16) -> void;
    auto append(i32) -> void;
    auto append(i64) -> void;
    auto append(u8) -> void;
    auto append(u16) -> void;
    auto append(u32) -> void;
    auto append(u64) -> void;
    auto append(f32) -> void;
    auto append(f64) -> void;
    auto append(codepoint_type) -> void;
    auto append(string_view const) -> void;

    template<usize size> auto append(char const (&str)[size]) -> void { append(str, size - 1); }
    template<typename T> auto append(T const & other) -> void { format(*this, other); }

    auto clear() -> void;
    auto clear_reduce_memory() -> void;

  public:
    operator string_view() const { return string_view(_data, _size); }
    auto size() const { return _size; }
    auto capacity() const { return _capacity; }
    auto c_str() const { return _data; }

  private:
    allocator * _allocator;
    char * _data;
    bytes<usize> _size;
    bytes<usize> _capacity;
};

template<typename T> auto operator<<(string_stream & ss, T const & v) -> string_stream & {
    ss.append(v);
    return ss;
}

template<typename... T> auto append(string_stream & stream, T &&... args) -> void {
    (stream.append(args), ...);
}

}  // namespace nlc
