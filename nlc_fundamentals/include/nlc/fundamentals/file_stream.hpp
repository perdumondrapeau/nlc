/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/expected.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/string_view.hpp>

#include "byte_stream.hpp"

namespace nlc {

class allocator;

struct access_denied {};
struct bad_path_name {};
struct path_not_found {};
struct unknown_error {};

class output_file_stream final : public output_byte_stream {
  public:
    using output_byte_stream::write;

    using create_result =
        expected<output_file_stream, access_denied, bad_path_name, path_not_found, unknown_error>;

    [[nodiscard]] static auto open(allocator & alloc_, nlc::string_view filepath, usize buffer_size)
        -> create_result;

    output_file_stream(output_file_stream const &) = delete;
    output_file_stream(output_file_stream &&);
    ~output_file_stream() override;

    auto write(span<byte const> bytes) -> void final;
    auto reset() -> void final;

    [[nodiscard]] auto any_error_occurred() const -> bool;

  private:
    static constexpr usize no_file = 0;

    output_file_stream(allocator & alloc_, usize file, span<byte> buffer);
    auto flush() -> void;

    allocator & alloc;
    span<byte> buffer;  // Allocated and owned by the stream
    usize cursor = 0;
    usize file = no_file;
    bool error_occurred = false;
#if !defined(NDEBUG)
    mutable bool error_checked = false;
#endif
};

class input_file_stream final : public input_byte_stream {
  public:
    using input_byte_stream::read;

    using create_result =
        expected<input_file_stream, access_denied, bad_path_name, path_not_found, unknown_error>;

    [[nodiscard]] static auto open(allocator & alloc_, nlc::string_view filepath, usize buffer_size)
        -> create_result;

    input_file_stream(input_file_stream const &) = delete;
    input_file_stream(input_file_stream &&);
    ~input_file_stream() override;

    virtual auto read(span<byte> bytes) -> void final override;
    virtual auto reset() -> void final override;

    [[nodiscard]] auto any_error_occurred() const -> bool;

  private:
    static constexpr usize no_file = 0;

    input_file_stream(allocator & alloc_, usize file, span<byte> buffer);

    allocator & alloc;
    span<byte> buffer;  // Allocated and owned by the stream
    span<byte> pending;
    usize file = no_file;
    bool error_occurred = false;
#if !defined(NDEBUG)
    mutable bool error_checked = false;
#endif
};

}  // namespace nlc
