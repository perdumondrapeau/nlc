/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>
#include <nlc/dialect/memory.hpp>
#include <nlc/dialect/new.hpp>
#include <nlc/meta/basics.hpp>

#include "function.hpp"

namespace nlc {

template<typename T> class scoped {
  public:
    template<typename Q, typename F>
    scoped(Q && arg, F && deleter)
        : _deleter(nlc_fwd(deleter))
        , _data() {
        new (_ptr()) T(nlc_fwd(arg));
    }

    ~scoped() { _deleter(*_ptr()); }

    scoped(scoped && other)
        : _deleter(nlc_move(other._deleter))
        , _data() {
        memcpy(_data, other._data, size_of<T>);
        other._deleter = [](T &) {};
    }

    auto operator=(scoped && other) -> scoped & {
        nlc_assert(this != &other);
        _deleter(*_ptr());
        memcpy(_data, other._data, size_of<T>);
        _deleter = nlc_move(other._deleter);
        other._deleter = [](T &) {};
        return *this;
    }

    scoped(scoped const &) = delete;
    auto operator=(scoped const &) -> scoped & = delete;

    auto operator->() const & -> T const * { return _ptr(); }
    auto operator->() & -> T * { return _ptr(); }
    auto operator*() const & -> T const & { return *_ptr(); }
    auto operator*() & -> T & { return *_ptr(); }
    auto operator*() && -> T { return nlc_move(*_ptr()); }

  private:
    auto _ptr() { return reinterpret_cast<T *>(_data); }
    auto _ptr() const { return reinterpret_cast<T const *>(_data); }

  private:
    function<void(T &)> _deleter;
    alignas(T) byte _data[sizeof(T)];
};

template<typename T, typename D>
scoped(T && resource, D && deleter) -> scoped<meta::rm_const_ref<T>>;

}  // namespace nlc
