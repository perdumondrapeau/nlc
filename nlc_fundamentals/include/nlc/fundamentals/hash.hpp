/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/span.hpp>
#include <nlc/dialect/types.hpp>
#include <nlc/meta/id.hpp>

namespace nlc {

class string_view;

namespace impl::hash {
    struct hash_def {
        using underlying_type = u64;
    };
}  // namespace impl::hash

using hash_result = meta::id<impl::hash::hash_def>;

[[nodiscard]] inline constexpr auto hash(u64 x) -> hash_result {
    // Mix13 http://zimbry.blogspot.com/2011/09/better-bit-mixing-improving-on.html
    x ^= x >> 30;
    x *= 0xbf58476d1ce4e5b9;
    x ^= x >> 27;
    x *= 0x94d049bb133111eb;
    x ^= x >> 31;

    return hash_result { x };
}

[[nodiscard]] auto hash(void const * data, bytes<usize> len) -> hash_result;
[[nodiscard]] auto hash(void const * data, bytes<usize> len, u64 seed) -> hash_result;
[[nodiscard]] auto hash(span<byte> const s) -> hash_result;
[[nodiscard]] auto hash(span<byte const> const s) -> hash_result;
[[nodiscard]] auto hash(string_view const s) -> hash_result;

[[nodiscard]] inline constexpr auto hash(hash_result const x) -> hash_result { return x; }
[[nodiscard]] inline constexpr auto hash(i64 const x) -> hash_result {
    return hash(static_cast<u64>(x));
}
[[nodiscard]] inline constexpr auto hash(u32 const x) -> hash_result {
    return hash(static_cast<u64>(x));
}
[[nodiscard]] inline constexpr auto hash(i32 const x) -> hash_result {
    return hash(static_cast<u64>(x));
}
[[nodiscard]] inline constexpr auto hash(u16 const x) -> hash_result {
    return hash(static_cast<u64>(x));
}
[[nodiscard]] inline constexpr auto hash(i16 const x) -> hash_result {
    return hash(static_cast<u64>(x));
}
[[nodiscard]] inline constexpr auto hash(u8 const x) -> hash_result {
    return hash(static_cast<u64>(x));
}
[[nodiscard]] inline constexpr auto hash(i8 const x) -> hash_result {
    return hash(static_cast<u64>(x));
}

template<typename Descriptor, typename Self>
[[nodiscard]] inline auto hash(meta::id<Descriptor, Self> const id) -> hash_result {
    return nlc::hash(id.value());
}

auto hash(char const *) = delete;

template<typename T> [[nodiscard]] inline auto hash(T * const ptr) -> hash_result {
    return hash(reinterpret_cast<u64>(ptr));
}

template<typename T> [[nodiscard]] inline constexpr auto hash(T const & obj) -> hash_result {
    // hash requires either a `nlc::hash_result hash(T)` free function in the namespace of T or
    // a `nlc::hash_result T::hash()` method
    return obj.hash();
}

template<typename T> auto format(T & log, hash_result const h) -> void { log.append(h.value()); }

}  // namespace nlc
