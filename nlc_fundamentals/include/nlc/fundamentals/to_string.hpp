/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/arithmetic_types.hpp>

#include "allocator.hpp"
#include "string.hpp"

namespace nlc {

template<typename T> auto to_string(allocator &, T) -> string;

template<> auto to_string<i8>(allocator &, i8) -> string;
template<> auto to_string<i16>(allocator &, i16) -> string;
template<> auto to_string<i32>(allocator &, i32) -> string;
template<> auto to_string<i64>(allocator &, i64) -> string;
template<> auto to_string<u8>(allocator &, u8) -> string;
template<> auto to_string<u16>(allocator &, u16) -> string;
template<> auto to_string<u32>(allocator &, u32) -> string;
template<> auto to_string<u64>(allocator &, u64) -> string;
template<> auto to_string<f32>(allocator &, f32) -> string;
template<> auto to_string<f64>(allocator &, f64) -> string;

}  // namespace nlc
