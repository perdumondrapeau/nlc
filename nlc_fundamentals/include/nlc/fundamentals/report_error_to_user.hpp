/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/basic_output.hpp>

namespace nlc {
namespace impl {
    auto report_error_to_user(log_entry & entry, bool const is_retryable) -> void;
    [[noreturn]] auto default_report_error_callback(log_entry & entry, bool const is_retryable) -> void;
}  // namespace impl

enum class retryable_error_result { ok, retry };

using report_callback_type = decltype(impl::default_report_error_callback) *;
auto set_report_error_callback(report_callback_type const) -> void;
auto reset_report_error_callback() -> void;

template<typename... Types>
auto report_error_to_user(bool const condition, Types &&... args) -> void {
    if (condition)
        return;
    log_entry entry {};
    append(entry, args...);
    impl::report_error_to_user(entry, false);
}

template<typename... Types> auto report_error_to_user(Types &&... args) -> void {
    log_entry entry {};
    append(entry, args...);
    impl::report_error_to_user(entry, false);
}

template<typename... Types>
[[nodiscard]] auto report_retryable_error_to_user(bool const condition,
                                                  Types &&... args) -> retryable_error_result {
    if (condition)
        return retryable_error_result::ok;
    log_entry entry {};
    append(entry, args...);
    impl::report_error_to_user(entry, true);
    return retryable_error_result::retry;
}

// This function has no condition check. Hence it should be called in a code path where it is known
// that an error occured.
// It implies that it doesn't need to return a `retryable_error_result` since returning from the
// function means that the user has chosen to retry.
template<typename... Types> auto report_retryable_error_to_user(Types &&... args) -> void {
    log_entry entry {};
    append(entry, args...);
    impl::report_error_to_user(entry, true);
}
}  // namespace nlc
