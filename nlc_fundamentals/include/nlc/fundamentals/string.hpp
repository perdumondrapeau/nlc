/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/string_view.hpp>

#include "allocator.hpp"
#include "hash.hpp"

namespace nlc {

class [[nodiscard]] string final {
    class empty_string_t {};

  public:
    static inline empty_string_t empty;

  public:
    [[nodiscard]] auto size() const -> bytes<usize>;
    [[nodiscard]] auto is_empty() const -> bool { return size() == 0u; }

    [[nodiscard]] auto c_str() const -> char const *;

    [[nodiscard]] auto begin() const { return string_iterator(c_str()); }
    [[nodiscard]] auto end() const { return string_iterator(c_str() + size()); }

  public:
    [[nodiscard]] operator string_view() const { return string_view(c_str(), size()); }
    [[nodiscard]] auto copy(allocator & a) const { return string(a, operator string_view()); }
    [[nodiscard]] auto copy() const { return string(*_allocator, operator string_view()); }

  public:
    string() = delete;
    string(empty_string_t);
    ~string();
    string(allocator & alloc, char const * str, bytes<usize> s);
    string(allocator & alloc, string_iterator const &, string_iterator const &);
    string(allocator & alloc, string_view const str);
    string(string &&);
    auto operator=(string &&) -> string &;
    string(string const &) = delete;
    auto operator=(string const &) = delete;

    [[nodiscard]] static inline auto from_c_str(allocator & alloc, char const * str) {
        return string(alloc, str, str_size(str));
    }

  private:
    byte * _data;
    allocator * _allocator;
};

[[nodiscard]] inline auto hash(string const & s) -> hash_result {
    return hash(s.c_str(), s.size());
}

class log_entry;
void format(log_entry & l, string const & s);

}  // namespace nlc
