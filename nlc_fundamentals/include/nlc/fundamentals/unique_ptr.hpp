/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/null.hpp>
#include <nlc/meta/basics.hpp>
#include <nlc/meta/traits.hpp>

#include "allocator.hpp"

namespace nlc {

template<typename T> class unique_ptr final {
    template<typename Q> friend class unique_ptr;

  public:
    unique_ptr()
        : _ptr(nullptr)
        , _allocator(nullptr) {}
    unique_ptr(nullptr_type)
        : unique_ptr {} {}
    unique_ptr(const unique_ptr &) = delete;
    ~unique_ptr() { reset(); }
    auto operator=(const unique_ptr &) = delete;
    auto operator=(nullptr_type) -> void { reset(); }

    auto operator==(nullptr_type) const -> bool { return _ptr == nullptr; }
    auto operator!=(nullptr_type) const -> bool { return _ptr != nullptr; }

    template<typename Q>
    explicit unique_ptr(allocator & alloc, Q * ptr)
        : _ptr { ptr }
        , _allocator(&alloc) {
        static_assert(meta::disjonction<meta::is_same<T, Q>, meta::has_virtual_destructor<T>>);
    }

    template<typename Q>
    unique_ptr(unique_ptr<Q> && other)
        : _ptr(other._ptr)
        , _allocator(other._allocator) {
        static_assert(meta::disjonction<meta::is_same<T, Q>, meta::has_virtual_destructor<T>>);
        other._ptr = nullptr;
    }

  public:  // assignations
    auto reset() -> void {
        if (_ptr != nullptr) {
            _allocator->destroy(_ptr);
            _ptr = nullptr;
        }
    }
    auto reset(allocator & alloc, T * ptr) -> void {
        reset();
        _ptr = ptr;
        _allocator = &alloc;
    }

    template<typename Q> auto operator=(unique_ptr<Q> && other) -> unique_ptr & {
        nlc_assert(this->_ptr != static_cast<T *>(other._ptr));
        nlc_assert(_allocator == other._allocator);
        static_assert(meta::disjonction<meta::is_same<T, Q>, meta::has_virtual_destructor<T>>);
        if (other._allocator != nullptr) {
            reset(*other._allocator, other._ptr);
        } else {
            nlc_assert(other._ptr == nullptr);
            reset();
        }
        other._ptr = nullptr;
        return *this;
    }

  public:
    [[nodiscard]] auto operator*() -> T & { return *_ptr; }
    [[nodiscard]] auto operator*() const -> const T & { return *_ptr; }
    [[nodiscard]] auto operator->() -> T * { return _ptr; }
    [[nodiscard]] auto operator->() const -> const T * { return _ptr; }

  public:
    [[nodiscard]] auto get() const -> T * { return _ptr; }

  private:
    T * _ptr;
    allocator * _allocator;
};

template<typename T, typename... Args>
[[nodiscard]] decltype(auto) make_unique(allocator & alloc, Args &&... args) {
    auto memory = alloc.allocate<T>().ptr();
    nlc_assert(memory != nullptr);
    new (memory) T(nlc_fwd(args)...);
    return unique_ptr<T>(alloc, memory);
}
}  // namespace nlc
