/* Copyright © 2021-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include "byte_stream.hpp"
#include "hash.hpp"

namespace nlc {

class allocator;

class hash_stream final : public output_byte_stream {
  public:
    hash_stream(allocator & alloc_, u64 seed = 0);
    ~hash_stream() override;

    using output_byte_stream::write;
    auto write(span<byte const> bytes) -> void override;
    auto reset() -> void override;

    auto digest() -> hash_result;

  private:
    allocator & alloc;
    void * state;
    u64 seed;
};

}  // namespace nlc
