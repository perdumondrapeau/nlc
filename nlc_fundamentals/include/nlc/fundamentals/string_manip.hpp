/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/expected.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/string_iterator.hpp>
#include <nlc/dialect/string_view.hpp>
#include <nlc/dialect/utf8.hpp>

namespace nlc {

class allocator;

[[nodiscard]] auto starts_with(string_view const str, string_view const prefix) -> bool;
[[nodiscard]] auto ends_with(string_view const str, string_view const suffix) -> bool;
[[nodiscard]] auto levenshtein_distance(nlc::allocator & allocator,
                                        string_view const a,
                                        string_view const b) -> usize;
[[nodiscard]] auto find(string_view, codepoint_type) -> string_iterator;
[[nodiscard]] auto find_last(string_view, codepoint_type) -> string_iterator;
[[nodiscard]] auto contains(string_view s, codepoint_type c) -> bool;
[[nodiscard]] auto get_codepoint_at(string_view const str, usize const index = 0u) -> string_iterator;

[[nodiscard]] auto get_splice_count(string_view const string, codepoint_type const separator) -> usize;
// Get the beggining of the string until `separator` is found.
// Result does not include the separator.
[[nodiscard]] auto get_first_splice(string_view const string,
                                    codepoint_type const separator) -> string_view;
// Split string when meeting `separator`.
// The splices are written to `output_buffer` and don't contain `separator`.
// Return the span that was actually filled in `output_buffer`, or buffer_too_small if
// `output_buffer` can't contain all the splices.
struct buffer_too_small final {};
[[nodiscard]] auto try_split_string(string_view const string,
                                    codepoint_type const separator,
                                    span<string_view> const output_buffer)
    -> expected<span<string_view>, buffer_too_small>;
// Split string when meeting `separator`.
// The splices are written to `output_buffer` and don't contain `separator`.
// Asserts if `output_buffer` can't contain all the splices.
// Return the span that was actually filled in `output_buffer`
[[nodiscard]] auto split_string(string_view const string,
                                codepoint_type const separator,
                                span<string_view> const output_buffer) -> span<string_view>;

/*
[[nodiscard]] auto getFront(string_view str, split_options options = split_options()) ->
string_view;
[[nodiscard]] auto frontChar(string_view) -> codepoint_type;
[[nodiscard]] auto backChar(string_view) -> codepoint_type;
[[nodiscard]] auto popFrontChar(string_view) -> string_view;
*/

/*
[[nodiscard]] auto removeQuotes(string_view str, string_view quotes) -> string_view;
[[nodiscard]] auto toInt(string_view, int base = 10) -> int;
[[nodiscard]] auto toFloat(string_view) -> float;
[[nodiscard]] auto toDouble(string_view) -> double;
*/
}  // namespace nlc
