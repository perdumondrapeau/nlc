/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/new.hpp>
#include <nlc/dialect/span.hpp>
#include <nlc/dialect/types.hpp>
#include <nlc/meta/is_same.hpp>

namespace nlc {

class align_def {};
using alignment = meta::unit<meta::pair<meta::list<align_def>, meta::list<>>, usize>;
inline constexpr auto operator""_align(unsigned long long v) -> alignment {
    return alignment(static_cast<usize>(v));
}

template<typename T> inline constexpr auto align_of = alignment(alignof(T));

class allocator {
  public:
    allocator() = default;
    virtual ~allocator();
    allocator(allocator &&) = delete;
    allocator(allocator const &) = delete;
    auto operator=(allocator &&) -> allocator & = delete;
    auto operator=(allocator const &) -> allocator & = delete;

    template<typename T> [[nodiscard]] auto allocate(usize count = 1u) -> span<T> {
        return span_cast<T>(allocate(count * size_of<T>, align_of<T>));
    }
    template<typename T> [[nodiscard]] auto reallocate(span<T> old, usize count) -> span<T> {
        return span_cast<T>(reallocate(span_cast<byte>(old), count * size_of<T>, align_of<T>));
    }
    template<typename T> auto free(span<T> objects) -> void {
        free(span_cast<byte>(objects), align_of<T>);
    }

    template<typename T, typename... Args> [[nodiscard]] auto create(Args &&... args) -> T * {
        auto ptr = allocate<T>().ptr();
        new (ptr) T { nlc_fwd(args)... };
        return ptr;
    }

    template<typename T> auto destroy(T * ptr) -> void {
        nlc_assert(ptr != nullptr);
        ptr->~T();
        free({ reinterpret_cast<byte *>(ptr), sizeof(T) }, align_of<T>);
    }

    // TODO protected: ?
#ifdef NDEBUG
    // non-debug directly calls implementation in the derived class

    [[nodiscard]] auto allocate(bytes<usize> size, alignment align) -> span<byte> {
        return _allocate(size, align);
    }

    [[nodiscard]] auto reallocate(span<byte> old, bytes<usize> size, alignment align) -> span<byte> {
        return _reallocate(old, size, align);
    }

    auto free(span<byte> memory, alignment) -> void { _free(memory); }
#else
    // debug wraps calls with runtime checks
    [[nodiscard]] auto allocate(bytes<usize> size, alignment align) -> span<byte>;
    [[nodiscard]] auto reallocate(span<byte> old, bytes<usize> size, alignment align) -> span<byte>;
    auto free(span<byte> memory, alignment align) -> void;
#endif

  protected:
    [[nodiscard]] virtual auto _allocate(bytes<usize> size, alignment align) -> span<byte> = 0;
    [[nodiscard]] virtual auto _reallocate(span<byte> old,
                                           bytes<usize> size,
                                           alignment align) -> span<byte> = 0;
    virtual auto _free(span<byte> memory) -> void = 0;

#ifndef NDEBUG
    [[nodiscard]] auto allocation_count() -> usize { return _count; }

  private:
    usize _count = 0;
#endif
};

}  // namespace nlc
