/* Copyright © 2019-2021
            Alexis A. D. COLIN,
            Antoine VUGLIANO,
            Gaëtan CHAMPARNAUD,
            Geoffrey L. TOURON,
            Grégoire A. P. BADIN
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission
- subsequentversions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence
is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the
Licence.*/

#pragma once

#include <nlc/dialect/assert.hpp>
#include <nlc/dialect/types.hpp>

namespace nlc::time {

struct time_def {};
using duration = meta::unit<meta::pair<meta::list<time_def>, meta::list<>>, u64>;

auto now() -> duration;

inline constexpr u64 ns_per_s = 1'000'000'000;
inline constexpr u64 ns_per_ms = 1'000'000;
inline constexpr u64 ns_per_us = 1'000;

template<typename T = float> [[nodiscard]] constexpr auto seconds(duration time) -> T {
    return static_cast<T>(rm_unit(time)) / ns_per_s;
}

template<typename T = float> [[nodiscard]] constexpr auto milliseconds(duration time) -> T {
    return static_cast<T>(rm_unit(time)) / ns_per_ms;
}

template<typename T = float> [[nodiscard]] constexpr auto microseconds(duration time) -> T {
    return static_cast<T>(rm_unit(time)) / ns_per_us;
}

template<typename T = float> [[nodiscard]] constexpr auto nanoseconds(duration time) -> T {
    return static_cast<T>(rm_unit(time));
}

template<typename T = float> [[nodiscard]] constexpr auto from_seconds(T time) -> duration {
    return static_cast<u64>(time * static_cast<T>(ns_per_s));
}

template<typename T = float> [[nodiscard]] constexpr auto from_milliseconds(T time) -> duration {
    return static_cast<u64>(time * static_cast<T>(ns_per_ms));
}

template<typename T = float> [[nodiscard]] constexpr auto from_microseconds(T time) -> duration {
    return static_cast<u64>(time * static_cast<T>(ns_per_us));
}

template<typename T = float> [[nodiscard]] constexpr auto from_nanoseconds(T time) -> duration {
    return static_cast<u64>(time);
}

}  // namespace nlc::time

inline constexpr auto operator""_s(unsigned long long s) -> nlc::time::duration {
    return { s * nlc::time::ns_per_s };
}
inline constexpr auto operator""_ms(unsigned long long ms) -> nlc::time::duration {
    return { ms * nlc::time::ns_per_ms };
}
inline constexpr auto operator""_us(unsigned long long us) -> nlc::time::duration {
    return { us * nlc::time::ns_per_us };
}
inline constexpr auto operator""_ns(unsigned long long ns) -> nlc::time::duration { return { ns }; }
